	SELECT DISTINCT
			 OwnerID=dbo.fnc_OwnerID()
			,T.ImportId, T.ASRImpID, T.ASRLink, T.ASRType, T.RE_DB_OwnerShort
			,RecordTypeID=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN dbo.fnc_RecordType('Relationship_Contact_Contact')
								WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN dbo.fnc_RecordType('Relationship_Account_Contact')  --need to revert from cont-acct TO acct-contac
								WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN dbo.fnc_RecordType('Relationship_Account_Contact')
								WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN dbo.fnc_RecordType('Relationship_Account_Account') END 
			,External_ID__c=T.RE_DB_OwnerShort+'-'+T.ASRImpID
			,RE_IRImpID__c=NULL
			,RE_ORImpID__c=NULL
			,RE_ASRImpID__c= T.RE_DB_OwnerShort+'-'+T.ASRImpID
			,RE_ESRImpID__c=NULL 
			,[rC_Bios__Account_1__r:External_ID__c]=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN NULL
										WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN (T2.NEW_TGT_IMPORT_ID)  --need to revert from cont-acct TO acct-contac
										WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN (T1.NEW_TGT_IMPORT_ID) 
										WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN (T1.NEW_TGT_IMPORT_ID) END 
			,[rC_Bios__Contact_1__r:External_ID__c]=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN (T1.NEW_TGT_IMPORT_ID) 
										WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN NULL
										WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN NULL
										WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN NULL END 
			,[rC_Bios__Account_2__r:External_ID__c]=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN NULL
										WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN NULL
										WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN NULL
										WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN (T2.NEW_TGT_IMPORT_ID) END 
			,[rC_Bios__Contact_2__r:External_ID__c]=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN (T2.NEW_TGT_IMPORT_ID)
										WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN (T1.NEW_TGT_IMPORT_ID) --need to revert from cont-acct TO acct-contac
										WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN (T2.NEW_TGT_IMPORT_ID)
										WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN NULL END 
			,rC_Bios__Category__c=T3.RELATIONSHIP_CATEGORY
			,rC_Bios__Role_1__c=T3.RELATIONSHIP_ROLE_1
			,rC_Bios__Role_2__c=T3.RELATIONSHIP_ROLE_2
			,rC_Bios__Starting_Month__c=CASE WHEN LEN(T.ASRDateFrom)>7 THEN MONTH(T.ASRDateFrom) WHEN LEN(T.ASRDateFrom)=7 THEN LEFT(T.ASRDateFrom,2) WHEN LEN(T.ASRDateFrom)=4 THEN NULL END
			,rC_Bios__Starting_Day__c=CASE WHEN LEN(T.ASRDateFrom)>7 THEN DAY(T.ASRDateFrom) END
			,rC_Bios__Starting_Year__c=CASE WHEN LEN(T.ASRDateFrom)>7 THEN YEAR(T.ASRDateFrom)  WHEN LEN(T.ASRDateFrom)=7 THEN RIGHT(T.ASRDateFrom,4) WHEN LEN(T.ASRDateFrom)=4 THEN T.ASRDateFrom END
			,rC_Bios__Stopping_Month__c=CASE WHEN LEN(T.ASRDateTo)>7 THEN MONTH(T.ASRDateTo) WHEN LEN(T.ASRDateTo)=7 THEN LEFT(T.ASRDateTo,2) WHEN LEN(T.ASRDateTo)=4 THEN NULL END
			,rC_Bios__Stopping_Day__c=CASE WHEN LEN(T.ASRDateTo)>7 THEN DAY(T.ASRDateTo) END
			,rC_Bios__Stopping_Year__c=CASE WHEN LEN(T.ASRDateTo)>7 THEN YEAR(T.ASRDateTo)  WHEN LEN(T.ASRDateTo)=7 THEN RIGHT(T.ASRDateTo,4) WHEN LEN(T.ASRDateTo)=4 THEN T.ASRDateTo END
			,CASE  WHEN [dbo].[fn_DateConvert](CASE WHEN LEN(T.ASRDateTo)='8' THEN T.ASRDateTo ELSE NULL END) < GETDATE() THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Active__c
			,NULL AS rC_Bios__Primary__c
			,rC_Bios__Comments__c=CAST(T.ASRNotes AS VARCHAR(MAX)) 
			,Education_Relationship_Type__c=NULL
			,Education_Major__c=NULL
			,Education_Minor__c=NULL

			,Affiliation__c=T.RE_DB_OwnerShort
	 
				--ref
				,T1.KeyInd refT1_KeyInd, T1.ImportID refT1_ImportId
				,T2.KeyInd refT2_KeyInd, T2.ImportID refT2_ImportId
				,T.ASRImpID AS refRImpID, T.ASRLink ref_RLink, T.RE_DB_OwnerShort, T.RE_DB_Tbl
				,T.ASRDateFrom refDateFrom, T.ASRDateTo refDateTo
				,T.ASRType refRelat, 'Solicitee' refRecip
				,'Cons_Solicitor' AS refSource			
			FROM SUTTER_1P_DATA.dbo.HC_Cons_Solicitor_v T
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T2 ON T.ASRLink=T2.ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_DATA.[dbo].[CHART_ConsSolicitor] T3 ON T.ASRLink=T3.ASRLink AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort AND T.ASRType=T3.ASRType
			WHERE T3.RELATIONSHIP_CATEGORY='Solicitor'
			ORDER BY T.ASRImpID
			--SELECT * FROM  SUTTER_1P_DATA.dbo.HC_Cons_Solicitor_v
		---	SELECT * FROM SUTTER_1P_DATA.[dbo].[CHART_ConsSolicitor]

	  