	
-- B/C charts needed to be split by regions, they need to be put back together/merged in order to proceed with finding new
-- values added between conversion.  
-- Below we Merge CHARTS recived from reviews FROM CLIENT/HC staff. CHARTS are put into tables instead of views. 
-- e.g. CHARTM.test_actiontype will be compared with new refreshed CHARTM.ActionType view in order to find new values. 

--1.connect to db.
--2.remove if exists.
--3.compile/merge


--1.
	USE RE_SUTTER_CHARTS
	GO

--2.remove if exists.

		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_ActionStatus]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_ActionStatus]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_ActionType]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_ActionType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_AddressType]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_AddressType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_Attributes]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_Attributes]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_CampaignAppeal]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_CampaignAppeal]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_ConsAttributes]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_ConsAttributes]
		--if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[ConsAttributes_nonTable]') and objectproperty(id, N'isTable') = 1)
		--	drop table  [CHARTM].[ConsAttributes_nonTable]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_ConsSolicitCodes]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_ConsSolicitCodes]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_ConstituencyCodes]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_ConstituencyCodes]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_EventType]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_EventType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_Fund]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_Fund]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_GiftAttribute]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_GiftAttribute]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_GiftCode]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_GiftCode]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_GiftSubType]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_GiftSubType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_ParticipantType]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_ParticipantType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHARTM].[Test_PhoneType]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHARTM].[Test_PhoneType]
		 
--3.compile/merge


--ActionStatus 
		 select DB_TYPE, RE_DB_OwnerShort, ACStatus, [COUNT], [CONVERT], NewACStatus 
		 into CHARTM.Test_ActionStatus
		 from dbo.CHART_CVR_ActionStatus where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, ACStatus, [COUNT], [CONVERT], NewACStatus 
		 from dbo.CHART_EBR_ActionStatus where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, ACStatus, [COUNT], [CONVERT], NewACStatus 
		 from dbo.CHART_PCR_ActionStatus where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, ACStatus, [COUNT], [CONVERT], NewACStatus 
		 from dbo.CHART_SCAH_ActionStatus where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, ACStatus, [COUNT], [CONVERT], NewACStatus 
		 from dbo.CHART_SSR_ActionStatus where RE_DB_OwnerShort is not null
		 
--ActionType 
		 select DB_TYPE, RE_DB_OwnerShort, ACType, [COUNT], [CONVERT], NewACType, NewACAttrCat, NewACAttrDesc
		 into CHARTM.Test_ActionType
		 from dbo.CHART_CVR_ActionType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, ACType, [COUNT], [CONVERT], NewACType, NewACAttrCat, NewACAttrDesc
		 from dbo.CHART_EBR_ActionType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, ACType, [COUNT], [CONVERT], NewACType, NewACAttrCat, NewACAttrDesc
		 from dbo.CHART_PCR_ActionType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, ACType, [COUNT], [CONVERT], NewACType, NewACAttrCat, NewACAttrDesc 
		 from dbo.CHART_SCAH_ActionType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, ACType, [COUNT], [CONVERT], NewACType, NewACAttrCat, NewACAttrDesc 
		 from dbo.CHART_SSR_ActionType where RE_DB_OwnerShort is not null
		 
--AddressType
		 select DB_TYPE, RE_DB_OwnerShort, AddrType, [COUNT], [CONVERT], NewAddressType, AddrSendMail, NewCADAttrCat, NewCADAttrDesc
		 into CHARTM.Test_AddressType
		 from CHART_CVR_AddressType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, AddrType, [COUNT], [CONVERT], NewAddressType, AddrSendMail, NewCADAttrCat, NewCADAttrDesc 
		 from CHART_EBR_AddressType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, AddrType, [COUNT], [CONVERT], NewAddressType, AddrSendMail, NewCADAttrCat, NewCADAttrDesc 
		 from CHART_PCR_AddressType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, AddrType, [COUNT], [CONVERT], NewAddressType, AddrSendMail, NewCADAttrCat, NewCADAttrDesc 
		 from CHART_SCAH_AddressType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, AddrType, [COUNT], [CONVERT], NewAddressType, AddrSendMail, NewCADAttrCat, NewCADAttrDesc 
		 from CHART_SSR_AddressType where RE_DB_OwnerShort is not null

--Attributes  --
		 select DB_TYPE, RE_DB_OwnerShort, [Type], [datatype], [category], tablename, [required], [unique], [active], [COUNT], [CONVERT], NewCATCat, NewCATDesc, NewCATDate, NewCAttrComm, AddSalType, AliasType, Alias, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes
		 into CHARTM.Test_Attributes
		 from CHART_CVR_Attributes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [Type], [datatype], [category], tablename, [required], [unique], [active], [COUNT], [CONVERT], NewCATCat, NewCATDesc, NewCATDate, NewCAttrComm, AddSalType, AliasType, Alias, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes
		 from CHART_EBR_Attributes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [Type], [datatype], [category], tablename, [required], [unique], [active], [COUNT], [CONVERT], NewCATCat, NewCATDesc, NewCATDate, NewCAttrComm, AddSalType, AliasType, Alias, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes
		 from CHART_PCR_Attributes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [Type], [datatype], [category], tablename, [required], [unique], [active], [COUNT], [CONVERT], NewCATCat, NewCATDesc, NewCATDate, NewCAttrComm, AddSalType, AliasType, Alias, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes
		 from CHART_SCAH_Attributes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [Type], [datatype], [category], tablename, [required], [unique], [active], [COUNT], [CONVERT], NewCATCat, NewCATDesc, NewCATDate, NewCAttrComm, AddSalType, AliasType, Alias, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes
		 from CHART_SSR_Attributes where RE_DB_OwnerShort is not null

--CampaignAppeal
		 select DB_TYPE, RE_DB_OwnerShort, [CampID], [CampDesc], [AppealID], AppDesc, [Count], [Sum], NewCampaignID, NewCampaignDesc, NewAppealID, NewAppealDesc, NewPackageID, NewPackageDesc, NewGftAttrCat1, NewGftAttrDesc1, NewGftAttrCat2, NewGftAttrDesc2, NewCampAttrCat1, NewCampAttrDesc1, NewCampAttrCat2, NewCampAttrDesc2, NewAppealAttrCat1, NewAppealAttrDesc1, NewAppealAttrCat2, NewAppealAttrDesc2, Notes 
		 into CHARTM.Test_CampaignAppeal
		 from CHART_CVR_CampaignAppeal where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [CampID], [CampDesc], [AppealID], AppDesc, [Count], [Sum], NewCampaignID, NewCampaignDesc, NewAppealID, NewAppealDesc, NewPackageID, NewPackageDesc, NewGftAttrCat1, NewGftAttrDesc1, NewGftAttrCat2, NewGftAttrDesc2, NewCampAttrCat1, NewCampAttrDesc1, NewCampAttrCat2, NewCampAttrDesc2, NewAppealAttrCat1, NewAppealAttrDesc1, NewAppealAttrCat2, NewAppealAttrDesc2, Notes 
		 from CHART_EBR_CampaignAppeal where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [CampID], [CampDesc], [AppealID], AppDesc, [Count], [Sum], NewCampaignID, NewCampaignDesc, NewAppealID, NewAppealDesc, NewPackageID, NewPackageDesc, NewGftAttrCat1, NewGftAttrDesc1, NewGftAttrCat2, NewGftAttrDesc2, NewCampAttrCat1, NewCampAttrDesc1, NewCampAttrCat2, NewCampAttrDesc2, NewAppealAttrCat1, NewAppealAttrDesc1, NewAppealAttrCat2, NewAppealAttrDesc2, Notes
		 from CHART_PCR_CampaignAppeal where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [CampID], [CampDesc], [AppealID], AppDesc, [Count], [Sum], NewCampaignID, NewCampaignDesc, NewAppealID, NewAppealDesc, NewPackageID, NewPackageDesc, NewGftAttrCat1, NewGftAttrDesc1, NewGftAttrCat2, NewGftAttrDesc2, NewCampAttrCat1, NewCampAttrDesc1, NewCampAttrCat2, NewCampAttrDesc2, NewAppealAttrCat1, NewAppealAttrDesc1, NewAppealAttrCat2, NewAppealAttrDesc2, Notes
		 from CHART_SCAH_CampaignAppeal where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [CampID], [CampDesc], [AppealID], AppDesc, [Count], [Sum], NewCampaignID, NewCampaignDesc, NewAppealID, NewAppealDesc, NewPackageID, NewPackageDesc, NewGftAttrCat1, NewGftAttrDesc1, NewGftAttrCat2, NewGftAttrDesc2, NewCampAttrCat1, NewCampAttrDesc1, NewCampAttrCat2, NewCampAttrDesc2, NewAppealAttrCat1, NewAppealAttrDesc1, NewAppealAttrCat2, NewAppealAttrDesc2, Notes
		 from CHART_SSR_CampaignAppeal where RE_DB_OwnerShort is not null

--ConsAttribute
		 select DB_TYPE, RE_DB_OwnerShort, [CAttrCat], [CAttrDesc], [DataType], [Count], [Convert], NewCATCat, NewCATDesc, cast([NewCATCom] as varchar(255)) as [NewCATCom], PPICode, NewCAttrCat1, NewCAttrDesc1, NewConsCode, ConsNoteType, ConsNoteNotes, AliasType, EventID, EventName, EventParticipationRecord, Registration, Invite, ParticipantHasAttended, Participation, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes

		 into CHARTM.Test_ConsAttributes
		 from CHART_SSR_ConsAttributes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [CAttrCat], [CAttrDesc], [DataType], [Count], [Convert], NewCATCat, NewCATDesc, cast([NewCATCom] as varchar(255)) as [NewCATCom], PPICode, NewCAttrCat1, NewCAttrDesc1, NewConsCode, ConsNoteType, ConsNoteNotes, AliasType, EventID, EventName, EventParticipationRecord, Registration, Invite, ParticipantHasAttended, Participation, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes 
		 from CHART_CVR_ConsAttributes where RE_DB_OwnerShort is not null  
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [CAttrCat], [CAttrDesc], [DataType], [Count], [Convert], NewCATCat, NewCATDesc, cast([NewCATCom] as varchar(255)) as [NewCATCom], PPICode, NewCAttrCat1, NewCAttrDesc1, NewConsCode, ConsNoteType, ConsNoteNotes, AliasType, EventID, EventName, EventParticipationRecord, Registration, Invite, ParticipantHasAttended, Participation, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes
		 from CHART_PCR_ConsAttributes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [CAttrCat], [CAttrDesc], [DataType], [Count], [Convert], NewCATCat, NewCATDesc, cast([NewCATCom] as varchar(255)) as [NewCATCom], PPICode, NewCAttrCat1, NewCAttrDesc1, NewConsCode, ConsNoteType, ConsNoteNotes, AliasType, EventID, EventName, EventParticipationRecord, Registration, Invite, ParticipantHasAttended, Participation, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes
		 from CHART_SCAH_ConsAttributes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [CAttrCat], [CAttrDesc], [DataType], [Count], [Convert], NewCATCat, NewCATDesc, cast([NewCATCom] as varchar(255)) as [NewCATCom], PPICode, NewCAttrCat1, NewCAttrDesc1, NewConsCode, ConsNoteType, ConsNoteNotes, AliasType, EventID, EventName, EventParticipationRecord, Registration, Invite, ParticipantHasAttended, Participation, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes
		 from CHART_EBR_ConsAttributes where RE_DB_OwnerShort is not null

--ConsSolicitCode
		 select DB_TYPE, RE_DB_OwnerShort, [SolicitCode], [Count], [Convert], CATCat, CATDesc, CATComment, NoValidAddr, Deceased, CATCat2, CATDesc2, CATComment2, Notes
		 into CHARTM.Test_ConsSolicitCodes
		 from CHART_CVR_ConsSolicitCodes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [SolicitCode], [Count], [Convert], CATCat, CATDesc, CATComment, NoValidAddr, Deceased, CATCat2, CATDesc2, CATComment2, Notes 
		 from CHART_EBR_ConsSolicitCodes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [SolicitCode], [Count], [Convert], CATCat, CATDesc, CATComment, NoValidAddr, Deceased, CATCat2, CATDesc2, CATComment2, Notes
		 from CHART_PCR_ConsSolicitCodes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [SolicitCode], [Count], [Convert], CATCat, CATDesc, CATComment, NoValidAddr, Deceased, CATCat2, CATDesc2, CATComment2, Notes
		 from CHART_SCAH_ConsSolicitCodes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [SolicitCode], [Count], [Convert], CATCat, CATDesc, CATComment, NoValidAddr, Deceased, CATCat2, CATDesc2, CATComment2, Notes
		 from CHART_SSR_ConsSolicitCodes where RE_DB_OwnerShort is not null

--ConstituencyCodes
		 select DB_TYPE, RE_DB_OwnerShort, [ConsCodeShort], [ConsCodeDesc], [RecordCount], [Convert], NewCons, NewCAttrCat, NewCAttrDesc, NewCAttrCom, NewSolCode, Deceased, NewCAttrCat1, NewCAttrDesc1, Notes
		 into CHARTM.Test_ConstituencyCodes
		 from CHART_CVR_ConstituencyCodes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [ConsCodeShort], [ConsCodeDesc],  [RecordCount], [Convert], NewCons, NewCAttrCat, NewCAttrDesc, NewCAttrCom, NewSolCode, Deceased, NewCAttrCat1, NewCAttrDesc1, Notes 
		 from CHART_EBR_ConstituencyCodes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [ConsCodeShort], [ConsCodeDesc],  [RecordCount], [Convert], NewCons, NewCAttrCat, NewCAttrDesc, NewCAttrCom, NewSolCode, Deceased, NewCAttrCat1, NewCAttrDesc1, Notes
		 from CHART_PCR_ConstituencyCodes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [ConsCodeShort], [ConsCodeDesc],  [RecordCount], [Convert], NewCons, NewCAttrCat, NewCAttrDesc, NewCAttrCom, NewSolCode, Deceased, NewCAttrCat1, NewCAttrDesc1, Notes
		 from CHART_SCAH_ConstituencyCodes where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [ConsCodeShort], [ConsCodeDesc],  [RecordCount], [Convert], NewCons, NewCAttrCat, NewCAttrDesc, NewCAttrCom, NewSolCode, Deceased, NewCAttrCat1, NewCAttrDesc1, Notes
		 from CHART_SSR_ConstituencyCodes where RE_DB_OwnerShort is not null
 
 --EventType
 		 select DB_TYPE, RE_DB_OwnerShort, [EVType], [Count], [Convert], NewEventType, Notes
		 into CHARTM.Test_EventType
		 from CHART_CVR_EventType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [EVType], [Count], [Convert], NewEventType, Notes 
		 from CHART_EBR_EventType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [EVType], [Count], [Convert], NewEventType, Notes
		 from CHART_PCR_EventType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [EVType], [Count], [Convert], NewEventType, Notes
		 from CHART_SCAH_EventType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [EVType], [Count], [Convert], NewEventType, Notes
		 from CHART_SSR_EventType where RE_DB_OwnerShort is not null
		 
--Fund
		 select DB_TYPE, RE_DB_OwnerShort, [FundID], FundDesc, FundType, FundCategory, FundIsRestricted, [Count], NewFundID, NewFundDesc, NewFundType, NewFundCategory, NewFundIsRestricted, NewFundAttrCat1, NewFundAttrDesc1, NewFundAttrCat2, NewFundAttrDesc2, NewFundAttrCat3, NewFundAttrDesc3, NewFundAttrCat4, NewFundAttrDesc4
 		 into CHARTM.Test_Fund
		 from CHART_CVR_Fund where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [FundID], FundDesc, FundType, FundCategory, FundIsRestricted, [Count], NewFundID, NewFundDesc, NewFundType, NewFundCategory, NewFundIsRestricted, NewFundAttrCat1, NewFundAttrDesc1, NewFundAttrCat2, NewFundAttrDesc2, NewFundAttrCat3, NewFundAttrDesc3, NewFundAttrCat4, NewFundAttrDesc4 
		 from CHART_EBR_Fund where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [FundID], FundDesc, FundType, FundCategory, FundIsRestricted, [Count], NewFundID, NewFundDesc, NewFundType, NewFundCategory, NewFundIsRestricted, NewFundAttrCat1, NewFundAttrDesc1, NewFundAttrCat2, NewFundAttrDesc2, NewFundAttrCat3, NewFundAttrDesc3, NewFundAttrCat4, NewFundAttrDesc4 
		 from CHART_PCR_Fund where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [FundID], FundDesc, FundType, FundCategory, FundIsRestricted, [Count], NewFundID, NewFundDesc, NewFundType, NewFundCategory, NewFundIsRestricted, NewFundAttrCat1, NewFundAttrDesc1, NewFundAttrCat2, NewFundAttrDesc2, NewFundAttrCat3, NewFundAttrDesc3, NewFundAttrCat4, NewFundAttrDesc4 
		 from CHART_SCAH_Fund where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [FundID], FundDesc, FundType, FundCategory, FundIsRestricted, [Count], NewFundID, NewFundDesc, NewFundType, NewFundCategory, NewFundIsRestricted, NewFundAttrCat1, NewFundAttrDesc1, NewFundAttrCat2, NewFundAttrDesc2, NewFundAttrCat3, NewFundAttrDesc3, NewFundAttrCat4, NewFundAttrDesc4 
		 from CHART_SSR_Fund where RE_DB_OwnerShort is not null
		 
--GiftAttribute
		 select DB_TYPE, RE_DB_OwnerShort, [GFAttrCat], [GFAttrDesc], [DataType], [Count], [Convert], NewGATCategory, NewGATDesc, NewGATCom, NewGiftSubType
		 into CHARTM.Test_GiftAttribute
		 from CHART_CVR_GiftAttribute where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [GFAttrCat], [GFAttrDesc], [DataType], [Count], [Convert], NewGATCategory, NewGATDesc, NewGATCom, NewGiftSubType 
		 from CHART_EBR_GiftAttribute where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [GFAttrCat], [GFAttrDesc], [DataType], [Count], [Convert], NewGATCategory, NewGATDesc, NewGATCom, NewGiftSubType
		 from CHART_PCR_GiftAttribute where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [GFAttrCat], [GFAttrDesc], [DataType], [Count], [Convert], NewGATCategory, NewGATDesc, NewGATCom, NewGiftSubType
		 from CHART_SCAH_GiftAttribute where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [GFAttrCat], [GFAttrDesc], [DataType], [Count], [Convert], NewGATCategory, NewGATDesc, NewGATCom, NewGiftSubType
		 from CHART_SSR_GiftAttribute where RE_DB_OwnerShort is not null

--GiftCode
		 select DB_TYPE, RE_DB_OwnerShort, [GFGiftCode], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc
		 into CHARTM.Test_GiftCode
		 from CHART_CVR_GiftCode where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [GFGiftCode], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc 
		 from CHART_EBR_GiftCode where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [GFGiftCode], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc 
		 from CHART_PCR_GiftCode where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [GFGiftCode], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc 
		 from CHART_SCAH_GiftCode where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [GFGiftCode], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc 
		 from CHART_SSR_GiftCode where RE_DB_OwnerShort is not null

--GiftSubType
		 select DB_TYPE, RE_DB_OwnerShort, [GFSubType], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc 
		 into CHARTM.Test_GiftSubType
		 from CHART_CVR_GiftSubType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [GFSubType], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc  
		 from CHART_EBR_GiftSubType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [GFSubType], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc  
		 from CHART_PCR_GiftSubType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [GFSubType], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc  
		 from CHART_SCAH_GiftSubType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [GFSubType], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc  
		 from CHART_SSR_GiftSubType where RE_DB_OwnerShort is not null

--ParticipantType
		 select DB_TYPE, RE_DB_OwnerShort, [REGParticipation], [Count], [Convert], NewREGParticipation, REGAttended, REGAttrCat, REGAttrDesc, Notes
		 into CHARTM.Test_ParticipantType
		 from CHART_CVR_ParticipantType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [REGParticipation], [Count], [Convert], NewREGParticipation, REGAttended, REGAttrCat, REGAttrDesc, Notes 
		 from CHART_EBR_ParticipantType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [REGParticipation], [Count], [Convert], NewREGParticipation, REGAttended, REGAttrCat, REGAttrDesc, Notes 
		 from CHART_PCR_ParticipantType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [REGParticipation], [Count], [Convert], NewREGParticipation, REGAttended, REGAttrCat, REGAttrDesc, Notes 
		 from CHART_SCAH_ParticipantType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [REGParticipation], [Count], [Convert], NewREGParticipation, REGAttended, REGAttrCat, REGAttrDesc, Notes 
		 from CHART_SSR_ParticipantType where RE_DB_OwnerShort is not null


--PhoneType
		 select DB_TYPE, RE_DB_OwnerShort, [PhoneType], [Count], [Convert], NewPhoneType 
		 into CHARTM.Test_PhoneType
		 from CHART_CVR_PhoneType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [PhoneType], [Count], [Convert], NewPhoneType 
		 from CHART_EBR_PhoneType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [PhoneType], [Count], [Convert], NewPhoneType 
		 from CHART_PCR_PhoneType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [PhoneType], [Count], '' [Convert], NewPhoneType 
		 from CHART_SCAH_PhoneType where RE_DB_OwnerShort is not null
		 UNION ALL
		 select DB_TYPE, RE_DB_OwnerShort, [PhoneType], [Count], '' [Convert], NewPhoneType 
		 from CHART_SSR_PhoneType where RE_DB_OwnerShort is not null
		 
		 

			
--5. UPDATE NULLs.
		-- ActionStatus
			SELECT * FROM CHARTM.Test_ActionStatus where ACStatus is null 
			-- UPDATE CHARTM.Test_ActionStatus set ACSTatus ='NULL' where ACStatus is null 
		-- ActionType
			SELECT * FROM CHARTM.Test_ActionType where ACType is null 
			--UPDATE CHARTM.Test_ActionType set ACType ='NULL' where ACType is null 
		-- AddressType
			SELECT * FROM CHARTM.Test_AddressType where AddrType is null 
			--UPDATE CHARTM.Test_AddressType set AddrType ='NULL' where AddrType is null 
		-- Attributes
			--created from --CHART_Attributes.sql --  SELECT * FROM RE_SUTTER_CHARTS.CHARTM.Attributes 
			
		--CampaignAppeal
			SELECT * FROM CHARTM.Test_CampaignAppeal where CampID is null 
			--UPDATE CHARTM.Test_CampaignAppeal set CampID ='NULL' where CampID is null 
			SELECT * FROM CHARTM.Test_CampaignAppeal where AppealID is null 
			--UPDATE CHARTM.Test_CampaignAppeal set AppealID ='NULL' where AppealID is null 
		
		--ConsAttribute
			SELECT * FROM CHARTM.Test_ConsAttributes where CAttrCat is null 
			--UPDATE CHARTM.Test_ConsAttributes set CAttrCat ='NULL' where CAttrCat is null 
			SELECT * FROM CHARTM.Test_ConsAttributes where CAttrDesc is null 
			--UPDATE CHARTM.Test_ConsAttributes set CAttrDesc ='NULL' where CAttrDesc is null 
			
		--ConsSolicitCodes
			SELECT * FROM CHARTM.Test_ConsSolicitCodes where SolicitCode is null 
			--UPDATE CHARTM.Test_ConsSolicitCodes set SolicitCode ='NULL' where SolicitCode is null
		
		--ConstituencyCode
			SELECT * FROM CHARTM.Test_ConstituencyCodes where ConsCodeShort is null 
			--UPDATE CHARTM.Test_ConstituencyCodes set ConsCodeShort ='NULL' where ConsCodeShort is null
			SELECT * FROM CHARTM.Test_ConstituencyCodes where ConsCodeDesc is null 
			--UPDATE CHARTM.Test_ConstituencyCodes set ConsCodeDesc ='NULL' where ConsCodeDesc is null
		
		--EventType
			SELECT * FROM CHARTM.Test_EventType where EVType is null 
			--UPDATE CHARTM.Test_EventType set EVType ='NULL' where EVType is null
		
		--Fund
			SELECT * FROM CHARTM.Test_Fund where FundId is null 
			--UPDATE CHARTM.Test_Fund set FundId ='NULL' where FundId is null
			
		--GiftAttribute
			SELECT * FROM CHARTM.Test_GiftAttribute where GFAttrCat is null 
			--UPDATE CHARTM.Test_GiftAttribute set GFAttrCat ='NULL' where GFAttrCat is null 
			SELECT * FROM CHARTM.Test_GiftAttribute where GFAttrDesc is null 
			--UPDATE CHARTM.Test_GiftAttribute set GFAttrDesc ='NULL' where GFAttrDesc is null 
		
		--GiftCode
			SELECT * FROM CHARTM.Test_GiftCode where GFGiftCode is null 
			--UPDATE CHARTM.Test_GiftCode set GFGiftCode ='NULL' where GFGiftCode is null
	
		--GiftSubType
			SELECT * FROM CHARTM.Test_GiftSubType where GFSubType is null 
			--UPDATE CHARTM.Test_GiftSubType set GFSubType ='NULL' where GFSubType is null

		--ParticipantType
			SELECT * FROM CHARTM.Test_ParticipantType where REGParticipation is null 
			--UPDATE CHARTM.Test_ParticipantType set REGParticipation ='NULL' where REGParticipation is null
		--PhoneType
			SELECT * FROM CHARTM.Test_PhoneType where PhoneType is null 
			--UPDATE CHARTM.Test_PhoneType set PhoneType ='NULL' where PhoneType is null
					
