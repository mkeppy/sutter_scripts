

--1./ NEW DATA/ The RE data has been merged into tables. The regular 'chart' views has been installed using those tables. 
	-- Now new tables are created from those views in order to add the "Source" field and to update any blank/null values to "NULL" 
	--2.create schema.
	--3.remove 
	--4.create.
	--5.clean NULLs.

USE RE_SUTTER_CHARTS
GO
	
--2./  
  --CREATE SCHEMA CHART
  --GO

--3./ remote to re-create charts 

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ActionStatus]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[ActionStatus]
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ActionType]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[ActionType]
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[AddressType]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[AddressType]
--if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Attributes]') and objectproperty(id, N'isTable') = 1)
--	drop table  [CHART].[Attributes]
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[CampaignAppealPackage]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[CampaignAppealPackage]

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConsAttributes]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[ConsAttributes]
--if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConsAttributes_nonTable]') and objectproperty(id, N'isTable') = 1)
--	drop table  [CHART].[ConsAttributes_nonTable]
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConsSolicitCodes]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[ConsSolicitCodes]
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConstituencyCodes]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[ConstituencyCodes]
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[EventType]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[EventType]
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Fund]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[Fund]
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftAttribute]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[GiftAttribute]
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftCode]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[GiftCode]
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftSubType]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[GiftSubType]
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ParticipantType]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[ParticipantType]
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[PhoneType]') and objectproperty(id, N'isTable') = 1)
	drop table  [CHART].[PhoneType]
 
--4./ create charts 

 		-- ActionStatus
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.ActionStatus FROM RE_SUTTER_MERGE.CHART.ActionStatus  
			GO
		-- ActionType
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.ActionType FROM RE_SUTTER_MERGE.CHART.ActionType 
			GO
		-- AddressType
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.AddressType FROM RE_SUTTER_MERGE.CHART.AddressType 
			GO
		-- Attributes
			--created from --CHART_Attributes.sql --  SELECT * FROM RE_SUTTER_CHARTS.CHART.Attributes 
			
		--CampaignAppeal
		--	SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.CampaignAppeal FROM RE_SUTTER_MERGE.CHART.CampaignAppeal
		--	GO

		--CampaignAppealPackage
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.CampaignAppealPackage FROM RE_SUTTER_MERGE.CHART.CampaignAppealPackage
			GO


		--ConsAttribute
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.ConsAttributes FROM RE_SUTTER_MERGE.CHART.ConsAttribute WHERE DataType ='TableEntry'
			GO 
		--ConsSolicitCodes
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.ConsSolicitCode FROM RE_SUTTER_MERGE.CHART.SolicitCode
			GO 
		--ConstituencyCode
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.ConstituencyCode FROM RE_SUTTER_MERGE.CHART.ConstituencyCodes 
			GO 
		--EventType
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.EventType FROM RE_SUTTER_MERGE.CHART.EventType
			GO 
		--Fund
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.Fund FROM RE_SUTTER_MERGE.CHART.Fund
			GO 
		 --GiftAttribute
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.GiftAttribute FROM RE_SUTTER_MERGE.CHART.GiftAttribute WHERE DataType ='TableEntry'
			GO 
		 --GiftCode
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.GiftCode FROM RE_SUTTER_MERGE.CHART.GiftCode 
			GO 
		--GiftSubType
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.GiftSubType FROM RE_SUTTER_MERGE.CHART.GiftSubType
			GO 
		--ParticipantType
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.ParticipantType FROM RE_SUTTER_MERGE.CHART.ParticipantType
			GO 
		--PhoneType
			SELECT 'Source' AS [DB_TYPE], * into RE_SUTTER_CHARTS.CHART.PhoneType FROM RE_SUTTER_MERGE.CHART.PhoneType 
			GO 
			
--5. UPDATE NULLs.
		-- ActionStatus
			SELECT * FROM CHART.ActionStatus where ACStatus is null 
			--UPDATE CHART.ActionStatus set ACSTatus ='NULL' where ACStatus is null 
		-- ActionType
			SELECT * FROM CHART.ActionType where ACType is null 
			--UPDATE CHART.ActionType set ACType ='NULL' where ACType is null 
		-- AddressType
			SELECT * FROM CHART.AddressType where AddrType is null 
			--UPDATE CHART.AddressType set AddrType ='NULL' where AddrType is null 
		-- Attributes
			--created from --CHART_Attributes.sql --  SELECT * FROM RE_SUTTER_CHARTS.CHART.Attributes 
			
		--CampaignAppealPackage
		    SELECT * FROM CHART.CampaignAppealPackage where CampID is null 
			UPDATE CHART.CampaignAppealPackage set CampID ='NULL' where CampID is null 
			SELECT * FROM CHART.CampaignAppealPackage where AppealID is null 
			UPDATE CHART.CampaignAppealPackage set AppealID ='NULL' where AppealID is null 
			SELECT * FROM CHART.CampaignAppealPackage where PackageID is null 
			UPDATE CHART.CampaignAppealPackage set PackageID ='NULL' where PackageID is null
		
		--ConsAttribute
			SELECT * FROM CHART.ConsAttributes where CAttrCat is null 
			--UPDATE CHART.ConsAttributes set CAttrCat ='NULL' where CAttrCat is null 
			SELECT * FROM CHART.ConsAttributes where CAttrDesc is null 
			--UPDATE CHART.ConsAttributes set CAttrDesc ='NULL' where CAttrDesc is null 
			
		--ConsSolicitCodes
			SELECT * FROM CHART.ConsSolicitCode where SolicitCode is null 
			--UPDATE CHART.ConsSolicitCode set SolicitCode ='NULL' where SolicitCode is null
		
		--ConstituencyCode
			SELECT * FROM CHART.ConstituencyCode where ConsCodeShort is null 
			--UPDATE CHART.ConstituencyCode set ConsCodeShort ='NULL' where ConsCodeShort is null
			SELECT * FROM CHART.ConstituencyCode where ConsCodeDesc is null 
			--UPDATE CHART.ConstituencyCode set ConsCodeDesc ='NULL' where ConsCodeDesc is null
		
		--EventType
			SELECT * FROM CHART.EventType where EVType is null 
			--UPDATE CHART.EventType set EVType ='NULL' where EVType is null
		
		--Fund
			SELECT * FROM CHART.Fund where FundId is null 
			--UPDATE CHART.Fund set FundId ='NULL' where FundId is null
			
		--GiftAttribute
			SELECT * FROM CHART.GiftAttribute where GFAttrCat is null 
			--UPDATE CHART.GiftAttribute set GFAttrCat ='NULL' where GFAttrCat is null 
			SELECT * FROM CHART.GiftAttribute where GFAttrDesc is null 
			--UPDATE CHART.GiftAttribute set GFAttrDesc ='NULL' where GFAttrDesc is null 
		
		--GiftCode
			SELECT * FROM CHART.GiftCode where GFGiftCode is null 
			--UPDATE CHART.GiftCode set GFGiftCode ='NULL' where GFGiftCode is null
	
		--GiftSubType
			SELECT * FROM CHART.GiftSubType where GFSubType is null 
			--UPDATE CHART.GiftSubType set GFSubType ='NULL' where GFSubType is null

		--ParticipantType
			SELECT * FROM CHART.ParticipantType where REGParticipation is null 
			--UPDATE CHART.ParticipantType set REGParticipation ='NULL' where REGParticipation is null
		--PhoneType
			SELECT * FROM CHART.PhoneType where PhoneType is null 
			--UPDATE CHART.PhoneType set PhoneType ='NULL' where PhoneType is null
					


			  
/*


-- CONS ATTRIBUTES
 
 	SET QUOTED_IDENTIFIER ON 
	GO
	SET ANSI_NULLS ON 
	GO
		CREATE VIEW CHART.ConsAttributes_nonTable AS
			SELECT 'Source' AS [DB_TYPE], * 
			FROM RE_SUTTER_MERGE.CHART.ConsAttribute WHERE DataType !='TableEntry'
			UNION ALL 
			SELECT 'Target' AS [DB_TYPE], * 
			FROM RE7_CPMC.CHART.ConsAttribute WHERE DataType !='TableEntry'
	GO 
	SET QUOTED_IDENTIFIER OFF 
	GO
	SET ANSI_NULLS OFF
	GO
*/