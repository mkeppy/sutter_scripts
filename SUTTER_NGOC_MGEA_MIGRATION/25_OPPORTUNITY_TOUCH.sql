
USE SUTTER_MGEA_DATA
GO


BEGIN
		DROP TABLE SUTTER_MGEA_MIGRATION.IMP.UPDATE_OPPORTUNITY_PARENT_Open
		DROP TABLE SUTTER_MGEA_MIGRATION.IMP.UPDATE_OPPORTUNITY_PARENT_Completed
END

 

BEGIN--OPEN TRANSACTION GENERATION (AKA RECORD TOUCH)


				--standard api option OPEN Parent
				SELECT	X1.ID, '' as rC_Giving__Rollup_Transactions__c, '' as rC_Giving__Update_Transactions__c, 'TRUE' RTP_Updated__c,
						X1.ACCOUNTID AS zrefAcctId, T.StageName AS zrefStageName, X1.EXTERNAL_ID__C zrefPledgeId,
						X1.RE_GSPLITIMPID__C AS zrefGFSplitImpID, X1.RE_GFIMPID__C AS zrefGFImpID
						
				INTO SUTTER_MGEA_MIGRATION.IMP.UPDATE_OPPORTUNITY_PARENT_Open
				FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT T
				INNER JOIN SUTTER_MGEA_MIGRATION.[XTR].[OPPORTUNITY] AS X1 ON T.External_Id__c=X1.EXTERNAL_ID__C
				WHERE T.StageName='Open'
				ORDER BY T.StageName, T.[ACCOUNT:External_Id__c], T.CloseDate, T.[CAMPAIGN:External_Id__c]
				

				--standard api option COMPELTE Parent
				SELECT	X1.ID, '' as rC_Giving__Rollup_Transactions__c, '' as rC_Giving__Update_Transactions__c, 'TRUE' RTP_Updated__c,
						X1.ACCOUNTID AS zrefAcctId, T.StageName AS zrefStageName, X1.EXTERNAL_ID__C zrefPledgeId,
						X1.RE_GSPLITIMPID__C AS zrefGFSplitImpID, X1.RE_GFIMPID__C AS zrefGFImpID
						
				INTO SUTTER_MGEA_MIGRATION.IMP.UPDATE_OPPORTUNITY_PARENT_Completed
				FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT T
				INNER JOIN SUTTER_MGEA_MIGRATION.[XTR].[OPPORTUNITY] AS X1 ON T.External_Id__c=X1.EXTERNAL_ID__C
				WHERE T.StageName='Completed'
				ORDER BY T.StageName, T.[ACCOUNT:External_Id__c], T.CloseDate, T.[CAMPAIGN:External_Id__c]
 
END;



--EXCEPTIONS from record touch not working.

				DROP TABLE SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT_UPDATE_amts

			--UPDATE AMOUNTS	
				SELECT	X1.ID, rC_Giving__Giving_Amount__c, [rC_Giving__Expected_Giving_Amount__c] ,[rC_Giving__Current_Giving_Amount__c], [rC_Giving__Giving_Years__c]
				,'Open' AS StageName
				,zrefGFType, T.RE_GFImpID__c zrefGFImpID, T.External_Id__c zrefExtId
			--	INTO SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT_UPDATE_amts  
				FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT_NEW T
				INNER JOIN SUTTER_MGEA_MIGRATION.[XTR].[OPPORTUNITY] AS X1 ON T.External_Id__c=X1.EXTERNAL_ID__C
				WHERE T.StageName='Open' 
				--AND T.External_Id__c='SAFH-GT-04121-553-0000139284' OR t.external_id__c='SAFH-GT-04121-553-0000138454'
				ORDER BY zrefgftype
				

		--standard api option OPEN Parent
				SELECT	X1.ID, '' as rC_Giving__Rollup_Transactions__c, '' as rC_Giving__Update_Transactions__c, 'TRUE' RTP_Updated__c,
						X1.ACCOUNTID AS zrefAcctId, T.StageName AS zrefStageName, X1.EXTERNAL_ID__C zrefPledgeId, T.zrefGFType,
						X1.RE_GSPLITIMPID__C AS zrefGFSplitImpID, X1.RE_GFIMPID__C AS zrefGFImpID
						
				--INTO SUTTER_MGEA_MIGRATION.IMP.UPDATE_OPPORTUNITY_PARENT_Open_new
				FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT_NEW T
				INNER JOIN SUTTER_MGEA_MIGRATION.[XTR].[OPPORTUNITY] AS X1 ON T.External_Id__c=X1.EXTERNAL_ID__C
				WHERE T.StageName='Open'
				ORDER BY T.zrefgftype, T.StageName, T.[ACCOUNT:External_Id__c], T.CloseDate, T.[CAMPAIGN:External_Id__c]
				

	  