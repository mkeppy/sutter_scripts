
USE SUTTER_MGEA_DATA
GO

BEGIN
	DROP TABLE SUTTER_MGEA_MIGRATION.IMP.GAU
END

BEGIN

				SELECT
					dbo.fnc_OwnerID() AS OwnerID
					,T.[GAU External ID] AS rC_Giving__External_ID__c
					,T.GAU_Name AS Name
					,T.GAU_Acknowledgement_Text__c AS Acknowledgement_Text__c
					,T.GAU_IsRestricted AS IsRestricted__c
					,T.GAU_Region__c AS Region__c
					,T.GAU_Affiliation AS Affiliation__c
					,T.GAU_Restriction_Type__c AS Restriction_Type__c
					,CAST(T1.FundNote AS VARCHAR(MAX)) AS rC_Giving__Description__c
					,T1.FundStartDate AS Start_Date__c
					,T1.FundEndDate AS End_Date__c
					
					,T1.FundID AS zrefFundId
					,T1.FundDesc AS zrefFundDesc
					,T1.RE_DB_OwnerShort AS zrefREOwner
					,LEN(T.GAU_Name) AS zrefNAMELen
				INTO SUTTER_MGEA_MIGRATION.IMP.GAU
				FROM SUTTER_MGEA_MIGRATION.dbo.CHART_Fund AS T
				INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Fund_v T1 ON T.FundID=T1.FundID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				ORDER BY T.[GAU External ID] 
END;


BEGIN--check for duplicates
	
				--external id
				SELECT * 
				FROM SUTTER_MGEA_MIGRATION.IMP.GAU
				WHERE rC_Giving__External_ID__c IN (SELECT rC_Giving__External_ID__c FROM SUTTER_MGEA_MIGRATION.IMP.GAU
													GROUP BY rC_Giving__External_ID__c HAVING COUNT(*)>1)
				ORDER BY rC_Giving__External_ID__c

				--name
				SELECT * 
				FROM SUTTER_MGEA_MIGRATION.IMP.GAU
				WHERE Name IN (SELECT Name FROM SUTTER_MGEA_MIGRATION.IMP.GAU
							   GROUP BY Name HAVING COUNT(*)>1)
				ORDER BY Name
			
			
END; 
 
 
BEGIN

	--	SELECT [rC_Giving__Description__c] FROM [SUTTER_MGEA_MIGRATION].[IMP].[GAU] WHERE [rC_Giving__Description__c] LIKE '%"%';        
		UPDATE [SUTTER_MGEA_MIGRATION].[IMP].GAU SET rC_Giving__Description__c=REPLACE(rC_Giving__Description__c,'"','''') where rC_Giving__Description__c like '%"%'
		UPDATE [SUTTER_MGEA_MIGRATION].[IMP].GAU SET Name=REPLACE(Name,'"','''') where Name like '%"%'
		UPDATE [SUTTER_MGEA_MIGRATION].[IMP].GAU SET Acknowledgement_Text__c=REPLACE(Acknowledgement_Text__c,'"','''') where Acknowledgement_Text__c like '%"%'
		UPDATE [SUTTER_MGEA_MIGRATION].[IMP].GAU SET zrefFundDesc=REPLACE(zrefFundDesc,'"','''') where zrefFundDesc like '%"%'
END