USE SUTTER_MGEA_DATA
GO

BEGIN--INACTIVE ACCOUNTS
 
	DROP TABLE [SUTTER_MGEA_MIGRATION].[TBL].[Account_Inactive] 

	 SELECT [External_ID__c], rC_Bios__Deceased__c, COUNT(*) C
	 INTO [SUTTER_MGEA_MIGRATION].[TBL].[Account_Inactive] 
	 FROM [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT]
	 GROUP BY [External_ID__c], rC_Bios__Deceased__c
	 ORDER BY [External_ID__c], rC_Bios__Deceased__c
END

BEGIN--ACCOUNT UPDATE: INACTIVE and PARENT ACCOUNT
		--INACTIVE ACCOUNTS
		
		DROP TABLE [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT_UPDATE]
		
		SELECT [External_ID__c]
		,'FALSE' AS rC_Bios__Active__c
		,NULL AS [Parent:External_ID__c]
		,rC_Bios__Deceased__c AS zref_rC_Bios__Deceased__c
		,C AS zrefCount
		INTO [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT_UPDATE] 
		FROM [SUTTER_MGEA_MIGRATION].[TBL].[Account_Inactive] 
		WHERE [External_ID__c] NOT IN (SELECT [External_ID__c] FROM [SUTTER_MGEA_MIGRATION].[TBL].[Account_Inactive] GROUP BY [External_ID__c] HAVING COUNT(*)>1)
		AND rC_Bios__Deceased__c='TRUE'
	 
	  UNION ALL
		--PARENT ACCOUNT
		SELECT 
		(T.RE_DB_OwnerShort+'-'+T.ImportID) AS [External_ID__c]
		,NULL AS rC_Bios__Active__c
		,(T.RE_DB_OwnerShort+'-'+T.ParentCorpLink) AS [Parent:External_ID__c]
		,NULL AS zref_rC_Bios__Deceased__c
		,NULL AS zrefCount
		FROM SUTTER_MGEA_DATA.dbo.HC_Constituents_v T
		WHERE T.ParentCorpLink IS NOT NULL
		AND (T.RE_DB_OwnerShort+'-'+T.ImportID)!=(T.RE_DB_OwnerShort+'-'+T.ParentCorpLink)
	 
END 
 
BEGIN--CHECK DUPLICATE
         SELECT *
         FROM   [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT_update]
         WHERE  [External_ID__c] IN (
                SELECT  [External_ID__c]
                FROM    [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT_update]
                GROUP BY [External_ID__c]
                HAVING  COUNT(*) > 1 );
END