 
	
USE SUTTER_MGEA_MIGRATION
	GO

BEGIN 
	DROP TABLE SUTTER_MGEA_MIGRATION.IMP.ACCOUNT
END

 
  

BEGIN--ACCOUNTS
	BEGIN
			--ACCOUNTS: CONSTITUENTS ORG_ACCOUNTS
			SELECT
			 dbo.fnc_OwnerID() AS OwnerID
			,dbo.fnc_RecordType('Account_Organizational') AS RecordTypeID
 		 	,CASE WHEN T.ConsID IS NULL OR T.ConsID ='' THEN '' ELSE (T.RE_DB_OwnerShort+'-'+T.ConsID) END AS	RE_ConsID__c
			,(T.RE_DB_OwnerShort+'-'+T.ImportID) AS External_ID__c
			,(T.RE_DB_OwnerShort+'-'+T.ImportID) AS RE_ImportID__c			
			,NULL AS RE_ORImpID__c
		
			-------------------,NULL AS MDM_ID__c


			,T.Name	AS	Name
			,T.GivesAnon	AS	Gives_Anonymously__c
			,T.NoValidAddr	AS	No_Valid_Address__c
			,T.IsInactive	AS	rC_Bios__Active__c
			,T.OrgMatch	AS	Employee_Matching__c
			,T.PrimAddText	AS	rC_Bios__Salutation__c
			,T.NoEmail	AS	RE_HasOptedOutOfEmail__c
			,T.DateAdded	AS	CreatedDate
			,T.RE_DB_OwnerShort	AS	rC_Giving__Primary_Affiliation__c
			,'TRUE'	AS	rC_Giving__Track_Affiliations__c
			,'TRUE' AS	rC_Giving__Track_Hard_Credits__c
			,'TRUE' AS	rC_Giving__Track_Soft_Credits__c
			,'TRUE' AS	rC_Giving__Track_Summaries__c
			,GETDATE()  AS	rC_Giving__Rollup_Affiliations__c
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
			,T3.Annotation AS rC_Bios__Warning_Comments__c
			,T4.Do_Not_Phone__c
			,T4.Do_Not_Solicit__c
			,T4.Do_Not_Mail__c
			,T4.Do_Not_Contact__c
			,T6.Phone
			,T6.RE_Email__c
			,T6.Fax
			,T6.RE_Home__c
			,T6.RE_Pager__c
			,T6.Website
			,CAST(T7.Additional_Phone_Numbers__c AS NVARCHAR(4000)) AS Additional_Phone_Numbers__c

			,CASE WHEN T8.AddrLine1 IS NULL THEN NULL WHEN T8.AddrLine2!='' THEN T8.AddrLine1 +' '+T8.AddrLine2 ELSE T8.AddrLine1 END AS BillingStreet
			,T8.AddrCity AS BillingCity
			,T8.AddrState AS BillingState
			,T8.AddrZIP AS BillingPostalCode
			,T8.AddrCountry BillingCountry
			
			--reference only
			,'Constituent Org' AS zrefSource
			,T.KeyInd zrefKeyInd
			,T.ConsID zrefConsID
			,T.ImportID zrefImportID
			,NULL AS zrefORImpID
			,NULL AS zrefESRImpID
			,T.RE_DB_OwnerShort zrefDBOwner
			,LEN(T7.Additional_Phone_Numbers__c) AS zrefAddphonelen
			
		    INTO SUTTER_MGEA_MIGRATION.IMP.ACCOUNT
			FROM SUTTER_MGEA_DATA.dbo.HC_Constituents_v T	
			INNER JOIN SUTTER_MGEA_MIGRATION.tbl.Account_Org T2 ON T.ImportID=T2.ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_DATA.DBO.HC_Cons_Annotation_v T3 ON T.ImportID=T3.ImportID AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort

			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.SolicitCodes T4 ON T.ImportID=T4.ImportID AND T.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Phones_seq_1_final T6 ON T.ImportID=T6.ImportID AND T.RE_DB_OwnerShort=T6.RE_DB_OwnerShort AND T.KeyInd=T6.KeyInd
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Phones_seq_2_final T7 ON T.ImportID=T7.ImportID AND T.RE_DB_OwnerShort=T7.RE_DB_OwnerShort AND T.KeyInd=T7.KeyInd
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Address_Pref	T8 ON T.RE_DB_OwnerShort=T8.RE_DB_OwnerShort AND T.ImportID=T8.ImportID
		
		UNION ALL
		
			--ACCOUNTS: CONSTITUENTS IND HEAD OF HOUSEHOLD
			SELECT
			 dbo.fnc_OwnerID() AS OwnerID
			,dbo.fnc_RecordType('Account_Household') AS RecordTypeID
 		 	,CASE WHEN T.ConsID IS NULL OR T.ConsID ='' THEN '' ELSE (T.RE_DB_OwnerShort+CHAR(10)+T.ConsID) END AS	RE_ConsID__c
			,(T.RE_DB_OwnerShort+'-'+T.ImportID) AS External_ID__c
			,(T.RE_DB_OwnerShort+'-'+T.ImportID) AS RE_ImportID__c
			,NULL AS RE_ORImpID__c
		
			-------------------,NULL AS MDM_ID__c


			,T.Name	AS	Name
			,T.GivesAnon	AS	Gives_Anonymously__c
			,T.NoValidAddr	AS	No_Valid_Address__c
			,CASE WHEN T2.Deceased='true' AND T2.NonHHDeceased='true' THEN 'FALSE'  
				WHEN T2.Deceased='true' AND T2.NonHHDeceased=''  THEN 'FALSE'
				ELSE 'TRUE' END AS rC_Bios__Active__c
			,T.OrgMatch	AS	Employee_Matching__c
			,T.PrimAddText	AS	rC_Bios__Salutation__c
			,T.NoEmail	AS	RE_HasOptedOutOfEmail__c
			,T.DateAdded	AS	CreatedDate
			,T.RE_DB_OwnerShort	AS	rC_Giving__Primary_Affiliation__c
			,'TRUE'	AS	rC_Giving__Track_Affiliations__c
			,'TRUE' AS	rC_Giving__Track_Hard_Credits__c
			,'TRUE' AS	rC_Giving__Track_Soft_Credits__c
			,'TRUE' AS	rC_Giving__Track_Summaries__c
			,GETDATE()  AS	rC_Giving__Rollup_Affiliations__c
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
			,T3.Annotation AS rC_Bios__Warning_Comments__c
			,T4.Do_Not_Phone__c
			,T4.Do_Not_Solicit__c
			,T4.Do_Not_Mail__c
			,T4.Do_Not_Contact__c
			,T6.Phone
			,T6.RE_Email__c
			,T6.Fax
			,T6.RE_Home__c
			,T6.RE_Pager__c
			,T6.Website
			,CAST(T7.Additional_Phone_Numbers__c AS NVARCHAR(4000)) AS Additional_Phone_Numbers__c
			,CASE WHEN T8.AddrLine1 IS NULL THEN NULL WHEN T8.AddrLine2!='' THEN T8.AddrLine1 +' '+T8.AddrLine2 ELSE T8.AddrLine1 END AS BillingStreet
			,T8.AddrCity AS BillingCity
			,T8.AddrState AS BillingState
			,T8.AddrZIP AS BillingPostalCode
			,T8.AddrCountry AS BillingCountry
	
			--reference only
			,'Constituent Ind' AS zrefSource
			,T.KeyInd zrefKeyInd
			,T.ConsID zrefConsID
			,T.ImportID zrefImportID
			,NULL AS zrefORImpID
			,NULL AS zrefESRImpID
			,T.RE_DB_OwnerShort zrefDBOwner
	  		,LEN(T7.Additional_Phone_Numbers__c) AS zrefaddphonelen
		 
			FROM SUTTER_MGEA_DATA.dbo.HC_Constituents_v T	
			INNER JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH T2 ON T.ImportID=T2.HH_ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort  --HH Acct Added new field HH_ImportID to the tbl.Contact_HofH
			LEFT JOIN SUTTER_MGEA_DATA.DBO.HC_Cons_Annotation_v T3 ON T.ImportID=T3.ImportID AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort

			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.SolicitCodes T4 ON T.ImportID=T4.ImportID AND T.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Phones_seq_1_final T6 ON T.ImportID=T6.ImportID AND T.RE_DB_OwnerShort=T6.RE_DB_OwnerShort AND T.KeyInd=T6.KeyInd
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Phones_seq_2_final T7 ON T.ImportID=T7.ImportID AND T.RE_DB_OwnerShort=T7.RE_DB_OwnerShort AND T.KeyInd=T7.KeyInd
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Address_Pref	T8 ON T.RE_DB_OwnerShort=T8.RE_DB_OwnerShort AND T.ImportID=T8.ImportID
			
			 

		UNION ALL 
		
			--ACCOUNT: NON-CONS ORGANIZATION RELATIONSHIP AND EDUCATION RELATIONSHIIPS 
			SELECT
			 dbo.fnc_OwnerID() AS OwnerID
			,dbo.fnc_RecordType('Account_Organizational') AS RecordTypeID
			,NULL AS RE_ConsID__c
 		 	,(T.RE_DB_OwnerShort+'-'+T.ORImpID) AS External_ID__c
 		 	,NULL AS RE_ImportID__c
			,(T.RE_DB_OwnerShort+'-'+T.ORImpID) AS RE_ORImpID__c
		
			-------------------,NULL AS MDM_ID__c


			,T.ORFullName AS Name
			,NULL AS Gives_Anonymously__c
			,NULL AS No_Valid_Address__c
			,NULL AS rC_Bios__Active__c
			,NULL AS Employee_Matching__c
			,NULL AS rC_Bios__Salutation__c
			,NULL AS RE_HasOptedOutOfEmail__c
			,NULL AS CreatedDate
			,T.RE_DB_OwnerShort AS rC_Giving__Primary_Affiliation__c
			,'TRUE'	AS	rC_Giving__Track_Affiliations__c
			,'TRUE' AS	rC_Giving__Track_Hard_Credits__c
			,'TRUE' AS	rC_Giving__Track_Soft_Credits__c
			,'TRUE' AS	rC_Giving__Track_Summaries__c
			,GETDATE()  AS	rC_Giving__Rollup_Affiliations__c
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
			,NULL AS rC_Bios__Warning_Comments__c
			,NULL AS Do_Not_Phone__c
			,NULL AS Do_Not_Solicit__c
			,NULL AS Do_Not_Mail__c
			,NULL AS Do_Not_Contact__c
			,T6.Phone
			,T6.RE_Email__c
			,T6.Fax
			,T6.RE_Home__c
			,NULL AS RE_Pager__c
			,T6.Website
			,CAST(T7.Additional_Phone_Numbers__c AS NVARCHAR(4000)) AS Additional_Phone_Numbers__c 

			,CASE WHEN T.ORAddrLine1 IS NULL THEN NULL WHEN T.ORAddrLine2!='' THEN T.ORAddrLine1 +' '+T.ORAddrLine2 ELSE T.ORAddrLine1 END AS BillingStreet
			,T.ORAddrCity AS BillingCity
			,T.ORAddrState AS BillingState
			,T.ORAddrZIP AS BillingPostalCode
			,T.ORAddrCountry AS BillingCountry
		
			--reference only
			,'Org Relat' AS zrefSource			
			,NULL AS zrefKeyInd
			,T.ConsID AS zrefConsID
			,T.ImportID AS zrefImportID
			,T.ORImpID AS zrefORImpID
			,NULL AS zrefESRImpID
			,T.RE_DB_OwnerShort AS zrefDBOwner
			, NULL AS zrefAaddphonelen
			
			FROM SUTTER_MGEA_DATA.dbo.HC_Org_Relat_v T
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.ORPhones_seq_1_final T6 ON T.ORImpID=T6.ORPhoneORImpID AND T.RE_DB_OwnerShort=T6.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.ORPhones_seq_2_final T7 ON T.ORImpID=T7.ORPhoneORImpID AND T.RE_DB_OwnerShort=T7.RE_DB_OwnerShort
			WHERE T.ORLink IS NULL OR T.ORLink=''

		UNION ALL			

			--EDUCATION RELATIONSHIPS to ACCOUNTS
			SELECT DISTINCT
			 dbo.fnc_OwnerID() AS OwnerID
			,dbo.fnc_RecordType('Account_Organizational') AS RecordTypeID
			,NULL AS RE_ConsID__c
 		 	,(T.RE_DB_OwnerShort+'-'+T.Unique_MD5) AS External_ID__c
			,NULL AS RE_ImportID__c
			,null AS RE_ORImpID__c
		
			-------------------,NULL AS MDM_ID__c


			,T.Name AS Name
			,NULL AS Gives_Anonymously__c
			,NULL AS No_Valid_Address__c
			,NULL AS rC_Bios__Active__c
			,NULL AS Employee_Matching__c
			,NULL AS rC_Bios__Salutation__c
			,NULL AS RE_HasOptedOutOfEmail__c
			,NULL AS CreatedDate
			,T.RE_DB_OwnerShort AS rC_Giving__Primary_Affiliation__c
			,'TRUE'	AS	rC_Giving__Track_Affiliations__c
			,'TRUE' AS	rC_Giving__Track_Hard_Credits__c
			,'TRUE' AS	rC_Giving__Track_Soft_Credits__c
			,'TRUE' AS	rC_Giving__Track_Summaries__c
			,GETDATE()  AS	rC_Giving__Rollup_Affiliations__c
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
			,NULL AS rC_Bios__Warning_Comments__c
			,NULL AS Do_Not_Phone__c
			,NULL AS Do_Not_Solicit__c
			,NULL AS Do_Not_Mail__c
			,NULL AS Do_Not_Contact__c
			,NULL AS Phone
			,NULL AS RE_Email__c
			,NULL AS Fax
			,NULL AS RE_Home__c
			,NULL AS RE_Pager__c
			,NULL AS Website
			,NULL AS Additional_Phone_Numbers__c
			,NULL AS BillingStreet
			,NULL AS BillingCity
			,NULL AS BillingState
			,NULL AS BillingPostalCode
			,NULL AS BillingCountry

			--reference only
			,'Educ Relat' AS zrefSource
			,NULL AS zrefKeyInd
			,NULL AS zrefConsID
			,NULL AS zrefImportID
			,NULL AS zrefORImpID
			,NULL AS zrefESRImpID
			,T.RE_DB_OwnerShort AS zrefDBOwner
			, NULL AS zrefAaddphonelen
			FROM SUTTER_MGEA_MIGRATION.TBL.ESRSchoolName T
		 
	END		
			
		BEGIN --REPLACE DOUBLE QUOTES TO SINGLE QUOTES
			-- select [name] from [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT] where [name] like '%"%'
			update [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT] set [name]=REPLACE([name],'"','''') where [name] like '%"%'
			-- select [rC_Bios__Salutation__c] from [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT] where [rC_Bios__Salutation__c] like '%"%'
			update [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT] set [rC_Bios__Salutation__c]=REPLACE([rC_Bios__Salutation__c],'"','''') where [rC_Bios__Salutation__c] like '%"%'
			-- select [rC_Bios__Warning_Comments__c] from [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT] where [rC_Bios__Warning_Comments__c] like '%"%'
			update [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT] set [rC_Bios__Warning_Comments__c]=REPLACE([rC_Bios__Warning_Comments__c],'"','''') where [rC_Bios__Warning_Comments__c] LIKE '%"%'
			-- select [BillingStreet] from [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT] where [BillingStreet] like '%"%'
			update [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT] set [BillingStreet]=REPLACE([BillingStreet],'"','''') where [BillingStreet] LIKE '%"%'
			-- select [BillingCity] from [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT] where [BillingCity] like '%"%'
			update [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT] set [BillingCity]=REPLACE([BillingCity],'"','''') where [BillingCity] LIKE '%"%'
		END


		BEGIN--check duplicates 
			SELECT * FROM [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT] 
			WHERE External_ID__c IN (SELECT External_ID__c FROM [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT] GROUP BY External_ID__c HAVING COUNT(*)>1)
			ORDER BY External_ID__c
		END 	 
			 
END


 SELECT * FROM SUTTER_MGEA_MIGRATION.imp.ACCOUNT