USE SUTTER_MGEA_MIGRATION
GO

BEGIN

	DROP TABLE SUTTER_MGEA_MIGRATION.TBL.OPPORTUNITY_CONTACT_CREDIT
	DROP TABLE SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_CONTACT_CREDIT

END

BEGIN

			--GIFT SOFTCREDIT
				/*	Link to Parent Opportunity only. 
					IF linked to a gift with [GFType] like '%pay%' then, link to the Parent Oppty associated to that Donation through [HC_Gift_Link_v].[GFPaymentGFImpID]
				*/

						SELECT 
						NULL AS ID
						,CASE WHEN T3.GFType LIKE '%pay%' THEN T6.RE_DB_OwnerShort+'-'+T6.RE_DB_Tbl+'-'+T6.GSplitImpID 
														  ELSE T5.RE_DB_OwnerShort+'-'+T5.RE_DB_Tbl+'-'+T5.GSplitImpID END 
														AS [rC_Giving__Opportunity__r:External_ID__c]
						,CASE WHEN T2.KeyInd='O' THEN T1.RE_DB_OwnerShort+'-'+T1.SoftCredRecip WHEN T7.NoHH_ImportID IS NOT NULL 
						 THEN T7.RE_DB_OwnerShort+'-'+T7.HH_ImportID ELSE T1.RE_DB_OwnerShort+'-'+T1.SoftCredRecip END AS [rC_Giving__Account__r:External_ID__c]
						,CASE WHEN T2.KeyInd='I' THEN T1.RE_DB_OwnerShort+'-'+T1.SoftCredRecip END AS [rC_Giving__Contact__r:External_ID__c]
						
						
						
						,T1.SoftCredAmt AS	rC_Giving__Amount__c
						,NULL AS Solicitor_Amount__c
						,T1.RE_DB_OwnerShort AS	Affiliation__c
						,CASE WHEN T2.KeyInd='O' THEN 'Account Only' WHEN T2.KeyInd='I' THEN 'Account & Contact' END AS	rC_Giving__Type__c
						,'Soft Credit' AS	rC_Giving__Contact_Role__c
						,'TRUE' AS	rC_Giving__Is_Fixed__c
						,CASE WHEN T3.GFType LIKE '%pay%' THEN 'Transaction Soft Credit' 
							  WHEN T3.GFType NOT LIKE '%pay%' THEN 'Donation Soft Credit' end AS Credit_Type__c
						 --reference
						,T2.KeyInd refKeyInd
						,T7.NoHH_ImportID AS refNoHH_ImportID
						,T1.GFLink refGiftImpID
						,T1.ConsID refRecipConsID
						,T3.GFType AS refGFType
						,T4.GFImpID AS refPledgeGFImpID
						,T6.GSplitImpID refPlegeGSplitGFImpID
						,T4.GFPaymentGFImpID
						,T5.GSplitImpID refPaymentGSplitImpID
						
						INTO SUTTER_MGEA_MIGRATION.TBL.OPPORTUNITY_CONTACT_CREDIT
						FROM SUTTER_MGEA_DATA.dbo.HC_Gift_SoftCredit_v T1 
						INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Constituents_v T2 ON T1.SoftCredRecip=T2.ImportID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
						LEFT JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH T7 ON T1.SoftCredRecip=T7.NoHH_ImportID AND T1.RE_DB_OwnerShort=T7.RE_DB_OwnerShort
						--GIFT
						INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Gift_v T3 ON T1.GFLink=T3.GFImpID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
						LEFT JOIN SUTTER_MGEA_DATA.DBO.HC_Gift_Link_v T4 ON T3.GFImpID=T4.GFPaymentGFImpID AND T3.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
						LEFT JOIN SUTTER_MGEA_DATA.DBO.HC_Gift_SplitGift_v T5 ON T1.GFLink=T5.GFImpID AND T1.RE_DB_OwnerShort=T5.RE_DB_OwnerShort  /*non payment*/
						LEFT JOIN SUTTER_MGEA_DATA.DBO.HC_Gift_SplitGift_v T6 ON T4.GFImpID=T6.GFImpID AND T4.RE_DB_OwnerShort=T6.RE_DB_OwnerShort /*parent split*/
							AND T5.GFSplitSequence = T6.GFSplitSequence
						WHERE (CASE WHEN T3.GFType LIKE '%pay%' THEN T6.RE_DB_OwnerShort+'-'+T6.RE_DB_Tbl+'-'+T6.GSplitImpID 
								 ELSE T5.RE_DB_OwnerShort+'-'+T5.RE_DB_Tbl+'-'+T5.GSplitImpID END) IS NOT NULL   --remove null OpportunityExternal Id from duplicates created from Splits. 
						--IF linked to a gift with [GFType] like '%pay%' then, link to the Parent Oppty associated to that Donation through [HC_Gift_Link_v].[GFPaymentGFImpID]
		 				
					UNION ALL
						 
				--GIFT SOLICITOR
						SELECT 
						NULL AS ID
						,CASE WHEN T3.GFType LIKE '%pay%' THEN T6.RE_DB_OwnerShort+'-'+T6.RE_DB_Tbl+'-'+T6.GSplitImpID 
														  ELSE T5.RE_DB_OwnerShort+'-'+T5.RE_DB_Tbl+'-'+T5.GSplitImpID END 
														AS [rC_Giving__Opportunity__r:External_ID__c]
						,CASE WHEN T2.KeyInd='O' THEN T1.RE_DB_OwnerShort+'-'+T1.SolImpID END AS [rC_Giving__Account__r:External_ID__c]
						,CASE WHEN T2.KeyInd='I' THEN T1.RE_DB_OwnerShort+'-'+T1.SolImpID END AS [rC_Giving__Contact__r:External_ID__c]
						,T1.[GFSolAmtGift Amount] AS rC_Giving__Amount__c
						,T1.[GFSolAmtGift Amount] AS Solicitor_Amount__c
						,T1.RE_DB_OwnerShort AS	Affiliation__c
						,CASE WHEN T2.KeyInd='O' THEN 'Account Only' WHEN T2.KeyInd='I' THEN 'Contact Only' END AS	rC_Giving__Type__c

						,'Peer to Peer Credit' AS	rC_Giving__Contact_Role__c
						,'TRUE' AS	rC_Giving__Is_Fixed__c
						,CASE WHEN T3.GFType LIKE '%pay%' THEN 'Transaction Soft Credit' 
							  WHEN T3.GFType NOT LIKE '%pay%' THEN 'Donation Soft Credit' end AS Source_Credit_Type__c
						--reference
						,T2.KeyInd refKeyInd
						,NULL AS refNoHH_ImportID

						,T1.GFLink refGiftImpID
						,T2.ConsID refRecipConsID
						,T3.GFType AS refGFType
						,T4.GFImpID AS refPledgeGFImpID
						,T6.GSplitImpID refPlegeGSplitGFImpID
						,T4.GFPaymentGFImpID
						,T5.GSplitImpID refPaymentGSplitImpID

						FROM SUTTER_MGEA_DATA.dbo.HC_Gift_Solicitor_v T1
						INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Constituents_v T2 ON T1.SolImpID=T2.ImportID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
						--GIFT
						INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Gift_v T3 ON T1.GFLink=T3.GFImpID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
						LEFT JOIN SUTTER_MGEA_DATA.DBO.HC_Gift_Link_v T4 ON T3.GFImpID=T4.GFPaymentGFImpID AND T3.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
						LEFT JOIN SUTTER_MGEA_DATA.DBO.HC_Gift_SplitGift_v T5 ON T1.GFLink=T5.GFImpID AND T1.RE_DB_OwnerShort=T5.RE_DB_OwnerShort 
						LEFT JOIN SUTTER_MGEA_DATA.DBO.HC_Gift_SplitGift_v T6 ON T4.GFImpID=T6.GFImpID AND T4.RE_DB_OwnerShort=T6.RE_DB_OwnerShort --IF linked to a gift with [GFType] like '%pay%' then, link to the Parent Oppty associated to that Donation through [HC_Gift_Link_v].[GFPaymentGFImpID]
								AND T5.GFSplitSequence = T6.GFSplitSequence
						WHERE (CASE WHEN T3.GFType LIKE '%pay%' THEN T6.RE_DB_OwnerShort+'-'+T6.RE_DB_Tbl+'-'+T6.GSplitImpID 
								 ELSE T5.RE_DB_OwnerShort+'-'+T5.RE_DB_Tbl+'-'+T5.GSplitImpID END) IS NOT NULL   --remove null OpportunityExternal Id from duplicates created from Splits. 
  
				UNION ALL
					
				--PROPOSAL SOLICITOR
						SELECT 
						NULL AS ID
						,T1.RE_DB_OwnerShort+'-PP-'+T1.PRSolPRImpID  AS [rC_Giving__Opportunity__r:External_ID__c]   --rC_Giving__Opportunity__c
						,CASE WHEN T2.KeyInd='O' THEN T1.RE_DB_OwnerShort+'-'+T1.PRSolImpID END AS [rC_Giving__Account__r:External_ID__c]
						,CASE WHEN T2.KeyInd='I' THEN T1.RE_DB_OwnerShort+'-'+T1.PRSolImpID END AS [rC_Giving__Contact__r:External_ID__c]
						,T1.PRSolAmount AS rC_Giving__Amount__c
						,T1.PRSolAmount AS Solicitor_Amount__c
						,T1.RE_DB_OwnerShort AS	Affiliation__c
						,CASE WHEN T2.KeyInd='O' THEN 'Account Only' WHEN T2.KeyInd='I' THEN 'Contact Only' END AS	rC_Giving__Type__c
						,'Peer to Peer Credit' AS	rC_Giving__Contact_Role__c
						,'TRUE' AS	rC_Giving__Is_Fixed__c
						,'Proposal Soft Credit' AS Source_Credit_Type__c

						--reference
						,T2.KeyInd refKeyInd
						,NULL AS refNoHH_ImportID
						,T1.PRSolPRImpID refGiftImpID
						,T2.ConsID refRecipConsID
						,NULL AS refGFType
						,NULL AS refPledgeGFImpID
						,NULL AS refPlegeGSplitGFImpID
						,NULL AS GFPaymentGFImpID
						,NULL AS refPaymentGSplitImpID

						FROM SUTTER_MGEA_DATA.dbo.HC_Proposal_Solicitor_v T1
						INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Constituents_v T2 ON T1.PRSolImpID=T2.ImportID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			
						--23656
						--24253
END 
		 
		 
BEGIN--The [Source_Credit_Type__c] will keep one-time and payments separate. 
			SELECT DISTINCT [ID]
				  ,[rC_Giving__Opportunity__r:External_ID__c]
				  ,[rC_Giving__Account__r:External_ID__c]
				  ,[rC_Giving__Contact__r:External_ID__c]
				  ,SUM([rC_Giving__Amount__c]) AS [rC_Giving__Amount__c]
				  ,SUM([Solicitor_Amount__c]) AS [Solicitor_Amount__c]
				  ,[Affiliation__c]
				  ,[rC_Giving__Type__c]
				  ,[rC_Giving__Contact_Role__c]
				  ,[rC_Giving__Is_Fixed__c]
				  ,[Source_Credit_Type__c]
			  INTO [SUTTER_MGEA_MIGRATION].[IMP].[OPPORTUNITY_CONTACT_CREDIT]
			  FROM [SUTTER_MGEA_MIGRATION].[TBL].[OPPORTUNITY_CONTACT_CREDIT]
			  GROUP BY
					[ID]
				  ,[rC_Giving__Opportunity__r:External_ID__c]
				  ,[rC_Giving__Account__r:External_ID__c]
				  ,[rC_Giving__Contact__r:External_ID__c]
				  ,[Affiliation__c]
				  ,[rC_Giving__Type__c]
				  ,[rC_Giving__Contact_Role__c]
				  ,[rC_Giving__Is_Fixed__c]
				  ,[Source_Credit_Type__c]
			
				--11341 final
 END
					 /*
					 
						SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_CONTACT_CREDIT
						WHERE EXTERNAL_id__c IN (SELECT External_id__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_CONTACT_CREDIT 
													GROUP BY External_id__c HAVING COUNT(*)>1)
						order by external_id__c
					
						SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_CONTACT_CREDIT
						WHERE  [rC_Giving__Opportunity__c:External_Id__c] is null
						
					
					*/
					 