

--SALUTATIONS 
USE SUTTER_MGEA_DATA
GO

BEGIN

	DROP TABLE SUTTER_MGEA_MIGRATION.IMP.SALUTATION
	
END


BEGIN--SALUTATIONS from CONSTITUENT records, ADDITIONAL ADDSAL, ALIAS
		
		--CONSTITUENT
		SELECT
		 dbo.fnc_OwnerID() AS OwnerID
		,dbo.fnc_RecordType('Salutation_Salutation') AS RecordTypeID
		,T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.ImportID AS External_ID__c
		,CASE WHEN T2.NoHH_ImportID IS NOT NULL THEN T2.RE_DB_OwnerShort+'-'+T2.HH_ImportID ELSE T.RE_DB_OwnerShort+'-'+T.ImportID END AS [rC_Bios__Account__r:External_ID__c]
		,NULL [rC_Bios__Contact__r:External_ID__c]
		,T.PrimAddText AS Name
		,'Primary Addressee/Salutation' AS rC_Bios__Salutation_Type__c
		,NULL AS Additional_Recognition_Subtype__c
		,T.PrimAddText AS rC_Bios__Salutation_Line_1__c
		,CASE WHEN T.KeyInd='I' THEN T.PrimSalText END AS rC_Bios__Inside_Salutation__c
		,CASE WHEN T2.NoHH_ImportID IS NOT NULL THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Preferred_Salutation__c    --only Prefered for HofHH. 
		,T.RE_DB_OwnerShort AS Affiliation__c
		,NULL AS rC_Bios__Salutation_Description__c
		--reference
		,'Constituent' AS zrefSource	
		,T.KeyInd AS zrefKeyInd
		,T.ConsID AS zrefConsID
		,T.ImportID AS zrefImportID
		,T.RE_DB_OwnerShort AS zrefREOwner
		,T.RE_DB_Tbl zrefTable
		,NULL zrefIRImpID		
		INTO SUTTER_MGEA_MIGRATION.IMP.SALUTATION
		FROM SUTTER_MGEA_DATA.dbo.HC_Constituents_v AS T
		LEFT JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH T2 ON T.ImportID=T2.NoHH_ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
		 
	UNION ALL
	
		--ADDITIONAL ADD/SAL
		SELECT
		 dbo.fnc_OwnerID() AS OwnerID
		,dbo.fnc_RecordType('Salutation_Salutation') AS RecordTypeID
		,T.UniqueID AS External_ID__c
		,CASE WHEN T2.NoHH_ImportID IS NOT NULL THEN T2.RE_DB_OwnerShort+'-'+T2.HH_ImportID ELSE T.RE_DB_OwnerShort+'-'+T.ImportID  END AS [ACCOUNT:External_Id__c]
		,T.RE_DB_OwnerShort+'-'+T.ImportID AS [rC_Bios__Contact__r:External_ID__c]
		,T.rc_Bios_Salutation_Type__c AS Name
		,T.rc_Bios_Salutation_Type__c AS rC_Bios__Salutation_Type__c
		,T.[Description] AS Additional_Recognition_Subtype__c
		,T.rC_Bios__Salutation_Line_1__c 
		,T.rC_Bios__Inside_Salutation__c
		,'FALSE' AS rC_Bios__Preferred_Salutation__c
		,T.RE_DB_OwnerShort AS Affiliation__c
		,'RE Additional Salutation Type: ' + T.[Description] AS rC_Bios__Salutation_Description__c
		--reference
		,'AddAddSal' AS zrefSource	
		,NULL AS zrefKeyInd
		,NULL AS zrefConsID
		,T.ImportID AS zrefImportID
		,T.RE_DB_OwnerShort AS zrefREOwner
		,NULL AS zrefTable
		,NULL zrefIRImpID	
	 
		FROM SUTTER_MGEA_MIGRATION.tbl.AddtlAddSal_final  T
		LEFT JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH AS T2 ON T.ImportID=T2.NoHH_ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
		 
 		 
 	UNION ALL
		--NON-CONS INDIVIDUAL RELATINOSHIP>
		SELECT
		 dbo.fnc_OwnerID() AS OwnerID
		,dbo.fnc_RecordType('Salutation_Salutation') AS RecordTypeID
		,T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.IRImpID AS External_ID__c
		,NULL AS [rC_Bios__Account__r:External_ID__c]
		,(T.RE_DB_OwnerShort+'-'+T.IRImpID) AS [rC_Bios__Contact__r:External_ID__c]
		,T.IRPrimAddText AS Name
		,'Primary Salutation' AS rC_Bios__Salutation_Type__c
		,NULL AS Additional_Recognition_Subtype__c
		,T.IRPrimAddText AS rC_Bios__Salutation_Line_1__c
		,T.IRPrimSalText AS rC_Bios__Inside_Salutation__c
		,'TRUE' AS rC_Bios__Preferred_Salutation__c
		,T.RE_DB_OwnerShort AS Affiliation__c
		,NULL AS rC_Bios__Salutation_Description__c
		--reference
		,'IndRelat' AS zrefSource	
		,'I' AS zrefKeyInd
		,T.ConsID AS zrefConsID
		,T.ImportID AS zrefImportID
		,T.RE_DB_OwnerShort AS zrefREOwner
		,T.RE_DB_Tbl zrefTable
		,T.IRImpID zrefIRImpID
		
		FROM SUTTER_MGEA_DATA.dbo.HC_Ind_Relat_v AS T
		WHERE (T.IRLink='' OR T.IRlink IS NULL)  
	
		 
END; 

BEGIN--CHECK DUPLICATES
		SELECT * 
		FROM SUTTER_MGEA_MIGRATION.IMP.SALUTATION
		WHERE External_ID__c IN (SELECT External_ID__c FROM SUTTER_MGEA_MIGRATION.IMP.SALUTATION GROUP BY External_ID__c HAVING COUNT(*)>1)
END



BEGIN
		--SELECT NAME FROM SUTTER_MGEA_MIGRATION.IMP.SALUTATION WHERE NAME LIKE '%"%'
		
		UPDATE SUTTER_MGEA_MIGRATION.IMP.SALUTATION SET NAME=REPLACE(NAME,'"','''') where NAME like '%"%'
		UPDATE SUTTER_MGEA_MIGRATION.IMP.SALUTATION SET rC_Bios__Salutation_Line_1__c=REPLACE(rC_Bios__Salutation_Line_1__c,'"','''') where rC_Bios__Salutation_Line_1__c like '%"%'
		UPDATE SUTTER_MGEA_MIGRATION.IMP.SALUTATION SET rC_Bios__Inside_Salutation__c=REPLACE(rC_Bios__Inside_Salutation__c,'"','''') where rC_Bios__Inside_Salutation__c like '%"%'
END		

BEGIN

	SELECT COUNT(*) FROM SUTTER_MGEA_MIGRATION.IMP.SALUTATION

END
 
BEGIN

	SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.SALUTATION ORDER BY [rC_Bios__Account__r:External_ID__c]

END
 
 ALTER TABLE SUTTER_MGEA_MIGRATION.IMP.SALUTATION
ALTER COLUMN Name VARCHAR(max)
 
 ALTER TABLE SUTTER_MGEA_MIGRATION.IMP.SALUTATION
ALTER COLUMN rC_Bios__Salutation_Type__c VARCHAR(max)
 
 ALTER TABLE SUTTER_MGEA_MIGRATION.IMP.SALUTATION
ALTER COLUMN Additional_Recognition_Subtype__c VARCHAR(max)

 ALTER TABLE SUTTER_MGEA_MIGRATION.IMP.SALUTATION
ALTER COLUMN rC_Bios__Salutation_Line_1__c VARCHAR(max)

 ALTER TABLE SUTTER_MGEA_MIGRATION.IMP.SALUTATION
ALTER COLUMN rC_Bios__Inside_Salutation__c VARCHAR(max)


 ALTER TABLE SUTTER_MGEA_MIGRATION.IMP.SALUTATION
ALTER COLUMN rC_Bios__Salutation_Description__c VARCHAR(max)