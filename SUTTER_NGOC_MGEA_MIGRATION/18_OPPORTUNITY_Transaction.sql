USE SUTTER_MGEA_MIGRATION
GO  

BEGIN
		DROP TABLE SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_TRANSACTION
END
 
		 
BEGIN
			--TRANSACTIONS OPPORTUNITIES from NON_PAYMENTS (Cash/Other) 
						SELECT  
						dbo.fnc_OwnerID() AS OwnerID         
	/*EXT_ID*/			,T.[ACCOUNT:External_Id__c]
						,'T-'+T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS External_Id__c
						,'T-'+T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS RE_GSplitImpID__c
						,'T-'+T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitGFImpID AS RE_GFImpID__c
	/*EXT_ID*/			,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS [rC_Giving__Parent__r:External_ID__c]

						,T.[GSplitRE_DB_OwnerShort]+' - '+T.Child_Giving_RecordType+' - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10))AS NAME
						,CAST(T.GFDate AS DATE) AS	CloseDate
						,dbo.fnc_RecordType('Opportunity_'+T.Child_Giving_RecordType) AS RecordTypeID
						,'Completed' AS StageName
						,CAST(T.GSplitAmt AS DECIMAL(12,2)) AS Amount 
					
						,'Payment' AS rC_Giving__Transaction_Type__c
						,T.[GSplitRE_DB_OwnerShort] AS rC_Giving__Affiliation__c 
						,T.rC_Giving__Is_Giving_Transaction__c
						,'FALSE' AS rC_Giving__Is_Payment_Refunded__c
						--payment method
						,T.[rC_Giving__Payment_Type__c] AS rC_Giving__Payment_Method__c
	/*EXT_ID*/			,T.[PAYMENT_METHOD_External_ID__c] AS [rC_Giving__Payment_Method_Selected__r:External_ID__c]
						--allocations
						,T.GSplitAmt AS rC_Giving__Allocation_Amount__c
	/*EXT_ID*/			,T.GAU_External_Id__c AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]	 
						--RE gift_v
						,T.RE_Legacy_Financial_Code__c
						,T.BATCH_NUMBER AS RE_Batch_Number__c
						,CASE WHEN T.GFAck='Acknowledged' THEN 'TRUE' ELSE 'FALSE' END	AS rC_Giving__Acknowledged__c
						,T.GFAckDate	AS	rC_Giving__Acknowledged_Date__c
						,T.GFAnon	AS	rC_Giving__Is_Anonymous__c
						,T.GFCheckDate	AS	rC_Giving__Check_Date__c
						,T.GFCheckNum	AS	rC_Giving__Check_Number__c
						,T.GFRef	AS	[Description]
						,T.GFStkIss	AS	Stock_Issuer__c
						,T.GFStkMedPrice	AS	Stock_Median_Price__c
						,T.GFStkNumUnits	AS	rC_Giving__Number_Of_Shares__c
						,T.GFStkSymbol	AS	rC_Giving__Ticker_Symbol__c
						,T.Production_Stage__c
						,T.DateAdded	AS	CreatedDate
						,T.GFConsDesc AS RE_Gift_Constituency_Code__c

						--attributes (chart-attributes)
							,T.OPX_Corporate_Donation_Contact__c
							,T.Donation_Page_Name__c
							,T.OPX_Provider__c
							,T.OPX_Transaction_ID__c	
						--gift attribute (chart_giftattribute)
							,CAST(T.OPPORTUNITY_Attribute__c AS varchar(max)) AS Attribute__c 

						--GIFT TRIBUTE
						,T.rC_Giving__Tribute_Type__c
						,T.rC_Giving__Tribute_Description__c	
						,T.rC_Giving__Is_Tribute__c
						,T.rC_Giving__Tribute_Effective_Date__c
						,T.rC_Giving__Tribute_Comments__c
						,T.[rC_Giving__Tribute_Contact__r:External_ID__c]
					
						--MISC.
						,T.RE_Split_Gift__c
					
						--reference
						--,X1.ID zrefAccountId
						,T.GFType AS zrefGFType
						,T.GSplitGFImpID AS zrefParentGFImpID
						,T.GSplitGFImpID AS zrefPaymentGFImpID
						,T.GSplitImpID AS zrefPaymentGSplitImpID
						,T.[GSplitRE_DB_OwnerShort] AS zrefRE_DB_Owner

						,NULL zrefGLinkAmt
					
						--,LEN(X1.Name +': '+ T.[GSplitRE_DB_OwnerShort]+' '+T.Child_Giving_RecordType+' Completed $'+
						 -- CAST(CAST(T.GSplitAmt AS DECIMAL(12,2))AS NVARCHAR(20))  +' '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10))) AS zrefNameLen

						INTO SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_TRANSACTION
						FROM SUTTER_MGEA_MIGRATION.TBL.GIFT T
						--LEFT JOIN SUTTER_MGEA_MIGRATION.XTR.ACCOUNT X1 ON T.[ACCOUNT:External_Id__c]=X1.EXTERNAL_ID__C  --- not needed. oppt name format changed and doesn't need the Acct Name

	/*transaction*/		WHERE T.Transaction_Source='NON_PAYMENT' 
						 -- 273,838
			UNION ALL

		--TRANSACTIONS OPPORTUNITIES from PAYMENTS (Pay-Cash/Pay-Other/Pay-*) 
					SELECT  
					dbo.fnc_OwnerID() AS OwnerID         
/*EXT_ID*/			,T.[ACCOUNT:External_Id__c]
					,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS External_Id__c
					,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS RE_GSplitImpID__c
					,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitGFImpID AS RE_GFImpID__c
/*EXT_ID*/			,T2.[GSplitRE_DB_OwnerShort]+'-'+T2.[GSplitRE_DB_Tbl]+'-'+T2.GSplitImpID AS [rC_Giving__Parent__r:External_ID__c]

					,T.[GSplitRE_DB_OwnerShort]+' - '+T.Child_Giving_RecordType+' - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10))AS NAME
					,CAST(T.GFDate AS DATE) AS	CloseDate
					,dbo.fnc_RecordType('Opportunity_'+T.Child_Giving_RecordType) AS RecordTypeID
					,'Completed' AS StageName
					,CAST(T.GSplitAmt AS DECIMAL(12,2)) AS Amount 
					
					,'Payment' AS rC_Giving__Transaction_Type__c
					,T.[GSplitRE_DB_OwnerShort] AS rC_Giving__Affiliation__c 
					,T.rC_Giving__Is_Giving_Transaction__c
					,'FALSE' AS rC_Giving__Is_Payment_Refunded__c
					--payment method
					,T.[rC_Giving__Payment_Type__c] AS rC_Giving__Payment_Method__c
/*EXT_ID*/			,T.[PAYMENT_METHOD_External_ID__c] AS [rC_Giving__Payment_Method_Selected__r:External_ID__c]
					--allocations
					,T.GSplitAmt AS rC_Giving__Allocation_Amount__c
/*EXT_ID*/			,T.GAU_External_Id__c AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]	 
					--RE gift_v
					,T.RE_Legacy_Financial_Code__c
					,T.BATCH_NUMBER AS RE_Batch_Number__c
					,CASE WHEN T.GFAck='Acknowledged' THEN 'TRUE' ELSE 'FALSE' END	AS rC_Giving__Acknowledged__c
					,T.GFAckDate	AS	rC_Giving__Acknowledged_Date__c
					,T.GFAnon	AS	rC_Giving__Is_Anonymous__c
					,T.GFCheckDate	AS	rC_Giving__Check_Date__c
					,T.GFCheckNum	AS	rC_Giving__Check_Number__c
					,T.GFRef	AS	[Description]
					,T.GFStkIss	AS	Stock_Issuer__c
					,T.GFStkMedPrice	AS	Stock_Median_Price__c
					,T.GFStkNumUnits	AS	rC_Giving__Number_Of_Shares__c
					,T.GFStkSymbol	AS	rC_Giving__Ticker_Symbol__c
					,T.Production_Stage__c
					,T.DateAdded	AS	CreatedDate
					,T.GFConsDesc AS RE_Gift_Constituency_Code__c

					--attributes (chart-attributes)
						,T.OPX_Corporate_Donation_Contact__c
						,T.Donation_Page_Name__c
						,T.OPX_Provider__c
						,T.OPX_Transaction_ID__c	
					--gift attribute (chart_giftattribute)
						,CAST(T.OPPORTUNITY_Attribute__c AS varchar(max)) AS Attribute__c 

					--GIFT TRIBUTE
					,T.rC_Giving__Tribute_Type__c
					,T.rC_Giving__Tribute_Description__c	
					,T.rC_Giving__Is_Tribute__c
					,T.rC_Giving__Tribute_Effective_Date__c
					,T.rC_Giving__Tribute_Comments__c
					,T.[rC_Giving__Tribute_Contact__r:External_ID__c]
							
					--MISC.
					,T.RE_Split_Gift__c
					
					--reference
					--,X1.ID zrefAccountId
					,T.GFType AS zrefGFType
					,T.GSplitGFImpID AS zrefParentGFImpID
					,T.GSplitGFImpID AS zrefPaymentGFImpID
					,T.GSplitImpID AS zrefPaymentGSplitImpID
					,T.[GSplitRE_DB_OwnerShort] AS zrefRE_DB_Owner
					,T1.GFPaymentAmount AS zrefGLinkAmt
					--,LEN(X1.Name +': '+ T.[GSplitRE_DB_OwnerShort]+' '+T.Child_Giving_RecordType+' Completed $'+
					--  CAST(CAST(T.GSplitAmt AS DECIMAL(12,2))AS NVARCHAR(20))  +' '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10))) AS zrefNameLen

					--INTO SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_TRANSACTION
					FROM SUTTER_MGEA_DATA.dbo.HC_Gift_Link_v AS T1 
/*parent*/			INNER JOIN SUTTER_MGEA_MIGRATION.TBL.GIFT AS T2 ON T1.GFImpID = T2.GSplitGFImpID AND T1.RE_DB_OwnerShort = T2.GSplitRE_DB_OwnerShort
/*transaction*/		INNER JOIN SUTTER_MGEA_MIGRATION.TBL.GIFT AS T ON (T1.GFPaymentGFImpID = T.GSplitGFImpID AND T1.RE_DB_OwnerShort = T.GSplitRE_DB_OwnerShort
					AND T2.GFSplitSequence = T.GFSplitSequence)   --Using the GSplitSequence for matching split payments and pledges. 

/*transaction*/		WHERE T.Transaction_Source='PAYMENT'
					--219,559
					 

			UNION ALL 
			
				--WRITE OFFs 
				SELECT  
					dbo.fnc_OwnerID() AS OwnerID         
/*EXT_ID*/			,T1.[ACCOUNT:External_Id__c]
					,T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.GFImpID AS External_Id__c
					,T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.GFImpID AS RE_GSplitImpID__c
					,T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.GFImpID AS RE_GFImpID__c
					,T3.RE_DB_OwnerShort+'-'+T3.RE_DB_Tbl+'-'+T3.GSplitImpID AS [rC_Giving__Parent__r:External_ID__c]

					,T1.RE_DB_OwnerShort+' - Transaction-Uncollectible - '+CAST(CAST(T1.GFDate AS DATE)AS NVARCHAR(10))AS NAME	
					,CAST(T1.GFDate AS DATE) AS	CloseDate
					,dbo.fnc_RecordType('Opportunity_Transaction') AS RecordTypeID
					,'Uncollectible' AS StageName
					,CAST(T1.GFTAmt AS DECIMAL(12,2)) AS Amount 


					,'Payment' AS rC_Giving__Transaction_Type__c
					,T1.RE_DB_OwnerShort AS rC_Giving__Affiliation__c 
					,'TRUE' rC_Giving__Is_Giving_Transaction__c
					
					,NULL AS rC_Giving__Is_Payment_Refunded__c
					--payment method
					,NULL AS rC_Giving__Payment_Method__c
					,NULL AS [rC_Giving__Payment_Method_Selected__r:External_ID__c]
					--allocations
					,NULL AS rC_Giving__Allocation_Amount__c
					,NULL AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]		 
					--RE gift_v
					,NULL AS RE_Legacy_Financial_Code__c
					,NULL AS RE_Batch_Number__c
					,NULL AS rC_Giving__Acknowledged__c
					,NULL AS rC_Giving__Acknowledged_Date__c
					,NULL AS rC_Giving__Is_Anonymous__c
					,NULL AS rC_Giving__Check_Date__c
					,NULL AS rC_Giving__Check_Number__c
					,NULL AS [Description]
					,NULL AS Stock_Issuer__c
					,NULL AS Stock_Median_Price__c
					,NULL AS rC_Giving__Number_Of_Shares__c
					,NULL AS rC_Giving__Ticker_Symbol__c
					,NULL AS Production_Stage__c
					,T1.DateAdded	AS	CreatedDate
					,NULL AS RE_Gift_Constituency_Code__c
					
					--attributes (chart-attributes)
					, NULL AS OPX_Corporate_Donation_Contact__c
					, NULL AS Donation_Page_Name__c
					, NULL AS OPX_Provider__c
					, NULL AS OPX_Transaction_ID__c	
					--gift attribute (chart_giftattribute)
					, NULL AS Attribute__c

					--GIFT TRIBUTE
					, NULL AS rC_Giving__Tribute_Type__c
					, NULL AS rC_Giving__Tribute_Description__c	
					, NULL AS rC_Giving__Is_Tribute__c
					, NULL AS rC_Giving__Tribute_Effective_Date__c
					, NULL AS rC_Giving__Tribute_Comments__c
					, NULL AS [rC_Giving__Tribute_Contact__r:External_ID__c]
					--MISC.
					, NULL AS RE_Split_Gift__c
					--reference
					--,X1.ID AS zrefAccountId
					,T1.GFType AS zrefGFType
					,T1.GFLink AS zrefParentGFImpID
					,T1.GFImpID AS zrefPaymentGFImpID
					,T3.GSplitImpID AS zrefPaymentGSplitImpID
					,T3.RE_DB_OwnerShort AS zrefRE_DB_Owner

					, NULL AS zrefGLinkAmt
					--,LEN(X1.Name +'- '+ T1.RE_DB_OwnerShort+'- Transaction- Uncollectible- $'+
	    			--	  CAST(CAST(T1.GFTAmt AS DECIMAL(12,2))AS NVARCHAR(20))+'- '+ 
	    			--	  CAST(CAST(T1.GFDate AS DATE)AS NVARCHAR(10))) AS zrefNameLen
	    				  
				FROM SUTTER_MGEA_MIGRATION.TBL.GIFT_WRITEOFF T1
/*Parent ID*/	INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Gift_SplitGift_v T3 ON T1.GFLink=T3.GFImpID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort  
				--LEFT JOIN SUTTER_MGEA_MIGRATION.XTR.ACCOUNT X1 ON T1.[ACCOUNT:External_Id__c]=X1.EXTERNAL_ID__C 
				--3,315
			
				--total final: 502,412
END;				


		---TRANSACTIONS OPPORTUNITIES from PAYMENTS (Pay-Cash/Pay-Other/Pay-*) 
			--paymnents missing from split payment query
					INSERT INTO [IMP].[OPPORTUNITY_TRANSACTION]
					([OwnerID] ,[ACCOUNT:External_Id__c] ,[External_Id__c] ,[RE_GSplitImpID__c] ,[RE_GFImpID__c] ,[rC_Giving__Parent__r:External_ID__c] ,[NAME]
					,[CloseDate] ,[RecordTypeID] ,[StageName] ,[Amount],[rC_Giving__Transaction_Type__c] ,[rC_Giving__Affiliation__c]
					,[rC_Giving__Is_Giving_Transaction__c] ,[rC_Giving__Is_Payment_Refunded__c] ,[rC_Giving__Payment_Method__c] ,[rC_Giving__Payment_Method_Selected__r:External_ID__c]
					,[rC_Giving__Allocation_Amount__c] ,[rC_Giving__GAU__r:rC_Giving__External_ID__c] ,[RE_Legacy_Financial_Code__c] ,[RE_Batch_Number__c]
					,[rC_Giving__Acknowledged__c] ,[rC_Giving__Acknowledged_Date__c] ,[rC_Giving__Is_Anonymous__c] ,[rC_Giving__Check_Date__c]
					,[rC_Giving__Check_Number__c] ,[Description] ,[Stock_Issuer__c] ,[Stock_Median_Price__c] ,[rC_Giving__Number_Of_Shares__c]
					,[rC_Giving__Ticker_Symbol__c] ,[Production_Stage__c] ,[CreatedDate] ,[RE_Gift_Constituency_Code__c] ,[OPX_Corporate_Donation_Contact__c]
					,[Donation_Page_Name__c] ,[OPX_Provider__c] ,[OPX_Transaction_ID__c] ,[Attribute__c] ,[rC_Giving__Tribute_Type__c] ,[rC_Giving__Tribute_Description__c]
					,[rC_Giving__Is_Tribute__c] ,[rC_Giving__Tribute_Effective_Date__c] ,[rC_Giving__Tribute_Comments__c] ,[rC_Giving__Tribute_Contact__r:External_ID__c]
					,[RE_Split_Gift__c] ,[zrefGFType] ,[zrefParentGFImpID] ,[zrefPaymentGFImpID] ,[zrefPaymentGSplitImpID] ,[zrefRE_DB_Owner]
					,[zrefGLinkAmt])
					SELECT  
					dbo.fnc_OwnerID() AS OwnerID         
/*EXT_ID*/			,T.[ACCOUNT:External_Id__c]
					,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS External_Id__c
					,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS RE_GSplitImpID__c
					,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitGFImpID AS RE_GFImpID__c
/*EXT_ID*/			,T2.[GSplitRE_DB_OwnerShort]+'-'+T2.[GSplitRE_DB_Tbl]+'-'+T2.GSplitImpID AS [rC_Giving__Parent__r:External_ID__c]

					,T.[GSplitRE_DB_OwnerShort]+' - '+T.Child_Giving_RecordType+' - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10))AS NAME
					,CAST(T.GFDate AS DATE) AS	CloseDate
					,dbo.fnc_RecordType('Opportunity_'+T.Child_Giving_RecordType) AS RecordTypeID
					,'Completed' AS StageName
					,CAST(T.GSplitAmt AS DECIMAL(12,2)) AS Amount 
					
					,'Payment' AS rC_Giving__Transaction_Type__c
					,T.[GSplitRE_DB_OwnerShort] AS rC_Giving__Affiliation__c 
					,T.rC_Giving__Is_Giving_Transaction__c
					,'FALSE' AS rC_Giving__Is_Payment_Refunded__c
					--payment method
					,T.[rC_Giving__Payment_Type__c] AS rC_Giving__Payment_Method__c
/*EXT_ID*/			,T.[PAYMENT_METHOD_External_ID__c] AS [rC_Giving__Payment_Method_Selected__r:External_ID__c]
					--allocations
					,T.GSplitAmt AS rC_Giving__Allocation_Amount__c
/*EXT_ID*/			,T.GAU_External_Id__c AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]	 
					--RE gift_v
					,T.RE_Legacy_Financial_Code__c
					,T.BATCH_NUMBER AS RE_Batch_Number__c
					,CASE WHEN T.GFAck='Acknowledged' THEN 'TRUE' ELSE 'FALSE' END	AS rC_Giving__Acknowledged__c
					,T.GFAckDate	AS	rC_Giving__Acknowledged_Date__c
					,T.GFAnon	AS	rC_Giving__Is_Anonymous__c
					,T.GFCheckDate	AS	rC_Giving__Check_Date__c
					,T.GFCheckNum	AS	rC_Giving__Check_Number__c
					,T.GFRef	AS	[Description]
					,T.GFStkIss	AS	Stock_Issuer__c
					,T.GFStkMedPrice	AS	Stock_Median_Price__c
					,T.GFStkNumUnits	AS	rC_Giving__Number_Of_Shares__c
					,T.GFStkSymbol	AS	rC_Giving__Ticker_Symbol__c
					,T.Production_Stage__c
					,T.DateAdded	AS	CreatedDate
					,T.GFConsDesc AS RE_Gift_Constituency_Code__c

					--attributes (chart-attributes)
						,T.OPX_Corporate_Donation_Contact__c
						,T.Donation_Page_Name__c
						,T.OPX_Provider__c
						,T.OPX_Transaction_ID__c	
					--gift attribute (chart_giftattribute)
						,CAST(T.OPPORTUNITY_Attribute__c AS varchar(max)) AS Attribute__c 

					--GIFT TRIBUTE
					,T.rC_Giving__Tribute_Type__c
					,T.rC_Giving__Tribute_Description__c	
					,T.rC_Giving__Is_Tribute__c
					,T.rC_Giving__Tribute_Effective_Date__c
					,T.rC_Giving__Tribute_Comments__c
					,T.[rC_Giving__Tribute_Contact__r:External_ID__c]
							
					--MISC.
					,T.RE_Split_Gift__c
					
					--reference
					--,X1.ID zrefAccountId
					,T.GFType AS zrefGFType
					,T.GSplitGFImpID AS zrefParentGFImpID
					,T.GSplitGFImpID AS zrefPaymentGFImpID
					,T.GSplitImpID AS zrefPaymentGSplitImpID
					,T.[GSplitRE_DB_OwnerShort] AS zrefRE_DB_Owner
					,T1.GFPaymentAmount AS zrefGLinkAmt

					FROM SUTTER_MGEA_DATA.dbo.HC_Gift_Link_v AS T1 
/*parent*/			INNER JOIN SUTTER_MGEA_MIGRATION.TBL.GIFT AS T2 ON T1.GFImpID = T2.GSplitGFImpID AND T1.RE_DB_OwnerShort = T2.GSplitRE_DB_OwnerShort
/*transaction*/		INNER JOIN SUTTER_MGEA_MIGRATION.TBL.GIFT AS T ON (T1.GFPaymentGFImpID = T.GSplitGFImpID AND T1.RE_DB_OwnerShort = T.GSplitRE_DB_OwnerShort)
					--AND T2.GFSplitSequence = T.GFSplitSequence)   --disable to help find missing gifts
/*OPP TRX */		LEFT JOIN SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_TRANSACTION T3 ON T.GSplitImpID =T3.zrefPaymentGSplitImpID
					AND T.[GSplitRE_DB_OwnerShort]=T3.zrefRE_DB_Owner
/*transaction*/		WHERE T.Transaction_Source='PAYMENT' AND T3.zrefPaymentGSplitImpID IS NULL
					--1,118





BEGIN--check duplicates

				/*	SELECT GFType, Parent_Giving_RecordType, COUNT(*) c 
					FROM SUTTER_MGEA_MIGRATION.TBL.GIFT
					GROUP BY GFType, Parent_Giving_RecordType
 				*/  
 			
 				SELECT DISTINCT *
 				FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_TRANSACTION
 				WHERE External_Id__c IN (SELECT External_Id__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_TRANSACTION GROUP BY External_Id__c HAVING COUNT(*)>1)
				ORDER BY External_Id__c 


END


BEGIN--REMOVE DOUBLE QUOTES					
	--SELECT rC_Giving__Tribute_Description__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE rC_Giving__Tribute_Description__c LIKE '%"%'
	--SELECT [Description] FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE [Description] LIKE '%"%'
	--SELECT Stock_Issuer__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE Stock_Issuer__c LIKE '%"%'
	--SELECT Stock_Median_Price__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE Stock_Median_Price__c LIKE '%"%'
	--SELECT rC_Giving__Tribute_Comments__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE rC_Giving__Tribute_Comments__c LIKE '%"%'
	
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].OPPORTUNITY_TRANSACTION SET NAME =REPLACE(NAME,'"','''') where NAME like '%"%'
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].OPPORTUNITY_TRANSACTION SET rC_Giving__Tribute_Description__c=REPLACE(rC_Giving__Tribute_Description__c,'"','''') where rC_Giving__Tribute_Description__c like '%"%'
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].OPPORTUNITY_TRANSACTION SET [Description]=REPLACE([Description],'"','''') where [Description] like '%"%'
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].OPPORTUNITY_TRANSACTION SET Stock_Issuer__c=REPLACE(Stock_Issuer__c,'"','''') where Stock_Issuer__c like '%"%'
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].OPPORTUNITY_TRANSACTION SET Stock_Median_Price__c=REPLACE(Stock_Median_Price__c,'"','''') where Stock_Median_Price__c like '%"%'
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].OPPORTUNITY_TRANSACTION SET rC_Giving__Tribute_Comments__c=REPLACE(rC_Giving__Tribute_Comments__c,'"','''') where rC_Giving__Tribute_Comments__c like '%"%'

END 
 

 


BEGIN-- QA for missing records

	SELECT T.*
	FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_TRANSACTION T
	LEFT JOIN SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY X ON T.External_Id__c=X.External_Id__c
	WHERE X.External_Id__c IS NULL 
	ORDER BY T.External_Id__c

END


SELECT COUNT(*) FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_TRANSACTION