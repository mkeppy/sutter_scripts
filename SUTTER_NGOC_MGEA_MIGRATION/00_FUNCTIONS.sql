



-- FUNCTION: Set OwnerID

		USE SUTTER_MGEA_MIGRATION 
		GO

		SET QUOTED_IDENTIFIER ON
		GO
		SET ANSI_NULLS ON
		GO

		CREATE FUNCTION [dbo].[fnc_OwnerId]()  
		  
		  RETURNS varchar(18)  
		  BEGIN 
			declare @Owner as varchar(18)     
			set @Owner = '00561000000Eef1AAC'   -- UserName: Nadia Mayard
			return @Owner
		  END

		GO

--FUNCTION: Set RecordType
	SET QUOTED_IDENTIFIER ON
	GO
	SET ANSI_NULLS ON
	GO

	
	CREATE FUNCTION [dbo].[fnc_RecordType]
	(  
		@RecordTypeName NVARCHAR(120)
	) 
	RETURNS nvarchar(120)  
	AS
	BEGIN 
		DECLARE @RecordTypeId AS NVARCHAR(18);    
			
			SET	@RecordTypeId = CASE	WHEN @RecordTypeName ='Account_Household' THEN '01261000000UqZqAAK'  --Household
										WHEN @RecordTypeName ='Account_Organizational' THEN '01261000000UqZrAAK'  --Organizational
										WHEN @RecordTypeName ='Campaign_Event' THEN '01261000000UsFFAA0'  --Event
										WHEN @RecordTypeName ='Campaign_Event_Session' THEN '01261000000UsFGAA0'  --Event_Session
										WHEN @RecordTypeName ='Campaign_Event_Template' THEN '01261000000UsFHAA0'  --Event_Template
										WHEN @RecordTypeName ='Campaign_Standard' THEN '01261000000UsOTAA0'  --Standard
										WHEN @RecordTypeName ='Campaign_Volunteer_Jobs' THEN '01261000000UsOaAAK'  --Volunteer_Jobs
										WHEN @RecordTypeName ='Campaign_Volunteer_Project' THEN '01261000000UsObAAK'  --Volunteer_Project
										WHEN @RecordTypeName ='Campaign_Volunteer_Shifts' THEN '01261000000UsOcAAK'  --Volunteer_Shifts
										WHEN @RecordTypeName ='CampaignMember_Event_Member' THEN '01261000000UsFIAA0'  --Event_Member
										WHEN @RecordTypeName ='CampaignMember_Scheduled_Volunteer' THEN '01261000000UsOdAAK'  --Scheduled_Volunteer
										WHEN @RecordTypeName ='Case_External_Cases' THEN '01261000000UqZuAAK'  --External_Cases
										WHEN @RecordTypeName ='Case_External_Cases' THEN '01261000000UsO4AAK'  --External_Cases
										WHEN @RecordTypeName ='Contact_Household' THEN '01261000000UqZvAAK'  --Household
										WHEN @RecordTypeName ='Contact_Organizational' THEN '01261000000UqZwAAK'  --Organizational
								
										WHEN @RecordTypeName ='Opportunity_Corporate_Underwriting' THEN '01261000000UqbZAAS'  --Corporate_Underwriting
										WHEN @RecordTypeName ='Opportunity_Donation' THEN '01261000000UqbaAAC'  --Donation
										WHEN @RecordTypeName ='Opportunity_Grant' THEN '01261000000UqbbAAC'  --Grant
										WHEN @RecordTypeName ='Opportunity_Inkind' THEN '01261000000UqbcAAC'  --Inkind
										WHEN @RecordTypeName ='Opportunity_Membership' THEN '01261000000UqbdAAC'  --Membership
										WHEN @RecordTypeName ='Opportunity_Outright_Gift' THEN '01261000000UqbeAAC'  --Outright_Gift
										WHEN @RecordTypeName ='Opportunity_Pledge' THEN '01261000000UqbfAAC'  --Pledge
										WHEN @RecordTypeName ='Opportunity_Pledge_Payment' THEN '01261000000UqbgAAC'  --Pledge_Payment
										WHEN @RecordTypeName ='Opportunity_Proposal' THEN '01261000000UqbhAAC'  --Proposal
										WHEN @RecordTypeName ='Opportunity_Purchase' THEN '01261000000UqbiAAC'  --Purchase
										WHEN @RecordTypeName ='Opportunity_Transaction' THEN '01261000000UqbjAAC'  --Transaction
										WHEN @RecordTypeName ='Opportunity_Credit_Recognition' THEN '01261000000UqbkAAC'  --Recognition
										WHEN @RecordTypeName ='Opportunity_Credit_Soft' THEN '01261000000UqblAAC'  --Soft

										WHEN @RecordTypeName ='Payment_Method_Cash/Check' THEN '01261000000UqbmAAC'  --Cash_Check
										WHEN @RecordTypeName ='Payment_Method_Charge Card' THEN '01261000000UqbnAAC'  --Charge_Card
										WHEN @RecordTypeName ='Payment_Method_Direct Debit' THEN '01261000000UqboAAC'  --Direct_Debit
										WHEN @RecordTypeName ='Payment_Method_EFT' THEN '01261000000UqbpAAC'  --EFT
										WHEN @RecordTypeName ='Payment_Method_Other' THEN '01261000000UqbqAAC'  --Other
										WHEN @RecordTypeName ='Payment_Method_Paypal' THEN '01261000000UqbrAAC'  --Paypal
										WHEN @RecordTypeName ='Payment_Method_Securities' THEN '01261000000UqbsAAC'  --Securities
										WHEN @RecordTypeName ='Payment_Method_Third Party Charge' THEN '01261000000UqbtAAC'  --Third_Party_Charge
										WHEN @RecordTypeName ='Payment_Method_Voucher' THEN '01261000000UqbuAAC'  --Voucher

										WHEN @RecordTypeName ='Planned_Giving_Bequest' THEN '01261000000UqbvAAC'  --Bequest
										WHEN @RecordTypeName ='Planned_Giving_Charitable_Gift_Annuity' THEN '01261000000UqbwAAC'  --Charitable_Gift_Annuity
										WHEN @RecordTypeName ='Planned_Giving_Charitable_Lead_Annuity_Trust' THEN '01261000000UqbxAAC'  --Charitable_Lead_Annuity_Trust
										WHEN @RecordTypeName ='Planned_Giving_Charitable_Lead_Unitrust' THEN '01261000000UqbyAAC'  --Charitable_Lead_Unitrust
										WHEN @RecordTypeName ='Planned_Giving_Charitable_Remainder_Annuity_Trust' THEN '01261000000UqbzAAC'  --Charitable_Remainder_Annuity_Trust
										WHEN @RecordTypeName ='Planned_Giving_Charitable_Remainder_Unitrust' THEN '01261000000Uqc0AAC'  --Charitable_Remainder_Unitrust
										WHEN @RecordTypeName ='Planned_Giving_Deferred_Gift_Annuity' THEN '01261000000Uqc1AAC'  --Deferred_Gift_Annuity
										WHEN @RecordTypeName ='Planned_Giving_Life_Insurance' THEN '01261000000Uqc2AAC'  --Life_Insurance
										WHEN @RecordTypeName ='Planned_Giving_Pooled_Income_Fund' THEN '01261000000Uqc3AAC'  --Pooled_Income_Fund
										WHEN @RecordTypeName ='Planned_Giving_Retained_Life_Estate' THEN '01261000000Uqc4AAC'  --Retained_Life_Estate
										WHEN @RecordTypeName ='Planned_Giving_Retirement_Plan' THEN '01261000000Uqc5AAC'  --Retirement_Plan
										WHEN @RecordTypeName ='Preference_Standard' THEN '01261000000UqZzAAK'  --Standard
										WHEN @RecordTypeName ='Preference_Volunteer' THEN '01261000000Uqa0AAC'  --Volunteer
										WHEN @RecordTypeName ='Product2_Item' THEN '01261000000Uqc6AAC'  --Item
										WHEN @RecordTypeName ='Product2_Package' THEN '01261000000Uqc7AAC'  --Package
										WHEN @RecordTypeName ='rC_Bios__Action_Plan__c_Action_Plan' THEN '01261000000UqZsAAK'  --Action_Plan
										WHEN @RecordTypeName ='rC_Bios__Action_Plan__c_Action_Plan_Template' THEN '01261000000UqZtAAK'  --Action_Plan_Template
										WHEN @RecordTypeName ='rC_Bios__Preference_Rule_Criteria2__c_Account' THEN '01261000000UqZxAAK'  --Account
										WHEN @RecordTypeName ='rC_Bios__Preference_Rule_Criteria2__c_Contact' THEN '01261000000UqZyAAK'  --Contact
										WHEN @RecordTypeName ='rC_Event__Campaign_Partner__c_Organizer' THEN '01261000000UsFJAA0'  --Organizer
										WHEN @RecordTypeName ='rC_Event__Campaign_Partner__c_Other' THEN '01261000000UsFKAA0'  --Other
										WHEN @RecordTypeName ='rC_Event__Campaign_Partner__c_Partner' THEN '01261000000UsFLAA0'  --Partner
										WHEN @RecordTypeName ='rC_Event__Campaign_Partner__c_Speaker' THEN '01261000000UsFMAA0'  --Speaker
										WHEN @RecordTypeName ='rC_Event__Campaign_Partner__c_Sponsor' THEN '01261000000UsFNAA0'  --Sponsor
										WHEN @RecordTypeName ='rC_Event__Campaign_Partner__c_Vendor' THEN '01261000000UsFOAA0'  --Vendor
										WHEN @RecordTypeName ='rC_Event__Campaign_Partner__c_Volunteer' THEN '01261000000UsFPAA0'  --Volunteer
										WHEN @RecordTypeName ='rC_Event__Campaign_Ticket__c_Item' THEN '01261000000UsFQAA0'  --Item
										WHEN @RecordTypeName ='rC_Event__Campaign_Ticket__c_Ticket' THEN '01261000000UsFRAA0'  --Ticket
										WHEN @RecordTypeName ='rC_Giving__Deliverable__c_Private' THEN '01261000000UqbXAAS'  --Private
										WHEN @RecordTypeName ='rC_Giving__Deliverable__c_Public' THEN '01261000000UqbYAAS'  --Public
										WHEN @RecordTypeName ='rC_Giving__Summary__c_Annual' THEN '01261000000Uqc8AAC'  --Annual
										WHEN @RecordTypeName ='rC_Giving__Summary__c_Gift_Membership' THEN '01261000000Uqc9AAC'  --Gift_Membership
										WHEN @RecordTypeName ='rC_Giving__Summary__c_Lifetime' THEN '01261000000UqcAAAS'  --Lifetime
										WHEN @RecordTypeName ='rC_Volunteers__Applicant__c_Group' THEN '01261000000UsOYAA0'  --Group
										WHEN @RecordTypeName ='rC_Volunteers__Applicant__c_Individual' THEN '01261000000UsOZAA0'  --Individual
										WHEN @RecordTypeName ='Relationship_Account_Account' THEN '01261000000Uqa1AAC'  --Account_Account
										WHEN @RecordTypeName ='Relationship_Account_Contact' THEN '01261000000Uqa2AAC'  --Account_Contact
										WHEN @RecordTypeName ='Relationship_Account_Giving' THEN '01261000000Uqa3AAC'  --Account_Giving
										WHEN @RecordTypeName ='Relationship_Contact_Contact' THEN '01261000000Uqa4AAC'  --Contact_Contact
										WHEN @RecordTypeName ='Relationship_Contact_Giving' THEN '01261000000Uqa5AAC'  --Contact_Giving
										WHEN @RecordTypeName ='Salutation_Name' THEN '01261000000Uqa6AAC'  --Name
										WHEN @RecordTypeName ='Salutation_Salutation' THEN '01261000000Uqa7AAC'  --Salutation

										END
										
		RETURN @RecordTypeId 
	END
	GO

	 
 
 --PROPER CASE 
 
		CREATE FUNCTION [dbo].[fnc_ProperCase]
		(@Text as varchar(80))
		RETURNS varchar(80) as
		BEGIN

		DECLARE @Reset bit
		DECLARE @Ret varchar(80)
		DECLARE @i int
		DECLARE @c char(1)

		SELECT @Reset = 1, @i=1, @Ret = ''

		WHILE @i <= LEN(@Text)
			SELECT @c= SUBSTRING(@Text,@i,1),
			@Ret = @Ret + CASE WHEN @Reset=1 THEN UPPER(@c) ELSE LOWER(@c) END,
			@Reset= CASE WHEN 
			CASE WHEN SUBSTRING(@Text,@i-1,2) like '[DdOoLl]''' THEN 1 
			WHEN SUBSTRING(@Text,@i-1,3) like '[Mm][cC][a-zA-Z]' THEN 1 
			WHEN SUBSTRING(@Text,@i-2,4) like '[Mm][Aa][cC][a-zA-Z]' THEN 1 
			ELSE 0 
			END = 1 
			THEN 1 
			ELSE CASE WHEN @c like '[a-zA-Z]' or @c in ('''') THEN 0 
			ELSE 1 
			END 
			END,
			@i = @i +1
		RETURN @Ret
		end
		 


--DATE FORMAT to check for date validity on fuzzy dates

			CREATE FUNCTION [dbo].[fn_DateConvert] 
			(
				-- Add the parameters for the function here
				@inDate varchar(15)
			)
			RETURNS DATETIME
			AS
			BEGIN
				DECLARE @OutDate datetime

			SET @OutDate = (SELECT
					CASE
						WHEN ISDATE(@inDate) = 0 AND
							LEN(@inDate) > 8 THEN CONVERT(DATETIME, CONVERT(DATETIME, @inDate, 103), 121)
						WHEN ISDATE(@inDate) = 0 AND
							LEN(@inDate) = 4 THEN CAST('01-01-' + @inDate AS DATETIME)
						WHEN ISDATE(@inDate) = 0 AND
							LEN(@inDate) = 7 AND
							@inDate LIKE '%-%' THEN CAST(@inDate + '-01' AS DATETIME)
						WHEN ISDATE(@inDate) = 0 AND
							LEN(@inDate) = 7 AND
							@inDate LIKE '%/%' THEN CAST(@inDate + '/01' AS DATETIME)
						ELSE CAST(@inDate AS DATETIME)
					END)

				-- Return the result of the function
				RETURN @OutDate
			END