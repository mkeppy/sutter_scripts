

USE SUTTER_MGEA_DATA
GO

BEGIN
	DROP TABLE SUTTER_MGEA_MIGRATION.IMP.CONTACT
END 

BEGIN--CONTACTS

			--CONTACTS: CONSTITUENTS HOUSEHOLD_CONTACTS
			SELECT
			 dbo.fnc_OwnerID() AS OwnerID
			,dbo.fnc_RecordType('Contact_Household') AS RecordTypeID
 		 	,CASE WHEN T.ConsID IS NULL OR T.ConsID ='' THEN '' ELSE T.RE_DB_OwnerShort+'-'+T.ConsID END AS	RE_ConsID__c
			,T.RE_DB_OwnerShort+'-'+T.ImportID AS RE_ImportID__c
			,T.RE_DB_OwnerShort+'-'+T.ImportID AS External_ID__c
			,T.RE_DB_OwnerShort+'-'+T.ImportID AS [ACCOUNT:External_ID__c]
			,NULL AS RE_IRImpID__c
		
			-------------------,NULL AS MDM_ID__c

			,CASE WHEN LEN(T.Bday)=10 THEN T.Bday END AS BirthDate
			,CASE WHEN LEN(T.Bday)=10 THEN MONTH(T.BDay ) WHEN LEN(T.Bday)=7 THEN LEFT(T.Bday,2) WHEN LEN(T.Bday)=4 THEN NULL END AS rC_Bios__Birth_Month__c
			,CASE WHEN LEN(T.Bday)=10 THEN DAY(T.BDay ) END AS rC_Bios__Birth_Day__c
			,CASE WHEN LEN(T.Bday)=10 THEN YEAR(T.BDay )  WHEN LEN(T.Bday)=7 THEN RIGHT(T.Bday,4) WHEN LEN(T.Bday)=4 THEN T.Bday END AS rC_Bios__Birth_Year__c
			
			,T.Deceased	AS	rC_Bios__Deceased__c
			,CASE WHEN LEN(T.DecDate)=10 THEN T.DecDate END AS rC_Bios__Deceased_Date__c
			,CASE WHEN LEN(T.DecDate)=10 THEN MONTH(T.DecDate ) WHEN LEN(T.DecDate)=7 THEN LEFT(T.DecDate,2) WHEN LEN(T.DecDate)=4 THEN NULL END AS rC_Bios__Deceased_Month__c
			,CASE WHEN LEN(T.DecDate)=10 THEN DAY(T.DecDate ) END AS rC_Bios__Deceased_Day__c
			,CASE WHEN LEN(T.DecDate)=10 THEN YEAR(T.DecDate )  WHEN LEN(T.DecDate)=7 THEN RIGHT(T.DecDate,4) WHEN LEN(T.DecDate)=4 THEN T.DecDate END AS rC_Bios__Deceased_Year__c

			,T.Ethnicity	AS	rC_Bios__Ethnicity__c
			,T.FirstName	AS	FirstName
			,T.Gender	AS	rC_Bios__Gender__c
			,T.GivesAnon	AS	Gives_Anonymously__c
			,T.NoValidAddr	AS	No_Valid_Address__c
			,CASE WHEN T.Deceased ='TRUE' THEN 'FALSE' WHEN T.IsInactive ='TRUE' THEN 'FALSE' WHEN T.IsInactive ='FALSE' THEN 'TRUE' END AS	rC_Bios__Active__c
			,T.IsSolicitor	AS	Is_Solicitor__c
			,T.LastName	AS	LastName
			,T.MaidName	AS	rC_Bios__Maiden_Name__c
			,T.MrtlStat	AS	rC_Bios__Marital_Status__c
			,T.MidName	AS	rC_Bios__Middle_Name__c
			,T.Nickname	AS	Nick_Name__c
			,T.NoEmail	AS	HasOptedOutOfEmail
			,T.Suff1	AS	rC_Bios__Suffix__c
			,T.Suff2	AS	Suffix_2__c
			,T.Titl1	AS	Salutation
			,T.Titl2	AS	Title_2__c
			,T.DateAdded	AS	CreatedDate
			-- ,T.RE_DB_OwnerShort	AS	rC_Giving__Primary_Affiliation__c NOTE:formulat field.
			,CASE WHEN T.Deceased='TRUE' THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Preferred_Contact__c
			,'FALSE' AS rC_Bios__Secondary_Contact__c
			
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
			,GETDATE() AS	rC_Giving__Rollup_Soft_Credits__c
			,T4.Do_Not_Phone__c
			,T4.Do_Not_Solicit__c
			,T4.Do_Not_Mail__c
			,T4.Do_Not_Contact__c
			,CASE WHEN T6.HomePhone  IS NOT NULL THEN 'Home' WHEN T6.rC_Bios__Work_Phone__c IS NOT NULL THEN 'Work' WHEN T6.MobilePhone IS NOT NULL THEN 'Mobile'
				END AS rC_Bios__Preferred_Phone__c
			,CASE WHEN T6.rC_Bios__Home_Email__c IS NOT NULL THEN 'Home' WHEN T6.rC_Bios__Other_Email__c IS NOT NULL THEN 'Other' 
				END AS rC_Bios__Preferred_Email__c
			,T6.Fax__c
			,T6.HomePhone
			,T6.MobilePhone
			,T6.Pager__c
			,T6.rC_Bios__Home_Email__c
			,T6.rC_Bios__Other_Email__c
			,T6.rC_Bios__Work_Phone__c
			,T6.Website__c

			,T6.rC_Bios__Home_Do_Not_Call__c
			,T6.rC_Bios__Mobile_Do_Not_Call__c
			,T6.rC_Bios__Work_Do_Not_Call__c
			--,T6.rC_Bios__Other_Do_Not_Call__c  data not available

			,CAST(T7.Additional_Phone_Numbers__c AS NVARCHAR(4000)) AS Additional_Phone_Numbers__c
			
			,CAST(T8.Agefinder_Birthdate__c AS NVARCHAR(255)) AS Agefinder_Birthdate__c
			,CAST(T8.CMS_Age__c  AS NVARCHAR(255)) AS CMS_Age__c
			,CAST(T8.PhoneFinder__c  AS NVARCHAR(255)) AS PhoneFinder__c
			
			,CASE WHEN T9.AddrLine1 IS NULL THEN NULL WHEN T9.AddrLine2!='' THEN T9.AddrLine1 +' '+T9.AddrLine2 ELSE T9.AddrLine1 END AS MailingStreet
			,T9.AddrCity AS MailingCity
			,T9.AddrState AS MailingState
			,T9.AddrZIP AS MailingPostalCode
			,T9.AddrCountry MailingCountry
			
			--reference only
			,'Constituent Ind HH' AS zrefSource
			,T.KeyInd AS zrefKeyInd
			,T.ConsID AS zrefConsID
			,T.ImportID AS zrefImportID
			,T.RE_DB_OwnerShort AS zrefDBOwner
			,T2.IRLink	AS zrefIRLink
			,T2.HH_ImportID AS zrefHH_ImportID 
			,T2.HH_ConsID AS zrefHH_ConsID
			,T2.NoHH_ImportID AS zrefNoHH_ImportID
			,T2.NoHH_ConsId AS zrefNoHH_ConsId
			,T2.IRIsSpouse AS zrefIsSpouse

			INTO SUTTER_MGEA_MIGRATION.IMP.CONTACT 
			FROM SUTTER_MGEA_DATA.dbo.HC_Constituents_v T	
			INNER JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH T2 ON T.ImportID=T2.HH_ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.SolicitCodes T4 ON T.ImportID=T4.ImportID AND T.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Phones_seq_1_final T6 ON T.ImportID=T6.ImportID AND T.RE_DB_OwnerShort=T6.RE_DB_OwnerShort AND T.KeyInd=T6.KeyInd
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Phones_seq_2_final T7 ON T.ImportID=T7.ImportID AND T.RE_DB_OwnerShort=T7.RE_DB_OwnerShort AND T.KeyInd=T7.KeyInd
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Attributes_to_Contact T8 ON T.ImportID=T8.ImportID AND T.RE_DB_OwnerShort=T8.RE_DB_OwnerShort  
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Address_Pref	T9 ON T.RE_DB_OwnerShort=T9.RE_DB_OwnerShort AND T.ImportID=T9.ImportID
	
		UNION ALL 
		
			--CONTACTS: CONSTITUENTS NON_HOUSEHOLD_CONTACT
			SELECT
			 dbo.fnc_OwnerID() AS OwnerID
			,dbo.fnc_RecordType('Contact_Household') AS RecordTypeID
 		 	,CASE WHEN T.ConsID IS NULL OR T.ConsID ='' THEN '' ELSE T.RE_DB_OwnerShort+'-'+T.ConsID END AS	RE_ConsID__c
			,T.RE_DB_OwnerShort+'-'+T.ImportID AS RE_ImportID__c
			,T.RE_DB_OwnerShort+'-'+T.ImportID AS External_ID__c
			,T2.RE_DB_OwnerShort+'-'+T2.HH_ImportID AS [ACCOUNT:External_ID__c]
			,NULL AS RE_IRImpID__c			
		
			-------------------,NULL AS MDM_ID__c


			,CASE WHEN LEN(T.Bday)=10 THEN T.Bday END AS BirthDate
			,CASE WHEN LEN(T.Bday)=10 THEN MONTH(T.BDay ) WHEN LEN(T.Bday)=7 THEN LEFT(T.Bday,2) WHEN LEN(T.Bday)=4 THEN NULL END AS rC_Bios__Birth_Month__c
			,CASE WHEN LEN(T.Bday)=10 THEN DAY(T.BDay ) END AS rC_Bios__Birth_Day__c
			,CASE WHEN LEN(T.Bday)=10 THEN YEAR(T.BDay )  WHEN LEN(T.Bday)=7 THEN RIGHT(T.Bday,4) WHEN LEN(T.Bday)=4 THEN T.Bday END AS rC_Bios__Birth_Year__c
			
			,T.Deceased	AS	rC_Bios__Deceased__c
			,CASE WHEN LEN(T.DecDate)=10 THEN T.DecDate END AS rC_Bios__Deceased_Date__c
			,CASE WHEN LEN(T.DecDate)=10 THEN MONTH(T.DecDate ) WHEN LEN(T.DecDate)=7 THEN LEFT(T.DecDate,2) WHEN LEN(T.DecDate)=4 THEN NULL END AS rC_Bios__Deceased_Month__c
			,CASE WHEN LEN(T.DecDate)=10 THEN DAY(T.DecDate ) END AS rC_Bios__Deceased_Day__c
			,CASE WHEN LEN(T.DecDate)=10 THEN YEAR(T.DecDate )  WHEN LEN(T.DecDate)=7 THEN RIGHT(T.DecDate,4) WHEN LEN(T.DecDate)=4 THEN T.DecDate END AS rC_Bios__Deceased_Year__c

			,T.Ethnicity	AS	rC_Bios__Ethnicity__c
			,T.FirstName	AS	FirstName
			,T.Gender	AS	rC_Bios__Gender__c
			,T.GivesAnon	AS	Gives_Anonymously__c
			,T.NoValidAddr	AS	No_Valid_Address__c
			,CASE WHEN T.Deceased ='TRUE' THEN 'FALSE' WHEN T.IsInactive ='TRUE' THEN 'FALSE' WHEN T.IsInactive ='FALSE' THEN 'TRUE' END AS	rC_Bios__Active__c
			,T.IsSolicitor	AS	Is_Solicitor__c
			,T.LastName	AS	LastName
			,T.MaidName	AS	rC_Bios__Maiden_Name__c
			,T.MrtlStat	AS	rC_Bios__Marital_Status__c
			,T.MidName	AS	rC_Bios__Middle_Name__c
			,T.Nickname	AS	Nick_Name__c
			,T.NoEmail	AS	HasOptedOutOfEmail
			,T.Suff1	AS	rC_Bios__Suffix__c
			,T.Suff2	AS	Suffix_2__c
			,T.Titl1	AS	Salutation
			,T.Titl2	AS	Title_2__c
			,T.DateAdded	AS	CreatedDate
			-- ,T.RE_DB_OwnerShort	AS	rC_Giving__Primary_Affiliation__c NOTE:formulat field.
			,'FALSE' AS rC_Bios__Preferred_Contact__c
			,CASE WHEN T.Deceased='TRUE' THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Secondary_Contact__c
			
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
			,GETDATE() AS	rC_Giving__Rollup_Soft_Credits__c
		
			,T4.Do_Not_Phone__c
			,T4.Do_Not_Solicit__c
			,T4.Do_Not_Mail__c
			,T4.Do_Not_Contact__c
					
			,CASE WHEN T6.HomePhone  IS NOT NULL THEN 'Home' WHEN T6.rC_Bios__Work_Phone__c IS NOT NULL THEN 'Work' WHEN T6.MobilePhone IS NOT NULL THEN 'Mobile'
				END AS rC_Bios__Preferred_Phone__c
			,CASE WHEN T6.rC_Bios__Home_Email__c IS NOT NULL THEN 'Home' WHEN T6.rC_Bios__Other_Email__c IS NOT NULL THEN 'Other' 
				END AS rC_Bios__Preferred_Email__c

			,T6.Fax__c
			,T6.HomePhone
			,T6.MobilePhone
			,T6.Pager__c
			,T6.rC_Bios__Home_Email__c
			,T6.rC_Bios__Other_Email__c
			,T6.rC_Bios__Work_Phone__c
			,T6.Website__c

			,T6.rC_Bios__Home_Do_Not_Call__c
			,T6.rC_Bios__Mobile_Do_Not_Call__c
			,T6.rC_Bios__Work_Do_Not_Call__c
			--,T6.rC_Bios__Other_Do_Not_Call__c  data not available

			,CAST(T7.Additional_Phone_Numbers__c AS NVARCHAR(4000)) AS Additional_Phone_Numbers__c
			,CAST(T8.Agefinder_Birthdate__c AS NVARCHAR(255)) AS Agefinder_Birthdate__c
			,CAST(T8.CMS_Age__c  AS NVARCHAR(255)) AS CMS_Age__c
			,CAST(T8.PhoneFinder__c  AS NVARCHAR(255)) AS PhoneFinder__c
			,CASE WHEN T9.AddrLine1 IS NULL THEN NULL WHEN T9.AddrLine2!='' THEN T9.AddrLine1 +' '+T9.AddrLine2 ELSE T9.AddrLine1 END AS MailingStreet
			,T9.AddrCity AS MailingCity
			,T9.AddrState AS MailingState
			,T9.AddrZIP AS MailingPostalCode
			,T9.AddrCountry MailingCountry

			--reference only
			,'Constituent Ind NoHH' AS zrefSource
			,T.KeyInd AS zrefKeyInd
			,T.ConsID AS zrefConsID
			,T.ImportID AS zrefImportID
			,T.RE_DB_OwnerShort AS zrefDBOwner
			,T2.IRLink	AS zrefIRLink
			,T2.HH_ImportID AS zrefHH_ImportID 
			,T2.HH_ConsID AS zrefHH_ConsID
			,T2.NoHH_ImportID AS zrefNoHH_ImportID
			,T2.NoHH_ConsId AS zrefNoHH_ConsId
			,T2.IRIsSpouse AS zrefIsSpouse

			FROM SUTTER_MGEA_DATA.dbo.HC_Constituents_v T	
			INNER JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH T2 ON T.ImportID=T2.NoHH_ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.SolicitCodes T4 ON T.ImportID=T4.ImportID AND T.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Phones_seq_1_final T6 ON T.ImportID=T6.ImportID AND T.RE_DB_OwnerShort=T6.RE_DB_OwnerShort AND T.KeyInd=T6.KeyInd
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Phones_seq_2_final T7 ON T.ImportID=T7.ImportID AND T.RE_DB_OwnerShort=T7.RE_DB_OwnerShort AND T.KeyInd=T7.KeyInd
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Attributes_to_Contact T8 ON T.ImportID=T8.ImportID AND T.RE_DB_OwnerShort=T8.RE_DB_OwnerShort  
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Address_Pref	T9 ON T.RE_DB_OwnerShort=T9.RE_DB_OwnerShort AND T.ImportID=T9.ImportID

		UNION ALL	
		
			--CONTACTS: NON_CONSTITUENT INDIVIDUAL RELATIONSHIPS
			SELECT
			dbo.fnc_OwnerID() AS OwnerID
			,CASE WHEN T8.KeyInd='I' then dbo.fnc_RecordType('Contact_Household') WHEN T8.KeyInd='O' THEN dbo.fnc_RecordType('Contact_Organizational') END AS RecordTypeID
			,NULL AS RE_ConsID__c
			,NULL AS RE_ImportID__c
			,T.RE_DB_OwnerShort+'-'+T.IRImpID AS External_ID__c
			,CASE WHEN T2.HH_ImportID IS NOT NULL THEN T2.RE_DB_OwnerShort+'-'+T2.HH_ImportID ELSE T.RE_DB_OwnerShort+'-'+T.ImportID END AS [ACCOUNT:External_ID__c]
			,T.RE_DB_OwnerShort+'-'+T.IRImpID AS RE_IRImpID__c
		
			-------------------,NULL AS MDM_ID__c


			,CASE WHEN LEN(T.IRBDay)=10 THEN T.IRBDay END AS BirthDate
			,CASE WHEN LEN(T.IRBDay)=10 THEN MONTH(T.IRBDay ) WHEN LEN(T.IRBDay)=7 THEN LEFT(T.IRBDay,2) WHEN LEN(T.IRBDay)=4 THEN NULL END AS rC_Bios__Birth_Month__c
			,CASE WHEN LEN(T.IRBDay)=10 THEN DAY(T.IRBDay ) END AS rC_Bios__Birth_Day__c
			,CASE WHEN LEN(T.IRBDay)=10 THEN YEAR(T.IRBDay )  WHEN LEN(T.IRBDay)=7 THEN RIGHT(T.IRBDay,4) WHEN LEN(T.IRBDay)=4 THEN T.IRBDay END AS rC_Bios__Birth_Year__c
			
			,T.IRDeceased	AS	rC_Bios__Deceased__c
			,CASE WHEN LEN(T.IRDecDate)=10 THEN T.IRDecDate END AS rC_Bios__Deceased_Date__c
			,CASE WHEN LEN(T.IRDecDate)=10 THEN MONTH(T.IRDecDate ) WHEN LEN(T.IRDecDate)=7 THEN LEFT(T.IRDecDate,2) WHEN LEN(T.IRDecDate)=4 THEN NULL END AS rC_Bios__Deceased_Month__c
			,CASE WHEN LEN(T.IRDecDate)=10 THEN DAY(T.IRDecDate ) END AS rC_Bios__Deceased_Day__c
			,CASE WHEN LEN(T.IRDecDate)=10 THEN YEAR(T.IRDecDate )  WHEN LEN(T.IRDecDate)=7 THEN RIGHT(T.IRDecDate,4) WHEN LEN(T.IRDecDate)=4 THEN T.IRDecDate END AS rC_Bios__Deceased_Year__c

			,NULL AS rC_Bios__Ethnicity__c
			,T.IRFirstName	AS	FirstName
			,T.IRGender AS rC_Bios__Gender__c
			,NULL AS Gives_Anonymously__c
			,NULL AS No_Valid_Address__c
			,CASE WHEN T.IRDeceased='TRUE' THEN 'FALSE' ELSE 'TRUE' END AS	rC_Bios__Active__c
			,NULL AS Is_Solicitor__c
			,T.IRLastName	AS	LastName
			,T.IRMaidName	AS	rC_Bios__Maiden_Name__c
			,NULL AS rC_Bios__Marital_Status__c
			,T.IRMidName	AS	rC_Bios__Middle_Name__c
			,T.IRNickname	AS	Nick_Name__c
			,NULL AS HasOptedOutOfEmail
			,T.IRSuff1	AS	rC_Bios__Suffix__c
			,T.IRSuff2	AS	Suffix_2__c
			,T.IRTitl1	AS	Salutation
			,T.IRTitl2	AS	Title_2__c
			,NULL AS CreatedDate
			-- ,T.RE_DB_OwnerShort	AS	rC_Giving__Primary_Affiliation__c NOTE:formulat field.
			,'FALSE' AS	rC_Bios__Preferred_Contact__c
			,T.IRIsSpouse AS rC_Bios__Secondary_Contact__c
	
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
			,GETDATE() AS	rC_Giving__Rollup_Soft_Credits__c
	
			,NULL AS Do_Not_Phone__c
			,NULL AS Do_Not_Solicit__c
			,NULL AS Do_Not_Mail__c
			,NULL AS Do_Not_Contact__c
					
			,CASE WHEN T6.HomePhone  IS NOT NULL THEN 'Home' WHEN T6.rC_Bios__Work_Phone__c IS NOT NULL THEN 'Work' WHEN T6.MobilePhone IS NOT NULL THEN 'Mobile'
				END AS rC_Bios__Preferred_Phone__c
			,CASE WHEN T6.rC_Bios__Home_Email__c IS NOT NULL THEN 'Home' WHEN T6.rC_Bios__Other_Email__c IS NOT NULL THEN 'Other' 
				END AS rC_Bios__Preferred_Email__c
			
			,T6.Fax__c
 			,T6.HomePhone
			,T6.MobilePhone
			,T6.Pager__c 
			,T6.rC_Bios__Home_Email__c
			,T6.rC_Bios__Other_Email__c
			,T6.rC_Bios__Work_Phone__c
			,T6.Website__c

			,NULL AS rC_Bios__Home_Do_Not_Call__c
			,NULL AS rC_Bios__Mobile_Do_Not_Call__c
			,NULL AS rC_Bios__Work_Do_Not_Call__c
			--,T6.rC_Bios__Other_Do_Not_Call__c  data not available

			,CAST(T7.Additional_Phone_Numbers__c AS NVARCHAR(4000)) AS Additional_Phone_Numbers__c
			
			,NULL AS Agefinder_Birthdate__c
			,NULL AS CMS_Age__c
			,NULL AS PhoneFinder__c
			,CASE WHEN T.IRAddrLine1 IS NULL THEN NULL WHEN T.IRAddrLine2!='' THEN T.IRAddrLine1 +' '+T.IRAddrLine2 ELSE T.IRAddrLine1 END AS MailingStreet
			,T.IRAddrCity AS MailingCity
			,T.IRAddrState AS MailingState
			,T.IRAddrZIP AS MailingPostalCode
			,T.IRAddrCountry MailingCountry
		
			--reference 
			,'IndRel NonCons' AS zrefSource
			,NULL  AS zrefKeyInd
			,T.ConsID AS zrefConsID
			,T.ImportID AS zrefImportID			
			,T.RE_DB_OwnerShort zrefDBOwner
			,T2.IRLink	AS zrefIRLink
			,T2.HH_ImportID AS zrefHH_ImportID 
			,T2.HH_ConsID AS zrefHH_ConsID
			,T2.NoHH_ImportID AS zrefNoHH_ImportID
			,T2.NoHH_ConsId AS zrefNoHH_ConsId
			,T2.IRIsSpouse AS zrefIsSpouse
			
			FROM SUTTER_MGEA_DATA.dbo.HC_Ind_Relat_v T
			INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Constituents_v T8 ON T.ImportID=T8.ImportID AND T.RE_DB_OwnerShort=T8.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH T2 ON T.ImportID=T2.NoHH_ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.IRPhones_seq_1_final T6 ON T.IRImpID=T6.IRPhoneIRImpID AND T.RE_DB_OwnerShort=T6.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.IRPhones_seq_2_final T7 ON T.IRImpID=T7.IRPhoneIRImpID AND T.RE_DB_OwnerShort=T7.RE_DB_OwnerShort

			WHERE (T.IRLink='' OR T.IRlink IS NULL)  


END;


	
BEGIN --REPLACE DOUBLE QUOTES TO SINGLE QUOTES
  
  --  SELECT  [FirstName] FROM [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] WHERE [FirstName] LIKE '%"%';
  --  SELECT  [rC_Bios__Middle_Name__c] FROM [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] WHERE [rC_Bios__Middle_Name__c] LIKE '%"%';
  --  SELECT  [LastName] FROM [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] WHERE [LastName] LIKE '%"%';
  --  SELECT  [Nick_Name__c] FROM [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] WHERE [Nick_Name__c] LIKE '%"%';
  --  SELECT  [rC_Bios__Maiden_Name__c] FROM [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] WHERE [rC_Bios__Maiden_Name__c] LIKE '%"%';
  -- SELECT  [Salutation] FROM [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] WHERE [Salutation] LIKE '%"%';
  --  SELECT  [Additional_Phone_Numbers__c] FROM [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] WHERE [Additional_Phone_Numbers__c] LIKE '%"%';
        
    UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] SET [FirstName]=REPLACE([FirstName],'"','''') where [FirstName] like '%"%'
    UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] SET [rC_Bios__Middle_Name__c]=REPLACE([rC_Bios__Middle_Name__c],'"','''') where [rC_Bios__Middle_Name__c] like '%"%'
    UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] SET [LastName]=REPLACE([LastName],'"','''') where [LastName] like '%"%'
    UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] SET [Nick_Name__c]=REPLACE([Nick_Name__c],'"','''') where [Nick_Name__c] like '%"%'
    UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] SET [rC_Bios__Maiden_Name__c]=REPLACE([rC_Bios__Maiden_Name__c],'"','''') where [rC_Bios__Maiden_Name__c] like '%"%'
    UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] SET [Salutation]=REPLACE([Salutation],'"','''') where [Salutation] like '%"%'
    UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] SET [Additional_Phone_Numbers__c]=REPLACE([Additional_Phone_Numbers__c],'"','''') where [Additional_Phone_Numbers__c] like '%"%'
    UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] SET [HomePhone]=REPLACE([HomePhone],'"','''') where [HomePhone] like '%"%'
    UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] SET [rC_Bios__Other_Email__c]=REPLACE([rC_Bios__Other_Email__c],'"','''') where [rC_Bios__Other_Email__c] like '%"%'
    UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] SET [LastName]='Unknown' where [LastName] IS NULL 
    UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] SET [MailingStreet]=REPLACE([MailingStreet],'"','''') where [MailingStreet] like '%"%'
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] SET [MailingCity]=REPLACE([MailingCity],'"','''') where [MailingCity] like '%"%'
	
	
END

--Update Deceased Year to Unknown.
BEGIN
	SELECT rC_Bios__Deceased__c, rC_Bios__Deceased_Date__c, rC_Bios__Deceased_Year__c 
	FROM SUTTER_MGEA_MIGRATION.IMP.CONTACT
	WHERE rC_Bios__Deceased__c='TRUE' AND rC_Bios__Deceased_Date__c IS null
		 
	ALTER TABLE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT]
	ALTER COLUMN rC_Bios__Deceased_Year__c NVARCHAR(20)

	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT]
	SET rC_Bios__Deceased_Year__c='Unknown'
	WHERE rC_Bios__Deceased__c='TRUE' AND rC_Bios__Deceased_Date__c IS NULL

END


BEGIN--check duplicates 
	SELECT * FROM [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] 
	WHERE External_ID__c IN (SELECT External_ID__c FROM [SUTTER_MGEA_MIGRATION].[IMP].[CONTACT] GROUP BY External_ID__c HAVING COUNT(*)>1)
	ORDER BY External_ID__c
END 



SELECT External_ID__c, 
rC_Bios__Preferred_Contact__c, rC_Bios__Secondary_Contact__c, zrefSource , RE_ImportID__c AS refImportID, RE_ConsID__c AS refConsId
FROM SUTTER_MGEA_MIGRATION.imp.contact
WHERE zrefSource LIKE '%constit%' 
 