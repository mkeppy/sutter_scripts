USE SUTTER_MGEA_DATA
GO

 
BEGIN
		DROP TABLE SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP
END
	

BEGIN 

			
			--INDIVIDUAL RELATIONSHIPS
			SELECT DISTINCT
			 OwnerID=dbo.fnc_OwnerID()
			,RecordTypeID=CASE T1.KeyInd WHEN 'I' THEN dbo.fnc_RecordType('Relationship_Contact_Contact')
							WHEN 'O' THEN dbo.fnc_RecordType('Relationship_Account_Contact') END
			,External_ID__c=T.RE_DB_OwnerShort+'-'+T.IRImpID
			,RE_IRImpID__c=T.RE_DB_OwnerShort+'-'+T.IRImpID				
			,RE_ORImpID__c=NULL 
			,RE_ASRImpID__c=NULL
			,RE_ESRImpID__c=NULL 
			,[rC_Bios__Account_1__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN (T1.RE_DB_OwnerShort+'-'+T1.ImportID) END
			,[rC_Bios__Contact_1__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN (T1.RE_DB_OwnerShort+'-'+T1.ImportID) END 
			,[rC_Bios__Account_2__r:External_ID__c]=NULL
			,[rC_Bios__Contact_2__r:External_ID__c]=CASE WHEN T.IRLink!=''  THEN (T2.RE_DB_OwnerShort+'-'+T2.ImportID) 
										WHEN T.IRLink=''   THEN (T.RE_DB_OwnerShort+'-'+T.IRImpID) END
			
			,rC_Bios__Category__c=[dbo].[fnc_ProperCase](T3.NGOC_CATEGORY)
			,rC_Bios__Role_1__c=[dbo].[fnc_ProperCase](T3.Role_1)
			,rC_Bios__Role_2__c=[dbo].[fnc_ProperCase](T3.Role_2)

			,rC_Bios__Starting_Month__c=CASE WHEN LEN(T.IRFromDate)>7 THEN MONTH(T.IRFromDate) WHEN LEN(T.IRFromDate)=7 THEN LEFT(T.IRFromDate,2) WHEN LEN(T.IRFromDate)=4 THEN NULL END
			,rC_Bios__Starting_Day__c=CASE WHEN LEN(T.IRFromDate)>7 THEN DAY(T.IRFromDate) END
			,rC_Bios__Starting_Year__c=CASE WHEN LEN(T.IRFromDate)>7 THEN YEAR(T.IRFromDate)  WHEN LEN(T.IRFromDate)=7 THEN RIGHT(T.IRFromDate,4) WHEN LEN(T.IRFromDate)=4 THEN T.IRFromDate END
			
			,rC_Bios__Stopping_Month__c=CASE WHEN LEN(T.IRToDate)>7 THEN MONTH(T.IRToDate) WHEN LEN(T.IRToDate)=7 THEN LEFT(T.IRToDate,2) WHEN LEN(T.IRToDate)=4 THEN NULL END
			,rC_Bios__Stopping_Day__c=CASE WHEN LEN(T.IRToDate)>7 THEN DAY(T.IRToDate) END
			,rC_Bios__Stopping_Year__c=CASE WHEN LEN(T.IRToDate)>7 THEN YEAR(T.IRToDate)  WHEN LEN(T.IRToDate)=7 THEN RIGHT(T.IRToDate,4) WHEN LEN(T.IRToDate)=4 THEN T.IRToDate END
		    
			,CASE WHEN [dbo].[fn_DateConvert](T.IRToDate) < GETDATE() THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Active__c
			
			,rC_Bios__Comments__c=CAST(T.IRNotes AS VARCHAR(MAX)) 

			,Education_Relationship_Type__c=NULL
			,Education_Major__c=NULL
			,Education_Minor__c=NULL

			,Affiliation__c=T.RE_DB_OwnerShort
	 
			--ref
			,T1.KeyInd AS zrefT1_KeyInd, T1.ImportID AS zrefT1_ImportId
			,T2.KeyInd AS zrefT2_KeyInd, T2.ImportID AS zrefT2_ImportId
			,T.IRImpID AS zrefRImpID, T.IRLink AS zref_RLink, T.RE_DB_OwnerShort AS zrefRE_DB_OwnerShort, T.RE_DB_Tbl AS zrefRE_DB_Tbl
			,T.IRFromDate AS zrefDateFrom, T.IRToDate AS zrefDateTo
			,T.IRRelat AS zrefRelat, T.IRRecip AS zrefRecip
			,'Ind_Relat' AS zrefSource
			
			INTO SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP
			FROM SUTTER_MGEA_DATA.dbo.HC_Ind_Relat_v T
			INNER JOIN SUTTER_MGEA_MIGRATION.TBL.RELATIONSHIP_IND_FINAL T0 ON T.IRImpID=T0.IRImpID AND T.RE_DB_OwnerShort=T0.RE_DB_OwnerShort
			INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T2 ON T.IRLink=T2.ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_RelatRecipType T3 ON T.IRRelat=T3.RELATIONSHIP AND T.IRRecip=T3.RECIPROCAL 
																	AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort 
																	AND T1.KeyInd=T3.KeyInd
			WHERE T3.RelationshipIndicator='Individual'  
 
		UNION ALL
		
			--ORGANIZATION RELATIONSHIPS
			--code reverts order when first constituent is Contact and second is Account. NGOC accepts Account-Contact only, not Contact-Account. 
			SELECT DISTINCT
			 OwnerID=dbo.fnc_OwnerID()
			,RecordTypeID=CASE T1.KeyInd WHEN 'I' THEN dbo.fnc_RecordType('Relationship_Account_Contact')
							WHEN 'O' THEN dbo.fnc_RecordType('Relationship_Account_Account') END
			,External_ID__c=T.RE_DB_OwnerShort+'-'+T.ORImpID
			,RE_IRImpID__c=NULL
			,RE_ORImpID__c=T.RE_DB_OwnerShort+'-'+T.ORImpID				
			,RE_ASRImpID__c=NULL
			,RE_ESRImpID__c=NULL 
			,[rC_Bios__Account_1__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN (T1.RE_DB_OwnerShort+'-'+T1.ImportID) 
									    WHEN (T1.KeyInd='I' AND T.ORLink!='')  THEN (T2.RE_DB_OwnerShort+'-'+T2.ImportID) 
										WHEN (T1.KeyInd='I' AND T.ORLink='' )  THEN (T.RE_DB_OwnerShort+'-'+T.ORImpID) END
			
			,[rC_Bios__Contact_1__r:External_ID__c]=NULL 
			
			,[rC_Bios__Account_2__r:External_ID__c]=CASE WHEN (T1.KeyInd='O' AND T.ORLink!='')  THEN (T2.RE_DB_OwnerShort+'-'+T2.ImportID) 
										WHEN (T1.KeyInd='O' AND T.ORLink='') THEN (T.RE_DB_OwnerShort+'-'+T.ORImpID) END
			,[rC_Bios__Contact_2__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN (T1.RE_DB_OwnerShort+'-'+T1.ImportID) END  
			
			,rC_Bios__Category__c=[dbo].[fnc_ProperCase](T3.NGOC_CATEGORY)
			,rC_Bios__Role_1__c=[dbo].[fnc_ProperCase](T3.Role_1)
			,rC_Bios__Role_2__c=[dbo].[fnc_ProperCase](T3.Role_2)

			,rC_Bios__Starting_Month__c=CASE WHEN LEN(T.ORFromDate)>7 THEN MONTH(T.ORFromDate) WHEN LEN(T.ORFromDate)=7 THEN LEFT(T.ORFromDate,2) WHEN LEN(T.ORFromDate)=4 THEN NULL END
			,rC_Bios__Starting_Day__c=CASE WHEN LEN(T.ORFromDate)>7 THEN DAY(T.ORFromDate) END
			,rC_Bios__Starting_Year__c=CASE WHEN LEN(T.ORFromDate)>7 THEN YEAR(T.ORFromDate)  WHEN LEN(T.ORFromDate)=7 THEN RIGHT(T.ORFromDate,4) WHEN LEN(T.ORFromDate)=4 THEN T.ORFromDate END
			
			,rC_Bios__Stopping_Month__c=CASE WHEN LEN(T.ORToDate)>7 THEN MONTH(T.ORToDate) WHEN LEN(T.ORToDate)=7 THEN LEFT(T.ORToDate,2) WHEN LEN(T.ORToDate)=4 THEN NULL END
			,rC_Bios__Stopping_Day__c=CASE WHEN LEN(T.ORToDate)>7 THEN DAY(T.ORToDate) END
			,rC_Bios__Stopping_Year__c=CASE WHEN LEN(T.ORToDate)>7 THEN YEAR(T.ORToDate)  WHEN LEN(T.ORToDate)=7 THEN RIGHT(T.ORToDate,4) WHEN LEN(T.ORToDate)=4 THEN T.ORToDate END
			
			,CASE WHEN [dbo].[fn_DateConvert](T.ORToDate) < GETDATE() THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Active__c
						
			,rC_Bios__Comments__c=CAST(T.ORNotes AS VARCHAR(MAX)) 

			,Education_Relationship_Type__c=NULL
			,Education_Major__c=NULL
			,Education_Minor__c=NULL

			,Affiliation__c=T.RE_DB_OwnerShort
	 
			--ref
			,T1.KeyInd AS zrefT1_KeyInd, T1.ImportID AS zrefT1_ImportId
			,T2.KeyInd AS zrefT2_KeyInd, T2.ImportID AS zrefT2_ImportId
			,T.ORImpID AS zrefRImpID, T.ORLink AS zref_RLink, T.RE_DB_OwnerShort AS zrefRE_DB_OwnerShort, T.RE_DB_Tbl AS zrefRE_DB_Tbl
			,T.ORFromDate AS zrefDateFrom, T.ORToDate AS zrefDateTo
			,T.ORRelat AS zrefRelat, T.ORRecip AS zrefRecip
			,'Org_Relat' AS zrefSource			
			FROM SUTTER_MGEA_DATA.dbo.HC_Org_Relat_v T
			INNER JOIN SUTTER_MGEA_MIGRATION.TBL.RELATIONSHIP_ORG_FINAL T0 ON T.ORImpID=T0.ORImpID AND T.RE_DB_OwnerShort=T0.RE_DB_OwnerShort
			INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T2 ON T.ORLink=T2.ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_RelatRecipType T3 ON T.ORRelat=T3.RELATIONSHIP AND T.ORRecip=T3.RECIPROCAL 
																	AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort 
																	AND T1.KeyInd=T3.KeyInd
			WHERE T3.RelationshipIndicator='Organization'  
			
					--12,455 SELECT COUNT(*) FROM SUTTER_MGEA_DATA.dbo.HC_Org_Relat_v
					
		UNION ALL
		
		/*
			--CONSTITUENT SOLICITORS-- WILL NOT BE CONVERTED AS RELATIONSHIPS
			--  LEAVE THE CODE FOR FUTURE MIGRATIONS FROM RE TO SALESFORCE	
					/*
					SELECT T1.KeyInd T1Key, T2.KeyInd T2Key, COUNT(*) 
					FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Solicitor_v T
					INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
					INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T2 ON T.ASRLink=T2.ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
					GROUP BY T1.KeyInd, T2.KeyInd
					ORDER BY T1.KeyInd, T2.KeyInd
					
					T1Key	T2Key	(No column name)
					I	I	3380	=contact_1-contact_2
					I	O	9		=account_1-contact_2 (was contact-account but needed to change to comply with ngoc)
					O	I	733		=account_1-contact_2
					O	O	2		=account_1-account_2
					*/	
		
			SELECT DISTINCT
			 OwnerID=dbo.fnc_OwnerID()
			,RecordTypeID=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN dbo.fnc_RecordType('Relationship_Contact_Contact')
								WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN dbo.fnc_RecordType('Relationship_Account_Contact')  --need to revert from cont-acct TO acct-contac
								WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN dbo.fnc_RecordType('Relationship_Account_Contact')
								WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN dbo.fnc_RecordType('Relationship_Account_Account') END 
			,External_ID__c=T.RE_DB_OwnerShort+'-'+T.ASRImpID
			,RE_IRImpID__c=NULL
			,RE_ORImpID__c=NULL
			,RE_ASRImpID__c= T.RE_DB_OwnerShort+'-'+T.ASRImpID
			,RE_ESRImpID__c=NULL 
			,[rC_Bios__Account_1__r:External_ID__c]=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN NULL
										WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN (T2.RE_DB_OwnerShort+'-'+T2.ImportID)  --need to revert from cont-acct TO acct-contac
										WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN (T1.RE_DB_OwnerShort+'-'+T1.ImportID) 
										WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN (T1.RE_DB_OwnerShort+'-'+T1.ImportID) END 
			,[rC_Bios__Contact_1__r:External_ID__c]=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN (T1.RE_DB_OwnerShort+'-'+T1.ImportID) 
										WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN NULL
										WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN NULL
										WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN NULL END 
			,[rC_Bios__Account_2__r:External_ID__c]=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN NULL
										WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN NULL
										WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN NULL
										WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN (T2.RE_DB_OwnerShort+'-'+T2.ImportID) END 
			,[rC_Bios__Contact_2__r:External_ID__c]=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN (T2.RE_DB_OwnerShort+'-'+T2.ImportID)
										WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN (T1.RE_DB_OwnerShort+'-'+T1.ImportID) --need to revert from cont-acct TO acct-contac
										WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN (T2.RE_DB_OwnerShort+'-'+T2.ImportID)
										WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN NULL END 
			
			,rC_Bios__Category__c='Solicitor'
			,rC_Bios__Role_1__c=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN 'Solicitee' ELSE T.ASRType END  --need to revert from cont-acct TO acct-contac
			,rC_Bios__Role_2__c=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN T.ASRType ELSE 'Solicitee' END 
			
			,rC_Bios__Starting_Month__c=CASE WHEN LEN(T.ASRDateFrom)>7 THEN MONTH(T.ASRDateFrom) WHEN LEN(T.ASRDateFrom)=7 THEN LEFT(T.ASRDateFrom,2) WHEN LEN(T.ASRDateFrom)=4 THEN NULL END
			,rC_Bios__Starting_Day__c=CASE WHEN LEN(T.ASRDateFrom)>7 THEN DAY(T.ASRDateFrom) END
			,rC_Bios__Starting_Year__c=CASE WHEN LEN(T.ASRDateFrom)>7 THEN YEAR(T.ASRDateFrom)  WHEN LEN(T.ASRDateFrom)=7 THEN RIGHT(T.ASRDateFrom,4) WHEN LEN(T.ASRDateFrom)=4 THEN T.ASRDateFrom END
			,rC_Bios__Stopping_Month__c=CASE WHEN LEN(T.ASRDateTo)>7 THEN MONTH(T.ASRDateTo) WHEN LEN(T.ASRDateTo)=7 THEN LEFT(T.ASRDateTo,2) WHEN LEN(T.ASRDateTo)=4 THEN NULL END
			,rC_Bios__Stopping_Day__c=CASE WHEN LEN(T.ASRDateTo)>7 THEN DAY(T.ASRDateTo) END
			,rC_Bios__Stopping_Year__c=CASE WHEN LEN(T.ASRDateTo)>7 THEN YEAR(T.ASRDateTo)  WHEN LEN(T.ASRDateTo)=7 THEN RIGHT(T.ASRDateTo,4) WHEN LEN(T.ASRDateTo)=4 THEN T.ASRDateTo END
			,CASE WHEN [dbo].[fn_DateConvert](T.ASRDateTo) < GETDATE() THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Active__c
			,rC_Bios__Comments__c=CAST(T.ASRNotes AS VARCHAR(MAX)) 

			,Education_Relationship_Type__c=NULL
			,Education_Major__c=NULL
			,Education_Minor__c=NULL

			,Affiliation__c=T.RE_DB_OwnerShort
	 
			--ref
			,T1.KeyInd refT1_KeyInd, T1.ImportID refT1_ImportId
			,T2.KeyInd refT2_KeyInd, T2.ImportID refT2_ImportId
			,T.ASRImpID AS refRImpID, T.ASRLink ref_RLink, T.RE_DB_OwnerShort AS zrefRE_DB_OwnerShort, T.RE_DB_Tbl AS zrefRE_DB_Tbl
			,T.ASRDateFrom refDateFrom, T.ASRDateTo refDateTo
			,T.ASRType refRelat, 'Solicitee' refRecip
			,'Cons_Solicitor' AS refSource			
			FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Solicitor_v T
			INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T2 ON T.ASRLink=T2.ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			
		UNION ALL */
		
			--EDUCATION RELATIONSHIP
			SELECT	
			 OwnerID=dbo.fnc_OwnerID()
			,RecordTypeID=dbo.fnc_RecordType('Relationship_Account_Contact')
			,External_ID__c=T.RE_DB_OwnerShort+'-'+T.ESRImpID
			,RE_IRImpID__c=NULL
			,RE_ORImpID__c=NULL
			,RE_ASRImpID__c= NULL
			,RE_ESRImpID__c=(T.RE_DB_OwnerShort+'-'+T.ESRImpID)
			,[rC_Bios__Account_1__r:External_ID__c]=(T1.RE_DB_OwnerShort+'-'+T1.Unique_MD5)
			,[rC_Bios__Contact_1__r:External_ID__c]=NULL
			,[rC_Bios__Account_2__r:External_ID__c]=NULL 
			,[rC_Bios__Contact_2__r:External_ID__c]=(T2.RE_DB_OwnerShort+'-'+T2.ImportID) 
										
			,rC_Bios__Category__c='Education'
			,rC_Bios__Role_1__c='Student'
			,rC_Bios__Role_2__c='School'
			
			,rC_Bios__Starting_Month__c=CASE WHEN LEN(T.ESRDateEnt)=8 THEN MONTH(T.ESRDateEnt) WHEN LEN(T.ESRDateEnt)=6 THEN RIGHT(T.ESRDateEnt,2) WHEN LEN(T.ESRDateEnt)=4 THEN NULL END
			,rC_Bios__Starting_Day__c=CASE WHEN LEN(T.ESRDateEnt)=8 THEN DAY(T.ESRDateEnt) END
			,rC_Bios__Starting_Year__c=CASE WHEN LEN(T.ESRDateEnt)=8 THEN YEAR(T.ESRDateEnt)  WHEN LEN(T.ESRDateEnt)=6 THEN LEFT(T.ESRDateEnt,4) WHEN LEN(T.ESRDateEnt)=4 THEN T.ESRDateEnt END
			
			,rC_Bios__Stopping_Month__c=CASE WHEN LEN(T.ESRDateGrad)=8 THEN MONTH(T.ESRDateGrad) WHEN LEN(T.ESRDateGrad)=8 THEN RIGHT(T.ESRDateGrad,2) WHEN LEN(T.ESRDateGrad)=4 THEN NULL END
			,rC_Bios__Stopping_Day__c=CASE WHEN LEN(T.ESRDateGrad)=8 THEN DAY(T.ESRDateGrad) END
			,rC_Bios__Stopping_Year__c=CASE WHEN LEN(T.ESRDateGrad)=8 THEN YEAR(T.ESRDateGrad)  WHEN LEN(T.ESRDateGrad)=8 THEN LEFT(T.ESRDateGrad,4) WHEN LEN(T.ESRDateGrad)=4 THEN T.ESRDateGrad END
			,'TRUE' AS rC_Bios__Active__c
			,rC_Bios__Comments__c=CAST(T.ESRNotes AS VARCHAR(MAX)) 
			
			,Education_Relationship_Type__c=T.ESRType
			,CAST(T3.Education_Major__c AS VARCHAR(MAX)) as Education_Major__c
			,CAST(T4.Education_Minor__c as VARCHAR(MAX)) AS Education_Minor__c
			
			,Affiliation__c=T.RE_DB_OwnerShort
	 
			--ref
			,'O' AS zrefT1_KeyInd, T1.ESRImpID AS zrefT1_ImportId
			,T2.KeyInd as zrefT2_KeyInd, T2.ImportID AS zrefT2_ImportId
			,T1.ESRImpID AS zrefRImpID, null AS zref_RLink, T.RE_DB_OwnerShort AS zrefRE_DB_OwnerShort, T.RE_DB_Tbl AS zrefRE_DB_Tbl
			,T.ESRDateGrad AS zrefDateFrom, T.ESRDateLeft AS zrefDateTo
			,null AS zrefRelat, NULL AS zrefRecip
			,'Educ_Relat' AS zrefSource						
			FROM SUTTER_MGEA_DATA.dbo.HC_Educ_Relat_v T
			INNER JOIN SUTTER_MGEA_MIGRATION.TBL.ESRSchoolName T1 ON T.ESRImpID=T1.ESRImpID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T2 ON T.ImportID=T2.ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Education_Major T3 ON T.ESRImpID=T3.ESRMajESRImpID AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Education_Minor T4 ON T.ESRImpID=T4.ESRMinESRImpID AND T.RE_DB_OwnerShort=T4.RE_DB_OwnerShort

END;


BEGIN
			SELECT rC_Bios__Comments__c FROM SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP WHERE rC_Bios__Comments__c LIKE '%"%'
			UPDATE SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP SET rC_Bios__Comments__c=REPLACE(rC_Bios__Comments__c,'"','''') where rC_Bios__Comments__c like '%"%'
END 

 
BEGIN 
		
		SELECT * 
		FROM SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP
		WHERE External_ID__c IN (SELECT External_ID__c FROM SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP GROUP BY External_ID__c HAVING COUNT(*)>1)

		 
		SELECT * 
		FROM SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP
		WHERE RE_IRImpID__c IN (SELECT RE_IRImpID__c FROM SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP GROUP BY RE_IRImpID__c HAVING COUNT(*)>1)

		SELECT * 
		FROM SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP
		WHERE RE_ORImpID__c IN (SELECT RE_ORImpID__c FROM SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP GROUP BY RE_ORImpID__c HAVING COUNT(*)>1)

		SELECT * 
		FROM SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP
		WHERE RE_ASRImpID__c IN (SELECT RE_ASRImpID__c FROM SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP GROUP BY RE_ASRImpID__c HAVING COUNT(*)>1)

		SELECT * 
		FROM SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP
		WHERE RE_ESRImpID__c IN (SELECT RE_ESRImpID__c FROM SUTTER_MGEA_MIGRATION.IMP.RELATIONSHIP GROUP BY RE_ESRImpID__c HAVING COUNT(*)>1)

END
 