
USE SUTTER_MGEA_DATA;
GO 
 
	BEGIN
		DROP TABLE SUTTER_MGEA_MIGRATION.TBL.PREFERENCE;
		DROP TABLE SUTTER_MGEA_MIGRATION.IMP.PREFERENCE;
		DROP TABLE SUTTER_MGEA_MIGRATION.IMP.PREFERENCE_CODE;
	END;

  
 
 BEGIN--BASE TABLE PREFERENCES 

		--CONSTITUENT ATTRIBUTE from CHART_ATTRIBUTES 
				SELECT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.CAttrImpID)
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				
				,rC_Bios__Category__c=T2.PREFERENCE_Category
				,rC_Bios__Subcategory__c=T2.PREFERENCE_Subcategory
				,rC_Bios__Type__c=T2.PREFERENCE_Type
				,rC_Bios__Subtype__c=T2.PREFERENCE_Subtype
				,rC_Bios__Value__c=T.CAttrDesc
				,rC_Bios__Comments__c=T.CAttrCom
				,rC_Bios__Start_Date__c=T.CAttrDate
				,rC_Bios__End_Date__c=T.CAttrDate
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.RE_DB_OwnerShort
				,NULL AS rC_Bios__Active__c
				,NULL AS rC_Bios__Status__c
				,NULL AS Capacity_Rating__c	
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c
				--reference
				,T1.KeyInd AS refKeyInd
				,T.CAttrImpID AS refRecImportID
				,'ConsAttribute' AS refRecSource
				,T.ImportID AS refConsImportID

				INTO SUTTER_MGEA_MIGRATION.TBL.PREFERENCE
				FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_Attributes T2 ON T.CAttrCat=T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.SF_Object_1='RC_BIOS_PREFERENCE__C' AND T.CAttrDesc IS NOT NULL 
			
			UNION ALL 
				
		--CONSTITUENT ATTRIBUTE from CHART_CONS_ATTRIBUTES 
				SELECT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.CAttrImpID)
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				
				,rC_Bios__Category__c=T2.PREFERENCE_Category
				,rC_Bios__Subcategory__c=T2.PREFERENCE_Subcategory
				,rC_Bios__Type__c=T2.PREFERENCE_Type
				,rC_Bios__Subtype__c=T2.PREFERENCE_Subtype
				,rC_Bios__Value__c=T.CAttrDesc
				,rC_Bios__Comments__c=T.CAttrCom
				,rC_Bios__Start_Date__c=T.CAttrDate
				,rC_Bios__End_Date__c=NULL
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.RE_DB_OwnerShort
				,CASE T2.PREFERENCE_Active WHEN 1 THEN 'TRUE' WHEN 0 THEN 'FALSE' END AS rC_Bios__Active__c  
				,NULL AS rC_Bios__Status__c
				,T2.PREFERENCE_Capacity_Rating__c AS Capacity_Rating__c
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c
				--reference
				,T1.KeyInd AS refKeyInd
				,T.CAttrImpID AS refRecImportID
				,'ConsAttribute' AS refRecSource
				,T.ImportID AS refConsImportID

				FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_ConsAttributes T2 ON T.CAttrCat=T2.CAttrCat AND T.CAttrDesc=T2.CAttrDesc AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.PREFERENCE_RecordType IS NOT NULL
				 
			UNION ALL 
				
		--CONSTITUENT CODE 
				SELECT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.ConsCodeImpID)
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				
				,rC_Bios__Category__c='Constituent Type'
				,rC_Bios__Subcategory__c=T2.PREFERENCE_Subcategory
				,rC_Bios__Type__c=T2.PREFERENCE_Type
				,rC_Bios__Subtype__c=T2.PREFERENCE_Subtype
				,rC_Bios__Value__c=T.ConsCodeDesc	
				,rC_Bios__Comments__c=NULL
				,rC_Bios__Start_Date__c=T.ConsCodeDateFrom
				,rC_Bios__End_Date__c=T.ConsCodeDateTo
				,rC_Bios__Priority__c=T2.PREFERENCE_Priority
				,rC_Bios__Affiliation__c=T.RE_DB_OwnerShort
				,T2.PREFERENCE_Active AS rC_Bios__Active__c  
				,NULL AS rC_Bios__Status__c
				,NULL AS Capacity_Rating__c
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c
			
				,T1.KeyInd AS refKeyInd
				,T.ConsCodeImpID AS refRecImportID
				,'ConsCode' AS refRecSource
				,T.ImportID AS refConsImportID
				
				FROM SUTTER_MGEA_DATA.dbo.HC_Cons_ConsCode_v T
				INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_ConstituencyCodes T2 ON T.ConsCode=T2.ConsCode AND T1.KeyInd=T2.KeyInD AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.[Convert]='Yes'
			
			UNION ALL 	 
		
		--CONSTITUENT SOLICIT CODE 
				SELECT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+CAST(T.ID AS NVARCHAR(20)))
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				
				,rC_Bios__Category__c=T2.PREFERENCE_Category
				,rC_Bios__Subcategory__c=T2.PREFERENCE_Sub_Category
				,rC_Bios__Type__c=NULL
				,rC_Bios__Subtype__c=NULL
				,rC_Bios__Value__c=NULL
				,rC_Bios__Comments__c=NULL
				,rC_Bios__Start_Date__c=NULL
				,rC_Bios__End_Date__c=NULL
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.RE_DB_OwnerShort
				,NULL rC_Bios__Active__c  
				,NULL AS rC_Bios__Status__c
				,NULL AS Capacity_Rating__c
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c
			
				,T1.KeyInd AS refKeyInd
				,CAST(T.ID AS NVARCHAR(20))AS refRecImportID
				,'ConsSolCode' AS refRecSource
				,T.ImportID AS refConsImportID
				
				FROM SUTTER_MGEA_DATA.dbo.HC_Cons_SolicitCodes_v T
				INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_ConsSolicitCodes T2 ON T.SolicitCode=T2.SolicitCode AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.[Convert]='Yes'
			
			UNION ALL 
			
		--PROSPECT PHILANTHROPIC INTEREST 
				SELECT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+CAST(T.ID AS NVARCHAR(20)))
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				
				,rC_Bios__Category__c='Prospect information'
				,rC_Bios__Subcategory__c='Philanthropic Interest'
				,rC_Bios__Type__c=NULL
				,rC_Bios__Subtype__c=NULL
				,rC_Bios__Value__c=T.PPICode
				,rC_Bios__Comments__c=T.PPICom
				,rC_Bios__Start_Date__c=NULL
				,rC_Bios__End_Date__c=NULL
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.RE_DB_OwnerShort
				,NULL rC_Bios__Active__c  
				,NULL AS rC_Bios__Status__c
				,NULL AS Capacity_Rating__c
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c
		
				,T1.KeyInd AS refKeyInd
				,CAST(T.ID AS NVARCHAR(20))AS refRecImportID
				,'ProsPhilant' AS refRecSource
				,T.ImportID AS refConsImportID
				
				FROM SUTTER_MGEA_DATA.dbo.HC_Prospect_Philanthropic_v T
				INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			
			UNION ALL 
			
		--PROSPECT RATING 
				SELECT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.PRateImpID)
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				
				,rC_Bios__Category__c='Prospect information'
				,rC_Bios__Subcategory__c='Rating'
				,rC_Bios__Type__c=T.PRateSource
				,rC_Bios__Subtype__c=T.PRateCat
				,rC_Bios__Value__c=T.PRateDesc
				,rC_Bios__Comments__c=T.PRateNotes
				,rC_Bios__Start_Date__c=T.PRateDate 
				,rC_Bios__End_Date__c=T.PRateDate 
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.RE_DB_OwnerShort
				,NULL rC_Bios__Active__c  
				,NULL AS rC_Bios__Status__c
				,NULL AS Capacity_Rating__c
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c
			
				,T1.KeyInd AS refKeyInd
				,CAST(T.ID AS NVARCHAR(20))AS refRecImportID
				,'ProsRating' AS refRecSource
				,T.ImportID AS refConsImportID
				
				FROM SUTTER_MGEA_DATA.dbo.HC_Prospect_Rating_v T
				INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			
			UNION ALL 
			
		--PROSPECT FINANCIAL
				SELECT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Financial_Info') AS RecordTypeID
				
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.PFIImportID)
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				
				,rC_Bios__Category__c='Prospect information'
				,rC_Bios__Subcategory__c='Financial Information'
				,rC_Bios__Type__c=T.PFIType
				,rC_Bios__Subtype__c=T.PFISource
				,rC_Bios__Value__c=CAST(T.PFIAmt AS NVARCHAR(30))
				,rC_Bios__Comments__c=T.PFINotes
				,rC_Bios__Start_Date__c=T.PFIDateAcq
				,rC_Bios__End_Date__c=T.PFIDateAcq
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.RE_DB_OwnerShort
				,NULL rC_Bios__Active__c  
				,NULL AS rC_Bios__Status__c
				,NULL AS Capacity_Rating__c
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c
			
				,T1.KeyInd AS refKeyInd
				,CAST(T.ID AS NVARCHAR(20))AS refRecImportID
				,'ProsFinInfo' AS refRecSource
				,T.ImportID AS refConsImportID
				
				FROM SUTTER_MGEA_DATA.dbo.HC_Prospect_Financial_v T
				INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort

			UNION ALL 
			
		--PROSPECT OTHER GIFTS 
				SELECT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.POGImpID)
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				
				,rC_Bios__Category__c='Prospect information'
				,rC_Bios__Subcategory__c='Gifts To Other Organizations'
				,rC_Bios__Type__c=T.POGOrgType
				,rC_Bios__Subtype__c=NULL 
				,rC_Bios__Value__c=T.POGOrgName
				,rC_Bios__Comments__c=T.POGCom
				,rC_Bios__Start_Date__c=T.POGDateGiven 
				,rC_Bios__End_Date__c=T.POGDateGiven 
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.RE_DB_OwnerShort
				,NULL rC_Bios__Active__c  
				,NULL AS rC_Bios__Status__c
				,NULL AS Capacity_Rating__c
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c
			
				,T1.KeyInd AS refKeyInd
				,CAST(T.ID AS NVARCHAR(20))AS refRecImportID
				,'ProsOtherGift' AS refRecSource
				,T.ImportID AS refConsImportID
				
				FROM SUTTER_MGEA_DATA.dbo.HC_Prospect_Other_Gifts_v T
				INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			
			UNION ALL 
			
		--PROSPECT WILL NOT GIVE  
				SELECT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+CAST(T.ID AS NVARCHAR(20)))
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				
				,rC_Bios__Category__c='Prospect information'
				,rC_Bios__Subcategory__c='Will Not Give'
				,rC_Bios__Type__c=NULL
				,rC_Bios__Subtype__c=NULL 
				,rC_Bios__Value__c=T.PWNGCode
				,rC_Bios__Comments__c=T.PWNGCom
				,rC_Bios__Start_Date__c=NULL
				,rC_Bios__End_Date__c=NULL 
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.RE_DB_OwnerShort
				,NULL rC_Bios__Active__c  
				,NULL AS rC_Bios__Status__c
				,NULL AS Capacity_Rating__c
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c
			
				,T1.KeyInd AS refKeyInd
				,CAST(T.ID AS NVARCHAR(20))AS refRecImportID
				,'ProsWillNotGive' AS refRecSource
				,T.ImportID AS refConsImportID
				
				FROM SUTTER_MGEA_DATA.dbo.HC_Prospect_WillNotGive_v T
				INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort

			UNION ALL 
			
		--VOLUNTEER TYPE
				SELECT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+CAST(T.ID AS NVARCHAR(20)))
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				
				,rC_Bios__Category__c='Volunteer Information'
				,rC_Bios__Subcategory__c=NULL
				,rC_Bios__Type__c=T.VTType
				,rC_Bios__Subtype__c=NULL 
				,rC_Bios__Value__c=NULL
				,rC_Bios__Comments__c=T.VTReasonFin
				,rC_Bios__Start_Date__c=T.VTStartDate
				,rC_Bios__End_Date__c=T.VTEndDate
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.RE_DB_OwnerShort
				,NULL rC_Bios__Active__c  
				,T.VTStatus AS rC_Bios__Status__c
				,NULL AS Capacity_Rating__c
			
				,T2.VEmerName AS Emergency_Contact_Name__c
				,T2.VEmerPhone AS Emergency_Contact_Phone__c
				,T2.VEmerRel AS Emergency_Contact_Relationship__c

				,T1.KeyInd AS refKeyInd
				,CAST(T.ID AS NVARCHAR(20))AS refRecImportID
				,'VolType' AS refRecSource
				,T.ImportID AS refConsImportID
				
				FROM SUTTER_MGEA_DATA.dbo.HC_Volunteer_Type_v T
				INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				LEFT JOIN SUTTER_MGEA_DATA.dbo.HC_Volunteer_v T2 ON T.ImportID=T2.ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort

			UNION ALL
				
		--CONSTITUENT ALIAS 
				SELECT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+CAST(T.ID AS NVARCHAR(20)))
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T.RE_DB_OwnerShort+'-'+T.ImportID END 
				
				,rC_Bios__Category__c='Aliases'
				,rC_Bios__Subcategory__c=CASE WHEN T.AliasType IS NULL THEN 'Unknown' ELSE T.AliasType END 
				,rC_Bios__Type__c=NULL
				,rC_Bios__Subtype__c=NULL 
				,rC_Bios__Value__c=T.AliasName
				,rC_Bios__Comments__c=NULL
				,rC_Bios__Start_Date__c=NULL
				,rC_Bios__End_Date__c=NULL
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.RE_DB_OwnerShort
				,NULL AS rC_Bios__Active__c  
				,NULL AS rC_Bios__Status__c
				,NULL AS Capacity_Rating__c
			
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c

				,T1.KeyInd AS refKeyInd
				,CAST(T.ID AS NVARCHAR(20))AS refRecImportID
				,'ConsAlias' AS refRecSource
				,T.ImportID AS refConsImportID
				
				FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Alias_v T
				INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				WHERE T.AliasName IS NOT NULL  

END;
 
BEGIN--PREFERENCE IMP
			SELECT 
					CAST(SUBSTRING(master.sys.fn_varbintohexstr(HASHBYTES('MD5', ISNULL(UPPER(rC_Bios__Category__c),' ')+ISNULL(UPPER(rC_Bios__Subcategory__c),' ')+
					ISNULL(UPPER(rC_Bios__Type__c),' ')+ISNULL(UPPER(rC_Bios__Subtype__c),' '))), 3, 50) AS NVARCHAR(50)) 
					AS  rC_Bios__Code_Value__c
					,*
			INTO SUTTER_MGEA_MIGRATION.IMP.PREFERENCE
			FROM SUTTER_MGEA_MIGRATION.TBL.PREFERENCE
			ORDER BY rC_Bios__Category__c, rC_Bios__Subcategory__c, rC_Bios__Type__c, rC_Bios__Subtype__c;
END;


BEGIN--CHECK DUPLICATES
		SELECT * 
		FROM SUTTER_MGEA_MIGRATION.IMP.PREFERENCE
		WHERE rC_Bios__External_ID__c IN (SELECT rC_Bios__External_ID__c FROM SUTTER_MGEA_MIGRATION.IMP.PREFERENCE GROUP BY rC_Bios__External_ID__c HAVING COUNT(*)>1);
END;


BEGIN--CHECK FIELD LENGTH
	SELECT	DISTINCT rC_Bios__Category__c, LEN(rC_Bios__Category__c) ln
	FROM SUTTER_MGEA_MIGRATION.IMP.PREFERENCE
	WHERE LEN(rC_Bios__Category__c)>40
	ORDER BY ln DESC;
	
	SELECT	DISTINCT rC_Bios__SubCategory__c, LEN(rC_Bios__SubCategory__c) ln
	FROM SUTTER_MGEA_MIGRATION.IMP.PREFERENCE
	WHERE LEN(rC_Bios__SubCategory__c)>40
	ORDER BY ln DESC;
	
	SELECT	DISTINCT rC_Bios__Type__c, LEN(rC_Bios__Type__c) ln
	FROM SUTTER_MGEA_MIGRATION.IMP.PREFERENCE
	WHERE LEN(rC_Bios__Type__c)>40
	ORDER BY ln DESC;
	
	SELECT	DISTINCT rC_Bios__SubType__c, LEN(rC_Bios__SubType__c) ln
	FROM SUTTER_MGEA_MIGRATION.IMP.PREFERENCE
	WHERE LEN(rC_Bios__SubType__c)>40
	ORDER BY ln DESC;
	
END;

BEGIN
		-- SELECT rC_Bios__External_ID__c, rC_Bios__Comments__c FROM SUTTER_MGEA_MIGRATION.IMP.PREFERENCE WHERE rC_Bios__Comments__c LIKE '%"%'
		
		UPDATE SUTTER_MGEA_MIGRATION.IMP.PREFERENCE SET rC_Bios__Comments__c=REPLACE(rC_Bios__Comments__c,'"','''') WHERE rC_Bios__Comments__c LIKE '%"%';
		UPDATE SUTTER_MGEA_MIGRATION.IMP.PREFERENCE SET rC_Bios__Value__c=REPLACE(rC_Bios__Value__c,'"','''') WHERE rC_Bios__Value__c LIKE '%"%';
	
		--SHORTEN "BLACKBAUD ANALYTICS' CUSTOM MODELING SERVICES" to comply with the 40 char max
		UPDATE SUTTER_MGEA_MIGRATION.IMP.PREFERENCE 
		SET rC_Bios__Type__c='Blackbaud Analytics'+''''+ ' CMS'
		WHERE rC_Bios__Category__c='Prospect information'	
		AND rC_Bios__Subcategory__c	='Rating'
		AND rC_Bios__Type__c LIKE 'Blackbaud Analytics%';
	 
		UPDATE SUTTER_MGEA_MIGRATION.IMP.PREFERENCE 
		SET RC_BIOS__SUBTYPE__C ='A.Lucchetti Women'+''''+'s & Children'+''''+'s Center'
		WHERE RC_BIOS__SUBTYPE__C LIKE 'Anderson L%';
	 
END;		
 

BEGIN--PREFERENCE CODE

	--CREATE
	SELECT	DISTINCT  
			dbo.fnc_OwnerID() AS OwnerID
			,rC_Bios__Category__c
			,rC_Bios__Subcategory__c
			,rC_Bios__Type__c
			,rC_Bios__Subtype__c
			,rC_Bios__Code_Value__c
			,rC_Bios__Priority__c
	INTO SUTTER_MGEA_MIGRATION.IMP.PREFERENCE_CODE		 	
	FROM SUTTER_MGEA_MIGRATION.IMP.PREFERENCE;

		/*not needed:replaced by md5	--ADD CODE
			DECLARE @myVar int
			SET @myVar = 0
			UPDATE SUTTER_MGEA_MIGRATION.IMP.PREFERENCE_CODE 
			SET @myvar = rC_Bios__Code_Value__c = @myVar + 1
		*/

END;

BEGIN
	SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.PREFERENCE_CODE;
END;
 
			   