
USE SUTTER_MGEA_MIGRATION
GO


BEGIN

	DROP TABLE SUTTER_MGEA_MIGRATION.IMP.PLANNED_GIVING

END 
 

 /*********************************************************************************************************************************************************
 NEED TO ADD SCRIPTS FOR PLANNED GIVING CONVERTED FROM HC_GIFT_V WHERE GFTYPE = PLANNED GIFT.
 **********************************************************************************************************************************************************/


BEGIN--PLANNED GIVING

					SELECT  
					dbo.fnc_OwnerID() AS OwnerID         
					,T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.PRImpID AS	rC_Giving__External_ID__c
					,T1.PRName AS Name
					,T1.PRAmtAsk AS rC_Giving__Giving_Amount__c
					--,T1.PRCampID AS [CAMPAIGN:External_ID__c]
					,CASE WHEN T4.NoHH_ImportID IS NOT NULL THEN T4.RE_DB_OwnerShort+'-'+T4.HH_ImportID ELSE T1.RE_DB_OwnerShort+'-'+T1.ImportID END AS [rC_Giving__Account__r:External_ID__c]
					,T1.RE_DB_OwnerShort+'-'+T1.ImportID  AS [rC_Giving__Contact__r:External_ID__c]
					,CAST(CASE WHEN T1.PRDateAsk IS NULL THEN T1.DateAdded ELSE T1.PRDateAsk END AS DATE) AS Proposal_Date_Asked__c
					,T3.[GAU External ID] AS [General_Accounting_Unit__r:rC_Giving__External_ID__c]
					,T1.PRPurpose	AS	Proposal_Purpose__c
					,T1.PRRating    AS  Proposal_Rating__c
					,T1.PRReason    AS  Proposal_Reason__c
					,CASE WHEN (T5.Proposal_Status__c IS NULL AND (CASE WHEN T1.PRDateAsk IS NULL THEN T1.DateAdded ELSE T1.PRDateAsk END) <=GETDATE()) 
								THEN 'Completed' WHEN (T5.Proposal_Status__c IS NULL AND (CASE WHEN T1.PRDateAsk IS NULL THEN T1.DateAdded ELSE T1.PRDateAsk END) >GETDATE()) 
								THEN 'Open' ELSE T5.Proposal_Status__c END AS rC_Giving__Stage__c

					,T1.DateAdded	AS	CreatedDate
					,T1.RE_DB_OwnerShort	AS	rC_Giving__Affiliation__c
					,dbo.fnc_RecordType('Planned_Giving_Bequest') AS RecordTypeID
					,NULL AS	rC_Giving__Rollup_Giving__c
					,CAST(T2.RE_Team_Member__c AS VARCHAR(255)) AS RE_Team_Member__c 

					--reference
					,T1.ImportID AS zref_ImportID
					,T4.ImportID AS zref_NoHH_ImportID
					,T4.HH_ImportID AS zref_HH_ImportID 
					,T1.PRImpID	 AS zrefPRImpID
					,T1.RE_DB_OwnerShort AS zrefRE_DB_OwnerShort
					
					
					INTO SUTTER_MGEA_MIGRATION.IMP.PLANNED_GIVING
					FROM SUTTER_MGEA_DATA.dbo.HC_Proposal_v T1
					LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Attribute_opportunity_proposal_1  T2 ON T1.PRImpID=T2.PRAttrPRImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
					LEFT JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_Fund T3 ON T1.PRFundID=T3.FundID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
					LEFT JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH T4 ON T1.ImportID=T4.NoHH_ImportID AND T1.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
					LEFT JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_PRStatus T5 ON T1.PRStatus=T5.PRStatus AND T1.RE_DB_OwnerShort=T5.RE_DB_OwnerShort
					WHERE T1.PRName  LIKE '%planned%'
					ORDER BY t1.RE_DB_OwnerShort, T1.PRImpID
					
END	   

			BEGIN --test dupes
					SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.PLANNED_GIVING
					WHERE rC_Giving__External_ID__c IN (SELECT rC_Giving__External_ID__c FROM SUTTER_MGEA_MIGRATION.IMP.PLANNED_GIVING
														GROUP BY rC_Giving__External_ID__c HAVING COUNT(*)>1)
			END; 


/*need to COMPLETE CODE FOR OPPORTUNITY PLANNED GIVIGIN UPDATE to link Donatiosn and Planned giving.... 11.10.2015  */
		--SELECT * FROM SUTTER_MGEA_DATA.dbo.HC_Gift_ProposalLink_v

		GFLink  (Opportunity.Id)
				(Opportunity.External_Id__c)
		PRImpID (Opportunity.rC_Giving__Planned_Giving__c) 
   


		SELECT t.rC_Giving__External_ID__c AS rC_Giving__Planned_Giving__c, T1.RE_DB_OwnerShort+'-'+t1.GFLink  AS OpportunityID
		FROM SUTTER_MGEA_MIGRATION.IMP.PLANNED_GIVING T
		INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Gift_ProposalLink_v t1 ON t.zrefPRImpID=t1.PRImpID AND t.zrefRE_DB_OwnerShort=t1.RE_DB_OwnerShort