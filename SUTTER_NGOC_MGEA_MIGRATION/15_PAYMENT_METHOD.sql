
USE SUTTER_MGEA_DATA
GO


BEGIN
	DROP TABLE SUTTER_MGEA_MIGRATION.TBL.PAYMENT_METHOD	
	DROP TABLE SUTTER_MGEA_MIGRATION.IMP.PAYMENT_METHOD	
END

 


BEGIN--PAYMENT METHODS 
				
		BEGIN--BASE TABLE
				   /*
				    DROP TABLE SUTTER_MGEA_MIGRATION.TBL.PAYMENT_METHOD
				    GO
					DROP TABLE SUTTER_MGEA_MIGRATION.IMP.PAYMENT_METHOD 
					GO
					*/
				SELECT 
				dbo.fnc_OwnerID() AS OwnerID
				,CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.RE_DB_OwnerShort+'-'+T3.HH_ImportID ELSE T1.RE_DB_OwnerShort+'-'+T1.ImportID  END AS ACCOUNT_External_ID__c
				,CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ISNULL(UPPER(CASE WHEN T3.ImportID IS NOT NULL THEN T3.RE_DB_OwnerShort+'-'+T3.HH_ImportID ELSE T1.RE_DB_OwnerShort+'-'+T1.ImportID END),' ')+   --Head of Household
						ISNULL(UPPER(CASE WHEN (T1.GFType LIKE '%Stock%') THEN 'Securities'
										  WHEN (T1.GFPayMeth LIKE '%Check%' OR T1.GFPayMeth='Cash') THEN 'Cash/Check'
										  WHEN (T1.GFPayMeth ='Credit Card') THEN 'Third Party Charge'
										  WHEN (T1.GFPayMeth ='Direct Debit') THEN 'EFT'
										  WHEN (T1.GFPayMeth ='Other') THEN 'Other'
										  ELSE 'Cash/Check' END ),' ')+ 
						ISNULL(UPPER(T1.GFCCType),' ')+ 
						ISNULL(UPPER(T1.GFCCNum),' ')+
						ISNULL(UPPER(T1.GFCardholderName),' ')+
						ISNULL(UPPER(T1.GFCCExpOn),' ')+
						ISNULL(UPPER(T1.GFAuthCode),' '))), 3, 50) AS NVARCHAR(50)) 
					AS External_Id__c
				,CASE WHEN T1.GFType LIKE '%stock%' THEN dbo.fnc_RecordType('Payment_Method_Securities') 
													ELSE dbo.fnc_RecordType('Payment_Method_'+T2.rC_Giving__Payment_Type__c) END AS RecordTypeID	
				,CASE WHEN T1.GFType LIKE '%stock%' THEN 'Securities' ELSE T2.rC_Giving__Payment_Type__c END AS Name
				,CASE WHEN T1.GFType LIKE '%stock%' THEN 'Securities' ELSE T2.rC_Giving__Payment_Type__c END AS rC_Giving__Payment_Type__c
				,CASE WHEN T2.rC_Giving__Payment_Type__c ='Cash/Check' then 'TRUE' else 'FALSE' end as rC_Giving__Is_Default__c
				,CASE WHEN T2.rC_Giving__Payment_Type__c ='Cash/Check' then 'TRUE' else 'FALSE' end as rC_Giving__Is_Active__c
				,T1.GFCCType AS rC_Giving__Card_Issuer__c
				,RIGHT(T1.GFCCNum,4) AS rC_Giving__Card_Number_Last_4__c 
				,T1.GFCardholderName AS rC_Giving__Card_Holder_Name__c
				,RIGHT(T1.GFCCExpOn,2)+'/'+LEFT(T1.GFCCExpOn, 4) AS rC_Giving__Card_Expiration__c
				,T1.GFAuthCode AS rC_Connect__Authentication_Value__c
				--ref
				,T1.GFPayMeth
				,T1.GFType
				,T1.GFImpID
				,T1.RE_DB_OwnerShort
				,T1.ImportID 
				,T3.HH_ImportID HH_ImportID
				,T3.ImportID AS NoHH_ImportId
				INTO SUTTER_MGEA_MIGRATION.TBL.PAYMENT_METHOD	
				FROM SUTTER_MGEA_DATA.dbo.HC_Gift_v T1
				INNER JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_GFPayMeth T2 ON T1.GFPayMeth=T2.GFPayMeth				
				LEFT JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH AS T3 ON T1.ImportID=T3.NoHH_ImportID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
	 		UNION ALL 
				--cons without gifts	
				SELECT 
				dbo.fnc_OwnerID() AS OwnerID
				,CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.RE_DB_OwnerShort+'-'+T3.HH_ImportID ELSE T1.RE_DB_OwnerShort+'-'+T1.ImportID  END AS ACCOUNT_External_ID__c
				,CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ISNULL(UPPER(CASE WHEN T3.ImportID IS NOT NULL THEN T3.RE_DB_OwnerShort+'-'+T3.HH_ImportID ELSE T1.RE_DB_OwnerShort+'-'+T1.ImportID END),' ')+   --Head of Household
						ISNULL(UPPER(CASE WHEN (T2.GFPayMeth LIKE '%Check%' OR T2.GFPayMeth='Cash') THEN 'Cash/Check'
										  WHEN (T2.GFPayMeth ='Credit Card') THEN 'Third Party Charge'
										  WHEN (T2.GFPayMeth ='Direct Debit') THEN 'EFT'
										  WHEN (T2.GFPayMeth ='Other') THEN 'Other'
										  ELSE 'Cash/Check' END ),' ')+ 
						ISNULL(UPPER(T2.GFCCType),' ')+ 
						ISNULL(UPPER(T2.GFCCNum),' ')+
						ISNULL(UPPER(T2.GFCardholderName),' ')+
						ISNULL(UPPER(T2.GFCCExpOn),' ')+
						ISNULL(UPPER(T2.GFAuthCode),' '))), 3, 50) AS NVARCHAR(50)) 
					AS External_Id__c
				,dbo.fnc_RecordType('Payment_Method_Cash/Check') AS RecordTypeID
				,'Cash/Check' AS Name
				,'Cash/Check' AS rC_Giving__Payment_Type__c
				,'TRUE' AS rC_Giving__Is_Default__c
				,'TRUE' AS rC_Giving__Is_Active__c
				,NULL AS rC_Giving__Card_Issuer__c
				,NULL AS rC_Giving__Card_Number_Last_4__c 
				,NULL AS rC_Giving__Card_Holder_Name__c
				,NULL AS rC_Giving__Card_Expiration__c
				,NULL AS rC_Connect__Authentication_Value__c
				--ref
				,NULL AS GFPayMeth
				,NULL AS GFType
				,T2.GFImpID 
				,T1.RE_DB_OwnerShort
				,T1.ImportID 
				,T3.HH_ImportID HH_ImportID
				,T3.ImportID AS NoHH_ImportId

				FROM SUTTER_MGEA_DATA.dbo.HC_Constituents_v AS T1
				LEFT JOIN SUTTER_MGEA_DATA.dbo.hc_gift_v AS T2 ON T1.ImportID=T2.ImportID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH AS T3 ON T1.ImportID=T3.NoHH_ImportID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort

				WHERE T2.ImportID IS NULL
				ORDER BY T1.RE_DB_OwnerShort, T1.ImportID
				 
		END
		----540,513
		--final 546,112

		BEGIN--IMP table 
				
				SELECT 
				ROW_NUMBER() OVER (PARTITION BY T1.ACCOUNT_External_ID__c 
									ORDER BY T1.ACCOUNT_External_ID__c, T1.rC_Giving__Payment_Type__c) AS zrefSeq 
				,T1.OwnerID
		  		,T1.ACCOUNT_External_ID__c AS [rC_Giving__Account__r:External_Id__c]
                ,T1.External_Id__c
                ,T1.RecordTypeID
                ,T1.Name 
                ,T1.rC_Giving__Payment_Type__c 
                ,T1.rC_Giving__Is_Default__c
                ,T1.rC_Giving__Is_Active__c 
                ,T1.rC_Giving__Card_Issuer__c
                ,T1.rC_Giving__Card_Number_Last_4__c
                ,T1.rC_Giving__Card_Holder_Name__c
                ,T1.rC_Giving__Card_Expiration__c
                ,T1.rC_Connect__Authentication_Value__c
				INTO SUTTER_MGEA_MIGRATION.IMP.PAYMENT_METHOD    
				FROM SUTTER_MGEA_MIGRATION.TBL.PAYMENT_METHOD T1
				GROUP BY 
				 T1.OwnerID
		  		,T1.ACCOUNT_External_ID__c
                ,T1.External_Id__c
                ,T1.RecordTypeID
                ,T1.Name 
                ,T1.rC_Giving__Payment_Type__c 
                ,T1.rC_Giving__Is_Default__c
                ,T1.rC_Giving__Is_Active__c 
                ,T1.rC_Giving__Card_Issuer__c
                ,T1.rC_Giving__Card_Number_Last_4__c
                ,T1.rC_Giving__Card_Holder_Name__c
                ,T1.rC_Giving__Card_Expiration__c
                ,T1.rC_Connect__Authentication_Value__c
                
		END;		
		--122447

		BEGIN--check duplicates. 
				SELECT    *
				FROM      SUTTER_MGEA_MIGRATION.IMP.PAYMENT_METHOD 
				WHERE     External_Id__c IN (
					        SELECT  External_Id__c
						    FROM     SUTTER_MGEA_MIGRATION.IMP.PAYMENT_METHOD 
							GROUP BY External_Id__c
							HAVING  COUNT(*) > 1 )
				ORDER BY  External_Id__c 
		
				--del dupe
				DELETE  SUTTER_MGEA_MIGRATION.IMP.PAYMENT_METHOD  
				WHERE zrefseq='3' AND [rC_Giving__Account__r:External_Id__c]='SAFH-1827' AND name='securities'

		END;
			
			
		BEGIN--check defaults
		
				SELECT [rC_Giving__Account__r:External_Id__c]
					   ,rC_Giving__Is_Default__c
					   ,COUNT(*) c
				FROM SUTTER_MGEA_MIGRATION.IMP.PAYMENT_METHOD 
				WHERE   rC_Giving__Is_Default__c = 'TRUE'
				GROUP BY [rC_Giving__Account__r:External_Id__c]
					   ,rC_Giving__Is_Default__c
				HAVING COUNT(*)>1
				ORDER BY c DESC

				SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.PAYMENT_METHOD
				WHERE [rC_Giving__Account__r:External_Id__c]='SAFH-5807'

				SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.PAYMENT_METHOD
				WHERE [rC_Giving__Account__r:External_Id__c]='SAFH-8004'
								
				UPDATE SUTTER_MGEA_MIGRATION.IMP.PAYMENT_METHOD SET rC_Giving__Is_Default__c='FALSE', rC_Giving__Is_Active__c='FALSE'
				WHERE [rC_Giving__Account__r:External_Id__c]='SAFH-5807' AND Name='Securities'

				UPDATE SUTTER_MGEA_MIGRATION.IMP.PAYMENT_METHOD SET rC_Giving__Is_Default__c='FALSE', rC_Giving__Is_Active__c='FALSE'
				WHERE [rC_Giving__Account__r:External_Id__c]='SAFH-8004' AND Name='Securities'
				
		END;
		 
		BEGIN
				SELECT * 
				FROM SUTTER_MGEA_MIGRATION.IMP.PAYMENT_METHOD 
				WHERE zrefSeq=1 AND rC_Giving__Payment_Type__c!='Cash/Check'
				ORDER BY [rC_Giving__Account__r:External_Id__c], rC_Giving__Payment_Type__c
				
		END; 

END;
 