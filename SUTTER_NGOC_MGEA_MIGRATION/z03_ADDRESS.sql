USE SUTTER_DATA
GO

BEGIN	

	DROP TABLE [SUTTER_MGEA].[TBL].[Address_MD5]
	DROP TABLE [SUTTER_MGEA].[IMP].[ADDRESS]
	
END 


BEGIN-- CREATE ADDRRESS with MG5.

			SELECT
			dbo.fnc_OwnerID() AS OwnerID
			,CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ISNULL(UPPER(T.AddrLine1),' ')+ISNULL(UPPER(T.AddrLine2),' ')+
			ISNULL(UPPER(T.AddrCity),' ')+ISNULL(UPPER(T.AddrState),' ')+ISNULL(UPPER(T.AddrZIP),' ')+ISNULL(UPPER(T.AddrCountry),' '))), 3, 50) AS NVARCHAR(50)) 
			AS rC_Bios__Unique_MD5__c
		  	,STUFF(
			  CASE WHEN T.AddrLine1 IS NULL THEN '' ELSE COALESCE(', ' + LTRIM(RTRIM(T.AddrLine1)), '') END +
			  CASE WHEN T.AddrLine2 ='' THEN '' ELSE COALESCE(', ' + LTRIM(RTRIM(T.AddrLine2)), '') END +
			  CASE WHEN T.AddrCity IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.AddrCity)),  '')  END +
			  CASE WHEN T.AddrState IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.AddrState)),  '')  END +
			  CASE WHEN T.AddrZIP IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.AddrZIP)),  '')  END +
			  CASE WHEN T.AddrCountry IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.AddrCountry)),  '') END 
			, 1, 2, '') AS Name  -- new code for Name. 
			,T.AddrLine1 ,T.AddrLine2 ,T.AddrCity, T.AddrState, T.AddrZIP, T.AddrCountry,T.AddrImpID, T.AddrValidFrom, T.AddrValidTo

			
			,T1.rC_Bios__Type__c
			,CASE T1.rC_Bios__Active__c WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS rC_Bios__Active__c

			
			--reference
			,T.RE_DB_OwnerShort
			,T.RE_DB_Tbl
			,LEN(STUFF(
			  CASE WHEN T.AddrLine1 IS NULL THEN '' ELSE COALESCE(', ' + LTRIM(RTRIM(T.AddrLine1)), '') END +
			  CASE WHEN T.AddrLine2 ='' THEN '' ELSE COALESCE(', ' + LTRIM(RTRIM(T.AddrLine2)), '') END +
			  CASE WHEN T.AddrCity IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.AddrCity)),  '')  END +
			  CASE WHEN T.AddrState IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.AddrState)),  '')  END +
			  CASE WHEN T.AddrZIP IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.AddrZIP)),  '')  END +
			  CASE WHEN T.AddrCountry IS NULL THEN '' ELSE + COALESCE(', ' +LTRIM( RTRIM(T.AddrCountry)),  '') END 
			, 1, 2, '')) AS refAddrLen  
	
			INTO [SUTTER_MGEA].[TBL].[Address_MD5]  -- DROP TABLE SUTTER_MGEA.TBL.Address_MD5
			FROM SUTTER_DATA.dbo.HC_Cons_Address_v T
			LEFT JOIN SUTTER_MGEA.dbo.CHART_AddressType T1 ON T.AddrType=T1.AddrType AND T.RE_DB_OwnerShort = T1.RE_DB_OwnerShort
			
			WHERE T.AddrLine1 IS NOT NULL AND T.AddrLine1 NOT LIKE '%deceased%' AND T1.[Convert]='Yes'
			
			 
		UNION ALL
			
			SELECT
			dbo.fnc_OwnerID() AS OwnerID
			,CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ISNULL(UPPER(T.IRAddrLine1),' ')+ISNULL(UPPER(T.IRAddrLine2),' ')+
			ISNULL(UPPER(T.IRAddrCity),' ')+ISNULL(UPPER(T.IRAddrState),' ')+ISNULL(UPPER(T.IRAddrZIP),' ')+ISNULL(UPPER(T.IRAddrCountry),' '))), 3, 50) AS NVARCHAR(50))
			AS rC_Bios__Unique_MD5__c
		  	,STUFF(
			  CASE WHEN T.IRAddrLine1 IS NULL THEN '' ELSE COALESCE(', ' + LTRIM(RTRIM(T.IRAddrLine1)), '') END +
			  CASE WHEN T.IRAddrLine2 ='' THEN '' ELSE COALESCE(', ' + LTRIM(RTRIM(T.IRAddrLine2)), '') END +
			  CASE WHEN T.IRAddrCity IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.IRAddrCity)),  '')  END +
			  CASE WHEN T.IRAddrState IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.IRAddrState)),  '')  END +
			  CASE WHEN T.IRAddrZIP IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.IRAddrZIP)),  '')  END +
			  CASE WHEN T.IRAddrCountry IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.IRAddrCountry)),  '') END 
			, 1, 2, '') AS Name   
			,T.IRAddrLine1 AS AddrLine1 ,T.IRAddrLine2 AS AddrLine2 ,T.IRAddrCity AS AddrCity, T.IRAddrState AS AddrState, 
			 T.IRAddrZIP AS AddrZIP, T.IRAddrCountry AS AddrCountry,T.IRImpID AS AddrImpID, T.IRAddrValidFrom AS AddrValidFrom, T.IRAddrValidTo AS AddrValidTo
			,T1.rC_Bios__Type__c
			,CASE T1.rC_Bios__Active__c WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS rC_Bios__Active__c

	
			--reference
			,T.RE_DB_OwnerShort
			,T.RE_DB_Tbl
		  	,LEN(STUFF(
			  CASE WHEN T.IRAddrLine1 IS NULL THEN '' ELSE COALESCE(', ' + LTRIM(RTRIM(T.IRAddrLine1)), '') END +
			  CASE WHEN T.IRAddrLine2 ='' THEN '' ELSE COALESCE(', ' + LTRIM(RTRIM(T.IRAddrLine2)), '') END +
			  CASE WHEN T.IRAddrCity IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.IRAddrCity)),  '')  END +
			  CASE WHEN T.IRAddrState IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.IRAddrState)),  '')  END +
			  CASE WHEN T.IRAddrZIP IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.IRAddrZIP)),  '')  END +
			  CASE WHEN T.IRAddrCountry IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.IRAddrCountry)),  '') END 
			, 1, 2, '')) AS refAddrLen

			FROM SUTTER_DATA.dbo.HC_Ind_Relat_v T
			LEFT JOIN SUTTER_MGEA.dbo.CHART_AddressType T1 ON T.IRAddrType=T1.AddrType AND T.RE_DB_OwnerShort = T1.RE_DB_OwnerShort
			WHERE T.IRAddrLine1 IS NOT NULL AND T.IRAddrLine1 NOT LIKE '%deceased%' AND T.IRLink='' AND T1.[Convert]='Yes'
			 
		UNION ALL
			
			SELECT
			dbo.fnc_OwnerID() AS OwnerID
			,CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ISNULL(UPPER(T.ORAddrLine1),' ')+ISNULL(UPPER(T.ORAddrLine2),' ')+
			ISNULL(UPPER(T.ORAddrCity),' ')+ISNULL(UPPER(T.ORAddrState),' ')+ISNULL(UPPER(T.ORAddrZIP),' ')+ISNULL(UPPER(T.ORAddrCountry),' '))), 3, 50) AS NVARCHAR(50))
			AS rC_Bios__Unique_MD5__c
		  	,STUFF(
			  CASE WHEN T.ORAddrLine1 IS NULL THEN '' ELSE COALESCE(', ' + LTRIM(RTRIM(T.ORAddrLine1)), '') END +
			  CASE WHEN T.ORAddrLine2 ='' THEN '' ELSE COALESCE(', ' + LTRIM(RTRIM(T.ORAddrLine2)), '') END +
			  CASE WHEN T.ORAddrCity IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.ORAddrCity)),  '')  END +
			  CASE WHEN T.ORAddrState IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.ORAddrState)),  '')  END +
			  CASE WHEN T.ORAddrZIP IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.ORAddrZIP)),  '')  END +
			  CASE WHEN T.ORAddrCountry IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.ORAddrCountry)),  '') END 
			, 1, 2, '') AS Name  -- new code for Name. 
			,T.ORAddrLine1 AS AddrLine1 ,T.ORAddrLine2 AS AddrLine2 ,T.ORAddrCity AS AddrCity, T.ORAddrState AS AddrState, 
			T.ORAddrZIP AS AddrZIP, T.ORAddrCountry AS AddrCountry,T.ORImpID AS AddrImpID, T.ORAddrValidFrom AS AddrValidFrom, T.ORAddrValidTo AS AddrValidTo
			,T1.rC_Bios__Type__c
			,CASE T1.rC_Bios__Active__c WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS rC_Bios__Active__c
			
			--reference
			,T.RE_DB_OwnerShort
			,T.RE_DB_Tbl
			
			,LEN(STUFF(
			  CASE WHEN T.ORAddrLine1 IS NULL THEN '' ELSE COALESCE(', ' + LTRIM(RTRIM(T.ORAddrLine1)), '') END +
			  CASE WHEN T.ORAddrLine2 ='' THEN '' ELSE COALESCE(', ' + LTRIM(RTRIM(T.ORAddrLine2)), '') END +
			  CASE WHEN T.ORAddrCity IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.ORAddrCity)),  '')  END +
			  CASE WHEN T.ORAddrState IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.ORAddrState)),  '')  END +
			  CASE WHEN T.ORAddrZIP IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.ORAddrZIP)),  '')  END +
			  CASE WHEN T.ORAddrCountry IS NULL THEN '' ELSE + COALESCE(', ' + LTRIM(RTRIM(T.ORAddrCountry)),  '') END 
			, 1, 2, '')) AS refAddrLen
			
			FROM SUTTER_DATA.dbo.HC_Org_Relat_v T
			LEFT JOIN SUTTER_MGEA.dbo.CHART_AddressType T1 ON T.ORAddrType=T1.AddrType AND T.RE_DB_OwnerShort = T1.RE_DB_OwnerShort
			WHERE T.ORAddrLine1 IS NOT NULL AND T.ORAddrLine1 NOT LIKE '%deceased%' AND T.ORLink='' AND T1.[Convert]='Yes'
			---233,890/  AFTER CHART: 233,888
END;
 

BEGIN--CREATE IMP OF UNIQUE ADDRESS 

		BEGIN
			SELECT DISTINCT [OwnerID]
			  ,[rC_Bios__Unique_MD5__c]
			  ,[rC_Bios__Unique_MD5__c] AS [rC_Bios__External_Id__c]
			  ,[Name]
			  ,[AddrLine1] AS [rC_Bios__Street_Line_1__c]
			  ,[AddrLine2] AS [rC_Bios__Street_Line_2__c]
			  ,[AddrCity] AS [rC_Bios__City__c]
			  ,[AddrState] AS [rC_Bios__State__c]
			  ,[AddrZIP] AS	 [rC_Bios__Postal_Code__c]
			  ,[AddrCountry] AS [rC_Bios__Country__c]
			INTO [SUTTER_MGEA].[IMP].[ADDRESS]		
			FROM [SUTTER_MGEA].[TBL].[Address_MD5]
			--179,518
		END;	
		
		BEGIN -- TEST FOR DUPLICATE EXT ID
			SELECT *
			FROM [SUTTER_MGEA].[IMP].[ADDRESS]
			WHERE [rC_Bios__External_Id__c] IN (SELECT [rC_Bios__External_Id__c] FROM [SUTTER_MGEA].[IMP].[ADDRESS] 
												GROUP BY [rC_Bios__External_Id__c] HAVING COUNT(*) >1)
		END;
		
		BEGIN --TEST ADDR LENGTH
			SELECT NAME, LEFT(NAME, 80) sNAME, *
			FROM [SUTTER_MGEA].[IMP].[ADDRESS]
			WHERE LEN(NAME)>80
			
			UPDATE [SUTTER_MGEA].[IMP].[ADDRESS]
			SET NAME = LEFT(NAME, 80) 
			WHERE LEN(NAME)>80
			--616
		END;
		
		BEGIN --REPLACE DOUBLE QUOTES TO SINGLE QUOTES
			-- select [name] from [SUTTER_MGEA].[IMP].[ADDRESS] where [name] like '%"%'
			update [SUTTER_MGEA].[IMP].[ADDRESS] set [name]=REPLACE([name],'"','''') where [name] like '%"%'
			-- select [rC_Bios__Street_Line_1__c] from [SUTTER_MGEA].[IMP].[ADDRESS] where [rC_Bios__Street_Line_1__c] like '%"%'
			update [SUTTER_MGEA].[IMP].[ADDRESS]  set [rC_Bios__Street_Line_1__c]=REPLACE([rC_Bios__Street_Line_1__c],'"','''') where [rC_Bios__Street_Line_1__c] like '%"%'
			-- select [rC_Bios__Street_Line_2__c] from [SUTTER_MGEA].[IMP].[ADDRESS] where [rC_Bios__Street_Line_2__c] like '%"%'
			update [SUTTER_MGEA].[IMP].[ADDRESS]  set [rC_Bios__Street_Line_2__c]=REPLACE([rC_Bios__Street_Line_2__c],'"','''') where [rC_Bios__Street_Line_2__c] like '%"%'
			-- select [rC_Bios__City__c] from [SUTTER_MGEA].[IMP].[ADDRESS] where [rC_Bios__City__c] like '%"%'
			update [SUTTER_MGEA].[IMP].[ADDRESS]  set [rC_Bios__City__c]=REPLACE([rC_Bios__City__c],'"','''') where [rC_Bios__City__c] like '%"%'
			-- select [rC_Bios__State__c] from [SUTTER_MGEA].[IMP].[ADDRESS] where [rC_Bios__State__c] like '%"%'
			update [SUTTER_MGEA].[IMP].[ADDRESS]  set [rC_Bios__State__c]=REPLACE([rC_Bios__State__c],'"','''') where [rC_Bios__State__c] like '%"%'
			-- select [rC_Bios__Postal_Code__c] from [SUTTER_MGEA].[IMP].[ADDRESS] where [rC_Bios__Postal_Code__c] like '%"%'
			update [SUTTER_MGEA].[IMP].[ADDRESS]  set [rC_Bios__Postal_Code__c]=REPLACE([rC_Bios__Postal_Code__c],'"','''') where [rC_Bios__Postal_Code__c] like '%"%'
			-- select [rC_Bios__Country__c] from [SUTTER_MGEA].[IMP].[ADDRESS] where [rC_Bios__Country__c] like '%"%'
			update [SUTTER_MGEA].[IMP].[ADDRESS]  set [rC_Bios__Country__c]=REPLACE([rC_Bios__Country__c],'"','''') where [rC_Bios__Country__c] like '%"%'
		END;
		
END;	
 



/*

 
--rC_Bios_Addres c

	--tbl.address of all adr.  --(HASHBYTES) http://www.bidn.com/blogs/TomLannen/bidn-blog/2265/using-hashbytes-to-compare-columns  

			SELECT [ASPCA_migration].dbo.OwnerId() as OwnerId, 
		 
			SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', isnull(UPPER(T.[ADDRESS3]),' ')+isnull(UPPER(T.[ADDRESS2]),' ')+
					ISNULL(UPPER(T.[CITY]),' ')+isnull(UPPER(T.[STATE]),' ')+
					isnull(case when T.[ZIP_PLUS4] is null then UPPER(T.ZIP) else UPPER(T.ZIP+'-'+ T.ZIP_PLUS4) end,' ')+
					isnull(UPPER(T.[COUNTRY_ZIP]),' ')+isnull(UPPER(T.[COUNTRY]),' '))), 3, 50) as rC_Bios__Unique_MD5__c,
			
			isnull(T.[ADDRESS3], '') + case T.ADDRESS3 when null then '' else ', ' end + isnull(T.[CITY], '') + case T.[CITY] when null then '' else ', ' end + 
			isnull(T.[STATE], '') + case T.[STATE] when null then '' else ', ' end + isnull(T.[ZIP], '') + 
			CASE T.[ZIP] when null then '' else ', ' end +
			isnull(T.[COUNTRY], null ) as Name, 
		
			T.ADDRESS3 as rC_Bios__Street_Line_1__c, 
			T.ADDRESS2 as rC_Bios__Street_Line_2__c, 
			T.CITY rC_Bios__City__c, 
			T.[STATE] rC_Bios__State__c, 
			case when T.COUNTRY_ZIP is not null then T.COUNTRY_ZIP when T.ZIP is not null and T.ZIP_PLUS4 is not null then t.zip +'-'+ T.ZIP_PLUS4 else T.zip end rC_Bios__Postal_Code__c,
			T.COUNTRY rC_Bios__Country__c,
			cast('' as nvarchar(255)) [rC_Bios__External_Id__c],
			T.ACCOUNT_ID as refACCOUNT_ID, 
			T.ALT_ADDRESS_ID as refALT_ADDRESS_ID
			
	 		INTO [ASPCA_MIGRATION].TBL.addresses  -- drop table [ASPCA_migration].tbl.addresses   select count(*) from [ASPCA_migration].tbl.addresses
			
				FROM [aspca_data].dbo.ALT_ADDRESS T
				INNER JOIN [aspca_migration].dbo.CHART_ADDRESS_TYPE_STATUS c1 on T.ADDRESS_TYPE = c1.ADDRESS_TYPE AND T.ADDRESS_STATUS = c1.ADDRESS_STATUS
				WHERE c1.[Convert]='Yes' 
				ORDER BY T.ACCOUNT_ID, T.ALT_ADDRESS_ID
 
			--count: 8,475,437

			--create index
	 		CREATE CLUSTERED INDEX TBL_addresses_idx ON ASPCA_MIGRATION.TBL.addresses(refALT_ADDRESS_ID)   -- e.g. [dbo].HHC_Address(ID)
			ON TABLES
			
			EXEC sp_help 'ASPCA_MIGRATION.TBL.addresses'
				GO
 
  
				 
		--IMP table with distinct values. 
		
                SELECT DISTINCT
                        [OwnerId] ,
                        CAST([rC_Bios__Unique_MD5__c] AS NVARCHAR(50)) AS [rC_Bios__Unique_MD5__c] ,
                        [Name] ,
                        [rC_Bios__Street_Line_1__c] ,
                        [rC_Bios__Street_Line_2__c] ,
                        [rC_Bios__City__c] ,
                        [rC_Bios__State__c] ,
                        [rC_Bios__Postal_Code__c] ,
                        [rC_Bios__Country__c]
                INTO    [ASPCA_MIGRATION].IMP.[ADDRESS]	-- drop table [aspca_migration].IMP.Bios_Address   -- select count(*) from [aspca_migration].IMP.Bios_Address 
                FROM    [ASPCA_MIGRATION].[TBL].[addresses]
				
				--count: 		 
		 
				--check dupes
				select * from [aspca_migration].IMP.[ADDRESS] -- select count(*) from [irc_pidi_migration].IMP.Bios_Address where rC_Bios__External_Id__c is null
					where rC_Bios__Unique_MD5__c in (	select rC_Bios__Unique_MD5__c 
														from [aspca_migration].IMP.[ADDRESS]
														group by rC_Bios__Unique_MD5__c 
														having count(*)>1)
    
				select TOP 10000 * from [aspca_migration].IMP.[ADDRESS]a

                    DELETE  [ASPCA_MIGRATION].IMP.[ADDRESS]
                    WHERE   rC_Bios__Unique_MD5__c = 'e79482ff942885487e710db9bfd95dae'
                            AND rC_Bios__City__c = 'Buffalo'
                    DELETE  [ASPCA_MIGRATION].IMP.[ADDRESS]
                    WHERE   rC_Bios__Unique_MD5__c = 'f0085b90c1683d82d1a37aa896925cc5'
                            AND rC_Bios__City__c = 'Sydney'
                    DELETE  [ASPCA_MIGRATION].IMP.[ADDRESS]
                    WHERE   rC_Bios__Unique_MD5__c = '169c3b8a084297a19d7d9de6f41809c4'
                            AND rC_Bios__City__c = 'Ne Salem'

					select count(*) c from [aspca_migration].IMP.[ADDRESS]

			--updates
			-- select name, len(name) m, left(name, 80)  from [aspca_migration].IMP.[ADDRESS] where len(Name) > 80	
			update [aspca_migration].IMP.[ADDRESS] set Name =left(Name, 80) where len(Name) > 80	
			GO
			-- select * from [aspca_migration].IMP.[ADDRESS] where name is null or name =''
			update [aspca_migration].IMP.[ADDRESS] set Name =  rC_Bios__Street_Line_1__c WHERE name IS NULL OR name =''	
			GO

			-- select * from [aspca_migration].IMP.[ADDRESS] where name is null or name =''
			update [aspca_migration].IMP.[ADDRESS] set Name = 'Unknown' WHERE name IS NULL OR name =''	
			GO
		
			-- select [name] from aspca_migration.IMP.[ADDRESS] where [name] like '%"%'
			update [aspca_migration].IMP.[ADDRESS]  set [name]=REPLACE([name],'"','''') where [name] like '%"%'
			GO
			-- select [rC_Bios__Street_Line_1__c] from aspca_migration.IMP.[ADDRESS] where [rC_Bios__Street_Line_1__c] like '%"%'
			update [aspca_migration].IMP.[ADDRESS]  set [rC_Bios__Street_Line_1__c]=REPLACE([rC_Bios__Street_Line_1__c],'"','''') where [rC_Bios__Street_Line_1__c] like '%"%'
			GO
			-- select [rC_Bios__Street_Line_2__c] from aspca_migration.IMP.[ADDRESS] where [rC_Bios__Street_Line_2__c] like '%"%'
			update [aspca_migration].IMP.[ADDRESS]  set [rC_Bios__Street_Line_2__c]=REPLACE([rC_Bios__Street_Line_2__c],'"','''') where [rC_Bios__Street_Line_2__c] like '%"%'
			GO	
			-- select [rC_Bios__City__c] from aspca_migration.IMP.[ADDRESS] where [rC_Bios__City__c] like '%"%'
			update [aspca_migration].IMP.[ADDRESS]  set [rC_Bios__City__c]=REPLACE([rC_Bios__City__c],'"','''') where [rC_Bios__City__c] like '%"%'
			GO		
			-- select [rC_Bios__State__c] from aspca_migration.IMP.[ADDRESS] where [rC_Bios__State__c] like '%"%'
			update [aspca_migration].IMP.[ADDRESS]  set [rC_Bios__State__c]=REPLACE([rC_Bios__State__c],'"','''') where [rC_Bios__State__c] like '%"%'
			GO	
			-- select [rC_Bios__Postal_Code__c] from aspca_migration.IMP.[ADDRESS] where [rC_Bios__Postal_Code__c] like '%"%'
			update [aspca_migration].IMP.[ADDRESS]  set [rC_Bios__Postal_Code__c]=REPLACE([rC_Bios__Postal_Code__c],'"','''') where [rC_Bios__Postal_Code__c] like '%"%'
			GO	
			-- select [rC_Bios__Country__c] from aspca_migration.IMP.[ADDRESS] where [rC_Bios__Country__c] like '%"%'
			update [aspca_migration].IMP.[ADDRESS]  set [rC_Bios__Country__c]=REPLACE([rC_Bios__Country__c],'"','''') where [rC_Bios__Country__c] like '%"%'
			GO		

			--create index
	 		CREATE CLUSTERED INDEX IMP_ADDRESS_idx ON ASPCA_MIGRATION.IMP.[ADDRESS](rC_Bios__Unique_MD5__c)   -- e.g. [dbo].HHC_Address(ID)
			ON IMPORTS
			
		
			USE ASPCA_MIGRATION
			GO		
			EXEC sp_help 'ASPCA_MIGRATION.IMP.[ADDRESS]'
			GO
 

				-- XTR check duplicate 
					select id, rC_Bios__External_Id__c, CreatedDate, [Name], [rC_Bios__Street_Line_1__c],[rC_Bios__Street_Line_2__c], [rC_Bios__City__c]
					from [ASPCA_migration].[xtr].[rC_Bios_Address]     
					where rC_Bios__External_Id__c in (	select rC_Bios__External_Id__c 
														from [ASPCA_migration].[dbo].[xtr_rC_Bios_Address]
														group by rC_Bios__External_ID__c 
														having count(*)>1)
						 --and CreatedById!='005f00000011mFBAAY'
					order by rC_Bios__External_Id__c, CreatedDate


  
	SELECT     [ASPCA_migration].IMP.Bios_Address.*, [ASPCA_migration].xtr.rC_Bios_Address.rC_Bios__External_Id__c AS xtrAddr
	into [ASPCA_migration].IMP.Bios_Address_xcp1
	FROM         [ASPCA_migration].IMP.Bios_Address LEFT OUTER JOIN
                      [ASPCA_migration].xtr.rC_Bios_Address ON 
                      [ASPCA_migration].IMP.Bios_Address.rC_Bios__External_Id__c = [ASPCA_migration].xtr.rC_Bios_Address.rC_Bios__External_Id__c
	WHERE     ([ASPCA_migration].xtr.rC_Bios_Address.rC_Bios__External_Id__c IS NULL) OR
                      ([ASPCA_migration].xtr.rC_Bios_Address.rC_Bios__External_Id__c
					  					   = N'')


  
		*/