USE SUTTER_MGEA_DATA
GO


BEGIN
		DROP TABLE SUTTER_MGEA_MIGRATION.TBL.NOTE
END


BEGIN--NOTES

	--CONSTITUENT NOTES, ORG CONSTITUENTS
		SELECT	ParentId= X1.ID --NEED THE ACCOUNT AND CONTACT SF ID.  ADD CASE FOR KEYIND. 
				,Title=    	  CASE WHEN T1.RE_DB_OwnerShort IS NOT NULL THEN  COALESCE(' ' + LTRIM(RTRIM(T1.RE_DB_OwnerShort)), '') ELSE ' ' END + CHAR(10) +
							+ CASE WHEN T1.NoteType IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.NoteType)), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.NoteDate IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.NoteDate)), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.NoteDesc IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.NoteDesc)), '') ELSE '' END
				
				,Body =    	  CASE WHEN T1.NoteAuth IS NOT NULL THEN  COALESCE('Author: ' + LTRIM(RTRIM(T1.NoteAuth)), '') ELSE 'Author: N/A' END + CHAR(10) +
							+ CASE WHEN T1.NoteTitle IS NOT NULL THEN COALESCE('; Title: ' + LTRIM(RTRIM(T1.NoteTitle)), '') ELSE '; Title: N/A' END + CHAR(10) +
							+ CASE WHEN T1.NoteDesc IS NOT NULL THEN COALESCE('; Desc: ' + LTRIM(RTRIM(T1.NoteDesc)), '') ELSE '; Desc: N/A' END + CHAR(10) +
							+ CASE WHEN T1.NoteNotes IS NOT NULL THEN COALESCE('; Notes: ' + LTRIM(RTRIM(T1.NoteNotes)), '') ELSE '; Notes: N/A' END
				--reference
				,'Constituents_Org' AS zrefrecordSource
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.ImportID AS zrefImportID
				,X1.EXTERNAL_ID__C zrefExternalID
	
		INTO SUTTER_MGEA_MIGRATION.TBL.NOTE
	
		FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Notes_v T1
		INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T2 ON T1.ImportID=T2.ImportID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	    INNER JOIN SUTTER_MGEA_MIGRATION.XTR.ACCOUNT X1 ON T2.RE_DB_OwnerShort+'-'+T2.ImportID=X1.EXTERNAL_ID__C
		WHERE T2.KeyInd='O'
		
		UNION ALL 

	--CONSTITUENT NOTES, ORG CONSTITUENTS
		SELECT	ParentId= X1.ID --NEED THE ACCOUNT AND CONTACT SF ID.  ADD CASE FOR KEYIND. 
				,Title=    	  CASE WHEN T1.RE_DB_OwnerShort IS NOT NULL THEN  COALESCE(' ' + LTRIM(RTRIM(T1.RE_DB_OwnerShort)), '') ELSE ' ' END + CHAR(10) +
							+ CASE WHEN T1.NoteType IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.NoteType)), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.NoteDate IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.NoteDate)), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.NoteDesc IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.NoteDesc)), '') ELSE '' END

				,Body =    	  CASE WHEN T1.NoteAuth IS NOT NULL THEN  COALESCE('Author: ' + LTRIM(RTRIM(T1.NoteAuth)), '') ELSE 'Author: N/A' END + CHAR(10) +
							+ CASE WHEN T1.NoteTitle IS NOT NULL THEN COALESCE('; Title: ' + LTRIM(RTRIM(T1.NoteTitle)), '') ELSE '; Title: N/A' END + CHAR(10) +
							+ CASE WHEN T1.NoteDesc IS NOT NULL THEN COALESCE('; Desc: ' + LTRIM(RTRIM(T1.NoteDesc)), '') ELSE '; Desc: N/A' END + CHAR(10) +
							+ CASE WHEN T1.NoteNotes IS NOT NULL THEN COALESCE('; Notes: ' + LTRIM(RTRIM(T1.NoteNotes)), '') ELSE '; Notes: N/A' END
				,'Constituents_Ind' AS recordSource
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.ImportID AS zrefImportID
				,X1.EXTERNAL_ID__C zrefExternalID
		FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Notes_v T1
		INNER JOIN	SUTTER_MGEA_DATA.dbo.HC_Constituents_v T2 ON T1.ImportID=T2.ImportID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	    INNER JOIN SUTTER_MGEA_MIGRATION.XTR.CONTACT X1 ON T2.RE_DB_OwnerShort+'-'+T2.ImportID=X1.EXTERNAL_ID__C
		WHERE T2.KeyInd='I'
		
		UNION ALL 
		
	--CONS_TRIBUTES WITH NO GIFTS LINKED. CONS ORG
		SELECT 
				ParentId=X1.ID
				,Title= CASE WHEN T1.TRType IS NOT NULL THEN COALESCE('Type: ' + cast(T1.TRType AS VARCHAR(MAX)), '') ELSE 'Type: N/A' END + CHAR(10) +
						+ CASE WHEN T1.TRDateFrom IS NOT NULL THEN COALESCE('; Date: ' + (T1.TRDateFrom), '') ELSE '; Date: N/A' END				
				
				,Body= CASE WHEN T1.TRDesc IS NOT NULL THEN COALESCE('Desc: ' + cast(T1.TRDesc AS VARCHAR(MAX)), '') ELSE 'Desc: N/A' END + CHAR(10) +
						+ CASE WHEN T1.TRNotes IS NOT NULL THEN COALESCE('; Notes: ' + cast(T1.TRNotes AS VARCHAR(MAX)), '') ELSE '; Notes: N/A' END
				
				
				,'ConsTributes_Org' AS recordSource		 
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.ImportID AS zrefImportID
				,X1.EXTERNAL_ID__C zrefExternalID

		FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Tribute_v T1
		LEFT JOIN SUTTER_MGEA_DATA.dbo.HC_Gift_Tribute_v T2 ON T1.TRImpID=T2.TribImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
		LEFT JOIN SUTTER_MGEA_DATA.Dbo.HC_Constituents_v T3 ON T1.ImportID=T3.ImportID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
		LEFT JOIN SUTTER_MGEA_MIGRATION.XTR.ACCOUNT X1 ON (T1.RE_DB_OwnerShort+'-'+T1.ImportID)=X1.EXTERNAL_ID__C
		WHERE T2.TribImpID IS NULL AND T3.KeyInd='O'
		
		UNION ALL 

	--CONS_TRIBUTES WITH NO GIFTS LINKED. CONS IND
		SELECT 
				ParentId=X1.ID
				,Title= CASE WHEN T1.TRType IS NOT NULL THEN COALESCE('Type: ' + cast(T1.TRType AS VARCHAR(MAX)), '') ELSE 'Type: N/A' END + CHAR(10) +
						+ CASE WHEN T1.TRDateFrom IS NOT NULL THEN COALESCE('; Date: ' + (T1.TRDateFrom), '') ELSE '; Date: N/A' END				
				
				,Body= CASE WHEN T1.TRDesc IS NOT NULL THEN COALESCE('Desc: ' + cast(T1.TRDesc AS VARCHAR(MAX)), '') ELSE 'Desc: N/A' END + CHAR(10) +
						+ CASE WHEN T1.TRNotes IS NOT NULL THEN COALESCE('; Notes: ' + cast(T1.TRNotes AS VARCHAR(MAX)), '') ELSE '; Notes: N/A' END
				
				
				,'ConsTributes_Org' AS recordSource		 
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.ImportID AS zrefImportID
				,X1.EXTERNAL_ID__C zrefExternalID

		FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Tribute_v T1
		LEFT JOIN SUTTER_MGEA_DATA.dbo.HC_Gift_Tribute_v T2 ON T1.TRImpID=T2.TribImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
		LEFT JOIN SUTTER_MGEA_DATA.Dbo.HC_Constituents_v T3 ON T1.ImportID=T3.ImportID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
		LEFT JOIN SUTTER_MGEA_MIGRATION.XTR.CONTACT X1 ON (T1.RE_DB_OwnerShort+'-'+T1.ImportID)=X1.EXTERNAL_ID__C
		WHERE T2.TribImpID IS NULL AND T3.KeyInd='I'
		
		UNION ALL 
		
		--PROPOSAL NOTES 
		SELECT	ParentId= X1.ID
				,Title= T1.RE_DB_OwnerShort +'- '+T1.PRNoteType +'- '+ SUBSTRING(T1.PRNoteDate, 5,2) +'/'+ SUBSTRING(T1.PRNoteDate, 7,2) +'/'+LEFT(T1.PRNoteDate , 4)
				,Body =    	  CASE WHEN T1.PRNoteAuth IS NOT NULL THEN  COALESCE('Author: ' + LTRIM(RTRIM(T1.PRNoteAuth)), '') ELSE 'Author: N/A' END + CHAR(10) +
							+ CASE WHEN T1.PRNoteTitle IS NOT NULL THEN COALESCE('; Title: ' + LTRIM(RTRIM(T1.PRNoteTitle)), '') ELSE '; Title: N/A' END + CHAR(10) +
							+ CASE WHEN T1.PRNoteDesc IS NOT NULL THEN COALESCE('; Desc: ' + LTRIM(RTRIM(T1.PRNoteDesc)), '') ELSE '; Desc: N/A' END + CHAR(10) +
							+ CASE WHEN T1.PRNoteNotes IS NOT NULL THEN COALESCE('; Notes: ' + LTRIM(RTRIM(CAST(T1.PRNoteNotes AS NVARCHAR(4000)))), '') ELSE '; Notes: N/A' END
				 ,'Proposals' AS recordSource
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.PRLink AS zrefImportID
				,X1.EXTERNAL_ID__C zrefExternalID
		FROM SUTTER_MGEA_DATA.dbo.HC_Proposal_Notes_v T1
		INNER JOIN SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY X1 ON (T1.RE_DB_OwnerShort+'-PP-'+T1.PRLink)=X1.External_ID__c

		UNION ALL 
		
		--GIFT NOTES (PARENT)
		SELECT	ParentId= X1.ID
				,Title=    	  CASE WHEN T1.RE_DB_OwnerShort IS NOT NULL THEN  COALESCE(' ' + LTRIM(RTRIM(T1.RE_DB_OwnerShort)), '') ELSE ' ' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteType IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.GFNoteType)), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDate IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(SUBSTRING(T1.GFNoteDate, 5,2) +'/'+ SUBSTRING(T1.GFNoteDate, 7,2) +'/'+LEFT(T1.GFNoteDate , 4))), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDesc IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.GFNoteDesc)), '') ELSE '' END
				
				
				,Body =    	  CASE WHEN T1.GFNoteAuth IS NOT NULL THEN  COALESCE('Author: ' + LTRIM(RTRIM(T1.GFNoteAuth)), '') ELSE 'Author: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteTitle IS NOT NULL THEN COALESCE('; Title: ' + LTRIM(RTRIM(T1.GFNoteTitle)), '') ELSE '; Title: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDesc IS NOT NULL THEN COALESCE('; Desc: ' + LTRIM(RTRIM(T1.GFNoteDesc)), '') ELSE '; Desc: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteNotes IS NOT NULL THEN COALESCE('; Notes: ' + LTRIM(RTRIM(CAST(T1.GFNoteNotes AS NVARCHAR(4000)))), '') ELSE '; Notes: N/A' END
				,'GiftsNotes_parent' AS recordSource	
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.GFLink AS zrefImportID
			    ,X1.External_ID__c AS zrefExternalID
		FROM SUTTER_MGEA_DATA.dbo.HC_Gift_Notes_v T1
		INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Gift_SplitGift_v T2 ON T1.GFLink=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	    INNER JOIN SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT T3 ON (T2.RE_DB_OwnerShort+'-'+T2.RE_DB_Tbl+'-'+T2.GSplitImpID) =T3.External_Id__c
	    INNER JOIN SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY X1 ON T3.External_Id__c=X1.External_ID__c
	     
	    UNION ALL
	    
		--GIFT NOTES (TRANSACTION)
		SELECT	ParentId= X1.ID
				,Title=    	  CASE WHEN T1.RE_DB_OwnerShort IS NOT NULL THEN  COALESCE(' ' + LTRIM(RTRIM(T1.RE_DB_OwnerShort)), '') ELSE ' ' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteType IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.GFNoteType)), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDate IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(SUBSTRING(T1.GFNoteDate, 5,2) +'/'+ SUBSTRING(T1.GFNoteDate, 7,2) +'/'+LEFT(T1.GFNoteDate , 4))), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDesc IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.GFNoteDesc)), '') ELSE '' END

				,Body =    	  CASE WHEN T1.GFNoteAuth IS NOT NULL THEN  COALESCE('Author: ' + LTRIM(RTRIM(T1.GFNoteAuth)), '') ELSE 'Author: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteTitle IS NOT NULL THEN COALESCE('; Title: ' + LTRIM(RTRIM(T1.GFNoteTitle)), '') ELSE '; Title: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDesc IS NOT NULL THEN COALESCE('; Desc: ' + LTRIM(RTRIM(T1.GFNoteDesc)), '') ELSE '; Desc: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteNotes IS NOT NULL THEN COALESCE('; Notes: ' + LTRIM(RTRIM(CAST(T1.GFNoteNotes AS NVARCHAR(4000)))), '') ELSE '; Notes: N/A' END
				,'GiftsNotes_trx' AS recordSource	
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.GFLink AS zrefImportID
			    ,X1.External_ID__c AS zrefExternalID
		FROM SUTTER_MGEA_DATA.dbo.HC_Gift_Notes_v T1
		INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Gift_SplitGift_v T2 ON T1.GFLink=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	    INNER JOIN SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_TRANSACTION T3 ON (T2.RE_DB_OwnerShort+'-'+T2.RE_DB_Tbl+'-'+T2.GSplitImpID) =T3.External_Id__c
	    INNER JOIN SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY X1 ON T3.External_Id__c=X1.External_ID__c
	   
		UNION ALL
		
		--GIFT ADJUSTMENTS (Parent)	
		SELECT	ParentId= X1.ID
				,Title = 'Adjustment Notes: '+T1.RE_DB_OwnerShort +'- Date: '+ T1.AdjustmentDate+'- Amount: '+CAST(T1.AdjustedAmount AS NVARCHAR(20))
				,Body =    	  CASE WHEN T1.AdjustedPreviousFundID IS NOT NULL THEN  COALESCE('Previous FundID: ' + LTRIM(RTRIM(T1.AdjustedPreviousFundID)), '') ELSE 'Previous FundID: N/A' END + CHAR(10) +
							+ CASE WHEN T1.AdjustedPreviousFundDescription IS NOT NULL THEN COALESCE('; Previous FundDesc: ' + LTRIM(RTRIM(T1.AdjustedPreviousFundDescription)), '') ELSE '; Previous FundDesc: N/A' END + CHAR(10) +
							+ CASE WHEN T1.AdjustedPreviousFundSplitAmount IS NOT NULL THEN COALESCE('; Previous Fund Amount: ' + LTRIM(RTRIM(T1.AdjustedPreviousFundSplitAmount)), '') ELSE '; Previous Fund Amount: N/A' END + CHAR(10) +
							+ CASE WHEN T1.AdjustmentReason IS NOT NULL THEN COALESCE('; Adjustment Reason: ' + LTRIM(RTRIM(T1.AdjustmentReason)), '') ELSE '; Adjustment Reason: N/A' END + CHAR(10) +
							+ CASE WHEN T1.AdjustmentNotes IS NOT NULL THEN COALESCE('; Adjustment Notes: ' + LTRIM(RTRIM(CAST(T1.AdjustmentNotes AS NVARCHAR(4000)))), '') ELSE '; Adjustment Notes: N/A' END 
				,'Adjustments_parent' AS recordSource
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.GFImpID AS zrefImportID
			    ,X1.External_ID__c AS zrefExternalID
						
		FROM SUTTER_MGEA_DATA.dbo.HC_Gift_Adjustments_v T1
		INNER JOIN	SUTTER_MGEA_DATA.dbo.HC_Gift_SplitGift_v T2 ON T1.GFImpID=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	    INNER JOIN SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT T3 ON (T2.RE_DB_OwnerShort+'-'+T2.RE_DB_Tbl+'-'+T2.GSplitImpID) =T3.External_Id__c
	    INNER JOIN SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY X1 ON T3.External_Id__c=X1.External_ID__c

		UNION ALL
		
		--GIFT ADJUSTMENTS (Transaction)	
		SELECT	ParentId= X1.ID
				,Title = 'Adjustment Notes: '+T1.RE_DB_OwnerShort +'- Date: '+ T1.AdjustmentDate+'- Amount: '+CAST(T1.AdjustedAmount AS NVARCHAR(20))
				,Body =    	  CASE WHEN T1.AdjustedPreviousFundID IS NOT NULL THEN  COALESCE('Previous FundID: ' + LTRIM(RTRIM(T1.AdjustedPreviousFundID)), '') ELSE 'Previous FundID: N/A' END + CHAR(10) +
							+ CASE WHEN T1.AdjustedPreviousFundDescription IS NOT NULL THEN COALESCE('; Previous FundDesc: ' + LTRIM(RTRIM(T1.AdjustedPreviousFundDescription)), '') ELSE '; Previous FundDesc: N/A' END + CHAR(10) +
							+ CASE WHEN T1.AdjustedPreviousFundSplitAmount IS NOT NULL THEN COALESCE('; Previous Fund Amount: ' + LTRIM(RTRIM(T1.AdjustedPreviousFundSplitAmount)), '') ELSE '; Previous Fund Amount: N/A' END + CHAR(10) +
							+ CASE WHEN T1.AdjustmentReason IS NOT NULL THEN COALESCE('; Adjustment Reason: ' + LTRIM(RTRIM(T1.AdjustmentReason)), '') ELSE '; Adjustment Reason: N/A' END + CHAR(10) +
							+ CASE WHEN T1.AdjustmentNotes IS NOT NULL THEN COALESCE('; Adjustment Notes: ' + LTRIM(RTRIM(CAST(T1.AdjustmentNotes AS NVARCHAR(4000)))), '') ELSE '; Adjustment Notes: N/A' END 
				,'Adjustments_trx' AS recordSource
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.GFImpID AS zrefImportID
			    ,X1.External_ID__c AS zrefExternalID
						
		FROM SUTTER_MGEA_DATA.dbo.HC_Gift_Adjustments_v T1
		INNER JOIN	SUTTER_MGEA_DATA.dbo.HC_Gift_SplitGift_v T2 ON T1.GFImpID=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	    INNER JOIN SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_TRANSACTION T3 ON (T2.RE_DB_OwnerShort+'-'+T2.RE_DB_Tbl+'-'+T2.GSplitImpID) =T3.External_Id__c
	    INNER JOIN SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY X1 ON T3.External_Id__c=X1.External_ID__c

		UNION ALL
		
		
		--GIFT AMENDMENTS (parent)
		SELECT	ParentId= X1.ID
				,Title = 'Amendment Notes: '+T1.RE_DB_OwnerShort +'- Date: '+ T1.RGADate
				,Body =    	  CASE WHEN T1.AmendmentPreviousAmount IS NOT NULL THEN  COALESCE('Previous Amount: ' + LTRIM(RTRIM(T1.AmendmentPreviousAmount)), '') ELSE 'Previous Amount: N/A' END + CHAR(10) +
							+ CASE WHEN T1.RGAPrevFreq IS NOT NULL THEN COALESCE('; Previous Freq.: ' + LTRIM(RTRIM(T1.RGAPrevFreq)), '') ELSE '; Previous Freq.: N/A' END + CHAR(10) +
							+ CASE WHEN T1.RGAPrevFundID IS NOT NULL THEN COALESCE('; Previous FundID: ' + LTRIM(RTRIM(T1.RGAPrevFundID)), '') ELSE '; Previous FundID: N/A' END + CHAR(10) +
							+ CASE WHEN T1.RGAPrevCampID IS NOT NULL THEN COALESCE('; Previous CampID: ' + LTRIM(RTRIM(T1.RGAPrevCampID)), '') ELSE '; Previous CampID: N/A' END + CHAR(10) +
							+ CASE WHEN T1.RGAPrevAppID IS NOT NULL THEN COALESCE('; Previous AppID: ' + LTRIM(RTRIM(CAST(T1.RGAPrevAppID AS NVARCHAR(4000)))), '') ELSE '; Previous AppID: N/A'  END + CHAR(10) +
							+ CASE WHEN T1.RGAPrevPackID IS NOT NULL THEN COALESCE('; Previous PackID: ' + LTRIM(RTRIM(CAST(T1.RGAPrevPackID AS NVARCHAR(4000)))), '') ELSE '; Previous PackID: N/A'  END + CHAR(10) +
							+ CASE WHEN T1.RGANotes IS NOT NULL THEN COALESCE('; Amendment Notes: ' + LTRIM(RTRIM(CAST(T1.RGANotes AS NVARCHAR(4000)))), '') ELSE '; Amendment Notes: N/A' END 
				,'Amendments_parent' AS recordSource
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.RGALink AS zrefImportID
			    ,X1.External_ID__c AS zrefExternalID

		FROM SUTTER_MGEA_DATA.dbo.HC_Gift_Amendments_v T1
		INNER JOIN	SUTTER_MGEA_DATA.dbo.HC_Gift_SplitGift_v T2 ON T1.RGALink=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	    INNER JOIN SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT T3 ON (T2.RE_DB_OwnerShort+'-'+T2.RE_DB_Tbl+'-'+T2.GSplitImpID) =T3.External_Id__c
	    INNER JOIN SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY X1 ON T3.External_Id__c=X1.External_ID__c
		
		UNION ALL

		--GIFT AMENDMENTS (trx)
		SELECT	ParentId= X1.ID
				,Title = 'Amendment Notes: '+T1.RE_DB_OwnerShort +'- Date: '+ T1.RGADate
				,Body =    	  CASE WHEN T1.AmendmentPreviousAmount IS NOT NULL THEN  COALESCE('Previous Amount: ' + LTRIM(RTRIM(T1.AmendmentPreviousAmount)), '') ELSE 'Previous Amount: N/A' END + CHAR(10) +
							+ CASE WHEN T1.RGAPrevFreq IS NOT NULL THEN COALESCE('; Previous Freq.: ' + LTRIM(RTRIM(T1.RGAPrevFreq)), '') ELSE '; Previous Freq.: N/A' END + CHAR(10) +
							+ CASE WHEN T1.RGAPrevFundID IS NOT NULL THEN COALESCE('; Previous FundID: ' + LTRIM(RTRIM(T1.RGAPrevFundID)), '') ELSE '; Previous FundID: N/A' END + CHAR(10) +
							+ CASE WHEN T1.RGAPrevCampID IS NOT NULL THEN COALESCE('; Previous CampID: ' + LTRIM(RTRIM(T1.RGAPrevCampID)), '') ELSE '; Previous CampID: N/A' END + CHAR(10) +
							+ CASE WHEN T1.RGAPrevAppID IS NOT NULL THEN COALESCE('; Previous AppID: ' + LTRIM(RTRIM(CAST(T1.RGAPrevAppID AS NVARCHAR(4000)))), '') ELSE '; Previous AppID: N/A'  END + CHAR(10) +
							+ CASE WHEN T1.RGAPrevPackID IS NOT NULL THEN COALESCE('; Previous PackID: ' + LTRIM(RTRIM(CAST(T1.RGAPrevPackID AS NVARCHAR(4000)))), '') ELSE '; Previous PackID: N/A'  END + CHAR(10) +
							+ CASE WHEN T1.RGANotes IS NOT NULL THEN COALESCE('; Amendment Notes: ' + LTRIM(RTRIM(CAST(T1.RGANotes AS NVARCHAR(4000)))), '') ELSE '; Amendment Notes: N/A' END 
				,'Amendments_trx' AS recordSource
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.RGALink AS zrefImportID
			    ,X1.External_ID__c AS zrefExternalID

		FROM SUTTER_MGEA_DATA.dbo.HC_Gift_Amendments_v T1
		INNER JOIN	SUTTER_MGEA_DATA.dbo.HC_Gift_SplitGift_v T2 ON T1.RGALink=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	    INNER JOIN SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_TRANSACTION T3 ON (T2.RE_DB_OwnerShort+'-'+T2.RE_DB_Tbl+'-'+T2.GSplitImpID) =T3.External_Id__c
	    INNER JOIN SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY X1 ON T3.External_Id__c=X1.External_ID__c
		
		UNION ALL
		
		--ADDITIONAL GIFT TRIBUTES where SEQ>1 (PARENT)
		SELECT 
				ParentId= X1.ID 
				,Title = 'Additional Tribute Information'
										
				,Body= T1.TributeInfo
				,'GiftTributes_parent' AS recordSource	
			 	,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.GFTLINK AS zrefImportID
			    ,X1.External_ID__c AS zrefExternalID


		FROM SUTTER_MGEA_MIGRATION.TBL.GiftTribute_note T1 
		INNER JOIN	SUTTER_MGEA_DATA.dbo.HC_Gift_SplitGift_v T2 ON T1.GFTLINK=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	    INNER JOIN SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT T3 ON (T2.RE_DB_OwnerShort+'-'+T2.RE_DB_Tbl+'-'+T2.GSplitImpID) =T3.External_Id__c
	    INNER JOIN SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY X1 ON T3.External_Id__c=X1.External_ID__c
	
		UNION ALL
		
		--ADDITIONAL GIFT TRIBUTES where SEQ>1 (TRANSACTION)
		SELECT 
				ParentId= X1.ID    
				,Title = 'Additional Tribute Information'
										
				,Body= T1.TributeInfo
				,'GiftTributes_trx' AS recordSource	
			 	,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.GFTLINK AS zrefImportID
			    ,X1.External_ID__c AS zrefExternalID


		FROM SUTTER_MGEA_MIGRATION.TBL.GiftTribute_note T1 
		INNER JOIN	SUTTER_MGEA_DATA.dbo.HC_Gift_SplitGift_v T2 ON T1.GFTLINK=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	    INNER JOIN SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_TRANSACTION T3 ON (T2.RE_DB_OwnerShort+'-'+T2.RE_DB_Tbl+'-'+T2.GSplitImpID) =T3.External_Id__c
	    INNER JOIN SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY X1 ON T3.External_Id__c=X1.External_ID__c

END; 

BEGIN

	  UPDATE [SUTTER_MGEA_MIGRATION].[TBL].[NOTE] SET Body=REPLACE(Body,'"','''') where Body like '%"%'
	  UPDATE [SUTTER_MGEA_MIGRATION].[TBL].[NOTE] SET Title=REPLACE(Title,'"','''') where Title like '%"%'

	 
	  SELECT SUBSTRING(BODY,1,CHARINDEX('{',BODY,1)-1) +' <IMAGE> '
	  ,parentid, title, body, LEN(BODY) L FROM SUTTER_MGEA_MIGRATION.TBL.NOTE  --ORDER BY L DESC 
	  WHERE BODY LIKE '%rtf1\ansi%'  OR BODY LIKE '%rtf1\fbidis\%'
	
		UPDATE SUTTER_MGEA_MIGRATION.TBL.NOTE 
		SET BODY =  SUBSTRING(BODY,1,CHARINDEX('{',BODY,1)-1) +' <IMAGE> '
		WHERE BODY LIKE '%rtf1\ansi%'  OR BODY LIKE '%rtf1\fbidis\%'
	 
	  
END

BEGIN	
			
			DROP TABLE SUTTER_MGEA_MIGRATION.IMP.NOTE
			
			SELECT 	ParentId 
					,LTRIM(CAST(Title AS VARCHAR(MAX))) AS TITLE
					,CAST(Body AS VARCHAR(MAX)) AS BODY
					,zrefrecordSource
					,zrefREDB
					,zrefImportID
					,zrefExternalID
			INTO SUTTER_MGEA_MIGRATION.IMP.NOTE  
			FROM SUTTER_MGEA_MIGRATION.TBL.NOTE

END