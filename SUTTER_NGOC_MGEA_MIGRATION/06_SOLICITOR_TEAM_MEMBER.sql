

USE SUTTER_MGEA_MIGRATION
GO

BEGIN
	DROP TABLE SUTTER_MGEA_MIGRATION.IMP.SOLICITOR_TEAM
END;	

BEGIN
		SELECT  
			 OwnerID=dbo.fnc_OwnerID()
			,RE_ASRImpID__c= T.RE_DB_OwnerShort+'-'+T.ASRImpID
			,Solicitor__c=X1.ID
			,[Account__r:External_ID__c]=CASE WHEN (T2.KeyInd='O') THEN (T2.RE_DB_OwnerShort+'-'+T2.ImportID)
											 WHEN (T2.KeyInd='I') THEN (CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.RE_DB_OwnerShort+'-'+T3.HH_ImportID ELSE T.RE_DB_OwnerShort+'-'+T.ImportID END) END 
			,[Contact__r:External_ID__c]=CASE WHEN (T2.KeyInd='I') THEN (T2.RE_DB_OwnerShort+'-'+T2.ImportID) END 
			,CASE WHEN LEN(T.ASRDateFrom)=4 THEN '01/01/'+T.ASRDateFrom  
			      WHEN LEN(T.ASRDateFrom)=6 THEN SUBSTRING(T.ASRDateFrom,5,2)+'/01/'+LEFT(T.ASRDateFrom,4) 
				  WHEN LEN(T.ASRDateFrom)=8 THEN SUBSTRING(T.ASRDateFrom,5,2)+'/'+RIGHT(T.ASRDateFrom,2)+'/'+LEFT(T.ASRDateFrom,4)    
				  END AS Start_Date__c
			,CASE WHEN LEN(T.ASRDateTo)=4 THEN '01/01/'+T.ASRDateTo  
			      WHEN LEN(T.ASRDateTo)=6 THEN SUBSTRING(T.ASRDateTo,5,2)+'/01/'+LEFT(T.ASRDateTo,4) 
				  WHEN LEN(T.ASRDateTo)=8 THEN SUBSTRING(T.ASRDateTo,5,2)+'/'+RIGHT(T.ASRDateTo,2)+'/'+LEFT(T.ASRDateTo,4)    
				  END AS End_Date__c
			,T.ASRNotes AS Notes__c
			,T1.SOLICITOR_TEAM_TYPE AS Type__c
			,T.RE_DB_OwnerShort AS Affiliation__c
			--,CASE WHEN (CAST(T.ASRDateTo AS DATE))<GETDATE() THEN 'FALSE' ELSE 'TRUE' END AS Is_Active__c 
			,CASE WHEN (CAST((CASE WHEN LEN(T.ASRDateTo)=4 THEN '01/01/'+T.ASRDateTo  
						WHEN LEN(T.ASRDateTo)=6 THEN SUBSTRING(T.ASRDateTo,5,2)+'/01/'+LEFT(T.ASRDateTo,4) 
						WHEN LEN(T.ASRDateTo)=8 THEN SUBSTRING(T.ASRDateTo,5,2)+'/'+RIGHT(T.ASRDateTo,2)+'/'+LEFT(T.ASRDateTo,4) END) AS DATE)) < GETDATE() 
				  THEN 'FALSE' ELSE 'TRUE' END AS Is_Active__c
			--ref
			,T2.KeyInd zrefT2_KeyInd
			,T2.ImportID zrefT2_ImportId
			,T3.NonHHImpID AS zrefNonHHImpID
			,T3.HH_ImportID AS zrefHH_ImportID
			,T.ASRLink zref_RLink
			,T.ASRDateFrom zrefDateFrom
			,T.ASRDateTo zrefDateTo
			
			INTO SUTTER_MGEA_MIGRATION.IMP.SOLICITOR_TEAM
			FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Solicitor_v T
			INNER JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_ConsSolicitor T1 ON T.ASRLink=T1.ASRLink AND T.ASRType=T1.ASRType AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.XTR.USERS X1 ON T1.SF_User_Email=X1.Email
			INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Constituents_v T2 ON T.ImportID=T2.ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort  
			LEFT JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH T3 ON T.ImportID=T3.NoHH_ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			 
			 WHERE T.RE_DB_OwnerShort='SAFH' OR T.RE_DB_OwnerShort='SMCF' 
END;
 
  