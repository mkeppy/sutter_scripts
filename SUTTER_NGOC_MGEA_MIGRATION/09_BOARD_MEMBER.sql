USE SUTTER_MGEA_DATA
GO

BEGIN	
	DROP TABLE SUTTER_MGEA_MIGRATION.IMP.BOARD_MEMBER
END

BEGIN

				--BOARD MEMBER from CONSTITUENT ATTRIBUTE from CHART_CONS_ATTRIBUTES 
				SELECT 
				 External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.CAttrImpID)
				,[Account__r:External_Id__c]=CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.RE_DB_OwnerShort+'-'+T3.HH_ImportID ELSE T.RE_DB_OwnerShort+'-'+T.ImportID end
				,[Contact__r:External_Id__c]=T.RE_DB_OwnerShort+'-'+T.ImportID
			
				,T2.BOARDMEMBER_Active__c AS Active__c
				,T2.BOARDMEMBER_Type__c	 AS Type__c
				,T2.BOARDMEMBER_Notes__c AS Notes__c
				,T2.BOARDMEMBER_Committee__c AS Committee__c
				,CASE WHEN T.CAttrDate IS NULL THEN T2.BOARDMEMBER_StartDate__c ELSE T.CAttrDate END AS Start_Date__c
				,T2.BOARDMEMBER_EndDate__c  AS End_Date__c
				
				,T.RE_DB_OwnerShort AS Affiliation__c
				,'Cons Attributes' AS Origin__c

				--reference
				,T.RE_DB_OwnerShort AS zrefDBOwner
				,T3.ImportID zref_NoHH_ImportID
				,T3.HH_ImportID AS zref_HH_ImportID 
				,T1.KeyInd AS zrefKeyInd
				
		 		INTO SUTTER_MGEA_MIGRATION.IMP.BOARD_MEMBER
				FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_ConsAttributes T2 ON T.CAttrCat=T2.CAttrCat AND T.CAttrDesc=T2.CAttrDesc AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH T3 ON T.ImportID=T3.NoHH_ImportID AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.BOARDMEMBER_Active__c IS NOT NULL

			UNION ALL 

				--board members from cons codes 
				SELECT 
		 		 External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.ConsCodeImpID)
				,[Account__r:External_Id__c]=CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.RE_DB_OwnerShort+'-'+T3.HH_ImportID ELSE T.RE_DB_OwnerShort+'-'+T.ImportID end
				,[Contact__r:External_Id__c]=T.RE_DB_OwnerShort+'-'+T.ImportID
			
				,T2.BOARDMEMBER_Active__c AS Active__c
				,T2.BOARDMEMBER_Type__c	 AS Type__c
				,T2.BOARDMEMBER_Notes__c AS Notes__c
				,T2.BOARDMEMBER_Committee__c AS Committee__c
				,CASE WHEN T.ConsCodeDateFrom IS NULL AND T2.BOARDMEMBER_StartDate__c IS NOT null THEN T2.BOARDMEMBER_StartDate__c ELSE T.ConsCodeDateFrom END AS Start_Date__c
				,CASE WHEN T.ConsCodeDateto IS NULL AND T2.BOARDMEMBER_EndDate__c IS NOT NULL THEN T2.BOARDMEMBER_EndDate__c ELSE T.ConsCodeDateTo end  AS End_Date__c
				
				,T.RE_DB_OwnerShort AS Affiliation__c
				,'Cons Codes' AS Origin__c

				--reference
				,T.RE_DB_OwnerShort AS zrefDBOwner
				,T3.ImportID zref_NoHH_ImportID
				,T3.HH_ImportID AS zref_HH_ImportID 
				,T1.KeyInd AS zrefKeyInd
				
 
				FROM SUTTER_MGEA_DATA.dbo.HC_Cons_ConsCode_v T
				INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_ConstituencyCodes T2 ON T.ConsCode=T2.ConsCode AND T1.KeyInd=T2.KeyInD AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH T3 ON T.ImportID=T3.NoHH_ImportID AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.BOARDMEMBER_Type__c IS NOT NULL
			
			
END;				
 
 SELECT * FROM SUTTER_MGEA_DATA.dbo.HC_Cons_ConsCode_v
 SELECT * FROM SUTTER_MGEA_MIGRATION.dbo.CHART_ConstituencyCodes


BEGIN--CHECK DUPLICATES
		SELECT * 
		FROM SUTTER_MGEA_MIGRATION.IMP.BOARD_MEMBER
		WHERE External_ID__c IN (SELECT External_ID__c FROM SUTTER_MGEA_MIGRATION.IMP.BOARD_MEMBER GROUP BY External_ID__c HAVING COUNT(*)>1)
END
 
 
 
 SELECT * FROM SUTTER_MGEA_MIGRATION.tbl.Contact_HofH WHERE ImportID='000029528'