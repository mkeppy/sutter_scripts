USE SUTTER_MGEA_MIGRATION
GO

BEGIN
	DROP TABLE SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_parent_NEW
END	




BEGIN--PARENT OPPORTUNITIES
					SELECT  
					--IDs
						dbo.fnc_OwnerID() AS OwnerID         
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS External_Id__c
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS RE_GSplitImpID__c
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitGFImpID AS RE_GFImpID__c
/*EXT_ID*/				,T.[ACCOUNT:External_Id__c]
						,T.GFDate	AS	CloseDate
						,T.[GSplitRE_DB_OwnerShort]+' - '+T.Parent_Giving_RecordType+' - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10)) NAME
					
						,dbo.fnc_RecordType('Opportunity_'+T.Parent_Giving_RecordType) AS RecordTypeID
					--CampaignId
/*EXT_ID*/				,T.CAMPAIGN_External_Id__c	AS [CAMPAIGN:External_Id__c]
						,CASE WHEN (T.GFType LIKE '%pledge%' AND  GFPledgeBalance>0) OR (T.GFType ='Recurring Gift' AND T.GFStatus='Active') 
							  THEN 'Open' ELSE 'Completed' END AS StageName
						,T.rC_Giving__Activity_Type__c
						,T.[GSplitRE_DB_OwnerShort] AS rC_Giving__Affiliation__c 
				
			 		--amounts
						,CASE WHEN (T.GFType='Pledge' OR T.GFType='MG Pledge') THEN (T.GFTAmt/T.GFInsNumPay) ELSE T.GSplitAmt END AS rC_Giving__Giving_Amount__c
						,CASE WHEN T.GFType ='Gift-in-Kind' THEN NULL ELSE T.GSplitAmt END AS rC_Giving__Expected_Giving_Amount__c
						,CASE WHEN T.GFType ='Gift-in-Kind' THEN NULL ELSE T.rC_Giving__Current_Giving_Amount__c END AS rC_Giving__Current_Giving_Amount__c
/*InKind*/				,CASE WHEN T.GFType ='Gift-in-Kind' THEN T.GSplitAmt ELSE NULL END AS rC_Giving__Projected_Amount__c				
					--allocations
						,T.GSplitAmt AS rC_Giving__Allocation_Amount__c
/*EXT_ID*/				,T.GAU_External_Id__c	AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]
 	 
					--frequencies
						,CASE WHEN (T.GFType='Pledge' OR T.GFType='MG Pledge' OR T.GFType='Recurring Gift') 
							THEN T.rC_Giving__Giving_Frequency__c ELSE NULL END AS rC_Giving__Giving_Frequency__c
						,CASE WHEN (T.GFType='Pledge' OR T.GFType='MG Pledge' OR T.GFType='Recurring Gift') 
							THEN T.rC_Giving__Giving_Frequency__c ELSE NULL END AS	rC_Giving__Payment_Frequency__c
							
						,CASE WHEN (T.GFInsNumPay='4' OR T.GFInsNumPay='9') THEN T.GFInsNumPay ELSE NULL END AS	rC_Giving__Payment_Count__c
					--givingtypes
						,T.rC_Giving__Is_Giving__c
						,T.rC_Giving__Is_Giving_Donation__c
						,T.rC_Giving__Is_Giving_Inkind__c
						,T.rC_Giving__Is_Sustainer__c
						,GETDATE() AS rC_Giving__Update_Transactions__c
						,GETDATE() AS rC_Giving__Rollup_Transactions__c
						,GETDATE() AS rC_Giving__Rollup_Giving__c
						--payment method
						,T.[rC_Giving__Payment_Type__c] AS rC_Giving__Payment_Method__c
/*EXT_ID*/				,T.[PAYMENT_METHOD_External_ID__c] AS [rC_Giving__Payment_Method_Selected__r:External_ID__c]

					--parent 
/*EXT_ID*/				,T.PARENT_PROPOSAL_External_Id__c AS [rC_Giving__Parent__r:External_ID__c]
					--payment dates
						,T.GFInsStartDate	AS	rC_Giving__First_Payment_Date__c
						,T.GFInsEndDate	AS	rC_Giving__Giving_End_Date__c
						,T.GFInsEndDate AS	rC_Giving__Payment_End_Date__c
						,CASE WHEN T.GFInsYears = 0 OR T.GFInsYears IS NULL THEN '1' ELSE T.GFInsYears END AS rC_Giving__Giving_Years__c
					--RE gift_v
						,T.RE_Legacy_Financial_Code__c
						,T.BATCH_NUMBER AS RE_Batch_Number__c
						,CASE WHEN T.GFAck='Acknowledged' THEN 'TRUE' ELSE 'FALSE' END	AS rC_Giving__Acknowledged__c
						,T.GFAckDate	AS	rC_Giving__Acknowledged_Date__c
						,T.GFAnon	AS	rC_Giving__Is_Anonymous__c
						,T.GFCheckDate	AS	rC_Giving__Check_Date__c
						,T.GFCheckNum	AS	rC_Giving__Check_Number__c
						,T.GFRef	AS	[Description]
						,T.GFStkIss	AS	Stock_Issuer__c
						,T.GFStkMedPrice	AS	Stock_Median_Price__c
						,T.GFStkNumUnits	AS	rC_Giving__Number_Of_Shares__c
						,T.GFStkSymbol	AS	rC_Giving__Ticker_Symbol__c
						,T.Production_Stage__c
						,T.DateAdded	AS	CreatedDate
						,T.GFConsDesc AS RE_Gift_Constituency_Code__c
						
					--attributes (chart-attributes)
						,T.OPX_Corporate_Donation_Contact__c
						,T.Donation_Page_Name__c
						,T.OPX_Provider__c
						,T.OPX_Transaction_ID__c	
					--gift attribute (chart_giftattribute)
						,CAST(T.OPPORTUNITY_Attribute__c AS varchar(max)) AS Attribute__c 
			
					--GIFT TRIBUTE
						,T.rC_Giving__Tribute_Type__c
						,T.rC_Giving__Tribute_Description__c	
						,T.rC_Giving__Is_Tribute__c
						,T.rC_Giving__Tribute_Effective_Date__c
						,T.rC_Giving__Tribute_Comments__c
						,T.[rC_Giving__Tribute_Contact__r:External_ID__c]
					--MISC.
						,T.RE_Split_Gift__c
					--reference
						--,T1.ID zrefAccountId  --not needed. 
						,T.ImportID  zref_ImportID
						,T.Parent_Giving_RecordType AS zrefParentRecordType
						,T.GFType AS zrefGFType
						,T.GFInsFreq AS zrefGFInsFreq
						,T.GFPledgeBalance AS zrefPledgeBal
						,T.GFStatus AS zrefRecurringStatus
					    ,T.GFInsYears zrefInsYears
						/* not needed- name format changed
						,LEN(T1.Name +': '+ T.[GSplitRE_DB_OwnerShort]+' '+T.Parent_Giving_RecordType+' '+ 
							(CASE WHEN (T.GFType LIKE '%pledge%' AND  GFPledgeBalance>0) OR (T.GFType ='Recurring Gift' AND T.GFStatus='Active') 
							  THEN 'Open' ELSE 'Completed' END) +' $'+
						  CAST(CAST(T.GSplitAmt AS DECIMAL(12,2))AS NVARCHAR(20))  +' '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10))) AS zrefNameLen
						*/
					INTO SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT_NEW
					FROM SUTTER_MGEA_MIGRATION.TBL.GIFT T
					--LEFT JOIN SUTTER_MGEA_MIGRATION.XTR.ACCOUNT T1 ON T.[ACCOUNT:External_Id__c]=T1.EXTERNAL_ID__C 
					WHERE T.Parent_Giving_RecordType IS NOT NULL   
					ORDER BY 	T.[GSplitRE_DB_OwnerShort], T.ImportID, T.GFDate ASC, T.CAMPAIGN_External_Id__c
					 --295,913

					 --297,574 final. 
					 
					
END

BEGIN--check duplicates

				/*	SELECT GFType, Parent_Giving_RecordType, COUNT(*) c 
					FROM SUTTER_MGEA_MIGRATION.TBL.GIFT
					GROUP BY GFType, Parent_Giving_RecordType
 				*/  
 				
 				SELECT * 
 				FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_parent
 				WHERE External_Id__c IN (SELECT External_Id__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_parent GROUP BY External_Id__c HAVING COUNT(*)>1)

END

BEGIN--REMOVE DOUBLE QUOTES					
	--SELECT rC_Giving__Tribute_Description__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE rC_Giving__Tribute_Description__c LIKE '%"%'
	--SELECT [Description] FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE [Description] LIKE '%"%'
	--SELECT Stock_Issuer__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE Stock_Issuer__c LIKE '%"%'
	--SELECT Stock_Median_Price__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE Stock_Median_Price__c LIKE '%"%'
	--SELECT rC_Giving__Tribute_Comments__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE rC_Giving__Tribute_Comments__c LIKE '%"%'
	
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[OPPORTUNITY_PARENT] SET rC_Giving__Tribute_Description__c=REPLACE(rC_Giving__Tribute_Description__c,'"','''') where rC_Giving__Tribute_Description__c like '%"%'
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[OPPORTUNITY_PARENT] SET [Description]=REPLACE([Description],'"','''') where [Description] like '%"%'
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[OPPORTUNITY_PARENT] SET Stock_Issuer__c=REPLACE(Stock_Issuer__c,'"','''') where Stock_Issuer__c like '%"%'
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[OPPORTUNITY_PARENT] SET Stock_Median_Price__c=REPLACE(Stock_Median_Price__c,'"','''') where Stock_Median_Price__c like '%"%'
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[OPPORTUNITY_PARENT] SET rC_Giving__Tribute_Comments__c=REPLACE(rC_Giving__Tribute_Comments__c,'"','''') where rC_Giving__Tribute_Comments__c like '%"%'


END


BEGIN-- QA for missing records

	SELECT T.*
	  
	FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT T
	LEFT JOIN SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY X ON T.External_Id__c=X.External_Id__c
	WHERE X.External_Id__c IS NULL 


	SELECT * 
 	FROM SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY
 	WHERE External_Id__c IN (SELECT External_Id__c FROM SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY GROUP BY External_Id__c HAVING COUNT(*)>1)
	AND External_ID__C !=''

	SELECT X.*
	FROM  SUTTER_MGEA_MIGRATION.XTR.OPPORTUNITY X 
	LEFT JOIN SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT T ON T.External_Id__c=X.External_Id__c
	WHERE T.External_Id__c IS NULL 




END;
 

 