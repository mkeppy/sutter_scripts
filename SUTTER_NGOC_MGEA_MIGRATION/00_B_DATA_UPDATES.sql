
USE SUTTER_MGEA_DATA
GO


/****************************************************************************************/	
--ADD RE_DB_Id to table to use for linking with combo external Id. 
-- this is only required when XTR files are needed for Data Loader Inserts.
		 
				--CONSTITUENT 
				ALTER TABLE SUTTER_MGEA_DATA.dbo.HC_Constituents_v
				ADD RE_DB_Id VARCHAR(30)
				GO
				UPDATE SUTTER_MGEA_DATA.dbo.HC_Constituents_v
				SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
				GO
					--SELECT ImportID, RE_DB_OwnerShort, RE_DB_Id FROM SUTTER_MGEA_DATA.dbo.HC_Constituents_v
			
				--INDIVIDUAL RELATIONSHIP
				ALTER TABLE SUTTER_MGEA_DATA.DBO.HC_Ind_Relat_v
				ADD RE_DB_Id VARCHAR(30)
				GO
				UPDATE SUTTER_MGEA_DATA.DBO.HC_Ind_Relat_v
				SET RE_DB_Id=RE_DB_OwnerShort+'-'+IRImpID
				GO 

				--ORGANIZATION RELATIONSHIP
				ALTER TABLE SUTTER_MGEA_DATA.DBO.HC_Org_Relat_v
				ADD RE_DB_Id VARCHAR(30)
				GO
				UPDATE SUTTER_MGEA_DATA.DBO.HC_Org_Relat_v
				SET RE_DB_Id=RE_DB_OwnerShort+'-'+ORImpID
				GO 

				--GIFT
				ALTER TABLE SUTTER_MGEA_DATA.dbo.HC_Gift_v
				ADD RE_DB_Id VARCHAR(30)
				GO
				UPDATE SUTTER_MGEA_DATA.dbo.HC_Gift_v
				SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
				GO
						--  SELECT top 1000 * FROM SUTTER_MGEA_DATA.dbo.HC_Gift_v
				
				--CONS ACTION 
				ALTER TABLE SUTTER_MGEA_DATA.dbo.HC_Cons_Action_v
				ADD RE_DB_Id VARCHAR(30)
				GO
				
				UPDATE SUTTER_MGEA_DATA.dbo.HC_Cons_Action_v
				SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
				GO


				--CONS SOLICITOR 
				ALTER TABLE SUTTER_MGEA_DATA.dbo.HC_Cons_Solicitor_v
				ADD RE_DB_Id VARCHAR(30)
				GO
					UPDATE SUTTER_MGEA_DATA.dbo.HC_Cons_Solicitor_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
					GO		
							--SELECT * FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Solicitor_v
				--PROPOSAL
				ALTER TABLE SUTTER_MGEA_DATA.dbo.HC_Proposal_v
				ADD RE_DB_Id VARCHAR(30)
				GO
				UPDATE SUTTER_MGEA_DATA.dbo.HC_Proposal_v
				SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
				GO

				--PROSPECT RATING
				ALTER TABLE SUTTER_MGEA_DATA.DBO.HC_Prospect_Rating_v
				ADD RE_DB_Id VARCHAR(30)	
				GO
				UPDATE SUTTER_MGEA_DATA.dbo.HC_Prospect_Rating_v
				SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
				GO
				--CONSTITUENT ATTRIBUTE
				ALTER TABLE SUTTER_MGEA_DATA.DBO.HC_Cons_Attributes_v
				ADD RE_DB_Id VARCHAR(30)	
				GO
				UPDATE SUTTER_MGEA_DATA.dbo.HC_Cons_Attributes_v
				SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
				GO
	 
				SELECT * FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Attributes_v

				--CHART CONS SOLICITOR	
				ALTER TABLE [SUTTER_1P_DATA].DBO.[CHART_ConsSolicitor]
				ADD RE_DB_Id VARCHAR(35)	
				
				UPDATE [SUTTER_1P_DATA].DBO.[CHART_ConsSolicitor]
				SET RE_DB_Id=RE_DB_OwnerShort+'-'+ASRLink
				GO
                
BEGIN --UPDATE CONS ADDRESS COUNTRY

		 SELECT [AddrState], [AddrCountry], count(*) 
		 FROM SUTTER_MGEA_DATA.DBO.HC_Cons_Address_v
		 WHERE --country is null and 
		 (AddrState ='AL' or AddrState='AK' or AddrState='AZ' or AddrState='AR'  or AddrState='CA' or AddrState='CO' or AddrState='CT' or AddrState='DE' or AddrState='DC' or AddrState='FL'
		 or AddrState='GA' or AddrState='HI' or AddrState='ID' or AddrState='IL' or AddrState='IN' or AddrState='IA' or AddrState='KS' or AddrState='KY' or AddrState='LA' or AddrState='ME' or AddrState='MD'
		 or AddrState='MA' or AddrState='MI' or AddrState='MN' or AddrState='MS' or AddrState='MO' or AddrState='MT' or AddrState='NE' or AddrState='NV' or AddrState='NH' or AddrState='NJ' or AddrState='NM' 
         or AddrState='NY' or AddrState='NC' or AddrState='ND' or AddrState='OH' or AddrState='OK' or AddrState='OR' or AddrState='PA' or AddrState='RI' or AddrState='SC' or AddrState='SD' or AddrState='TN'
         or AddrState='TX' or AddrState='UT' or AddrState='VT' or AddrState='VA' or AddrState='WA' or AddrState='WV' or AddrState='WI' or AddrState='WY')
		 GROUP BY [AddrState], [AddrCountry]   --'United States'
		  
		 
		 UPDATE SUTTER_MGEA_DATA.DBO.HC_Cons_Address_v SET [AddrCountry] = 'United States' WHERE --country is null AND 
		 (AddrState ='AL' or AddrState='AK' or AddrState='AZ' or AddrState='AR'  or AddrState='CA' or AddrState='CO' or AddrState='CT' or AddrState='DE' or AddrState='DC' or AddrState='FL'
		 or AddrState='GA' or AddrState='HI' or AddrState='ID' or AddrState='IL' or AddrState='IN' or AddrState='IA' or AddrState='KS' or AddrState='KY' or AddrState='LA' or AddrState='ME' or AddrState='MD'
		 or AddrState='MA' or AddrState='MI' or AddrState='MN' or AddrState='MS' or AddrState='MO' or AddrState='MT' or AddrState='NE' or AddrState='NV' or AddrState='NH' or AddrState='NJ' or AddrState='NM' 
         or AddrState='NY' or AddrState='NC' or AddrState='ND' or AddrState='OH' or AddrState='OK' or AddrState='OR' or AddrState='PA' or AddrState='RI' or AddrState='SC' or AddrState='SD' or AddrState='TN'
         or AddrState='TX' or AddrState='UT' or AddrState='VT' or AddrState='VA' or AddrState='WA' or AddrState='WV' or AddrState='WI' or AddrState='WY')
		 
END;


BEGIN --UPDATE NON-CONS IND RELAT ADDRESS COUNTRY

		 SELECT [IRAddrState], [IRAddrCountry], count(*) 
		 FROM SUTTER_MGEA_DATA.DBO.HC_Ind_Relat_v
		 WHERE (IRLink IS NULL OR IRLink='') AND 
		 (IRAddrState ='AL' or IRAddrState='AK' or IRAddrState='AZ' or IRAddrState='AR'  or IRAddrState='CA' or IRAddrState='CO' or IRAddrState='CT' or IRAddrState='DE' or IRAddrState='DC' or IRAddrState='FL'
		 or IRAddrState='GA' or IRAddrState='HI' or IRAddrState='ID' or IRAddrState='IL' or IRAddrState='IN' or IRAddrState='IA' or IRAddrState='KS' or IRAddrState='KY' or IRAddrState='LA' or IRAddrState='ME' or IRAddrState='MD'
		 or IRAddrState='MA' or IRAddrState='MI' or IRAddrState='MN' or IRAddrState='MS' or IRAddrState='MO' or IRAddrState='MT' or IRAddrState='NE' or IRAddrState='NV' or IRAddrState='NH' or IRAddrState='NJ' or IRAddrState='NM' 
         or IRAddrState='NY' or IRAddrState='NC' or IRAddrState='ND' or IRAddrState='OH' or IRAddrState='OK' or IRAddrState='OR' or IRAddrState='PA' or IRAddrState='RI' or IRAddrState='SC' or IRAddrState='SD' or IRAddrState='TN'
         or IRAddrState='TX' or IRAddrState='UT' or IRAddrState='VT' or IRAddrState='VA' or IRAddrState='WA' or IRAddrState='WV' or IRAddrState='WI' or IRAddrState='WY')
		 GROUP BY [IRAddrState], [IRAddrCountry]   --'United States'
		 
		 UPDATE SUTTER_MGEA_DATA.DBO.HC_Ind_Relat_v SET [IRAddrCountry] = 'United States' WHERE (IRLink IS NULL OR IRLink='') AND 
		 (IRAddrState ='AL' or IRAddrState='AK' or IRAddrState='AZ' or IRAddrState='AR'  or IRAddrState='CA' or IRAddrState='CO' or IRAddrState='CT' or IRAddrState='DE' or IRAddrState='DC' or IRAddrState='FL'
		 or IRAddrState='GA' or IRAddrState='HI' or IRAddrState='ID' or IRAddrState='IL' or IRAddrState='IN' or IRAddrState='IA' or IRAddrState='KS' or IRAddrState='KY' or IRAddrState='LA' or IRAddrState='ME' or IRAddrState='MD'
		 or IRAddrState='MA' or IRAddrState='MI' or IRAddrState='MN' or IRAddrState='MS' or IRAddrState='MO' or IRAddrState='MT' or IRAddrState='NE' or IRAddrState='NV' or IRAddrState='NH' or IRAddrState='NJ' or IRAddrState='NM' 
         or IRAddrState='NY' or IRAddrState='NC' or IRAddrState='ND' or IRAddrState='OH' or IRAddrState='OK' or IRAddrState='OR' or IRAddrState='PA' or IRAddrState='RI' or IRAddrState='SC' or IRAddrState='SD' or IRAddrState='TN'
         or IRAddrState='TX' or IRAddrState='UT' or IRAddrState='VT' or IRAddrState='VA' or IRAddrState='WA' or IRAddrState='WV' or IRAddrState='WI' or IRAddrState='WY')
		 
END;


BEGIN --UPDATE NON-CONS ORG RELAT ADDRESS COUNTRY

		 SELECT [ORAddrState], [ORAddrCountry], count(*) 
		 FROM SUTTER_MGEA_DATA.DBO.HC_Org_Relat_v
		 WHERE (ORLINK IS NULL OR ORLINK='') AND 
		 (ORAddrState ='AL' or ORAddrState='AK' or ORAddrState='AZ' or ORAddrState='AR'  or ORAddrState='CA' or ORAddrState='CO' or ORAddrState='CT' or ORAddrState='DE' or ORAddrState='DC' or ORAddrState='FL'
		 or ORAddrState='GA' or ORAddrState='HI' or ORAddrState='ID' or ORAddrState='IL' or ORAddrState='IN' or ORAddrState='IA' or ORAddrState='KS' or ORAddrState='KY' or ORAddrState='LA' or ORAddrState='ME' or ORAddrState='MD'
		 or ORAddrState='MA' or ORAddrState='MI' or ORAddrState='MN' or ORAddrState='MS' or ORAddrState='MO' or ORAddrState='MT' or ORAddrState='NE' or ORAddrState='NV' or ORAddrState='NH' or ORAddrState='NJ' or ORAddrState='NM' 
         or ORAddrState='NY' or ORAddrState='NC' or ORAddrState='ND' or ORAddrState='OH' or ORAddrState='OK' or ORAddrState='OR' or ORAddrState='PA' or ORAddrState='RI' or ORAddrState='SC' or ORAddrState='SD' or ORAddrState='TN'
         or ORAddrState='TX' or ORAddrState='UT' or ORAddrState='VT' or ORAddrState='VA' or ORAddrState='WA' or ORAddrState='WV' or ORAddrState='WI' or ORAddrState='WY')
		 GROUP BY [ORAddrState], [ORAddrCountry]   --'United States'
		 
		 UPDATE SUTTER_MGEA_DATA.DBO.HC_Org_Relat_v SET [ORAddrCountry] = 'United States' WHERE (ORLINK IS NULL OR ORLINK='') AND 
		 (ORAddrState ='AL' or ORAddrState='AK' or ORAddrState='AZ' or ORAddrState='AR'  or ORAddrState='CA' or ORAddrState='CO' or ORAddrState='CT' or ORAddrState='DE' or ORAddrState='DC' or ORAddrState='FL'
		 or ORAddrState='GA' or ORAddrState='HI' or ORAddrState='ID' or ORAddrState='IL' or ORAddrState='IN' or ORAddrState='IA' or ORAddrState='KS' or ORAddrState='KY' or ORAddrState='LA' or ORAddrState='ME' or ORAddrState='MD'
		 or ORAddrState='MA' or ORAddrState='MI' or ORAddrState='MN' or ORAddrState='MS' or ORAddrState='MO' or ORAddrState='MT' or ORAddrState='NE' or ORAddrState='NV' or ORAddrState='NH' or ORAddrState='NJ' or ORAddrState='NM' 
         or ORAddrState='NY' or ORAddrState='NC' or ORAddrState='ND' or ORAddrState='OH' or ORAddrState='OK' or ORAddrState='OR' or ORAddrState='PA' or ORAddrState='RI' or ORAddrState='SC' or ORAddrState='SD' or ORAddrState='TN'
         or ORAddrState='TX' or ORAddrState='UT' or ORAddrState='VT' or ORAddrState='VA' or ORAddrState='WA' or ORAddrState='WV' or ORAddrState='WI' or ORAddrState='WY')
		 
END;

BEGIN --UPDATE ASSIGNED SOLICITOR RELATIONSHIP TYPE TO "SOLICITOR" WHEN ASRTYPE IS NULL

		SELECT * FROM SUTTER_MGEA_DATA.DBO.HC_Cons_Solicitor_v WHERE ASRType IS NULL
		UPDATE SUTTER_MGEA_DATA.DBO.HC_Cons_Solicitor_v SET ASRType='Solicitor' WHERE ASRType IS NULL
		
END;

BEGIN--UPDATE CONS ATTR DESC TO 'NULL'

		SELECT * FROM SUTTER_MGEA_DATA.DBO.HC_Cons_Attributes_v WHERE CAttrDesc IS NULL
		UPDATE SUTTER_MGEA_DATA.DBO.HC_Cons_Attributes_v SET CAttrDesc='NULL' WHERE CAttrDesc IS NULL	
END; 


BEGIN--UPDATE GIFT: CAMPAIGN, APPEAL, PACKAGE TO "NULL"

		SELECT GSplitCamp, GSplitAppeal, GSplitPkg, GSplitFund
		FROM SUTTER_MGEA_DATA.DBO.HC_Gift_SplitGift_v
		WHERE GSplitCamp IS NULL OR GSplitAppeal IS NULL OR GSplitPkg IS NULL OR GSplitFund	IS NULL
		GROUP BY GSplitCamp, GSplitAppeal, GSplitPkg, GSplitFund
		
		UPDATE SUTTER_MGEA_DATA.DBO.HC_Gift_SplitGift_v SET GSplitCamp='NULL' WHERE GSplitCamp IS NULL
		UPDATE SUTTER_MGEA_DATA.DBO.HC_Gift_SplitGift_v SET GSplitAppeal='NULL' WHERE GSplitAppeal IS NULL
		UPDATE SUTTER_MGEA_DATA.DBO.HC_Gift_SplitGift_v SET GSplitPkg='NULL' WHERE GSplitPkg IS NULL
		
END;

BEGIN--UPDATE CONS APPEAL: APPEAL, PACKAGE TO "NULL"
		SELECT * FROM SUTTER_MGEA_DATA.DBO.HC_Cons_Appeal_v WHERE CAPPackageID IS NULL 
		UPDATE SUTTER_MGEA_DATA.DBO.HC_Cons_Appeal_v SET CAPPackageID='NULL' WHERE CAPPackageID IS NULL 
		

END;

BEGIN--UPDATE CREDIT CARD TYPE
		SELECT GFPayMeth, GFCCType, COUNT(*) C FROM SUTTER_MGEA_DATA.DBO.HC_Gift_v
		GROUP BY GFPayMeth, GFCCType
		
		UPDATE SUTTER_MGEA_DATA.DBO.HC_Gift_v SET GFCCType='MasterCard' WHERE GFCCType='Master Card'
		UPDATE SUTTER_MGEA_DATA.DBO.HC_Gift_v SET GFCCType=NULL WHERE GFCCType='Visa' AND GFPayMeth='Personal Check'
		
END; 

BEGIN--UPDATE payment info where paytype is not credit card. 

		SELECT GFImpId, GFPayMeth, GFCCType, GFCCNum, GFCardholderName, GFCCExpOn, GFAuthCode
		FROM SUTTER_MGEA_DATA.dbo.hc_gift_v 
		WHERE GFPayMeth!='Credit Card' AND (GFCCType IS NOT NULL 
											OR GFCCNum IS NOT NULL 
											OR GFCardholderName IS NOT NULL 
											OR GFCCExpOn IS NOT NULL
											OR GFAuthCode IS NOT NULL)
		
		UPDATE SUTTER_MGEA_DATA.dbo.hc_gift_v SET GFCCNum=NULL, GFCardholderName=NULL
		WHERE GFPayMeth!='Credit Card' AND (GFCCType IS NOT NULL 
											OR GFCCNum IS NOT NULL 
											OR GFCardholderName IS NOT NULL 
											OR GFCCExpOn IS NOT NULL
											OR GFAuthCode IS NOT NULL)
END 	


BEGIN

	--CANotes with PICUTRE On NOTE field
	SELECT * FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Action_Notes_v 
	WHERE RE_DB_OwnerShort='SMCF' AND (CANoteImpID='03947-420-0000035508' 
		OR CANoteImpID='03947-420-0000041416'
		OR CANoteImpID='03947-420-0000050951')
	
	--REMOVE contect of NOTES fields
	UPDATE SUTTER_MGEA_DATA.dbo.HC_Cons_Action_Notes_v
	SET CANoteNotes=NULL
	WHERE RE_DB_OwnerShort='SMCF' AND (CANoteImpID='03947-420-0000035508' 
		OR CANoteImpID='03947-420-0000041416'
		OR CANoteImpID='03947-420-0000050951')
END
	
	