USE SUTTER_MGEA_DATA
GO

BEGIN
		DROP TABLE SUTTER_MGEA_MIGRATION.IMP.TASK	
		
END
 
BEGIN
		--TASKS FROM CONS ACTION 
			SELECT DISTINCT
				 CASE WHEN T7.OwnerID IS NULL THEN X3.ID ELSE T7.OwnerID END AS OwnerID
				,WhatId=CASE WHEN T1.KeyInd='O' THEN X1.ID END 
				,WhoId=CASE WHEN T1.KeyInd='I' THEN X2.ID END 
				,T.RE_DB_OwnerShort+'-'+T.ACImpID AS RE_ACImpID__c
				,T.ACDate AS ActivityDate
				,T.ACCat AS Type__c
				
				,T2.Move__c
				,T3.Stage__c
				,CASE WHEN T9.CANoteDesc IS NULL THEN T3.Stage__c ELSE T3.Stage__c +'. '+T9.CANoteDesc END AS [Subject]
				,T.RE_DB_OwnerShort AS Affiliation__c
				,CASE WHEN (T.ACDate < GETDATE()) THEN 'Completed' ELSE 'In Progress' END AS [Status]
				,CAST(T4.RE_Action_Solicitor__c  AS VARCHAR(MAX)) AS RE_Action_Solicitor__c
				,CAST(T6.RE_Team_Member__c  AS VARCHAR(MAX)) AS RE_Team_Member__c
				,CAST(T6.Thank_you_call__c  AS VARCHAR(MAX)) AS Thank_you_call__c
				,CAST(T5.[Description] AS VARCHAR(MAX)) AS [Description]
				,T.AddedBy AS RE_Action_Added_By__c

				--reference
				,T1.KeyInd zref_KeyInd
				,T.ImportID AS zrefImportID
				,T.AddedBy zref_AddedBy
				,T9.CANoteDesc  AS zref_NoteDesc
				

			INTO SUTTER_MGEA_MIGRATION.IMP.TASK	
			FROM SUTTER_MGEA_DATA.DBO.HC_Cons_Action_v T
			INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Contact_HofH T10 ON T.ImportID=T10.NoHH_ImportID AND T.RE_DB_OwnerShort=T10.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.DBO.CHART_ActionStatus T2 ON T.ACStatus=T2.ACStatus AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.DBO.CHART_ActionType T3 ON T.ACType=T3.ACType AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Cons_Action_Solicitor_1 T4 ON T.ACImpID=T4.ACImpID
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Cons_Action_Notes_1 T5 ON T.ACImpID=T5.CALink
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Cons_Action_Notes_desc T9 ON T.ACImpID=T9.CALink
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Attribute_task_1 T6 ON T.ACImpID=T6.ACImpId
			LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.AssginedSolicitor_OwnerId T7 ON T.ACImpID=T7.ACImpID AND T.RE_DB_OwnerShort=T7.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.DBO.CHART_ActionSolicitor T8 ON T.AddedBy=T8.AddedBy AND T.RE_DB_OwnerShort=T8.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.XTR.ACCOUNT X1 ON T1.RE_DB_Id=X1.External_Id__c
			LEFT JOIN SUTTER_MGEA_MIGRATION.XTR.CONTACT X2 ON T1.RE_DB_Id=X2.External_Id__c
			LEFT JOIN SUTTER_MGEA_MIGRATION.XTR.USERS X3 ON T8.SF_User_Email=X3.Email
			
			WHERE T3.[Convert]='Yes'
			--69740
			--71132 final
			 SELECT * FROM SUTTER_MGEA_MIGRATION.[dbo].[CHART_ActionSolicitor] 
		 
		 SELECT * FROM SUTTER_MGEA_MIGRATION.xtr.contact

END; 

BEGIN--check duplicates

		SELECT * 
		FROM SUTTER_MGEA_MIGRATION.IMP.TASK 
		WHERE rC_Bios__External_ID__c IN (SELECT rC_Bios__External_ID__c FROM SUTTER_MGEA_MIGRATION.IMP.TASK GROUP BY rC_Bios__External_ID__c HAVING COUNT(*)>1)
		
END

BEGIN

	--	SELECT [Description] FROM [SUTTER_MGEA_MIGRATION].[IMP].[TASK] WHERE [Description] LIKE '%"%';        
		UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[TASK] SET [Description]=REPLACE([Description],'"','''') where [Description] like '%"%'

END
 