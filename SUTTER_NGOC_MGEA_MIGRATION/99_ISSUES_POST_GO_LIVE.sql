 -- campaign issues with mgea import id
 	SELECT * FROM [SUTTER_MGEA_MIGRATION].[IMP].[CAMPAIGN] WHERE zrefCampLevel='Level_2' OR zrefCampLevel='Level_1'



	SELECT DISTINCT CampID, AppealID, PackageID, CAMPAIGNLevel1_Name, CAMPAIGNLevel2_Name 
	FROM SUTTER_MGEA_MIGRATION.dbo.CHART_Campaign WHERE (CAMPAIGNLevel3_Name IS NULL OR CAMPAIGNLevel3_Name='') 
	ORDER BY CampID, AppealID, PackageID, CAMPAIGNLevel1_Name, CAMPAIGNLevel2_Name 

	
	SELECT DISTINCT CAMPAIGNLevel1_Name--, CAMPAIGNLevel2_Name 
	FROM SUTTER_MGEA_MIGRATION.dbo.CHART_Campaign WHERE CAMPAIGNLevel3_Name IS NULL OR CAMPAIGNLevel3_Name=''
	ORDER BY CAMPAIGNLevel1_Name--, CAMPAIGNLevel2_Name 



	SELECT AppealId, CampId, PackageId, CAMPAIGNLevel1_Name, CAMPAIGNLevel2_Name, CAMPAIGNLevel3_Name, PRIMARY_CAMPAIGN_SOURCE  
	FROM SUTTER_MGEA_MIGRATION.TBL.CAMPAIGN 
	WHERE CAMPAIGNLevel3_Name IS NULL 
	GROUP BY AppealId, CampId, PackageId, CAMPAIGNLevel1_Name, CAMPAIGNLevel2_Name, CAMPAIGNLevel3_Name, PRIMARY_CAMPAIGN_SOURCE  
	ORDER BY AppealId, CampId, PackageId, CAMPAIGNLevel1_Name, CAMPAIGNLevel2_Name, CAMPAIGNLevel3_Name, PRIMARY_CAMPAIGN_SOURCE  



 --I-0037  --RE ID in SAFH is 306839 Gift ID is 56826
	 SELECT * FROM SUTTER_MGEA_MIGRATION.imp.CONTACT
	 WHERE RE_ConsID__c LIKE '%306839%'

	 SELECT * FROM SUTTER_MGEA_DATA.dbo.HC_Constituents_v 
	 WHERE ConsID LIKE '%306839%'
  
	  SELECT * FROM SUTTER_MGEA_DATA.dbo.hc_gift_v WHERE GiftID='56826'  

	  SELECT * FROM SUTTER_MGEA_DATA.dbo.HC_Gift_Tribute_v WHERE GFTLink='04121-545-0000135591'  --in memory of	Lovelle Russell

	  SELECT * FROM SUTTER_MGEA_DATA.dbo.HC_Cons_Tribute_v WHERE TRImpID='04121-079-0000002879'	--in memory of  Lovelle Russell

	  SELECT RE_GFImpID__c, rC_Giving__Tribute_Type__c,
			[rC_Giving__Tribute_Contact__r:External_ID__c], rC_Giving__Tribute_Description__c,
			rC_Giving__Tribute_Effective_Date__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE RE_GFImpID__c LIKE '%04121-545-0000135591%'

--I-0015

	SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_CONTACT_CREDIT 
	WHERE [rC_Giving__Opportunity__r:External_ID__c] IN (SELECT [rC_Giving__Opportunity__r:External_ID__c] FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_CONTACT_CREDIT 
														GROUP BY [rC_Giving__Opportunity__r:External_ID__c] HAVING COUNT(*)>1)
	ORDER BY [rC_Giving__Opportunity__r:External_ID__c]

--I-0024

	SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.TASK 
	WHERE  [status] ='In Progress'


--I-0007

	SELECT * FROM SUTTER_MGEA_MIGRATION.imp.RELATIONSHIP  --IF IRToDate < Today() then FALSE, else TRUE.
	WHERE rC_Bios__Active__c='TRUE' AND rC_Bios__Stopping_Year__c IS NOT NULL AND rC_Bios__Starting_Year__c IS NOT NULL

