USE SUTTER_MGEA_DATA
GO



BEGIN--GET COUNTS()
		SELECT DISTINCT CAMPAIGNLevel1_Name, CAMPAIGNLevel1_Campaign_Description__c FROM SUTTER_MGEA_MIGRATION.DBO.CHART_Campaign WHERE CAMPAIGNLevel1_Name IS NOT null
		--16
		SELECT DISTINCT CAMPAIGNLevel2_Name, CAMPAIGNLevel2_Campaign_Description__c FROM SUTTER_MGEA_MIGRATION.DBO.CHART_Campaign WHERE CAMPAIGNLevel2_Name IS NOT null
		--684
		SELECT DISTINCT CAMPAIGNLevel3_Name, CAMPAIGNLevel3_Campaign_Description__c FROM SUTTER_MGEA_MIGRATION.DBO.CHART_Campaign WHERE CAMPAIGNLevel3_Name IS NOT null
		--2272
END

BEGIN
		DROP TABLE SUTTER_MGEA_MIGRATION.TBL.CAMPAIGN
		DROP TABLE SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN
END


 

BEGIN--CREATE BASE TABLE for CAMPAIGNS. 

		 DROP TABLE SUTTER_MGEA_MIGRATION.TBL.CAMPAIGN


		SELECT DISTINCT T1.RE_DB_OwnerShort, T1.FundId, T1.CampId, T1.AppealId, T1.PackageId, T1.FundDesc, T1.CampDesc, T1.AppDesc, T1.PackDesc,
		T1.CAMPAIGNLevel1_Name, T1.CAMPAIGNLevel1_Campaign_Description__c,
		T1.CAMPAIGNLevel2_Name, T1.CAMPAIGNLevel2_Campaign_Description__c,
		T1.CAMPAIGNLevel3_Name, T1.CAMPAIGNLevel3_Campaign_Description__c,
		PRIMARY_CAMPAIGN_SOURCE=CASE WHEN T1.CAMPAIGNLevel3_Name IS NOT NULL THEN T1.CAMPAIGNLevel3_Name
						     WHEN T1.CAMPAIGNLevel2_Name IS NOT NULL THEN T1.CAMPAIGNLevel2_Name
						     WHEN T1.CAMPAIGNLevel1_Name IS NOT NULL THEN T1.CAMPAIGNLevel1_Name END, 
		T1.PrimaryGAU_ExternalID__c,
		rC_Giving__Campaign_Type__c = CASE WHEN T1.CAMPAIGNLevel3_Name IS NOT NULL THEN T3.Campaign_TYPE
						     WHEN T1.CAMPAIGNLevel2_Name IS NOT NULL THEN T3.Campaign_TYPE 
						     WHEN T1.CAMPAIGNLevel1_Name IS NOT NULL THEN T3.Campaign_TYPE 
							 END, 
		rC_Giving__Channel__c = CASE WHEN T1.CAMPAIGNLevel3_Name IS NOT NULL THEN T3.Campaign_CHANNEL
						     WHEN T1.CAMPAIGNLevel2_Name IS NOT NULL THEN T3.Campaign_CHANNEL 
						     WHEN T1.CAMPAIGNLevel1_Name IS NOT NULL THEN T3.Campaign_CHANNEL END,
		MGEA_Import_Id__c = T1.RE_DB_OwnerShort+(CASE WHEN T1.AppealID ='NULL' THEN '' ELSE LTRIM(RTRIM(T1.AppealId)) END)
												+(CASE WHEN T1.CampId ='NULL' THEN '' ELSE LTRIM(RTRIM(T1.CampId)) END) 
												+(CASE WHEN T1.PackageId ='NULL' THEN '' ELSE LTRIM(RTRIM(T1.PackageId)) END)  
		
		INTO SUTTER_MGEA_MIGRATION.TBL.CAMPAIGN   
		FROM SUTTER_MGEA_MIGRATION.DBO.CHART_Campaign T1
		LEFT JOIN SUTTER_MGEA_DATA.dbo.HC_Appeal_v T2 ON T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort AND T1.AppealID=t2.AppealID
		LEFT JOIN SUTTER_MGEA_MIGRATION.DBO.CHART_AppealCategory T3 ON T2.RE_DB_OwnerShort=T3.RE_DB_OwnerShort AND T2.AppCategory=T3.AppCategory
			 --9,563
			--10,037 final

END;


BEGIN--CAMPAIGN

		--LEVEL 3
				SELECT DISTINCT
				 OwnerID= dbo.fnc_OwnerID() 
				,RecordTypeID= dbo.fnc_RecordType('Campaign_Standard')
				,Name= T1.CAMPAIGNLevel3_Name
				,External_Id__c= T1.CAMPAIGNLevel3_Name 
				,Campaign_Description__c= T1.CAMPAIGNLevel3_Campaign_Description__c 
				,rC_Giving__Affiliation__c= T1.RE_DB_OwnerShort 
				,T1.rC_Giving__Campaign_Type__c
				,T1.rC_Giving__Channel__c
				--PKG info
				,[Description]= NULL 
				,StartDate= CAST(MIN(T2.PackStartDate) AS DATE)
				,EndDate= CAST(MAX(T2.PackEndDate) AS DATE)
				,IsActive= MIN(CASE WHEN T2.PackIsInactive='TRUE' THEN 'FALSE' ELSE 'TRUE' END)
			 	,MAX(MGEA_Import_Id__c) AS MGEA_Import_Id__c
				--ref
				,LEN(T1.CAMPAIGNLevel3_Name) AS zrefCAMPLen
				,'Level_3' AS zrefCampLevel
				
				INTO SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN
				FROM SUTTER_MGEA_MIGRATION.TBL.CAMPAIGN T1 
				LEFT JOIN SUTTER_MGEA_DATA.dbo.HC_Package_v T2 ON T1.PackageId=T2.PackageID 
														 AND T1.AppealId=T2.AppealID 
														 AND T1.RE_DB_OwnerShort =T2.RE_DB_OwnerShort
				WHERE T1.CAMPAIGNLevel3_Name IS NOT NULL   AND T1.CAMPAIGNLevel3_Name!=''
				GROUP BY T1.CAMPAIGNLevel3_Name, T1.CAMPAIGNLevel3_Campaign_Description__c, T1.RE_DB_OwnerShort, T1.PrimaryGAU_ExternalID__c
						 --,T1.MGEA_Import_Id__c
						 ,T1.rC_Giving__Campaign_Type__c, T1.rC_Giving__Channel__c 
				--2,272
				
			UNION ALL 
		
		--LEVEL 2
				SELECT DISTINCT
				 OwnerID= dbo.fnc_OwnerID() 
				,RecordTypeID= dbo.fnc_RecordType('Campaign_Standard')
				,Name= T1.CAMPAIGNLevel2_Name
				,External_Id__c= T1.CAMPAIGNLevel2_Name 
				,Campaign_Description__c= T1.CAMPAIGNLevel2_Campaign_Description__c 
				,rC_Giving__Affiliation__c= T1.RE_DB_OwnerShort 
				,MAX(T1.rC_Giving__Campaign_Type__c) AS rC_Giving__Campaign_Type__c
				,MAX(T1.rC_Giving__Channel__c) AS rC_Giving__Channel__c
				
				--APP info
				,[Description]= MAX(CAST(T2.AppNote AS NVARCHAR(4000)))
				,StartDate= CAST(MIN(T2.AppStartDate) AS DATE)
				,EndDate= CAST(MAX(T2.AppEndDate) AS DATE)
				,IsActive= MAX(CASE WHEN T2.AppIsInactive='TRUE' THEN 'FALSE' ELSE 'TRUE' END)
				,MAX(T1.MGEA_Import_Id__c) AS MGEA_Import_Id__c
				--ref
				,LEN(T1.CAMPAIGNLevel2_Name) zrefCAMPLen 
				,'Level_2' AS zrefCampLevel
				
				--INTO SUTTER_MGEA_MIGRATION.dbo.zTestCampL2  --drop table SUTTER_MGEA_MIGRATION.dbo.zTestCampL2  
				FROM SUTTER_MGEA_MIGRATION.TBL.CAMPAIGN T1 
				LEFT JOIN SUTTER_MGEA_DATA.dbo.HC_Appeal_v T2 ON T1.AppealID=T2.AppealID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T1.CAMPAIGNLevel2_Name IS NOT NULL 
				GROUP BY T1.CAMPAIGNLevel2_Name, T1.CAMPAIGNLevel2_Campaign_Description__c, T1.RE_DB_OwnerShort
				--684 
						 
			UNION ALL 
			
		--LEVEL 1
				SELECT DISTINCT
				 OwnerID= dbo.fnc_OwnerID() 
				,RecordTypeID= dbo.fnc_RecordType('Campaign_Standard')
				,Name= T1.CAMPAIGNLevel1_Name
				,External_Id__c= T1.CAMPAIGNLevel1_Name 
				,Campaign_Description__c= T1.CAMPAIGNLevel1_Campaign_Description__c 
				,rC_Giving__Affiliation__c= T1.RE_DB_OwnerShort 
				,rC_Giving__Campaign_Type__c=MAX(T2.CampCategory)
				,NULL rC_Giving__Channel__c

				--camp info
				,[Description]= MAX(CAST(T2.CampNote AS NVARCHAR(4000)))
				,StartDate= CAST(MIN(T2.CampStartDate) AS DATE)
				,EndDate= CAST(MAX(T2.CampEndDate) AS DATE)
				,IsActive= MAX(CASE WHEN T2.CampIsInactive='TRUE' THEN 'FALSE' ELSE 'TRUE' END)
				
				,MAX(T1.MGEA_Import_Id__c) AS MGEA_Import_Id__c

				,LEN(T1.CAMPAIGNLevel1_Name) zrefCAMPLen
				,'Level_1' AS zrefCampLevel
			
				FROM SUTTER_MGEA_MIGRATION.TBL.CAMPAIGN T1 
				LEFT JOIN SUTTER_MGEA_DATA.DBO.HC_Campaign_v T2 ON T1.CampID=T2.CampID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T1.CAMPAIGNLevel1_Name IS NOT NULL
				GROUP BY T1.CAMPAIGNLevel1_Name, T1.CAMPAIGNLevel1_Campaign_Description__c, T1.RE_DB_OwnerShort 
			 
  
END;

BEGIN--REMOVE DOUBLE QUOTES

			SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN where Campaign_Description__c LIKE '%"%'
			SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN where [Description] LIKE '%"%'
			
			UPDATE SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN 
			SET Campaign_Description__c=REPLACE(Campaign_Description__c,'"','''') 
			WHERE Campaign_Description__c LIKE '%"%'
			
			UPDATE SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN 
			SET [Description]=REPLACE([Description],'"','''') 
			WHERE [Description] LIKE '%"%'
			
			
END

BEGIN--DATA CHECK

			--test for duplicates
					SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN
					WHERE Name IN (SELECT NAME FROM SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN GROUP BY name HAVING COUNT(*) >1)
					ORDER BY name
					
					SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN
					WHERE MGEA_Import_Id__c IN (SELECT MGEA_Import_Id__c FROM SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN GROUP BY MGEA_Import_Id__c HAVING COUNT(*) >1)
					ORDER BY MGEA_Import_Id__c

					SELECT * FROM SUTTER_MGEA_MIGRATION.tbl.CAMPAIGN WHERE MGEA_Import_Id__c='SAFHA12ASA3D'
	
					SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN
					WHERE External_Id__c IN (SELECT External_Id__c FROM SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN GROUP BY External_Id__c HAVING COUNT(*) >1)
					ORDER BY External_Id__c
					
			--check for Start/End dates
					SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN
					WHERE EndDate < StartDate
					
					UPDATE SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN
					SET StartDate=EndDate, EndDate='2005-01-01'
					WHERE EndDate < StartDate 
						
END; 	

SELECT External_Id__c, MGEA_Import_Id__c, zrefCampLevel
FROM SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN
WHERE zrefCampLevel LIKE '%_2' OR zrefCampLevel LIKE '%_1'


BEGIN--UPDATE OF PARENT CAMPAIGN ON CAMP LEVEL 3 and CAMP LEVEL 2 and GAU on CAMP LEVEL 3

				SELECT DISTINCT
				T1.CAMPAIGNLevel3_Name External_Id__c
				,T1.CAMPAIGNLevel2_Name [Parent:External_Id__c]
				,T1.PrimaryGAU_ExternalID__c AS [rC_Giving__GAU__r:rc_Giving__External_Id__c]
				INTO SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN_UPDATE
				FROM SUTTER_MGEA_MIGRATION.TBL.CAMPAIGN T1 
				WHERE T1.CAMPAIGNLevel3_Name IS NOT NULL  

				UNION ALL 
				SELECT DISTINCT
				T1.CAMPAIGNLevel2_Name AS External_Id__c
				,T1.CAMPAIGNLevel1_Name AS [Parent:External_Id__c]
				,NULL AS [rC_Giving__GAU__r:rc_Giving__External_Id__c]
				
				FROM SUTTER_MGEA_MIGRATION.TBL.CAMPAIGN T1 
				WHERE T1.CAMPAIGNLevel2_Name IS NOT NULL  


END;



		SELECT CAMPAIGN_External_Id__c, COUNT(*) C 
		FROM SUTTER_MGEA_MIGRATION.TBL.GIFT
		GROUP BY CAMPAIGN_External_Id__c
		HAVING COUNT(*) >40000
		ORDER BY C DESC


		SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.CAMPAIGN 
		WHERE External_Id__c='SAFH Don/employee'

		SELECT [CAMPAIGN:External_Id__c], count (*) C
		FROM SUTTER_MGEA_MIGRATION.imp.OPPORTUNITY_PARENT 
		GROUP BY [CAMPAIGN:External_Id__c]
		ORDER BY C desc
		WHERE [CAMPAIGN:External_Id__c]='SAFH Don/employee'


		SELECT 
		External_Id__c
		, NAME
		,CASE WHEN [CAMPAIGN:External_Id__c]='SAFH Don/employee' 
			THEN [CAMPAIGN:External_Id__c] +' '+ CAST(((ROW_NUMBER() OVER (PARTITION BY [CAMPAIGN:External_Id__c] ORDER BY [CAMPAIGN:External_Id__c],[ACCOUNT:External_Id__c] ) - 1)/50000 + 1 ) AS NVARCHAR(1)) 
			ELSE NULL END AS [CAMPAIGN:External_Id__c]
		,ROW_NUMBER() OVER (ORDER BY [CAMPAIGN:External_Id__c], [ACCOUNT:External_Id__c]) AS zrefSeq
		,((ROW_NUMBER() OVER (PARTITION BY [CAMPAIGN:External_Id__c] ORDER BY [CAMPAIGN:External_Id__c], [ACCOUNT:External_Id__c] ) - 1)/50000 + 1 ) AS zrefCamp50K   --50,000 is the cut out 
		--,[CAMPAIGN:External_Id__c] AS zrefCamp
		INTO SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT_UPDATE_name_campaign
		FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT
		--WHERE [CAMPAIGN:External_Id__c]='SAFH Don/employee'

		SELECT * FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PARENT_UPDATE_name_campaign

/*plan
current campaigns 
