USE SUTTER_MGEA_MIGRATION
GO

 
BEGIN-- UPDATE MATCHING gift information

			 SELECT		T3.RE_DB_OwnerShort+'-'+T3.RE_DB_Tbl+'-'+T3.GSplitImpID AS External_ID__c, 
						T3.GSplitAmt AS rC_Giving__Matching_Amount__c,
						T1.RE_DB_OwnerShort+'-'+T1.MatchedBy_ImportID AS [rC_Giving__Matching_Account__r:External_Id__c],
						T4.RE_DB_OwnerShort+'-'+T4.RE_DB_Tbl+'-'+T4.GSplitImpID AS [rC_Giving__Matching_Opportunity__r:External_Id__c],
						
						--ref
						T1.RE_DB_OwnerShort, 
						T1.GFLink, 
						T2.GFType,
						T3.GSplitImpID,
						T3.GSplitAmt, 
						T1.MatchedBy_ImportID,  
						T1.MatchedBy_OrgName,
						T1.Matching_GFImpID,
						T4.GSplitImpID AS Matching_GSplitImpID

			 INTO SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_MATCHING_GIFT
			 FROM SUTTER_MGEA_DATA.dbo.HC_Gift_MatchingLink_v T1 
			 INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Gift_v T2 ON T1.GFLink=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			 INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Gift_SplitGift_v T3 ON T1.GFLink=T3.GFImpID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
			 INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Gift_SplitGift_v T4 ON T1.Matching_GFImpID=T4.GFImpID AND T1.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
			 AND T3.GFSplitSequence = T4.GFSplitSequence
			 ORDER BY T3.GSplitImpID, T4.GSplitImpID
			 
			  
	END;		
		
		/*check dupes		
				  SELECT * 
				  FROM SUTTER_MGEA_DATA.dbo.HC_Gift_MatchingLink_v
				  WHERE Matching_GFImpID IN (SELECT Matching_GFImpID FROM SUTTER_MGEA_DATA.dbo.HC_Gift_MatchingLink_v GROUP BY Matching_GFImpID HAVING COUNT(*)>1)
				  
				 
		*/
		
		
				