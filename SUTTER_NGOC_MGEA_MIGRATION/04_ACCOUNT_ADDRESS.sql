USE SUTTER_MGEA_MIGRATION
GO


BEGIN
	DROP TABLE SUTTER_MGEA_MIGRATION.IMP.ACCOUNT_ADDRES	
END



BEGIN --ACCOUNT ADDRESS 

			--CONSTITUENT ORGANIZATION to ORG ACCOUNTS. 
			SELECT dbo.fnc_OwnerID() AS OwnerID
			,(T.RE_DB_OwnerShort+'-'+T.ImportID) AS [rC_Bios__Account__r:External_ID__c]
			,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c 
			,CASE T2.rC_Bios__Active__c when '1' THEN 'TRUE' WHEN '0' THEN 'FALSE' END AS rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='0' THEN 'FALSE' ELSE T1.AddrPref END AS rC_Bios__Preferred_Billing__c
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c

			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			, NULL AS rC_Bios__Do_Not_Mail__c
			
			--refernce
			,'Account_Org' AS zrefSource
			,T.ImportID AS zrefImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq
			
			INTO SUTTER_MGEA_MIGRATION.IMP.ACCOUNT_ADDRES	  
			FROM SUTTER_MGEA_MIGRATION.TBL.Account_Org T
			INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Cons_Address_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.DBO.CHART_AddressType T2 ON T1.AddrType=T2.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.[Convert]='Yes'
		
		UNION ALL
			
			--CONSTITUENT INDIVIDUAL - HEAD OF HOUSEHOLD to HOUSEHOLD ACCOUNT.
			SELECT dbo.fnc_OwnerID() AS OwnerID
			,(T.RE_DB_OwnerShort+'-'+T.HH_ImportID) AS [rC_Bios__Account__r:External_ID__c]
			,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c 
			,CASE T2.rC_Bios__Active__c when '1' THEN 'TRUE' WHEN '0' THEN 'FALSE' END AS rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='0' THEN 'FALSE' ELSE T1.AddrPref END AS rC_Bios__Preferred_Billing__c			
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c

			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			, NULL AS rC_Bios__Do_Not_Mail__c
			
			--reference
			,'Contact_HofH' AS zrefSource
			,T.ImportID AS zrefImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq
		 
			
			FROM SUTTER_MGEA_MIGRATION.TBL.Contact_HofH T
			INNER JOIN SUTTER_MGEA_DATA.DBO.HC_Cons_Address_v T1 ON T.HH_ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA_MIGRATION.DBO.CHART_AddressType T2 ON T1.AddrType=T2.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.[Convert]='Yes'
			
			UNION ALL 
		
		 
			--NON-CONSTITUENT ORGANIZATION RELATIONSHIP
			SELECT
			 dbo.fnc_OwnerID() AS OwnerID
 		 	,(T.RE_DB_OwnerShort+'-'+T.ORImpID) AS [rC_Bios__Account__r:External_ID__c]
 		 	,(T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.ORImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c
			,CASE T2.rC_Bios__Active__c when '1' THEN 'TRUE' WHEN '0' THEN 'FALSE' END AS rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='FALSE' THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Preferred_Billing__c			
			,T.[ORAddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T.[ORAddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T.[ORAddrCity] AS rC_Bios__Original_City__c
			,T.[ORAddrState] AS rC_Bios__Original_State__c
			,T.[ORAddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T.[ORAddrCountry] AS rC_Bios__Original_Country__c

			,CONVERT(NVARCHAR(10),T.[ORAddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T.[ORAddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			,T.ORNoMail AS rC_Bios__Do_Not_Mail__c
			
			--reference
			,'HC_Org_Relat_v' AS zrefSource
			,T.ORImpID AS zrefImportID
			,T.ORImpID AS zrefAddrImpID
			,T2.RE_DB_OwnerShort AS zrefDBOwner
			,NULL AS zrefAddrSeq
			 		
			
			FROM SUTTER_MGEA_DATA.dbo.HC_Org_Relat_v T
			LEFT JOIN SUTTER_MGEA_MIGRATION.DBO.CHART_AddressType T2 ON T.ORAddrType=T2.AddrType AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.[Convert]='Yes' AND T.ORLink IS NULL OR T.ORLink=''
		
END;

BEGIN--REMOVE DOUBLE QUOTES
	--  SELECT [rC_Bios__Original_Street_Line_1__c] FROM [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT_ADDRES] WHERE [rC_Bios__Original_Street_Line_1__c] LIKE '%"%';
	--  SELECT [rC_Bios__Original_Street_Line_2__c] FROM [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT_ADDRES] WHERE [rC_Bios__Original_Street_Line_2__c] LIKE '%"%';        
    --  SELECT [rC_Bios__Original_City__c] FROM [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT_ADDRES] WHERE [rC_Bios__Original_City__c] LIKE '%"%';        
    UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT_ADDRES] SET rC_Bios__Original_Street_Line_1__c=REPLACE(rC_Bios__Original_Street_Line_1__c,'"','''') where rC_Bios__Original_Street_Line_1__c like '%"%'
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT_ADDRES] SET rC_Bios__Original_Street_Line_2__c=REPLACE(rC_Bios__Original_Street_Line_2__c,'"','''') where rC_Bios__Original_Street_Line_2__c like '%"%'
	UPDATE [SUTTER_MGEA_MIGRATION].[IMP].[ACCOUNT_ADDRES] SET rC_Bios__Original_City__c=REPLACE(rC_Bios__Original_City__c,'"','''') where rC_Bios__Original_City__c like '%"%'
END


BEGIN--check duplicate ACCOUNT_ADDRESS
 
	 SELECT *
	 FROM SUTTER_MGEA_MIGRATION.IMP.ACCOUNT_ADDRES	  
	 WHERE rC_Bios__External_ID__c IN (SELECT rC_Bios__External_ID__c FROM SUTTER_MGEA_MIGRATION.IMP.ACCOUNT_ADDRES	  
									GROUP BY rC_Bios__External_ID__c HAVING COUNT(*)>1)
END

BEGIN
	SELECT COUNT(*) FROM SUTTER_MGEA_MIGRATION.IMP.ACCOUNT_ADDRES	
END




SELECT     T1.*
INTO	SUTTER_MGEA_MIGRATION.IMP.ACCOUNT_ADDRES_exc_hh
FROM         IMP.ACCOUNT_ADDRES AS T1 LEFT OUTER JOIN
                      IMP.zACCOUNT_ADDRES AS T2 ON T1.[rC_Bios__Account__r:External_ID__c] = T2.[rC_Bios__Account__r:External_ID__c]
WHERE     (T2.[rC_Bios__Account__r:External_ID__c] IS NULL)

