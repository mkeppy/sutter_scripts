USE SUTTER_MGEA_MIGRATION
GO

BEGIN
		DROP TABLE SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PROPOSAL
END



BEGIN--PROPOSALS
					SELECT  
					dbo.fnc_OwnerID() AS OwnerID         
					,T1.RE_DB_OwnerShort+'-'+T1.PRImpID	AS	RE_PRImpID__c
					,T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.PRImpID	AS	External_Id__c
					,CASE WHEN T4.NoHH_ImportID IS NOT NULL THEN T4.RE_DB_OwnerShort+'-'+T4.HH_ImportID ELSE T1.RE_DB_OwnerShort+'-'+T1.ImportID END AS [ACCOUNT:External_ID__c]
					,CAST(CASE WHEN T1.PRDateAsk	IS NULL THEN T1.DateAdded ELSE T1.PRDateAsk END AS DATE) AS	CloseDate
					,T1.PRName +'  '+ T1.DateAdded AS Name
					,dbo.fnc_RecordType('Opportunity_Proposal') AS RecordTypeID
					--,T1.PRCampID	AS	[CAMPAIGN:External_ID__c]
					,CASE WHEN (T5.Proposal_Status__c IS NULL AND (CASE WHEN T1.PRDateAsk IS NULL THEN T1.DateAdded ELSE T1.PRDateAsk END) <=GETDATE()) 
								THEN 'Completed' WHEN (T5.Proposal_Status__c IS NULL AND (CASE WHEN T1.PRDateAsk IS NULL THEN T1.DateAdded ELSE T1.PRDateAsk END) >GETDATE()) 
								THEN 'Open' ELSE T5.Proposal_Status__c END AS StageName
					,T1.PRAmtAsk	AS	rC_Giving__Projected_Amount__c
					,NULL AS rC_Giving__Expected_Giving_Amount__c
					,T1.RE_DB_OwnerShort	AS	rC_Giving__Affiliation__c
					,GETDATE()	AS	rC_Giving__Update_Transactions__c
					,GETDATE()	AS	rC_Giving__Rollup_Transactions__c
					,GETDATE()	AS	rC_Giving__Rollup_Giving__c
					
					,T3.[GAU External ID] AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]
					
					,T1.PRPurpose	AS	Proposal_Purpose__c
					,T1.PRRating    AS  Proposal_Rating__c
					,T1.PRReason    AS  Proposal_Reason__c
						,T1.DateAdded	AS	CreatedDate
					,CAST(T2.RE_Team_Member__c AS VARCHAR(255)) AS RE_Team_Member__c 

					--reference
					,T1.ImportID AS zref_ImportID
					,T4.HH_ImportID AS zref_HH_ImportID 
					,T4.NoHH_ImportID AS zref_NoHH_ImportID
	
					INTO SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PROPOSAL
					FROM SUTTER_MGEA_DATA.dbo.HC_Proposal_v T1
					LEFT JOIN SUTTER_MGEA_MIGRATION.TBL.Attribute_opportunity_proposal_1  T2 ON T1.PRImpID=T2.PRAttrPRImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
					LEFT JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_Fund T3 ON T1.PRFundID=T3.FundID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
					LEFT JOIN SUTTER_MGEA_MIGRATION.tbl.Contact_HofH T4 ON T1.ImportID=T4.NoHH_ImportID AND T1.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
					LEFT JOIN SUTTER_MGEA_MIGRATION.dbo.CHART_PRStatus T5 ON T1.PRStatus=T5.PRStatus AND T1.RE_DB_OwnerShort=T5.RE_DB_OwnerShort
					WHERE T1.PRName NOT LIKE '%planned%'
					ORDER BY CloseDate DESC, t1.RE_DB_OwnerShort, T1.PRImpID
					
END	   

BEGIN	
				SELECT * 
				FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PROPOSAL
				WHERE RE_PRImpID__c IN (SELECT RE_PRImpID__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PROPOSAL GROUP BY RE_PRImpID__c HAVING COUNT(*) >1)

				SELECT * 
				FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PROPOSAL
				WHERE External_Id__c IN (SELECT External_Id__c FROM SUTTER_MGEA_MIGRATION.IMP.OPPORTUNITY_PROPOSAL GROUP BY External_Id__c HAVING COUNT(*) >1)
END

		/* CAMPAIGN ON PROPOSALS IS TBD. 
		
				SELECT PRCampID FROM SUTTER_MGEA_DATA.DBO.HC_Proposal_v  GROUP BY PRCampID
				  SELECT DISTINCT RE_DB_OwnerShort, CampId, AppealId, PackageId, PRIMARY_CAMPAIGN_SOURCE FROM SUTTER_MGEA_MIGRATION.TBL.CAMPAIGN
				  ORDER BY RE_DB_OwnerShort, CampId, PRIMARY_CAMPAIGN_SOURCE 
				  
		*/ 
		
		
		/*Single gift applied to multiple proposals. 
				
			SELECT * FROM SUTTER_MGEA_DATA.dbo.HC_Gift_ProposalLink_v
			WHERE GFLink IN (SELECT GFLink FROM SUTTER_MGEA_DATA.dbo.HC_Gift_ProposalLink_v GROUP BY GFLink HAVING COUNT(*)>1)
			ORDER BY GFLink		
		*/
		
	 