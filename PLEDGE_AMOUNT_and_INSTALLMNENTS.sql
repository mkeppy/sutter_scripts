				DROP TABLE [SUTTER_1P_MIGRATION].IMP.UPDATE_OPPORTUNITY_OPEN
				
				SELECT  
					 CAST((T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.GFInsImpID) AS VARCHAR(35)) AS External_Id__c
 					,T.RE_DB_OwnerShort+' - Installment - '+CAST(CAST(T.GFInsDate AS DATE)AS NVARCHAR(10))AS NAME	
					,CAST(T.GFInsBalance AS DECIMAL(12,2)) AS Amount 
					,CAST(T.GFInsBalance AS DECIMAL(12,2)) AS rC_Giving__Current_Giving_Amount__c
					,CAST((T1.GSplitRE_DB_OwnerShort+'-'+T1.GSplitRE_DB_Tbl+'-'+T1.GSplitImpID) AS VARCHAR(35))  AS [zrefParentExternal_ID__c]
					,CAST((CASE WHEN (T1.GFType='Pledge' OR T1.GFType='MG Pledge') THEN (T1.GFTAmt/T1.GFInsNumPay) ELSE T1.GSplitAmt END) AS DECIMAL(10,2)) AS zrefParentGiving_Amount__c
					,CAST(T.GFInsDate AS DATE) AS zrefCloseDate
				INTO [SUTTER_1P_MIGRATION].TBL.UPDATE_OPPORTUNITY_OPEN	 		 		 
				FROM [SUTTER_1P_DATA].DBO.[HC_Gift_Installments_v] T
/*Parent ID*/	INNER JOIN [SUTTER_1P_MIGRATION].TBL.GIFT T1 ON T.GFLink=T1.GSplitGFImpID AND T.RE_DB_OwnerShort=T1.GSplitRE_DB_OwnerShort  
				WHERE T.[GFInsBalance]>0    
	 			UNION 
				SELECT  
				 	 CAST((T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.GFInsImpID) AS VARCHAR(35)) AS External_Id__c
 					,T.RE_DB_OwnerShort+' - Installment - '+CAST(CAST(T.GFInsDate AS DATE)AS NVARCHAR(10))AS NAME	
					,CAST(T.GFInsBalance AS DECIMAL(12,2)) AS Amount 
					,CAST(T.GFInsBalance AS DECIMAL(12,2)) AS rC_Giving__Current_Giving_Amount__c
					,CAST((T1.GSplitRE_DB_OwnerShort+'-'+T1.GSplitRE_DB_Tbl+'-'+T1.GSplitImpID) AS VARCHAR(35))  AS [zrefParentExternal_ID__c]
					,CAST((CASE WHEN (T1.GFType='Pledge' OR T1.GFType='MG Pledge') THEN (T1.GFTAmt/T1.GFInsNumPay) ELSE T1.GSplitAmt END) AS DECIMAL(10,2)) AS zrefParentGiving_Amount__c
 					,CAST(T.GFInsDate AS DATE) AS zrefCloseDate
 				FROM [SUTTER_1P_DATA].DBO.[HC_Gift_Installments_v] T
/*Parent ID*/	INNER JOIN [SUTTER_1P_DELTA].TBL.GIFT T1 ON T.GFLink=T1.GSplitGFImpID AND T.RE_DB_OwnerShort=T1.GSplitRE_DB_OwnerShort  
			 	WHERE T.[GFInsBalance]>0  
				ORDER BY [zrefParentExternal_ID__c], zrefCloseDate DESC
				-- (8178 row(s) affected)
	
				SELECT External_Id__c, NAME, Amount, rC_Giving__Current_Giving_Amount__c 
				,zrefCloseDate
				,[zrefParentExternal_ID__c], zrefParentGiving_Amount__c 
				,ROW_NUMBER() OVER ( PARTITION BY [zrefParentExternal_ID__c] ORDER BY zrefParentGiving_Amount__c, zrefCloseDate DESC) AS zrefSeq 
				,CASE WHEN Amount!=zrefParentGiving_Amount__c THEN 'FALSE' ELSE 'TRUE' END AS zrefParentAmtDifferent
				INTO [SUTTER_1P_MIGRATION].IMP.UPDATE_OPPORTUNITY_OPEN	
				FROM [SUTTER_1P_MIGRATION].TBL.UPDATE_OPPORTUNITY_OPEN	
	 
	
				  SELECT * FROM [SUTTER_DATA_T2].dbo.[HC_Gift_SplitGift_v] WHERE [GFImpID]='06307-545-0000052110'
				  SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_SplitGift_v] WHERE [GFImpID]='06307-545-0000052110'
	
				 SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Installments_v] WHERE [GFLink]='06307-545-0000052110'
				SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Installments_v] WHERE [GFInsImpID]='06307-556-0000018360'
				  
				SELECT * FROM [SUTTER_1P_MIGRATION].imp.opportunity_parent WHERE re_gfimpid__c LIKE '%06307-545-0000052110%'
	
				   
	 SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Constituents_v] WHERE [LastName]='kohn' AND [FirstName]='barbara'

  

	 SELECT [ImportID], dbo.hc_gift_v.[GFImpID], [GFType], [GFTAmt],dbo.hc_gift_v.[RE_DB_OwnerShort], [GFInsFreq] , GFInsNumPay, (GFTAmt/GFInsNumPay) amt
	 ,CASE WHEN (GFType='Pledge' OR GFType='MG Pledge') THEN (GFTAmt/GFInsNumPay) ELSE GSplitAmt END AS rC_Giving__Giving_Amount__c
	 FROM SUTTER_DATA_T2.dbo.hc_gift_v 
	 INNER JOIN SUTTER_DATA_T2.dbo.[HC_Gift_SplitGift_v] ON  dbo.[HC_Gift_SplitGift_v].[GFImpID]=dbo.hc_gift_v.[GFImpID]
	 WHERE dbo.hc_gift_v.[ImportID]='13941'	AND dbo.hc_gift_v.[GFImpID]='00001-545-0000337270' --   2,272.73
 	 ORDER BY [GFType]
			 
	 SELECT * FROM [SUTTER_DATA_T2].dbo.[HC_Gift_SplitGift_v] WHERE [GFImpID] LIKE '%00001-545-0000337270'

	 SELECT * FROM [SUTTER_DATA_T2].dbo.[HC_Gift_Link_v] WHERE [GFImpID] LIKE '%00001-545-0000337270'

	 SELECT DISTINCT [GFLink] FROM [SUTTER_DATA_T2].dbo.[HC_Gift_Installments_v] WHERE [GFInsBalance]>0    [GFLink] LIKE '%00001-545-0000337270'

	  SELECT * FROM [SUTTER_DATA_T2].dbo.[HC_Gift_Installments_v] WHERE [GFInsBalance]>0   AND  [GFLink] LIKE '%05502-545-0000424854'

	 SELECT * FROM [SUTTER_1P_MIGRATION].imp.opportunity_parent WHERE [RE_GFImpID__c] like	 '%00001-545-0000337270'
	 SELECT * FROM [SUTTER_1P_MIGRATION].imp.opportunity_Transaction WHERE 	[rC_Giving__Parent__r:External_ID__c] LIKE '%00001-700-0000202962'

/********************
	 parent Giving Amount AND compare TO the LAST Installement Balance Amount, ON pledges WITH installments WHERE the balanace IS >0.

	 1, run update on all gift instllments to re-populate AMOUNT on trx. AND Name 
	 2, may need to run update on Parent oppo from pledges base on comparison abovve. 

********************/ 