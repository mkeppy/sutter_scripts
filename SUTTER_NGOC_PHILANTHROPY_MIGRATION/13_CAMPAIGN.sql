USE [SUTTER_1P_MIGRATION]
GO



BEGIN--GET COUNTS()
		SELECT DISTINCT Campaign_Level_1, CAMPAIGNLevel1_Campaign_Description__c FROM SUTTER_1P_DATA.DBO.CHART_Campaign WHERE Campaign_Level_1 IS NOT null
		--109
		SELECT DISTINCT Campaign_Level_2, CAMPAIGNLevel2_Campaign_Description__c FROM SUTTER_1P_DATA.DBO.CHART_Campaign WHERE Campaign_Level_2 IS NOT null
		--11,779
		SELECT DISTINCT Campaign_Level_3, CAMPAIGNLevel3_Campaign_Description__c FROM SUTTER_1P_DATA.DBO.CHART_Campaign WHERE Campaign_Level_3 IS NOT null
		--43284
END

BEGIN
		DROP TABLE SUTTER_1P_MIGRATION.TBL.CAMPAIGN
		DROP TABLE SUTTER_1P_MIGRATION.IMP.CAMPAIGN
END


 

BEGIN--CREATE BASE TABLE for CAMPAIGNS. 
 
		SELECT DISTINCT T1.RE_DB_OwnerShort, T1.FundId, T1.CampId, T1.AppealId, T1.PackageId, T1.FundDesc, T1.CampDesc, T1.AppDesc, T1.PackDesc,
		T1.Campaign_Level_1, T1.CAMPAIGNLevel1_Campaign_Description__c, 
		T1.Campaign_Level_2, T1.CAMPAIGNLevel2_Campaign_Description__c, 
		T1.Campaign_Level_3, T1.CAMPAIGNLevel3_Campaign_Description__c,  

		PRIMARY_CAMPAIGN_SOURCE=CASE WHEN T1.Campaign_Level_3 IS NOT NULL THEN T1.Campaign_Level_3
						     WHEN T1.Campaign_Level_2 IS NOT NULL THEN T1.Campaign_Level_2
						     WHEN T1.Campaign_Level_1 IS NOT NULL THEN T1.Campaign_Level_1 END, 
		T1.PrimaryGAU_ExternalID__c,
	
		rC_Giving__Campaign_Type__c = CASE WHEN T1.Campaign_Level_3 IS NOT NULL THEN T3.Campaign_TYPE
						     WHEN T1.Campaign_Level_2 IS NOT NULL THEN T3.Campaign_TYPE 
							 END, 

		T1.rC_Giving__Affiliation__c,
	 	T1.rC_Giving_Fundraising_Program__c,
		T5.CAMPAIGN_Level1_rC_Giving__Sub_Affiliation__c,
		T7.CAMPAIGN_Level3_rC_Giving__Sub_Affiliation__c

		INTO SUTTER_1P_MIGRATION.TBL.CAMPAIGN   
		FROM SUTTER_1P_DATA.DBO.CHART_Campaign T1
		--appeal
		LEFT JOIN SUTTER_1P_DATA.dbo.HC_Appeal_v T2 ON T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort AND T1.AppealID=t2.AppealID
		LEFT JOIN SUTTER_1P_DATA.DBO.CHART_AppealCategory T3 ON T2.RE_DB_OwnerShort=T3.RE_DB_OwnerShort AND T2.AppCategory=T3.AppCategory
		--camp
		LEFT JOIN [SUTTER_1P_DATA].dbo.[HC_Campaign_v] T4 ON T1.[RE_DB_OwnerShort]=T4.[RE_DB_OwnerShort] AND T1.[CampID]=T4.[CampID]
		LEFT JOIN [SUTTER_1P_DATA].DBO.[CHART_CampaignCategory] T5 ON T4.[RE_DB_OwnerShort]=T5.[RE_DB_OwnerShort]  AND T4.[CampCategory]=T5.[CampCategory]
		--pack
		LEFT JOIN [SUTTER_1P_DATA].dbo.[HC_Package_v] T6 ON T1.[RE_DB_OwnerShort]=T6.[RE_DB_OwnerShort] AND T1.[PackageID]=T6.[PackageID] AND T1.[AppealID]=T6.[AppealID]
		LEFT JOIN [SUTTER_1P_DATA].DBO.[CHART_PackageCategory] T7 ON T6.[RE_DB_OwnerShort]=T7.[RE_DB_OwnerShort]  AND T6.PackCategory=T7.[PackCategory]

		--126,810	 --final 133,042
		
	  	 -- SELECT * FROM [SUTTER_1P_MIGRATION].tbl.[CAMPAIGN] WHERE [Campaign_Level_2]='ABSF AA14C'	  ABSFANNUAL	ABSF AA14C

END;


BEGIN--CAMPAIGN

		--LEVEL 3
				SELECT DISTINCT
	 			 OwnerID= dbo.fnc_OwnerID() 
				,RecordTypeID= dbo.fnc_RecordType('Campaign_Standard')
				,Name= T1.Campaign_Level_3
				,External_Id__c= T1.Campaign_Level_3 
				,Campaign_Description__c= T1.CAMPAIGNLevel3_Campaign_Description__c 
				,T1.rC_Giving__Affiliation__c
				,rC_Giving__Sub_Affiliation__c=MAX(T1.CAMPAIGN_Level3_rC_Giving__Sub_Affiliation__c) 
				,rC_Giving__Campaign_Type__c = MAX(T1.rC_Giving__Campaign_Type__c)  
		 
				--PKG info
				,[Description]= NULL 
				,StartDate= CAST(MIN(T2.PackStartDate) AS DATE)
				,EndDate= CAST(MAX(T2.PackEndDate) AS DATE)
				,IsActive= MAX(CASE WHEN T2.PackIsInactive='TRUE' THEN 'FALSE' ELSE 'TRUE' END)
				,[Status]= CASE WHEN (MAX(CASE WHEN T2.PackIsInactive='TRUE' THEN 'FALSE' ELSE 'TRUE' END)) ='False' THEN 'Completed' ELSE 'In Progress' END 
				
				--ref
				,LEN(T1.Campaign_Level_3) AS zrefCAMPLen
				,'Level_3' AS zrefCampLevel
				,MAX(T1.rC_Giving__Campaign_Type__c) AS z_ref_camp_type
				,NULL AS zref_fundraisprog
								
				INTO SUTTER_1P_MIGRATION.IMP.CAMPAIGN
				FROM SUTTER_1P_MIGRATION.TBL.CAMPAIGN T1 
				LEFT JOIN SUTTER_1P_DATA.dbo.HC_Package_v T2 ON T1.PackageId=T2.PackageID 
														 AND T1.AppealId=T2.AppealID 
														 AND T1.RE_DB_OwnerShort =T2.RE_DB_OwnerShort
				WHERE T1.Campaign_Level_3 IS NOT NULL   AND T1.Campaign_Level_3!=''
				GROUP BY T1.Campaign_Level_3, T1.CAMPAIGNLevel3_Campaign_Description__c, T1.rC_Giving__Affiliation__c, T1.PrimaryGAU_ExternalID__c
					 
				--38,037
				
			UNION ALL 
		
		--LEVEL 2
				SELECT DISTINCT
				 OwnerID= dbo.fnc_OwnerID() 
				,RecordTypeID= dbo.fnc_RecordType('Campaign_Standard')
				,Name= T1.Campaign_Level_2
				,External_Id__c= T1.Campaign_Level_2 
				,Campaign_Description__c= T1.CAMPAIGNLevel2_Campaign_Description__c 
				,T1.rC_Giving__Affiliation__c 
				,rC_Giving__Sub_Affiliation__c  = NULL 
				,rC_Giving__Campaign_Type__c = CASE WHEN MAX(T1.rC_Giving__Campaign_Type__c) IS NULL THEN MAX(T1.rC_Giving_Fundraising_Program__c) ELSE MAX(T1.rC_Giving__Campaign_Type__c) END 
 	 
				--APP info
				,[Description]= MAX(CAST(T2.AppNote AS NVARCHAR(4000)))
				,StartDate= CAST(MIN(T2.AppStartDate) AS DATE)
				,EndDate= CAST(MAX(T2.AppEndDate) AS DATE)
				,IsActive= MAX(CASE WHEN T2.AppIsInactive='TRUE' THEN 'FALSE' ELSE 'TRUE' END)
				,[Status]= CASE WHEN (MAX(CASE WHEN T2.AppIsInactive='TRUE' THEN 'FALSE' ELSE 'TRUE' END)) ='False' THEN 'Completed' ELSE 'In Progress' END 

				--ref
				,LEN(T1.Campaign_Level_2) zrefCAMPLen 
				,'Level_2' AS zrefCampLevel
				,MAX(T1.rC_Giving__Campaign_Type__c) AS z_ref_camp_type
				,MAX(T1.rC_Giving_Fundraising_Program__c) AS zref_fundraisprog
				
				--INTO SUTTER_1P_MIGRATION.dbo.zTestCampL2  --drop table SUTTER_1P_MIGRATION.dbo.zTestCampL2  
				FROM SUTTER_1P_MIGRATION.TBL.CAMPAIGN T1 
				LEFT JOIN SUTTER_1P_DATA.dbo.HC_Appeal_v T2 ON T1.AppealID=T2.AppealID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T1.Campaign_Level_2 IS NOT NULL 
				GROUP BY T1.Campaign_Level_2, T1.CAMPAIGNLevel2_Campaign_Description__c, T1.rC_Giving__Affiliation__c 
				--11,831 
						 
			UNION ALL 
			
		--LEVEL 1
				SELECT DISTINCT
				 OwnerID= dbo.fnc_OwnerID() 
				,RecordTypeID= dbo.fnc_RecordType('Campaign_Standard')
				,Name= T1.Campaign_Level_1
				,External_Id__c= T1.Campaign_Level_1 
				,Campaign_Description__c= T1.CAMPAIGNLevel1_Campaign_Description__c 
		 		,T1.rC_Giving__Affiliation__c
				,rC_Giving__Sub_Affiliation__c = MAX(T1.CAMPAIGN_Level1_rC_Giving__Sub_Affiliation__c)
				,rC_Giving__Campaign_Type__c=NULL 
 
				--camp info
				,[Description]= MAX(CAST(T2.CampNote AS NVARCHAR(4000)))
				,StartDate= CAST(MIN(T2.CampStartDate) AS DATE)
				,EndDate= CAST(MAX(T2.CampEndDate) AS DATE)
				,IsActive= MAX(CASE WHEN T2.CampIsInactive='TRUE' THEN 'FALSE' ELSE 'TRUE' END)
				,[Status]= CASE WHEN (MAX(CASE WHEN T2.CampIsInactive='TRUE' THEN 'FALSE' ELSE 'TRUE' END)) ='False' THEN 'Completed' ELSE 'In Progress' END 
				,LEN(T1.Campaign_Level_1) zrefCAMPLen
				,'Level_1' AS zrefCampLevel
				,NULL AS z_ref_camp_type
				,NULL AS zref_fundraisprog			

				FROM SUTTER_1P_MIGRATION.TBL.CAMPAIGN T1 
				LEFT JOIN SUTTER_1P_DATA.DBO.HC_Campaign_v T2 ON T1.CampID=T2.CampID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T1.Campaign_Level_1 IS NOT NULL
				GROUP BY T1.Campaign_Level_1, T1.CAMPAIGNLevel1_Campaign_Description__c, T1.rC_Giving__Affiliation__c
				ORDER BY t1.[Campaign_Level_1]
			--128
  			 --ABSFANNUAL
			 --ABSFANNUAL

			--total: 55172
END; --end of creata imp campaign  55172

BEGIN--REMOVE DOUBLE QUOTES

			SELECT * FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN where Campaign_Description__c LIKE '%"%'
			SELECT * FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN where [Description] LIKE '%"%'
			
			UPDATE SUTTER_1P_MIGRATION.IMP.CAMPAIGN 
			SET Campaign_Description__c=REPLACE(Campaign_Description__c,'"','''') 
			WHERE Campaign_Description__c LIKE '%"%'
			
			UPDATE SUTTER_1P_MIGRATION.IMP.CAMPAIGN 
			SET [Description]=REPLACE([Description],'"','''') 
			WHERE [Description] LIKE '%"%'
			
		BEGIN 
			USE [SUTTER_1P_MIGRATION] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'CAMPAIGN'
 		END
END

SELECT [External_ID__c], zrefCampLevel 
FROM [SUTTER_1P_MIGRATION].IMP.CAMPAIGN 

BEGIN--DATA CHECK

			--test for duplicates
					SELECT * FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN
					WHERE Name IN (SELECT NAME FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN GROUP BY name HAVING COUNT(*) >1)
					ORDER BY name
					
					SELECT * FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN
					WHERE External_Id__c IN (SELECT External_Id__c FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN GROUP BY External_Id__c HAVING COUNT(*) >1)
					ORDER BY External_Id__c
					
			--check for Start/End dates
					SELECT * FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN
					WHERE EndDate < StartDate
					
					UPDATE SUTTER_1P_MIGRATION.IMP.CAMPAIGN
					SET StartDate=EndDate, EndDate=StartDate
					WHERE EndDate < StartDate 
						
END; 	

BEGIN--CHECK LEN OF EXTERANL ID
			SELECT  LEN([External_Id__c]) L , *
			FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN
			WHERE LEN([External_Id__c])>60
			ORDER BY L DESC 
END;

SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].[CAMPAIGN]

BEGIN--UPDATE OF PARENT CAMPAIGN ON CAMP LEVEL 3 and CAMP LEVEL 2 and GAU on CAMP LEVEL 3
	 --UPDATE PARENT checkbox
				DROP TABLE SUTTER_1P_MIGRATION.TBL.CAMPAIGN_UPDATE

				SELECT DISTINCT 
				T2.ID
				,[rC_Giving__GAU__r:rc_Giving__External_Id__c]=T1.PrimaryGAU_ExternalID__c
				,zrefExternal_Id__c=T1.Campaign_Level_3
				INTO [SUTTER_1P_MIGRATION].[IMP].UPDATE_CAMPAIGN_GAU
				FROM [SUTTER_1P_MIGRATION].[TBL].[CAMPAIGN]	AS	T1
				INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[CAMPAIGN] AS T2 ON T1.[Campaign_Level_3]=T2.[EXTERNAL_ID__C]
				WHERE Campaign_Level_3 IS NOT NULL AND Campaign_Level_3!=''
				  --43284
				UNION
				SELECT DISTINCT 
				T2.ID
				,[rC_Giving__GAU__r:rc_Giving__External_Id__c]=T1.PrimaryGAU_ExternalID__c
				,zrefExternal_Id__c=T1.Campaign_Level_2

				FROM [SUTTER_1P_MIGRATION].[TBL].[CAMPAIGN]	AS	T1
				INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[CAMPAIGN] AS T2 ON T1.[Campaign_Level_2]=T2.[EXTERNAL_ID__C]
				WHERE T1.[Campaign_Level_2] IS NOT NULL AND T1.[Campaign_Level_2]!=''
				   --11779
				UNION
				SELECT DISTINCT 
				T2.ID
				,[rC_Giving__GAU__r:rc_Giving__External_Id__c]=T1.PrimaryGAU_ExternalID__c
				,zrefExternal_Id__c=T1.Campaign_Level_1

				FROM [SUTTER_1P_MIGRATION].[TBL].[CAMPAIGN]	AS	T1
				INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[CAMPAIGN] AS T2 ON T1.Campaign_Level_1=T2.[EXTERNAL_ID__C]
				WHERE T1.Campaign_Level_1 IS NOT NULL AND T1.Campaign_Level_1!=''

				--55172
	
			  --PARENT CAMPAIGN
				SELECT DISTINCT T2.ID , 
								T3.ID AS ParentId  ,
								rC_Giving__Is_Parent__c='FALSE' ,
								zrefExternal_Id__c= T1.Campaign_Level_3  , 
 								zrefParExternal_Id__c= T1.Campaign_Level_2 
 				INTO [SUTTER_1P_MIGRATION].[IMP].UPDATE_CAMPAIGN_PARENT	 
				FROM SUTTER_1P_MIGRATION.TBL.CAMPAIGN  AS T1
				INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[CAMPAIGN] AS T2 ON T1.Campaign_Level_3=T2.[EXTERNAL_ID__C]
				INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[CAMPAIGN] AS T3 ON T1.Campaign_Level_2=T3.[EXTERNAL_ID__C]
				WHERE (T1.Campaign_Level_3 IS NOT NULL AND  T1.Campaign_Level_3  !='')	
				  AND (T1.Campaign_Level_2 IS NOT NULL AND  T1.Campaign_Level_2  !='')			    
				 --43284
				UNION 
				SELECT DISTINCT T2.ID , 
								T3.ID AS ParentId  ,
								rC_Giving__Is_Parent__c='TRUE' ,
								zrefExternal_Id__c= T1.Campaign_Level_2  , 
 								zrefParExternal_Id__c= T1.Campaign_Level_1 
 	 			FROM SUTTER_1P_MIGRATION.TBL.CAMPAIGN  AS T1
				INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[CAMPAIGN] AS T2 ON T1.Campaign_Level_2=T2.[EXTERNAL_ID__C]
				INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[CAMPAIGN] AS T3 ON T1.Campaign_Level_1=T3.[EXTERNAL_ID__C]
				WHERE (T1.Campaign_Level_1 IS NOT NULL AND  T1.Campaign_Level_1  !='')	
				  AND (T1.Campaign_Level_2 IS NOT NULL AND  T1.Campaign_Level_2  !='')			    
				 --11779
				UNION 
				SELECT DISTINCT T2.ID , 
								NULL  AS ParentId  ,
								rC_Giving__Is_Parent__c='TRUE' ,
								zrefExternal_Id__c= T1.Campaign_Level_1  , 
 								zrefParExternal_Id__c= NULL
 	 			FROM SUTTER_1P_MIGRATION.TBL.CAMPAIGN  AS T1
				INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[CAMPAIGN] AS T2 ON T1.Campaign_Level_1=T2.[EXTERNAL_ID__C]
				WHERE (T1.Campaign_Level_1 IS NOT NULL AND  T1.Campaign_Level_1  !='')	
				--109  			    
				--TOTAL 55,172   

			   SELECT * FROM [SUTTER_1P_MIGRATION].IMP.UPDATE_CAMPAIGN_parent  WHERE parentid IS null
			--final
				DROP TABLE  SUTTER_1P_MIGRATION.IMP.CAMPAIGN_UPDATE
				
				SELECT	[External_ID__c]
						,MAX([Parent:External_Id__c]) AS [Parent:External_Id__c]
						,MAX([rC_Giving__Is_Parent__c]) AS [rC_Giving__Is_Parent__c]
						,MAX([rC_Giving__GAU__r:rc_Giving__External_Id__c]) AS [rC_Giving__GAU__r:rc_Giving__External_Id__c]

				INTO SUTTER_1P_MIGRATION.IMP.CAMPAIGN_UPDATE
				FROM SUTTER_1P_MIGRATION.TBL.CAMPAIGN_UPDATE
				WHERE External_Id__c IS NOT NULL 
				GROUP BY External_Id__c 

END;   

BEGIN--check dupes
			SELECT * 
			FROM  SUTTER_1P_MIGRATION.IMP.CAMPAIGN_UPDATE
			WHERE External_Id__c IN (SELECT External_Id__c FROM [SUTTER_1P_MIGRATION].IMP.CAMPAIGN_UPDATE GROUP BY External_Id__c HAVING COUNT(*)>1)
			ORDER BY [External_ID__c], rC_Giving__Is_Parent__c
END;

SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN
SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN_UPDATE

SELECT * FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN_UPDATE  WHERE [Parent:External_Id__c] IS null
ORDER BY [External_ID__c], [Parent:External_Id__c] 	

SELECT * FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN_UPDATE
WHERE [Parent:External_Id__c]='SSCF R16B'

SSCF R16B
SSRF 12EMPLOYEE
SSRF 12WEB
SSRF 14EMPLOYEE
SSRF 15EG
SSRF 16EG
SSRF CPMC12APFLLWUP
SSRF CPMC12DRSDAY
SSRF CPMC12DRSDAYFLLWUP
SSRF CPMC12PRIORITIES
THFX 16DM05
THFX 2012AA
