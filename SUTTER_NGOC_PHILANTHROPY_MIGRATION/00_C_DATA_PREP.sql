
USE SUTTER_DATA
GO
	 
	 
	 
BEGIN--PREFERRED ADDRESS 

		SELECT RE_DB_OwnerShort, ImportID, ConsID, AddrPref, AddrImpID, AddrLine1, AddrLine2, AddrCity, AddrState, AddrZIP, AddrCountry
		INTO SUTTER_MGEA.TBL.Address_Pref
		FROM SUTTER_DATA.dbo.HC_Cons_Address_v
		WHERE AddrPref='TRUE'
		
		BEGIN--check dupe
			SELECT RE_DB_OwnerShort, ImportID, COUNT(*) C
			FROM SUTTER_MGEA.TBL.ADDR_PREF
			GROUP BY RE_DB_OwnerShort, ImportID 
			HAVING COUNT(*)>1
			ORDER BY C desc
		END 
END



	 
BEGIN--CONSTITUENT SOLICIT CODES

			-- SELECT * FROM SUTTER_DATA.dbo.CHART_ConsSolicitCodes where sf_object_2 is not null
  			--create base table 
			 SELECT DISTINCT
					T.ImportID ,
					T.RE_DB_OwnerShort ,
					MAX(T2.Do_Not_Phone__c) AS Do_Not_Phone__c ,
					MAX(T2.Do_Not_Solicit__c) AS Do_Not_Solicit__c ,
					MAX(T2.Do_Not_Mail__c) AS Do_Not_Mail__c ,
					MAX(T2.Do_Not_Contact__c) AS Do_Not_Contact__c 
					--T.SolicitCode oldSolicitCode
			 INTO   SUTTER_MGEA.TBL.SolicitCodes		--DROP TABLE SUTTER_MGEA.TBL.SolicitCodes
			 FROM   SUTTER_DATA.dbo.HC_Cons_SolicitCodes_v T
					INNER JOIN SUTTER_MGEA.dbo.CHART_ConsSolicitCodes T2 ON T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
																  AND T.SolicitCode = T2.SolicitCode
			 WHERE  T2.SF_Object_2 != ''  
			 GROUP BY T.ImportID , 
					  T.RE_DB_OwnerShort
			 ORDER BY T.RE_DB_OwnerShort ,
					T.ImportID;  --16570
		
		BEGIN --check duplicates	
				SELECT ImportId, RE_DB_OwnerShort, COUNT(*) c
				FROM SUTTER_MGEA.TBL.SolicitCodes
				GROUP BY ImportId, RE_DB_OwnerShort
				HAVING COUNT(*) >1
		END;
END;
	
BEGIN--CONSTITUENT PHONES
 		-- SELECT COUNT(*) FROM SUTTER_DATA.DBO.HC_Cons_Phone_v  --96260
 		-- SELECT * FROM SUTTER_MGEA.dbo.CHART_PhoneType
		--SELECT * FROM SUTTER_DATA.dbo.HC_Cons_Phone_v 


		BEGIN
			DROP TABLE SUTTER_MGEA.TBL.Phones
			DROP TABLE SUTTER_MGEA.TBL.Phones_seq
			DROP TABLE SUTTER_MGEA.TBL.Phones_seq_1
			DROP TABLE SUTTER_MGEA.TBL.Phones_seq_2
			DROP TABLE SUTTER_MGEA.TBL.Phones_seq_1_final
			DROP TABLE SUTTER_MGEA.TBL.Phones_seq_2_final 
		END	
		 
		BEGIN--PHONES_1: REMOVE DEDUPLICATE PHONE NUMBER and create base table
			
				SELECT DISTINCT
						T.RE_DB_OwnerShort ,
						T.ConsID ,
						T.ImportID ,
						T.KeyInd ,
						T2.PhoneType ,
						T2.PhoneNum ,
						T3.SF_Object ,
						T3.SF_Field_API,
						T2.PhoneDoNotCall
				INTO    SUTTER_MGEA.TBL.Phones    -- select * from SUTTER_MGEA.TBL.Phones
				FROM    SUTTER_DATA.dbo.HC_Constituents_v T
						INNER JOIN SUTTER_DATA.dbo.HC_Cons_Phone_v T2 ON T.ImportID = T2.ImportID
																  AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
						INNER JOIN SUTTER_DATA.dbo.HC_Cons_Address_v T4 ON T2.PhoneAddrImpID = T4.AddrImpID    
																  AND T2.RE_DB_OwnerShort = T4.RE_DB_OwnerShort
						INNER JOIN SUTTER_MGEA.dbo.CHART_PhoneType T3 ON T2.PhoneType = T3.PhoneType
																  AND T2.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
																  AND T.KeyInd = T3.KeyInd
				WHERE T4.AddrPref='TRUE'
				UNION ALL 
				SELECT DISTINCT   --code to set to true the do not call fields. this way, the pivot fnc won't create dupe rows. 
						T.RE_DB_OwnerShort ,
						T.ConsID ,
						T.ImportID ,
						T.KeyInd ,
						T2.PhoneType ,
						'TRUE' AS PhoneNum ,
						T3.SF_Object ,
						CASE WHEN T3.SF_Field_API='HomePhone' AND T2.PhoneDoNotCall='TRUE' THEN  'rC_Bios__Home_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='MobilePhone' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Mobile_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='rC_Bios__Work_Phone__c' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Work_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='OtherPhone' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Other_Do_Not_Call__c' ELSE NULL END AS SF_Field_API,
						T2.PhoneDoNotCall
				--INTO    SUTTER_MGEA.TBL.Phones    -- select * from SUTTER_MGEA.TBL.Phones
				FROM    SUTTER_DATA.dbo.HC_Constituents_v T
						INNER JOIN SUTTER_DATA.dbo.HC_Cons_Phone_v T2 ON T.ImportID = T2.ImportID
																  AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
						INNER JOIN SUTTER_DATA.dbo.HC_Cons_Address_v T4 ON T2.PhoneAddrImpID = T4.AddrImpID    
																  AND T2.RE_DB_OwnerShort = T4.RE_DB_OwnerShort
						INNER JOIN SUTTER_MGEA.dbo.CHART_PhoneType T3 ON T2.PhoneType = T3.PhoneType
																  AND T2.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
																  AND T.KeyInd = T3.KeyInd
				WHERE T4.AddrPref='TRUE' AND T2.PhoneDoNotCall='TRUE'
				
				--75851

		END;
		
		BEGIN--PHONES_2: ASSIGN SEQUENCE and BASE TABLES 
				--PHONES where SEQUENCE >1 WILL BE ADDED in the same table WITH THE ADDITIONAL PHONE NUMBERS, as they'll be consolidated with in the same txt field. 
				--DO NOT INCLUDE ADDITIONAL PHONE NUMBER TYPES this time around. they'll be added later. 
			    
				SELECT 	
				T.RE_DB_OwnerShort, T.ConsID, T.ImportID, T.KeyInd, T.PhoneType, T.PhoneNum, T.SF_Object, T.SF_Field_API, T.PhoneDoNotCall
				,ROW_NUMBER() OVER ( PARTITION BY T.RE_DB_OwnerShort, T.KeyInd, T.ImportID,  T.SF_Object, T.SF_Field_API
										ORDER BY T.RE_DB_OwnerShort, T.KeyInd, T.ImportID, T.PhoneType ) AS Seq 
				INTO SUTTER_MGEA.TBL.Phones_seq			
				FROM SUTTER_MGEA.TBL.Phones T 
				WHERE (T.SF_Field_API!='Additional_Phone_Numbers__c')   -- do not include additional phones
		
		END; --58,857
			 
		BEGIN--PHONES_3: create base table for pivot tbl of PHONES where Seq =1
			
				SELECT * 
				INTO SUTTER_MGEA.TBL.Phones_seq_1		
				FROM SUTTER_MGEA.TBL.Phones_seq
				WHERE Seq='1'
				--58081 
		END;
		
		BEGIN--PHONES_4: create base table for pivot tbl of ADDITIONAL phones;
			 --includes additional numbers where Seq>1  AND those mapped to additional_phone_numbers.
				
					SELECT  *
					INTO SUTTER_MGEA.TBL.Phones_seq_2    --all other additional phones.
					FROM    SUTTER_MGEA.TBL.Phones_seq
					WHERE   Seq != '1'						--includes numbers where seq>1
					UNION
					SELECT  * , '1' Seq
					FROM    SUTTER_MGEA.TBL.Phones
					WHERE   (SF_Field_API = 'Additional_Phone_Numbers__c')  --includes additional numbers. 
					--17,770
		END;		
			
		BEGIN--PHONES_5: CREATE final pivot phone tables --PHONE TYPES from SF_Field_API
			  
			EXEC HC_PivotWizard_p   'ImportID, RE_DB_OwnerShort, ConsId, KeyInd',	--fields to include as unique identifier/normally it is just a unique ID field.
										'SF_Field_API',									--column that stores all new phone types
										'PhoneNum',										--phone numbers
										'SUTTER_MGEA.TBL.Phones_seq_1_final',			--INTO..     
										'SUTTER_MGEA.TBL.Phones_seq_1',					--FROM..
										'PhoneNum is not null'							--WHERE..
	
					/*	BEGIN --test pivot results ACCOUNT
								SELECT RE_DB_OwnerShort, ImportID, KeyInd, COUNT(*) c 
								FROM SUTTER_MGEA.TBL.Phones_seq_1
								GROUP BY RE_DB_OwnerShort, ImportID, KeyInd
								HAVING COUNT(*)>1
								ORDER BY c DESC 
								
								SELECT importID, COUNT(*) C FROM SUTTER_MGEA.TBL.Phones GROUP BY importid  ORDER BY c desc
								
								SELECT * FROM SUTTER_MGEA.TBL.Phones 
								WHERE importid='000000794'
								SELECT * FROM SUTTER_MGEA.TBL.Phones_seq_1		
								WHERE importid='000000794'
								SELECT * FROM SUTTER_MGEA.TBL.Phones_seq_1_final	
								WHERE importid='000000794'
								SELECT * FROM SUTTER_MGEA.TBL.Phones_seq_2		
								WHERE importid='000000794'
								SELECT * FROM SUTTER_MGEA.TBL.Phones_seq_2_final	
								WHERE importid='000000794'
					
								SELECT * FROM SUTTER_MGEA.TBL.Phones_seq_1
								WHERE rC_Bios__Home_Do_Not_Call__c IS NOT NULL 
								OR rC_Bios__Mobile_Do_Not_Call__c IS NOT NULL 
								OR rC_Bios__Work_Do_Not_Call__c IS NOT NULL 
								OR rC_Bios__Other_Do_Not_Call__c IS NOT NULL 
								ORDER BY ImportID
					
						END;*/
		END;		 
		 	
		BEGIN--PHONES_6: ADDITIONAL PHONE NUMBERS field
					SELECT DISTINCT T.RE_DB_OwnerShort, T.KeyInd, T.ConsID, T.ImportID,  
						STUFF(( SELECT ' '+ T1.PhoneType +': '+ T1.PhoneNum +'; ' + CHAR(10)
								FROM SUTTER_MGEA.TBL.Phones_seq_2 T1
									WHERE T1.ImportId=T.ImportId AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort AND T1.KeyInd=T.KeyInd
									ORDER BY T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportId
								FOR
									XML PATH('')
									  ), 1, 1, '') AS Additional_Phone_Numbers__c
					INTO SUTTER_MGEA.TBL.Phones_seq_2_final   
					FROM SUTTER_MGEA.TBL.Phones_seq_2 T
					ORDER BY T.RE_DB_OwnerShort, T.KeyInd, T.ImportId
		END 	
						
						
				BEGIN--test for duplicates
							SELECT  ImportId ,
									KeyInd ,
									RE_DB_OwnerShort ,
									COUNT(*) c
							FROM    SUTTER_MGEA.TBL.Phones_seq_1_final
							GROUP BY ImportId ,
										KeyInd ,
										RE_DB_OwnerShort
							HAVING  COUNT(*) > 1
							ORDER BY importid
							

							SELECT * FROM SUTTER_MGEA.TBL.Phones_seq_1_final WHERE ImportID='000000794'
							SELECT * FROM SUTTER_DATA.dbo.HC_Cons_Phone_v WHERE ImportID='000000794'

							SELECT  ImportId ,
									KeyInd ,
									RE_DB_OwnerShort ,
									COUNT(*) c
							FROM    SUTTER_MGEA.TBL.Phones_seq_2_final
							GROUP BY ImportId ,
										KeyInd ,
										RE_DB_OwnerShort
							HAVING  COUNT(*) > 1
							ORDER BY importid
				END;
		 			
END;					
	
BEGIN--INDIVIDUAL RELATIONSHIP PHONES
 		-- SELECT COUNT(*) FROM SUTTER_DATA.DBO.HC_Cons_Phone_v  --96260
 		-- SELECT * FROM SUTTER_MGEA.dbo.CHART_PhoneType
			 
		BEGIN--PHONES_1: REMOVE DEDUPLICATE PHONE NUMBER and create base table
			
					SELECT DISTINCT
						T.RE_DB_OwnerShort ,
						T.IRPhoneIRImpID ,
						T.IRPhoneType,
						T.IRPhoneNum,
						T2.SF_Field_API
					INTO    SUTTER_MGEA.TBL.IRPhones    -- DROP TABLE SUTTER_MGEA.TBL.IRPhones
					FROM    SUTTER_DATA.dbo.HC_Ind_Relat_Phone_v T
					INNER JOIN SUTTER_MGEA.dbo.CHART_IRPhoneType T2 ON T.IRPhoneType= T2.IRPhoneType AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
					INNER JOIN SUTTER_DATA.dbo.HC_Ind_Relat_v T3 ON T.IRPhoneIRImpID=T3.IRImpID
					WHERE T3.IRLink='' OR IRLink IS null 
	                                                              
					ORDER BY T.RE_DB_OwnerShort, T.IRPhoneIRImpID, T.IRPhoneType
       				--22794
		END;		 

		BEGIN--PHONES_2: ASSIGN SEQUENCE and BASE TABLES 
				--PHONES where SEQUENCE >1 WILL BE ADDED in the same table WITH THE ADDITIONAL PHONE NUMBERS, as they'll be consolidated with in the same txt field. 
				--DO NOT INCLUDE ADDITIONAL PHONE NUMBER TYPES this time around. they'll be added later. 
			    
				SELECT 	
				T.RE_DB_OwnerShort, T.IRPhoneIRImpID, T.IRPhoneType, T.IRPhoneNum, T.SF_Field_API
				,ROW_NUMBER() OVER ( PARTITION BY T.RE_DB_OwnerShort, T.IRPhoneIRImpID, T.SF_Field_API
										ORDER BY T.RE_DB_OwnerShort, T.IRPhoneIRImpID, T.IRPhoneType ) AS Seq 
				INTO SUTTER_MGEA.TBL.IRPhones_seq			--DROP TABLE SUTTER_MGEA.TBL.IRPhones_seq
				FROM SUTTER_MGEA.TBL.IRPhones T 
			
				WHERE (T.SF_Field_API!='Additional_Phone_Numbers__c')   -- do not include additional phones
				--22462
		END;
		
		BEGIN--PHONES_3: create base table for pivot tbl of PHONES where Seq =1
			
				SELECT * 
				INTO SUTTER_MGEA.TBL.IRPhones_seq_1		--DROP TABLE SUTTER_MGEA.TBL.IRPhones_seq_1
				FROM SUTTER_MGEA.TBL.IRPhones_seq
				WHERE Seq='1'
				--22091 
		END;
		
		BEGIN--PHONES_4: create base table for pivot tbl of ADDITIONAL phones;
			 --includes additional numbers where Seq>1  AND those mapped to additional_phone_numbers.
				
					SELECT  *
					INTO SUTTER_MGEA.TBL.IRPhones_seq_2    --DROP TABLE SUTTER_MGEA.TBL.IRPhones_seq_2 --all other additional phones.
					FROM    SUTTER_MGEA.TBL.IRPhones_seq
					WHERE   Seq != '1'						--includes numbers where seq>1
					UNION
					SELECT  * , '1' Seq
					FROM    SUTTER_MGEA.TBL.IRPhones
					WHERE   (SF_Field_API = 'Additional_Phone_Numbers__c')  --includes additional numbers. 
		END;		
			
		BEGIN--PHONES_5: --CREATE final pivot phone tables --PHONE TYPES from SF_Field_API
			  
			  
				EXEC HC_PivotWizard_p   'IRPhoneIRImpID, RE_DB_OwnerShort',				--fields to include as unique identifier/normally it is just an ID field.
										'SF_Field_API',									--column that stores all new phone types
										'IRPhoneNum',									--phone numbers
										'SUTTER_MGEA.TBL.IRPhones_seq_1_final',			--INTO..     -- DROP TABLE SUTTER_MGEA.TBL.IRPhones_seq_1_final
										'SUTTER_MGEA.TBL.IRPhones_seq_1',				--FROM..
										'IRPhoneNum is not null'						--WHERE..
				--17337
				
				BEGIN --test pivot results ACCOUNT
						SELECT IRPhoneIRImpID, COUNT(*) c 
						FROM SUTTER_MGEA.TBL.IRPhones_seq_1
						GROUP BY IRPhoneIRImpID
						HAVING COUNT(*)>1
						ORDER BY c DESC 
						
						SELECT importID, COUNT(*) C FROM SUTTER_MGEA.TBL.Phones GROUP BY importid  ORDER BY c desc
						
						SELECT * FROM SUTTER_MGEA.TBL.IRPhones 
						WHERE IRPhoneIRImpID='03947-518-0000142502'
						SELECT * FROM SUTTER_MGEA.TBL.IRPhones_seq_1		
						WHERE IRPhoneIRImpID='03947-518-0000142502'
						SELECT * FROM SUTTER_MGEA.TBL.IRPhones_seq_1_final	
						WHERE IRPhoneIRImpID='03947-518-0000142502'
						SELECT * FROM SUTTER_MGEA.TBL.IRPhones_seq_2		
						WHERE IRPhoneIRImpID='03947-518-0000142502'
						SELECT * FROM SUTTER_MGEA.TBL.IRPhones_seq_2_final	
						WHERE IRPhoneIRImpID='03947-518-0000142502'
				END;
		END;		 
		 	
		BEGIN--PHONE_6: ADDITIONAL PHONE NUMBERS field
					 
					SELECT DISTINCT T.RE_DB_OwnerShort, T.IRPhoneIRImpID,  
						STUFF(( SELECT ' '+T1.IRPhoneType +': '+ T1.IRPhoneNum +'; ' + CHAR(10)
								FROM SUTTER_MGEA.TBL.IRPhones_seq_2 T1
									WHERE T1.IRPhoneIRImpID=T.IRPhoneIRImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
									ORDER BY T1.RE_DB_OwnerShort, T1.IRPhoneIRImpID
								FOR
									XML PATH('')
									  ), 1, 1, '') AS Additional_Phone_Numbers__c
					INTO SUTTER_MGEA.TBL.IRPhones_seq_2_final   --DROP TABLE SUTTER_MGEA.TBL.IRPhones_seq_2_final   
					FROM SUTTER_MGEA.TBL.IRPhones_seq_2 T
					WHERE T.IRPhoneNum IS NOT null 
					ORDER BY T.RE_DB_OwnerShort, T.IRPhoneIRImpID
				
						
				BEGIN--test for duplicates
							SELECT  IRPhoneIRImpID,
									RE_DB_OwnerShort ,
									COUNT(*) c
							FROM    SUTTER_MGEA.TBL.IRPhones_seq_1_final
							GROUP BY IRPhoneIRImpID,
										RE_DB_OwnerShort
							HAVING  COUNT(*) > 1
							ORDER BY IRPhoneIRImpID
							
							SELECT  IRPhoneIRImpID ,
									RE_DB_OwnerShort ,
									COUNT(*) c
							FROM    SUTTER_MGEA.TBL.IRPhones_seq_2_final
							GROUP BY IRPhoneIRImpID,
										RE_DB_OwnerShort
							HAVING  COUNT(*) > 1
							ORDER BY IRPhoneIRImpID
				END;
		END;			

END;	
	
BEGIN--ORGANIZATION RELATIONSHIP PHONES
 		-- SELECT COUNT(*) FROM SUTTER_DATA.DBO.HC_Org_Relat_Phone_v --11525
 		-- SELECT * FROM SUTTER_MGEA.dbo.CHART_ORPhoneType
			 
		BEGIN--PHONES_1: REMOVE DEDUPLICATE PHONE NUMBER and create base table
			
					SELECT DISTINCT
						T.RE_DB_OwnerShort ,
						T.ORPhoneORImpID ,
						T.ORPhoneType,
						T.ORPhoneNum,
						T2.SF_Field_API
					INTO    SUTTER_MGEA.TBL.ORPhones    -- DROP TABLE SUTTER_MGEA.TBL.ORPhones
					FROM    SUTTER_DATA.dbo.HC_Org_Relat_Phone_v T
					INNER JOIN SUTTER_MGEA.dbo.CHART_ORPhoneType T2 ON T.ORPhoneType= T2.ORPhoneType AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
					INNER JOIN SUTTER_DATA.dbo.HC_Org_Relat_v T3 ON T.ORPhoneORImpID=T3.ORImpID
					WHERE T3.ORLink='' OR ORLink IS null 
	                                                              
					ORDER BY T.RE_DB_OwnerShort, T.ORPhoneORImpID, T.ORPhoneType
       				--5114
		END;		 

		BEGIN--PHONES_2: ASSIGN SEQUENCE and BASE TABLES 
				--PHONES where SEQUENCE >1 WILL BE ADDED in the same table WITH THE ADDITIONAL PHONE NUMBERS, as they'll be consolidated with in the same txt field. 
				--DO NOT INCLUDE ADDITIONAL PHONE NUMBER TYPES this time around. they'll be added later. 
			    
				SELECT 	
				T.RE_DB_OwnerShort, T.ORPhoneORImpID, T.ORPhoneType, T.ORPhoneNum, T.SF_Field_API
				,ROW_NUMBER() OVER ( PARTITION BY T.RE_DB_OwnerShort, T.ORPhoneORImpID, T.SF_Field_API
										ORDER BY T.RE_DB_OwnerShort, T.ORPhoneORImpID, T.ORPhoneType ) AS Seq 
				INTO SUTTER_MGEA.TBL.ORPhones_seq			--DROP TABLE SUTTER_MGEA.TBL.ORPhones_seq
				FROM SUTTER_MGEA.TBL.ORPhones T 
			
				WHERE (T.SF_Field_API!='Additional_Phone_Numbers__c')   -- do not include additional phones
				--4595
		END;
		
		BEGIN--PHONES_3: create base table for pivot tbl of PHONES where Seq =1
			
				SELECT * 
				INTO SUTTER_MGEA.TBL.ORPhones_seq_1		--DROP TABLE SUTTER_MGEA.TBL.ORPhones_seq_1
				FROM SUTTER_MGEA.TBL.ORPhones_seq
				WHERE Seq='1'
				--4526 
		END;
		
		BEGIN--PHONES_4: create base table for pivot tbl of ADDITIONAL phones;
			 --includes additional numbers where Seq>1  AND those mapped to additional_phone_numbers.
				
					SELECT  *
					INTO SUTTER_MGEA.TBL.ORPhones_seq_2    --DROP TABLE SUTTER_MGEA.TBL.ORPhones_seq_2 --all other additional phones.
					FROM    SUTTER_MGEA.TBL.ORPhones_seq
					WHERE   Seq != '1'						--includes numbers where seq>1
					UNION
					SELECT  * , '1' Seq
					FROM    SUTTER_MGEA.TBL.ORPhones
					WHERE   (SF_Field_API = 'Additional_Phone_Numbers__c')  --includes additional numbers. 
		END;		
			
		BEGIN--PHONES_5: --CREATE final pivot phone tables --PHONE TYPES from SF_Field_API
			  
			  
				EXEC HC_PivotWizard_p   'ORPhoneORImpID, RE_DB_OwnerShort',				--fields to include as unique identifier/normally it is just an ID field.
										'SF_Field_API',									--column that stores all new phone types
										'ORPhoneNum',									--phone numbers
										'SUTTER_MGEA.TBL.ORPhones_seq_1_final',			--INTO..     -- DROP TABLE SUTTER_MGEA.TBL.ORPhones_seq_1_final
										'SUTTER_MGEA.TBL.ORPhones_seq_1',				--FROM..
										'ORPhoneNum is not null'						--WHERE..
				--3133
				
				BEGIN --test pivot results ACCOUNT
						SELECT ORPhoneORImpID, COUNT(*) c 
						FROM SUTTER_MGEA.TBL.ORPhones_seq_1
						GROUP BY ORPhoneORImpID
						HAVING COUNT(*)>1
						ORDER BY c DESC 
						
			 
						
						SELECT * FROM SUTTER_MGEA.TBL.ORPhones 
						WHERE ORPhoneORImpID='03947-518-0000094923'
						SELECT * FROM SUTTER_MGEA.TBL.ORPhones_seq_1		
						WHERE ORPhoneORImpID='03947-518-0000094923'
						SELECT * FROM SUTTER_MGEA.TBL.ORPhones_seq_1_final	
						WHERE ORPhoneORImpID='03947-518-0000094923'
						SELECT * FROM SUTTER_MGEA.TBL.ORPhones_seq_2		
						WHERE ORPhoneORImpID='03947-518-0000094923'
						SELECT * FROM SUTTER_MGEA.TBL.ORPhones_seq_2_final	
						WHERE ORPhoneORImpID='03947-518-0000094923'
				END;
		END;		 
		 	
		BEGIN--PHONE_6: ADDITIONAL PHONE NUMBERS field
					 
					SELECT DISTINCT T.RE_DB_OwnerShort, T.ORPhoneORImpID,  
						STUFF(( SELECT ' '+T1.ORPhoneType +': '+ T1.ORPhoneNum +'; ' + CHAR(10)
								FROM SUTTER_MGEA.TBL.ORPhones_seq_2 T1
									WHERE T1.ORPhoneORImpID=T.ORPhoneORImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
									ORDER BY T1.RE_DB_OwnerShort, T1.ORPhoneORImpID
								FOR
									XML PATH('')
									  ), 1, 1, '') AS Additional_Phone_Numbers__c
					INTO SUTTER_MGEA.TBL.ORPhones_seq_2_final   --DROP TABLE SUTTER_MGEA.TBL.ORPhones_seq_2_final   
					FROM SUTTER_MGEA.TBL.ORPhones_seq_2 T
					WHERE T.ORPhoneNum IS NOT null 
					ORDER BY T.RE_DB_OwnerShort, T.ORPhoneORImpID
				
						
				BEGIN--test for duplicates
							SELECT  ORPhoneORImpID,
									RE_DB_OwnerShort ,
									COUNT(*) c
							FROM    SUTTER_MGEA.TBL.ORPhones_seq_1_final
							GROUP BY ORPhoneORImpID,
										RE_DB_OwnerShort
							HAVING  COUNT(*) > 1
							ORDER BY ORPhoneORImpID
							
							SELECT  ORPhoneORImpID ,
									RE_DB_OwnerShort ,
									COUNT(*) c
							FROM    SUTTER_MGEA.TBL.ORPhones_seq_2_final
							GROUP BY ORPhoneORImpID,
										RE_DB_OwnerShort
							HAVING  COUNT(*) > 1
							ORDER BY ORPhoneORImpID
				END;
		END;			

END;	

BEGIN--EDUCATION RELATIONSHIPS TABLES 			 
		BEGIN--BASE TABLE EDUCATION RELATIONSHIPS 
			
				-- DROP TABLE SUTTER_MGEA.TBL.ESRSchoolName	 
				-- DROP TABLE SUTTER_MGEA.TBL.Education_Major 
				-- DROP TABLE SUTTER_MGEA.TBL.Education_Minor 
				
				SELECT	T.RE_DB_OwnerShort
						,T.ESRImpID
						,T.ESRSchoolName AS Name
						,CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ISNULL(UPPER(T.ESRSchoolName),' '))), 3, 20) AS NVARCHAR(20)) 
						AS Unique_MD5	
				INTO SUTTER_MGEA.TBL.ESRSchoolName	 
				FROM SUTTER_DATA.dbo.HC_Educ_Relat_v T
				ORDER BY T.ESRSchoolName
		END; 


		BEGIN--UPDATE DATES TO COMPLY WITH TRANSLATION RULES
				SELECT ESRDateGrad, ESRDateLeft
				FROM SUTTER_DATA.dbo.HC_Educ_Relat_v 
				GROUP BY ESRDateGrad, ESRDateLeft
				
				UPDATE SUTTER_DATA.dbo.HC_Educ_Relat_v  SET ESRDateGrad=ESRDateLeft WHERE ESRDateGrad IS NULL 
		END;

		BEGIN--EDUCATION RELATIONSHIP MAJOR TO MULTIPICKLIST

						SELECT DISTINCT T.RE_DB_OwnerShort, T.ESRMajESRImpID,  
							STUFF(( SELECT '; ' + T1.ESRMajMajor 
									FROM SUTTER_DATA.dbo.HC_Educ_Relat_Major_v T1
										WHERE T1.ESRMajESRImpID=T.ESRMajESRImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
										GROUP BY T1.ESRMajMajor
									FOR
										XML PATH('')
										  ), 1, 1, '') AS Education_Major__c
						INTO SUTTER_MGEA.TBL.Education_Major    
						FROM SUTTER_DATA.dbo.HC_Educ_Relat_Major_v T
						WHERE T.ESRMajMajor IS NOT NULL
						ORDER BY T.RE_DB_OwnerShort, T.ESRMajESRImpID
		END;


		BEGIN--EDUCATION RELATIONSHIP MINOR TO MULTIPICKLIST

						SELECT DISTINCT T.RE_DB_OwnerShort, T.ESRMinESRImpID,  
							STUFF(( SELECT '; ' + T1.ESRMinMinor 
									FROM SUTTER_DATA.dbo.HC_Educ_Relat_Minor_v T1
										WHERE T1.ESRMinESRImpID=T.ESRMinESRImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
										GROUP BY T1.ESRMinMinor
									FOR
										XML PATH('')
										  ), 1, 1, '') AS Education_Minor__c
						INTO SUTTER_MGEA.TBL.Education_Minor     
						FROM SUTTER_DATA.dbo.HC_Educ_Relat_Minor_v T
						WHERE T.ESRMinMinor IS NOT NULL
						ORDER BY T.RE_DB_OwnerShort, T.ESRMinESRImpID
		END;
END;

BEGIN--CONSTITUENT ATTRIBUTES TO CONTACT 
		-- DROP TABLE SUTTER_MGEA.TBL.Attribute_contact
		-- DROP TABLE SUTTER_MGEA.TBL.Attributes_to_Contact
		BEGIN 	
				--BASE TABLE CONS ATTR TO CONTACT  -- remove duplicates if present
				SELECT DISTINCT T.RE_DB_OwnerShort, T.ImportID, T2.SF_Object_2, T2.SF_Field_2, 
				CASE WHEN (T2.SF_Field_2='Agefinder_Birthdate__c' AND LEN(T.CAttrDesc)='6') THEN right(T.CAttrDesc,2)+'/'+left(T.CAttrDesc,4) 
					 WHEN (T2.SF_Field_2='Agefinder_Birthdate__c' AND LEN(T.CAttrDesc)='8' AND T.CAttrDesc NOT LIKE '%/%') 
						  THEN SUBSTRING(T.CAttrDesc, 5, 2)+'/'+right(T.CAttrDesc,2)+'/'+left(T.CAttrDesc,4) 
					 ELSE T.CAttrDesc END AS SF_FieldValue ,T.CAttrDesc refCAttrDesc ,LEN(T.CAttrDesc) refFieldLenght
				
				INTO SUTTER_MGEA.TBL.Attribute_contact
				FROM SUTTER_DATA.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				LEFT JOIN SUTTER_MGEA.dbo.CHART_Attributes T2 ON T.CAttrCat=T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T1.KeyInd='I' AND T2.SF_Object_2='CONTACT' AND T.CAttrDesc IS NOT null
				 
			UNION
			--ADDR ATTR TO CONTACT
				SELECT DISTINCT T.RE_DB_OwnerShort, T1.ImportID,  T2.SF_Object_2, T2.SF_Field_2, t.CADAttrCom AS SF_FieldValue, T.CADAttrDesc refCAttrDesc, 
				LEN(T.CADAttrCom) refFieldLength
				FROM SUTTER_DATA.dbo.HC_Cons_Address_Attr_v T
				INNER JOIN SUTTER_DATA.DBO.HC_Cons_Address_v T1 ON T.CADAttrAddrImpID=T1.AddrImpID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				LEFT JOIN SUTTER_MGEA.dbo.CHART_Attributes T2 ON T.CADAttrCat =T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.SF_Object_2='CONTACT'  
	
		END; 
		
		SELECT sf_field_2 FROM SUTTER_MGEA.TBL.Attribute_contact GROUP BY sf_field_2
		
		BEGIN--ATTRIBUTE to CONTACT - MULTIPICKLIST

				SELECT DISTINCT T.RE_DB_OwnerShort, T.ImportID,  
					STUFF(( SELECT '; ' + T1.SF_FieldValue 
							FROM SUTTER_MGEA.TBL.Attribute_contact T1
								WHERE T1.SF_Field_2='Agefinder_Birthdate__c' AND T1.ImportID=T.ImportID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
								GROUP BY T1.SF_FieldValue
							FOR
								XML PATH('')
								  ), 1, 1, '') AS Agefinder_Birthdate__c
								  
					,STUFF(( SELECT '; ' + T1.SF_FieldValue 
							FROM SUTTER_MGEA.TBL.Attribute_contact T1
								WHERE T1.SF_Field_2='CMS_Age__c' AND T1.ImportID=T.ImportID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
								GROUP BY T1.SF_FieldValue
							FOR
								XML PATH('')
								  ), 1, 1, '') AS CMS_Age__c
					,STUFF(( SELECT '; ' + T1.SF_FieldValue 
							FROM SUTTER_MGEA.TBL.Attribute_contact T1
								WHERE T1.SF_Field_2='PhoneFinder__c' AND T1.ImportID=T.ImportID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
								GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
							FOR
								XML PATH('')
								  ), 1, 1, '') AS PhoneFinder__c
								  
				INTO SUTTER_MGEA.TBL.Attributes_to_Contact 
				FROM SUTTER_MGEA.TBL.Attribute_contact T
				ORDER BY T.RE_DB_OwnerShort, T.ImportId
		END;
		
		
		BEGIN--CHECK DUPLICATES
			SELECT T.RE_DB_OwnerShort, T.ImportID, COUNT(*) C
			FROM SUTTER_MGEA.TBL.Attributes_to_Contact T
			GROUP BY T.RE_DB_OwnerShort, T.ImportID
			HAVING COUNT(*)>1

		END;
END;

BEGIN--GIFT ATTRIBUTES-CHART_Attribute

		BEGIN	
			-- DROP TABLE SUTTER_MGEA.TBL.Attribute_opportunity_gift 
			-- DROP TABLE SUTTER_MGEA.TBL.Attribute_opportunity_gift_1
			-- DROP TABLE SUTTER_MGEA.TBL.Attribute_opportunity_proposal
			-- DROP TABLE SUTTER_MGEA.TBL.Attribute_opportunity_proposal_1
			
			--GIFT ATTRIBUTE-BASE TABLE 
			SELECT DISTINCT T.RE_DB_OwnerShort, T.GFImpID,  T2.SF_Object_2, T2.SF_Field_2, 
			CASE WHEN T3.Name IS NOT NULL THEN T3.Name ELSE t.GFAttrDesc END AS SF_FieldValue, T.GFAttrDesc AS refCAttrDesc, 
			LEN(T.GFAttrDesc) refFieldLength 
			INTO SUTTER_MGEA.TBL.Attribute_opportunity_gift
			FROM SUTTER_DATA.dbo.HC_Gift_Attr_v T
			INNER JOIN SUTTER_MGEA.dbo.CHART_Attributes T2 ON T.GFAttrCat =T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_DATA.DBO.HC_Constituents_v T3 ON T.GFAttrDesc=T3.ConsID AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
			WHERE T2.SF_Object_2='OPPORTUNITY'  
			ORDER BY T.RE_DB_OwnerShort, T.GFImpID, T2.SF_Field_2
			
			--FINAL TABLE
			EXEC HC_PivotWizard_p   'RE_DB_OwnerShort, GFImpID',				--fields to include as unique identifier/normally it is just an ID field.
						'SF_Field_2',									--column that stores all new phone types
						'SF_FieldValue',								--phone numbers
						'SUTTER_MGEA.TBL.Attribute_opportunity_gift_1',	--INTO..     
						'SUTTER_MGEA.TBL.Attribute_opportunity_gift',	--FROM..
						'SF_FieldValue is not null'						--WHERE..
					--571
		END;
		
		BEGIN
		
			--GIFT PROPOSAL ATTRIBUTE-- BASE TABLE
			SELECT DISTINCT T.RE_DB_OwnerShort, T.PRAttrPRImpID,  T2.SF_Object_2, T2.SF_Field_2, T.PRAttrDesc AS SF_FieldValue, T.PRAttrDesc AS refCAttrDesc, 
			LEN(T.PRAttrDesc) refFieldLength 
			INTO SUTTER_MGEA.TBL.Attribute_opportunity_proposal
			FROM SUTTER_DATA.dbo.HC_Proposal_Attr_v T
			INNER JOIN SUTTER_MGEA.dbo.CHART_Attributes T2 ON T.PRAttrCat =T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.SF_Object_2='OPPORTUNITY'  
			ORDER BY T.RE_DB_OwnerShort, T.PRAttrPRImpID, T2.SF_Field_2
			
			SELECT DISTINCT T.RE_DB_OwnerShort, T.PRAttrPRImpID,  
				STUFF(( SELECT '; ' + T1.SF_FieldValue 
						FROM SUTTER_MGEA.TBL.Attribute_opportunity_proposal T1
							WHERE T1.PRAttrPRImpID=T.PRAttrPRImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS RE_Team_Member__c
								  
			INTO SUTTER_MGEA.TBL.Attribute_opportunity_proposal_1  
			FROM SUTTER_MGEA.TBL.Attribute_opportunity_proposal T
			ORDER BY T.RE_DB_OwnerShort, T.PRAttrPRImpID
		END;
END; 

BEGIN--GIFT ATTRIBUTE-CHART_GiftAttribute
 
		-- DROP TABLE SUTTER_MGEA.TBL.Gift_Attribute	
		-- DROP TABLE SUTTER_MGEA.TBL.Gift_Attribute_1 
		
		
	BEGIN--BASE TABLE 	
		SELECT DISTINCT
		T1.RE_DB_OwnerShort, T1.GFImpID, T1.GFAttrCat, T1.GFAttrDesc, T1.GFAttrDate, T1.GFAttrCom,
		T2.OPPORTUNITY_Attribute__c
		INTO SUTTER_MGEA.TBL.Gift_Attribute		
		FROM SUTTER_DATA.dbo.HC_Gift_Attr_v T1
		INNER JOIN SUTTER_MGEA.DBO.CHART_GiftAttribute T2 ON T1.GFAttrCat=T2.GFAttrCat AND T1.GFAttrDesc=T2.GFAttrDesc AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
		WHERE T2.[Convert]='Yes'
	END;
	BEGIN--FINAL TABLE
		SELECT DISTINCT T.RE_DB_OwnerShort, T.GFImpID,  
				STUFF(( SELECT '; ' + T1.OPPORTUNITY_Attribute__c 
						FROM SUTTER_MGEA.TBL.Gift_Attribute T1
							WHERE T1.GFImpID=T.GFImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.OPPORTUNITY_Attribute__c ORDER BY T1.OPPORTUNITY_Attribute__c DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS OPPORTUNITY_Attribute__c
				
			INTO SUTTER_MGEA.TBL.Gift_Attribute_1  
			FROM SUTTER_MGEA.TBL.Gift_Attribute T
			WHERE T.OPPORTUNITY_Attribute__c IS NOT NULL
			ORDER BY T.RE_DB_OwnerShort, T.GFImpID	 
	END;
		
		
END; 
 
BEGIN--ACTION ATTRIBUTES TO TASK.

			-- DROP TABLE SUTTER_MGEA.TBL.Attribute_task
			-- DROP TABLE SUTTER_MGEA.TBL.Attribute_task_1
			
			
			--CREATE BASE TABLE
			SELECT DISTINCT T.RE_DB_OwnerShort, T.ACImpID,  T2.SF_Object_2, T2.SF_Field_2, 
			CASE WHEN T.ACAttrCat='Thank-you Call' THEN 'TRUE' ELSE T.ACAttrDesc END AS SF_FieldValue, T.ACAttrDesc AS refCAttrDesc, t.ACAttrCom,
			LEN(T.ACAttrDesc) refFieldLength 
			INTO SUTTER_MGEA.TBL.Attribute_task
			FROM SUTTER_DATA.dbo.HC_Cons_Action_Attr_v T
			INNER JOIN SUTTER_MGEA.dbo.CHART_Attributes T2 ON T.ACAttrCat =T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.SF_Object_2='TASK'  
			ORDER BY T.RE_DB_OwnerShort, T.ACImpID, T2.SF_Field_2

			--CREATE FINAL TABLE. 
			SELECT DISTINCT T.RE_DB_OwnerShort, T.ACImpID,  
				STUFF(( SELECT '; ' + T1.SF_FieldValue 
						FROM SUTTER_MGEA.TBL.Attribute_task T1
							WHERE T1.SF_Field_2='RE_Team_Member__c' AND T1.ACImpID=T.ACImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS RE_Team_Member__c
				,STUFF(( SELECT '; ' + T1.SF_FieldValue 
						FROM SUTTER_MGEA.TBL.Attribute_task T1
							WHERE T1.SF_Field_2='Thank_you_call__c' AND T1.ACImpID=T.ACImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS Thank_you_call__c
			INTO SUTTER_MGEA.TBL.Attribute_task_1  
			FROM SUTTER_MGEA.TBL.Attribute_task T
			WHERE T.SF_FieldValue IS NOT NULL
			ORDER BY T.RE_DB_OwnerShort, T.ACImpID
			
		BEGIN 
			UPDATE SUTTER_MGEA.TBL.Attribute_task_1  SET RE_Team_Member__c=LTRIM(RE_Team_Member__c) 
			UPDATE SUTTER_MGEA.TBL.Attribute_task_1  SET Thank_you_call__c=LTRIM(Thank_you_call__c) 
		END

END;

BEGIN--CONSTITUENT ACTION SOLICITOR TO MULTI-PICKLIST

		-- DROP TABLE SUTTER_MGEA.TBL.Cons_Action_Solicitor
		-- DROP TABLE SUTTER_MGEA.TBL.Cons_Action_Solicitor_1


		BEGIN--BASE TABLE	
			SELECT DISTINCT T.RE_DB_OwnerShort, T.ACImpID, T1.ImportID, T1.Name
			INTO SUTTER_MGEA.TBL.Cons_Action_Solicitor
			FROM SUTTER_DATA.dbo.HC_Cons_Action_Solicitor_v T
			INNER JOIN SUTTER_DATA.dbo.HC_Constituents_v T1 ON T.ACSolImpID=T1.ImportID
			ORDER BY T.RE_DB_OwnerShort, T.ACImpID, T1.ImportID
		END;
		
		BEGIN--MULTI-SELECT PICKLIST
			SELECT DISTINCT T.RE_DB_OwnerShort, T.ACImpID
				,STUFF(( SELECT '; ' + T1.Name 
						FROM SUTTER_MGEA.TBL.Cons_Action_Solicitor T1
							WHERE T1.ACImpID=T.ACImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.Name ORDER BY T1.Name DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS RE_Action_Solicitor__c
			INTO SUTTER_MGEA.TBL.Cons_Action_Solicitor_1 -- select * from SUTTER_MGEA.TBL.Cons_Action_Solicitor_1
			FROM SUTTER_MGEA.TBL.Cons_Action_Solicitor T
			ORDER BY T.RE_DB_OwnerShort, T.ACImpID
		END;
		
		BEGIN 
			UPDATE SUTTER_MGEA.TBL.Cons_Action_Solicitor_1  SET RE_Action_Solicitor__c=LTRIM(RE_Action_Solicitor__c) 
		END
		
END;

--ASSIGNED SOLICITOR TO OWNER ID (Assigned TO) 
BEGIN

		SELECT	ROW_NUMBER() OVER ( PARTITION BY T1.RE_DB_OwnerShort, T1.ACImpID
									ORDER BY T1.RE_DB_OwnerShort, T1.ACImpID, T1.ACSolImpID) AS Seq,
				T1.RE_DB_OwnerShort, T1.ACImpID, 
				T1.ACSolImpID, 
				T2.SF_User_Email,
				T3.ID AS OwnerId
		INTO SUTTER_MGEA.TBL.AssginedSolicitor_OwnerId
		FROM SUTTER_DATA.dbo.HC_Cons_Action_Solicitor_v T1
		INNER JOIN SUTTER_MGEA.[dbo].[CHART_ActionSolicitor] T2 ON T1.ACSolImpID=T2.ACSolImpID
		LEFT JOIN SUTTER_MGEA.XTR.USERS T3 ON T2.SF_User_Email=T3.EMAIL
 
 END;


BEGIN--CONSTITUENT ACTION NOTES compiled
		
		BEGIN 
			DROP TABLE SUTTER_MGEA.TBL.Cons_Action_Notes  
			DROP TABLE SUTTER_MGEA.TBL.Cons_Action_Notes_1
		END
		

		BEGIN--BASE TABLE
			SELECT T.RE_DB_OwnerShort, T.CALink, 
			[Description] = 'Category: '+ T1.ACCat +'; ' + CHAR(10) +
							+'Date: ' + CAST(CONVERT(DATE, T.CANoteDate, 101) AS NVARCHAR(20)) +  CHAR(10) +
							+'Type: ' + T.CANoteType +'; ' + CHAR(10) +
							+ CASE WHEN T.CANoteTitle IS NOT NULL THEN COALESCE('Author: ' + LTRIM(RTRIM(T.CANoteAuthor)), '') ELSE '' END +  CHAR(10) +  --+'Author: ' + T.CANoteAuthor +'; ' + CHAR(10) +
							+ CASE WHEN T.CANoteTitle IS NOT NULL THEN COALESCE('Title: ' + LTRIM(RTRIM(T.CANoteTitle)), '') ELSE '' END +  CHAR(10) +
							+ CASE WHEN T.CANoteDesc IS NOT NULL THEN COALESCE('Desc: ' + LTRIM(RTRIM(T.CANoteDesc)), '') ELSE '' END +  CHAR(10) +
							+ CASE WHEN T.CANoteNotes IS NOT NULL THEN COALESCE('Notes: ' + LTRIM(RTRIM(T.CANoteNotes)), '') ELSE '' END
			INTO SUTTER_MGEA.TBL.Cons_Action_Notes  
			FROM SUTTER_DATA.DBO.HC_Cons_Action_Notes_v T
			INNER JOIN SUTTER_DATA.dbo.HC_Cons_Action_v T1 ON T.CALink=T1.ACImpID
			ORDER BY T.CALink, T.CANoteDate DESC
		END;	
		
		BEGIN--MULTI-SELECT PICKLIST (CONCATENATE MULTIPLE NOTES. 
			SELECT DISTINCT T.RE_DB_OwnerShort, T.CALink
				,STUFF(( SELECT '. ' + CHAR(10)+ T1.[Description] 
						FROM SUTTER_MGEA.TBL.Cons_Action_Notes T1
							WHERE T1.CALink=T.CALink AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.[Description] --ORDER BY T1.[Description] DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS [Description]
			INTO SUTTER_MGEA.TBL.Cons_Action_Notes_1		-- drop table SUTTER_MGEA.TBL.Cons_Action_Notes_1
			FROM SUTTER_MGEA.TBL.Cons_Action_Notes T
			ORDER BY T.RE_DB_OwnerShort, T.CALink
		END;


			 /* TEST 
			 SELECT CALink, COUNT (*) c  FROM SUTTER_MGEA.TBL.Cons_Action_Notes GROUP BY CALink HAVING COUNT(*)>1 ORDER BY C DESC
			 SELECT CALink, COUNT (*) c  FROM SUTTER_MGEA.TBL.Cons_Action_Notes_1 GROUP BY CALink HAVING COUNT(*)>1 ORDER BY C DESC
			 SELECT * FROM SUTTER_MGEA.TBL.CONS_ACTION_NOTES WHERE CALINK ='03947-504-0000015601'
			 SELECT * FROM SUTTER_MGEA.TBL.CONS_ACTION_NOTES_1 WHERE CALINK ='03947-504-0000015601' or CALINK='03947-504-0000045452'
		
			 */  
END;

BEGIN	--CONSTITUENT ACTION NOTE. MOST RECENT ACTION NOTE TO ADD "DESCRIPTION" INTO TASK SUBJECT LINE.
			--BASE TABLE
			SELECT T.RE_DB_OwnerShort, T.CALink, T.CANoteDate, T.CANoteDesc,
			ROW_NUMBER() OVER ( PARTITION BY T.RE_DB_OwnerShort, T.CALink
											ORDER BY T.RE_DB_OwnerShort, T.CALink, CAST(T.CANoteDate AS DATE) ) AS Seq 
			INTO SUTTER_MGEA.TBL.Cons_Action_Notes_desc 
			FROM SUTTER_DATA.DBO.HC_Cons_Action_Notes_v T
			WHERE T.CANoteDesc IS NOT NULL AND T.CANoteDesc!=''
			ORDER BY T.RE_DB_OwnerShort, T.CALink, CAST(T.CANoteDate AS DATE) DESC
		
				--CHECK FOR SEQ NUMBERS (MULTIPLE NOTES/ACTION)
				SELECT * FROM SUTTER_MGEA.TBL.Cons_Action_Notes_desc WHERE SEQ!='1'
				--DELETE SEQ NOT 1
				DELETE SUTTER_MGEA.TBL.Cons_Action_Notes_desc WHERE SEQ!='1'
				--CHECK DUPLICATE
				SELECT T.RE_DB_OwnerShort, T.CALink, COUNT(*) c
				FROM SUTTER_MGEA.TBL.Cons_Action_Notes_desc  T
				GROUP BY T.RE_DB_OwnerShort, T.CALink 
				HAVING COUNT(*)>1


END;
 
BEGIN--GIFT LEGACY FINANCIAL CODES 

		-- DROP TABLE SUTTER_MGEA.TBL.GiftLegacyFinancialCodes		

		--BASE TABLE
		SELECT T.RE_DB_OwnerShort, T.GSplitImpID, T.GSplitFund, T.GSplitCamp, T.GSplitAppeal, T.GSplitPkg,
		[RE_Legacy_Financial_Code__c]=	CASE WHEN T.GSplitFund IS NOT NULL THEN COALESCE('Fund: ' + LTRIM(RTRIM(T.GSplitFund)), '') ELSE 'N/A' END + CHAR(10) +
										CASE WHEN T.GSplitCamp IS NOT NULL THEN COALESCE('; Campaign: ' + LTRIM(RTRIM(T.GSplitCamp)), '') ELSE 'N/A' END + CHAR(10) +
										CASE WHEN T.GSplitAppeal IS NOT NULL THEN COALESCE('; Appeal: ' + LTRIM(RTRIM(T.GSplitAppeal)), '') ELSE 'N/A' END + CHAR(10) +
										CASE WHEN T.GSplitPkg IS NOT NULL THEN COALESCE('; Package: ' + LTRIM(RTRIM(T.GSplitPkg)), '') ELSE 'N/A' END + CHAR(10)
		INTO SUTTER_MGEA.TBL.GiftLegacyFinancialCodes								
		FROM SUTTER_DATA.dbo.HC_Gift_SplitGift_v T
		 
		 
		--UPDATE - REPLACE "NULL" WITH "N/A"
		SELECT * FROM SUTTER_MGEA.TBL.GiftLegacyFinancialCodes WHERE [RE_Legacy_Financial_Code__c] LIKE '%NULL%'
		
		UPDATE SUTTER_MGEA.TBL.GiftLegacyFinancialCodes 
			SET [RE_Legacy_Financial_Code__c] =REPLACE([RE_Legacy_Financial_Code__c] ,'NULL','N/A') 
			WHERE [RE_Legacy_Financial_Code__c] LIKE '%NULL%'
 
		SELECT * FROM SUTTER_MGEA.TBL.GiftLegacyFinancialCodes WHERE [RE_Legacy_Financial_Code__c] LIKE '%N/A%'
		SELECT [RE_Legacy_Financial_Code__c], LEN([RE_Legacy_Financial_Code__c] ) l FROM SUTTER_MGEA.TBL.GiftLegacyFinancialCodes  ORDER BY l desc


END;

BEGIN--GIFT TOTAL PAYMENT AMT (current giving amt) 
	--DROP TABLE SUTTER_MGEA.TBL.TotalPaymentAmt
	
	SELECT RE_DB_OwnerShort, GFType, GFImpId, SUM(GFPaymentAmount) TotalPaymentAmt, COUNT(*) TotalNumberPayments
	INTO SUTTER_MGEA.TBL.TotalPaymentAmt	
	FROM SUTTER_DATA.dbo.HC_Gift_Link_v
	GROUP BY RE_DB_OwnerShort, GFType, GFImpId
	ORDER BY RE_DB_OwnerShort, GFType, GFImpId
	
	
END;

BEGIN--GIFT TRIBUTE

		--DELETE BASE TABLE
		DROP TABLE SUTTER_MGEA.TBL.GiftTribute
		DROP TABLE SUTTER_MGEA.TBL.GiftTribute_1
		DROP TABLE SUTTER_MGEA.TBL.GiftTribute_2
		DROP TABLE SUTTER_MGEA.TBL.GiftTribute_3
		DROP TABLE SUTTER_MGEA.TBL.GiftTribute_note
		
		BEGIN--CREATE BASE TABLE
			SELECT	ROW_NUMBER() OVER ( PARTITION BY T1.RE_DB_OwnerShort, T1.GFTLink
											ORDER BY T1.RE_DB_OwnerShort, T1.GFTLink, T1.GFTribDesc DESC) AS Seq 
					,T1.RE_DB_OwnerShort
					,T1.GFTLink
					,CASE WHEN T1.GFTribType IS NOT NULL THEN T1.GFTribType ELSE T2.TRType END AS rC_Giving__Tribute_Type__c
					,CASE WHEN T1.GFTribDesc IS NOT NULL THEN T1.GFTribDesc WHEN T2.TRDesc IS NOT NULL THEN T2.TRDesc WHEN T3.ImportID IS NOT NULL THEN T3.FirstName +' '+ T3.LastName END AS rC_Giving__Tribute_Description__c
					,'TRUE' AS rC_Giving__Is_Tribute__c
					,CASE WHEN LEN(T2.TRDateFrom)=8 THEN SUBSTRING(T2.TRDateFrom, 5,2) +'/'+ SUBSTRING(T2.TRDateFrom, 7,2) +'/'+LEFT(T2.TRDateFrom , 4) ELSE NULL END AS rC_Giving__Tribute_Effective_Date__c
					,CAST(T2.TRNotes AS NVARCHAR(4000)) AS rC_Giving__Tribute_Comments__c

					--ref
					,T2.ImportID, T3.IsHonMem, T3.FirstName, T3.LastName, T1.TribImpID, T1.TribGiftImpID
					
		--	INTO SUTTER_MGEA.TBL.GiftTribute
			FROM SUTTER_DATA.dbo.HC_Gift_Tribute_v T1
			LEFT JOIN SUTTER_DATA.dbo.HC_Cons_Tribute_v T2 ON T1.TribImpID=T2.TRImpID and T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_DATA.dbo.HC_Constituents_v T3 ON T2.ImportID=T3.ImportID AND T2.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
		 END;
					--CHECK MULTIPLE TRIBUTES / GIFT
						SELECT * 
						FROM SUTTER_MGEA.TBL.GiftTribute
						WHERE GFTLink IN (SELECT GFTLink FROM SUTTER_MGEA.TBL.GiftTribute GROUP BY GFTLink HAVING COUNT(*)>1)
						ORDER BY GFTLink, Seq
						
		BEGIN--CREATE TABLE OF UNIQUE TRIBUTES. 
					SELECT * 
					INTO SUTTER_MGEA.TBL.GiftTribute_1
					FROM SUTTER_MGEA.TBL.GiftTribute
					WHERE SEQ=1
					ORDER BY  RE_DB_OwnerShort, GFTLink
		END;	
					--CHECK MULTIPLE TRIBUTES / GIFT
					SELECT * 
					FROM SUTTER_MGEA.TBL.GiftTribute_1
					WHERE GFTLink IN (SELECT GFTLink FROM SUTTER_MGEA.TBL.GiftTribute_1 GROUP BY GFTLink HAVING COUNT(*)>1)
					ORDER BY GFTLink, Seq
					 
		BEGIN--CREATE TABLE OF NON UNIQUE TRIBUTES FOR FOLLOW UP.
				SELECT * 
				INTO SUTTER_MGEA.TBL.GiftTribute_2
				FROM SUTTER_MGEA.TBL.GiftTribute
				WHERE SEQ!=1
				ORDER BY  RE_DB_OwnerShort, GFTLink
		END;		
				
		BEGIN--CONCATENATE MULTIPLE FIELDS
				SELECT  DISTINCT 
						T1.RE_DB_OwnerShort
						,T1.GFTLink
						,TributeInfo=CASE WHEN T1.rC_Giving__Tribute_Type__c IS NOT NULL 
						    THEN COALESCE(' Type: ' + LTRIM(RTRIM(T1.rC_Giving__Tribute_Type__c)), '') 
							ELSE ' Type: N/A' END + CHAR(10) +
						CASE WHEN T1.rC_Giving__Tribute_Effective_Date__c IS NOT NULL 
							THEN COALESCE(' Date: ' + LTRIM(RTRIM(T1.rC_Giving__Tribute_Effective_Date__c)), '') 
							ELSE ' Date: N/A' END + CHAR(10) +
						CASE WHEN T1.rC_Giving__Tribute_Description__c IS NOT NULL 
							THEN COALESCE(' Description: ' + LTRIM(RTRIM(T1.rC_Giving__Tribute_Description__c)), '') 
							ELSE ' Description: N/A' END + CHAR(10) + 
						CASE WHEN T1.rC_Giving__Tribute_Comments__c IS NOT NULL 
							THEN COALESCE('; Notes: ' + LTRIM(RTRIM(T1.rC_Giving__Tribute_Comments__c)), '') 
							ELSE '; Notes: N/A' END 
				INTO SUTTER_MGEA.TBL.GiftTribute_3    
				FROM SUTTER_MGEA.TBL.GiftTribute_2 T1
				ORDER BY T1.RE_DB_OwnerShort, T1.GFTLink 
		END;
		
		BEGIN--MULTI-SELECT PICKLIST (CONCATENATE MULTIPLE  GIFT TRIBUTE NOTES. 
			SELECT DISTINCT T.RE_DB_OwnerShort, T.GFTLink
				,STUFF(( SELECT '. ' + CHAR(10)+ T1.TributeInfo 
						FROM SUTTER_MGEA.TBL.GiftTribute_3 T1
							WHERE T1.GFTLink=T.GFTLink AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.TributeInfo --ORDER BY T1.[Description] DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS TributeInfo
			INTO SUTTER_MGEA.TBL.GiftTribute_note  
			FROM SUTTER_MGEA.TBL.GiftTribute_3 T
			ORDER BY T.RE_DB_OwnerShort, T.GFTLink
		END;

			SELECT * FROM SUTTER_MGEA.TBL.GiftTribute_note
			WHERE GFTLINK IN (SELECT GFTLINK FROM SUTTER_MGEA.TBL.GiftTribute_note GROUP BY GFTLINK HAVING COUNT(*)>1)
			
				
				
				
END;	

BEGIN--GIFT/PROPOSAL LINK 
		
		-- DROP TABLE SUTTER_MGEA.TBL.PROPOSAL_GIFT_LINK
		-- BASE TABLE- include only records where 1 gift is applied to 1 proposal. 
		
		SELECT T2.RE_DB_OwnerShort, T2.GSplitImpID, T2.GFImpID, T1.GFLink, T1.PRImpID, T1.Amount, T1.UpdatePRAmtFunded, T1.PRName
		INTO SUTTER_MGEA.TBL.PROPOSAL_GIFT_LINK				
		FROM SUTTER_DATA.dbo.HC_Gift_ProposalLink_v T1
		INNER JOIN SUTTER_DATA.DBO.HC_Gift_SplitGift_v T2 ON T1.GFLink=T2.GFImpID 
		WHERE GFLink IN (SELECT GFLink FROM SUTTER_DATA.dbo.HC_Gift_ProposalLink_v GROUP BY GFLink HAVING COUNT(*)=1)
		ORDER BY T2.RE_DB_OwnerShort, T2.GSplitImpID, T2.GFImpID, T1.PRImpID

END;


BEGIN --GIFT SPLITS (add RE_Split_Gift__c checkbox on opportunities created from gifts with splits)

		SELECT RE_DB_OwnerShort, GFImpID, COUNT(*) CountOFSplits, 'TRUE' AS RE_Split_Gift__c 
		INTO SUTTER_MGEA.TBL.GIFT_SPLIT
		FROM SUTTER_DATA.dbo.HC_Gift_SplitGift_v
		GROUP BY RE_DB_OwnerShort, GFImpID 
		HAVING COUNT(*)>1 
		
END;
  


BEGIN--GIFT BASE TABLE 
			
			-- DROP TABLE SUTTER_MGEA.TBL.GIFT
			
			SELECT DISTINCT 
			--constituent 
				T0.KeyInd, T0.Name,
				CASE WHEN T14.NoHH_ImportID IS NOT NULL THEN T14.RE_DB_OwnerShort+'-'+T14.HH_ImportID ELSE T0.RE_DB_OwnerShort+'-'+T0.ImportID  END AS [ACCOUNT:External_Id__c],
			--GSplitGift
				T.GSplitImpID, T.GFImpID GSplitGFImpID, T.GFSplitSequence, T.GSplitAmt, T.GSplitFund, T.GSplitCamp, T.GSplitAppeal, T.GSplitPkg, T.RE_DB_OwnerShort GSplitRE_DB_OwnerShort, T.RE_DB_Tbl GSplitRE_DB_Tbl,
			--Gift
				T1.BATCH_NUMBER, T1.GFDate, T1.GFType, T1.GFTAmt, T1.GFPledgeBalance, T1.GFStatus, T1.GFAck, T1.GFAckDate, T1.GFAnon, T1.GFConsDesc,
				CASE WHEN LEN(T1.GFCheckDate)=10 THEN T1.GFCheckDate END AS GFCheckDate,
				T1.GFCheckNum, T1.ImportID, T1.GFPayMeth, T1.GFRef, 
				T1.GFStkIss, T1.GFStkMedPrice, T1.GFStkNumUnits, T1.GFStkSymbol, T1.GFSubType, T1.GFInsEndDate, T1.GFInsFreq, T1.GFInsNumPay, T1.GFInsStartDate, T1.DateAdded
			--GAU
				,T2.rC_Giving__External_ID__c AS GAU_External_Id__c		--rC_Giving__GAU__c
			--Campaign
				,T3.PRIMARY_CAMPAIGN_SOURCE AS CAMPAIGN_External_Id__c	--CampaignId
			--GiftTpe
				,T4.Parent_Giving_RecordType, T4.rC_Giving__Activity_Type__c, T4.rC_Giving__Is_Giving__c, T4.rC_Giving__Is_Giving_Donation__c	
				,T4.rC_Giving__Is_Giving_Inkind__c, T4.rC_Giving__Is_Sustainer__c, T4.Child_Giving_RecordType
				,T4.rC_Giving__Is_Giving_Transaction__c, T4.Transaction_Source
			--GSubType
				,T5.OPPORTUNITY_Production_Stage__c AS Production_Stage__c
			--PaymentMethod
				,T6.rC_Giving__Payment_Type__c
				,T6.External_Id__c PAYMENT_METHOD_External_ID__c
				,T6.ACCOUNT_External_ID__c AS PAYMENT_METHOD_ACCOUNT_External_ID__c
			--attributes (chart-attributes)
				,T7.OPX_Corporate_Donation_Contact__c
				,T7.Donation_Page_Name__c
				,T7.OPX_Provider__c
				,T7.OPX_Transaction_ID__c	
			--gift attribute (chart_giftattribute)
				,T8.OPPORTUNITY_Attribute__c
			--legacy codes
				,T9.RE_Legacy_Financial_Code__c
			--frequency
				,T10.rC_Giving__Giving_Frequency__c
			--rC_Giving__Current_Giving_Amount__c
				,CASE WHEN T11.TotalPaymentAmt IS NOT NULL THEN T11.TotalPaymentAmt ELSE T.GSplitAmt END AS rC_Giving__Current_Giving_Amount__c
			--parent opp Proposal
				,T12.RE_DB_OwnerShort+'-PP-'+T12.PRImpID AS  PARENT_PROPOSAL_External_Id__c
			--GIFT TRIBUTE
				,T13.rC_Giving__Tribute_Type__c
				,T13.rC_Giving__Tribute_Description__c	
				,T13.rC_Giving__Is_Tribute__c
				,T13.rC_Giving__Tribute_Effective_Date__c
				,T13.rC_Giving__Tribute_Comments__c
				,T13.RE_DB_OwnerShort+'-'+T13.ImportID AS [rC_Giving__Tribute_Contact__r:External_ID__c]
			--MISC.
				,T15.RE_Split_Gift__c
			--ref
				,T1.ImportID AS refImportId
				,T1.GFImpID AS refGFImpID
				,T14.HH_ImportID 
				,T14.NoHH_ImportID
				
			INTO SUTTER_MGEA.TBL.GIFT
			FROM SUTTER_DATA.DBO.HC_Gift_SplitGift_v AS T
			INNER JOIN SUTTER_DATA.DBO.HC_Gift_v AS T1 ON T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort AND T.GFImpID=T1.GFImpID
			INNER JOIN SUTTER_DATA.dbo.HC_Constituents_v T0 ON T1.ImportID=T0.ImportID AND T1.RE_DB_OwnerShort=T0.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA.tbl.Contact_HofH AS T14 ON T0.ImportID=T14.NoHH_ImportID AND T0.RE_DB_OwnerShort=T14.RE_DB_OwnerShort
			
			LEFT JOIN SUTTER_MGEA.IMP.GAU AS T2 ON T.RE_DB_OwnerShort=T2.refREOwner AND T.GSplitFund=T2.refFundId 
			LEFT JOIN SUTTER_MGEA.TBL.CAMPAIGN AS T3 ON T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort 
														AND T.GSplitFund=T3.FundId
														AND T.GSplitCamp=T3.CampId 
														AND T.GSplitAppeal=T3.AppealId 
														AND T.GSplitPkg=T3.PackageId
			
			LEFT JOIN SUTTER_MGEA.dbo.CHART_GiftType AS T4 ON T1.GFType=T4.GFType 
			LEFT JOIN SUTTER_MGEA.dbo.CHART_GiftSubType AS T5 ON T1.RE_DB_OwnerShort=T5.RE_DB_OwnerShort AND T1.GFSubType=T5.GFSubType
			LEFT JOIN SUTTER_MGEA.TBL.PAYMENT_METHOD AS T6 ON T1.RE_DB_OwnerShort=T6.RE_DB_OwnerShort  AND T1.GFImpID=T6.GFImpID 
			LEFT JOIN SUTTER_MGEA.TBL.Attribute_opportunity_gift_1 AS T7 ON T1.RE_DB_OwnerShort=T7.RE_DB_OwnerShort AND T1.GFImpID=T7.GFImpID
			LEFT JOIN SUTTER_MGEA.TBL.Gift_Attribute_1 AS T8 ON T1.RE_DB_OwnerShort=T8.RE_DB_OwnerShort AND T1.GFImpID=T8.GFImpID 
			LEFT JOIN SUTTER_MGEA.TBL.GiftLegacyFinancialCodes AS T9 ON T.RE_DB_OwnerShort=T9.RE_DB_OwnerShort AND T.GSplitImpID=T9.GSplitImpID
			LEFT JOIN SUTTER_MGEA.DBO.CHART_Pledge_Frequency AS T10 ON T1.GFInsFreq = T10.GFInsFreq
			LEFT JOIN SUTTER_MGEA.TBL.TotalPaymentAmt AS  T11  ON T1.RE_DB_OwnerShort=T11.RE_DB_OwnerShort AND T1.GFImpID=T11.GFImpID
			LEFT JOIN SUTTER_MGEA.TBL.PROPOSAL_GIFT_LINK AS T12 ON T.GSplitImpID=T12.GSplitImpID AND T.RE_DB_OwnerShort=T12.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA.TBL.GiftTribute_1 AS T13 ON T1.GFImpID=T13.GFTLink AND T1.RE_DB_OwnerShort=T13.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA.TBL.GIFT_SPLIT AS T15 ON T.GFImpID=T15.GFImpID AND T.RE_DB_OwnerShort=T15.RE_DB_OwnerShort
			
			 --expected: 516,276 
			 --actuals:  516,276
			 
		  
			  
END;   

BEGIN
	 
			SELECT	T4.HH_ImportID
				    ,T4.NoHH_ImportID
					,CASE WHEN T4.NoHH_ImportID IS NOT NULL THEN T4.RE_DB_OwnerShort+'-'+T4.HH_ImportID ELSE T2.RE_DB_OwnerShort+'-'+T2.ImportID  END AS [ACCOUNT:External_Id__c]
					,T1.*
		--    INTO SUTTER_MGEA.TBL.GIFT_WRITEOFF    
		 	FROM SUTTER_DATA.dbo.HC_Gift_WriteOff_v T1
			INNER JOIN SUTTER_DATA.dbo.HC_Constituents_v T2 ON T1.ImportID=T2.ImportID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA.tbl.Contact_HofH AS T4 ON T2.ImportID=T4.NoHH_ImportID AND T2.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
END 




BEGIN--ADDITIONAL_ADD_SAL

	USE SUTTER_DATA
	GO
	
	--TBL ADDITIONAL ADD SAL combined in one row when 2 of Sames AddSal type are added. 

		--remove tables
		BEGIN	
					DROP TABLE SUTTER_MGEA.TBL.AddtlAddSal
					DROP TABLE SUTTER_MGEA.TBL.AddtlAddSal_1
					DROP TABLE SUTTER_MGEA.TBL.AddtlAddSal_final
		END;

		--create master table
		BEGIN 
					SELECT T1.AddSalImpID, T1.ImportID, T1.RE_DB_Tbl, T1.RE_DB_OwnerShort, 
					T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.ImportID +'-'+T2.SALUTATION_TYPE AS UniqueId, 
					T1.AddSalText, T2.SALUTATION_TYPE, T2.SF_FIELD, T1.AddSalType
					INTO SUTTER_MGEA.TBL.AddtlAddSal
					FROM SUTTER_DATA.dbo.HC_Cons_Addl_Addressee_v T1
					INNER JOIN SUTTER_MGEA.[dbo].[CHART_AddSalType] T2 ON T1.AddSalType=T2.AddSalType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
					WHERE T2.[Convert]='Yes'
					ORDER BY T1.RE_DB_OwnerShort, T1.ImportID, T2.SALUTATION_TYPE, SF_FIELD DESC  
		END; 

			--test
			BEGIN 
					SELECT * FROM SUTTER_MGEA.TBL.AddtlAddSal
				 
			END; 
		 
		 
		--append tables in prep for pivot
		BEGIN 
			SELECT  DISTINCT T1.UNIQUEID, T1.UNIQUEID AS SF_Value, 'UniqueImportID' SF_Field, T1.RE_DB_OwnerShort, T1.RE_DB_Tbl, T1.ImportID
			INTO SUTTER_MGEA.tbl.AddtlAddSal_1-- DROP TABLE RE_WMF.dbo.TBL_AddAddSal_append
			FROM SUTTER_MGEA.tbl.AddtlAddSal T1
			UNION ALL
			SELECT  DISTINCT T1.UNIQUEID, T1.AddSalText AS SF_Value, SF_Field, T1.RE_DB_OwnerShort, T1.RE_DB_Tbl, T1.ImportID
			FROM SUTTER_MGEA.tbl.AddtlAddSal T1
			UNION ALL
			SELECT  DISTINCT T1.UNIQUEID, T1.SALUTATION_TYPE AS SF_Value, 'rc_Bios_Salutation_Type__c' SF_Field, T1.RE_DB_OwnerShort, T1.RE_DB_Tbl, T1.ImportID
			FROM SUTTER_MGEA.tbl.AddtlAddSal T1
			UNION ALL
			SELECT DISTINCT T.UNIQUEID 
				,STUFF(( SELECT '; ' + T1.AddSalType 
						FROM SUTTER_MGEA.tbl.AddtlAddSal T1
							WHERE T1.UNIQUEID=T.UNIQUEID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.AddSalType 
							ORDER BY T1.AddSalType DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS SF_Value,
			'Description' SF_Field, T.RE_DB_OwnerShort, T.RE_DB_Tbl, T.ImportID
			FROM SUTTER_MGEA.tbl.AddtlAddSal T
		END;
			  
		--create final table
		BEGIN	
				EXEC SUTTER_DATA.dbo.HC_PivotWizard_p 'UNIQUEID, ImportId, RE_DB_OwnerShort',
													'SF_Field',
													'SF_Value',
													'SUTTER_MGEA.tbl.AddtlAddSal_final',   --DROP TABLE  SUTTER_MGEA.tbl.AddtlAddSal_final
													'SUTTER_MGEA.tbl.AddtlAddSal_1'
		END							
											
			--test	
			BEGIN 
				SELECT * FROM SUTTER_MGEA.tbl.AddtlAddSal WHERE ImportID='04164-079-0001027' ORDER BY UniqueId
				SELECT * FROM SUTTER_MGEA.tbl.AddtlAddSal_1 WHERE ImportID='04164-079-0001027' ORDER BY UniqueId
				SELECT * FROM SUTTER_MGEA.tbl.AddtlAddSal_final  WHERE ImportID='04164-079-0001027' ORDER BY UniqueId
			END; 
END;


