 
USE SUTTER_1P_DATA
GO


/****************************************************************************************/	
--ADD RE_DB_Id to table to use for linking with combo external Id. 
-- this is only required when XTR files are needed for Data Loader Inserts.
		BEGIN
				--CONSTITUENT 
				ALTER TABLE SUTTER_1P_DATA.dbo.HC_Constituents_v
				ADD RE_DB_Id VARCHAR(30)
				
					UPDATE SUTTER_1P_DATA.dbo.HC_Constituents_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID

				ALTER TABLE SUTTER_1P_DATA.dbo.HC_Cons_Address_v
				ADD RE_DB_Id VARCHAR(30)
				GO
					UPDATE SUTTER_1P_DATA.dbo.HC_Cons_Address_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
					GO
	
				--INDIVIDUAL RELATIONSHIP
				ALTER TABLE SUTTER_1P_DATA.DBO.HC_Ind_Relat_v
				ADD RE_DB_Id VARCHAR(30)
				GO
					UPDATE SUTTER_1P_DATA.DBO.HC_Ind_Relat_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+IRImpID
				 	GO
				  
				--ORGANIZATION RELATIONSHIP
				ALTER TABLE SUTTER_1P_DATA.DBO.HC_Org_Relat_v
				ADD RE_DB_Id VARCHAR(30)
				GO
					UPDATE SUTTER_1P_DATA.DBO.HC_Org_Relat_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ORImpID
				 	GO

					--SELECT ImportID, RE_DB_OwnerShort, RE_DB_Id FROM SUTTER_1P_DATA.dbo.HC_Constituents_v
				--GIFT
				ALTER TABLE SUTTER_1P_DATA.dbo.HC_Gift_v
				ADD RE_DB_Id VARCHAR(30)
				GO
					UPDATE SUTTER_1P_DATA.dbo.HC_Gift_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
					GO
						--  SELECT top 1000 * FROM SUTTER_1P_DATA.dbo.HC_Gift_v

				--GIFT TRIBUTE
				ALTER TABLE SUTTER_1P_DATA.dbo.[HC_Gift_Tribute_v]
				ADD RE_DB_Id VARCHAR(30)
				GO			
					UPDATE SUTTER_1P_DATA.dbo.[HC_Gift_Tribute_v]
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+[GFTLink]
					GO
					 --  SELECT top 1000 * FROM SUTTER_1P_DATA.dbo.[HC_Gift_Tribute_v]

				--CONS ACTION 
				ALTER TABLE SUTTER_1P_DATA.dbo.HC_Cons_Action_v
				ADD RE_DB_Id VARCHAR(30)
				GO 
					UPDATE SUTTER_1P_DATA.dbo.HC_Cons_Action_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
					GO
				--CONS ACTION SOLICITOR
				ALTER TABLE SUTTER_1P_DATA.dbo.[HC_Cons_Action_Solicitor_v]
				ADD RE_DB_Id VARCHAR(30)
				GO 
					UPDATE SUTTER_1P_DATA.dbo.[HC_Cons_Action_Solicitor_v]
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+[ACSolImpID]
 					GO

 				ALTER TABLE SUTTER_1P_DATA.dbo.[HC_Cons_Action_Solicitor_v]
				ADD RE_DB_Id_AC VARCHAR(30)
				GO 
					UPDATE SUTTER_1P_DATA.dbo.[HC_Cons_Action_Solicitor_v]
					SET RE_DB_Id_AC=RE_DB_OwnerShort+'-'+[ACImpID]
 		 			GO
				--CONS SOLICITOR 
				ALTER TABLE SUTTER_1P_DATA.dbo.HC_Cons_Solicitor_v
				ADD RE_DB_Id VARCHAR(30)
			 	GO
					UPDATE SUTTER_1P_DATA.dbo.HC_Cons_Solicitor_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
				 	GO	

				ALTER TABLE SUTTER_1P_DATA.dbo.HC_Cons_Solicitor_v
				ADD RE_DB_Id_ASRLink VARCHAR(30)
				GO 
					UPDATE SUTTER_1P_DATA.dbo.HC_Cons_Solicitor_v
					SET RE_DB_Id_ASRLink=RE_DB_OwnerShort+'-'+ASRLink
					GO 		

							--SELECT * FROM SUTTER_1P_DATA.dbo.HC_Cons_Solicitor_v where ASRLink is null or ASRLink=''
				--PROPOSAL
				ALTER TABLE SUTTER_1P_DATA.dbo.HC_Proposal_v
				ADD RE_DB_Id VARCHAR(30)
				GO 
					UPDATE SUTTER_1P_DATA.dbo.HC_Proposal_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
				 	GO

				--PROSPECT RATING
				ALTER TABLE SUTTER_1P_DATA.DBO.HC_Prospect_Rating_v
				ADD RE_DB_Id VARCHAR(30)	
				GO 
					UPDATE SUTTER_1P_DATA.dbo.HC_Prospect_Rating_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
					GO 

				--CONSTITUENT ATTRIBUTE
				ALTER TABLE SUTTER_1P_DATA.DBO.HC_Cons_Attributes_v
				ADD RE_DB_Id VARCHAR(30)	
				GO 
					UPDATE SUTTER_1P_DATA.dbo.HC_Cons_Attributes_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
					GO 

				--CONSTITUENT CODE 
				ALTER TABLE SUTTER_1P_DATA.DBO.HC_Cons_ConsCode_v
				ADD RE_DB_Id VARCHAR(30)	
			 	GO
					UPDATE SUTTER_1P_DATA.dbo.HC_Cons_ConsCode_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
					GO 

END 					 
 


BEGIN--MDM: Constituents
			--	SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.HC_Constituents_v  1,080,598

			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Constituents_v
			ADD NEW_TGT_IMPORT_ID VARCHAR(50)
			GO	
			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Constituents_v
			ADD MDM_PTY_ID VARCHAR(50)
			GO			
			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Constituents_v
			ADD FULL_NM VARCHAR(255)
			GO
			SELECT * FROM SUTTER_1P_DATA.dbo.HC_Constituents_v
			SELECT * FROM SUTTER_1P_DATA.[TBL].MDM_RECORDS_1P1T_CN
			
			SELECT   TOP 10  T.RE_Db_Id, T.KeyInd, T.ConsID, T.ImportID, T.Name, T.RE_DB_OwnerShort
					, T2.NEW_TGT_IMPORT_ID, T2.MDM_PTY_ID, T2.FULL_NM
			FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T
			LEFT JOIN SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_CN AS T2 ON T.RE_DB_Id = T2.IMPORT_ID

				UPDATE  SUTTER_1P_DATA.dbo.HC_Constituents_v 
				SET NEW_TGT_IMPORT_ID=T2.NEW_TGT_IMPORT_ID, MDM_PTY_ID=T2.MDM_PTY_ID, FULL_NM=T2.FULL_NM
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T
				LEFT JOIN SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_CN AS T2 ON T.RE_DB_Id = T2.IMPORT_ID

				SELECT * FROM SUTTER_1P_DATA.dbo.HC_Constituents_v 
				WHERE NEW_TGT_IMPORT_ID IS NULL  
			
				UPDATE SUTTER_1P_DATA.dbo.HC_Constituents_v  SET NEW_TGT_IMPORT_ID =RE_DB_Id
				WHERE NEW_TGT_IMPORT_ID IS NULL

			--test
				SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.HC_Constituents_v  --before 1,080,598

				SELECT TOP 100 ImportID, Name, RE_DB_OwnerShort, RE_DB_Id, NEW_TGT_IMPORT_ID, MDM_PTY_ID
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v  
				WHERE RE_DB_Id!=NEW_TGT_IMPORT_ID 

				SELECT * FROM SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_CN WHERE MDM_PTY_ID ='MDM00012033778'

				SELECT re_Db_id FROM sUTTER_1P_DATA.dbo.HC_Constituents_v  WHERE re_Db_id  IS NULL GROUP BY re_Db_id   --1,080,598
				SELECT NEW_TGT_IMPORT_ID FROM sUTTER_1P_DATA.dbo.HC_Constituents_v  WHERE new_tgt_import_id IS NULL GROUP BY NEW_TGT_IMPORT_ID  --1,050,869

END;

--CREATE FIELDS TO ALLOCATE "MERGED FROM" VALUES.--CONSTITUENTS
	--create 1 field for each affiliate and place id in it. 
			DROP TABLE SUTTER_1P_MIGRATION.TBL.CONSTITUENTS_MULTIPLE_IDS
			
			USE [SUTTER_1P_MIGRATION]
			GO
			CREATE SCHEMA TBL
			GO
			CREATE SCHEMA IMP
			GO

			SELECT NEW_TGT_IMPORT_ID, COUNT(*) c 
			INTO SUTTER_1P_MIGRATION.TBL.CONSTITUENTS_MULTIPLE_IDS
			FROM SUTTER_1P_DATA.dbo.HC_Constituents_v GROUP BY NEW_TGT_IMPORT_ID HAVING COUNT(*)>1
			ORDER BY c DESC
			
			DROP TABLE  SUTTER_1P_MIGRATION.TBL.CONSTITUENTS_MERGED_FROM  

			BEGIN--IDS: CREATE final pivot of ids  
 					SELECT DISTINCT T.NEW_TGT_IMPORT_ID,  
						CAST(STUFF(( SELECT ' '+ T1.RE_DB_Id +'; ' --+ CHAR(10)
								FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
									WHERE T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID AND (T1.RE_DB_ID!=T1.NEW_TGT_IMPORT_ID)  
									ORDER BY T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportId
								FOR
									XML PATH('')
									  ), 1, 1, '') AS NVARCHAR(1000)) AS Merged_from__c
						,CAST(STUFF(( SELECT ' '+RE_DB_OwnerShort+'-'+ T1.ConsID +'; ' --+ CHAR(10)
								FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
									WHERE T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID AND (T1.RE_DB_ID!=T1.NEW_TGT_IMPORT_ID)  
									ORDER BY T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportId
								FOR
									XML PATH('')
									  ), 1, 1, '') AS NVARCHAR(1000)) AS Merged_from_ConsID__c
					INTO SUTTER_1P_MIGRATION.TBL.CONSTITUENTS_MERGED_FROM    -- DROP TABLE  SUTTER_1P_MIGRATION.TBL.CONSTITUENTS_MERGED_FROM  
					FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T
					INNER JOIN SUTTER_1P_MIGRATION.TBL.CONSTITUENTS_MULTIPLE_IDS T3 ON T.NEW_TGT_IMPORT_ID=T3.NEW_TGT_IMPORT_ID
					ORDER BY T.NEW_TGT_IMPORT_ID
		 
			END;
			 	
				SELECT *, LEFT(NEW_TGT_IMPORT_ID, 4) Affil, LEN(merged_from__c) l 
				FROM SUTTER_1P_MIGRATION.TBL.CONSTITUENTS_MERGED_FROM 
				order BY AFFil ASC , l desc

				SELECT * FROM SUTTER_1P_MIGRATION.TBL.CONSTITUENTS_MERGED_FROM  --WHERE NEW_TGT_IMPORT_id='CVRX-LB143'
				WHERE NEW_TGT_IMPORT_ID IN (SELECT NEW_TGT_IMPORT_ID FROM SUTTER_1P_MIGRATION.TBL.CONSTITUENTS_MERGED_FROM  GROUP BY NEW_TGT_IMPORT_ID HAVING COUNT(*)>1)
				ORDER BY NEW_TGT_IMPORT_ID


BEGIN--MDM: IndRelat
			--SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v 518,557
			--const fields
			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			ADD RE_DB_Id_IRLink VARCHAR(50)
	
				SELECT IRImpID, IRLink, RE_DB_Id, RE_DB_Id_IRLink
				FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
				WHERE IRLink IS NOT NULL AND IRLink!=''

				UPDATE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
				SET RE_DB_Id_IRLink= RE_DB_OwnerShort+'-'+IRLink 
				WHERE IRLink IS NOT NULL AND IRLink!=''

			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			ADD RE_DB_Id_CN VARCHAR(50)
	
		 		SELECT ImportID, RE_DB_Id_CN, IRImpID, RE_DB_Id, IRLink , RE_DB_Id_IRLink
				FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
						 
				UPDATE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
				SET RE_DB_Id_CN= RE_DB_OwnerShort+'-'+ImportID 

			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			ADD NEW_TGT_IMPORT_ID_IR VARCHAR(50)

			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			ADD NEW_TGT_IMPORT_ID_CN VARCHAR(50)
			
			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			ADD NEW_TGT_IMPORT_ID_IRLINK VARCHAR(50)

			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			ADD MDM_PTY_ID VARCHAR(50)

			--NOTE: see INDIVIDUAL_RELATINOSHIP_ID.SQL FOR DETAILS
			--clear
				UPDATE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
				SET NEW_TGT_IMPORT_ID_IR=NULL, MDM_PTY_ID=NULL, NEW_TGT_IMPORT_ID_CN=NULL, NEW_TGT_IMPORT_ID_IRLINK=NULL
				 
				--UPDATE
				UPDATE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
				SET NEW_TGT_IMPORT_ID_IR=CASE WHEN (T2.IMPORT_ID IS NULL OR T2.IMPORT_ID='') OR (T2.NEW_TGT_RECORD_TYPE='IndvDonorConstituent') THEN T1.RE_DB_Id ELSE T2.NEW_TGT_IMPORT_ID END
				,MDM_PTY_ID=T2.MDM_PTY_ID
				,NEW_TGT_IMPORT_ID_CN=T3.NEW_TGT_IMPORT_ID  		--ImportID
				,NEW_TGT_IMPORT_ID_IRLINK=CASE WHEN (T2.NEW_TGT_RECORD_TYPE='IndvDonorConstituent') THEN T2.NEW_TGT_IMPORT_ID
											WHEN (T4.IMPORT_ID IS NOT NULL) THEN T4.NEW_TGT_IMPORT_ID ELSE T1.RE_DB_Id_IRLink  END  --IRLink
				FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v T1
				LEFT JOIN SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_IR AS T2 ON T1.RE_DB_Id = T2.IMPORT_ID			--IRImpID
				LEFT JOIN SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_CN AS T3 ON T1.RE_DB_Id_CN = T3.IMPORT_ID			--ImportID
				LEFT JOIN SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_CN AS T4 ON T1.RE_DB_Id_IRLink = T4.IMPORT_ID		--IRLink
				 
				SELECT * FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v WHERE NEW_TGT_IMPORT_ID_CN IS NULL OR NEW_TGT_IMPORT_ID_CN=''
				UPDATE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v  SET NEW_TGT_IMPORT_ID_CN=RE_DB_ID_CN WHERE NEW_TGT_IMPORT_ID_CN IS NULL 

				SELECT * FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v 
				WHERE NEW_TGT_IMPORT_ID_IR IS NULL OR NEW_TGT_IMPORT_ID_IR=''
				
				UPDATE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v  SET NEW_TGT_IMPORT_ID_IR=[RE_DB_OwnerShort]+'-'+[IRImpID] 
				WHERE NEW_TGT_IMPORT_ID_IR IS NULL OR NEW_TGT_IMPORT_ID_IR=''

				SELECT * FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v 
				WHERE (NEW_TGT_IMPORT_ID_IRLINK IS NULL OR NEW_TGT_IMPORT_ID_IRLINK='') 
						AND (IRLink IS NOT NULL AND IRLink!='')



--CREATE FIELDS TO ALLOCATE "MERGED FROM" VALUES.--INDIVIDUAL RELATIONSHIPS 
	--create 1 field for each affiliate and place id in it. 

	 

			DROP TABLE SUTTER_1P_MIGRATION.TBL.RELATIONSHIPS_MULTIPLE_IDS_IR
			GO 
			DROP TABLE  SUTTER_1P_MIGRATION.TBL.RELATIONSHIPS_MERGED_FROM_IR 
			GO 
			
			SELECT NEW_TGT_IMPORT_ID_IR, COUNT(*) c 
			INTO SUTTER_1P_MIGRATION.TBL.RELATIONSHIPS_MULTIPLE_IDS_IR	
			FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v GROUP BY NEW_TGT_IMPORT_ID_IR HAVING COUNT(*)>1
			ORDER BY c DESC
			GO

			
		BEGIN--IDS: CREATE final pivot of ids for Ind Relat   
 				SELECT DISTINCT T.NEW_TGT_IMPORT_ID_IR,  
					CAST(STUFF(( SELECT ' '+ T1.RE_DB_Id +'; ' --+ CHAR(10)
							FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v T1
								WHERE T1.NEW_TGT_IMPORT_ID_IR=T.NEW_TGT_IMPORT_ID_IR AND (T1.RE_DB_ID!=T1.NEW_TGT_IMPORT_ID_IR)  
								ORDER BY T1.RE_DB_OwnerShort, T1.IRImpID
							FOR
								XML PATH('')
									), 1, 1, '') AS NVARCHAR(1000)) AS Merged_from__c
				INTO SUTTER_1P_MIGRATION.TBL.RELATIONSHIPS_MERGED_FROM_IR     
				FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v T
				INNER JOIN SUTTER_1P_MIGRATION.TBL.RELATIONSHIPS_MULTIPLE_IDS_IR T3 ON T.NEW_TGT_IMPORT_ID_IR=T3.NEW_TGT_IMPORT_ID_IR
				ORDER BY T.NEW_TGT_IMPORT_ID_IR
		END

BEGIN--MDM: ORG_RELAT
			--SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v 518,557
			--const fields
			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Org_Relat_v
			ADD RE_DB_Id_ORLink VARCHAR(50)
	
				SELECT ORImpID, ORLink, RE_DB_Id, RE_DB_Id_ORLink, RE_DB_OwnerShort
				FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v
				WHERE ORLink IS NOT NULL AND ORLink!=''

				UPDATE SUTTER_1P_DATA.dbo.HC_Org_Relat_v
				SET RE_DB_Id_ORLink= RE_DB_OwnerShort+'-'+ORLink 
				WHERE ORLink IS NOT NULL AND ORLink!=''

			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Org_Relat_v
			ADD RE_DB_Id_CN VARCHAR(50)
	
		 		SELECT ImportID, RE_DB_Id_CN, ORImpID, RE_DB_Id, ORLink , RE_DB_Id_ORLink
				FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v
						 
				UPDATE SUTTER_1P_DATA.dbo.HC_Org_Relat_v
				SET RE_DB_Id_CN= RE_DB_OwnerShort+'-'+ImportID 

			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Org_Relat_v
			ADD NEW_TGT_IMPORT_ID_OR VARCHAR(50)

			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Org_Relat_v
			ADD NEW_TGT_IMPORT_ID_CN VARCHAR(50)
			
			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Org_Relat_v
			ADD NEW_TGT_IMPORT_ID_ORLINK VARCHAR(50)

			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Org_Relat_v
			ADD MDM_PTY_ID VARCHAR(50)

			--NOTE: see INDIVIDUAL_RELATINOSHIP_ID.SQL FOR DETAILS
			--clear
				UPDATE SUTTER_1P_DATA.dbo.HC_Org_Relat_v
				SET NEW_TGT_IMPORT_ID_OR=NULL, MDM_PTY_ID=NULL, NEW_TGT_IMPORT_ID_CN=NULL, NEW_TGT_IMPORT_ID_ORLINK=NULL
			
			
		 	--UPDATE
			UPDATE SUTTER_1P_DATA.dbo.HC_Org_Relat_v
			SET NEW_TGT_IMPORT_ID_OR=CASE WHEN (T2.IMPORT_ID IS NULL OR T2.IMPORT_ID='') OR (T2.NEW_TGT_RECORD_TYPE='OrgDonorConstituent') THEN T1.RE_DB_Id ELSE T2.NEW_TGT_IMPORT_ID END
			,MDM_PTY_ID=T2.MDM_PTY_ID
			,NEW_TGT_IMPORT_ID_CN=CASE WHEN T3.IMPORT_ID IS NULL THEN T1.RE_DB_Id_CN ELSE T3.NEW_TGT_IMPORT_ID END 		--ImportID
			,NEW_TGT_IMPORT_ID_ORLINK=CASE WHEN (T2.NEW_TGT_RECORD_TYPE='OrgDonorConstituent') THEN T2.NEW_TGT_IMPORT_ID
									  WHEN (T4.IMPORT_ID IS NOT NULL) THEN T4.NEW_TGT_IMPORT_ID ELSE T1.RE_DB_Id_ORLink  END  --IRLink
			FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v T1
			LEFT JOIN SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_OR AS T2 ON T1.RE_DB_Id = T2.IMPORT_ID			--IRImpID
			LEFT JOIN SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_CN AS T3 ON T1.RE_DB_Id_CN = T3.IMPORT_ID			--ImportID
			LEFT JOIN SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_CN AS T4 ON T1.RE_DB_Id_ORLink = T4.IMPORT_ID		--IRLink

				SELECT * FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v WHERE NEW_TGT_IMPORT_ID_CN IS NULL 	OR NEW_TGT_IMPORT_ID_CN=''
				UPDATE SUTTER_1P_DATA.dbo.HC_Org_Relat_v  SET NEW_TGT_IMPORT_ID_CN=RE_DB_ID_CN WHERE NEW_TGT_IMPORT_ID_CN IS NULL 

				SELECT * FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v WHERE NEW_TGT_IMPORT_ID_OR IS NULL OR NEW_TGT_IMPORT_ID_OR=''
				SELECT * FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v WHERE (NEW_TGT_IMPORT_ID_ORLINK IS NULL OR NEW_TGT_IMPORT_ID_ORLINK='') AND (ORLink IS NOT NULL AND ORLink!='')

--CREATE FIELDS TO ALLOCATE "MERGED FROM" VALUES.--ORGANIZATION RELATIONSHIPS 
	--create 1 field for each affiliate and place id in it. 
			DROP TABLE SUTTER_1P_MIGRATION.TBL.RELATIONSHIPS_MULTIPLE_IDS_OR
			 
			SELECT NEW_TGT_IMPORT_ID_OR, COUNT(*) c 
			INTO SUTTER_1P_MIGRATION.TBL.RELATIONSHIPS_MULTIPLE_IDS_OR  -- 
			FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v GROUP BY NEW_TGT_IMPORT_ID_OR HAVING COUNT(*)>1
			ORDER BY c DESC
			GO

			 DROP TABLE  SUTTER_1P_MIGRATION.TBL.RELATIONSHIPS_MERGED_FROM_OR  
			 GO

			BEGIN--IDS: CREATE final pivot of ids for ORG Relat   
 					SELECT DISTINCT T.NEW_TGT_IMPORT_ID_OR,  
						CAST(STUFF(( SELECT ' '+ T1.RE_DB_Id +'; ' --+ CHAR(10)
								FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v T1
									WHERE T1.NEW_TGT_IMPORT_ID_OR=T.NEW_TGT_IMPORT_ID_OR AND (T1.RE_DB_ID!=T1.NEW_TGT_IMPORT_ID_OR)  
									ORDER BY T1.RE_DB_OwnerShort, T1.ORImpID
								FOR
									XML PATH('')
									  ), 1, 1, '') AS NVARCHAR(1000)) AS Merged_from__c
					INTO SUTTER_1P_MIGRATION.TBL.RELATIONSHIPS_MERGED_FROM_OR    --
					FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v T
					INNER JOIN SUTTER_1P_MIGRATION.TBL.RELATIONSHIPS_MULTIPLE_IDS_OR T3 ON T.NEW_TGT_IMPORT_ID_OR=T3.NEW_TGT_IMPORT_ID_OR
					ORDER BY T.NEW_TGT_IMPORT_ID_OR
			END 


BEGIN--- CREATE BASE TABLES OF CONSTITUENTS AND INDIVIDUAL RELATIONSHIPS TO WORK WITH THE HEAD OF HOUSEHOLD STRUCTURE AND ONLY CREATE ACCOUNTS AND CONTACTS THAT WERE NOT MERGED. 
				SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.HC_Constituents_v --983348
				SELECT COUNT(*) FROM SUTTER_1P_DATA.TBL.SF_Constituents   --953520
				SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v --523003
		
				SELECT DISTINCT NEW_TGT_IMPORT_ID FROM SUTTER_1P_DATA.dbo.HC_Constituents_v --931,593
				SELECT DISTINCT NEW_TGT_IMPORT_ID_IR FROM SUTTER_1P_DATA.DBO.HC_Ind_Relat_v --511,580

				DROP TABLE SUTTER_1P_DATA.TBL.SF_CONSTITUENTS 
				DROP TABLE SUTTER_1P_DATA.TBL.SF_IND_RELAT	
				DROP TABLE SUTTER_1P_DATA.TBL.SF_ORG_RELAT	
			 
				--BASE CONSITUENT TABLE FOR HOUSEHOLDING  -- table already has then new tgt id. 
					SELECT DISTINCT TA.ConsID,  TA.NEW_TGT_IMPORT_ID AS ImportID, TA.KeyInd, TA.FirstName, TA.LastName, TA.Name,TA.Deceased, TA.DecDate, TA.RE_DB_OwnerShort, TA.RE_DB_ID, TA.MDM_PTY_ID, TA.NEW_TGT_IMPORT_ID
					INTO SUTTER_1P_DATA.TBL.SF_CONSTITUENTS
					FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T
					INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v TA ON T.NEW_TGT_IMPORT_ID=TA.RE_DB_Id
					--983348
 
						SELECT * FROM SUTTER_1P_DATA.TBL.SF_CONSTITUENTS
						WHERE importid IN (SELECT ImportID FROM SUTTER_1P_DATA.TBL.SF_CONSTITUENTS GROUP BY ImportID HAVING COUNT(*)>1)


				--BASE IND RELAT TABLE FOR HOUSEHOLDING
					SELECT DISTINCT TA.NEW_TGT_IMPORT_ID_IR AS IRImpID, 
									TA.NEW_TGT_IMPORT_ID_CN AS ImportID, 
									TA.NEW_TGT_IMPORT_ID_IRLINK AS IRLink, 
									TA.IRFirstName, TA.IRLastName, TA.IRIsHH, TA.IRIsSpouse, TA.IRDeceased, TA.IRDecDate,
									TA.NEW_TGT_IMPORT_ID_IR,
									TA.RE_DB_OwnerShort,
									TA.RE_DB_ID AS zrefRE_DB_Id,
									TA.ImportID AS zrefImportID,
									TA.IRImpID AS zrefIRImpID,
									TA.IRLink AS zrefIRLink
					INTO SUTTER_1P_DATA.TBL.SF_IND_RELAT			 
					FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v T 
					INNER JOIN SUTTER_1P_DATA.dbo.HC_Ind_Relat_v TA ON T.NEW_TGT_IMPORT_ID_IR=TA.RE_DB_Id
			 		--515,849
			
						SELECT * FROM SUTTER_1P_DATA.TBL.SF_IND_RELAT
						WHERE IRImpID IN (SELECT IRImpID FROM SUTTER_1P_DATA.TBL.SF_IND_RELAT GROUP BY IRImpID HAVING COUNT(*)>1)


				--BASE ORG RELAT TABLE 
 					SELECT DISTINCT TA.NEW_TGT_IMPORT_ID_OR AS ORImpID, 
									TA.NEW_TGT_IMPORT_ID_CN AS ImportID, 
									TA.NEW_TGT_IMPORT_ID_ORLINK AS ORLink, 
									TA.ORFullName,
									TA.RE_DB_OwnerShort,
									TA.RE_DB_ID AS zrefRE_DB_Id,
									TA.ImportID AS zrefImportID,
									TA.ORImpID AS zrefORImpID,
									TA.ORLink AS zrefORLink
					INTO SUTTER_1P_DATA.TBL.SF_ORG_RELAT			 
					FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v T 
					INNER JOIN SUTTER_1P_DATA.dbo.HC_Org_Relat_v TA ON T.NEW_TGT_IMPORT_ID_OR=TA.RE_DB_Id

						SELECT * FROM SUTTER_1P_DATA.TBL.SF_ORG_RELAT
						WHERE ORImpID IN (SELECT ORImpID FROM SUTTER_1P_DATA.TBL.SF_ORG_RELAT GROUP BY ORImpID HAVING COUNT(*)>1)

					SELECT * FROM SUTTER_1P_DATA.TBL.SF_ORG_RELAT	--51828
					SELECT * FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v --52083

END;

BEGIN --UPDATE CONS ADDRESS COUNTRY

		 SELECT [AddrState], [AddrCountry], count(*) 
		 FROM SUTTER_1P_DATA.DBO.HC_Cons_Address_v
		 WHERE --country is null and 
		 (AddrState ='AL' or AddrState='AK' or AddrState='AZ' or AddrState='AR'  or AddrState='CA' or AddrState='CO' or AddrState='CT' or AddrState='DE' or AddrState='DC' or AddrState='FL'
		 or AddrState='GA' or AddrState='HI' or AddrState='ID' or AddrState='IL' or AddrState='IN' or AddrState='IA' or AddrState='KS' or AddrState='KY' or AddrState='LA' or AddrState='ME' or AddrState='MD'
		 or AddrState='MA' or AddrState='MI' or AddrState='MN' or AddrState='MS' or AddrState='MO' or AddrState='MT' or AddrState='NE' or AddrState='NV' or AddrState='NH' or AddrState='NJ' or AddrState='NM' 
         or AddrState='NY' or AddrState='NC' or AddrState='ND' or AddrState='OH' or AddrState='OK' or AddrState='OR' or AddrState='PA' or AddrState='RI' or AddrState='SC' or AddrState='SD' or AddrState='TN'
         or AddrState='TX' or AddrState='UT' or AddrState='VT' or AddrState='VA' or AddrState='WA' or AddrState='WV' or AddrState='WI' or AddrState='WY')
		 GROUP BY [AddrState], [AddrCountry]   --'United States'
		 
		 UPDATE SUTTER_1P_DATA.DBO.HC_Cons_Address_v SET [AddrCountry] = 'United States' WHERE --country is null AND 
		 (AddrState ='AL' or AddrState='AK' or AddrState='AZ' or AddrState='AR'  or AddrState='CA' or AddrState='CO' or AddrState='CT' or AddrState='DE' or AddrState='DC' or AddrState='FL'
		 or AddrState='GA' or AddrState='HI' or AddrState='ID' or AddrState='IL' or AddrState='IN' or AddrState='IA' or AddrState='KS' or AddrState='KY' or AddrState='LA' or AddrState='ME' or AddrState='MD'
		 or AddrState='MA' or AddrState='MI' or AddrState='MN' or AddrState='MS' or AddrState='MO' or AddrState='MT' or AddrState='NE' or AddrState='NV' or AddrState='NH' or AddrState='NJ' or AddrState='NM' 
         or AddrState='NY' or AddrState='NC' or AddrState='ND' or AddrState='OH' or AddrState='OK' or AddrState='OR' or AddrState='PA' or AddrState='RI' or AddrState='SC' or AddrState='SD' or AddrState='TN'
         or AddrState='TX' or AddrState='UT' or AddrState='VT' or AddrState='VA' or AddrState='WA' or AddrState='WV' or AddrState='WI' or AddrState='WY')


END;


BEGIN --UPDATE NON-CONS IND RELAT ADDRESS COUNTRY

		 SELECT [IRAddrState], [IRAddrCountry], count(*) 
		 FROM SUTTER_1P_DATA.DBO.HC_Ind_Relat_v
		 WHERE (IRLink IS NULL OR IRLink='') AND 
		 (IRAddrState ='AL' or IRAddrState='AK' or IRAddrState='AZ' or IRAddrState='AR'  or IRAddrState='CA' or IRAddrState='CO' or IRAddrState='CT' or IRAddrState='DE' or IRAddrState='DC' or IRAddrState='FL'
		 or IRAddrState='GA' or IRAddrState='HI' or IRAddrState='ID' or IRAddrState='IL' or IRAddrState='IN' or IRAddrState='IA' or IRAddrState='KS' or IRAddrState='KY' or IRAddrState='LA' or IRAddrState='ME' or IRAddrState='MD'
		 or IRAddrState='MA' or IRAddrState='MI' or IRAddrState='MN' or IRAddrState='MS' or IRAddrState='MO' or IRAddrState='MT' or IRAddrState='NE' or IRAddrState='NV' or IRAddrState='NH' or IRAddrState='NJ' or IRAddrState='NM' 
         or IRAddrState='NY' or IRAddrState='NC' or IRAddrState='ND' or IRAddrState='OH' or IRAddrState='OK' or IRAddrState='OR' or IRAddrState='PA' or IRAddrState='RI' or IRAddrState='SC' or IRAddrState='SD' or IRAddrState='TN'
         or IRAddrState='TX' or IRAddrState='UT' or IRAddrState='VT' or IRAddrState='VA' or IRAddrState='WA' or IRAddrState='WV' or IRAddrState='WI' or IRAddrState='WY')
		 GROUP BY [IRAddrState], [IRAddrCountry]   --'United States'
		 
		 UPDATE SUTTER_1P_DATA.DBO.HC_Ind_Relat_v SET [IRAddrCountry] = 'United States' WHERE (IRLink IS NULL OR IRLink='') AND 
		 (IRAddrState ='AL' or IRAddrState='AK' or IRAddrState='AZ' or IRAddrState='AR'  or IRAddrState='CA' or IRAddrState='CO' or IRAddrState='CT' or IRAddrState='DE' or IRAddrState='DC' or IRAddrState='FL'
		 or IRAddrState='GA' or IRAddrState='HI' or IRAddrState='ID' or IRAddrState='IL' or IRAddrState='IN' or IRAddrState='IA' or IRAddrState='KS' or IRAddrState='KY' or IRAddrState='LA' or IRAddrState='ME' or IRAddrState='MD'
		 or IRAddrState='MA' or IRAddrState='MI' or IRAddrState='MN' or IRAddrState='MS' or IRAddrState='MO' or IRAddrState='MT' or IRAddrState='NE' or IRAddrState='NV' or IRAddrState='NH' or IRAddrState='NJ' or IRAddrState='NM' 
         or IRAddrState='NY' or IRAddrState='NC' or IRAddrState='ND' or IRAddrState='OH' or IRAddrState='OK' or IRAddrState='OR' or IRAddrState='PA' or IRAddrState='RI' or IRAddrState='SC' or IRAddrState='SD' or IRAddrState='TN'
         or IRAddrState='TX' or IRAddrState='UT' or IRAddrState='VT' or IRAddrState='VA' or IRAddrState='WA' or IRAddrState='WV' or IRAddrState='WI' or IRAddrState='WY')
		 
END;


BEGIN --UPDATE NON-CONS ORG RELAT ADDRESS COUNTRY

		 SELECT [ORAddrState], [ORAddrCountry], count(*) 
		 FROM SUTTER_1P_DATA.DBO.HC_Org_Relat_v
		 WHERE (ORLINK IS NULL OR ORLINK='') AND 
		 (ORAddrState ='AL' or ORAddrState='AK' or ORAddrState='AZ' or ORAddrState='AR'  or ORAddrState='CA' or ORAddrState='CO' or ORAddrState='CT' or ORAddrState='DE' or ORAddrState='DC' or ORAddrState='FL'
		 or ORAddrState='GA' or ORAddrState='HI' or ORAddrState='ID' or ORAddrState='IL' or ORAddrState='IN' or ORAddrState='IA' or ORAddrState='KS' or ORAddrState='KY' or ORAddrState='LA' or ORAddrState='ME' or ORAddrState='MD'
		 or ORAddrState='MA' or ORAddrState='MI' or ORAddrState='MN' or ORAddrState='MS' or ORAddrState='MO' or ORAddrState='MT' or ORAddrState='NE' or ORAddrState='NV' or ORAddrState='NH' or ORAddrState='NJ' or ORAddrState='NM' 
         or ORAddrState='NY' or ORAddrState='NC' or ORAddrState='ND' or ORAddrState='OH' or ORAddrState='OK' or ORAddrState='OR' or ORAddrState='PA' or ORAddrState='RI' or ORAddrState='SC' or ORAddrState='SD' or ORAddrState='TN'
         or ORAddrState='TX' or ORAddrState='UT' or ORAddrState='VT' or ORAddrState='VA' or ORAddrState='WA' or ORAddrState='WV' or ORAddrState='WI' or ORAddrState='WY')
		 GROUP BY [ORAddrState], [ORAddrCountry]   --'United States'
		 
		 UPDATE SUTTER_1P_DATA.DBO.HC_Org_Relat_v SET [ORAddrCountry] = 'United States' WHERE (ORLINK IS NULL OR ORLINK='') AND 
		 (ORAddrState ='AL' or ORAddrState='AK' or ORAddrState='AZ' or ORAddrState='AR'  or ORAddrState='CA' or ORAddrState='CO' or ORAddrState='CT' or ORAddrState='DE' or ORAddrState='DC' or ORAddrState='FL'
		 or ORAddrState='GA' or ORAddrState='HI' or ORAddrState='ID' or ORAddrState='IL' or ORAddrState='IN' or ORAddrState='IA' or ORAddrState='KS' or ORAddrState='KY' or ORAddrState='LA' or ORAddrState='ME' or ORAddrState='MD'
		 or ORAddrState='MA' or ORAddrState='MI' or ORAddrState='MN' or ORAddrState='MS' or ORAddrState='MO' or ORAddrState='MT' or ORAddrState='NE' or ORAddrState='NV' or ORAddrState='NH' or ORAddrState='NJ' or ORAddrState='NM' 
         or ORAddrState='NY' or ORAddrState='NC' or ORAddrState='ND' or ORAddrState='OH' or ORAddrState='OK' or ORAddrState='OR' or ORAddrState='PA' or ORAddrState='RI' or ORAddrState='SC' or ORAddrState='SD' or ORAddrState='TN'
         or ORAddrState='TX' or ORAddrState='UT' or ORAddrState='VT' or ORAddrState='VA' or ORAddrState='WA' or ORAddrState='WV' or ORAddrState='WI' or ORAddrState='WY')
		 
END;

BEGIN --UPDATE ASSIGNED SOLICITOR RELATIONSHIP TYPE TO "SOLICITOR" WHEN ASRTYPE IS NULL

		SELECT * FROM SUTTER_1P_DATA.DBO.HC_Cons_Solicitor_v WHERE ASRType IS NULL
		UPDATE SUTTER_1P_DATA.DBO.HC_Cons_Solicitor_v SET ASRType='Solicitor' WHERE ASRType IS NULL
		
END;

BEGIN--UPDATE CONS ATTR DESC TO 'NULL'

		SELECT * FROM SUTTER_1P_DATA.DBO.HC_Cons_Attributes_v WHERE CAttrDesc IS NULL
		UPDATE SUTTER_1P_DATA.DBO.HC_Cons_Attributes_v SET CAttrDesc='NULL' WHERE CAttrDesc IS NULL	

END; 


BEGIN--UPDATE GIFT: CAMPAIGN, APPEAL, PACKAGE TO "NULL"

	--	SELECT GSplitCamp, GSplitAppeal, GSplitPkg, GSplitFund
	--	FROM SUTTER_1P_DATA.DBO.HC_Gift_SplitGift_v
	--	WHERE GSplitCamp IS NULL OR GSplitAppeal IS NULL OR GSplitPkg IS NULL OR GSplitFund	IS NULL
	--	GROUP BY GSplitCamp, GSplitAppeal, GSplitPkg, GSplitFund
		
		UPDATE SUTTER_1P_DATA.DBO.HC_Gift_SplitGift_v SET GSplitCamp='NULL' WHERE GSplitCamp IS NULL
		UPDATE SUTTER_1P_DATA.DBO.HC_Gift_SplitGift_v SET GSplitAppeal='NULL' WHERE GSplitAppeal IS NULL
		UPDATE SUTTER_1P_DATA.DBO.HC_Gift_SplitGift_v SET GSplitPkg='NULL' WHERE GSplitPkg IS NULL
		
END;

BEGIN--UPDATE CONS APPEAL: APPEAL, PACKAGE TO "NULL"
		SELECT * FROM SUTTER_1P_DATA.DBO.HC_Cons_Appeal_v WHERE CAPPackageID IS NULL 
		
		UPDATE SUTTER_1P_DATA.DBO.HC_Cons_Appeal_v SET CAPPackageID='NULL' WHERE CAPPackageID IS NULL 
		

END;

BEGIN--UPDATE CREDIT CARD TYPE
		--SELECT GFPayMeth, GFCCType, COUNT(*) C 
		--FROM SUTTER_1P_DATA.DBO.HC_Gift_v
		--GROUP BY GFPayMeth, GFCCType
		--ORDER BY GFPayMeth, GFCCType
		
		UPDATE SUTTER_1P_DATA.DBO.HC_Gift_v SET GFCCType='MasterCard' WHERE GFCCType='Master Card'
		UPDATE SUTTER_1P_DATA.DBO.HC_Gift_v SET GFCCType='American Express' WHERE GFCCType='Amer Express' OR [GFCCType]='Amex'
		UPDATE SUTTER_1P_DATA.DBO.HC_Gift_v SET GFCCType='Discover' WHERE GFCCType='DISC' 
		UPDATE SUTTER_1P_DATA.DBO.HC_Gift_v SET GFCCType=NULL WHERE GFCCType='Visa' AND GFPayMeth='Personal Check'
		UPDATE SUTTER_1P_DATA.DBO.HC_Gift_v SET GFCCType=NULL WHERE (GFCCType='No credit card'   OR GFCCType='PAYD' OR GFCCType='VCHR' OR GFCCType='?' OR [GFCCType]='Credit Card' OR [GFCCType]='MACC')
		
END; 

BEGIN--UPDATE payment info where paytype is not credit card. 

		SELECT GFImpId, GFPayMeth, GFCCType, GFCCNum, GFCardholderName, GFCCExpOn, GFAuthCode
		FROM SUTTER_1P_DATA.dbo.hc_gift_v 
		WHERE GFPayMeth!='Credit Card' AND (GFCCType IS NOT NULL 
											OR GFCCNum IS NOT NULL 
											OR GFCardholderName IS NOT NULL 
											OR GFCCExpOn IS NOT NULL
											OR GFAuthCode IS NOT NULL)
		
		UPDATE SUTTER_1P_DATA.dbo.hc_gift_v SET GFCCNum=NULL, GFCardholderName=NULL
		WHERE GFPayMeth!='Credit Card' AND (GFCCType IS NOT NULL 
											OR GFCCNum IS NOT NULL 
											OR GFCardholderName IS NOT NULL 
											OR GFCCExpOn IS NOT NULL
											OR GFAuthCode IS NOT NULL)
END; 	

BEGIN--ConsAction AddedBY to use on chart_actionsolicitor column addedby
		SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Cons_Action_v]
		WHERE [AddedBy] IS NULL

		UPDATE  [SUTTER_1P_DATA].dbo.[HC_Cons_Action_v]
		SET [AddedBy]='NULL'
		WHERE [AddedBy] IS NULL
END;

BEGIN

	--CANotes with PICUTRE On NOTE field
	SELECT * FROM SUTTER_1P_DATA.dbo.HC_Cons_Action_Notes_v 
	WHERE RE_DB_OwnerShort='SMCF' AND (CANoteImpID='03947-420-0000035508' 
		OR CANoteImpID='03947-420-0000041416'
		OR CANoteImpID='03947-420-0000050951'
		OR CANoteImpID='00001-420-0000012297')
	

	SELECT LEN([CANoteNotes]) AS  l, * 
	FROM SUTTER_1P_DATA.dbo.HC_Cons_Action_Notes_v 
	WHERE LEN([CANoteNotes]) >32000
	ORDER BY l ASC 

	UPDATE [SUTTER_1P_DATA].dbo.HC_Cons_Action_Notes_v
	SET [CANoteNotes]=NULL 
	WHERE LEN([CANoteNotes]) >100000
	--REMOVE contect of NOTES fields
	UPDATE SUTTER_1P_DATA.dbo.HC_Cons_Action_Notes_v
	SET CANoteNotes=NULL
	WHERE RE_DB_OwnerShort='SMCF' AND (CANoteImpID='03947-420-0000035508' 
		OR CANoteImpID='03947-420-0000041416'
		OR CANoteImpID='03947-420-0000050951')	 OR ([RE_DB_OwnerShort]='PAMF' AND [CANoteImpID]='00001-420-0000012297')
END;
	
BEGIN -- PROPOSAL ATTRIBUTE DESCRIPTION

	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Proposal_Attr_v] WHERE [PRAttrDesc] IS NULL
	
	UPDATE [SUTTER_1P_DATA].dbo.[HC_Proposal_Attr_v]
	SET [PRAttrDesc]='NULL' 
	WHERE [PRAttrDesc] IS null

	SELECT * FROM [SUTTER_1P_DATA].dbo.[CHART_ProposalAttr] WHERE [PRAttrDesc] IS NULL
	
	UPDATE [SUTTER_1P_DATA].dbo.[CHART_ProposalAttr]
	SET [PRAttrDesc]='NULL' 
	WHERE [PRAttrDesc] IS null

END 

BEGIN 
	SELECT * FROM [SUTTER_1P_DATA].DBO.[HC_Cons_Action_Attr_v]
	WHERE [ACAttrDesc] IS NULL 

	UPDATE [SUTTER_1P_DATA].DBO.[HC_Cons_Action_Attr_v]
	SET [ACAttrDesc]='NULL'
	WHERE [ACAttrDesc] IS NULL 
END 

BEGIN --- UPDATE RATING fields 
	SELECT * FROM  [SUTTER_1P_DATA].DBO.HC_Prospect_Rating_v]
	WHERE PRateCat IS NULL OR  PRateSource IS NULL OR  PRateDesc IS null

	UPDATE [SUTTER_1P_DATA].DBO.HC_Prospect_Rating_v
	SET PRateCat='NULL'
	WHERE PRateCat IS NULL 

 	UPDATE [SUTTER_1P_DATA].DBO.HC_Prospect_Rating_v
	SET PRateSource='NULL'
	WHERE PRateSource IS NULL 

	UPDATE [SUTTER_1P_DATA].DBO.HC_Prospect_Rating_v
	SET PRateDesc='NULL'
	WHERE PRateDesc IS NULL 
END 