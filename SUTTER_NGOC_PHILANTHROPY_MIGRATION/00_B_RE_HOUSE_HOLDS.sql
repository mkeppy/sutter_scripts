	
	/*
		USE SUTTER_1P_MIGRATION
		GO
		CREATE SCHEMA TBL
		GO
		CREATE SCHEMA IMP
		GO
		CREATE SCHEMA XTR
		GO
		
		
	*/
	
	USE SUTTER_1P_DATA
	GO

	/*
		DROP TABLE SUTTER_1P_MIGRATION.tbl.Account_Org
		GO
		DROP TABLE SUTTER_1P_MIGRATION.tbl.Contact_HofH
		GO
		 
	*/
	
 

	--A_Organization Accounts
			--A_tbl_Account_Org:  Constituents that are Organization Constituents;  58,821  (expected: 55,799)
				SELECT DISTINCT T.KeyInd,  T.MDM_PTY_ID 
				,CASE WHEN T.NEW_TGT_IMPORT_ID IS NULL THEN T.RE_DB_id ELSE T.NEW_TGT_IMPORT_ID END AS NEW_TGT_IMPORT_ID
				INTO SUTTER_1P_MIGRATION.tbl.Account_Org
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T
				WHERE T.KeyInd='O' 
				GROUP BY T.KeyInd, T.MDM_PTY_ID, T.NEW_TGT_IMPORT_ID, T.RE_DB_id
				GO
			 	
					--CHECK_DUPES
					SELECT * FROM SUTTER_1P_MIGRATION.tbl.Account_Org
					WHERE NEW_TGT_IMPORT_ID IN (SELECT NEW_TGT_IMPORT_ID FROM SUTTER_1P_MIGRATION.tbl.Account_Org GROUP BY NEW_TGT_IMPORT_ID HAVING COUNT(*)>1)
					ORDER BY NEW_TGT_IMPORT_ID



	--_B_C_D Head of Household ACcounts/Contacts
	
			-- DROP TABLE SUTTER_1P_MIGRATION.tbl.Contact_HofH
			
			--B_tbl_Contact_Cons_HofH:  Constituents that are Head of Household Constituents only;
				SELECT T.RE_DB_OwnerShort, T.KeyInd, T.ImportID,  T.ConsID, T.Deceased,  -- hh
				T3.ImportID AS NonHHImpID, T3.ConsID AS NonHHConsID, T3.Deceased NonHHDeceased,		--non-hh
				T2.IRLink, T2.IRIsHH, T2.IRIsSpouse, T2.IRImpID, T2.RE_DB_OwnerShort AS IR_RE_DB_OwnerShort,
				CAST('' AS nvarchar(20)) AS HH_ImportID, 
				CAST('' AS nvarchar(20)) AS HH_ConsID, 
				CAST('' AS nvarchar(20)) AS NoHH_ImportID, 
				CAST('' AS nvarchar(20)) AS NoHH_ConsID
				
				INTO SUTTER_1P_MIGRATION.tbl.Contact_HofH
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Ind_Relat_v T2 ON T.ImportID = T2.IRLink AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T3 ON T2.ImportID=T3.ImportID AND T2.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
				WHERE (((T.KeyInd)='I') AND ((T2.IRIsHH)='TRUE') AND ((T2.IRIsSpouse)='TRUE'))   
				ORDER BY T.ImportID
				GO
 



			--C_tbl_Contact_Cons_NonConsSpouse; Constituents with non-cons Ind Relat Spouse
				INSERT INTO SUTTER_1P_MIGRATION.tbl.Contact_HofH (RE_DB_OwnerShort, KeyInd, ImportID, ConsID, Deceased, NonHHImpID, NonHHConsID, NonHHDeceased, 
														  IRLink, IRIsHH, IRIsSpouse, IRImpID, IR_RE_DB_OwnerShort, 
														  HH_ImportID, HH_ConsID, NoHH_ImportID, NoHH_ConsID)
				SELECT T.RE_DB_OwnerShort, T.KeyInd,  T.ImportID, T.ConsID, T.Deceased, 
				T2.ImportID AS IRNonHHImpID, T2.ConsID NonHHConsID,  T2.IRDeceased AS NonHHDeceased, 
				'' AS IRLink, T2.IRIsHH, T2.IRIsSpouse, T2.IRImpID, T2.RE_DB_OwnerShort AS IR_RE_DB_OwnerShort,
				NULL AS HH_ImportID, null AS HH_ConsID, null AS NoHH_ImportID, null AS NoHH_ConsID
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T
				LEFT JOIN SUTTER_1P_DATA.dbo.HC_Ind_Relat_v T2 ON T.ImportID = T2.ImportID AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T3 ON T2.ImportID=T3.ImportID  AND T2.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
				GROUP BY T.KeyInd, T2.IRIsHH, T.ImportID,  T.RE_DB_OwnerShort, T.ConsID,  T.Deceased, T2.ImportID, T2.ConsID, T2.IRDeceased, T2.IRLink, T2.IRIsSpouse, T2.IRImpID, T2.RE_DB_OwnerShort
				HAVING (((T.KeyInd)='I') AND ((T2.IRLink) Is Null Or (T2.IRLink)='') AND ((T2.IRIsSpouse)='TRUE'))
				ORDER BY T.ImportID
				GO
			
			--D_tbl_Contact_Cons_WNoSpouse: all other non-cons Ind Relat; Ind Constituents with no HofH constituents/relationships;  Ind Constituents with no non-HofH constituents/relationships; 
				INSERT INTO SUTTER_1P_MIGRATION.tbl.Contact_HofH (RE_DB_OwnerShort, KeyInd, ImportID, ConsID, Deceased, NonHHImpID, NonHHConsID, 
				NonHHDeceased, IRLink, IRIsHH, IRIsSpouse, IRImpID, IR_RE_DB_OwnerShort,
				HH_ImportID, HH_ConsID, NoHH_ImportID, NoHH_ConsID)
				SELECT T.RE_DB_OwnerShort, T.KeyInd, T.ImportID, T.ConsID, T.Deceased,
				T3.NonHHImpID, T3.NonHHConsID, '' AS NonHHDeceased, '' AS IRLink, 'TRUE' AS IRIsHH, 
				'' AS IRIsSpouse, '' AS IRImpID, T3.RE_DB_OwnerShort AS IR_RE_DB_OwnerShort,
				NULL AS HH_ImportID, null AS HH_ConsID, null AS NoHH_ImportID, null AS NoHH_ConsID
				FROM (SUTTER_1P_DATA.dbo.HC_Constituents_v T
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH AS T2 ON T.ImportID = T2.ImportID  AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort) 
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH AS T3 ON T.ImportID = T3.NonHHImpID  AND T.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
				GROUP BY T.KeyInd, T.ImportID, T.RE_DB_OwnerShort, T.ConsID, T.Deceased, T3.NonHHImpID, T3.NonHHConsID, T2.ImportID, T3.RE_DB_OwnerShort
				HAVING (((T.KeyInd)='I') AND ((T3.NonHHImpID) Is Null) AND ((T2.ImportID) Is Null))
				GO
			 

			--_E_Non Head of Household constituents. 	
			/* REPLACED BY UPDATES ON THE HH TABLE.  MAY ONLY USE HH TABLE OR CREATE A NEW SUB TABLE FROM HH AND CALL IT THE SAEM AS THE NOHH 
					-- DROP TABLE SUTTER_1P_MIGRATION.tbl.Contact_NoHofH	
					--E_tbl_Contact_Cons_NonHofH; Constituents NON-head of Household (to be combined into Head of Household Account. 
						SELECT T2.IRIsHH, T.KeyInd, T.ConsID, T.ImportID, T.RE_DB_OwnerShort, T.Deceased, T2.IRLink, T2.IRImpID, T2.RRImpID, T2.IRISSpouse, T2.RE_DB_OwnerShort AS IR_RE_DB_OwnerShort, 
						T3.ConsID AS HH_ConsID, T3.ImportID AS HH_ImportID , T3.Deceased HH_Deceased, T3.RE_DB_OwnerShort AS HH_RE_DB_OwnerShort
						--INTO SUTTER_1P_MIGRATION.tbl.Contact_NoHofH
						FROM (SUTTER_1P_DATA.dbo.HC_Constituents_v AS T
						INNER JOIN SUTTER_1P_DATA.dbo.HC_Ind_Relat_v AS T2 ON T.ImportID = T2.ImportID) 
						INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v AS T3 ON T2.IRLink = T3.ImportID
						GROUP BY T2.IRIsHH, T.KeyInd, T.ConsID, T.ImportID, T.RE_DB_OwnerShort, T.Deceased, T2.IRLink, T2.IRImpID, T2.RRImpID, T2.IRISSpouse, T2.RE_DB_OwnerShort, 
						T3.ConsID, T3.ImportID, T3.Deceased, T3.RE_DB_OwnerShort
						HAVING (((T2.IRIsHH)='TRUE') AND ((T.KeyInd)='I'))
						ORDER BY T.ConsID
						GO
						--832	 
				*/
	--UPDATE HofH constituent for Account, and Contact Primary and Secondary. 
				
			SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
			WHERE importid!=NonHHImpID 			 
				
			--HH update 	
			UPDATE SUTTER_1P_MIGRATION.tbl.Contact_HofH
			SET HH_ImportID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN NonHHImpID ELSE ImportID END WHERE importid!=NonHHImpID 
			GO
			UPDATE SUTTER_1P_MIGRATION.tbl.Contact_HofH
			SET HH_ConsId= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN NonHHConsID ELSE ConsId END WHERE importid!=NonHHImpID 
			GO
			UPDATE SUTTER_1P_MIGRATION.tbl.Contact_HofH 
			SET HH_ImportID=ImportID WHERE HH_ImportID IS null
			GO
			UPDATE SUTTER_1P_MIGRATION.tbl.Contact_HofH 
			SET HH_ConsId=ConsId WHERE HH_ConsID IS null
			GO
			--Non-HH update
			UPDATE SUTTER_1P_MIGRATION.tbl.Contact_HofH
			SET NoHH_ImportID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN ImportID ELSE NonHHImpID END WHERE importid!=NonHHImpID 
			GO
			UPDATE SUTTER_1P_MIGRATION.tbl.Contact_HofH
			SET NoHH_ConsID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN ConsID ELSE NonHHConsID END WHERE importid!=NonHHImpID 
			GO
	
	
			 
				
		   
	--total (A) Cons Org:		 58,821
	--total (B_C_D) Cons HofH :  
		--B:    17609
		--C:    211629
		--D:   668879
	--total (E) Cons NonHoH:         
	-----------------------------------
	--total HC_Constituent_v: 974,547   
	  
	--check count: SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.HC_Constituents_v -- 974,547

	--SELECT T.ImportID, T.KeyInd, dbo.AccRecordType(T.ImportID) RecordTypeID FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T 
	--ORDER BY t.KeyInd desc

			 
 --account active test
		SELECT Deceased, NonHHDeceased, 
		CASE WHEN Deceased='true' AND NonHHDeceased='true' THEN 'FALSE'  
		WHEN Deceased='true' AND NonHHDeceased=''  THEN 'FALSE'
		ELSE 'TRUE'
		END AS ACCOUNT_ACTIVE,
		CASE WHEN Deceased='true' THEN 'FALSE' ELSE 'TRUE' END AS PreferredContact,
		COUNT(*) c
		FROM SUTTER_1P_MIGRATION.TBL.Contact_HofH
		GROUP BY Deceased, NonHHDeceased
		ORDER BY Deceased, NonHHDeceased 
		
		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
		WHERE RE_DB_OwnerShort+'-'+ImportId IN (SELECT RE_DB_OwnerShort+'-'+ImportId  FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH GROUP BY RE_DB_OwnerShort+'-'+ImportId HAVING COUNT(*)>1)
		ORDER BY RE_DB_OwnerShort+'-'+ImportId 

 