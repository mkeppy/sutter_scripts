USE [SUTTER_1P_MIGRATION]
GO

BEGIN	
	DROP TABLE SUTTER_1P_MIGRATION.IMP.BOARD_MEMBER
END

BEGIN
				--BOARD MEMBER from CONSTITUENT ATTRIBUTE from CHART_CONS_ATTRIBUTES 
				SELECT DISTINCT
				 External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.CAttrImpID)
				,[Account__r:External_Id__c]= CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.HH_ImportID ELSE T1.NEW_TGT_IMPORT_ID END
				,[Contact__r:External_Id__c]=CASE WHEN T1.KeyInd='I' THEN T1.NEW_TGT_IMPORT_ID ELSE NULL END
				,T2.BOARDMEMBER_Active__c AS Active__c
				,T2.BOARDMEMBER_Type__c	 AS Type__c
				,T2.BOARDMEMBER_Notes__c AS Notes__c
				,T2.BOARDMEMBER_Committee__c AS Committee__c
				,CASE WHEN T.CAttrDate IS NULL THEN T2.BOARDMEMBER_StartDate__c ELSE T.CAttrDate END AS Start_Date__c
				,T2.BOARDMEMBER_EndDate__c  AS End_Date__c
				,T.[RE_DB_OwnerShort] AS Affiliation__c
				,'Cons Attributes' AS Origin__c
				--reference
				,T3.NoHH_ImportID zref_NoHH_ImportID
				,T3.HH_ImportID AS zref_HH_ImportID 
				,T1.KeyInd AS zrefKeyInd
				
		 	 	INTO SUTTER_1P_MIGRATION.IMP.BOARD_MEMBER
				FROM SUTTER_1P_DATA.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.RE_DB_ID=T1.RE_DB_ID 
				INNER JOIN SUTTER_1P_DATA.dbo.CHART_ConsAttributes T2 ON T.CAttrCat=T2.CAttrCat AND T.CAttrDesc=T2.CAttrDesc AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T3 ON T1.NEW_TGT_IMPORT_ID=T3.NoHH_ImportID  
				WHERE T2.BOARDMEMBER_Active__c IS NOT NULL
		 
			UNION ALL 
				--board members from cons codes 
				SELECT DISTINCT
		 		 External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.ConsCodeImpID)
				,[Account__r:External_Id__c]=CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.HH_ImportID ELSE T1.NEW_TGT_IMPORT_ID END
				,[Contact__r:External_Id__c]=CASE WHEN T1.KeyInd='I' THEN T1.NEW_TGT_IMPORT_ID ELSE NULL END
				,T2.BOARDMEMBER_Active__c AS Active__c
				,T2.BOARDMEMBER_Type__c	 AS Type__c
				,T2.BOARDMEMBER_Notes__c AS Notes__c
				,T2.BOARDMEMBER_Committee__c AS Committee__c
				,CASE WHEN T.ConsCodeDateFrom IS NULL AND T2.BOARDMEMBER_StartDate__c IS NOT null THEN T2.BOARDMEMBER_StartDate__c ELSE T.ConsCodeDateFrom END AS Start_Date__c
				,CASE WHEN T.ConsCodeDateto IS NULL AND T2.BOARDMEMBER_EndDate__c IS NOT NULL THEN T2.BOARDMEMBER_EndDate__c ELSE T.ConsCodeDateTo end  AS End_Date__c
				,T.[RE_DB_OwnerShort] AS Affiliation__c
				,'Cons Codes' AS Origin__c
				--reference
				,T3.NoHH_ImportID zref_NoHH_ImportID
				,T3.HH_ImportID AS zref_HH_ImportID 
				,T1.KeyInd AS zrefKeyInd
				FROM SUTTER_1P_DATA.dbo.HC_Cons_ConsCode_v T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.RE_DB_ID=T1.RE_DB_ID
				INNER JOIN SUTTER_1P_DATA.dbo.CHART_ConstituencyCodes T2 ON T.ConsCode=T2.ConsCode AND T1.KeyInd=T2.KeyInD AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T3 ON T1.NEW_TGT_IMPORT_ID=T3.NoHH_ImportID
				WHERE T2.BOARDMEMBER_Type__c IS NOT NULL
				 4672
END;				

BEGIN--check double qoutes. 
		USE [SUTTER_1P_MIGRATION] 
 		EXEC sp_FindStringInTable '%"%', 'IMP', 'BOARD_MEMBER'
END 

BEGIN--CHECK DUPLICATES
		SELECT * 
		FROM SUTTER_1P_MIGRATION.IMP.BOARD_MEMBER
		WHERE External_ID__c IN (SELECT External_ID__c FROM SUTTER_1P_MIGRATION.IMP.BOARD_MEMBER GROUP BY External_ID__c HAVING COUNT(*)>1)
END

BEGIN --UPdATE - check dates

	 SELECT Start_Date__c, LEN(Start_Date__c) lsd
	 FROM SUTTER_1P_MIGRATION.IMP.BOARD_MEMBER
	 GROUP BY Start_Date__c
	 ORDER BY lsd
 
	 SELECT End_Date__c, LEN(End_Date__c) lsd
	 FROM SUTTER_1P_MIGRATION.IMP.BOARD_MEMBER
	 GROUP BY End_Date__c
	 ORDER BY lsd

		UPDATE SUTTER_1P_MIGRATION.IMP.BOARD_MEMBER
		SET Start_Date__c='01/01/'+Start_Date__c 
		WHERE Start_Date__c IS NOT NULL AND LEN(Start_Date__c) =4

		UPDATE SUTTER_1P_MIGRATION.IMP.BOARD_MEMBER
		SET End_Date__c='01/01/'+End_Date__c 
		WHERE End_Date__c IS NOT NULL AND LEN(End_Date__c) =4

		UPDATE SUTTER_1P_MIGRATION.IMP.BOARD_MEMBER
		SET Start_Date__c=LEFT(Start_Date__c,2)+'/01/'+RIGHT(Start_Date__c,4) 
		WHERE Start_Date__c IS NOT NULL AND LEN(Start_Date__c) =7

		UPDATE SUTTER_1P_MIGRATION.IMP.BOARD_MEMBER
		SET End_Date__c=LEFT(End_Date__c,2)+'/01/'+RIGHT(End_Date__c,4) 
		WHERE End_Date__c IS NOT NULL AND LEN(End_Date__c) =7


 END
 
  
  