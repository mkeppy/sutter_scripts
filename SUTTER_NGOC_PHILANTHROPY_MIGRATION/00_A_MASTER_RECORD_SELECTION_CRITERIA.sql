USE SUTTER_1P_DATA
GO

	--CREATE SCHEMA TBL
	--GO
--check for the RE_DB_Id field in core tables, i.e. constituent, gift, actions, proposals.  The RE_DB_Id = RE_DB_Ownershort + ImportID. 


	SELECT * FROM SUTTER_1P_DATA.dbo.HC_Constituents_v 
	WHERE [RE_DB_Id] IN (SELECT re_db_id 
						FROM SUTTER_1P_DATA.dbo.HC_Constituents_v 
						GROUP BY re_db_id
						HAVING COUNT(*)>1)
	ORDER BY [RE_DB_Id] 
			
	DELETE [SUTTER_1P_DATA].dbo.[HC_Constituents_v] 
	WHERE [AddrImpID]='04121-516-0000088442' AND re_db_id='SAFH-04121-593-0000033521'	 

	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Constituents_v] WHERE re_db_id='SAFH-04121-593-0000033521'	 

		
BEGIN--Remove table to start over
            IF EXISTS ( SELECT  *
                        FROM    dbo.sysobjects
                        WHERE   id = OBJECT_ID(N'[SUTTER_1P_DATA].[TBL].[MASTER_RECORD_CRITERIA]')
                                AND OBJECTPROPERTY(id, N'IsTable') = 1 )
                DROP TABLE [SUTTER_1P_DATA].[TBL].MASTER_RECORD_CRITERIA; 
END;	

BEGIN--CREATE TBL.MASTER_RECORD_CRITERIA
		--CRITERIA 01: SH: If there is an (Active OR Inactive Board Member) that is also an (Active Employee or Active Physician) 
				SELECT  DISTINCT CAST('1' AS INT) AS SCORE, 'C_01' AS CRITERIA, T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportID, T1.RE_DB_Id
				INTO SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA
				FROM SUTTER_1P_DATA.dbo.HC_Cons_ConsCode_v T   
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.RE_DB_ID=T1.[RE_DB_Id]
				INNER JOIN SUTTER_1P_DATA.dbo.CHART_ConstituencyCodes T2 ON T.ConsCode=T2.ConsCode AND T1.KeyInd=T2.KeyInD AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				INNER JOIN (SELECT DISTINCT sT1.[RE_DB_Id]		--board members from ConsAttr Active or Inactive
								FROM SUTTER_1P_DATA.dbo.HC_Cons_Attributes_v sT
								INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v sT1 ON sT.RE_DB_ID=sT1.RE_DB_ID 
								INNER JOIN SUTTER_1P_DATA.dbo.CHART_ConsAttributes sT2 ON sT.CAttrCat=sT2.CAttrCat AND sT.CAttrDesc=sT2.CAttrDesc AND sT.RE_DB_OwnerShort=sT2.RE_DB_OwnerShort
								WHERE sT2.BOARDMEMBER_Active__c IS NOT NULL
								UNION ALL 
								SELECT DISTINCT [sT1].[RE_DB_Id]		--board members from ConsCodes Active or Inactive
								FROM SUTTER_1P_DATA.dbo.HC_Cons_ConsCode_v sT   
								INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v sT1 ON sT.RE_DB_ID=sT1.RE_DB_ID
								INNER JOIN SUTTER_1P_DATA.dbo.CHART_ConstituencyCodes sT2 ON sT.ConsCode=sT2.ConsCode AND sT1.KeyInd=sT2.KeyInD AND sT.RE_DB_OwnerShort=sT2.RE_DB_OwnerShort
 								WHERE sT2.BOARDMEMBER_Type__c IS NOT NULL
							) sq ON T1.[RE_DB_Id] = sq.RE_DB_Id
				WHERE ((T2.[PREFERENCE_Subcategory]='Employee' OR T2.[PREFERENCE_Subcategory]='Physician') AND (T2.[PREFERENCE_Active]='TRUE'))		
		--CRITERIA 02: SH: If there is an active Staff Solicitor, use the record with the solicitor assigned.
				INSERT INTO SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA (SCORE, CRITERIA, RE_DB_OwnerShort, KeyInd, ImportID, RE_DB_Id) 
				SELECT DISTINCT '1' SCORE, 'C_02' AS CRITERIA, T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportID, T1.RE_DB_Id 
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Cons_Solicitor_v T2 ON T1.RE_DB_Id=T2.RE_DB_Id  
				WHERE T2.ASRType LIKE 'Staff solicitor%' AND T2.ASRDateTo IS NULL
		--CRITERIA 03: SH: If there is a record with an open pledge, use the record with the open pledge.
				INSERT INTO SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA (SCORE, CRITERIA, RE_DB_OwnerShort, KeyInd, ImportID, RE_DB_Id) 
				SELECT DISTINCT '1' SCORE, 'C_03' AS CRITERIA, T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportID, T1.RE_DB_Id
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Gift_v T2 ON T1.RE_DB_Id=T2.RE_DB_Id  
				WHERE CAST(T2.GFPledgeBalance AS DECIMAL)> 0
		--CRITERIA 04: SH: If there is a gift of $5,000 or more in the last two years, use this record over more recent smaller contributions.
				INSERT INTO SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA (SCORE, CRITERIA, RE_DB_OwnerShort, KeyInd, ImportID, RE_DB_Id) 
				SELECT DISTINCT '1' SCORE, 'C_04' AS CRITERIA, T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportID, T1.RE_DB_Id 
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Gift_v T2 ON T1.RE_DB_Id=T2.RE_DB_Id  
				WHERE CAST(T2.GFTAmt  AS DECIMAL)>= 5000 AND T2.GFDate>='01/01/2014'
		--CRITERIA 05: SH: If there is no gift over $5,000, but there is an open proposal or solicitation action, use the record with the open proposal or solicitation.
				INSERT INTO SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA (SCORE, CRITERIA, RE_DB_OwnerShort, KeyInd, ImportID, RE_DB_Id) 
				SELECT DISTINCT '1' SCORE, 'C_05' AS CRITERIA, T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportID, T1.RE_DB_Id
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
				LEFT JOIN SUTTER_1P_DATA.dbo.HC_Proposal_v T2 ON T1.RE_DB_Id=T2.RE_DB_Id 
				LEFT JOIN SUTTER_1P_DATA.dbo.HC_Cons_Action_v T3 ON T1.RE_DB_Id=T3.RE_DB_Id  
				WHERE ((T2.PRDateFund IS NULL AND (CAST(T2.DateAdded AS DATE)>='01/01/2014')) OR (T3.ACType = 'Solicitation' AND T3.ACComplete='FALSE'))
				AND T1.RE_DB_Id NOT IN (SELECT RE_DB_Id FROM SUTTER_1P_DATA.dbo.hc_gift_v WHERE GFTAmt >=5000)
				ORDER BY T1.ImportID
		--CRITERIA 06: SH: If there is no gift over $5,000 and no current solicitation, use the most recent gift.
				INSERT INTO SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA (SCORE, CRITERIA, RE_DB_OwnerShort, KeyInd, ImportID, RE_DB_Id) 
				SELECT DISTINCT '1' SCORE, 'C_06' AS CRITERIA, T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportID, T1.RE_DB_Id   
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
				LEFT JOIN SUTTER_1P_DATA.DBO.HC_GIFT_V T2 ON  T1.RE_DB_Id=T2.RE_DB_Id  
				WHERE T1.RE_DB_Id NOT IN (SELECT RE_DB_Id FROM SUTTER_1P_DATA.dbo.hc_gift_v WHERE GFTAmt >=5000)
				AND T1.RE_DB_Id NOT IN (SELECT RE_DB_Id FROM SUTTER_1P_DATA.dbo.HC_Cons_Action_v WHERE ACType = 'Solicitation' AND ACComplete='FALSE')
				AND T2.GFDate >='01/01/2014'
 		--CRITERIA 07: If there are none of the above except other types of actions, use the record with the actions. 
				INSERT INTO SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA (SCORE, CRITERIA, RE_DB_OwnerShort, KeyInd, ImportID, RE_DB_Id) 
				SELECT DISTINCT '1' SCORE, 'C_07' AS CRITERIA, T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportID, T1.RE_DB_Id 
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Cons_Action_v T2 ON T1.RE_DB_Id=T2.RE_DB_Id
				LEFT JOIN SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA T3 ON T1.RE_DB_Id=T3.RE_DB_Id	/*needed to create this table first*/
				WHERE T3.RE_DB_Id IS NULL 
		--CRITERIA 08: 	If none of the above exists, look for indicators of prospect research and use the record with research information (located in attributes OR the prospect tab).
				INSERT INTO SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA (SCORE, CRITERIA, RE_DB_OwnerShort, KeyInd, ImportID, RE_DB_Id) 
				SELECT DISTINCT '1' SCORE, 'C_08' AS CRITERIA, T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportID, T1.RE_DB_Id 
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
				LEFT JOIN SUTTER_1P_DATA.DBO.HC_Prospect_Rating_v T2 ON T1.RE_DB_Id=T2.RE_DB_Id
				LEFT JOIN SUTTER_1P_DATA.DBO.HC_Cons_Attributes_v T3 ON T1.RE_DB_Id=T3.RE_DB_Id
				LEFT JOIN SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA T4 ON T1.RE_DB_Id=T4.RE_DB_Id
				WHERE ((T3.CAttrCat LIKE '%research%'  OR T3.CAttrCat LIKE 'WP%' OR T3.CAttrCat LIKE 'WE%') OR (T2.ImportID IS NOT NULL))
				AND T4.RE_DB_Id IS null
 		--CRITERIA 09: If nothing exists on the records, then use the record with the latest date changed or entered.
				INSERT INTO SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA (SCORE, CRITERIA, RE_DB_OwnerShort, KeyInd, ImportID, RE_DB_Id) 
				SELECT DISTINCT '1' SCORE, 'C_09' AS CRITERIA, T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportID, T1.RE_DB_Id 
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
				LEFT JOIN SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA T2 ON T1.RE_DB_Id=T2.RE_DB_Id
				WHERE T2.RE_DB_Id IS NULL AND CAST(T1.DateAdded AS DATE)>='01/01/2014'
		--CRITERIA 10: Non-Deceased (in the event that more than 1 duplicates don't meet any of the criteria abvove,
						 --then we need to look at "Deceased" and proioritice non-deceased constituent.  
				INSERT INTO SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA (SCORE, CRITERIA, RE_DB_OwnerShort, KeyInd, ImportID, RE_DB_Id) 
				SELECT DISTINCT '1' SCORE, 'C_10' AS CRITERIA, T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportID, T1.RE_DB_Id 
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
				LEFT JOIN SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA T2 ON T1.RE_DB_Id=T2.RE_DB_Id
				WHERE T2.RE_DB_Id IS NULL AND T1.[Deceased]='FALSE' 
		--CRITERIA 11: Otherwise, use the MDM-identified Master Record.
				INSERT INTO SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA (SCORE, CRITERIA, RE_DB_OwnerShort, KeyInd, ImportID, RE_DB_Id) 
				SELECT DISTINCT '1' SCORE, 'C_11' AS CRITERIA, T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportID, T1.RE_DB_Id 
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
				LEFT JOIN SUTTER_1P_DATA.DBO.NGOC_1P_FINAL_EXTRACT T2 ON T1.[RE_DB_Id]=T2.[TGT_IMPORT_ID]
				LEFT JOIN SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA T3 ON T1.RE_DB_Id=T3.RE_DB_Id
				WHERE (T2.[TGT_IMPORT_ID]!='' AND T2.[TGT_IMPORT_ID] IS NOT null) AND T3.RE_DB_Id IS NULL 
		--CRITERIA 12: Everyone else 
				INSERT INTO SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA (SCORE, CRITERIA, RE_DB_OwnerShort, KeyInd, ImportID, RE_DB_Id) 
				SELECT DISTINCT '1' SCORE, 'C_12' AS CRITERIA, T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportID, T1.RE_DB_Id 
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
				LEFT JOIN SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA T2 ON T1.RE_DB_Id=T2.RE_DB_Id
				WHERE T2.RE_DB_Id IS NULL  
END; --end of criteria 

			
				/*DISREGARD OLD CRITERIA
				--CRITERIA 6: If there are Ambassador/Friends & Family/PAS actions or memberships without any solicitors or gifts � 
					-- USE the record with the most recent Ambassador/PAS action.
			
					INSERT INTO SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA (SCORE, CRITERIA, RE_DB_OwnerShort, KeyInd, ImportID, RE_DB_Id) 
					SELECT DISTINCT '1' SCORE, 'C_6' AS CRITERIA, T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportID, T1.RE_DB_Id  
					FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T1
					LEFT JOIN SUTTER_1P_DATA.DBO.HC_Cons_Action_v T2 ON T1.RE_DB_Id=T2.RE_DB_Id
					WHERE T1.RE_DB_Id NOT IN (SELECT RE_DB_Id FROM SUTTER_1P_DATA.dbo.hc_gift_v WHERE GFTAmt >0)
					AND T1.RE_DB_Id NOT IN (SELECT RE_DB_Id FROM SUTTER_1P_DATA.DBO.HC_Cons_Solicitor_v)
					AND T2.ACType LIKE '%amb%'
				*/

		
--test	
			SELECT * FROM SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA 
			WHERE  RE_DB_ID='WBRX-05502-079-0000149431'	 
			ORDER BY [RE_DB_Id] 
			
			DELETE [SUTTER_1P_DATA].dbo.[HC_Constituents_v] 
			WHERE [AddrImpID]='05502-516-0000717325' AND re_db_id='WBRX-05502-079-0000149431'	 

 
			--missing record??
			SELECT T1.ConsID, T1.RE_DB_Id 
			FROM SUTTER_1P_DATA.dbo.HC_Constituents_v t1
			LEFT JOIN SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA t2 ON t1.re_db_id=t2.re_db_id
			WHERE t2.re_db_id ='' OR t2.re_db_id IS NULL 
            
		  	--dupes
		   	SELECT * FROM SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA 
			WHERE  RE_DB_ID	 IN (SELECT RE_DB_ID FROM SUTTER_1P_DATA.TBL.MASTER_RECORD_CRITERIA  GROUP BY RE_DB_ID HAVING COUNT(*)>1)
			ORDER BY RE_DB_ID, criteria
