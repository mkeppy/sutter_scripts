
--CHECK FOR EMP CONS CODES TO ADD TO ATTRIBUTESS
	SELECT [RE_DB_OwnerShort], [GFType], [GFConsDesc], COUNT(*) C
	FROM [SUTTER_1P_DATA].dbo.[HC_Gift_v]
	WHERE [GFType] LIKE '%recurring%' AND ([GFConsDesc] LIKE '%empl%' OR [GFConsDesc] LIKE '%staff%')
	GROUP BY [RE_DB_OwnerShort], [GFType],  [GFConsDesc]
	order BY [RE_DB_OwnerShort], [GFType],  [GFConsDesc]

	'Employee Recurring Gift' = 'TRUE'


	SELECT [GFType], COUNT(*) FROM [SUTTER_1P_DATA].dbo.[HC_Gift_v]
	WHERE [GFType] LIKE '%recurring%' AND ([GFConsDesc] LIKE '%empl%' OR [GFConsDesc] LIKE '%staff%')
	GROUP BY [GFType]

	--check attr
	SELECT	[GFImpID], 
			NULL AS GFAttrImpID, 
			'Employee Recurring Gift' AS GFAttrCat,
			
			[GFType]+'-'+ [GFConsDesc] AS GFAttrCom,
			NULL AS GFAttrDate,
			'TRUE' AS GFAttrDesc,
			NULL AS DataType,
			NULL AS RE_DB_Owner,
			[RE_DB_OwnerShort],
			'GA' AS RE_DB_Tbl
	FROM [SUTTER_1P_DATA].dbo.[HC_Gift_v]
	WHERE [GFType] LIKE '%recurring%' AND ([GFConsDesc] LIKE '%empl%' OR [GFConsDesc] LIKE '%staff%')
	ORDER BY [RE_DB_OwnerShort], [GFType],  [GFConsDesc]

	--add attribute to table 
	INSERT INTO  [SUTTER_1P_DATA].dbo.[HC_Gift_Attr_v]
	([GFImpID], GFAttrImpID, GFAttrCat, GFAttrCom, [GFAttrDate], [GFAttrDesc], [DataType], [RE_DB_Owner], [RE_DB_OwnerShort], [RE_DB_Tbl])
	SELECT	[GFImpID], 
			NULL AS GFAttrImpID, 
			'Employee Recurring Gift' AS GFAttrCat,
	 		[GFType]+'-'+ [GFConsDesc] AS GFAttrCom,
			NULL AS GFAttrDate,
			'TRUE' AS GFAttrDesc,
			NULL AS DataType,
			NULL AS RE_DB_Owner,
			[RE_DB_OwnerShort],
			'GA' AS RE_DB_Tbl
	FROM [SUTTER_1P_DATA].dbo.[HC_Gift_v]
	WHERE [GFType] LIKE '%recurring%' AND ([GFConsDesc] LIKE '%empl%' OR [GFConsDesc] LIKE '%staff%')
	
	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Attr_v] WHERE [GFAttrCat]='Employee Recurring Gift'