USE [SUTTER_1P_MIGRATION]
GO

BEGIN
	DROP TABLE SUTTER_1P_MIGRATION.IMP.PLANNED_GIVING
END 
 
BEGIN--PLANNED GIVING
		 		SELECT  
					--IDs
						dbo.fnc_OwnerID() AS OwnerID  
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS RE_GSplitImpID__c
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitGFImpID AS RE_GFImpID__c
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS rC_Giving__External_ID__c
						,T.GFVehicle +': '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10)) Name       
						,T.[GSplitAmt] AS rC_Giving__Giving_Amount__c
						,T.[GSplitAmt] AS rC_Giving__Estimated_Giving_Amount__c
/*EXT_ID*/				,T.CAMPAIGN_External_Id__c	AS [rC_Giving__Campaign__r:External_ID__c]						
/*EXT_ID*/				,T.[ACCOUNT:External_Id__c]	AS [rC_Giving__Account__r:External_ID__c]					
						,T.[GFDate] AS rC_Giving__Date_Station_Notified_Of_Intention__c
						,T.refNewImportID AS [rC_Giving__Contact__r:External_ID__c]

						,NULL AS Proposal_Date_Asked__c
/*EXT_ID*/				,T.GAU_External_Id__c	AS [General_Accounting_Unit__r:rC_Giving__External_ID__c]
						,NULL AS Proposal_Purpose__c
						,NULL AS Proposal_Rating__c
						,NULL AS Proposal_Reason__c

						,CASE WHEN T.GFPGStatus IS NULL OR T.GFPGStatus ='' THEN 'Intention' ELSE T.GFPGStatus END AS rC_Giving__Stage__c
						,T.DateAdded	AS	CreatedDate
						,T.[GSplitRE_DB_OwnerShort] AS rC_Giving__Affiliation__c 
						,CASE T.GFVehicle 
							WHEN 'Bequest'					THEN dbo.fnc_RecordType('Planned_Giving_Bequest') 
							WHEN 'Gift Annuity'				THEN dbo.fnc_RecordType('Planned_Giving_Charitable_Gift_Annuity')
							WHEN 'Lead Unitrust'			THEN dbo.fnc_RecordType('Planned_Giving_Charitable_Lead_Unitrust')
							WHEN 'Life Insurance'			THEN dbo.fnc_RecordType('Planned_Giving_Life_Insurance')
							WHEN 'Other Planned Gift'		THEN dbo.fnc_RecordType('Planned_Giving_Bequest')
							WHEN 'Pooled Income Fund'		THEN dbo.fnc_RecordType('Planned_Giving_Pooled_Income_Fund')
							WHEN 'Remainder Annuity Trust'	THEN dbo.fnc_RecordType('Planned_Giving_Charitable_Remainder_Annuity_Trust')
							WHEN 'Remainder Unitrust'		THEN dbo.fnc_RecordType('Planned_Giving_Charitable_Remainder_Unitrust')
							WHEN 'Retained Life Estate'		THEN dbo.fnc_RecordType('Planned_Giving_Retained_Life_Estate')
						  END AS RecordTypeID  

						,NULL AS rC_Giving__Rollup_Giving__c
		  
					--gift attribute (chart_giftattribute)
						,T2.[Considering bequest in] AS Considering_bequest_in__c
						,T2.[Production] AS Production__c
						,T2.[Report Exclusion] AS Report_Exclusion__c
						,T2.[Source of notification] AS Source_of_notification__c
					--MISC.
						,T.RE_Split_Gift__c
						,T.GFType AS RE_Gift_Type__c
						,T.RE_Gift_SubType__c
						,T.GFPostDate__c AS Post_Date__c
						,T.GFPostStatus__c AS Post_Status__c
						,T.GFConsDesc AS RE_Gift_Constituency_Code__c
						,T.GFPayMeth AS RE_Gift_Payment_Method__c
						,T.GFRef AS rC_Giving__Notes__c
						,T.GFTRealized AS RE_Gift_Realized__c	
						,T.RE_Legacy_Financial_Code__c

					--reference
					,'RE Gift' AS zrefSrc
					,T.ImportID AS zref_ImportID	
					,T.HH_ImportID AS zref_NoHH_ImportID	
					,T.NoHH_ImportID AS zref_HH_ImportID 	
					,NULL	 AS zrefPRImpID
					,LEFT(T.RE_GFSplitImportID_ID,4) AS zrefRE_DB_OwnerShort	
 					INTO [SUTTER_1P_MIGRATION].[IMP].[PLANNED_GIVING] 
					FROM SUTTER_1P_MIGRATION.TBL.GIFT T
					--LEFT JOIN SUTTER_1P_MIGRATION.XTR.ACCOUNT T1 ON T.[ACCOUNT:External_Id__c]=T1.EXTERNAL_ID__C 
					LEFT JOIN SUTTER_1P_MIGRATION.TBL.Gift_Attribute_1 T2 ON T.GSplitGFImpID = T2.GFIMPID AND T.GSplitRE_DB_OwnerShort=T2.RE_DB_OwnerShort
					WHERE T.GFType  ='Planned Gift'
 
END;				 
 

SELECT * FROM [SUTTER_1P_MIGRATION].[IMP].[PLANNED_GIVING] 
 --double quotes
 BEGIN 
		USE [SUTTER_1P_MIGRATION] 
 		EXEC sp_FindStringInTable '%"%', 'IMP', 'PLANNED_GIVING'
 
	 	UPDATE [SUTTER_1P_MIGRATION].[IMP].[PLANNED_GIVING] SET rC_Giving__Notes__c=REPLACE(rC_Giving__Notes__c,'"','''') where rC_Giving__Notes__c like '%"%'

 END 
 
 SELECT GFType, COUNT(*) c
 FROM SUTTER_1P_MIGRATION.TBL.GIFT
 GROUP BY GFType

 /*DNC _-- all proposals will be migrated as proposals. 
 
 		--PLANNED GIFT FROM PROPOSAL
					SELECT  
					dbo.fnc_OwnerID() AS OwnerID   
					,NULL AS RE_GSplitImpID__c
					,NULL AS RE_GFImpID__c
					,T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.PRImpID AS	rC_Giving__External_ID__c
					,T1.PRName AS Name
					,T1.PRAmtAsk AS rC_Giving__Giving_Amount__c
					,NULL AS [rC_Giving__Campaign__r:External_ID__c]
					,CASE WHEN T4.NoHH_ImportID IS NOT NULL THEN T4.HH_ImportID ELSE T11.[NEW_TGT_IMPORT_ID] END AS [rC_Giving__Account__r:External_ID__c]
 					,T11.[NEW_TGT_IMPORT_ID]  AS [rC_Giving__Contact__r:External_ID__c]

					,CAST(CASE WHEN T1.PRDateAsk IS NULL THEN T1.DateAdded ELSE T1.PRDateAsk END AS DATE) AS Proposal_Date_Asked__c
					,T3.[GAU External ID] AS [General_Accounting_Unit__r:rC_Giving__External_ID__c]
					,T1.PRPurpose	AS	Proposal_Purpose__c
					,T1.PRRating    AS  Proposal_Rating__c
					,T1.PRReason    AS  Proposal_Reason__c

					,CASE WHEN (T5.Proposal_Status__c IS NULL AND (CASE WHEN T1.PRDateAsk IS NULL THEN T1.DateAdded ELSE T1.PRDateAsk END) <=GETDATE()) 
								THEN 'Completed' WHEN (T5.Proposal_Status__c IS NULL AND (CASE WHEN T1.PRDateAsk IS NULL THEN T1.DateAdded ELSE T1.PRDateAsk END) >GETDATE()) 
								THEN 'Open' ELSE T5.Proposal_Status__c END AS rC_Giving__Stage__c

					,T1.DateAdded	AS	CreatedDate
					,T1.RE_DB_OwnerShort	AS	rC_Giving__Affiliation__c

					,dbo.fnc_RecordType('Planned_Giving_Bequest') AS RecordTypeID
					
					,NULL AS	rC_Giving__Rollup_Giving__c
			 
					--gift attribute (chart_giftattribute)
						,NULL AS Considering_bequest_in__c
						,NULL AS Production__c
						,NULL AS Report_Exclusion__c
						,NULL AS Source_of_notification__c
					--MISC.
						,NULL AS RE_Split_Gift__c
						,NULL AS RE_Gift_Type__c
						,NULL AS RE_Gift_SubType__c
						,NULL AS Post_Date__c 
						,NULL AS Post_Status__c
						,NULL AS RE_Gift_Constituency_Code__c
						,NULL AS RE_Gift_Payment_Method__c
						,NULL AS rC_Giving__Notes__c
						,NULL AS RE_Gift_Realized__c	
						,NULL AS RE_Legacy_Financial_Code__c


					--reference
					,'RE Proposal' AS zrefSrc
					,T1.ImportID AS zref_ImportID
					,T4.ImportID AS zref_NoHH_ImportID
					,T4.HH_ImportID AS zref_HH_ImportID 
					,T1.PRImpID	 AS zrefPRImpID
					,T1.RE_DB_OwnerShort AS zrefRE_DB_OwnerShort
					
					INTO SUTTER_1P_MIGRATION.IMP.PLANNED_GIVING
					FROM SUTTER_1P_DATA.dbo.HC_Proposal_v T1
					INNER JOIN [SUTTER_1P_DATA].dbo.[HC_Constituents_v] T11 ON T1.[ImportID]=T11.[ImportID] AND T1.[RE_DB_OwnerShort]=T11.[RE_DB_OwnerShort]
					LEFT JOIN  SUTTER_1P_MIGRATION.TBL.ProposalAttribute_final  T2 ON T1.PRImpID=T2.PRAttrPRImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
					LEFT JOIN [SUTTER_1P_DATA].dbo.CHART_Fund T3 ON T1.PRFundID=T3.FundID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
					LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T4 ON T11.[NEW_TGT_IMPORT_ID]=T4.NoHH_ImportID 
					LEFT JOIN [SUTTER_1P_DATA].dbo.CHART_PRStatus T5 ON T1.PRStatus=T5.PRStatus AND T1.RE_DB_OwnerShort=T5.RE_DB_OwnerShort
					WHERE T1.PRName  LIKE '%planned%'
					 
			UNION ALL

*/