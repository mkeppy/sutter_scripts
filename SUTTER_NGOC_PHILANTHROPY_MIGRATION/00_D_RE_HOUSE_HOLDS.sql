	
	/*  USE SUTTER_1P_MIGRATION
		GO
		CREATE SCHEMA TBL
		GO
		CREATE SCHEMA IMP
		GO
		CREATE SCHEMA XTR
		GO
	*/
	
	USE SUTTER_1P_DATA
	GO
 
	--A_Organization Accounts
			--A_tbl_Account_Org:  Constituents that are Organization Constituents;  58,821  (expected: 55,799)
				
			   IF EXISTS ( SELECT * FROM dbo.sysobjects WHERE   id = OBJECT_ID(N'[SUTTER_1P_MIGRATION].[TBL].[Account_Org]') AND OBJECTPROPERTY(id, N'IsTable') = 1 )
					DROP TABLE [SUTTER_1P_MIGRATION].[TBL].Account_Org; 
 	  	 
			 
				SELECT DISTINCT T.*
				INTO SUTTER_1P_MIGRATION.tbl.Account_Org
				FROM SUTTER_1P_DATA.TBL.SF_CONSTITUENTS T
				WHERE T.KeyInd='O' 
				GO
				--57147
 
			 	
					--CHECK_DUPES
					SELECT * FROM SUTTER_1P_MIGRATION.tbl.Account_Org
					WHERE IMPORTID IN (SELECT IMPORTID FROM SUTTER_1P_MIGRATION.tbl.Account_Org GROUP BY IMPORTID HAVING COUNT(*)>1)
					ORDER BY IMPORTID

	--_B_C_D Head of Household ACcounts/Contacts
			   IF EXISTS ( SELECT  *
                        FROM    dbo.sysobjects
                        WHERE   id = OBJECT_ID(N'[SUTTER_1P_MIGRATION].[TBL].[Contact_HofH]')
                                AND OBJECTPROPERTY(id, N'IsTable') = 1 )
                DROP TABLE [SUTTER_1P_MIGRATION].[TBL].Contact_HofH
		 
			
			--B_tbl_Contact_Cons_HofH:  Constituents that are Head of Household Constituents only;
 				SELECT T.RE_DB_OwnerShort, T.KeyInd, T.ImportID,  T.ConsID, T.Deceased,  -- hh
				T3.ImportID AS NonHHImpID, T3.Deceased NonHHDeceased,		--non-hh
				T2.IRLink, T2.IRIsHH, T2.IRIsSpouse, T2.IRImpID,  
				CAST('' AS nvarchar(50)) AS HH_ImportID, 
				CAST('' AS nvarchar(50)) AS HH_ConsID, 
				CAST('' AS nvarchar(50)) AS NoHH_ImportID, 
				CAST('' AS nvarchar(50)) AS NoHH_ConsID,
				'tbl_B' AS scr
				,ROW_NUMBER() OVER ( PARTITION BY T.ImportID, T3.ImportID ORDER BY T.ImportID, T3.ImportId) AS Seq 
				INTO SUTTER_1P_MIGRATION.tbl.Contact_HofH
				FROM SUTTER_1P_DATA.TBL.SF_CONSTITUENTS T
				INNER JOIN SUTTER_1P_DATA.TBL.SF_IND_RELAT T2 ON T.ImportID = T2.IRLink  
				INNER JOIN SUTTER_1P_DATA.TBL.SF_CONSTITUENTS T3 ON T2.ImportID=T3.ImportID  
				WHERE (((T.KeyInd)='I') AND ((T2.IRIsHH)='TRUE') AND ((T2.IRIsSpouse)='TRUE')) 
				ORDER BY T.ImportID
				GO
				--17717
   				
				--HH update to set the correct HH based on Deceased constituents. 	
					UPDATE SUTTER_1P_MIGRATION.tbl.Contact_HofH
					SET HH_ImportID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN NonHHImpID ELSE ImportID END WHERE importid!=NonHHImpID 
					GO
					--17663
					
					--Non-HH update
					UPDATE SUTTER_1P_MIGRATION.tbl.Contact_HofH
					SET NoHH_ImportID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN ImportID ELSE NonHHImpID END WHERE importid!=NonHHImpID 
					GO
					--17663

	 				SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
					SELECT * FROM [SUTTER_1P_MIGRATION].tbl.Contact_HofH WHERE importid='ABSF-000223'
					SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Constituents_v] WHERE [RE_DB_Id]='SCAH-050223' OR [ImportID]='ABSF-000223'
					

			--C_tbl_Contact_Cons_NonConsSpouse; Constituents with non-cons Ind Relat Spouse
				INSERT INTO SUTTER_1P_MIGRATION.tbl.Contact_HofH (RE_DB_OwnerShort, KeyInd, ImportID, ConsID, Deceased, 
																  NonHHImpID, NonHHDeceased, 
																  IRLink, IRIsHH, IRIsSpouse, IRImpID,   
														          HH_ImportID, HH_ConsID, NoHH_ImportID, NoHH_ConsID, scr, Seq)
				SELECT T.RE_DB_OwnerShort, T.KeyInd,  T.ImportID, T.ConsID, T.Deceased, 
				T2.ImportID AS IRNonHHImpID,  T2.IRDeceased AS NonHHDeceased, 
				'' AS IRLink, T2.IRIsHH, T2.IRIsSpouse, T2.IRImpID,  
				NULL AS HH_ImportID, null AS HH_ConsID, null AS NoHH_ImportID, null AS NoHH_ConsID,
				'tbl_C' AS scr,
				ROW_NUMBER() OVER ( PARTITION BY T.ImportID, T2.ImportID ORDER BY T.ImportID, T2.ImportId) AS Seq 
				FROM SUTTER_1P_DATA.TBL.SF_CONSTITUENTS T
				LEFT JOIN SUTTER_1P_DATA.TBL.SF_IND_RELAT T2 ON T.ImportID = T2.ImportID 
				LEFT JOIN SUTTER_1P_DATA.TBL.SF_CONSTITUENTS T3 ON T2.ImportID=T3.ImportID 
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T4 ON T.IMPORTID =T4.IMPORTID  --- THIS IS TO REMOVE DUPLICATE CONSTITUENTS ORIGINATED FROM MDM MERGE. WHERE SOME MAY HAVE A SPOUSE RELATIONSHIP WITH AN IRLINK (SPOUSE =TRUE) AND 
																							    --AND SOME MAY HAVE A RELATINOSHIPS SPOUSE == TRUE WITH A NON-CONSTITUENT... LOOK EXAMPLE WITH IMPORTIID = ABSF-000223
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T5 ON T.IMPORTID =T5.NoHH_ImportID
				GROUP BY T.KeyInd, T2.IRIsHH, T.ImportID,  T.RE_DB_OwnerShort, T.ConsID,  T.Deceased, T2.ImportID, T2.IRDeceased, T2.IRLink, T2.IRIsSpouse, T2.IRImpID, T2.RE_DB_OwnerShort, T4.IMPORTID, T5.NoHH_ImportID
				HAVING (((T.KeyInd)='I') AND ((T2.IRLink) Is Null Or (T2.IRLink)='') AND ((T2.IRIsSpouse)='TRUE')) 
				 AND T4.IMPORTID IS NULL  AND T5.NoHH_ImportID IS NULL 
				ORDER BY T.ImportID
				GO
			    --200006

			--D_tbl_Contact_Cons_WNoSpouse: all other non-cons Ind Relat; Ind Constituents with no HofH constituents/relationships;  Ind Constituents with no non-HofH constituents/relationships; 
				INSERT INTO SUTTER_1P_MIGRATION.tbl.Contact_HofH (RE_DB_OwnerShort, KeyInd, ImportID, ConsID, Deceased, 
																  NonHHImpID, NonHHDeceased, 
																  IRLink, IRIsHH, IRIsSpouse, IRImpID, 
																  HH_ImportID, HH_ConsID, NoHH_ImportID, NoHH_ConsID, scr, Seq)
				SELECT T.RE_DB_OwnerShort, T.KeyInd, T.ImportID, T.ConsID, T.Deceased,
				T3.NonHHImpID, '' AS NonHHDeceased, '' AS IRLink, 'TRUE' AS IRIsHH, 
				'' AS IRIsSpouse, '' AS IRImpID,  
				NULL AS HH_ImportID, null AS HH_ConsID, null AS NoHH_ImportID, null AS NoHH_ConsID,
				'tbl_D' AS scr,
				ROW_NUMBER() OVER ( PARTITION BY T.ImportID, T3.NonHHImpID ORDER BY T.ImportID, T3.NonHHImpID) AS Seq 
				FROM SUTTER_1P_DATA.TBL.SF_CONSTITUENTS T
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH AS T2 ON T.ImportID = T2.ImportID  
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH AS T3 ON T.ImportID = T3.NonHHImpID  
				GROUP BY T.KeyInd, T.ImportID, T.RE_DB_OwnerShort, T.ConsID, T.Deceased, T3.NonHHImpID, T2.ImportID, T3.RE_DB_OwnerShort
				HAVING (((T.KeyInd)='I') AND ((T3.NonHHImpID) Is Null) AND ((T2.ImportID) Is Null)) 
				GO
				--664661
 							  
					--_E_Non Head of Household constituents. 	
					/* REPLACED BY UPDATES ON THE HH TABLE.  MAY ONLY USE HH TABLE OR CREATE A NEW SUB TABLE FROM HH AND CALL IT THE SAEM AS THE NOHH 
							-- DROP TABLE SUTTER_1P_MIGRATION.tbl.Contact_NoHofH	
							--E_tbl_Contact_Cons_NonHofH; Constituents NON-head of Household (to be combined into Head of Household Account. 
								SELECT T2.IRIsHH, T.KeyInd, T.ConsID, T.ImportID, T.RE_DB_OwnerShort, T.Deceased, T2.IRLink, T2.IRImpID, T2.RRImpID, T2.IRISSpouse, T2.RE_DB_OwnerShort AS IR_RE_DB_OwnerShort, 
								T3.ConsID AS HH_ConsID, T3.ImportID AS HH_ImportID , T3.Deceased HH_Deceased, T3.RE_DB_OwnerShort AS HH_RE_DB_OwnerShort
								--INTO SUTTER_1P_MIGRATION.tbl.Contact_NoHofH
								FROM (SUTTER_1P_DATA.dbo.HC_Constituents_v AS T
								INNER JOIN SUTTER_1P_DATA.dbo.HC_Ind_Relat_v AS T2 ON T.ImportID = T2.ImportID) 
								INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v AS T3 ON T2.IRLink = T3.ImportID
								GROUP BY T2.IRIsHH, T.KeyInd, T.ConsID, T.ImportID, T.RE_DB_OwnerShort, T.Deceased, T2.IRLink, T2.IRImpID, T2.RRImpID, T2.IRISSpouse, T2.RE_DB_OwnerShort, 
								T3.ConsID, T3.ImportID, T3.Deceased, T3.RE_DB_OwnerShort
								HAVING (((T2.IRIsHH)='TRUE') AND ((T.KeyInd)='I'))
								ORDER BY T.ConsID
								GO
								--832	 
						*/
	--UPDATE HofH constituent for Account, and Contact Primary and Secondary. 
				
			SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
			WHERE importid!=NonHHImpID 			 
			--17663

	 		--HH update to set the correct HH based on Deceased constituents. 	
			UPDATE SUTTER_1P_MIGRATION.tbl.Contact_HofH
			SET HH_ImportID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN NonHHImpID ELSE ImportID END 
			WHERE importid!=NonHHImpID 
			GO
			--17663
			--Non-HH update
			UPDATE SUTTER_1P_MIGRATION.tbl.Contact_HofH
			SET NoHH_ImportID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN ImportID ELSE NonHHImpID END 
			WHERE importid!=NonHHImpID 
			GO
			--17663

			--UPDATE missing blank HH 
			SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
			WHERE (HH_ImportID IS NULL OR HH_ImportID='')
			GO
			
			UPDATE SUTTER_1P_MIGRATION.tbl.Contact_HofH 
			SET HH_ImportID=ImportID 
			WHERE (HH_ImportID IS NULL OR HH_ImportID='')
			GO
			--864721
 
	  	--CHECK DUPLES
			SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
			WHERE importId IN (SELECT ImportID FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH  GROUP BY importid HAVING COUNT(*)>1) 
			ORDER BY ImportID, NoHH_ImportID, Seq
		 

			--REMOVE SEQ>1
			DELETE SUTTER_1P_MIGRATION.tbl.Contact_HofH 
			WHERE seq!='1'

			--remaining duplicates because of a 1 NoHH_ImportID with 2 HH_IMport with  . 
   				--CHECK DUPLES
				SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
				WHERE importId IN (SELECT ImportID FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH  GROUP BY importid HAVING COUNT(*)>1) 
			
				ORDER BY ImportID, NoHH_ImportID, Seq
		 
			    SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
				WHERE [HH_ImportID] IN (SELECT [HH_ImportID] FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH  GROUP BY [HH_ImportID] HAVING COUNT(*)>1) 
				ORDER BY [HH_ImportID]
	 
			    SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
				WHERE [NoHH_ImportID] IN (SELECT [NoHH_ImportID] FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH  GROUP BY [NoHH_ImportID] HAVING COUNT(*)>1) 
				AND [NoHH_ImportID] !=''
				ORDER BY [NoHH_ImportID]


			SELECT	T.*,
					ROW_NUMBER() OVER ( PARTITION BY T.ImportID ORDER BY T.ImportID, T.NonHHImpID) AS Seq2 
			FROM	SUTTER_1P_MIGRATION.tbl.Contact_HofH T
			WHERE [ImportID]='ABSF-016861'

			SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
			WHERE importid='ABSF-016861' OR importid='ABSF-M03649' OR importid='SCAH-00001-079-0100122'





	--total (A) Cons Org:		 58,821
	--total (B_C_D) Cons HofH :  
		--B:    17609
		--C:    211629
		--D:   668879
	--total (E) Cons NonHoH:         
	-----------------------------------
	--total HC_Constituent_v: 974,547   
	  
	--check count: SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.HC_Constituents_v -- 983,348

	--SELECT T.ImportID, T.KeyInd, dbo.AccRecordType(T.ImportID) RecordTypeID FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T 
	--ORDER BY t.KeyInd desc

			 
 --account active test
		SELECT Deceased, NonHHDeceased, 
		CASE WHEN Deceased='true' AND NonHHDeceased='true' THEN 'FALSE'  
		WHEN Deceased='true' AND NonHHDeceased=''  THEN 'FALSE'
		ELSE 'TRUE'
		END AS ACCOUNT_ACTIVE,
		CASE WHEN Deceased='true' THEN 'FALSE' ELSE 'TRUE' END AS PreferredContact,
		COUNT(*) c
		FROM SUTTER_1P_MIGRATION.TBL.Contact_HofH
		GROUP BY Deceased, NonHHDeceased
		ORDER BY Deceased, NonHHDeceased 
		
		SELECT * 
		FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
		WHERE RE_DB_OwnerShort+'-'+ImportId IN (SELECT RE_DB_OwnerShort+'-'+ImportId  
												FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH GROUP BY RE_DB_OwnerShort+'-'+ImportId HAVING COUNT(*)>1)
		ORDER BY RE_DB_OwnerShort+'-'+ImportId 

 
		 SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
		 WHERE importid IN( SELECT importid FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH GROUP BY ImportID HAVING COUNT(*)>1)
		 ORDER BY [ImportID], nohh_importid

 --ISSUES
 --SAMPLE 1--MK:2016.04.07 Resolved the issue when a given constituent ends up being a HH AND a NonHH (e.g.ABSF-018570)
		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
		WHERE importid='ABSF-018570' OR importId='SCAH-03038-079-0057566' OR importid='SCAH-068570' OR [NonHHImpID]='ABSF-018570'
					
		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
		WHERE Importid='SCAH-M21427_A1' OR Importid='SCAH-087094'

		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
		WHERE Importid='MPHF-18458' OR Importid='SCAH-03038-079-0153533'

--SAMPLE 2: 1 const with 2 HH const. 
		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
		WHERE Importid='CVRX-TR2363' OR Importid='SCAH-130409'

		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
		WHERE [NoHH_ImportID] IN (SELECT [NoHH_ImportID] FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH  GROUP BY [NoHH_ImportID] HAVING COUNT(*)>1)
		AND [NoHH_ImportID]!='' AND [NoHH_ImportID] IS NOT null
		ORDER BY [NoHH_ImportID] 


		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH 
		WHERE [NoHH_ImportID]='ABSF-001914' OR [ImportID]='ABSF-001914' OR 