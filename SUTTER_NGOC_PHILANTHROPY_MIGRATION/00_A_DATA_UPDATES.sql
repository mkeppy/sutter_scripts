
USE SUTTER_1P_DATA
GO


/****************************************************************************************/	
--ADD RE_DB_Id to table to use for linking with combo external Id. 
-- this is only required when XTR files are needed for Data Loader Inserts.
		BEGIN
				--CONSTITUENT 
				ALTER TABLE SUTTER_1P_DATA.dbo.HC_Constituents_v
				ADD RE_DB_Id VARCHAR(30)
				
				UPDATE SUTTER_1P_DATA.dbo.HC_Constituents_v
				SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID

	
				--INDIVIDUAL RELATIONSHIP
				ALTER TABLE SUTTER_1P_DATA.DBO.HC_Ind_Relat_v
				ADD RE_DB_Id VARCHAR(30)
				
				UPDATE SUTTER_1P_DATA.DBO.HC_Ind_Relat_v
				SET RE_DB_Id=RE_DB_OwnerShort+'-'+IRImpID
				 

				--ORGANIZATION RELATIONSHIP
				ALTER TABLE SUTTER_1P_DATA.DBO.HC_Org_Relat_v
				ADD RE_DB_Id VARCHAR(30)
				
				UPDATE SUTTER_1P_DATA.DBO.HC_Org_Relat_v
				SET RE_DB_Id=RE_DB_OwnerShort+'-'+ORImpID
				 


					--SELECT ImportID, RE_DB_OwnerShort, RE_DB_Id FROM SUTTER_1P_DATA.dbo.HC_Constituents_v
				--GIFT
				ALTER TABLE SUTTER_1P_DATA.dbo.HC_Gift_v
				ADD RE_DB_Id VARCHAR(30)
				
					UPDATE SUTTER_1P_DATA.dbo.HC_Gift_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID

						--  SELECT top 1000 * FROM SUTTER_1P_DATA.dbo.HC_Gift_v
				
				--CONS ACTION 
				ALTER TABLE SUTTER_1P_DATA.dbo.HC_Cons_Action_v
				ADD RE_DB_Id VARCHAR(30)
				
					UPDATE SUTTER_1P_DATA.dbo.HC_Cons_Action_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID

				--CONS SOLICITOR 
				ALTER TABLE SUTTER_1P_DATA.dbo.HC_Cons_Solicitor_v
				ADD RE_DB_Id VARCHAR(30)
				
					UPDATE SUTTER_1P_DATA.dbo.HC_Cons_Solicitor_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
							
							--SELECT * FROM SUTTER_1P_DATA.dbo.HC_Cons_Solicitor_v
				--PROPOSAL
				ALTER TABLE SUTTER_1P_DATA.dbo.HC_Proposal_v
				ADD RE_DB_Id VARCHAR(30)
				
					UPDATE SUTTER_1P_DATA.dbo.HC_Proposal_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
			
				--PROSPECT RATING
				ALTER TABLE SUTTER_1P_DATA.DBO.HC_Prospect_Rating_v
				ADD RE_DB_Id VARCHAR(30)	

					UPDATE SUTTER_1P_DATA.dbo.HC_Prospect_Rating_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID

				--CONSTITUENT ATTRIBUTE
				ALTER TABLE SUTTER_1P_DATA.DBO.HC_Cons_Attributes_v
				ADD RE_DB_Id VARCHAR(30)	

					UPDATE SUTTER_1P_DATA.dbo.HC_Cons_Attributes_v
					SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID




		END 
 


BEGIN--MDM: Constituents
			--	SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.HC_Constituents_v  974,547
			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Constituents_v
			ADD NEW_TGT_IMPORT_ID VARCHAR(50)
				
			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Constituents_v
			ADD MDM_PTY_ID VARCHAR(50)
						
			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Constituents_v
			ADD FULL_NM VARCHAR(255)

			SELECT DISTINCT T.RE_Db_Id, T.KeyInd, T.ConsID, T.ImportID, T.OrgName, T.RE_DB_OwnerShort
					, T2.NEW_TGT_IMPORT_ID, T2.MDM_PTY_ID, T2.FULL_NM
			FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T
			LEFT JOIN SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_CN AS T2 ON T.RE_DB_Id = T2.IMPORT_ID

			UPDATE  SUTTER_1P_DATA.dbo.HC_Constituents_v 
			SET NEW_TGT_IMPORT_ID=T2.NEW_TGT_IMPORT_ID, MDM_PTY_ID=T2.MDM_PTY_ID, FULL_NM=T2.FULL_NM
			FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T
			LEFT JOIN SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_CN AS T2 ON T.RE_DB_Id = T2.IMPORT_ID

		--test
				SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.HC_Constituents_v  --before 974,547

				SELECT TOP 100 ImportID, Name, RE_DB_OwnerShort, RE_DB_Id, NEW_TGT_IMPORT_ID, MDM_PTY_ID
				FROM SUTTER_1P_DATA.dbo.HC_Constituents_v  
				WHERE RE_DB_Id!=NEW_TGT_IMPORT_ID 

				SELECT * FROM SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_CN WHERE MDM_PTY_ID ='MDM00012033778'
END;


BEGIN--MDM: IndRelat
	
			--	SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v 518,557
			--const fields
			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			ADD NEW_TGT_IMPORT_ID VARCHAR(50)

			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			ADD MDM_PTY_ID VARCHAR(50)

			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			ADD FULL_NM VARCHAR(255)
			
			--ind relat
			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			ADD IR_NEW_TGT_IMPORT_ID VARCHAR(50)

			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			ADD IR_MDM_PTY_ID VARCHAR(50)

			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			ADD IR_FULL_NM VARCHAR(255)

			--irlink
			ALTER TABLE SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			ADD IRLINK_NEW_TGT_IMPORT_ID VARCHAR(50)
	 

			--IRImpID new update
			SELECT DISTINCT T.RE_Db_Id, T.ConsID, T.ImportID, T.IRLink, T.RE_DB_OwnerShort
							,T2.NEW_TGT_IMPORT_ID, T2.MDM_PTY_ID, T2.FULL_NM
			FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v T
			LEFT JOIN SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_IR AS T2 ON T.RE_DB_Id = T2.IMPORT_ID

			SELECT * FROM SUTTER_1P_DATA.TBL.MDM_RECORDS_1P1T_IR 
 
			SELECT * FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v WHERE RE_DB_Id='SMCF-03947-700-0000056428' OR re_db_id='SAHF-04164-518-0000002752'
		 




BEGIN --UPDATE CONS ADDRESS COUNTRY

		 SELECT [AddrState], [AddrCountry], count(*) 
		 FROM SUTTER_1P_DATA.DBO.HC_Cons_Address_v
		 WHERE --country is null and 
		 (AddrState ='AL' or AddrState='AK' or AddrState='AZ' or AddrState='AR'  or AddrState='CA' or AddrState='CO' or AddrState='CT' or AddrState='DE' or AddrState='DC' or AddrState='FL'
		 or AddrState='GA' or AddrState='HI' or AddrState='ID' or AddrState='IL' or AddrState='IN' or AddrState='IA' or AddrState='KS' or AddrState='KY' or AddrState='LA' or AddrState='ME' or AddrState='MD'
		 or AddrState='MA' or AddrState='MI' or AddrState='MN' or AddrState='MS' or AddrState='MO' or AddrState='MT' or AddrState='NE' or AddrState='NV' or AddrState='NH' or AddrState='NJ' or AddrState='NM' 
         or AddrState='NY' or AddrState='NC' or AddrState='ND' or AddrState='OH' or AddrState='OK' or AddrState='OR' or AddrState='PA' or AddrState='RI' or AddrState='SC' or AddrState='SD' or AddrState='TN'
         or AddrState='TX' or AddrState='UT' or AddrState='VT' or AddrState='VA' or AddrState='WA' or AddrState='WV' or AddrState='WI' or AddrState='WY')
		 GROUP BY [AddrState], [AddrCountry]   --'United States'
		  
		 
		 UPDATE SUTTER_1P_DATA.DBO.HC_Cons_Address_v SET [AddrCountry] = 'United States' WHERE --country is null AND 
		 (AddrState ='AL' or AddrState='AK' or AddrState='AZ' or AddrState='AR'  or AddrState='CA' or AddrState='CO' or AddrState='CT' or AddrState='DE' or AddrState='DC' or AddrState='FL'
		 or AddrState='GA' or AddrState='HI' or AddrState='ID' or AddrState='IL' or AddrState='IN' or AddrState='IA' or AddrState='KS' or AddrState='KY' or AddrState='LA' or AddrState='ME' or AddrState='MD'
		 or AddrState='MA' or AddrState='MI' or AddrState='MN' or AddrState='MS' or AddrState='MO' or AddrState='MT' or AddrState='NE' or AddrState='NV' or AddrState='NH' or AddrState='NJ' or AddrState='NM' 
         or AddrState='NY' or AddrState='NC' or AddrState='ND' or AddrState='OH' or AddrState='OK' or AddrState='OR' or AddrState='PA' or AddrState='RI' or AddrState='SC' or AddrState='SD' or AddrState='TN'
         or AddrState='TX' or AddrState='UT' or AddrState='VT' or AddrState='VA' or AddrState='WA' or AddrState='WV' or AddrState='WI' or AddrState='WY')
		 
END;


BEGIN --UPDATE NON-CONS IND RELAT ADDRESS COUNTRY

		 SELECT [IRAddrState], [IRAddrCountry], count(*) 
		 FROM SUTTER_1P_DATA.DBO.HC_Ind_Relat_v
		 WHERE (IRLink IS NULL OR IRLink='') AND 
		 (IRAddrState ='AL' or IRAddrState='AK' or IRAddrState='AZ' or IRAddrState='AR'  or IRAddrState='CA' or IRAddrState='CO' or IRAddrState='CT' or IRAddrState='DE' or IRAddrState='DC' or IRAddrState='FL'
		 or IRAddrState='GA' or IRAddrState='HI' or IRAddrState='ID' or IRAddrState='IL' or IRAddrState='IN' or IRAddrState='IA' or IRAddrState='KS' or IRAddrState='KY' or IRAddrState='LA' or IRAddrState='ME' or IRAddrState='MD'
		 or IRAddrState='MA' or IRAddrState='MI' or IRAddrState='MN' or IRAddrState='MS' or IRAddrState='MO' or IRAddrState='MT' or IRAddrState='NE' or IRAddrState='NV' or IRAddrState='NH' or IRAddrState='NJ' or IRAddrState='NM' 
         or IRAddrState='NY' or IRAddrState='NC' or IRAddrState='ND' or IRAddrState='OH' or IRAddrState='OK' or IRAddrState='OR' or IRAddrState='PA' or IRAddrState='RI' or IRAddrState='SC' or IRAddrState='SD' or IRAddrState='TN'
         or IRAddrState='TX' or IRAddrState='UT' or IRAddrState='VT' or IRAddrState='VA' or IRAddrState='WA' or IRAddrState='WV' or IRAddrState='WI' or IRAddrState='WY')
		 GROUP BY [IRAddrState], [IRAddrCountry]   --'United States'
		 
		 UPDATE SUTTER_1P_DATA.DBO.HC_Ind_Relat_v SET [IRAddrCountry] = 'United States' WHERE (IRLink IS NULL OR IRLink='') AND 
		 (IRAddrState ='AL' or IRAddrState='AK' or IRAddrState='AZ' or IRAddrState='AR'  or IRAddrState='CA' or IRAddrState='CO' or IRAddrState='CT' or IRAddrState='DE' or IRAddrState='DC' or IRAddrState='FL'
		 or IRAddrState='GA' or IRAddrState='HI' or IRAddrState='ID' or IRAddrState='IL' or IRAddrState='IN' or IRAddrState='IA' or IRAddrState='KS' or IRAddrState='KY' or IRAddrState='LA' or IRAddrState='ME' or IRAddrState='MD'
		 or IRAddrState='MA' or IRAddrState='MI' or IRAddrState='MN' or IRAddrState='MS' or IRAddrState='MO' or IRAddrState='MT' or IRAddrState='NE' or IRAddrState='NV' or IRAddrState='NH' or IRAddrState='NJ' or IRAddrState='NM' 
         or IRAddrState='NY' or IRAddrState='NC' or IRAddrState='ND' or IRAddrState='OH' or IRAddrState='OK' or IRAddrState='OR' or IRAddrState='PA' or IRAddrState='RI' or IRAddrState='SC' or IRAddrState='SD' or IRAddrState='TN'
         or IRAddrState='TX' or IRAddrState='UT' or IRAddrState='VT' or IRAddrState='VA' or IRAddrState='WA' or IRAddrState='WV' or IRAddrState='WI' or IRAddrState='WY')
		 
END;


BEGIN --UPDATE NON-CONS ORG RELAT ADDRESS COUNTRY

		 SELECT [ORAddrState], [ORAddrCountry], count(*) 
		 FROM SUTTER_1P_DATA.DBO.HC_Org_Relat_v
		 WHERE (ORLINK IS NULL OR ORLINK='') AND 
		 (ORAddrState ='AL' or ORAddrState='AK' or ORAddrState='AZ' or ORAddrState='AR'  or ORAddrState='CA' or ORAddrState='CO' or ORAddrState='CT' or ORAddrState='DE' or ORAddrState='DC' or ORAddrState='FL'
		 or ORAddrState='GA' or ORAddrState='HI' or ORAddrState='ID' or ORAddrState='IL' or ORAddrState='IN' or ORAddrState='IA' or ORAddrState='KS' or ORAddrState='KY' or ORAddrState='LA' or ORAddrState='ME' or ORAddrState='MD'
		 or ORAddrState='MA' or ORAddrState='MI' or ORAddrState='MN' or ORAddrState='MS' or ORAddrState='MO' or ORAddrState='MT' or ORAddrState='NE' or ORAddrState='NV' or ORAddrState='NH' or ORAddrState='NJ' or ORAddrState='NM' 
         or ORAddrState='NY' or ORAddrState='NC' or ORAddrState='ND' or ORAddrState='OH' or ORAddrState='OK' or ORAddrState='OR' or ORAddrState='PA' or ORAddrState='RI' or ORAddrState='SC' or ORAddrState='SD' or ORAddrState='TN'
         or ORAddrState='TX' or ORAddrState='UT' or ORAddrState='VT' or ORAddrState='VA' or ORAddrState='WA' or ORAddrState='WV' or ORAddrState='WI' or ORAddrState='WY')
		 GROUP BY [ORAddrState], [ORAddrCountry]   --'United States'
		 
		 UPDATE SUTTER_1P_DATA.DBO.HC_Org_Relat_v SET [ORAddrCountry] = 'United States' WHERE (ORLINK IS NULL OR ORLINK='') AND 
		 (ORAddrState ='AL' or ORAddrState='AK' or ORAddrState='AZ' or ORAddrState='AR'  or ORAddrState='CA' or ORAddrState='CO' or ORAddrState='CT' or ORAddrState='DE' or ORAddrState='DC' or ORAddrState='FL'
		 or ORAddrState='GA' or ORAddrState='HI' or ORAddrState='ID' or ORAddrState='IL' or ORAddrState='IN' or ORAddrState='IA' or ORAddrState='KS' or ORAddrState='KY' or ORAddrState='LA' or ORAddrState='ME' or ORAddrState='MD'
		 or ORAddrState='MA' or ORAddrState='MI' or ORAddrState='MN' or ORAddrState='MS' or ORAddrState='MO' or ORAddrState='MT' or ORAddrState='NE' or ORAddrState='NV' or ORAddrState='NH' or ORAddrState='NJ' or ORAddrState='NM' 
         or ORAddrState='NY' or ORAddrState='NC' or ORAddrState='ND' or ORAddrState='OH' or ORAddrState='OK' or ORAddrState='OR' or ORAddrState='PA' or ORAddrState='RI' or ORAddrState='SC' or ORAddrState='SD' or ORAddrState='TN'
         or ORAddrState='TX' or ORAddrState='UT' or ORAddrState='VT' or ORAddrState='VA' or ORAddrState='WA' or ORAddrState='WV' or ORAddrState='WI' or ORAddrState='WY')
		 
END;

BEGIN --UPDATE ASSIGNED SOLICITOR RELATIONSHIP TYPE TO "SOLICITOR" WHEN ASRTYPE IS NULL

		SELECT * FROM SUTTER_1P_DATA.DBO.HC_Cons_Solicitor_v WHERE ASRType IS NULL
		UPDATE SUTTER_1P_DATA.DBO.HC_Cons_Solicitor_v SET ASRType='Solicitor' WHERE ASRType IS NULL
		
END;

BEGIN--UPDATE CONS ATTR DESC TO 'NULL'

		SELECT * FROM SUTTER_1P_DATA.DBO.HC_Cons_Attributes_v WHERE CAttrDesc IS NULL
		UPDATE SUTTER_1P_DATA.DBO.HC_Cons_Attributes_v SET CAttrDesc='NULL' WHERE CAttrDesc IS NULL	
END; 


BEGIN--UPDATE GIFT: CAMPAIGN, APPEAL, PACKAGE TO "NULL"

		SELECT GSplitCamp, GSplitAppeal, GSplitPkg, GSplitFund
		FROM SUTTER_1P_DATA.DBO.HC_Gift_SplitGift_v
		WHERE GSplitCamp IS NULL OR GSplitAppeal IS NULL OR GSplitPkg IS NULL OR GSplitFund	IS NULL
		GROUP BY GSplitCamp, GSplitAppeal, GSplitPkg, GSplitFund
		
		UPDATE SUTTER_1P_DATA.DBO.HC_Gift_SplitGift_v SET GSplitCamp='NULL' WHERE GSplitCamp IS NULL
		UPDATE SUTTER_1P_DATA.DBO.HC_Gift_SplitGift_v SET GSplitAppeal='NULL' WHERE GSplitAppeal IS NULL
		UPDATE SUTTER_1P_DATA.DBO.HC_Gift_SplitGift_v SET GSplitPkg='NULL' WHERE GSplitPkg IS NULL
		
END;

BEGIN--UPDATE CONS APPEAL: APPEAL, PACKAGE TO "NULL"
		SELECT * FROM SUTTER_1P_DATA.DBO.HC_Cons_Appeal_v WHERE CAPPackageID IS NULL 
		UPDATE SUTTER_1P_DATA.DBO.HC_Cons_Appeal_v SET CAPPackageID='NULL' WHERE CAPPackageID IS NULL 
		

END;

BEGIN--UPDATE CREDIT CARD TYPE
		SELECT GFPayMeth, GFCCType, COUNT(*) C FROM SUTTER_1P_DATA.DBO.HC_Gift_v
		GROUP BY GFPayMeth, GFCCType
		
		UPDATE SUTTER_1P_DATA.DBO.HC_Gift_v SET GFCCType='MasterCard' WHERE GFCCType='Master Card'
		UPDATE SUTTER_1P_DATA.DBO.HC_Gift_v SET GFCCType=NULL WHERE GFCCType='Visa' AND GFPayMeth='Personal Check'
		
END; 

BEGIN--UPDATE payment info where paytype is not credit card. 

		SELECT GFImpId, GFPayMeth, GFCCType, GFCCNum, GFCardholderName, GFCCExpOn, GFAuthCode
		FROM SUTTER_1P_DATA.dbo.hc_gift_v 
		WHERE GFPayMeth!='Credit Card' AND (GFCCType IS NOT NULL 
											OR GFCCNum IS NOT NULL 
											OR GFCardholderName IS NOT NULL 
											OR GFCCExpOn IS NOT NULL
											OR GFAuthCode IS NOT NULL)
		
		UPDATE SUTTER_1P_DATA.dbo.hc_gift_v SET GFCCNum=NULL, GFCardholderName=NULL
		WHERE GFPayMeth!='Credit Card' AND (GFCCType IS NOT NULL 
											OR GFCCNum IS NOT NULL 
											OR GFCardholderName IS NOT NULL 
											OR GFCCExpOn IS NOT NULL
											OR GFAuthCode IS NOT NULL)
END 	


BEGIN

	--CANotes with PICUTRE On NOTE field
	SELECT * FROM SUTTER_1P_DATA.dbo.HC_Cons_Action_Notes_v 
	WHERE RE_DB_OwnerShort='SMCF' AND (CANoteImpID='03947-420-0000035508' 
		OR CANoteImpID='03947-420-0000041416'
		OR CANoteImpID='03947-420-0000050951')
	
	--REMOVE contect of NOTES fields
	UPDATE SUTTER_1P_DATA.dbo.HC_Cons_Action_Notes_v
	SET CANoteNotes=NULL
	WHERE RE_DB_OwnerShort='SMCF' AND (CANoteImpID='03947-420-0000035508' 
		OR CANoteImpID='03947-420-0000041416'
		OR CANoteImpID='03947-420-0000050951')
END
	
	