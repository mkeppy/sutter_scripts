USE SUTTER_1P_DATA
GO

BEGIN--INACTIVE ACCOUNTS  ([ACCOUNT:External_ID__c]) 
	 --If all of the related Contacts have the Deceased flag equal to true then the rC Bios Active flag would be set to false.
 
	DROP TABLE [SUTTER_1P_MIGRATION].[TBL].[Account_Inactive] 

	SELECT [ACCOUNT:External_ID__c], rC_Bios__Deceased__c, COUNT(*) C
	INTO [SUTTER_1P_MIGRATION].[TBL].[Account_Inactive] 
	FROM [SUTTER_1P_MIGRATION].[IMP].[CONTACT]
	GROUP BY [ACCOUNT:External_ID__c], rC_Bios__Deceased__c
	ORDER BY [ACCOUNT:External_ID__c], rC_Bios__Deceased__c

END
 

BEGIN--ACCOUNT UPDATE: INACTIVE and PARENT ACCOUNT
		--INACTIVE ACCOUNTS
		
		DROP TABLE [SUTTER_1P_MIGRATION].[IMP].[ACCOUNT_UPDATE]
		
			SELECT DISTINCT [ACCOUNT:External_ID__c] AS [External_ID__c]
			,'FALSE' AS rC_Bios__Active__c
			,NULL AS [Parent:External_ID__c]
			,rC_Bios__Deceased__c AS zref_rC_Bios__Deceased__c
			,C AS zrefCount
			INTO [SUTTER_1P_MIGRATION].[IMP].[ACCOUNT_UPDATE] 
			FROM [SUTTER_1P_MIGRATION].[TBL].[Account_Inactive] 
			WHERE [ACCOUNT:External_ID__c] NOT IN (SELECT [ACCOUNT:External_ID__c] FROM  [SUTTER_1P_MIGRATION].[TBL].[Account_Inactive]  GROUP BY [ACCOUNT:External_ID__c] HAVING COUNT(*)>1)
			AND rC_Bios__Deceased__c='TRUE'
		UNION ALL
			--PARENT ACCOUNT
			SELECT DISTINCT 
			T.NEW_TGT_IMPORT_ID [External_ID__c]
			,NULL AS rC_Bios__Active__c
			,T2.NEW_TGT_IMPORT_ID AS [Parent:External_ID__c]
			,NULL AS zref_rC_Bios__Deceased__c
			,NULL AS zrefCount
			FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T
			LEFT JOIN SUTTER_1P_DATA.[TBL].[MDM_RECORDS_1P1T_CN] T2 ON (T.RE_DB_OwnerShort+'-'+T.ParentCorpLink)=T2.IMPORT_ID
			WHERE T2.NEW_TGT_IMPORT_ID IS NOT NULL
			AND (T.NEW_TGT_IMPORT_ID)!=(T2.NEW_TGT_IMPORT_ID)
			ORDER BY [ACCOUNT:External_ID__c]


END  

BEGIN--CHECK DUPLICATE
         SELECT *
         FROM   [SUTTER_1P_MIGRATION].[IMP].[ACCOUNT_UPDATE]
         WHERE  [External_ID__c] IN (
                SELECT  [External_ID__c]
                FROM    [SUTTER_1P_MIGRATION].[IMP].[ACCOUNT_UPDATE]
                GROUP BY [External_ID__c]
                HAVING  COUNT(*) > 1 );
END
 
BEGIN	
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[ACCOUNT_update]
		SET [Parent:External_ID__c] = 'SCAH-951096'
		WHERE [External_ID__c]='SCAH-03038-079-0188487'

		DELETE [SUTTER_1P_MIGRATION].[IMP].[ACCOUNT_update]
		WHERE [External_ID__c]='SCAH-03038-079-0188487' AND rC_Bios__Active__c IS NULL AND zrefCount IS null
END
