USE [SUTTER_1P_MIGRATION]
GO
   

BEGIN
		DROP TABLE SUTTER_1P_MIGRATION.IMP.RELATIONSHIP
END
	

BEGIN--CREATE RELATIONSHIP	  
			--INDIVIDUAL RELATIONSHIPS
			SELECT DISTINCT
			 OwnerID=dbo.fnc_OwnerID()
			,RecordTypeID=CASE T1.KeyInd WHEN 'I' THEN dbo.fnc_RecordType('Relationship_Contact_Contact')
							WHEN 'O' THEN dbo.fnc_RecordType('Relationship_Account_Contact') END
			,External_ID__c=T.NEW_TGT_IMPORT_ID_IR
			,RE_IRImpID__c=T.NEW_TGT_IMPORT_ID_IR			
			,RE_ORImpID__c=NULL 
			,RE_ASRImpID__c=NULL
			,RE_ESRImpID__c=NULL 
			,[rC_Bios__Account_1__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN (T1.NEW_TGT_IMPORT_ID) END
			,[rC_Bios__Contact_1__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN (T1.NEW_TGT_IMPORT_ID) END 
			,[rC_Bios__Account_2__r:External_ID__c]=NULL
			,[rC_Bios__Contact_2__r:External_ID__c]=CASE WHEN (T.NEW_TGT_IMPORT_ID_IRLINK!='' AND T.NEW_TGT_IMPORT_ID_IRLINK IS NOT NULL) THEN (T2.NEW_TGT_IMPORT_ID) 
										WHEN (T.NEW_TGT_IMPORT_ID_IRLINK='' OR T.NEW_TGT_IMPORT_ID_IRLINK IS NULL)  THEN ('IR-'+T.NEW_TGT_IMPORT_ID_IR) END
			,rC_Bios__Category__c=CASE WHEN T.[IRIsSpouse]='TRUE' THEN 'Family' ELSE [dbo].[fnc_ProperCase](T3.NGOC_CATEGORY) END 
			,rC_Bios__Role_1__c=CASE WHEN T.[IRIsSpouse]='TRUE' THEN 'Spouse' ELSE [dbo].[fnc_ProperCase](T3.Role_1) END
			,rC_Bios__Role_2__c=CASE WHEN T.[IRIsSpouse]='TRUE' THEN 'Spouse' ELSE [dbo].[fnc_ProperCase](T3.Role_2) END 

			,rC_Bios__Starting_Month__c=CASE WHEN LEN(T.IRFromDate)>7 THEN MONTH(T.IRFromDate) WHEN LEN(T.IRFromDate)=7 THEN LEFT(T.IRFromDate,2) WHEN LEN(T.IRFromDate)=4 THEN NULL END
			,rC_Bios__Starting_Day__c=CASE WHEN LEN(T.IRFromDate)>7 THEN DAY(T.IRFromDate) END
			,rC_Bios__Starting_Year__c=CASE WHEN LEN(T.IRFromDate)>7 THEN YEAR(T.IRFromDate)  WHEN LEN(T.IRFromDate)=7 THEN RIGHT(T.IRFromDate,4) WHEN LEN(T.IRFromDate)=4 THEN T.IRFromDate END
			,rC_Bios__Stopping_Month__c=CASE WHEN LEN(T.IRToDate)>7 THEN MONTH(T.IRToDate) WHEN LEN(T.IRToDate)=7 THEN LEFT(T.IRToDate,2) WHEN LEN(T.IRToDate)=4 THEN NULL END
			,rC_Bios__Stopping_Day__c=CASE WHEN LEN(T.IRToDate)>7 THEN DAY(T.IRToDate) END
			,rC_Bios__Stopping_Year__c=CASE WHEN LEN(T.IRToDate)>7 THEN YEAR(T.IRToDate)  WHEN LEN(T.IRToDate)=7 THEN RIGHT(T.IRToDate,4) WHEN LEN(T.IRToDate)=4 THEN T.IRToDate END
			,CASE WHEN [dbo].[fn_DateConvert](T.IRToDate) < GETDATE() THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Active__c
			,'FALSE' AS rC_Bios__Primary__c
			,rC_Bios__Position__c=[dbo].[fnc_ProperCase](T.[IRPos])
			,rC_Bios__Comments__c=CAST(T.IRNotes AS VARCHAR(MAX)) 
			,Education_Relationship_Type__c=NULL
			,Education_Major__c=NULL
			,Education_Minor__c=NULL
			,Affiliation__c=LEFT(T.NEW_TGT_IMPORT_ID_IR,4)
			,Contact_Type__c=T.[IRContactType]
			--ref
				,T1.KeyInd zrefT1_KeyInd, T1.NEW_TGT_IMPORT_ID zrefT1_ImportId
				,T2.KeyInd zrefT2_KeyInd, T2.NEW_TGT_IMPORT_ID zrefT2_ImportId
				,T.NEW_TGT_IMPORT_ID_IR AS zrefRImpID, T.NEW_TGT_IMPORT_ID_IRLINK zref_RLink
				,'Ind_Relat' AS zrefSource
			
			INTO SUTTER_1P_MIGRATION.IMP.RELATIONSHIP
			FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v T
			INNER JOIN SUTTER_1P_MIGRATION.TBL.RELATIONSHIP_IND_FINAL T0 ON T.RE_DB_Id=T0.IRImpID  
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T1 ON T.NEW_TGT_IMPORT_ID_CN=T1.NEW_TGT_IMPORT_ID
			LEFT JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T2 ON T.NEW_TGT_IMPORT_ID_IRLINK=T2.NEW_TGT_IMPORT_ID
			LEFT JOIN SUTTER_1P_DATA.dbo.CHART_RelatRecipType T3 ON T.IRRelat=T3.RELATIONSHIP AND T.IRRecip=T3.RECIPROCAL 
																	AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort 
																	AND T1.KeyInd=T3.KeyInd 
			WHERE T3.RelationshipIndicator='Individual'  
			 

		UNION ALL
		
			--ORGANIZATION RELATIONSHIPS
			--code reverts order when first constituent is Contact and second is Account. NGOC accepts Account-Contact only, not Contact-Account. 
			SELECT DISTINCT
			 OwnerID=dbo.fnc_OwnerID()
			,RecordTypeID=CASE T1.KeyInd WHEN 'I' THEN dbo.fnc_RecordType('Relationship_Account_Contact')
							WHEN 'O' THEN dbo.fnc_RecordType('Relationship_Account_Account') END
			,External_ID__c=T.NEW_TGT_IMPORT_ID_OR
			,RE_IRImpID__c=NULL
			,RE_ORImpID__c=T.NEW_TGT_IMPORT_ID_OR
			,RE_ASRImpID__c=NULL
			,RE_ESRImpID__c=NULL 
			,[rC_Bios__Account_1__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN (T1.NEW_TGT_IMPORT_ID) 
									    WHEN (T1.KeyInd='I' AND (T.NEW_TGT_IMPORT_ID_ORLINK!='' AND T.NEW_TGT_IMPORT_ID_ORLINK IS NOT NULL))  THEN (T2.NEW_TGT_IMPORT_ID) 
										WHEN (T1.KeyInd='I' AND (T.NEW_TGT_IMPORT_ID_ORLINK='' OR T.NEW_TGT_IMPORT_ID_ORLINK IS NULL))  THEN ('OR-'+T.NEW_TGT_IMPORT_ID_OR) END
			,[rC_Bios__Contact_1__r:External_ID__c]=NULL 
			,[rC_Bios__Account_2__r:External_ID__c]=CASE WHEN (T1.KeyInd='O' AND (T.NEW_TGT_IMPORT_ID_ORLINK!='' and NEW_TGT_IMPORT_ID_ORLINK IS NOT NULL)) THEN (T2.NEW_TGT_IMPORT_ID) 
										WHEN (T1.KeyInd='O' AND (T.NEW_TGT_IMPORT_ID_ORLINK='' OR NEW_TGT_IMPORT_ID_ORLINK IS NULL)) THEN ('OR-'+T.NEW_TGT_IMPORT_ID_OR) END
			,[rC_Bios__Contact_2__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN (T1.NEW_TGT_IMPORT_ID) END  
			,rC_Bios__Category__c=[dbo].[fnc_ProperCase](T3.NGOC_CATEGORY)
			,rC_Bios__Role_1__c=[dbo].[fnc_ProperCase](T3.Role_1)
			,rC_Bios__Role_2__c=[dbo].[fnc_ProperCase](T3.Role_2)
			,rC_Bios__Starting_Month__c=CASE WHEN LEN(T.ORFromDate)>7 THEN MONTH(T.ORFromDate) WHEN LEN(T.ORFromDate)=7 THEN LEFT(T.ORFromDate,2) WHEN LEN(T.ORFromDate)=4 THEN NULL END
			,rC_Bios__Starting_Day__c=CASE WHEN LEN(T.ORFromDate)>7 THEN DAY(T.ORFromDate) END
			,rC_Bios__Starting_Year__c=CASE WHEN LEN(T.ORFromDate)>7 THEN YEAR(T.ORFromDate)  WHEN LEN(T.ORFromDate)=7 THEN RIGHT(T.ORFromDate,4) WHEN LEN(T.ORFromDate)=4 THEN T.ORFromDate END
			,rC_Bios__Stopping_Month__c=CASE WHEN LEN(T.ORToDate)>7 THEN MONTH(T.ORToDate) WHEN LEN(T.ORToDate)=7 THEN LEFT(T.ORToDate,2) WHEN LEN(T.ORToDate)=4 THEN NULL END
			,rC_Bios__Stopping_Day__c=CASE WHEN LEN(T.ORToDate)>7 THEN DAY(T.ORToDate) END
			,rC_Bios__Stopping_Year__c=CASE WHEN LEN(T.ORToDate)>7 THEN YEAR(T.ORToDate)  WHEN LEN(T.ORToDate)=7 THEN RIGHT(T.ORToDate,4) WHEN LEN(T.ORToDate)=4 THEN T.ORToDate END
			,CASE WHEN [dbo].[fn_DateConvert](T.ORToDate) < GETDATE() THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Active__c
			,'FALSE' AS rC_Bios__Primary__c		
			,rC_Bios__Position__c=[dbo].[fnc_ProperCase](T.[ORPos])
			,rC_Bios__Comments__c=CAST(T.ORNotes AS VARCHAR(MAX)) 
			,Education_Relationship_Type__c=NULL
			,Education_Major__c=NULL
			,Education_Minor__c=NULL
			,Affiliation__c=LEFT(T.NEW_TGT_IMPORT_ID_OR,4)
	 		,Contact_Type__c=T.[ORContactType]
			--ref
				,T1.KeyInd zrefT1_KeyInd, T1.NEW_TGT_IMPORT_ID zrefT1_ImportId
				,T2.KeyInd zrefT2_KeyInd, T2.NEW_TGT_IMPORT_ID zrefT2_ImportId
				,T.NEW_TGT_IMPORT_ID_OR AS zrefRImpID, T.NEW_TGT_IMPORT_ID_ORLINK zref_RLink
				,'Org_Relat' AS zrefSource			
			 
			FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v T
			INNER JOIN SUTTER_1P_MIGRATION.TBL.RELATIONSHIP_ORG_FINAL T0 ON T.RE_DB_Id=T0.ORImpID 
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T1 ON T.NEW_TGT_IMPORT_ID_CN=T1.NEW_TGT_IMPORT_ID
			LEFT JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T2 ON T.NEW_TGT_IMPORT_ID_ORLINK=T2.NEW_TGT_IMPORT_ID 
			LEFT JOIN SUTTER_1P_DATA.dbo.CHART_RelatRecipType T3 ON T.ORRelat=T3.RELATIONSHIP AND T.ORRecip=T3.RECIPROCAL 
																	AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort 
																	AND T1.KeyInd=T3.KeyInd
			WHERE T3.RelationshipIndicator='Organization'  

		UNION ALL 
		
			--EDUCATION RELATIONSHIP
			SELECT	
			 OwnerID=dbo.fnc_OwnerID()
			,RecordTypeID=dbo.fnc_RecordType('Relationship_Account_Contact')
			,External_ID__c=T.RE_DB_OwnerShort+'-'+T.ESRImpID
			,RE_IRImpID__c=NULL
			,RE_ORImpID__c=NULL
			,RE_ASRImpID__c= NULL
			,RE_ESRImpID__c=(T.RE_DB_OwnerShort+'-'+T.ESRImpID)
			,[rC_Bios__Account_1__r:External_ID__c]=(T1.RE_DB_OwnerShort+'-'+T1.Unique_MD5)
			,[rC_Bios__Contact_1__r:External_ID__c]=NULL
			,[rC_Bios__Account_2__r:External_ID__c]=NULL 
			,[rC_Bios__Contact_2__r:External_ID__c]=(T2.NEW_TGT_IMPORT_ID)
										
			,rC_Bios__Category__c='Education'
			,rC_Bios__Role_1__c='Student'
			,rC_Bios__Role_2__c='School'
			
			,rC_Bios__Starting_Month__c=CASE WHEN LEN(T.ESRDateEnt)=8 THEN MONTH(T.ESRDateEnt) WHEN LEN(T.ESRDateEnt)=6 THEN RIGHT(T.ESRDateEnt,2) WHEN LEN(T.ESRDateEnt)=4 THEN NULL END
			,rC_Bios__Starting_Day__c=CASE WHEN LEN(T.ESRDateEnt)=8 THEN DAY(T.ESRDateEnt) END
			,rC_Bios__Starting_Year__c=CASE WHEN LEN(T.ESRDateEnt)=8 THEN YEAR(T.ESRDateEnt)  WHEN LEN(T.ESRDateEnt)=6 THEN LEFT(T.ESRDateEnt,4) WHEN LEN(T.ESRDateEnt)=4 THEN T.ESRDateEnt END
			
			,rC_Bios__Stopping_Month__c=CASE WHEN LEN(T.ESRDateGrad)=8 THEN MONTH(T.ESRDateGrad) WHEN LEN(T.ESRDateGrad)=8 THEN RIGHT(T.ESRDateGrad,2) WHEN LEN(T.ESRDateGrad)=4 THEN NULL END
			,rC_Bios__Stopping_Day__c=CASE WHEN LEN(T.ESRDateGrad)=8 THEN DAY(T.ESRDateGrad) END
			,rC_Bios__Stopping_Year__c=CASE WHEN LEN(T.ESRDateGrad)=8 THEN YEAR(T.ESRDateGrad)  WHEN LEN(T.ESRDateGrad)=8 THEN LEFT(T.ESRDateGrad,4) WHEN LEN(T.ESRDateGrad)=4 THEN T.ESRDateGrad END
			,'TRUE' AS rC_Bios__Active__c
			,NULL AS rC_Bios__Primary__c
			,rC_Bios__Position__c=NULL
			,rC_Bios__Comments__c=CAST(T.ESRNotes AS VARCHAR(MAX)) 
			
			,Education_Relationship_Type__c=T.ESRType
			,CAST(T3.Education_Major__c AS VARCHAR(MAX)) as Education_Major__c
			,CAST(T4.Education_Minor__c as VARCHAR(MAX)) AS Education_Minor__c
			,Affiliation__c=T.RE_DB_OwnerShort
			,Contact_Type__c=NULL
			--ref
			,'O' zrefT1_KeyInd, T1.ESRImpID zrefT1_ImportId
			,T2.KeyInd zrefT2_KeyInd, T2.ImportID zrefT2_ImportId
			,T1.ESRImpID zrefRImpID, null zref_RLink 
		 	,'Educ_Relat' AS zrefSource	
			FROM SUTTER_1P_DATA.dbo.HC_Educ_Relat_v T
			INNER JOIN SUTTER_1P_MIGRATION.TBL.ESRSchoolName T1 ON T.ESRImpID=T1.ESRImpID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T2 ON T.ImportID=T2.ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Education_Major T3 ON T.ESRImpID=T3.ESRMajESRImpID AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Education_Minor T4 ON T.ESRImpID=T4.ESRMinESRImpID AND T.RE_DB_OwnerShort=T4.RE_DB_OwnerShort


END--end of create


BEGIN --Search double qoutes.

			USE [SUTTER_1P_MIGRATION] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'RELATIONSHIP'
END 

BEGIN---REMOVE double qoutes
	
			SELECT rC_Bios__Comments__c FROM SUTTER_1P_MIGRATION.IMP.RELATIONSHIP WHERE rC_Bios__Comments__c LIKE '%"%'
			
			UPDATE SUTTER_1P_MIGRATION.IMP.RELATIONSHIP SET rC_Bios__Comments__c=REPLACE(rC_Bios__Comments__c,'"','''') where rC_Bios__Comments__c like '%"%'
			UPDATE SUTTER_1P_MIGRATION.IMP.RELATIONSHIP SET rC_Bios__Position__c=REPLACE(rC_Bios__Position__c,'"','''') where rC_Bios__Position__c like '%"%'

END 
  
BEGIN--DELETE SELF_RELATED

		--same acct
			SELECT [rC_Bios__Account_1__r:External_ID__c], [rC_Bios__Contact_1__r:External_ID__c], [rC_Bios__Account_2__r:External_ID__c], 	[rC_Bios__Contact_2__r:External_ID__c]
			FROM SUTTER_1P_MIGRATION.IMP.RELATIONSHIP
			WHERE ([rC_Bios__Account_1__r:External_ID__c] =[rC_Bios__Account_2__r:External_ID__c])
		
			DELETE SUTTER_1P_MIGRATION.IMP.RELATIONSHIP
			WHERE ([rC_Bios__Account_1__r:External_ID__c] =[rC_Bios__Account_2__r:External_ID__c])
		--same contact
			SELECT [rC_Bios__Account_1__r:External_ID__c], [rC_Bios__Contact_1__r:External_ID__c], [rC_Bios__Account_2__r:External_ID__c], 	[rC_Bios__Contact_2__r:External_ID__c]
			FROM SUTTER_1P_MIGRATION.IMP.RELATIONSHIP
			WHERE ([rC_Bios__Contact_1__r:External_ID__c] =[rC_Bios__Contact_2__r:External_ID__c])

			DELETE SUTTER_1P_MIGRATION.IMP.RELATIONSHIP
			WHERE ([rC_Bios__Contact_1__r:External_ID__c] =[rC_Bios__Contact_2__r:External_ID__c])


BEGIN --UPDATE PRIMARY RELATIONSHIP FIELD
			--base table
				 SELECT External_ID__c, zrefSource	
						,[rC_Bios__Account_1__r:External_ID__c], [rC_Bios__Contact_1__r:External_ID__c], [rC_Bios__Account_2__r:External_ID__c], 	[rC_Bios__Contact_2__r:External_ID__c],
						[rC_Bios__Category__c], rC_Bios__Role_1__c, rC_Bios__Role_2__c
				,CASE WHEN ([rC_Bios__Category__c]='Employment' 
				       AND (rC_Bios__Role_1__c='Employee' OR rC_Bios__Role_2__c='Employee')  
					   AND ( ([rC_Bios__Account_1__r:External_ID__c] IS NOT NULL AND [rC_Bios__Contact_2__r:External_ID__c] IS NOT NULL))) 
					   AND ROW_NUMBER() OVER ( PARTITION BY [rC_Bios__Contact_2__r:External_ID__c] 
										ORDER BY [rC_Bios__Contact_2__r:External_ID__c] , [rC_Bios__Account_1__r:External_ID__c]
							)='1' THEN 'TRUE' ELSE 'FALSE' END AS PrimaryEmp
				,ROW_NUMBER() OVER ( PARTITION BY [rC_Bios__Contact_2__r:External_ID__c] 
										ORDER BY [rC_Bios__Contact_2__r:External_ID__c] , [rC_Bios__Account_1__r:External_ID__c]
							) AS Seq
				 INTO SUTTER_1P_MIGRATION.TBL.PrimaryRelat
				 FROM SUTTER_1P_MIGRATION.[IMP].[RELATIONSHIP]
				 WHERE [rC_Bios__Category__c]='Employment' 
				       AND (rC_Bios__Role_1__c='Employee' OR rC_Bios__Role_2__c='Employee')  
					   AND ( ([rC_Bios__Account_1__r:External_ID__c] IS NOT NULL AND [rC_Bios__Contact_2__r:External_ID__c] IS NOT NULL))
                       
				 ORDER BY 	[rC_Bios__Contact_2__r:External_ID__c],
				 			 [rC_Bios__Account_1__r:External_ID__c]
			--check	
				SELECT * FROM SUTTER_1P_MIGRATION.TBL.PrimaryRelat
				ORDER BY 	[rC_Bios__Contact_2__r:External_ID__c],
				 			 [rC_Bios__Account_1__r:External_ID__c]

			--delete seq!=1
				DELETE SUTTER_1P_MIGRATION.TBL.PrimaryRelat
				WHERE Seq!='1'
			

			--update IMP_RELATIONSHIP
			UPDATE SUTTER_1P_MIGRATION.[IMP].[RELATIONSHIP]
			SET rC_Bios__Primary__c = t2.PrimaryEmp 
			FROM SUTTER_1P_MIGRATION.[IMP].[RELATIONSHIP] t1
			INNER JOIN SUTTER_1P_MIGRATION.TBL.PrimaryRelat t2 ON t1.External_ID__c = t2.External_ID__c
			 
				--check
				SELECT * FROM SUTTER_1P_MIGRATION.[IMP].[RELATIONSHIP]
				ORDER BY 	[rC_Bios__Contact_2__r:External_ID__c],
				 				 [rC_Bios__Account_1__r:External_ID__c]
END 

BEGIN-- sort rC_Bios__Account__c, Account.External ID and rC_Bios__Contact__c, Contact.External ID

		CREATE INDEX idx_relationship ON SUTTER_1P_MIGRATION.IMP.[RELATIONSHIP]([rC_Bios__Account_1__r:External_ID__c], [rC_Bios__Account_2__r:External_ID__c], [rC_Bios__Contact_1__r:External_ID__c], [rC_Bios__Contact_2__r:External_ID__c]);
		
		SELECT * 
		INTO SUTTER_1P_MIGRATION.[IMP].[RELATIONSHIP_srt]
		FROM SUTTER_1P_MIGRATION.[IMP].[RELATIONSHIP]
		ORDER BY [rC_Bios__Account_1__r:External_ID__c], [rC_Bios__Account_2__r:External_ID__c], [rC_Bios__Contact_1__r:External_ID__c], [rC_Bios__Contact_2__r:External_ID__c]

		SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.[IMP].[RELATIONSHIP_srt]
 		SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.[IMP].[RELATIONSHIP]
END

BEGIN 
		--duplicate
		SELECT * 
		FROM SUTTER_1P_MIGRATION.IMP.RELATIONSHIP
		WHERE External_ID__c IN (SELECT External_ID__c FROM SUTTER_1P_MIGRATION.IMP.RELATIONSHIP GROUP BY External_ID__c HAVING COUNT(*)>1)
		ORDER BY External_ID__c 

		SELECT * 
		FROM SUTTER_1P_MIGRATION.IMP.RELATIONSHIP
		WHERE RE_IRImpID__c IN (SELECT RE_IRImpID__c FROM SUTTER_1P_MIGRATION.IMP.RELATIONSHIP GROUP BY RE_IRImpID__c HAVING COUNT(*)>1)
		ORDER BY RE_IRImpID__c

		SELECT * 
		FROM SUTTER_1P_MIGRATION.IMP.RELATIONSHIP
		WHERE RE_ORImpID__c IN (SELECT RE_ORImpID__c FROM SUTTER_1P_MIGRATION.IMP.RELATIONSHIP GROUP BY RE_ORImpID__c HAVING COUNT(*)>1)
		ORDER BY RE_ORImpID__c

		 
		SELECT * 
		FROM SUTTER_1P_MIGRATION.IMP.RELATIONSHIP
		WHERE RE_ESRImpID__c IN (SELECT RE_ESRImpID__c FROM SUTTER_1P_MIGRATION.IMP.RELATIONSHIP GROUP BY RE_ESRImpID__c HAVING COUNT(*)>1)
		ORDER BY RE_ESRImpID__c

		SELECT * 
		FROM SUTTER_1P_MIGRATION.IMP.RELATIONSHIP
		WHERE [rC_Bios__Account_1__r:External_ID__c] IS NOT NULL
		AND [rC_Bios__Contact_1__r:External_ID__c] IS NULL 
        AND [rC_Bios__Account_2__r:External_ID__c] IS NULL 
		AND [rC_Bios__Contact_2__r:External_ID__c] IS NULL 

		DELETE SUTTER_1P_MIGRATION.IMP.RELATIONSHIP
		WHERE [rC_Bios__Account_1__r:External_ID__c] IS NOT NULL
		AND [rC_Bios__Contact_1__r:External_ID__c] IS NULL 
        AND [rC_Bios__Account_2__r:External_ID__c] IS NULL 
		AND [rC_Bios__Contact_2__r:External_ID__c] IS NULL 

		SELECT * FROM SUTTER_1P_MIGRATION.IMP.RELATIONSHIP


END
  


  SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Constituents_v] WHERE re_db_id='EMCF-06307-079-0000019144' OR re_db_id='EMCF-06307-593-0000023485'
  OR [ImportID]='06307-593-0000023485' OR [RE_DB_Id]='EMCF-6194' OR [NEW_TGT_IMPORT_ID]='EMCF-6194'
  SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Ind_Relat_v] WHERE [RE_DB_Id_IRLink]='EMCF-6194'



SELECT [ConsID], [ImportID], [Name], [KeyInd], [RE_DB_Id], [NEW_TGT_IMPORT_ID], [RE_DB_OwnerShort]
 FROM [SUTTER_1P_DATA].dbo.[HC_Constituents_v]
WHERE [NEW_TGT_IMPORT_ID]='CVRX-LB143'

SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Ind_Relat_v] WHERE [RE_DB_Id]='CVRX-LB143'

SELECT * FROM [SUTTER_1P_DATA].[dbo].[NGOC_1P_FINAL_EXTRACT_HC1P1T]
WHERE [NEW_TGT_IMPORT_ID]='CVRX-LB143'

SELECT * FROM [TBL].[MDM_RECORDS_1P1T]
WHERE [NEW_TGT_IMPORT_ID]='CVRX-LB143'



SELECT [ConsID], [ImportID], [Name], [KeyInd], [RE_DB_Id], [NEW_TGT_IMPORT_ID], [RE_DB_OwnerShort]
 FROM [SUTTER_1P_DATA].dbo.[HC_Constituents_v]
WHERE [NEW_TGT_IMPORT_ID]='CVRX-TR2901'

SELECT * FROM [SUTTER_1P_DATA].[TBL].[NGOC_1P_FINAL_EXTRACT_HC1P1T]
WHERE [NEW_TGT_IMPORT_ID]='CVRX-TR2901'

SELECT * FROM [TBL].[MDM_RECORDS_1P1T]
WHERE [NEW_TGT_IMPORT_ID]='CVRX-TR2901'



	/* DNC 		
					--12,455 SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v
					-- select * from SUTTER_1P_MIGRATION.TBL.RELATIONSHIP_ORG_FINAL 
					-- select * from SUTTER_1P_DATA.dbo.HC_Org_Relat_v
					---select * from SUTTER_1P_DATA.dbo.CHART_RelatRecipType 
	
		--CONSTITUENT SOLICITORS-- WILL NOT BE CONVERTED AS RELATIONSHIPS
				--  LEAVE THE CODE FOR FUTURE MIGRATIONS FROM RE TO SALESFORCE	
			/*UNION ALL

			
						/*
						SELECT T1.KeyInd T1Key, T2.KeyInd T2Key, COUNT(*) 
						FROM SUTTER_1P_DATA.dbo.HC_Cons_Solicitor_v T
						INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
						INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T2 ON T.ASRLink=T2.ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
						GROUP BY T1.KeyInd, T2.KeyInd
						ORDER BY T1.KeyInd, T2.KeyInd
					
						T1Key	T2Key	(No column name)
						I	I	3380	=contact_1-contact_2
						I	O	9		=account_1-contact_2 (was contact-account but needed to change to comply with ngoc)
						O	I	733		=account_1-contact_2
						O	O	2		=account_1-account_2
						*/	
		
			SELECT DISTINCT
			 OwnerID=dbo.fnc_OwnerID()
			,RecordTypeID=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN dbo.fnc_RecordType('Relationship_Contact_Contact')
								WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN dbo.fnc_RecordType('Relationship_Account_Contact')  --need to revert from cont-acct TO acct-contac
								WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN dbo.fnc_RecordType('Relationship_Account_Contact')
								WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN dbo.fnc_RecordType('Relationship_Account_Account') END 
			,External_ID__c=T.RE_DB_OwnerShort+'-'+T.ASRImpID
			,RE_IRImpID__c=NULL
			,RE_ORImpID__c=NULL
			,RE_ASRImpID__c= T.RE_DB_OwnerShort+'-'+T.ASRImpID
			,RE_ESRImpID__c=NULL 
			,[rC_Bios__Account_1__r:External_ID__c]=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN NULL
										WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN (T2.NEW_TGT_IMPORT_ID)  --need to revert from cont-acct TO acct-contac
										WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN (T1.NEW_TGT_IMPORT_ID) 
										WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN (T1.NEW_TGT_IMPORT_ID) END 
			,[rC_Bios__Contact_1__r:External_ID__c]=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN (T1.NEW_TGT_IMPORT_ID) 
										WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN NULL
										WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN NULL
										WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN NULL END 
			,[rC_Bios__Account_2__r:External_ID__c]=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN NULL
										WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN NULL
										WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN NULL
										WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN (T2.NEW_TGT_IMPORT_ID) END 
			,[rC_Bios__Contact_2__r:External_ID__c]=CASE	WHEN (T1.KeyInd='I' AND T2.KeyInd='I') THEN (T2.NEW_TGT_IMPORT_ID)
										WHEN (T1.KeyInd='I' AND T2.KeyInd='O') THEN (T1.NEW_TGT_IMPORT_ID) --need to revert from cont-acct TO acct-contac
										WHEN (T1.KeyInd='O' AND T2.KeyInd='I') THEN (T2.NEW_TGT_IMPORT_ID)
										WHEN (T1.KeyInd='O' AND T2.KeyInd='O') THEN NULL END 
			,rC_Bios__Category__c=T3.RELATIONSHIP_CATEGORY
			,rC_Bios__Role_1__c=T3.RELATIONSHIP_ROLE_1
			,rC_Bios__Role_2__c=T3.RELATIONSHIP_ROLE_2
			,rC_Bios__Starting_Month__c=CASE WHEN LEN(T.ASRDateFrom)>7 THEN MONTH(T.ASRDateFrom) WHEN LEN(T.ASRDateFrom)=7 THEN LEFT(T.ASRDateFrom,2) WHEN LEN(T.ASRDateFrom)=4 THEN NULL END
			,rC_Bios__Starting_Day__c=CASE WHEN LEN(T.ASRDateFrom)>7 THEN DAY(T.ASRDateFrom) END
			,rC_Bios__Starting_Year__c=CASE WHEN LEN(T.ASRDateFrom)>7 THEN YEAR(T.ASRDateFrom)  WHEN LEN(T.ASRDateFrom)=7 THEN RIGHT(T.ASRDateFrom,4) WHEN LEN(T.ASRDateFrom)=4 THEN T.ASRDateFrom END
			,rC_Bios__Stopping_Month__c=CASE WHEN LEN(T.ASRDateTo)>7 THEN MONTH(T.ASRDateTo) WHEN LEN(T.ASRDateTo)=7 THEN LEFT(T.ASRDateTo,2) WHEN LEN(T.ASRDateTo)=4 THEN NULL END
			,rC_Bios__Stopping_Day__c=CASE WHEN LEN(T.ASRDateTo)>7 THEN DAY(T.ASRDateTo) END
			,rC_Bios__Stopping_Year__c=CASE WHEN LEN(T.ASRDateTo)>7 THEN YEAR(T.ASRDateTo)  WHEN LEN(T.ASRDateTo)=7 THEN RIGHT(T.ASRDateTo,4) WHEN LEN(T.ASRDateTo)=4 THEN T.ASRDateTo END
			,CASE  WHEN [dbo].[fn_DateConvert](CASE WHEN LEN(T.ASRDateTo)='8' THEN T.ASRDateTo ELSE NULL END) < GETDATE() THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Active__c
			,NULL AS rC_Bios__Primary__c
			,rC_Bios__Position__c=NULL
			,rC_Bios__Comments__c=CAST(T.ASRNotes AS VARCHAR(MAX)) 
			,Education_Relationship_Type__c=NULL
			,Education_Major__c=NULL
			,Education_Minor__c=NULL

			,Affiliation__c=T.RE_DB_OwnerShort
	 
				--ref
				,T1.KeyInd zrefT1_KeyInd, T1.ImportID zrefT1_ImportId
				,T2.KeyInd zrefT2_KeyInd, T2.ImportID zrefT2_ImportId
				,T.ASRImpID AS zrefRImpID, T.ASRLink zref_RLink
				,'Cons_Solicitor' AS zrefSource			
	 
			FROM SUTTER_1P_DATA.dbo.HC_Cons_Solicitor_v T
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T2 ON T.ASRLink=T2.ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_DATA.[dbo].[CHART_ConsSolicitor] T3 ON T.ASRLink=T3.ASRLink AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort AND T.ASRType=T3.ASRType
			WHERE T3.RELATIONSHIP_CATEGORY='Solicitor'
			SELECT * FROM  SUTTER_1P_DATA.[dbo].[CHART_ConsSolicitor]
			*/