USE SUTTER_1P_DATA
GO

BEGIN 
	 DROP TABLE SUTTER_1P_MIGRATION.TBL.ConsAppealDate_Ind
	 DROP TABLE SUTTER_1P_MIGRATION.TBL.ConsAppealDate_Org	 
END;
 

--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
--********************NEED TO ADD CODE PER ISSUE 247 (create camp mem on donor proxy on Org Cons, or use existing contact on ORg) 
--* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


BEGIN --CREATE BASE TABLE of INDIVIDUALS
	SELECT DISTINCT
		  	 LEFT(T2.NEW_TGT_IMPORT_ID, 4) AS RE_DB_OwnerShort
			,T2.NEW_TGT_IMPORT_ID
			,T3.PRIMARY_CAMPAIGN_SOURCE
			, CAST(T1.CAPDate AS DATE) AS CAPDate 
			INTO SUTTER_1P_MIGRATION.TBL.ConsAppealDate_Ind	
			FROM SUTTER_1P_DATA.dbo.HC_Cons_Appeal_v T1
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T2 ON T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort AND T1.ImportID=T2.ImportID
			INNER JOIN SUTTER_1P_MIGRATION.TBL.CAMPAIGN T3 ON T1.CAPAppealID=T3.AppealID AND T1.CAPPackageID=T3.PackageID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
			WHERE T2.KeyInd='I' 
			 
END;  --5773000  --  select count(*) from SUTTER_1P_DATA.dbo.HC_Cons_Appeal_v hc cons appeals 6,044,852

BEGIN--CHECK DUPES
		SELECT 
		T1.NEW_TGT_IMPORT_ID, T1.PRIMARY_CAMPAIGN_SOURCE, CAPDate
		FROM SUTTER_1P_MIGRATION.TBl.ConsAppealDate_Ind T1
		INNER JOIN
		 (SELECT NEW_TGT_IMPORT_ID, PRIMARY_CAMPAIGN_SOURCE 
						FROM SUTTER_1P_MIGRATION.TBl.ConsAppealDate_Ind T2 
						GROUP BY NEW_TGT_IMPORT_ID, PRIMARY_CAMPAIGN_SOURCE
						HAVING COUNT(*) >1) T2
		 ON T1.NEW_TGT_IMPORT_ID=T2.NEW_TGT_IMPORT_ID AND T1.PRIMARY_CAMPAIGN_SOURCE=T2.PRIMARY_CAMPAIGN_SOURCE
		ORDER BY NEW_TGT_IMPORT_ID, PRIMARY_CAMPAIGN_SOURCE, CAPDate DESC
END;


BEGIN --CREATE BASE TABLE of ORGANIZATIONS 
	SELECT DISTINCT
		  	 LEFT(T2.NEW_TGT_IMPORT_ID, 4) AS RE_DB_OwnerShort
			,T2.NEW_TGT_IMPORT_ID
			,T3.PRIMARY_CAMPAIGN_SOURCE
			, CAST(T1.CAPDate AS DATE) AS CAPDate 
			INTO SUTTER_1P_MIGRATION.TBL.ConsAppealDate_Org	
			FROM SUTTER_1P_DATA.dbo.HC_Cons_Appeal_v T1
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T2 ON T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort AND T1.ImportID=T2.ImportID
			INNER JOIN SUTTER_1P_MIGRATION.TBL.CAMPAIGN T3 ON T1.CAPAppealID=T3.AppealID AND T1.CAPPackageID=T3.PackageID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
			WHERE T2.KeyInd='O' 
			 
END;  --134488

	BEGIN--CHECK DUPES
		SELECT 
		T1.NEW_TGT_IMPORT_ID, T1.PRIMARY_CAMPAIGN_SOURCE, CAPDate
		FROM SUTTER_1P_MIGRATION.TBl.ConsAppealDate_Org T1
		INNER JOIN
		 (SELECT NEW_TGT_IMPORT_ID, PRIMARY_CAMPAIGN_SOURCE 
						FROM SUTTER_1P_MIGRATION.TBl.ConsAppealDate_Org T2 
						GROUP BY NEW_TGT_IMPORT_ID, PRIMARY_CAMPAIGN_SOURCE
						HAVING COUNT(*) >1) T2
		 ON T1.NEW_TGT_IMPORT_ID=T2.NEW_TGT_IMPORT_ID AND T1.PRIMARY_CAMPAIGN_SOURCE=T2.PRIMARY_CAMPAIGN_SOURCE
		ORDER BY NEW_TGT_IMPORT_ID, PRIMARY_CAMPAIGN_SOURCE, CAPDate DESC
	END;


BEGIN--CAMPAIGN MEMBER (from KEY_IND ="I") -  IMP with MULTI-SELECT PICKLIST
		SELECT DISTINCT
				 X1.ID ContactId
				,X2.ID CampaignId
				,T.RE_DB_OwnerShort AS Affiliation__c
				,STUFF(( SELECT '; ' + CAST(T1.CAPDate AS VARCHAR(10))
					FROM SUTTER_1P_MIGRATION.TBL.ConsAppealDate_Ind T1
						WHERE T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID AND T1.PRIMARY_CAMPAIGN_SOURCE=T.PRIMARY_CAMPAIGN_SOURCE
						GROUP BY T1.CAPDate ORDER BY T1.CAPDate DESC
					FOR
						XML PATH('')
							), 1, 1, '') AS RE_Cons_Appeal_Date__c
				--reference
					,T.NEW_TGT_IMPORT_ID AS zrefNewImportId
					,T.PRIMARY_CAMPAIGN_SOURCE zrefCampaign
		INTO SUTTER_1P_MIGRATION.TBL.CAMPAIGN_MEMBER    
		FROM SUTTER_1P_MIGRATION.TBL.ConsAppealDate_Ind T
		INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[CONTACT] AS X1 ON T.NEW_TGT_IMPORT_ID = X1.[EXTERNAL_ID__C]
		INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[CAMPAIGN] AS X2 ON T.PRIMARY_CAMPAIGN_SOURCE = X2.[EXTERNAL_ID__C]
	
	UNION ALL 

		SELECT DISTINCT
				 X1.ID ContactId
				,X2.ID CampaignId
				,T.RE_DB_OwnerShort AS Affiliation__c
				,STUFF(( SELECT '; ' + CAST(T1.CAPDate AS VARCHAR(10))
					FROM SUTTER_1P_MIGRATION.TBL.ConsAppealDate_Org T1
						WHERE T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID AND T1.PRIMARY_CAMPAIGN_SOURCE=T.PRIMARY_CAMPAIGN_SOURCE
						GROUP BY T1.CAPDate ORDER BY T1.CAPDate DESC
					FOR
						XML PATH('')
							), 1, 1, '') AS RE_Cons_Appeal_Date__c
				--reference
					,T.NEW_TGT_IMPORT_ID AS zrefNewImportId
					,T.PRIMARY_CAMPAIGN_SOURCE zrefCampaign
  		FROM SUTTER_1P_MIGRATION.TBL.ConsAppealDate_Org T
		INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[ACCOUNT] AS X ON T.NEW_TGT_IMPORT_ID= X.[EXTERNAL_ID__C]
		INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[CONTACT] AS X1 ON X.ID = X1.[ACCOUNTID]
		INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[CAMPAIGN] AS X2 ON T.PRIMARY_CAMPAIGN_SOURCE = X2.[EXTERNAL_ID__C]
		ORDER BY T.NEW_TGT_IMPORT_ID, T.PRIMARY_CAMPAIGN_SOURCE
 
END; --5640018
			 
--CREATE INDEX TO SORT RECORDS
        BEGIN

			CREATE INDEX idx_campaign_member  ON SUTTER_1P_MIGRATION.TBL.CAMPAIGN_MEMBER(CampaignId, ContactId);
		
		END;

		DROP TABLE  SUTTER_1P_MIGRATION.IMP.CAMPAIGN_MEMBER

		SELECT [ContactId]
			  ,[CampaignId]
			  ,[Affiliation__c]
			  ,CAST([RE_Cons_Appeal_Date__c] AS VARCHAR(4000)) AS [RE_Cons_Appeal_Date__c]
			  ,[zrefNewImportId]
			  ,[zrefCampaign]
			  ,ROW_NUMBER() OVER (ORDER BY CampaignId, ContactId) Seq
		INTO SUTTER_1P_MIGRATION.IMP.CAMPAIGN_MEMBER
		FROM SUTTER_1P_MIGRATION.TBL.CAMPAIGN_MEMBER T1
		ORDER BY CampaignId, ContactId

		--split camp member
		SELECT * 
		INTO [SUTTER_1P_MIGRATION].IMP.[CAMPAIGN_MEMBER_part_1of6]
 		FROM [SUTTER_1P_MIGRATION].IMP.[CAMPAIGN_MEMBER]
		WHERE Seq <1000001
		ORDER BY CampaignId, ContactId
		GO

		SELECT * 
		INTO [SUTTER_1P_MIGRATION].IMP.[CAMPAIGN_MEMBER_part_2of6]
 		FROM [SUTTER_1P_MIGRATION].IMP.[CAMPAIGN_MEMBER]
		WHERE Seq >1000000 AND Seq <2000001
		ORDER BY CampaignId, ContactId
		GO

		SELECT * 
 		INTO [SUTTER_1P_MIGRATION].IMP.[CAMPAIGN_MEMBER_part_3of6]
 		FROM [SUTTER_1P_MIGRATION].IMP.[CAMPAIGN_MEMBER]
		WHERE Seq >2000000 AND Seq <3000001
		ORDER BY CampaignId, ContactId
		GO

		SELECT * 
		INTO [SUTTER_1P_MIGRATION].IMP.[CAMPAIGN_MEMBER_part_4of6]
 		FROM [SUTTER_1P_MIGRATION].IMP.[CAMPAIGN_MEMBER]
		WHERE Seq >3000000 AND Seq <4000001
		ORDER BY CampaignId, ContactId
		GO 

		SELECT * 
		INTO [SUTTER_1P_MIGRATION].IMP.[CAMPAIGN_MEMBER_part_5of6]
 		FROM [SUTTER_1P_MIGRATION].IMP.[CAMPAIGN_MEMBER]
		WHERE Seq >4000000 AND Seq <5000001
		ORDER BY CampaignId, ContactId
		GO 

		SELECT * 
 		INTO [SUTTER_1P_MIGRATION].IMP.[CAMPAIGN_MEMBER_part_6of6]
 		FROM [SUTTER_1P_MIGRATION].IMP.[CAMPAIGN_MEMBER]
		WHERE Seq >5000000
		ORDER BY CampaignId, ContactId
		GO 
-- 5640018
--Audits

BEGIN
		SELECT *, LEN(RE_Cons_Appeal_Date__c) l 
		FROM  SUTTER_1P_MIGRATION.IMP.CAMPAIGN_MEMBER  
		WHERE LEN(RE_Cons_Appeal_Date__c) >255
		ORDER BY l desc

		ALTER TABLE [SUTTER_1P_MIGRATION].IMP.CAMPAIGN_MEMBER
		ALTER COLUMN RE_Cons_Appeal_Date__c NVARCHAR(500)
END;

	BEGIN--CHECK DUPES
		SELECT 
		T1.[ContactId], T1.[CampaignId], T1.RE_Cons_Appeal_Date__c, T1.Affiliation__c
		FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN_MEMBER T1
		INNER JOIN
		 (SELECT T2.[ContactId], T2.[CampaignId], Affiliation__c 
						FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN_MEMBER T2 
						GROUP BY T2.[ContactId], T2.[CampaignId], Affiliation__c 
						HAVING COUNT(*) >1) T2
		 ON T1.[ContactId]=T2.[ContactId] AND T1.[CampaignId]=T2.[CampaignId] AND T1.Affiliation__c=T2.Affiliation__c
		ORDER BY  [ContactId], [CampaignId], Affiliation__c DESC
	END;

	BEGIN
		
		SELECT * FROM  SUTTER_1P_MIGRATION.IMP.CAMPAIGN_MEMBER  WHERE RE_Cons_Appeal_Date__c like '%;%'
		
		SELECT *, LEN(RE_Cons_Appeal_Date__c) l 
		FROM  SUTTER_1P_MIGRATION.IMP.CAMPAIGN_MEMBER  
		WHERE LEN(RE_Cons_Appeal_Date__c) >255
		ORDER BY l desc

	END;

	SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN_MEMBER -- 5,190,349


	SELECT 
	T1.[ContactId], T1.[ContactId], COUNT(*) C
	FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN_MEMBER T1
	GROUP BY T1.[ContactId], T1.[ContactId]
	ORDER BY c DESC 



/*old code
BEGIN--CAMPAIGN MEMBER

			SELECT  
			--,dbo.fnc_RecordType('CampaignMember_Standard') AS RecordTypeID --READ ONLY
			T1.RE_DB_OwnerShort+'-'+T1.ImportID AS [CONTACT:External_Id__c]
			,T3.PRIMARY_CAMPAIGN_SOURCE AS [CAMPAIGN:External_Id__c]
			,T1.RE_DB_OwnerShort AS Affiliation__c
			
			INTO SUTTER_1P_MIGRATION.IMP.CAMPAIGN_MEMBER
			FROM SUTTER_1P_DATA.DBO.HC_Cons_Appeal_v T1
			INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T2 ON T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort AND T1.ImportID=T2.ImportID
			INNER JOIN SUTTER_1P_MIGRATION.TBL.CAMPAIGN T3 ON T1.CAPAppealID=T3.AppealID AND T1.CAPPackageID=T3.PackageID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
			WHERE T2.KeyInd='I'    
			GROUP BY T1.RE_DB_OwnerShort, T1.ImportID, T3.PRIMARY_CAMPAIGN_SOURCE
			
END;
*/
 