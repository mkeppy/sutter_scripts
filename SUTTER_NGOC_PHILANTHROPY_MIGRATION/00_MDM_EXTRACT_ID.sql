	--sample MDM file
		SELECT * FROM dbo.MDM_SAMPLE_EXTRACT

	--assign duplicate group number and seq. TargetSeq is always 1. this is not the final target master records.  SourceSeq is assigned starting
	--   with 2 to follow the 1 assigned to the target records. 
		SELECT 
		DENSE_RANK() OVER (ORDER BY [ TGT_IMPORT_ID]) AS GroupNumber, 
		1 AS TargetSeq,
		[ TGT_IMPORT_ID], [ TGT_FULL_NM], [ TGT_RECORD_TYPE], [ TGT_MDM_PTY_ID],
		ROW_NUMBER() over (PARTITION BY [ TGT_IMPORT_ID] ORDER BY [ TGT_IMPORT_ID]) + 1 AS SourceSeq, 
		[ SRC_IMPORT_ID], [ SRC_FULL_NM], [ SRC_RECORD_TYPE], [ SRC_MDM_PTY_ID]
		INTO dbo.MDM_SAMPLE_GRNO   
		FROM dbo.MDM_SAMPLE_EXTRACT
		ORDER BY [ TGT_IMPORT_ID]


		DROP TABLE dbo.MDM_SAMPLE_GRNO

		SELECT * FROM dbo.MDM_SAMPLE_GRNO ORDER BY groupnumber, sourceseq

	--assemble list by group in sequential order. 
	
		SELECT DISTINCT [ TGT_MDM_PTY_ID] MDM_ID, [GROUPNUMBER], [TargetSeq] AS Seq,  [ TGT_IMPORT_ID] IMPORT_ID, [ TGT_FULL_NM] FULL_NAME, [ TGT_RECORD_TYPE] RECORDTYPE
		INTO dbo.MDM_SAMPLE_APPEND
		FROM dbo.MDM_SAMPLE_GRNO 
		--WHERE GROUPNUMBER ='49'
		UNION ALL
		SELECT DISTINCT [ SRC_MDM_PTY_ID] MDM_ID, [GROUPNUMBER], [SourceSeq] AS Seq, [ SRC_IMPORT_ID] IMPORT_ID, [ SRC_FULL_NM] FULL_NAME, [ SRC_RECORD_TYPE] RECORDTYPE
		FROM dbo.MDM_SAMPLE_GRNO 
		--WHERE GROUPNUMBER ='49'
		ORDER BY GROUPnumber, seq
		
		SELECT * FROM dbo.MDM_SAMPLE_APPEND 
		WHERE groupnumber = 233 
		ORDER BY seq ASC
		
		
		