-- SELECT * FROM [SUTTER_1P_MIGRATION].[dbo].[conv_to_sust_gift]
-- SELECT TOP 10 * FROM SUTTER_1P_MIGRATION.TBL.GIFT 	

-- SELECT TOP 10 * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Link_v]															  
	
	SELECT * FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY] WHERE [EXTERNAL_ID__C] LIKE '%00001-553-0000181537'
	
	SELECT	T1.*, 
			T2.[ACCOUNT:External_Id__c], T2.[rC_Giving__Parent__r:External_ID__c], T2.[External_Id__c]	,
 			T3.[ACCOUNT:External_Id__c], T3.[rC_Giving__Parent__r:External_ID__c], T3.[External_Id__c]
	FROM [SUTTER_1P_MIGRATION].[dbo].[conv_to_sust_gift] AS T1
	LEFT JOIN [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_TRANSACTION] AS T2 ON T1.[External Id] = T2.[External_Id__c]
	LEFT JOIN [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PARENT] AS T3 ON T1.[External Id] = T3.[External_Id__c]
 																								    																								    

	 				SELECT T1.*, T2.*
					FROM [SUTTER_1P_MIGRATION].[dbo].[conv_to_sust_gift] AS T1
/*parent*/			INNER JOIN SUTTER_1P_MIGRATION.TBL.GIFT AS T2 ON T1.[RE GSplitImpID] = (T2.GSplitRE_DB_OwnerShort+'-GT-'+T2.GSplitImpID) 
/*transaction*/--	INNER JOIN SUTTER_1P_MIGRATION.TBL.GIFT AS T ON (T1.GFPaymentGFImpID = T.GSplitGFImpID AND T1.RE_DB_OwnerShort = T.GSplitRE_DB_OwnerShort
				


		SELECT   
		--parent id 
		T1.[RE_DB_OwnerShort], t1.[GFType], [T1].[GFImpID],    --recurr
		T1.[GFPaymentType],T1.[GFPaymentGFImpID]			   --payment
		,T3.GSplitRE_DB_OwnerShort, T3.[GSplitImpID]	AS ParentSplitImpID
		,T4.GSplitRE_DB_OwnerShort, T4.[GSplitImpID]	AS PaymentSplitImpID
 
		,T5.[External_Id__c] parent_external_id
		,T6.[External_Id__c] parentpossible_external_id
		,T7.[External_Id__c] trx_external_id

		,X5.ID newParentID
		,X6.ID oldParentID_to_delete
		,X7.ID TransactionID_to_move

		FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Link_v] AS T1
		INNER JOIN [SUTTER_1P_MIGRATION].[dbo].[conv_to_sust_gift] AS T2 ON (T1.[RE_DB_OwnerShort]+'-GT-'+T1.[GFPaymentGFImpID]) = T2.[RE GFIMPID]	--payment  
		INNER JOIN SUTTER_1P_MIGRATION.TBL.GIFT AS T3 ON T1.[GFImpID] = T3.GSplitGFImpID AND T1.RE_DB_OwnerShort = T3.GSplitRE_DB_OwnerShort			--parent split id
		INNER JOIN SUTTER_1P_MIGRATION.TBL.GIFT AS T4 ON T1.[GFPaymentGFImpID] = T4.GSplitGFImpID AND T1.RE_DB_OwnerShort = T4.GSplitRE_DB_OwnerShort	--payment split id

		LEFT JOIN [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PARENT] AS T5 ON  (T3.GSplitRE_DB_OwnerShort+'-GT-'+T3.[GSplitImpID])  = T5.[RE_GSplitImpID__c]
		LEFT JOIN [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PARENT] AS T6 ON  (T4.GSplitRE_DB_OwnerShort+'-GT-'+T4.[GSplitImpID])  = T6.[RE_GSplitImpID__c]
		LEFT JOIN [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_TRANSACTION] AS T7 ON  T6.[External_Id__c]=T7.[rC_Giving__Parent__r:External_ID__c]
    
		LEFT JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY] AS X5 ON (T5.[EXTERNAL_ID__C])= X5.[EXTERNAL_ID__C]
    	LEFT JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY] AS X6 ON (T6.[EXTERNAL_ID__C])= X6.[EXTERNAL_ID__C]
		LEFT JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY] AS X7 ON (T7.[EXTERNAL_ID__C])= X7.[EXTERNAL_ID__C]
		ORDER BY T5.[External_Id__c] ,T6.[External_Id__c] ,T7.[External_Id__c] 


		--1. move transactions from TransactionID_to_move TO newParentId
		--1.a. Step 1 will leave parents with no transactions, i.e. old_Parent_to_delete.
		--1.b. transactions should be marked as NON employee giving. 
		--2. delete old parents from old_ParentID_to_delete
		--3. parents shouild 


--sustainer to recurring employee giving with 'empl recurring gift' = true
	--create only donation table
	SELECT * 
	INTO [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_don] 
	FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_new] 
	WHERE RC_GIVING__PARENT__C IS null OR RC_GIVING__PARENT__C =''

	SELECT COUNT(*) FROM  [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_don] 
	
	--create only trx tbl
 	SELECT * 
	INTO [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_trx] 
	FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_new] 
	WHERE RC_GIVING__PARENT__C IS NOT NULL AND RC_GIVING__PARENT__C!=''

	SELECT COUNT(*) FROM  [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_trx] 

	
	SELECT T1.* 
	FROM [SUTTER_1P_MIGRATION].[dbo].[conv_to_emp_recur_gift] AS T1
	LEFT JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_trx] AS T3 ON T1.[External ID]=T3.RC_GIVING__PARENT__C
	--WHERE [T3].[RC_GIVING__PARENT__C] IS NOT NULL AND [T3].[RC_GIVING__PARENT__C]!=''	 -- 0 
	WHERE [T3].[RC_GIVING__PARENT__C] IS NULL OR [T3].[RC_GIVING__PARENT__C]=''	 --1906.
	
	  	
	
	DROP TABLE  [SUTTER_1P_MIGRATION].DBO.donations_orphan
	
	SELECT D.*, T.RC_GIVING__PARENT__C AS txt_RC_GIVING__PARENT__C
--	INTO [SUTTER_1P_MIGRATION].DBO.donations_orphan
	FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_don] AS D 
	LEFT JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_trx] AS T ON D.ID = T.RC_GIVING__PARENT__C
	WHERE (T.RC_GIVING__PARENT__C IS NULL OR T.RC_GIVING__PARENT__C ='') 
	AND D.RE_GIFT_TYPE__C LIKE '%recurring%'
	--AND (D.RE_PRIMPID__C IS NULL OR D.RE_PRIMPID__C ='') AND D.RE_GIFT_TYPE__C!='Gift-in-Kind' AND D.EXTERNAL_ID__C!=''
	ORDER BY D.[RE_GIFT_TYPE__C], D.RE_PRIMPID__C

	SELECT * FROM [SUTTER_1P_MIGRATION].dbo.donations_orphan

	DROP TABLE [SUTTER_1P_MIGRATION].IMP.DELETE_OPPORTUNITY_recurring

	SELECT DISTINCT T2.Id, T1.* 
	INTO [SUTTER_1P_MIGRATION].IMP.DELETE_OPPORTUNITY_recurring
	FROM [SUTTER_1P_MIGRATION].[dbo].[conv_to_emp_recur_gift] AS T1
	INNER JOIN [SUTTER_1P_MIGRATION].DBO.donations_orphan AS T2	 ON T1.[External ID]=T2.EXTERNAL_ID__C
	INNER JOIN [SUTTER_1P_DATA].dbo.[HC_Gift_Link_v] AS T3 ON T1.[RE GFImpID]=(T3.[RE_DB_OwnerShort]+'-GT-'+[T3].[GFImpID])  --ADDED TO ENSURE TO REMOVE THOSE THAT HAD PAYMENTS. IF NO PAYMENTS WERE ADDED, IT MEANS THAT THERE WAS AN AMMENDMENT
	
	SELECT * FROM [SUTTER_1P_MIGRATION].IMP.DELETE_OPPORTUNITY_recurring

	/*-- Delete data from Table1	
		DELETE Table1
		FROM Table1 t1
		INNER JOIN Table2 t2 ON t1.Col1 = t2.Col1
		WHERE t2.Col3 IN ('Two-Three','Two-Four')
		GO
	 */

		DELETE [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_don]
		FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_don] AS T1
		INNER JOIN  [SUTTER_1P_MIGRATION].IMP.DELETE_OPPORTUNITY_recurring AS T2 ON T1.ID = T2.ID
	
	--LIST OF RECURRING GITS THAT DID NOT HAVE ANY PAYMENT TOWARD THEM.
	    SELECT D.*, T.RC_GIVING__PARENT__C AS txt_RC_GIVING__PARENT__C
		INTO [SUTTER_1P_MIGRATION].DBO.donations_orphan_2
		FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_don] AS D 
		LEFT JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_trx] AS T ON D.ID = T.RC_GIVING__PARENT__C
		WHERE (T.RC_GIVING__PARENT__C IS NULL OR T.RC_GIVING__PARENT__C ='') 
		AND D.RE_GIFT_TYPE__C LIKE '%recurring%'
		--AND (D.RE_PRIMPID__C IS NULL OR D.RE_PRIMPID__C ='') AND D.RE_GIFT_TYPE__C!='Gift-in-Kind' AND D.EXTERNAL_ID__C!=''
		ORDER BY D.[RE_GIFT_TYPE__C], D.RE_PRIMPID__C


			/*  testing 
			
				SELECT * FROM [SUTTER_1P_MIGRATION].imp.[OPPORTUNITY_PARENT] WHERE [External_Id__c]='SRMC-GT-02594-553-0000286670'
				SELECT * FROM [SUTTER_1P_MIGRATION].imp.[OPPORTUNITY_TRANSACTION] WHERE [rC_Giving__Parent__r:External_ID__c]='SRMC-GT-02594-553-0000286670'

				SELECT * FROM [SUTTER_1P_DELTA].imp.[OPPORTUNITY_PARENT] WHERE [External_Id__c]='SRMC-GT-02594-553-0000286670'
				SELECT * FROM [SUTTER_1P_DELTA].imp.[OPPORTUNITY_TRANSACTION] WHERE [rC_Giving__Parent__r:External_ID__c]='SRMC-GT-02594-553-0000286670'

				SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_v] WHERE [GFImpID]='06307-545-0000043383'

				SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Link_v] WHERE [GFImpID]='00001-545-0000360938'

				SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Installments_v] WHERE [GFLink]='06307-545-0000043383'

				SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_WriteOff_v] WHERE [GFLink]='03038-057-0000191492'
				SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Adjustments_v] WHERE [GFImpID]='06307-545-0000043383'
				SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Amendments_v] WHERE [RGALink]='06307-545-0000043383'


				SELECT * FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_new] 
				WHERE [RE_GFIMPID__C] LIKE '%00001-545-0000370444%'
				OR [RE_GFIMPID__C] LIKE '%00001-545-0000370763%'
				OR [RE_GFIMPID__C] LIKE '%00001-545-0000363491%'
				OR [RE_GFIMPID__C] LIKE '%00001-545-0000365172%'
				OR [RE_GFIMPID__C] LIKE '%00001-545-0000363185%'
 
				SELECT * FROM [SUTTER_1P_MIGRATION].xtr.[OPPORTUNITY_new] WHERE id='0066100000ByLSDAA3' OR [RC_GIVING__PARENT__C]='0066100000ByLSDAA3'


				02594-545-0000294859
				02594-545-0000295145
				02594-545-0000295544
				02594-545-0000295915
				02594-545-0000296434
				02594-545-0000296816
			*/

--sustainer to recurring -- leave over donation with trx. 
   
	SELECT T1.* 
	FROM [SUTTER_1P_MIGRATION].[dbo].[conv_to_emp_recur_gift] AS T1
	LEFT JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_trx] AS T3 ON T1.[External ID]=T3.RC_GIVING__PARENT__C
	---WHERE [T3].[RC_GIVING__PARENT__C] IS NOT NULL AND [T3].[RC_GIVING__PARENT__C]!=''	 -- 0 
	WHERE [T3].[RC_GIVING__PARENT__C] IS NULL OR [T3].[RC_GIVING__PARENT__C]=''	 --1906.
	


	SELECT D.*, T.RC_GIVING__PARENT__C AS txt_RC_GIVING__PARENT__C
	INTO [SUTTER_1P_MIGRATION].DBO.donations_orphan_2
	FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_don] AS D 
	LEFT JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_trx] AS T ON D.ID = T.RC_GIVING__PARENT__C
	WHERE (T.RC_GIVING__PARENT__C IS NULL OR T.RC_GIVING__PARENT__C ='') 
	AND D.RE_GIFT_TYPE__C LIKE '%recurring%'
	--AND (D.RE_PRIMPID__C IS NULL OR D.RE_PRIMPID__C ='') AND D.RE_GIFT_TYPE__C!='Gift-in-Kind' AND D.EXTERNAL_ID__C!=''
	ORDER BY D.[RE_GIFT_TYPE__C], D.RE_PRIMPID__C

	SELECT DISTINCT T2.Id, T1.* 
	--INTO [SUTTER_1P_MIGRATION].IMP.DELETE_OPPORTUNITY_recurring
	FROM [SUTTER_1P_MIGRATION].[dbo].[conv_to_emp_recur_gift] AS T1
	INNER JOIN [SUTTER_1P_MIGRATION].DBO.donations_orphan_2 AS T2	 ON T1.[External ID]=T2.EXTERNAL_ID__C
	INNER JOIN [SUTTER_1P_DATA].dbo.[HC_Gift_Link_v] AS T3 ON T1.[RE GFImpID]=(T3.[RE_DB_OwnerShort]+'-GT-'+[T3].[GFImpID])  --ADDED TO ENSURE TO REMOVE THOSE THAT HAD PAYMENTS. IF NO PAYMENTS WERE ADDED, IT MEANS THAT THERE WAS AN AMMENDMENT

	
	--base table 

			SELECT DISTINCT 
			[X1].[ID] DonId, 
			[X1].[EXTERNAL_ID__C] DonExtId,
			[X1].[RE_GFIMPID__C] DonGFImpID,
			[X1].[RE_GSPLITIMPID__C] DonGFSplitImpId,
	
			[X2].[ID] TrxId, 
			[X2].[EXTERNAL_ID__C] TrxExternalId,
			[X2].[RE_GFIMPID__C] TrxGFImpID,
			[X2].[RE_GSPLITIMPID__C] TrxGFSplitImpId,
	  
  			T1.* 
			INTO [SUTTER_1P_MIGRATION].tbl.[conv_to_emp_recur_gift]
			FROM [SUTTER_1P_MIGRATION].[dbo].[conv_to_emp_recur_gift] AS T1
			INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_don] AS X1 ON [T1].[External ID]=[X1].[EXTERNAL_ID__C]
			INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_trx] AS X2 ON X1.[ID] = X2.[RC_GIVING__PARENT__C]
 			ORDER BY x1.id, x2.id
	
	 --steps to re-migrate.
	 -- 1. delete trx where external id is null// trx were created by record touch process.
	 -- 2. update trx external Id to add "T-". 
	 -- 3. use trx info to create and upsert don where External id is the same but w/o "t-"
	 -- 5. Update trx to move to new parent
	 -- 6. delte orphan donation.

	 -- 1. delete trx where external id is null// trx were created by record touch process.

		 SELECT TrxID AS ID, TrxExternalId, TrxGFImpID, TrxGFSplitImpID, DonID, DonExtId 
		 INTO [SUTTER_1P_MIGRATION].IMP.[conv_to_emp_recur_gift_1_delete_trx]		   -- DROP TABLE  [SUTTER_1P_MIGRATION].IMP.[conv_to_emp_recur_gift_0_delete_trx]
		 FROM [SUTTER_1P_MIGRATION].tbl.[conv_to_emp_recur_gift]
		 WHERE TrxExternalId IS NULL OR TrxExternalId =''
			--1274
			 SELECT * FROM  [SUTTER_1P_MIGRATION].IMP.[conv_to_emp_recur_gift_1_delete_trx]

	 -- 2. update trx external Id to add "T-". 
		 SELECT TrxID AS ID,
				'T-'+ TrxExternalId AS External_Id__c, 
				'T-'+ TrxGFSplitImpId AS  RE_GSPLITIMPID__C, 
				'T-'+TrxGFImpID AS RE_GFIMPID__C, 
				'TRUE' AS Employee_Recurring_Gift__c,
				TrxExternalId AS oldTrxExternalId, 
				TrxGFImpID AS oldTrxGFImpID, 
				TrxGFSplitImpID AS oldTrxGFSplitImpID, 
				DonID, DonExtId 
		 INTO [SUTTER_1P_MIGRATION].IMP.[conv_to_emp_recur_gift_2_update_trx_extid]		   -- DROP TABLE  [SUTTER_1P_MIGRATION].IMP.[conv_to_emp_recur_gift_1_update_trx_extid]
		 FROM [SUTTER_1P_MIGRATION].tbl.[conv_to_emp_recur_gift]
		 WHERE TrxExternalId IS NOT NULL AND TrxExternalId !=''
			--3040
			 SELECT * FROM  [SUTTER_1P_MIGRATION].IMP.[conv_to_emp_recur_gift_2_update_trx_extid]
		
	 -- 3. use trx info to create DON where External id is the same but w/o "t-"

		BEGIN
		   SELECT DISTINCT 
			 T2.[OwnerID]
 			,T2.[External_Id__c]
			,T2.[RE_GSplitImpID__c]
			,T2.[RE_GFImpID__c]
 			,T2.[ACCOUNT:External_Id__c]
			,T2.[CloseDate]
			,T2.[Gift_Date__c]
			,NAME = CASE WHEN T2.[NAME] LIKE '%transaction%' THEN REPLACE(T2.NAME,'Transaction','Donation')	ELSE T2.[NAME] END	
 			,T3.[RecordTypeID]
			,T3.[CAMPAIGN:External_Id__c]
			,T2.[StageName]
			,'Donation' AS rC_Giving__Activity_Type__c
			,T2.[rC_Giving__Affiliation__c]
			,T2.[Fundraising_Program__c]
			,'TRUE' AS [Employee_Recurring_Gift__c]
			,T2.[Amount] AS [rC_Giving__Giving_Amount__c]
			,T2.[Amount] AS [rC_Giving__Expected_Giving_Amount__c]
 			,T2.[rC_Giving__Current_Giving_Amount__c]
			,T2.[Amount] AS [rC_Giving__Projected_Amount__c]
			,T2.[Amount] AS [rC_Giving__Allocation_Amount__c]
			,T3.[rC_Giving__GAU__r:rC_Giving__External_ID__c]
			,'One Payment' AS [rC_Giving__Giving_Frequency__c]
			,'One Payment' AS [rC_Giving__Payment_Frequency__c]
			,NULL AS [rC_Giving__Payment_Count__c]
			,'TRUE' AS [rC_Giving__Is_Giving__c]
			,'TRUE' AS [rC_Giving__Is_Giving_Donation__c]
			,'FALSE' AS [rC_Giving__Is_Sustainer__c]
			,GETDATE() AS [rC_Giving__Update_Transactions__c]
			,GETDATE() AS [rC_Giving__Rollup_Transactions__c]
			,GETDATE() AS [rC_Giving__Rollup_Giving__c]
			,T2.[rC_Giving__Payment_Method__c]
			,T2.[rC_Giving__Payment_Method_Selected__r:External_ID__c]
			,'1' AS [rC_Giving__Giving_Years__c]
 			,T2.[RE_Legacy_Financial_Code__c]
			,T2.[RE_Batch_Number__c]
			,T2.[rC_Giving__Acknowledged__c]
			,T2.[rC_Giving__Acknowledged_Date__c]
			,T2.[rC_Giving__Is_Anonymous__c]
			,T2.[rC_Giving__Check_Date__c]
			,T2.[rC_Giving__Check_Number__c]
			,T2.[Description]
			,T2.[Stock_Issuer__c]
			,T2.[Stock_Median_Price__c]
			,T2.[rC_Giving__Number_Of_Shares__c]
			,T2.[rC_Giving__Ticker_Symbol__c]
			,T2.[Production__c]
			,T2.[CreatedDate]
			,T2.[RE_Gift_Constituency_Code__c]
			,T2.[RE_Letter_Code__c]
			,T2.[RE_GFPayMeth__c]
			,T2.[RE_Pledge_Balance__c]
			,T2.[RE_Gift_Type__c]
			,T2.[RE_Gift_SubType__c]
			,T2.[DAF_Pledge_Payment__c]
			,T2.[Donation_Page_Name__c]
			,T2.[Donor_Recognition_Name__c]
			,T2.[Gift_Comments__c]
			,T2.[NCHX_Source_Entity__c]
			,T2.[Non_Donation_Payment__c]
			,T2.[Original_Pledge_Amount__c]
			,T2.[Plaque_Wording__c]
			,T2.[Research_Payment__c]
			,T2.[Transaction_ID__c]
			,T2.[Tribute_In_Honor_of__c]
			,T2.[Tribute_In_Memory_of__c]
			,T2.[Hours_Per_Period__c]
			,T2.[Anticipated_Payment_Type__c]
			,T2.[Considering_bequest_in__c]
			,T2.[Donor_Designation_Fund__c]
			,T2.[Exceed_Appeal__c]
			,T2.[Exceed_Batch_Number__c]
			,T2.[Exceed_Campaign__c]
			,T2.[Former_Appeal__c]
			,T2.[Former_Campaign__c]
			,T2.[Former_ConCode__c]
			,T2.[Former_Fund__c]
			,T2.[Former_Package__c]
			,T2.[Gift_in_kind__c]
			,T2.[Hour_Club__c]
			,T2.[Lights_of_Love_FY14_RSVP__c]
			,T2.[Physician_Engagement__c]
			,T2.[Recognition__c]
			,T2.[Report_Exclusion__c]
			,T2.[Selected_Naming_Opporunity__c]
			,T2.[Sobrato_Challenge__c]
			,T2.[Source_of_notification__c]
			,T2.[SphereGiftIdentifier__c]
			,T2.[Adjusted_Date__c]
			,T2.[Adjustment_Post_Date__c]
			,T2.[Adjustment_Post_Status__c]
			,T2.[Irregular__c]
 			,T2.[RE_Split_Gift__c]
			,T2.[GiftSubType__c]
			,T2.[Post_Date__c]
			,T2.[Posted_Status__c]
  			,T2.[zrefGFType]
			,T2.[zrefParentGFImpID]
			,T2.[zrefPaymentGFImpID]
			,T2.[zrefPaymentGSplitImpID]
			,T2.[zrefRE_DB_Owner]
			,T2.[zrefGLinkAmt]
			,T2.[zrefmyID]
			,T2.[zrefNewExternal_Id__c]	 
  			,T1.Id AS zrefTrxId
			,T1.EXTERNAL_id__c AS zrefTrxExtenalId
		   INTO [SUTTER_1P_MIGRATION].[IMP].[conv_to_emp_recur_gift_3_insert_don]			 -- DROP TABLE [SUTTER_1P_MIGRATION].[IMP].[conv_to_emp_recur_gift_3_create_don]
		   FROM [SUTTER_1P_MIGRATION].IMP.[conv_to_emp_recur_gift_2_update_trx_extid] AS T1 
		   INNER JOIN [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_TRANSACTION] AS T2 ON T1.oldTrxExternalId = T2.[External_Id__c]
		   INNER JOIN [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PARENT] AS T3 ON T2.[rC_Giving__Parent__r:External_ID__c] = T3.[External_Id__c]
		  --eof	--- 339 UNIQUE EXTER ID of  464 total (125 below)
	
	
		UNION 

		 
		   SELECT DISTINCT 
			 T2.[OwnerID]
 			,T2.[External_Id__c]
			,T2.[RE_GSplitImpID__c]
			,T2.[RE_GFImpID__c]
 			,T2.[ACCOUNT:External_Id__c]
			,T2.[CloseDate]
			,T2.[Gift_Date__c]
			,NAME = CASE WHEN T2.[NAME] LIKE '%transaction%' THEN REPLACE(T2.NAME,'Transaction','Donation')	ELSE T2.[NAME] END	
 			,T3.[RecordTypeID]
			,T3.[CAMPAIGN:External_Id__c]
			,T2.[StageName]
			,'Donation' AS rC_Giving__Activity_Type__c
			,T2.[rC_Giving__Affiliation__c]
			,T2.[Fundraising_Program__c]
			,'TRUE' AS [Employee_Recurring_Gift__c]
			,T2.[Amount] AS [rC_Giving__Giving_Amount__c]
			,T2.[Amount] AS [rC_Giving__Expected_Giving_Amount__c]
 			,T2.[rC_Giving__Current_Giving_Amount__c]
			,T2.[Amount] AS [rC_Giving__Projected_Amount__c]
			,T2.[Amount] AS [rC_Giving__Allocation_Amount__c]
			,T4.[GAU_External_Id__c] AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]
			,'One Payment' AS [rC_Giving__Giving_Frequency__c]
			,'One Payment' AS [rC_Giving__Payment_Frequency__c]
			,NULL AS [rC_Giving__Payment_Count__c]
			,'TRUE' AS [rC_Giving__Is_Giving__c]
			,'TRUE' AS [rC_Giving__Is_Giving_Donation__c]
			,'FALSE' AS [rC_Giving__Is_Sustainer__c]
			,GETDATE() AS [rC_Giving__Update_Transactions__c]
			,GETDATE() AS [rC_Giving__Rollup_Transactions__c]
			,GETDATE() AS [rC_Giving__Rollup_Giving__c]
			,T2.[rC_Giving__Payment_Method__c]
			,T2.[rC_Giving__Payment_Method_Selected__r:External_ID__c]
			,'1' AS [rC_Giving__Giving_Years__c]
 			,T2.[RE_Legacy_Financial_Code__c]
			,T2.[RE_Batch_Number__c]
			,T2.[rC_Giving__Acknowledged__c]
			,T2.[rC_Giving__Acknowledged_Date__c]
			,T2.[rC_Giving__Is_Anonymous__c]
			,T2.[rC_Giving__Check_Date__c]
			,T2.[rC_Giving__Check_Number__c]
			,T2.[Description]
			,T2.[Stock_Issuer__c]
			,T2.[Stock_Median_Price__c]
			,T2.[rC_Giving__Number_Of_Shares__c]
			,T2.[rC_Giving__Ticker_Symbol__c]
			,T2.[Production__c]
			,T2.[CreatedDate]
			,T2.[RE_Gift_Constituency_Code__c]
			,T2.[RE_Letter_Code__c]
			,T2.[RE_GFPayMeth__c]
			,T2.[RE_Pledge_Balance__c]
			,T2.[RE_Gift_Type__c]
			,T2.[RE_Gift_SubType__c]
			,T2.[DAF_Pledge_Payment__c]
			,T2.[Donation_Page_Name__c]
			,T2.[Donor_Recognition_Name__c]
			,T2.[Gift_Comments__c]
			,T2.[NCHX_Source_Entity__c]
			,T2.[Non_Donation_Payment__c]
			,T2.[Original_Pledge_Amount__c]
			,T2.[Plaque_Wording__c]
			,T2.[Research_Payment__c]
			,T2.[Transaction_ID__c]
			,T2.[Tribute_In_Honor_of__c]
			,T2.[Tribute_In_Memory_of__c]
			,T2.[Hours_Per_Period__c]
			,T2.[Anticipated_Payment_Type__c]
			,T2.[Considering_bequest_in__c]
			,T2.[Donor_Designation_Fund__c]
			,T2.[Exceed_Appeal__c]
			,T2.[Exceed_Batch_Number__c]
			,T2.[Exceed_Campaign__c]
			,T2.[Former_Appeal__c]
			,T2.[Former_Campaign__c]
			,T2.[Former_ConCode__c]
			,T2.[Former_Fund__c]
			,T2.[Former_Package__c]
			,T2.[Gift_in_kind__c]
			,T2.[Hour_Club__c]
			,T2.[Lights_of_Love_FY14_RSVP__c]
			,T2.[Physician_Engagement__c]
			,T2.[Recognition__c]
			,T2.[Report_Exclusion__c]
			,T2.[Selected_Naming_Opporunity__c]
			,T2.[Sobrato_Challenge__c]
			,T2.[Source_of_notification__c]
			,T2.[SphereGiftIdentifier__c]
			,T2.[Adjusted_Date__c]
			,T2.[Adjustment_Post_Date__c]
			,T2.[Adjustment_Post_Status__c]
			,T2.[Irregular__c]
 			,T2.[RE_Split_Gift__c]
			,T2.[GiftSubType__c]
			,T2.[Post_Date__c]
			,T2.[Posted_Status__c]
  			,T2.[zrefGFType]
			,T2.[zrefParentGFImpID]
			,T2.[zrefPaymentGFImpID]
			,T2.[zrefPaymentGSplitImpID]
			,T2.[zrefRE_DB_Owner]
			,T2.[zrefGLinkAmt]
			,T2.[zrefmyID]
			,T2.[zrefNewExternal_Id__c]	 
  			,T1.Id AS zrefTrxId
			,T1.EXTERNAL_id__c AS zrefTrxExtenalId
	 	   FROM [SUTTER_1P_MIGRATION].IMP.[conv_to_emp_recur_gift_2_update_trx_extid] AS T1 
		   INNER JOIN [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_TRANSACTION] AS T2 ON T1.oldTrxExternalId = T2.[External_Id__c]
		   INNER JOIN [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT_missing] AS T3 ON T2.[rC_Giving__Parent__r:External_ID__c] = T3.[External_Id__c]
		   INNER JOIN [SUTTER_1P_MIGRATION].TBL.[GIFT] AS T4 ON T3.[External_Id__c]= (T4.[GSplitRE_DB_OwnerShort]+'-'+T4.[GSplitRE_DB_Tbl]+'-'+T4.GSplitImpID)
						 							 
		  --eof	--- 125 UNIQUE EXTER ID of  464 total (339 above)

		UNION  
		   SELECT DISTINCT 
			 T2.[OwnerID]
 			,T2.[External_Id__c]
			,T2.[RE_GSplitImpID__c]
			,T2.[RE_GFImpID__c]
 			,T2.[ACCOUNT:External_Id__c]
			,T2.[CloseDate]
			,T2.[Gift_Date__c]
			,NAME = CASE WHEN T2.[NAME] LIKE '%transaction%' THEN REPLACE(T2.NAME,'Transaction','Donation')	ELSE T2.[NAME] END	
 			,T3.[RecordTypeID]
			,T4.CAMPAIGN_External_Id__c AS [CAMPAIGN:External_Id__c]
			,T2.[StageName]
			,'Donation' AS rC_Giving__Activity_Type__c
			,T2.[rC_Giving__Affiliation__c]
			,T2.[Fundraising_Program__c]
			,'TRUE' AS [Employee_Recurring_Gift__c]
			,T2.[Amount] AS [rC_Giving__Giving_Amount__c]
			,T2.[Amount] AS [rC_Giving__Expected_Giving_Amount__c]
 			,T2.[rC_Giving__Current_Giving_Amount__c]
			,T2.[Amount] AS [rC_Giving__Projected_Amount__c]
			,T2.[Amount] AS [rC_Giving__Allocation_Amount__c]
			,T4.[GAU_External_Id__c] AS  [rC_Giving__GAU__r:rC_Giving__External_ID__c]
			,'One Payment' AS [rC_Giving__Giving_Frequency__c]
			,'One Payment' AS [rC_Giving__Payment_Frequency__c]
			,NULL AS [rC_Giving__Payment_Count__c]
			,'TRUE' AS [rC_Giving__Is_Giving__c]
			,'TRUE' AS [rC_Giving__Is_Giving_Donation__c]
			,'FALSE' AS [rC_Giving__Is_Sustainer__c]
			,GETDATE() AS [rC_Giving__Update_Transactions__c]
			,GETDATE() AS [rC_Giving__Rollup_Transactions__c]
			,GETDATE() AS [rC_Giving__Rollup_Giving__c]
			,T2.[rC_Giving__Payment_Method__c]
			,T2.[rC_Giving__Payment_Method_Selected__r:External_ID__c]
			,'1' AS [rC_Giving__Giving_Years__c]
 			,T2.[RE_Legacy_Financial_Code__c]
			,T2.[RE_Batch_Number__c]
			,T2.[rC_Giving__Acknowledged__c]
			,T2.[rC_Giving__Acknowledged_Date__c]
			,T2.[rC_Giving__Is_Anonymous__c]
			,T2.[rC_Giving__Check_Date__c]
			,T2.[rC_Giving__Check_Number__c]
			,T2.[Description]
			,T2.[Stock_Issuer__c]
			,T2.[Stock_Median_Price__c]
			,T2.[rC_Giving__Number_Of_Shares__c]
			,T2.[rC_Giving__Ticker_Symbol__c]
			,T2.[Production__c]
			,T2.[CreatedDate]
			,T2.[RE_Gift_Constituency_Code__c]
			,T2.[RE_Letter_Code__c]
			,T2.[RE_GFPayMeth__c]
			,T2.[RE_Pledge_Balance__c]
			,T2.[RE_Gift_Type__c]
			,T2.[RE_Gift_SubType__c]
			,T2.[DAF_Pledge_Payment__c]
			,T2.[Donation_Page_Name__c]
			,T2.[Donor_Recognition_Name__c]
			,T2.[Gift_Comments__c]
			,T2.[NCHX_Source_Entity__c]
			,T2.[Non_Donation_Payment__c]
			,T2.[Original_Pledge_Amount__c]
			,T2.[Plaque_Wording__c]
			,T2.[Research_Payment__c]
			,T2.[Transaction_ID__c]
			,T2.[Tribute_In_Honor_of__c]
			,T2.[Tribute_In_Memory_of__c]
			,T2.[Hours_Per_Period__c]
			,T2.[Anticipated_Payment_Type__c]
			,NULL AS [Considering_bequest_in__c]
			,NULL AS [Donor_Designation_Fund__c]
			,NULL AS [Exceed_Appeal__c]
			,NULL AS [Exceed_Batch_Number__c]
			,NULL AS [Exceed_Campaign__c]
			,NULL AS [Former_Appeal__c]
			,NULL AS [Former_Campaign__c]
			,NULL AS [Former_ConCode__c]
			,NULL AS [Former_Fund__c]
			,NULL AS [Former_Package__c]
			,NULL AS [Gift_in_kind__c]
			,NULL AS [Hour_Club__c]
			,NULL AS [Lights_of_Love_FY14_RSVP__c]
			,NULL AS [Physician_Engagement__c]
			,NULL AS [Recognition__c]
			,NULL AS [Report_Exclusion__c]
			,NULL AS [Selected_Naming_Opporunity__c]
			,NULL AS [Sobrato_Challenge__c]
			,NULL AS [Source_of_notification__c]
			,NULL AS [SphereGiftIdentifier__c]
			,NULL AS [Adjusted_Date__c]
			,NULL AS [Adjustment_Post_Date__c]
			,NULL AS [Adjustment_Post_Status__c]
			,T2.[Irregular__c]
 			,T2.[RE_Split_Gift__c]
			,T2.[GiftSubType__c]
			,T2.[Post_Date__c]
			,T2.[Posted_Status__c]
  			,T2.[zrefGFType]
			,T2.[zrefParentGFImpID]
			,T2.[zrefPaymentGFImpID]
			,T2.[zrefPaymentGSplitImpID]
			,T2.[zrefRE_DB_Owner]
			,T2.[zrefGLinkAmt]
			,NULL AS [zrefmyID]
			,NULL AS [zrefNewExternal_Id__c]	 
  			,T1.Id AS zrefTrxId
			,T1.EXTERNAL_id__c AS zrefTrxExtenalId
		   FROM [SUTTER_1P_MIGRATION].IMP.[conv_to_emp_recur_gift_2_update_trx_extid] AS T1
			INNER JOIN [SUTTER_1P_DELTA].[IMP].[OPPORTUNITY_TRANSACTION] AS T2 ON T1.oldTrxExternalId = T2.[External_Id__c]
			INNER JOIN [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PARENT] AS T3 ON T2.[rC_Giving__Parent__r:External_ID__c] = T3.[External_Id__c]
			LEFT JOIN [SUTTER_1P_DELTA].TBL.[GIFT] AS T4 ON T2.[External_Id__c]= (T4.[GSplitRE_DB_OwnerShort]+'-'+T4.[GSplitRE_DB_Tbl]+'-'+T4.GSplitImpID)
			 
		  --eof	---2573 UNIQUE EXTER ID of 2576 total (3 below)

		 UNION 
		   SELECT DISTINCT 
			 T2.[OwnerID]
 			,T2.[External_Id__c]
			,T2.[RE_GSplitImpID__c]
			,T2.[RE_GFImpID__c]
 			,T2.[ACCOUNT:External_Id__c]
			,T2.[CloseDate]
			,T2.[Gift_Date__c]
			,NAME = CASE WHEN T2.[NAME] LIKE '%transaction%' THEN REPLACE(T2.NAME,'Transaction','Donation')	ELSE T2.[NAME] END	
 			,T3.[RecordTypeID]
			,T4.CAMPAIGN_External_Id__c AS [CAMPAIGN:External_Id__c]
			,T2.[StageName]
			,'Donation' AS rC_Giving__Activity_Type__c
			,T2.[rC_Giving__Affiliation__c]
			,T2.[Fundraising_Program__c]
			,'TRUE' AS [Employee_Recurring_Gift__c]
			,T2.[Amount] AS [rC_Giving__Giving_Amount__c]
			,T2.[Amount] AS [rC_Giving__Expected_Giving_Amount__c]
 			,T2.[Amount] AS [rC_Giving__Current_Giving_Amount__c]
			,T2.[Amount] AS [rC_Giving__Projected_Amount__c]
			,T2.[Amount] AS [rC_Giving__Allocation_Amount__c]
			,T4.[GAU_External_Id__c] AS  [rC_Giving__GAU__r:rC_Giving__External_ID__c]
			,'One Payment' AS [rC_Giving__Giving_Frequency__c]
			,'One Payment' AS [rC_Giving__Payment_Frequency__c]
			,NULL AS [rC_Giving__Payment_Count__c]
			,'TRUE' AS [rC_Giving__Is_Giving__c]
			,'TRUE' AS [rC_Giving__Is_Giving_Donation__c]
			,'FALSE' AS [rC_Giving__Is_Sustainer__c]
			,GETDATE() AS [rC_Giving__Update_Transactions__c]
			,GETDATE() AS [rC_Giving__Rollup_Transactions__c]
			,GETDATE() AS [rC_Giving__Rollup_Giving__c]
			,T2.[rC_Giving__Payment_Method__c]
			,T2.[rC_Giving__Payment_Method_Selected__r:External_ID__c]
			,'1' AS [rC_Giving__Giving_Years__c]
 			,T2.[RE_Legacy_Financial_Code__c]
			,T2.[RE_Batch_Number__c]
			,T2.[rC_Giving__Acknowledged__c]
			,T2.[rC_Giving__Acknowledged_Date__c]
			,T2.[rC_Giving__Is_Anonymous__c]
			,T2.[rC_Giving__Check_Date__c]
			,T2.[rC_Giving__Check_Number__c]
			,T2.[Description]
			,T2.[Stock_Issuer__c]
			,T2.[Stock_Median_Price__c]
			,T2.[rC_Giving__Number_Of_Shares__c]
			,T2.[rC_Giving__Ticker_Symbol__c]
			,T2.[Production__c]
			,T2.[CreatedDate]
			,T2.[RE_Gift_Constituency_Code__c]
			,T2.[RE_Letter_Code__c]
			,T2.[RE_GFPayMeth__c]
			,T2.[RE_Pledge_Balance__c]
			,T2.[RE_Gift_Type__c]
			,T2.[RE_Gift_SubType__c]
			,T2.[DAF_Pledge_Payment__c]
			,T2.[Donation_Page_Name__c]
			,T2.[Donor_Recognition_Name__c]
			,T2.[Gift_Comments__c]
			,T2.[NCHX_Source_Entity__c]
			,T2.[Non_Donation_Payment__c]
			,T2.[Original_Pledge_Amount__c]
			,T2.[Plaque_Wording__c]
			,T2.[Research_Payment__c]
			,T2.[Transaction_ID__c]
			,T2.[Tribute_In_Honor_of__c]
			,T2.[Tribute_In_Memory_of__c]
			,T2.[Hours_Per_Period__c]
			,T2.[Anticipated_Payment_Type__c]
			,NULL AS [Considering_bequest_in__c]
			,NULL AS [Donor_Designation_Fund__c]
			,NULL AS [Exceed_Appeal__c]
			,NULL AS [Exceed_Batch_Number__c]
			,NULL AS [Exceed_Campaign__c]
			,NULL AS [Former_Appeal__c]
			,NULL AS [Former_Campaign__c]
			,NULL AS [Former_ConCode__c]
			,NULL AS [Former_Fund__c]
			,NULL AS [Former_Package__c]
			,NULL AS [Gift_in_kind__c]
			,NULL AS [Hour_Club__c]
			,NULL AS [Lights_of_Love_FY14_RSVP__c]
			,NULL AS [Physician_Engagement__c]
			,NULL AS [Recognition__c]
			,NULL AS [Report_Exclusion__c]
			,NULL AS [Selected_Naming_Opporunity__c]
			,NULL AS [Sobrato_Challenge__c]
			,NULL AS [Source_of_notification__c]
			,NULL AS [SphereGiftIdentifier__c]
			,NULL AS [Adjusted_Date__c]
			,NULL AS [Adjustment_Post_Date__c]
			,NULL AS [Adjustment_Post_Status__c]
			,T2.[Irregular__c]
 			,T2.[RE_Split_Gift__c]
			,T2.[GiftSubType__c]
			,T2.[Post_Date__c]
			,T2.[Posted_Status__c]
  			,T2.[zrefGFType]
			,T2.[zrefParentGFImpID]
			,T2.[zrefPaymentGFImpID]
			,T2.[zrefPaymentGSplitImpID]
			,T2.[zrefRE_DB_Owner]
			,T2.[zrefGLinkAmt]
			,NULL AS [zrefmyID]
			,NULL AS [zrefNewExternal_Id__c]	 
  			,T1.Id AS zrefTrxId
			,T1.EXTERNAL_id__c AS zrefTrxExtenalId
		   FROM [SUTTER_1P_MIGRATION].IMP.[conv_to_emp_recur_gift_2_update_trx_extid] AS T1
			INNER JOIN [SUTTER_1P_DELTA].[IMP].[OPPORTUNITY_TRANSACTION] AS T2 ON T1.oldTrxExternalId = T2.[External_Id__c]
			INNER JOIN [SUTTER_1P_DELTA].[IMP].[OPPORTUNITY_PARENT] AS T3 ON T2.[rC_Giving__Parent__r:External_ID__c] = T3.[External_Id__c]
			LEFT JOIN [SUTTER_1P_DELTA].TBL.[GIFT] AS T4 ON T2.[External_Id__c]= (T4.[GSplitRE_DB_OwnerShort]+'-'+T4.[GSplitRE_DB_Tbl]+'-'+T4.GSplitImpID)
			ORDER BY [External_Id__c]
		END --eof	---   3 UNIQUE EXTER ID of 2576 total (2573 above)	
	
		  SELECT * FROM [SUTTER_1P_MIGRATION].[IMP].[conv_to_emp_recur_gift_3_insert_don]

  	 -- 4. Update trx to move to new parent

		 SELECT zrefTrxId AS ID  
				,[External_ID__c] AS [rC_Giving__Parent__r:External_ID__c]
		 INTO [SUTTER_1P_MIGRATION].[IMP].[conv_to_emp_recur_gift_4_upsert_trx]  
		 FROM [SUTTER_1P_MIGRATION].[IMP].[conv_to_emp_recur_gift_3_create_don]
					 
	-- 5. delete orphan donation records
	
		SELECT  DISTINCT DonId AS ID, DonExtId  
		INTO [SUTTER_1P_MIGRATION].IMP.[conv_to_emp_recur_gift_5_delete_don]			
		FROM  [SUTTER_1P_MIGRATION].IMP.[conv_to_emp_recur_gift_2_update_trx_extid]	
		
			SELECT * FROM [SUTTER_1P_MIGRATION].IMP.[conv_to_emp_recur_gift_5_delete_don]

				DonID				DonExtId
				0066100000CMdqQAAT	CVRX-GT-00001-553-0000189288
				0066100000CMehoAAD	CVRX-GT-00001-553-0000191359
				0066100000CMeiDAAT	CVRX-GT-00001-553-0000191360
				0066100000CMehpAAD	CVRX-GT-00001-553-0000191361
				0066100000CMehqAAD	CVRX-GT-00001-553-0000191362
				0066100000CMd5LAAT	CVRX-GT-00001-553-0000191363
				0066100000By4ipAAB	MPHF-GT-00001-553-0000376035
				0066100000BvAcSAAV	MPHF-GT-00001-553-0000376046
				0066100000By4gxAAB	MPHF-GT-00001-553-0000395197
				0066100000CMf5VAAT	SRMC-GT-02594-553-0000341595
				0066100000CMf65AAD	SRMC-GT-02594-553-0000341607
				0066100000CMf6AAAT	SRMC-GT-02594-553-0000341916
				0066100000BzGQMAA3	SCAH-GT-03038-553-0000277460
				0066100000BuIe8AAF	SCAH-GT-03038-553-0000277533

	   






		 
		
		 			/*  testing 
		
			
				SELECT * FROM [SUTTER_1P_MIGRATION].imp.[OPPORTUNITY_PARENT] WHERE [External_Id__c]='SRMC-GT-02594-553-0000286670'
				SELECT * FROM [SUTTER_1P_MIGRATION].imp.[OPPORTUNITY_TRANSACTION] WHERE [rC_Giving__Parent__r:External_ID__c]='SRMC-GT-02594-553-0000286670'

				SELECT * FROM [SUTTER_1P_DELTA].imp.[OPPORTUNITY_PARENT] WHERE [External_Id__c]='SRMC-GT-02594-553-0000286670'
				SELECT * FROM [SUTTER_1P_DELTA].imp.[OPPORTUNITY_TRANSACTION] WHERE [rC_Giving__Parent__r:External_ID__c]='SRMC-GT-02594-553-0000286670'

				SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_v] WHERE [GFImpID]='06307-545-0000043383'

				SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Link_v] WHERE [GFImpID]='00001-545-0000360938'

				SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Installments_v] WHERE [GFLink]='06307-545-0000043383'

				SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_WriteOff_v] WHERE [GFLink]='03038-057-0000191492'
				SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Adjustments_v] WHERE [GFImpID]='06307-545-0000043383'
				SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Amendments_v] WHERE [RGALink]='06307-545-0000043383'


				SELECT * FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_new] 
				WHERE [RE_GFIMPID__C] LIKE '%00001-545-0000370444%'
				OR [RE_GFIMPID__C] LIKE '%00001-545-0000370763%'
				OR [RE_GFIMPID__C] LIKE '%00001-545-0000363491%'
				OR [RE_GFIMPID__C] LIKE '%00001-545-0000365172%'
				OR [RE_GFIMPID__C] LIKE '%00001-545-0000363185%'
 
				SELECT * FROM [SUTTER_1P_MIGRATION].xtr.[OPPORTUNITY_new] WHERE id='0066100000ByLSDAA3' OR [RC_GIVING__PARENT__C]='0066100000ByLSDAA3'
		    
			*/
