--MDM ISSUES

--1.cases where a merged consittuent ended up being a 1 constituent records, and therefore if HH was setup, 
-- then it ended up as a HH in 1 relationship and a non-HH in the other relationship. see 
		--sample 1
			ABSF-018570	ABSF-018570
			ABSF-018570	SCAH-03038-079-0057566

		SELECT * FROM SUTTER_1P_DATA.dbo.HC_Constituents_v
		WHERE RE_DB_Id='ABSF-018570'
		OR RE_DB_Id='SCAH-03038-079-0057566'
		OR RE_DB_Id='SCAH-068570'

		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
		WHERE importid='ABSF-018570'
		OR importid='SCAH-03038-079-0057566'


		SELECT *
		FROM SUTTER_1P_DATA.[TBL].[NGOC_1P_FINAL_EXTRACT_HC1P1T]
		WHERE new_tgt_import_id='ABSF-018570'
		OR new_tgt_import_id='SCAH-03038-079-0057566'
		OR TGT_IMPORT_ID='ABSF-018570' OR SRC_IMPORT_Id='ABSF-018570'
		OR TGT_IMPORT_ID='SCAH-03038-079-0057566' OR SRC_IMPORT_ID='SCAH-03038-079-0057566'

		SELECT IRImpID, ImportId, ConsId, IRLink, IRIsHH, IRIsSpouse, IRDeceased, IRFirstName, IRLastName, RE_DB_ID, RE_DB_Id_CN, RE_DB_Id_IRLink, 
		NEW_TGT_IMPORT_ID_IR, NEW_TGT_IMPORT_ID_CN, NEW_TGT_IMPORT_ID_IRLINK
		FROM SUTTER_1P_DATA.dbo.hc_ind_relat_v
		WHERE re_db_id_cn='ABSF-018570' OR re_db_id_cn='SCAH-03038-079-0057566'
		OR re_db_id_cn='SCAH-068570' 
		OR RE_DB_Id_IRLink='ABSF-018570' OR RE_DB_Id_IRLink='SCAH-03038-079-0057566'
		OR RE_DB_Id_IRLink='SCAH-068570' 
		

		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
		WHERE importid='ABSF-018570'
		OR importid='SCAH-03038-079-0057566'
		OR ImportID='SCAH-068570'

		SELECT nohh_importId, COUNT(hh_importID) 
		FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
		GROUP BY nohh_importId
		HAVING COUNT(hh_importID)>1 AND nohh_importId IS not null
		ORDER BY nohh_importId 

		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH WHERE NoHH_ImportID='ABSF-M01975'

	SELECT * FROM SUTTER_1P_MIGRATION.imp.contact WHERE external_Id__c='ABSF-018570'
		
		SELECT * FROM SUTTER_1P_DATA.dbo.hc_constituents_v WHERE re_Db_id='ABSF-018570'



--1a.cases where a merged consittuent ended up being a 1 constituent records, and therefore if HH was setup, 
-- then it ended up as a HH in 1 relationship and a non-HH in the other relationship. see 
		--sample 1
CVRX-TR2362	SCAH-130409
CVRX-TR2362	CVRX-TR2363
 
	
		SELECT *
		FROM SUTTER_1P_DATA.[TBL].[NGOC_1P_FINAL_EXTRACT_HC1P1T]
		WHERE new_tgt_import_id='CVRX-TR2362'
		OR new_tgt_import_id='SCAH-130409' OR NEW_TGT_IMPORT_ID='CVRX-TR2363'
		OR TGT_IMPORT_ID='CVRX-TR2362'
		OR TGT_IMPORT_ID='SCAH-130409' OR TGT_IMPORT_ID='CVRX-TR2363'
		OR SRC_IMPORT_ID='CVRX-TR2362'
		OR SRC_IMPORT_ID='SCAH-130409' OR SRC_IMPORT_ID='CVRX-TR2363'


		SELECT * FROM SUTTER_1P_DATA.dbo.HC_Constituents_v
		WHERE RE_DB_Id='CVRX-TR2362'
		OR RE_DB_Id='SCAH-130409'
		OR RE_DB_Id='CVRX-TR2363'
		OR RE_DB_Id='SCAH-03038-593-0000235743'
		OR RE_DB_Id='SMCF-03947-700-0000059409'

		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
		WHERE ImportID='CVRX-TR2362'
		OR ImportID='SCAH-130409'
		OR ImportID='CVRX-TR2363'
		OR ImportID='SCAH-03038-593-0000235743'
		OR HH_ImportID='CVRX-TR2362'
		OR HH_ImportID='SCAH-130409'
		OR HH_ImportID='CVRX-TR2363'
		OR HH_ImportID='SCAH-03038-593-0000235743'
		OR NoHH_ImportID='CVRX-TR2362'
		OR NoHH_ImportID='SCAH-130409'
		OR NoHH_ImportID='CVRX-TR2363'
		OR NoHH_ImportID='SCAH-03038-593-0000235743'


		SELECT IRImpID, ImportId, ConsId, IRLink, IRIsHH, IRIsSpouse, IRDeceased, IRFirstName, IRLastName, RE_DB_ID, RE_DB_Id_CN, RE_DB_Id_IRLink, 
		NEW_TGT_IMPORT_ID_IR, NEW_TGT_IMPORT_ID_CN, NEW_TGT_IMPORT_ID_IRLINK
		FROM SUTTER_1P_DATA.dbo.hc_ind_relat_v
		WHERE RE_DB_Id_CN='CVRX-TR2362'
		OR RE_DB_Id_CN='SCAH-130409'
		OR RE_DB_Id_CN='CVRX-TR2363'
		OR RE_DB_Id_CN='SCAH-03038-593-0000235743'
		OR RE_DB_Id_CN='SMCF-03947-700-0000059409'
		OR RE_DB_ID='SMCF-03947-700-0000059409'
		

		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
		WHERE importid='ABSF-018570'
		OR importid='SCAH-03038-079-0057566'
		OR ImportID='SCAH-068570'

		SELECT nohh_importId, COUNT(hh_importID) 
		FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
		GROUP BY nohh_importId
		HAVING COUNT(hh_importID)>1 AND nohh_importId IS not null
		ORDER BY nohh_importId 

		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH WHERE NoHH_ImportID='ABSF-M01975'



		SELECT * FROM SUTTER_1P_MIGRATION.imp.contact WHERE external_Id__c='ABSF-018570'
		
		SELECT * FROM SUTTER_1P_DATA.dbo.hc_constituents_v WHERE re_Db_id='ABSF-018570'



--2 issue where 2 constituents are merged into 1 creating 2 HH relationships. the table tbl.contact_hofh will have only 1 records withe the HH and NoHH

		SELECT * FROM SUTTER_1P_MIGRATION.imp.account
		WHERE external_id__c='SCAH-071490' OR external_id__c='SCAH-03038-079-0058659'

		SELECT * FROM SUTTER_1P_MIGRATION.imp.account
		WHERE external_id__c='SCAH-071490'  
		
		SELECT * FROM SUTTER_1P_DATA.dbo.hc_Constituents_v 
		WHERE re_Db_id='SCAH-071490'
		OR re_Db_id='SRMC-02594-593-0000073854'

		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
		WHERE importid='SCAH-071490' OR importid='ABSF-02869-079-0027089'
		OR HH_ImporTID='SCAH-03038-079-0058659'	OR noHH_ImportID='SCAH-071490'

		SELECT * FROM SUTTER_1P_DATA.dbo.hc_ind_relat_v
		WHERE re_db_id_cn='SCAH-071490' OR re_db_id_cn='SCAH-03038-079-0058659'
		OR re_Db_id_cn='ABSF-02869-079-0027089'

--3. cases where the same Contact is mapped to 2 HH Accounts/ see contact exp file

	
	SELECT * FROM SUTTER_1P_MIGRATION.imp.contact WHERE external_id__c='CVRX-TR2362'

		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
		WHERE importid='CVRX-TR2362' 
		OR HH_ImporTID='CVRX-TR2362'	
		OR noHH_ImportID='CVRX-TR2362'

		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
		WHERE importid='CVRX-TR_2363' 
		OR HH_ImporTID='CVRX-TR_2363'	
		OR noHH_ImportID='CVRX-TR_2363'

		
--4. cases where a Contacts (from a non-cons ind relat is mapps to an account that seems not to be created.   see contacts excp file
	--IR-SCAH-03038-033-0023911		SCAH-071490
	--IR-SCAH-03038-033-0024999		SCAH-071490	


	--ind relat
		SELECT ImportID, IRImpID, irfirstname, irlastname, IRLink, re_Db_id, re_Db_id_cn, re_Db_id_irlink, new_Tgt_import_id_ir, new_Tgt_import_id_cn, new_Tgt_import_id_irlink
		FROM SUTTER_1P_data.dbo.hc_ind_relat_v
		WHERE re_db_id='SCAH-03038-033-0023911' --'SCAH-071490' 
		OR re_Db_id='SCAH-03038-033-0024999'	--'SCAH-071490' 
		OR re_db_id='SCAH-03038-033-0024615' --'SCAH-096097'
		ORDER BY re_db_id_cn

		SELECT keyind, importid, consId, firstname, lastname, re_Db_id, new_tgt_import_id, mdm_pty_id
		FROM SUTTER_1P_DATA.dbo.hc_constituents_v
		WHERE re_db_id='SCAH-071490' 
		OR re_db_id='SCAH-096097'
		ORDER BY re_db_id

		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
		WHERE importid='SCAH-071490' 
		OR HH_ImporTID='SCAH-071490'	
		OR noHH_ImportID='SCAH-071490'
		OR nonhhimpid='SCAH-071490'
		
		OR importid='SCAH-096097' 
		OR HH_ImporTID='SCAH-096097'	
		OR noHH_ImportID='SCAH-096097'
		OR nonhhimpid='SCAH-096097'
		
		SELECT * FROM SUTTER_1P_DATA.[TBL].[NGOC_1P_FINAL_EXTRACT_HC1P1T]
		WHERE new_tgt_import_id='SCAH-096097'
		OR new_tgt_import_id='SCAH-071490'

		SELECT * FROM SUTTER_1P_DATA.[TBL].[SF_CONSTITUENTS]
		WHERE ImportId='SCAH-096097'
		OR ImportId='SCAH-071490'

		SELECT * FROM SUTTER_1P_MIGRATION.imp.account
		WHERE EXTERNAL_id__c='SCAH-096097'
		OR EXTERNAL_id__c='SCAH-071490'



--issue with tbl.Contact_HofH

		SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
		WHERE importid='SCAH-071490' 
		OR HH_ImporTID='SCAH-071490'	
		OR noHH_ImportID='SCAH-071490'
		OR nonhhimpid='SCAH-071490'
		
		OR importid='SCAH-096097' 
		OR HH_ImporTID='SCAH-096097'	
		OR noHH_ImportID='SCAH-096097'
		OR nonhhimpid='SCAH-096097'
 

			--CHECK DUPLES
				SELECT *, 
				ROW_NUMBER() OVER ( PARTITION BY ImportID, NonHHImpID ORDER BY ImportID, NonHHImpID) AS Seq2
				FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH  
				WHERE importId IN (SELECT ImportID FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH  GROUP BY importid HAVING COUNT(*)>1) 
				AND importid='ABSF-046097' OR importid='SCAH-03038-079-0058659' OR importid='ABSF-00001-079-0108844'
				ORDER BY ImportID, NonHHImpID

				SELECT * FROM SUTTER_1P_MIGRATION.IMP.CONTACT
				WHERE [ACCOUNT:External_ID__c] ='SCAH-03038-079-0058659' 
				OR [ACCOUNT:External_ID__c] ='ABSF-046097'
				ORDER BY [ACCOUNT:External_ID__c] 

--DUPLICATE WITH NON-CONS INDIVIDUAL RELATIONSHIPS
		SELECT IRImpID, ImportId, ConsId, IRLInk, irishh, irfirstname, irlastname, re_db_id, re_Db_Id_cn, re_db_id_irlink, new_tgt_import_id_ir, new_tgt_import_id_cn, new_tgt_import_id_irlink
		FROM SUTTER_1P_DATA.dbo.hc_ind_relat_v 
		WHERE new_tgt_import_id_ir='WBRX-SL8606_P_1455' 

  		SELECT 
		'IR-'+T.NEW_TGT_IMPORT_ID_IR AS External_ID__c
		,CASE WHEN T2.HH_ImportID IS NOT NULL THEN T2.HH_ImportID ELSE T.NEW_TGT_IMPORT_ID_CN END AS [ACCOUNT:External_ID__c], T2.HH_ImportID, T2.NoHH_ImportID , T.NEW_TGT_IMPORT_ID_CN
		,T.NEW_TGT_IMPORT_ID_IR AS RE_IRImpID__c
		,T.IRImpID, T.ImportId, T.ConsId, T.IRLInk, T.irishh, T.irfirstname, T.irlastname, T.re_db_id, T.re_Db_Id_cn, T.re_db_id_irlink
		,T.new_tgt_import_id_ir, T.new_tgt_import_id_cn, T.new_tgt_import_id_irlink, T.RE_DB_ID_CN, T8.RE_DB_ID
		FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v T
		--INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T8 ON T.ImportID=T8.ImportID AND T.RE_DB_OwnerShort=T8.RE_DB_OwnerShort
		INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T8 ON T.re_Db_Id_cn=T8.re_Db_Id  
		INNER JOIN SUTTER_1P_DATA.TBL.SF_IND_RELAT T9 ON T.RE_DB_ID=T9.IRImpID    --(ORImpID is the NEW_TGT_IMPORT_ID_OR)
		LEFT JOIN SUTTER_1P_MIGRATION.TBL.Contact_HofH T2 ON T.ImportID=T2.NoHH_ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
		LEFT JOIN SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1_final T6 ON T.NEW_TGT_IMPORT_ID_IR=T6.NEW_TGT_IMPORT_ID_IR 
		LEFT JOIN SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2_final T7 ON T.NEW_TGT_IMPORT_ID_IR=T7.NEW_TGT_IMPORT_ID_IR 
		LEFT JOIN SUTTER_1P_MIGRATION.TBL.RELATIONSHIPS_MERGED_FROM_IR T10 ON T.NEW_TGT_IMPORT_ID_IR=T10.NEW_TGT_IMPORT_ID_IR
		WHERE (T.NEW_TGT_IMPORT_ID_IRLINK='' OR T.NEW_TGT_IMPORT_ID_IRLINK IS NULL)  
		AND T.new_tgt_import_id_ir='WBRX-SL8606_P_1455' 


	