
USE SUTTER_1P_DATA
GO



BEGIN--CONTACT ADDRESS

			DROP TABLE SUTTER_1P_MIGRATION.TBL.CONTACT_ADDRESS
			DROP TABLE SUTTER_1P_MIGRATION.IMP.CONTACT_ADDRESS

			--CONSTITUENT INDIVIDUAL HEAD OF HOUSEHOLD 
			SELECT DISTINCT
			X1.ID AS rC_Bios__Contact__c
			--,(T.RE_DB_OwnerShort+'-'+T.HH_ImportID) AS [rC_Bios__Contact__r:External_Id__c]
			,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c
			,T2.rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='False' THEN 'False' 
		          WHEN (T1.AddrPref='True' AND T0.RE_DB_ID!=T0.NEW_TGT_IMPORT_ID) THEN 'False' ELSE T1.AddrPref END AS rC_Bios__Preferred_Mailing__c
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c
			,T1.[AddrCounty] AS County__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			,NULL AS rC_Bios__Do_Not_Mail__c

			--refernce
			,'Contact_HofH' AS zrefSource
			,T0.ImportID AS zrefOldImportID
			,T0.NEW_TGT_IMPORT_ID AS zrefNewImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq
 
 			INTO SUTTER_1P_MIGRATION.TBL.CONTACT_ADDRESS
			FROM SUTTER_1P_MIGRATION.TBL.Contact_HofH T
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T0 ON T.HH_ImportID=T0.NEW_TGT_IMPORT_ID
			INNER JOIN SUTTER_1P_DATA.DBO.HC_Cons_Address_v T1 ON T0.RE_DB_ID=T1.RE_DB_ID
			INNER JOIN SUTTER_1P_MIGRATION.XTR.CONTACT X1 ON T.HH_ImportID=X1.External_Id__c
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_AddressType T2 ON T1.AddrType=T2.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.[Convert]='Yes' AND (T1.[AddrLine1] IS NOT NULL AND T1.[AddrLine1]!='' AND T1.[AddrLine1] NOT LIKE '%deceased%' AND T1.[AddrLine1]!='dec')
	
		UNION ALL	
				 
			--CONSTITUENT INDIVIDUAL NON-HEAD OF HOUSEHOLD 
			SELECT DISTINCT
			X1.ID AS rC_Bios__Contact__c
			--,(T.NoHH_ImportID) AS [rC_Bios__Contact__r:External_Id__c]
			,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c
			,T2.rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='False' THEN 'False' 
		          WHEN (T1.AddrPref='True' AND T0.RE_DB_ID!=T0.NEW_TGT_IMPORT_ID) THEN 'False' ELSE T1.AddrPref END AS rC_Bios__Preferred_Mailing__c
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
			,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c
			,T1.[AddrCounty] AS County__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			,NULL AS rC_Bios__Do_Not_Mail__c

			--reference
			,'Contact_NoHofH' AS zrefSource
			,T0.ImportID AS zrefOldImportID
			,T0.NEW_TGT_IMPORT_ID AS zrefNewImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq
	 			
			FROM SUTTER_1P_MIGRATION.TBL.Contact_HofH T
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T0 ON T.NoHH_ImportID=T0.NEW_TGT_IMPORT_ID
			INNER JOIN SUTTER_1P_DATA.DBO.HC_Cons_Address_v T1 ON T0.RE_DB_ID=T1.RE_DB_ID
			INNER JOIN SUTTER_1P_MIGRATION.XTR.CONTACT X1 ON T.NoHH_ImportID=X1.External_Id__c
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_AddressType T2 ON T1.AddrType=T2.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Contact_HofH TA ON T.NoHH_ImportID=TA.HH_ImportID
			WHERE T2.[Convert]='Yes'  AND TA.HH_ImportID IS NULL AND (T1.[AddrLine1] IS NOT NULL AND T1.[AddrLine1]!='' AND T1.[AddrLine1] NOT LIKE '%deceased%' AND T1.[AddrLine1]!='dec')
 
 			
			UNION ALL
		 	
			--NON-CONSTITUENT INDIVIDUAL RELATIONSHIP
		SELECT DISTINCT
			 X1.ID AS rC_Bios__Contact__c
			--,(T.RE_DB_OwnerShort+'-'+T.IRImpID) AS [rC_Bios__Contact__r:External_Id__c]
			,(T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.IRImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c
			,T2.rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='False' THEN 'False' 
			      WHEN (T.RE_DB_ID!=T.NEW_TGT_IMPORT_ID_IR) THEN 'False' ELSE 'True' END AS rC_Bios__Preferred_Mailing__c
			,T.[IRAddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T.[IRAddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T.[IRAddrCity] AS rC_Bios__Original_City__c
			,T.[IRAddrState] AS rC_Bios__Original_State__c
			,T.[IRAddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T.[IRAddrCountry] AS rC_Bios__Original_Country__c
			,T.[IRAddrCounty] AS County__c
			,CONVERT(NVARCHAR(10),T.[IRAddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T.[IRAddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			,T.IRNoMail AS rC_Bios__Do_Not_Mail__c

			--refernce
			,'HC_Ind_Relat_v' AS zrefSource
			,T.IRImpID AS zrefOldImportID
			,T.NEW_TGT_IMPORT_ID_IR AS zrefNewImportID
			,T.IRImpID AS zrefAddrImpID			
			,T2.RE_DB_OwnerShort AS zrefDBOwner
			,NULL AS zrefAddrSeq
	 			
			FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v T
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_AddressType T2 ON T.IRAddrType=T2.AddrType AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			INNER JOIN SUTTER_1P_MIGRATION.XTR.CONTACT X1 ON T.NEW_TGT_IMPORT_ID_IR=X1.RE_IRImpID__c
			WHERE T2.[Convert]='Yes' AND (T.NEW_TGT_IMPORT_ID_IRLINK IS NULL OR T.NEW_TGT_IMPORT_ID_IRLINK='')
			--AND (T.[IRAddrLine1] IS NOT NULL AND T.[IRAddrLine1]!='' AND T.[IRAddrLine1] NOT LIKE '%deceased%' AND T.[IRAddrLine1]!='dec')
	 
	 UNION ALL 		
		--CONSTITUENT DONOR PROXY from  ORG CONS with no ind relat. 
			SELECT DISTINCT  
			X1.ID AS rC_Bios__Contact__c
			--(T.ImportID) AS [rC_Bios__Account__r:External_ID__c]
			,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c 
			,T2.rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='False' THEN 'False' 
			      WHEN (T1.AddrPref='True' AND T0.RE_DB_ID!=T0.NEW_TGT_IMPORT_ID) THEN 'False' ELSE T1.AddrPref END AS rC_Bios__Preferred_Mailing__c
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c
			,T1.[AddrCounty] AS County__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			, NULL AS rC_Bios__Do_Not_Mail__c
			
			--refernce
			,'Account_Org' AS zrefSource
			,T0.RE_DB_ID AS zrefOldImportID
			,T.ImportID AS zrefNewImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq
			
		 	FROM SUTTER_1P_MIGRATION.TBL.Account_Org T
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T0 ON T.ImportID=T0.NEW_TGT_IMPORT_ID
			LEFT JOIN [SUTTER_1P_DATA].dbo.[HC_Ind_Relat_v] TT ON T0.[NEW_TGT_IMPORT_ID]=TT.[NEW_TGT_IMPORT_ID_CN]
			INNER JOIN SUTTER_1P_DATA.DBO.HC_Cons_Address_v T1 ON T0.RE_DB_ID=T1.RE_DB_ID 
			LEFT JOIN SUTTER_1P_MIGRATION.XTR.CONTACT X1 ON T.ImportID=X1.External_Id__c
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_AddressType T2 ON T1.AddrType=T2.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.[Convert]='Yes' 
			AND (T1.[AddrLine1] IS NOT NULL AND T1.[AddrLine1]!='' AND T1.[AddrLine1] NOT LIKE '%deceased%' AND T1.[AddrLine1]!='dec')
		    AND TT.[NEW_TGT_IMPORT_ID_CN] IS NULL  
		--1617126
	 

END;

BEGIN--REMOVE DOUBLE QUOTES

	--  SELECT [rC_Bios__Original_Street_Line_1__c] FROM [SUTTER_1P_MIGRATION].[IMP].[ACCOUNT_ADDRES] WHERE [rC_Bios__Original_Street_Line_1__c] LIKE '%"%';
	--  SELECT [rC_Bios__Original_Street_Line_2__c] FROM [SUTTER_1P_MIGRATION].[IMP].[ACCOUNT_ADDRES] WHERE [rC_Bios__Original_Street_Line_2__c] LIKE '%"%';        
    --  SELECT [rC_Bios__Original_City__c] FROM [SUTTER_1P_MIGRATION].[IMP].[ACCOUNT_ADDRES] WHERE [rC_Bios__Original_City__c] LIKE '%"%';        
    
    UPDATE [SUTTER_1P_MIGRATION].TBL.[CONTACT_ADDRESS] SET rC_Bios__Original_Street_Line_1__c=REPLACE(rC_Bios__Original_Street_Line_1__c,'"','''') where rC_Bios__Original_Street_Line_1__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].TBL.[CONTACT_ADDRESS] SET rC_Bios__Original_Street_Line_2__c=REPLACE(rC_Bios__Original_Street_Line_2__c,'"','''') where rC_Bios__Original_Street_Line_2__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].TBL.[CONTACT_ADDRESS] SET rC_Bios__Original_City__c=REPLACE(rC_Bios__Original_City__c,'"','''') where rC_Bios__Original_City__c like '%"%'

	USE [SUTTER_1P_MIGRATION] 
 	EXEC sp_FindStringInTable '%"%', 'TBL', 'CONTACT_ADDRESS'


END


BEGIN--check duplicate CONTACTADDRESS
 
	 SELECT *
	 FROM SUTTER_1P_MIGRATION.TBL.[CONTACT_ADDRESS]	  
	 WHERE rC_Bios__External_ID__c IN (SELECT rC_Bios__External_ID__c FROM SUTTER_1P_MIGRATION.TBL.[CONTACT_ADDRESS]	  
									GROUP BY rC_Bios__External_ID__c HAVING COUNT(*)>1)
	 ORDER BY rC_Bios__External_ID__c 

END



--CREATE INDEX TO SORT RECORDS
BEGIN

	CREATE INDEX idx_contact_address_id ON SUTTER_1P_MIGRATION.TBL.CONTACT_ADDRESS(rC_Bios__Contact__c, rC_Bios__Original_Postal_Code__c);
		
END;

 --CREATE SORT TABLE 
BEGIN
	SELECT *
	INTO SUTTER_1P_MIGRATION.IMP.CONTACT_ADDRESS 
	FROM SUTTER_1P_MIGRATION.TBL.CONTACT_ADDRESS
	ORDER BY rC_Bios__Contact__c, rC_Bios__Original_Postal_Code__c
END;


BEGIN
	SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.IMP.[CONTACT_ADDRESS]	
END

	    


BEGIN-- check exceptions

	SELECT T.*
	INTO SUTTER_1P_MIGRATION.IMP.[CONTACT_ADDRESS_XCP]
	FROM SUTTER_1P_MIGRATION.IMP.[CONTACT_ADDRESS]	T
	LEFT JOIN SUTTER_1P_MIGRATION.XTR.CONTACT_ADDRESS X ON T.rC_Bios__External_ID__c =X.rC_Bios__External_ID__c
	WHERE X.rC_Bios__External_ID__c IS NULL OR X.rC_Bios__External_ID__c=''
	ORDER BY T.rC_Bios__External_ID__c 


END 


SELECT * FROM [SUTTER_1P_MIGRATION].imp.[CONTACT_ADDRESS] WHERE [rC_Bios__External_ID__c]='MPHF-AD-00001-516-0000173786'
OR [rC_Bios__External_ID__c]='MPHF-AD-00001-027-0014488'
OR [rC_Bios__External_ID__c]='SCAH-IR-03038-518-0000114582'
OR [rC_Bios__External_ID__c]='WBRX-AD-05502-516-0000382476'

