USE SUTTER_1P_MIGRATION
GO


BEGIN
	DROP TABLE SUTTER_1P_MIGRATION.IMP.ACCOUNT_ADDRESS	
END

BEGIN --ACCOUNT ADDRESS 

			--CONSTITUENT ORGANIZATION to ORG ACCOUNTS. 
			SELECT DISTINCT  
			X1.ID AS rC_Bios__Account__c
			--(T.ImportID) AS [rC_Bios__Account__r:External_ID__c]
			,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c 
			,T2.rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='False' THEN 'False' 
			      WHEN (T1.AddrPref='True' AND T0.RE_DB_ID!=T0.NEW_TGT_IMPORT_ID) THEN 'False' ELSE T1.AddrPref END AS rC_Bios__Preferred_Billing__c
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c
			,T1.[AddrCounty] AS County__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T1.RE_DB_OwnerShort AS Affiliation__c
			, NULL AS rC_Bios__Do_Not_Mail__c
			
			--refernce
			,'Account_Org' AS zrefSource
			,T0.RE_DB_ID AS zrefOldImportID
			,T.ImportID AS zrefNewImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq
			
			INTO SUTTER_1P_MIGRATION.TBL.ACCOUNT_ADDRESS	  
			FROM SUTTER_1P_MIGRATION.TBL.Account_Org T
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T0 ON T.ImportID=T0.NEW_TGT_IMPORT_ID
			INNER JOIN SUTTER_1P_DATA.DBO.HC_Cons_Address_v T1 ON T0.RE_DB_ID=T1.RE_DB_ID 
			INNER JOIN SUTTER_1P_MIGRATION.XTR.ACCOUNT X1 ON T.ImportID=X1.External_Id__c
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_AddressType T2 ON T1.AddrType=T2.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.[Convert]='Yes' AND (T1.[AddrLine1] IS NOT NULL AND T1.[AddrLine1]!='' AND T1.[AddrLine1] NOT LIKE '%deceased%' AND T1.[AddrLine1]!='dec')
			 
		UNION ALL
			
			--CONSTITUENT INDIVIDUAL - HEAD OF HOUSEHOLD to HOUSEHOLD ACCOUNT.
			SELECT DISTINCT  
			X1.ID AS rC_Bios__Account__c
			--(T.HH_ImportID) AS [rC_Bios__Account__r:External_ID__c]
			,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c 
			,T2.rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='False' THEN 'False' 
			      WHEN (T1.AddrPref='True' AND T0.RE_DB_ID!=T0.NEW_TGT_IMPORT_ID) THEN 'False' ELSE T1.AddrPref END AS rC_Bios__Preferred_Billing__c
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c
			,T1.[AddrCounty] AS County__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T1.RE_DB_OwnerShort AS Affiliation__c
			, NULL AS rC_Bios__Do_Not_Mail__c
			
			--reference
			,'Contact_HofH' AS zrefSource
			,T0.ImportID AS zrefOldImportID
			,T0.NEW_TGT_IMPORT_ID AS zrefNewImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq
			
			FROM SUTTER_1P_MIGRATION.TBL.Contact_HofH T
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T0 ON T.HH_ImportID=T0.NEW_TGT_IMPORT_ID
			INNER JOIN SUTTER_1P_DATA.DBO.HC_Cons_Address_v T1 ON T0.RE_DB_ID=T1.RE_DB_ID
			INNER JOIN SUTTER_1P_MIGRATION.XTR.ACCOUNT X1 ON T.HH_ImportID=X1.External_Id__c			
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_AddressType T2 ON T1.AddrType=T2.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.[Convert]='Yes'  AND (T1.[AddrLine1] IS NOT NULL AND T1.[AddrLine1]!='' AND T1.[AddrLine1] NOT LIKE '%deceased%' AND T1.[AddrLine1]!='dec')
	 

		UNION ALL 
		
			--NON-CONSTITUENT ORGANIZATION RELATIONSHIP
			SELECT DISTINCT
			X1.ID AS rC_Bios__Account__c
			--('OR-'+T.NEW_TGT_IMPORT_ID_OR) AS [rC_Bios__Account__r:External_ID__c]
 		 	,(T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.ORImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c
			,T2.rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='False' THEN 'False' 
			      WHEN (T.RE_DB_ID!=T.NEW_TGT_IMPORT_ID_OR) THEN 'False' ELSE 'True' END AS rC_Bios__Preferred_Billing__c

			,T.[ORAddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T.[ORAddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T.[ORAddrCity] AS rC_Bios__Original_City__c
			,T.[ORAddrState] AS rC_Bios__Original_State__c
			,T.[ORAddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T.[ORAddrCountry] AS rC_Bios__Original_Country__c
			,T.[ORAddrCounty] AS County__c
			,CONVERT(NVARCHAR(10),T.[ORAddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T.[ORAddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			,T.ORNoMail AS rC_Bios__Do_Not_Mail__c
			
			--reference
			,'HC_Org_Relat_v' AS zrefSource
			,T.ORImpID AS zrefOldImportID
			,T.NEW_TGT_IMPORT_ID_OR AS zrefNewImportID
			,T.ORImpID AS zrefAddrImpID
			,T2.RE_DB_OwnerShort AS zrefDBOwner
			,NULL AS zrefAddrSeq
			 		
			FROM SUTTER_1P_DATA.dbo.HC_Org_Relat_v T
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_AddressType T2 ON T.ORAddrType=T2.AddrType AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			INNER JOIN SUTTER_1P_MIGRATION.XTR.ACCOUNT X1 ON T.NEW_TGT_IMPORT_ID_OR=X1.RE_ORImpID__c
			WHERE T2.[Convert]='Yes' AND (T.NEW_TGT_IMPORT_ID_ORLINK IS NULL OR T.NEW_TGT_IMPORT_ID_ORLINK='')
			AND (T.[ORAddrLine1] IS NOT NULL AND T.[ORAddrLine1]!='')
			
END;  --1389919

 


BEGIN--REMOVE DOUBLE QUOTES
	--  SELECT [rC_Bios__Original_Street_Line_1__c] FROM [SUTTER_1P_MIGRATION].[TBL].[ACCOUNT_ADDRES] WHERE [rC_Bios__Original_Street_Line_1__c] LIKE '%"%';
	--  SELECT [rC_Bios__Original_Street_Line_2__c] FROM [SUTTER_1P_MIGRATION].[TBL].[ACCOUNT_ADDRES] WHERE [rC_Bios__Original_Street_Line_2__c] LIKE '%"%';        
    --  SELECT [rC_Bios__Original_City__c] FROM [SUTTER_1P_MIGRATION].[TBL].[ACCOUNT_ADDRES] WHERE [rC_Bios__Original_City__c] LIKE '%"%';        
    UPDATE SUTTER_1P_MIGRATION.TBL.ACCOUNT_ADDRESS SET rC_Bios__Original_Street_Line_1__c=REPLACE(rC_Bios__Original_Street_Line_1__c,'"','''') where rC_Bios__Original_Street_Line_1__c like '%"%'
	UPDATE SUTTER_1P_MIGRATION.TBL.ACCOUNT_ADDRESS SET rC_Bios__Original_Street_Line_2__c=REPLACE(rC_Bios__Original_Street_Line_2__c,'"','''') where rC_Bios__Original_Street_Line_2__c like '%"%'
	UPDATE SUTTER_1P_MIGRATION.TBL.ACCOUNT_ADDRESS SET rC_Bios__Original_City__c=REPLACE(rC_Bios__Original_City__c,'"','''') where rC_Bios__Original_City__c like '%"%'

	USE [SUTTER_1P_MIGRATION] 
 	EXEC sp_FindStringInTable '%"%', 'TBL', 'ACCOUNT_ADDRESS'
 
END


BEGIN--check duplicate ACCOUNT_ADDRESS
 
	 SELECT *
	 FROM SUTTER_1P_MIGRATION.TBL.ACCOUNT_ADDRESS	  
	 WHERE rC_Bios__External_ID__c IN (SELECT rC_Bios__External_ID__c FROM SUTTER_1P_MIGRATION.TBL.ACCOUNT_ADDRESS	  
									GROUP BY rC_Bios__External_ID__c HAVING COUNT(*)>1)
	 ORDER BY rC_Bios__External_ID__c

END
											  

--CREATE INDEX TO SORT RECORDS
BEGIN

	CREATE INDEX idx_account_address_id ON SUTTER_1P_MIGRATION.TBL.ACCOUNT_ADDRESS(rC_Bios__Account__c, rC_Bios__Original_Postal_Code__c);
		
END;

 --CREATE SORT TABLE 
BEGIN
	SELECT *
	INTO SUTTER_1P_MIGRATION.IMP.ACCOUNT_ADDRESS 
	FROM SUTTER_1P_MIGRATION.TBL.ACCOUNT_ADDRESS
	ORDER BY rC_Bios__Account__c, rC_Bios__Original_Postal_Code__c
END;

--EXCEPTIONS
SELECT     T1.*
		INTO	[SUTTER_1P_MIGRATION].[IMP].[ACCOUNT_ADDRESS_XCP]
		FROM    [SUTTER_1P_MIGRATION].[IMP].[ACCOUNT_ADDRESS] AS T1 
				LEFT JOIN SUTTER_1P_MIGRATION.[XTR].[ACCOUNT_ADDRESS] AS T2 ON T1.RC_BIOS__EXTERNAL_ID__C = T2.RC_BIOS__EXTERNAL_ID__C
WHERE			(T2.RC_BIOS__EXTERNAL_ID__C IS NULL OR T2.RC_BIOS__EXTERNAL_ID__C='')

	   SELECT * FROM [SUTTER_1P_MIGRATION].[IMP].[ACCOUNT_ADDRESS_XCP] 