
USE SUTTER_1P_DATA
GO

BEGIN
	DROP TABLE SUTTER_1P_MIGRATION.IMP.GAU
END
	  
BEGIN
				SELECT
					dbo.fnc_OwnerID() AS OwnerID
					,T.[GAU External ID] AS rC_Giving__External_ID__c
					,T.GAU_Name AS Name
					,T.GAU_Acknowledgement_Text__c AS Acknowledgement_Text__c
					,T.GAU_IsRestricted AS IsRestricted__c
					,T.GAU_Region__c AS Region__c
					,T.GAU_Affiliation AS Affiliation__c
					,T.GAU_Restriction_Type__c AS Restriction_Type__c
					,MAX(CAST(T1.FundNote AS VARCHAR(MAX))) AS Comments__c
					,MAX(T1.FundStartDate) AS Start_Date__c
					,MAX(T1.FundEndDate) AS End_Date__c
					,MAX(T2.FundAttrDesc) AS Historic_Fund_ID__c
 					,CASE WHEN T1.RE_DB_OwnerShort='PAMF' THEN T1.FundCategory  ELSE NULL END AS Campus_Name__c	 

					--fund attr
					,T3.[CPMC Campus/Program]  AS CPMC_Campus_Program__c
					,T3.[Fund Restriction]	  AS Fund_Restriction__c
					,T3.[Historical Fund Type or Category]	AS Historical_Fund_Type_or_Category__c
					,T3.[Interest Code Aid]		 AS Interest_Code_Aid__c
					,T3.[Service Line] AS Service_Line__c
					,T3.[Sutter Service Line] AS  Sutter_Service_Line__c
					,T3.[WBRX Fund Classifcation]  AS WBRX_Fund_Classifcation__c
   
				 	--fund gl
					  ,T4.[Credit_Account_Cash__c]
					  ,T4.[Credit_Account_Pledge__c]
					  ,T4.[Credit_Account_Pledge_Pay_Cash__c]
					  ,T4.[Credit_Account_Pledge_Pay_Stock__c]
					  ,T4.[Credit_Account_Pledge_Write_Off__c]
					  ,T4.[Credit_Account_Stock__c]
					  ,T4.[Credit_Accounting_Unit__c]
					  ,T4.[Credit_Activity_Code__c]
					  ,T4.[Credit_Category_Cash__c]
					  ,T4.[Credit_Category_Pledge__c]
					  ,T4.[Credit_Category_Pledge_Pay_Cash__c]
					  ,T4.[Credit_Category_Pledge_Pay_Stock__c]
					  ,T4.[Credit_Category_Stock__c]
					  ,T4.[Credit_Company__c]
					  ,T4.[Credit_Sub_Account_Cash__c]
					  ,T4.[Credit_Sub_Account_Pledge__c]
					  ,T4.[Credit_Sub_Account_Pledge_Pay_Cash__c]
					  ,T4.[Credit_Sub_Account_Pledge_Pay_Stock__c]
					  ,T4.[Credit_Sub_Account_Pledge_Write_Off__c]
					  ,T4.[Credit_Sub_Account_Stock__c]
					  ,T4.[Debit_Account_Cash__c]
					  ,T4.[Debit_Account_Pledge__c]
					  ,T4.[Debit_Account_Pledge_Pay_Cash__c]
					  ,T4.[Debit_Account_Pledge_Pay_Stock__c]
					  ,T4.[Debit_Account_Pledge_Write_Off__c]
					  ,T4.[Debit_Account_Stock__c]
					  ,T4.[Debit_Accounting_Unit__c]
					  ,T4.[Debit_Activity_Code__c]
					  ,T4.[Debit_Category_Pledge_Pay_Cash__c]
					  ,T4.[Debit_Category_Pledge_Pay_Stock__c]
					  ,T4.[Debit_Category_Pledge_Write_Off__c]
					  ,T4.[Debit_Company__c]
					  ,T4.[Debit_Sub_Account_Cash__c]
					  ,T4.[Debit_Sub_Account_Pledge__c]
					  ,T4.[Debit_Sub_Account_Pledge_Pay_Cash__c]
					  ,T4.[Debit_Sub_Account_Pledge_Pay_Stock__c]
					  ,T4.[Debit_Sub_Account_Pledge_Write_Off__c]
					  ,T4.[Debit_Sub_Account_Stock__c]
	    		
				 	--ref
					,MAX(T.[FundID]) AS zrefFundId
					,T.[RE_DB_OwnerShort] AS zrefREOwner
				 
				INTO SUTTER_1P_MIGRATION.IMP.GAU
				FROM SUTTER_1P_DATA.dbo.CHART_Fund AS T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Fund_v T1 ON T.FundID=T1.FundID AND T.RE_DB_OwnerShort=T1.[RE_DB_OwnerShort]
				LEFT JOIN [SUTTER_1P_MIGRATION].TBL.GAU_FundAttribute T2 ON T1.[FundID]=T2.FundId AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
				LEFT JOIN [SUTTER_1P_MIGRATION].TBL.FundAttribute_tableentry_final AS T3 ON T1.FundID=T3.FundID AND T1.RE_DB_OwnerShort=T3.[RE_DB_OwnerShort]
				LEFT JOIN [SUTTER_1P_DATA].[TBL].[GL_Distribution_final] AS T4 ON T1.FundID=T4.FundID --not needed. no dupes found. AND T1.RE_DB_OwnerShort=T4.[RE_DB_OwnerShort] 

				WHERE T.[GAU External ID] IS NOT NULL
				GROUP BY   T.RE_DB_OwnerShort, T.[GAU External ID] , T1.RE_DB_OwnerShort 
					,T.GAU_Name  
					,T.GAU_Acknowledgement_Text__c  
					,T.GAU_IsRestricted  
					,T.GAU_Region__c 
					,T.GAU_Affiliation  
					,T.GAU_Restriction_Type__c  
			 		,T1.FundCategory  
			 		,T3.[CPMC Campus/Program]  
					,T3.[Fund Restriction]	   
					,T3.[Historical Fund Type or Category]	 
					,T3.[Interest Code Aid]		 
					,T3.[Service Line]  
					,T3.[Sutter Service Line]  
					,T3.[WBRX Fund Classifcation]   
				 	,T4.[Credit_Account_Cash__c]
					  ,T4.[Credit_Account_Pledge__c]
					  ,T4.[Credit_Account_Pledge_Pay_Cash__c]
					  ,T4.[Credit_Account_Pledge_Pay_Stock__c]
					  ,T4.[Credit_Account_Pledge_Write_Off__c]
					  ,T4.[Credit_Account_Stock__c]
					  ,T4.[Credit_Accounting_Unit__c]
					  ,T4.[Credit_Activity_Code__c]
					  ,T4.[Credit_Category_Cash__c]
					  ,T4.[Credit_Category_Pledge__c]
					  ,T4.[Credit_Category_Pledge_Pay_Cash__c]
					  ,T4.[Credit_Category_Pledge_Pay_Stock__c]
					  ,T4.[Credit_Category_Stock__c]
					  ,T4.[Credit_Company__c]
					  ,T4.[Credit_Sub_Account_Cash__c]
					  ,T4.[Credit_Sub_Account_Pledge__c]
					  ,T4.[Credit_Sub_Account_Pledge_Pay_Cash__c]
					  ,T4.[Credit_Sub_Account_Pledge_Pay_Stock__c]
					  ,T4.[Credit_Sub_Account_Pledge_Write_Off__c]
					  ,T4.[Credit_Sub_Account_Stock__c]
					  ,T4.[Debit_Account_Cash__c]
					  ,T4.[Debit_Account_Pledge__c]
					  ,T4.[Debit_Account_Pledge_Pay_Cash__c]
					  ,T4.[Debit_Account_Pledge_Pay_Stock__c]
					  ,T4.[Debit_Account_Pledge_Write_Off__c]
					  ,T4.[Debit_Account_Stock__c]
					  ,T4.[Debit_Accounting_Unit__c]
					  ,T4.[Debit_Activity_Code__c]
					  ,T4.[Debit_Category_Pledge_Pay_Cash__c]
					  ,T4.[Debit_Category_Pledge_Pay_Stock__c]
					  ,T4.[Debit_Category_Pledge_Write_Off__c]
					  ,T4.[Debit_Company__c]
					  ,T4.[Debit_Sub_Account_Cash__c]
					  ,T4.[Debit_Sub_Account_Pledge__c]
					  ,T4.[Debit_Sub_Account_Pledge_Pay_Cash__c]
					  ,T4.[Debit_Sub_Account_Pledge_Pay_Stock__c]
					  ,T4.[Debit_Sub_Account_Pledge_Write_Off__c]
					  ,T4.[Debit_Sub_Account_Stock__c]
				ORDER BY T.[GAU External ID] 
				--4995
																					    
END;   
				INSERT INTO [SUTTER_1P_MIGRATION].[IMP].[GAU]
				   ([OwnerID]
				   ,[rC_Giving__External_ID__c]
				   ,[Name]
				   ,[Acknowledgement_Text__c]
				   ,[IsRestricted__c]
				   ,[Region__c]
				   ,[Affiliation__c])
		 		SELECT DISTINCT 
					dbo.fnc_OwnerID() AS OwnerID
						,T1.[PrimaryGAU_ExternalID__c] AS rC_Giving__External_ID__c
						,T1.PrimaryGAU_Name__c AS Name
						,NULL AS [Acknowledgement_Text__c]
						,NULL AS [IsRestricted__c]
						,NULL AS [Region__c]
						,LEFT(T1.[PrimaryGAU_ExternalID__c],4) AS [Affiliation__c]
		 				FROM [SUTTER_1P_DATA].dbo.[CHART_Campaign] T1
					LEFT JOIN SUTTER_1P_MIGRATION.IMP.GAU T2 ON T1.[PrimaryGAU_ExternalID__c]=T2.[rC_Giving__External_ID__c]
					WHERE T2.[rC_Giving__External_ID__c] IS NULL AND T1.[PrimaryGAU_ExternalID__c] IS NOT NULL 
	


BEGIN--check for duplicates
	
				--external id
				SELECT * 
				FROM SUTTER_1P_MIGRATION.IMP.GAU
				WHERE rC_Giving__External_ID__c IN (SELECT rC_Giving__External_ID__c FROM SUTTER_1P_MIGRATION.IMP.GAU
													GROUP BY rC_Giving__External_ID__c HAVING COUNT(*)>1)
				ORDER BY rC_Giving__External_ID__c
	    		 
				--name
				SELECT * 
				FROM SUTTER_1P_MIGRATION.IMP.GAU
				WHERE Name IN (SELECT Name FROM SUTTER_1P_MIGRATION.IMP.GAU
							   GROUP BY Name HAVING COUNT(*)>1)
				ORDER BY Name
			 
END; 
 
BEGIN-- check len

		SELECT Name, LEN(Name) l
		FROM [SUTTER_1P_MIGRATION].IMP.[GAU]
		WHERE LEN(name)>80
		ORDER BY l desc
		 
 
BEGIN 
		USE [SUTTER_1P_MIGRATION] 
 		EXEC sp_FindStringInTable '%"%', 'IMP', 'GAU'
  
	 	UPDATE [SUTTER_1P_MIGRATION].[IMP].GAU SET Comments__c=REPLACE(Comments__c,'"','''') where Comments__c like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].GAU SET Name=REPLACE(Name,'"','''') where Name like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].GAU SET Acknowledgement_Text__c=REPLACE(Acknowledgement_Text__c,'"','''') where Acknowledgement_Text__c like '%"%'
		  
		SELECT COUNT(*) FROM [SUTTER_1P_MIGRATION].[IMP].GAU  --5017
END
 
 

	SELECT [rC_Giving__External_ID__c]
		  ,[Credit_Sub_Account_Cash__c]
		  ,[Credit_Sub_Account_Pledge__c]
		  ,[Credit_Sub_Account_Pledge_Pay_Cash__c]
		  ,[Credit_Sub_Account_Pledge_Pay_Stock__c]
		  ,[Credit_Sub_Account_Pledge_Write_Off__c]
		  ,[Credit_Sub_Account_Stock__c]
		  ,[Debit_Sub_Account_Cash__c]
		  ,[Debit_Sub_Account_Pledge__c]
		  ,[Debit_Sub_Account_Pledge_Pay_Cash__c]
		  ,[Debit_Sub_Account_Pledge_Pay_Stock__c]
		  ,[Debit_Sub_Account_Pledge_Write_Off__c]
		  ,[Debit_Sub_Account_Stock__c]
	 -- INTO [SUTTER_1P_MIGRATION].IMP.UPDATE_GAU
	  FROM SUTTER_1P_MIGRATION.[IMP].[GAU]
	  WHERE  [Credit_Sub_Account_Cash__c]	IS NOT NULL 
		  OR [Credit_Sub_Account_Pledge__c] IS NOT NULL 
		  OR [Credit_Sub_Account_Pledge_Pay_Cash__c]	  IS NOT NULL 
		  OR [Credit_Sub_Account_Pledge_Pay_Stock__c]	IS NOT NULL 
		  OR [Credit_Sub_Account_Pledge_Write_Off__c]	IS NOT NULL 
		  OR [Credit_Sub_Account_Stock__c] IS NOT NULL 
		  OR [Debit_Sub_Account_Cash__c] IS NOT NULL 
		  OR [Debit_Sub_Account_Pledge__c] IS NOT NULL 
		  OR [Debit_Sub_Account_Pledge_Pay_Cash__c]  IS NOT NULL 
		  OR [Debit_Sub_Account_Pledge_Pay_Stock__c] IS NOT NULL 
		  OR [Debit_Sub_Account_Pledge_Write_Off__c] IS NOT NULL 
		  OR [Debit_Sub_Account_Stock__c] IS NOT NULL 
	ORDER BY [rC_Giving__External_ID__c]
 
		   																															 