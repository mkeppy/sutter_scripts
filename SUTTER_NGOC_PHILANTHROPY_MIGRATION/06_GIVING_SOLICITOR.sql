--GIVING_SOLICITOR__c
  USE [SUTTER_1P_MIGRATION]
  GO 

BEGIN--END GIVING_SOLICITOR
	DROP TABLE [SUTTER_1P_MIGRATION].IMP.GIVING_SOLICITOR	
END 
	
BEGIN--CREATE GIVING_SOLICITOR
				SELECT DISTINCT 
				NULL AS [Id]
				,[OwnerID]=[dbo].[fnc_OwnerID]()
				,CASE [T3].[SOLICITOR_TEAM_RECORD_TYPE] WHEN 'Solicitor_Philanthropy' THEN ([dbo].[fnc_RecordType]('Giving_Solicitor_Philanthropy'))	
														WHEN 'Solicitor_External' THEN ([dbo].[fnc_RecordType]('Giving_Solicitor_External')) 
														ELSE NULL END AS [RecordTypeID]
				,[Affiliation__c] = [T1].[RE_DB_OwnerShort]
 				,[Proposal_Grant__r:External_Id__c] = T4.RE_DB_OwnerShort+'-'+T4.RE_DB_Tbl+'-'+T4.PRImpID
				,[Solicitor_External_Contact__r:External_Id__c]= CASE WHEN (T3.SOLICITOR_TEAM_RECORD_TYPE='Solicitor_External' AND T2.KeyInd='I') THEN [T2].[NEW_TGT_IMPORT_ID] ELSE NULL END  						--External
 				,[Solicitor_Internal_User__c]=CASE WHEN T3.SOLICITOR_TEAM_RECORD_TYPE ='Solicitor_Philanthropy' THEN X1.ID ELSE NULL END  --Internal(Philanthropy)
				,[Type__c]=MAX(T3.[SOLICITOR_TEAM_TYPE])
			  
			 	INTO [SUTTER_1P_MIGRATION].IMP.GIVING_SOLICITOR	
  				FROM [SUTTER_1P_DATA].[dbo].[HC_Proposal_Solicitor_v] AS [T1]
				--solicitor
				LEFT JOIN [SUTTER_1P_DATA].[DBO].[HC_Constituents_v] AS [T2] ON [T1].[PRSolImpID]=[T2].[ImportID] AND [T1].[RE_DB_OwnerShort]=[T2].[RE_DB_OwnerShort]
				INNER JOIN [SUTTER_1P_DATA].[dbo].[CHART_ConsSolicitor] AS [T3] ON [T1].[PRSolImpID]=[T3].[ASRLink] AND [T1].[RE_DB_OwnerShort]=[T3].[RE_DB_OwnerShort]
				LEFT JOIN [SUTTER_1P_MIGRATION].[XTR].[USERS] AS [X1] ON [T3].[SH_Email]=[X1].[Email]
 				--proposal 
				INNER JOIN	[SUTTER_1P_DATA].dbo.[HC_Proposal_v] AS T4 ON T1.[PRSolPRImpID]=T4.[PRImpID] AND T1.[RE_DB_OwnerShort]=T4.[RE_DB_OwnerShort]
				WHERE T3.[Source] ='ProposalSolicitor'
				GROUP BY  [T3].[SOLICITOR_TEAM_RECORD_TYPE], [T1].[RE_DB_OwnerShort],
							T4.RE_DB_OwnerShort, T4.RE_DB_Tbl, T4.PRImpID,
							T2.KeyInd,[T2].[NEW_TGT_IMPORT_ID],	T3.[SH_Email],
							X1.ID
  			UNION ALL 
				--sol team member from PROPOSAL ATTRIBUTE.
				SELECT DISTINCT 
				NULL AS [Id]
				,[OwnerID]=[dbo].[fnc_OwnerID]()
				,CASE [T3].[SOLICITOR_TEAM_RECORD_TYPE] WHEN 'Solicitor_Philanthropy' THEN ([dbo].[fnc_RecordType]('Giving_Solicitor_Philanthropy'))	
													WHEN 'Solicitor_External' THEN ([dbo].[fnc_RecordType]('Giving_Solicitor_External')) 
													ELSE NULL END AS [RecordTypeID]
				,[Affiliation__c] = [T1].[RE_DB_OwnerShort]
				,[Proposal_Grant__r:External_Id__c] = T4.RE_DB_OwnerShort+'-'+T4.RE_DB_Tbl+'-'+T4.PRImpID
				,[Solicitor_External_Contact__r:External_Id__c]= CASE WHEN (T3.SOLICITOR_TEAM_RECORD_TYPE='Solicitor_External' AND T7.KeyInd='I') THEN [T7].[NEW_TGT_IMPORT_ID] ELSE NULL END  						--External
 				,[Solicitor_Internal_User__c]=CASE WHEN T3.SOLICITOR_TEAM_RECORD_TYPE ='Solicitor_Philanthropy' THEN X1.ID ELSE NULL END  --Internal(Philanthropy)
				,[Type__c]=MAX(T3.[SOLICITOR_TEAM_TYPE])
  			 	FROM [SUTTER_1P_DATA].dbo.[HC_Proposal_Attr_v] T1 
				--proposal
				INNER JOIN	[SUTTER_1P_DATA].dbo.[HC_Proposal_v] AS T4 ON T1.[PRAttrPRImpID]=T4.[PRImpID] AND T1.[RE_DB_OwnerShort]=T4.[RE_DB_OwnerShort]
				--solicitor
 				INNER  JOIN [SUTTER_1P_DATA].dbo.[CHART_ProposalAttr] T2 ON T1.[PRAttrCat]=T2.[PRAttrCat] AND T1.[PRAttrDesc]=T2.[PRAttrDesc] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
				INNER  JOIN [SUTTER_1P_DATA].dbo.[CHART_ConsSolicitor] T3 ON T2.[SF_Value]=(T3.[RE_DB_OwnerShort]+'-'+T3.[ASRLink])
				LEFT JOIN [SUTTER_1P_DATA].[DBO].[HC_Constituents_v] AS [T7] ON (T3.[RE_DB_OwnerShort]+'-'+T3.[ASRLink])=[T7].RE_DB_ID
				LEFT JOIN [SUTTER_1P_MIGRATION].[XTR].[USERS] AS [X1] ON [T3].[SH_Email]=[X1].[Email]
				WHERE T3.[Source] ='ProposalSolicitor'	
				 		GROUP BY  [T3].[SOLICITOR_TEAM_RECORD_TYPE], [T1].[RE_DB_OwnerShort],
						T4.RE_DB_OwnerShort, T4.RE_DB_Tbl, T4.PRImpID,
						T7.KeyInd,[T7].[NEW_TGT_IMPORT_ID],
						X1.ID
END 
	
	SELECT * FROM [SUTTER_1P_MIGRATION].IMP.GIVING_SOLICITOR	
	WHERE [Solicitor_External_Contact__r:External_Id__c] IS NULL
	AND 	[Solicitor_Internal_User__c] IS NULL 
	
	SELECT * FROM [SUTTER_1P_DATA].dbo.[CHART_ConsSolicitor] 