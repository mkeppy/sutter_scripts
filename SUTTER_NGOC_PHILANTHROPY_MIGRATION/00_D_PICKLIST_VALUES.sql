
--PICKLIST values

--board member
		
		SELECT DISTINCT BOARDMEMBER_Committee__c
		FROM SUTTER_MGEA.dbo.CHART_ConsAttributes
		WHERE BOARDMEMBER_Committee__c IS NOT NULL 

		SELECT DISTINCT BOARDMEMBER_Type__c
		FROM SUTTER_MGEA.dbo.CHART_ConsAttributes
		WHERE BOARDMEMBER_Type__c IS NOT NULL

--opportunity

		--OPPORTUNITY_Production_Stage__c 
		SELECT DISTINCT OPPORTUNITY_Production_Stage__c 
		FROM SUTTER_MGEA.dbo.CHART_GiftSubType
		WHERE OPPORTUNITY_Production_Stage__c IS NOT null
		
		 
		--PROPOSAL rating
		SELECT DISTINCT PRRating 
		FROM SUTTER_DATA.dbo.HC_Proposal_v
		WHERE PRRating IS NOT NULL
		
		--PROPOSAL reason
		SELECT DISTINCT PRReason
		FROM SUTTER_DATA.dbo.HC_Proposal_v
		WHERE PRReason IS NOT NULL
		
		--PROPOSAL Status
		SELECT DISTINCT Proposal_Status__c
		FROM SUTTER_MGEA.dbo.CHART_PRStatus
		WHERE Proposal_Status__c IS NOT NULL
		ORDER BY Proposal_Status__c
	
--PREFERENCE

		SELECT DISTINCT PREFERENCE_CapacityRating__c		
		FROM SUTTER_MGEA.dbo.CHART_ConsAttributes
		WHERE PREFERENCE_CapacityRating__c IS NOT NULL AND PREFERENCE_CapacityRating__c!='NULL'
		ORDER BY PREFERENCE_CapacityRating__c		

--RELATIONSHIP 

		SELECT DISTINCT ESRType
		FROM SUTTER_DATA.dbo.HC_Educ_Relat_v
		WHERE ESRType IS NOT NULL 
		ORDER BY ESRType
		
--GAU

		SELECT DISTINCT GAU_Region__c 
		FROM SUTTER_MGEA.dbo.CHART_Fund
		WHERE GAU_Region__c IS NOT NULL
		ORDER BY GAU_Region__c
		 
		SELECT DISTINCT GAU_Restriction_Type__c
		FROM SUTTER_MGEA.dbo.CHART_Fund
		WHERE GAU_Restriction_Type__c IS NOT NULL
		ORDER BY GAU_Restriction_Type__c

--TASK
		SELECT DISTINCT TASK_Move__c
		FROM SUTTER_MGEA.dbo.CHART_ActionStatus
		WHERE TASK_Move__c IS NOT NULL
		ORDER BY TASK_Move__c
		
		SELECT DISTINCT TASK_Stage__c
		FROM SUTTER_MGEA.dbo.CHART_ActionType
		WHERE TASK_Stage__c IS NOT NULL
		ORDER BY TASK_Stage__c
		
		
		SELECT DISTINCT ACCat 
		FROM SUTTER_DATA.dbo.HC_Cons_Action_v
		WHERE ACCat IS NOT NULL 
		ORDER BY ACCat
		
		--RE_Action_Solicitor__c  
		SELECT DISTINCT Name 
		FROM SUTTER_MGEA.TBL.Cons_Action_Solicitor
		WHERE name IS NOT NULL
		ORDER BY name 
		
