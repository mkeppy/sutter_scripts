USE [SUTTER_1P_MIGRATION]
GO

BEGIN--PAYMENT METHODS 
		BEGIN--BASE TABLE
				/*
					DROP TABLE SUTTER_1P_MIGRATION.TBL.PAYMENT_METHOD
				    GO
					DROP TABLE SUTTER_1P_MIGRATION.IMP.PAYMENT_METHOD 
					GO
				*/
				SELECT 
				dbo.fnc_OwnerID() AS OwnerID
				,ROW_NUMBER() OVER (PARTITION BY (CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.HH_ImportID ELSE T4.NEW_TGT_IMPORT_ID  END) 
									ORDER BY  (CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.HH_ImportID ELSE T4.NEW_TGT_IMPORT_ID  END),
												CASE WHEN (T1.GFType LIKE '%Stock%') THEN '3'
												  WHEN (T1.GFPayMeth LIKE '%Check%' OR T1.GFPayMeth='Cash') THEN '1'
												  WHEN (T1.GFPayMeth ='Credit Card') THEN '2'
												  WHEN (T1.GFPayMeth ='Direct Debit') THEN '2'
												  WHEN (T1.GFPayMeth ='Other') THEN '4'
												  ELSE '1' END) AS zrefSeq 
				,CASE	WHEN (T1.GFType LIKE '%Stock%') THEN '3'
						WHEN (T1.GFPayMeth LIKE '%Check%' OR T1.GFPayMeth='Cash') THEN '1'
						WHEN (T1.GFPayMeth ='Credit Card') THEN '2'
						WHEN (T1.GFPayMeth ='Direct Debit') THEN '2'
						WHEN (T1.GFPayMeth ='Other') THEN '4'
						ELSE '1' END AS zrefPaySeq
				,CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.HH_ImportID ELSE T4.NEW_TGT_IMPORT_ID  END AS ACCOUNT_External_ID__c
				,CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ISNULL(UPPER(CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.HH_ImportID ELSE T4.NEW_TGT_IMPORT_ID END),' ')+   --Head of Household
						ISNULL(UPPER(CASE WHEN (T1.GFType LIKE '%Stock%') THEN 'Securities'
										  WHEN (T1.GFPayMeth LIKE '%Check%' OR T1.GFPayMeth='Cash') THEN 'Cash/Check'
										  WHEN (T1.GFPayMeth ='Credit Card') THEN 'Third Party Charge'
										  WHEN (T1.GFPayMeth ='Direct Debit') THEN 'Third Party Charge'
										  WHEN (T1.GFPayMeth ='Other') THEN 'Other'
										  ELSE 'Cash/Check' END ),' ')+ 
						ISNULL(UPPER(T1.GFCCType),' ')+ 
						ISNULL(UPPER(T1.GFCCNum),' ')+
						ISNULL(UPPER(T1.GFCardholderName),' ')+
						ISNULL(UPPER(T1.GFCCExpOn),' ')+
						ISNULL(UPPER(T1.GFAuthCode),' '))), 3, 50) AS NVARCHAR(50)) 
					AS External_Id__c
				,CASE WHEN T1.GFType LIKE '%stock%' THEN dbo.fnc_RecordType('Payment_Method_Securities') ELSE dbo.fnc_RecordType('Payment_Method_'+T2.rC_Giving__Payment_Type__c) END AS RecordTypeID	
				,CASE WHEN T1.GFType LIKE '%stock%' THEN 'Securities' ELSE T2.rC_Giving__Payment_Type__c END AS Name
				,CASE WHEN T1.GFType LIKE '%stock%' THEN 'Securities' ELSE T2.rC_Giving__Payment_Type__c END AS rC_Giving__Payment_Type__c
				,CASE WHEN T2.rC_Giving__Payment_Type__c ='Cash/Check' then 'TRUE' else 'FALSE' end as rC_Giving__Is_Default__c
				,CASE WHEN T2.rC_Giving__Payment_Type__c ='Cash/Check' then 'TRUE' else 'FALSE' end as rC_Giving__Is_Active__c
				,T1.GFCCType AS rC_Giving__Card_Issuer__c
				,RIGHT(T1.GFCCNum,4) AS rC_Giving__Card_Number_Last_4__c 
				,T1.GFCardholderName AS rC_Giving__Card_Holder_Name__c
				,RIGHT(T1.GFCCExpOn,2)+'/'+LEFT(T1.GFCCExpOn, 4) AS rC_Giving__Card_Expiration__c
				,T1.GFAuthCode AS rC_Connect__Authentication_Value__c
				--ref
				,PayTypeSeq= CASE WHEN (T1.GFType LIKE '%Stock%') THEN '3'
										  WHEN (T1.GFPayMeth LIKE '%Check%' OR T1.GFPayMeth='Cash') THEN '1'
										  WHEN (T1.GFPayMeth ='Credit Card') THEN '2'
										  WHEN (T1.GFPayMeth ='Direct Debit') THEN '2'
										  WHEN (T1.GFPayMeth ='Other') THEN '4'
										  ELSE '1' END 
				,T1.GFPayMeth
				,T1.GFType
				,T1.GFImpID
				,T1.RE_DB_Id 
				,T1.[RE_DB_OwnerShort]
				,T4.NEW_TGT_IMPORT_ID 
				,T3.HH_ImportID HH_ImportID
				,T3.NoHH_ImportID AS NoHH_ImportId
				,'cons_gifts' AS  src
				INTO SUTTER_1P_MIGRATION.TBL.PAYMENT_METHOD	
				FROM SUTTER_1P_DATA.dbo.HC_Gift_v T1
				INNER JOIN SUTTER_1P_DATA.dbo.CHART_GFPayMeth AS T2 ON T1.GFPayMeth=T2.GFPayMeth
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v AS T4 ON T1.RE_DB_Id=T4.RE_DB_Id				
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH AS T3 ON T4.NEW_TGT_IMPORT_ID=T3.NoHH_ImportID 
				 --3110633
			END;

				/*--not included in 1P1T. these are payment methods from constituents with no gifts. BP Consultants suggested not to include them. 
								--cons without gifts	
									INSERT INTO SUTTER_1P_MIGRATION.[TBL].[PAYMENT_METHOD]
										   ([OwnerID]
										   ,[ACCOUNT_External_ID__c]
										   ,[External_Id__c]
										   ,[RecordTypeID]
										   ,[Name]
										   ,[rC_Giving__Payment_Type__c]
										   ,[rC_Giving__Is_Default__c]
										   ,[rC_Giving__Is_Active__c]
										   ,[rC_Giving__Card_Issuer__c]
										   ,[rC_Giving__Card_Number_Last_4__c]
										   ,[rC_Giving__Card_Holder_Name__c]
										   ,[rC_Giving__Card_Expiration__c]
										   ,[rC_Connect__Authentication_Value__c]
										   ,[GFPayMeth]
										   ,[GFType]
										   ,[GFImpID]
										   ,[RE_DB_Id]
										   ,[NEW_TGT_IMPORT_ID]
										   ,[HH_ImportID]
										   ,[NoHH_ImportId]
										   ,[src])
									SELECT  
									dbo.fnc_OwnerID() AS OwnerID
									,T1.NEW_TGT_IMPORT_ID AS ACCOUNT_External_ID__c
									,CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ISNULL(UPPER(CASE WHEN T4.NoHH_ImportID IS NOT NULL THEN T4.HH_ImportID ELSE T1.NEW_TGT_IMPORT_ID END),' ')+   --Head of Household
											ISNULL(UPPER('Cash/Check'),' ')+
											ISNULL(UPPER(T3.GFCCType),' ')+ 
											ISNULL(UPPER(T3.GFCCNum),' ')+
											ISNULL(UPPER(T3.GFCardholderName),' ')+
											ISNULL(UPPER(T3.GFCCExpOn),' ')+
											ISNULL(UPPER(T3.GFAuthCode),' '))), 3, 50) AS NVARCHAR(50)) 
										AS External_Id__c
									,dbo.fnc_RecordType('Payment_Method_Cash/Check') AS RecordTypeID
									,'Cash/Check' AS Name
									,'Cash/Check' AS rC_Giving__Payment_Type__c
									,'TRUE' AS rC_Giving__Is_Default__c
									,'TRUE' AS rC_Giving__Is_Active__c
									,NULL AS rC_Giving__Card_Issuer__c
									,NULL AS rC_Giving__Card_Number_Last_4__c 
									,NULL AS rC_Giving__Card_Holder_Name__c
									,NULL AS rC_Giving__Card_Expiration__c
									,NULL AS rC_Connect__Authentication_Value__c
									--ref
									,'' AS GFPayMeth
									,'' AS GFType
									,'' AS GFImpID 
									,T1.RE_DB_Id 
									,T1.NEW_TGT_IMPORT_ID 
									,'' HH_ImportID
									,'' NoHH_ImportId
									,'No_gifts' AS  src
									FROM SUTTER_1P_DATA.dbo.HC_Constituents_v AS T1
									LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH AS T4 ON T1.NEW_TGT_IMPORT_ID=T4.NoHH_ImportID 
									LEFT JOIN SUTTER_1P_MIGRATION.TBL.PAYMENT_METHOD AS T2 ON T1.NEW_TGT_IMPORT_ID=T2.ACCOUNT_External_ID__c 
									LEFT JOIN SUTTER_1P_DATA.dbo.HC_Gift_v T3 ON T1.RE_DB_Id=T3.RE_DB_Id
									WHERE T2.ACCOUNT_External_ID__c IS NULL
					*/
		 
		--UPDATE DEFAULT

				SELECT		
					zrefSeq
					,zrefPaySeq
					,T1.OwnerID
		  			,T1.ACCOUNT_External_ID__c AS [rC_Giving__Account__r:External_Id__c]
					,T1.External_Id__c
					,T1.RecordTypeID
					,T1.Name 
					,T1.rC_Giving__Payment_Type__c 
					,T1.rC_Giving__Is_Default__c
					,T1.rC_Giving__Is_Active__c 
				FROM SUTTER_1P_MIGRATION.TBL.PAYMENT_METHOD T1
				ORDER BY ACCOUNT_External_ID__c

				UPDATE SUTTER_1P_MIGRATION.TBL.PAYMENT_METHOD
				SET rC_Giving__Is_Default__c='TRUE', rC_Giving__Is_Active__c='TRUE'
				WHERE zrefSeq='1'
				--	 519,193
				UPDATE SUTTER_1P_MIGRATION.TBL.PAYMENT_METHOD
				SET rC_Giving__Is_Default__c='FALSE', rC_Giving__Is_Active__c='FALSE'
				WHERE zrefSeq!='1'
				-- 2,571,312
				
		BEGIN--IMP table 
				SELECT 
					T1.OwnerID
		  			,T1.ACCOUNT_External_ID__c AS [rC_Giving__Account__r:External_Id__c]
					,T1.External_Id__c
					,T1.RecordTypeID
					,T1.Name 
					,T1.rC_Giving__Payment_Type__c 
					,MAX(T1.rC_Giving__Is_Default__c) AS rC_Giving__Is_Default__c
					,MAX(T1.rC_Giving__Is_Active__c) AS rC_Giving__Is_Active__c 
					,T1.rC_Giving__Card_Issuer__c
					,T1.rC_Giving__Card_Number_Last_4__c
					,T1.rC_Giving__Card_Holder_Name__c
					,T1.rC_Giving__Card_Expiration__c
					,T1.rC_Connect__Authentication_Value__c
				INTO SUTTER_1P_MIGRATION.IMP.PAYMENT_METHOD    
				FROM SUTTER_1P_MIGRATION.TBL.PAYMENT_METHOD T1
				GROUP BY T1.OwnerID
		  				,T1.ACCOUNT_External_ID__c
						,T1.External_Id__c
						,T1.RecordTypeID
						,T1.Name 
						,T1.rC_Giving__Payment_Type__c 
						,T1.rC_Giving__Card_Issuer__c
						,T1.rC_Giving__Card_Number_Last_4__c
						,T1.rC_Giving__Card_Holder_Name__c
						,T1.rC_Giving__Card_Expiration__c
						,T1.rC_Connect__Authentication_Value__c
				ORDER BY T1.ACCOUNT_External_ID__c
				 
		END;			
		--580990
 
		BEGIN--check duplicates. 
				SELECT    *
				FROM      SUTTER_1P_MIGRATION.IMP.PAYMENT_METHOD  --ORDER BY [rc_Giving__Account__r:External_Id__C], zrefseq
				WHERE     External_Id__c IN (
					        SELECT  External_Id__c
						    FROM     SUTTER_1P_MIGRATION.IMP.PAYMENT_METHOD 
							GROUP BY External_Id__c
							HAVING  COUNT(*) > 1 )
				ORDER BY  External_Id__c 
		 
		END;
	 
		BEGIN--check defaults
		
				SELECT [rC_Giving__Account__r:External_Id__c]
					   ,rC_Giving__Is_Default__c
					   ,COUNT(*) c
				FROM SUTTER_1P_MIGRATION.IMP.PAYMENT_METHOD 
				WHERE   rC_Giving__Is_Default__c = 'TRUE'
				GROUP BY [rC_Giving__Account__r:External_Id__c]
					   ,rC_Giving__Is_Default__c
				HAVING COUNT(*)>1
				ORDER BY c DESC

				--IMP
				SELECT * FROM SUTTER_1P_MIGRATION.IMP.PAYMENT_METHOD
				WHERE [rC_Giving__Account__r:External_Id__c]='ABSF-02869-079-0130709'  
				--TBL
				SELECT * FROM SUTTER_1P_MIGRATION.TBL.PAYMENT_METHOD
				WHERE ACCOUNT_External_ID__c='ABSF-02869-079-0130709' ORDER BY ACCOUNT_External_ID__c, zrefseq

				SELECT * FROM SUTTER_1P_MIGRATION.IMP.PAYMENT_METHOD
				WHERE [rC_Giving__Account__r:External_Id__c]='PAMF-00001-079-0013673'
 			   --TBL
				SELECT * FROM SUTTER_1P_MIGRATION.TBL.PAYMENT_METHOD
				WHERE ACCOUNT_External_ID__c='PAMF-00001-079-0013673' ORDER BY ACCOUNT_External_ID__c, zrefseq

				
		END;
				SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.IMP.PAYMENT_METHOD
END;
  