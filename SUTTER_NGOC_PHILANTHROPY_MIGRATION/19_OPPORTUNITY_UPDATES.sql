USE [SUTTER_1P_DATA]
GO

SELECT * 
INTO SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_UPDATE_backup_matching
FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_UPDATE

SELECT * FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_UPDATE_backup_matching

BEGIN
	TRUNCATE TABLE SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_UPDATE
END;

BEGIN-- UPDATE MATCHING gift information
			 INSERT INTO SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_UPDATE ([External_ID__c]
					  ,[rC_Giving__Planned_Giving__r:rC_Giving__External_ID__c]
					  ,[CAMPAIGN:External_Id__c]
					  ,[rC_Giving__Matching_Amount__c]
					  ,[rC_Giving__Matching_Account__r:External_Id__c]
					  ,[rC_Giving__Matching_Opportunity__r:External_Id__c]
					  ,[rC_Giving__Matching_Status__c]
					  ,[zrefSrc])
 			 SELECT		T3.RE_DB_OwnerShort+'-'+T3.RE_DB_Tbl+'-'+T3.GSplitImpID AS External_ID__c, 
						CAST('' AS VARCHAR(60)) AS [rC_Giving__Planned_Giving__r:rC_Giving__External_ID__c],
						CAST('' AS VARCHAR(60)) AS [CAMPAIGN:External_Id__c],
						CAST(T3.GSplitAmt AS VARCHAR(20)) AS rC_Giving__Matching_Amount__c,
						T5.[NEW_TGT_IMPORT_ID] AS [rC_Giving__Matching_Account__r:External_Id__c],
						T4.RE_DB_OwnerShort+'-'+T4.RE_DB_Tbl+'-'+T4.GSplitImpID AS [rC_Giving__Matching_Opportunity__r:External_Id__c],
						'Approved' AS rC_Giving__Matching_Status__c,
						'matching_gift_update' zrefSrc
							/*	--ref 
								T1.RE_DB_OwnerShort,  T1.GFLink,  T2.GFType, T3.GSplitImpID,  T3.GSplitAmt, T1.MatchedBy_ImportID,  
								T1.MatchedBy_OrgName,  T1.Matching_GFImpID,  T4.GSplitImpID AS Matching_GSplitImpID */
  			 FROM [SUTTER_1P_DATA].dbo.HC_Gift_MatchingLink_v T1 
			 INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Constituents_v] T5 ON T1.[MatchedBy_ImportID]=T5.[ImportID] AND T1.[RE_DB_OwnerShort]=T5.[RE_DB_OwnerShort]
			 INNER JOIN [SUTTER_1P_DATA].dbo.HC_Gift_v T2 ON T1.GFLink=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			 INNER JOIN [SUTTER_1P_DATA].dbo.HC_Gift_SplitGift_v T3 ON T1.GFLink=T3.GFImpID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
			 INNER JOIN [SUTTER_1P_DATA].DBO.HC_Gift_SplitGift_v T4 ON T1.Matching_GFImpID=T4.GFImpID AND T1.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
			 AND T3.GFSplitSequence = T4.GFSplitSequence
			 ORDER BY T3.GSplitImpID, T4.GSplitImpID
END;
		 
BEGIN--OPPORTUNITY PLANNED GIVIGIN UPDATE to link Donations and Planned giving.... 11.10.2015  */
 			INSERT INTO SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_UPDATE ([External_ID__c],  [rC_Giving__Planned_Giving__r:rC_Giving__External_ID__c],
																	[CAMPAIGN:External_Id__c], [rC_Giving__Matching_Amount__c],  [rC_Giving__Matching_Account__r:External_Id__c],
																	[rC_Giving__Matching_Opportunity__r:External_Id__c], [rC_Giving__Matching_Status__c], [zrefSrc])
 			SELECT  (T4.[RE_DB_OwnerShort]+'-GT-'+T4.[GSplitImpID]) AS [External_ID__c],
					[T2].[RE_DB_OwnerShort]+'-GT-'+T2.[GSplitImpID] AS [rC_Giving__Planned_Giving__r:rC_Giving__External_ID__c],
					NULL AS [CAMPAIGN:External_Id__c], NULL AS [rC_Giving__Matching_Amount__c],  NULL AS [rC_Giving__Matching_Account__r:External_Id__c],
					NULL AS [rC_Giving__Matching_Opportunity__r:External_Id__c], '' AS [rC_Giving__Matching_Status__c]
					,'pg_gift' AS zrefSrc
			FROM [SUTTER_1P_DATA].dbo.hc_gift_v T1
			INNER JOIN [SUTTER_1P_DATA].[dbo].[HC_Gift_SplitGift_v] T2 ON T1.[GFImpID]=T2.[GFImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
			INNER JOIN [SUTTER_1P_DATA].[dbo].[HC_Gift_v] T3 ON T1.[SystemRecordID]=T3.[GFPlannedGiftID] AND T1.[RE_DB_OwnerShort]=T3.[RE_DB_OwnerShort]
			INNER JOIN [SUTTER_1P_DATA].[dbo].[HC_Gift_SplitGift_v] T4 ON T3.[GFImpID]=T4.[GFImpID] AND T3.[RE_DB_OwnerShort]=T4.[RE_DB_OwnerShort]
END;
BEGIN--CAMPAIGN 50K+ OPPORTUNITY UPDATE
  	        SELECT [CAMPAIGN:External_Id__c], COUNT(*) C
	 		FROM [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PARENT]
	 		GROUP BY [CAMPAIGN:External_Id__c]
	 		HAVING COUNT(*)>49999
	 		ORDER BY C DESC
		
			--SMCF EMPGVG	86226
		 	--SAFH Don/employee	76949

			--SAFH Don/employee
				-- SAFH Don/employee becomes PARENT campaign
				-- SAFH Don/employee 1 (first split) = 45,000
				-- SAFH Don/employee 2 (first split) > 45,000

			--SMCF EMPGVG
				-- SMCF EMPGVG becomes PARENT campaign
				-- SMCF EMPGVG 1 (first split) = 45,000
				-- SMCF EMPGVG 2 (first split) > 45,000
 		INSERT INTO SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_UPDATE ([External_ID__c],  [rC_Giving__Planned_Giving__r:rC_Giving__External_ID__c],
																[CAMPAIGN:External_Id__c], [rC_Giving__Matching_Amount__c],  [rC_Giving__Matching_Account__r:External_Id__c],
																[rC_Giving__Matching_Opportunity__r:External_Id__c], [rC_Giving__Matching_Status__c], [zrefSrc])
	
		SELECT T.[External_Id__c], NULL AS [rC_Giving__Planned_Giving__r:rC_Giving__External_ID__c],
				CASE WHEN (ROW_NUMBER() OVER (PARTITION BY  T.[CAMPAIGN:External_Id__c] ORDER BY T.[CAMPAIGN:External_Id__c]))<50001 THEN T.[CAMPAIGN:External_Id__c]+' 1'
				WHEN (ROW_NUMBER() OVER (PARTITION BY  T.[CAMPAIGN:External_Id__c] ORDER BY T.[CAMPAIGN:External_Id__c]))>50000 THEN T.[CAMPAIGN:External_Id__c]+' 2'
			END AS [CAMPAIGN:External_Id__c],
			NULL AS [rC_Giving__Matching_Amount__c],  NULL AS [rC_Giving__Matching_Account__r:External_Id__c], NULL AS [rC_Giving__Matching_Opportunity__r:External_Id__c],
			'' AS [rC_Giving__Matching_Status__c],
			'campaign_split' AS [zrefSrc]
		FROM [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PARENT] T
 		WHERE [T].[CAMPAIGN:External_Id__c] IN (SELECT [CAMPAIGN:External_Id__c] 
										FROM [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PARENT]
										GROUP BY [CAMPAIGN:External_Id__c]
										HAVING COUNT(*)>49999)

END; 

/*
(    743 row(s) affected)
(     53 row(s) affected)
(163,175 row(s) affected) 
(163,971 total		    ) 
*/

SELECT * FROM  SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_UPDATE		
SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_UPDATE			   