			--	 	SELECT * FROM [SUTTER_1P_MIGRATION].dbo.[ConsoliatedDowntimeActions_new]
		 
	--new records from template sent late
			
		--TASKS FROM CONS ACTION FROM NEW 
			SELECT DISTINCT  
				 OwnerID=X3.ID
				,WhatId=CASE WHEN T2.KeyInd='O' THEN X1.ID END 
				,WhoId=CASE WHEN T2.KeyInd='I' THEN X2.ID END 
		 		,RE_ACImpID__c=CASE WHEN T2.[NEW_TGT_IMPORT_ID] IS NULL THEN NULL ELSE  (T1.ActionAffiliation+'-'+T1.CONSID) END
				,T1.ActionDate  AS ActivityDate
				,T1.ActionCategory AS Type__c
				,T1.ActionSTatus AS Archived_Status__c
				,T1.ActionSTatus AS Move__c
				,T1.ActionType AS Stage__c
				,T1.ActionType AS [Subject]
 				,T1.ActionType AS Archived_Action_Type__c
 				,T1.ActionAffiliation AS Affiliation__c
 	 			,T1.Category AS Category__c
				,T1.Subcategory AS Subcategory__c
				,CASE WHEN (T1.ActionDate < GETDATE()) THEN 'Completed' ELSE 'Open' END	AS [Status]
				,T1.ActionSolicitor AS RE_Action_Solicitor__c
					,LTRIM(RTRIM(CASE WHEN T1.[Description]!='' THEN COALESCE('Description: ' + LTRIM(RTRIM(T1.[Description])), '') ELSE '' END +  CHAR(10) +
	        	   + CASE WHEN T1.Notes!='' THEN COALESCE('Notes: ' + LTRIM(RTRIM(T1.Notes)), '') ELSE '' END)) AS [Description]

				,T1.ActionAffiliation AS zrefAffiliation
				,T1.RE_DB AS zrefRE_DB
  				,T1.CONSID AS zrefConsId
				,T1.firstname AS zrefFName
				,T1.lastname AS zrefLName
			INTO SUTTER_1P_MIGRATION.IMP.DLT_TASKS_new		
			FROM SUTTER_1P_MIGRATION.[dbo].[ConsoliatedDowntimeActions_new_fixed] AS T1
			LEFT JOIN [SUTTER_1P_DATA].DBO.[HC_Constituents_v] AS T2 ON T1.RE_DB=T2.[RE_DB_OwnerShort] AND T1.CONSID=T2.[ConsID]
 			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_ActionSolicitor T4 ON T1.actionsolicitor=T4.NAME 
  	 		LEFT JOIN SUTTER_1P_MIGRATION.XTR.ACCOUNT X1 ON T2.NEW_TGT_IMPORT_ID=X1.External_Id__c
			LEFT JOIN SUTTER_1P_MIGRATION.XTR.CONTACT X2 ON T2.NEW_TGT_IMPORT_ID=X2.External_Id__c
			LEFT JOIN SUTTER_1P_MIGRATION.XTR.USERS X3 ON T4.SF_User_Email=X3.Email
	 	 	ORDER BY whatId, WhoId, T1.RE_DB, T1.CONSID

			 																 


		--TASKS FROM CONS ACTION missing

			SELECT DISTINCT 
				 OwnerID=X3.ID
				,WhatId=CASE WHEN T5.KeyInd='O' THEN X1.ID END 
				,WhoId=CASE WHEN T5.KeyInd='I' THEN X2.ID END 
		 		,RE_ACImpID__c=CASE WHEN T5.[NEW_TGT_IMPORT_ID] IS NULL THEN NULL ELSE  (T.[zrefAfil] +'-'+T.CONSID) END
				,T1.ActionDate  AS ActivityDate
				,T1.ActionCategory AS Type__c
				,T1.ActionSTatus AS Archived_Status__c
				,T1.ActionSTatus AS Move__c
				,T1.ActionType AS Stage__c
				,T1.ActionType AS [Subject]
 				,T1.ActionType AS Archived_Action_Type__c
 				,T1.Affiliation AS Affiliation__c
 	 			,T1.Category AS Category__c
				,T1.Subcategory AS Subcategory__c
				,CASE WHEN (T1.ActionDate < GETDATE()) THEN 'Completed' ELSE 'Open' END	AS [Status]
				,T1.ActionSolicitor AS RE_Action_Solicitor__c
					,LTRIM(RTRIM(CASE WHEN T1.[Description]!='' THEN COALESCE('Description: ' + LTRIM(RTRIM(T1.[Description])), '') ELSE '' END +  CHAR(10) +
	        	   + CASE WHEN T1.Notes!='' THEN COALESCE('Notes: ' + LTRIM(RTRIM(T1.Notes)), '') ELSE '' END)) AS [Description]

				,T1.Affiliation AS zrefAffiliation
				,T1.RE_DB AS zrefRE_DB
  				,T1.CONSID AS zrefConsId
				,T1.firstname AS zrefFName
				,T1.lastname AS zrefLName
			 	,T.[CONSID] zrefNEWCONSID
 			INTO [SUTTER_1P_MIGRATION].IMP.DLT_TASK_missing
			FROM SUTTER_1P_MIGRATION.[dbo].[ConsoliatedDowntimeActions] AS T1
		 	LEFT JOIN [SUTTER_1P_MIGRATION].dbo.SHConsIDs_updated AS T ON T1.CONSID=CAST(T.[zrefConsId] AS nVARCHAR(20)) AND T1.[RE_DB]=T.[zrefRE_DB]
			LEFT JOIN [SUTTER_1P_DATA].DBO.[HC_Constituents_v] AS T2 ON T1.[RE_DB]=T2.[RE_DB_OwnerShort] AND T1.CONSID=T2.[ConsID]
 			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_ActionSolicitor T4 ON T1.zrefSolicitor=T4.NAME 
			LEFT JOIN [SUTTER_1P_DATA].DBO.[HC_Constituents_v] AS T5 ON T.[zrefRE_DB]=T5.[RE_DB_OwnerShort] AND T.CONSID=T5.[ConsID]

  	 	 	LEFT JOIN SUTTER_1P_MIGRATION.XTR.ACCOUNT X1 ON T5.NEW_TGT_IMPORT_ID=X1.External_Id__c
			LEFT JOIN SUTTER_1P_MIGRATION.XTR.CONTACT X2 ON T5.NEW_TGT_IMPORT_ID=X2.External_Id__c
			LEFT JOIN SUTTER_1P_MIGRATION.XTR.USERS X3 ON T4.SF_User_Email=X3.Email
	 		WHERE T2.[RE_DB_OwnerShort] IS NULL AND  T2.[ConsID] IS null
	 		ORDER BY WhatId, WhoId, T1.RE_DB, T1.CONSID
 	
		 
		 	 SELECT * FROM [SUTTER_1P_MIGRATION].IMP.DLT_TASK_missing
		
		
		
		
		
		 
	---- 	 	
		DROP TABLE [SUTTER_1P_MIGRATION].IMP.DLT_ACTION											  
	 
		SELECT DISTINCT
 		External_ID__c=CASE WHEN T2.[NEW_TGT_IMPORT_ID] IS NULL THEN NULL ELSE 
			(T1.Affiliation+'-'+T1.CONSID+'-'+CAST(ROW_NUMBER() OVER (ORDER BY T2.[NEW_TGT_IMPORT_ID]) AS NVARCHAR(4))) END 
		,[Account__r:External_ID__c]=CASE WHEN T2.KeyInd='O' THEN T2.[NEW_TGT_IMPORT_ID] 
								WHEN (T2.KeyInd='I' AND T12.NoHH_ImportID IS NOT NULL) THEN T12.HH_ImportID ELSE T2.NEW_TGT_IMPORT_ID END
		,[Proposal__r:External_ID__c]=T3.RE_DB_OwnerShort+'-PP-'+T3.[PRImpID] 	
		,T1.ActionCategory AS Attribute_Category__c
		,T1.ActionType AS Stage__c
		,T1.ActionType AS [Subject]
		,T1.Affiliation AS Affiliation__c
		,T1.ActionDate AS Date__c
		,T1.ActionSTatus AS Archived_Status__c
		,CASE WHEN (CAST(T1.ActionDate AS DATE) < GETDATE()) THEN 'Completed' ELSE 'In Progress' END AS [Status] 
	 	,T1.Category AS Category__c
		,T1.Subcategory AS Subcategory__c
 		,CASE WHEN T1.ActionSTatus ='Primary' OR T1.ActionSTatus='Support' THEN T1.ActionStatus ELSE NULL END AS Move__c
		,T1.ActionSolicitor AS RE_Action_Solicitor__c
		,LTRIM(RTRIM(CASE WHEN T1.[Description]!='' THEN COALESCE('Description: ' + LTRIM(RTRIM(T1.[Description])), '') ELSE '' END +  CHAR(10) +
	        	   + CASE WHEN T1.Notes!='' THEN COALESCE('Notes: ' + LTRIM(RTRIM(T1.Notes)), '') ELSE '' END)) AS Comments__c

		--ref
 			,T1.CONSID zrefConsId
			,T1.affiliation zrefAffiliation
			,T1.proposalname AS zrefProposalName
			,CAST(T1.ActionSolicitor AS VARCHAR(50)) AS zrefSolicitor
			,T1.SolicitorRecordType AS zrefSolicitorRecordType

		,ROW_NUMBER() OVER (ORDER BY T2.[NEW_TGT_IMPORT_ID]) AS seq			 
	 	
		INTO [SUTTER_1P_MIGRATION].IMP.DLT_ACTION	   --DeltaLoadTemplate
		FROM [dbo].[ConsoliatedDowntimeActions] AS T1
		LEFT JOIN [SUTTER_1P_DATA].DBO.[HC_Constituents_v] AS T2 ON T1.RE_DB=T2.[RE_DB_OwnerShort] AND T1.CONSID=T2.[ConsID]
 		LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH AS T12 ON T2.[NEW_TGT_IMPORT_ID]=T12.NoHH_ImportID
		LEFT JOIN [SUTTER_1P_DATA].dbo.[HC_Proposal_v] AS T3 ON T1.ProposalName =T3.[PRName]  
		LEFT JOIN SUTTER_1P_MIGRATION.XTR.ACCOUNT AS X1 ON T2.NEW_TGT_IMPORT_ID=X1.External_Id__c
		LEFT JOIN SUTTER_1P_MIGRATION.XTR.CONTACT AS X2 ON T2.NEW_TGT_IMPORT_ID=X2.External_Id__c
		ORDER BY seq
 	 		
			BEGIN 
		  		USE [SUTTER_1P_MIGRATION] 
 				EXEC sp_FindStringInTable '%"%', 'IMP', 'DLT_ACTION'
 			END		
 
	UPDATE [SUTTER_1P_MIGRATION].[IMP].DLT_ACTION
	SET zrefSolicitor='Benjamin, Kristen Kelly'
	WHERE zrefSolicitor='Benjamin, Kristen'

	UPDATE [SUTTER_1P_MIGRATION].[IMP].DLT_ACTION
	SET zrefSolicitor='Ethel Bernal'
	WHERE zrefSolicitor='Bernal, Ethel'

	UPDATE [SUTTER_1P_MIGRATION].[IMP].DLT_ACTION
	SET zrefSolicitor='Dufresne, Beau P.'
	WHERE zrefSolicitor='Dufresne, Beau'

	UPDATE [SUTTER_1P_MIGRATION].[IMP].DLT_ACTION
	SET zrefSolicitor='Schemel, Leslie Miller'
	WHERE zrefSolicitor='Schemel, Leslie'

	BEGIN--ACTION PARTICIPANT   
					DROP TABLE SUTTER_1P_MIGRATION.IMP.[DLT_ACTION_PARTICIPANT]	
	
	 				SELECT DISTINCT
					[Action__r:External_ID__c]=T.External_Id__c
  					,RecordType=dbo.fnc_RecordType('Action_Participant__c_Philanthropy')    
					,User__c= CASE WHEN T.zrefSolicitor!='' THEN X3.ID ELSE NULL END    
				 
					,T.zrefSolicitor
			 	
					INTO SUTTER_1P_MIGRATION.IMP.[DLT_ACTION_PARTICIPANT]	
					FROM [SUTTER_1P_MIGRATION].[IMP].DLT_ACTION  T
				 	LEFT JOIN SUTTER_1P_DATA.DBO.CHART_ActionSolicitor T8 ON T.zrefSolicitor=T8.Name --AND T.zrefAffiliation=T8.RE_DB_OwnerShort		   --ACtion AddedBy 
		 		 	LEFT JOIN SUTTER_1P_MIGRATION.XTR.USERS X3 ON T8.SF_User_Email=X3.Email 
					WHERE T.zrefSolicitor!=''  AND T.External_Id__c IS NOT null

	END			 	
	
 				  						    