

USE [SUTTER_1P_MIGRATION]
GO

BEGIN
	DROP TABLE SUTTER_1P_MIGRATION.IMP.CONTACT
END 

BEGIN--CONTACTS

			--CONTACTS: CONSTITUENTS HOUSEHOLD_CONTACTS
			SELECT DISTINCT
			 dbo.fnc_OwnerID() AS OwnerID
			,dbo.fnc_RecordType('Contact_Household') AS RecordTypeID
 		 	,CASE WHEN T.ConsID IS NULL OR T.ConsID ='' THEN '' ELSE T.RE_DB_OwnerShort+'-'+T.ConsID END AS	RE_ConsID__c
			,T.RE_DB_OwnerShort+'-'+T.ImportID AS RE_ImportID__c
			,T.RE_DB_OwnerShort+'-'+T.ImportID AS External_ID__c
			,T.RE_DB_OwnerShort+'-'+T.ImportID AS [ACCOUNT:External_ID__c]
			,NULL AS RE_IRImpID__c
		 
			,T.MDM_PTY_ID as MDM_PTY_ID__c

			,CASE WHEN LEN(T.Bday)=10 THEN T.Bday END AS BirthDate
			,CASE WHEN LEN(T.Bday)=10 THEN MONTH(T.BDay ) WHEN LEN(T.Bday)=7 THEN LEFT(T.Bday,2) WHEN LEN(T.Bday)=4 THEN NULL END AS rC_Bios__Birth_Month__c
			,CASE WHEN LEN(T.Bday)=10 THEN DAY(T.BDay ) END AS rC_Bios__Birth_Day__c
			,CASE WHEN LEN(T.Bday)=10 THEN YEAR(T.BDay )  WHEN LEN(T.Bday)=7 THEN RIGHT(T.Bday,4) WHEN LEN(T.Bday)=4 THEN T.Bday END AS rC_Bios__Birth_Year__c
			
			,T.Deceased	AS	rC_Bios__Deceased__c
			,CASE WHEN LEN(T.DecDate)=10 THEN T.DecDate END AS rC_Bios__Deceased_Date__c
			,CASE WHEN LEN(T.DecDate)=10 THEN MONTH(T.DecDate ) WHEN LEN(T.DecDate)=7 THEN LEFT(T.DecDate,2) WHEN LEN(T.DecDate)=4 THEN NULL END AS rC_Bios__Deceased_Month__c
			,CASE WHEN LEN(T.DecDate)=10 THEN DAY(T.DecDate ) END AS rC_Bios__Deceased_Day__c
			,CASE WHEN LEN(T.DecDate)=10 THEN YEAR(T.DecDate )  WHEN LEN(T.DecDate)=7 THEN RIGHT(T.DecDate,4) WHEN LEN(T.DecDate)=4 THEN T.DecDate END AS rC_Bios__Deceased_Year__c

			,T.Ethnicity	AS	rC_Bios__Ethnicity__c
			,T.FirstName	AS	FirstName
			,T.Gender	AS	rC_Bios__Gender__c
			,T.GivesAnon	AS	Gives_Anonymously__c
			,T.NoValidAddr	AS	No_Valid_Address__c
			,CASE WHEN T.Deceased ='TRUE' THEN 'FALSE' WHEN T.IsInactive ='TRUE' THEN 'FALSE' WHEN T.IsInactive ='FALSE' THEN 'TRUE' END AS	rC_Bios__Active__c
			,T.IsSolicitor	AS	Is_Solicitor__c
			,T.LastName	AS	LastName
			,T.MaidName	AS	rC_Bios__Maiden_Name__c
			,T.MrtlStat	AS	rC_Bios__Marital_Status__c
			,T.MidName	AS	rC_Bios__Middle_Name__c
			,T.Nickname	AS	Nick_Name__c
			,T.NoEmail	AS	HasOptedOutOfEmail
			,T.Suff1	AS	Suffix_1__c
			,T.Suff2	AS	Suffix_2__c
			,T.Titl1	AS	Salutation
			,T.Titl2	AS	Title_2__c
			,T.DateAdded	AS	CreatedDate
			-- ,T.RE_DB_OwnerShort	AS	rC_Giving__Primary_Affiliation__c NOTE:formulat field.
			,CASE WHEN T.Deceased='TRUE' THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Preferred_Contact__c
			,'FALSE' AS rC_Bios__Secondary_Contact__c
			
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
			,GETDATE() AS	rC_Giving__Rollup_Soft_Credits__c
			,T4.Do_Not_Phone__c
			,T4.Do_Not_Solicit__c
			,T4.Do_Not_Mail__c
			,T4.Do_Not_Contact__c
			,CASE	WHEN T6.HomePhone IS NOT NULL THEN 'Home' 
					WHEN T6.rC_Bios__Work_Phone__c IS NOT NULL THEN 'Work' 
					WHEN T6.MobilePhone IS NOT NULL THEN 'Mobile'
					WHEN T6.rC_Bios__Other_Phone__c IS NOT NULL THEN 'Other' ELSE NULL 
					END AS rC_Bios__Preferred_Phone__c
			,CASE	WHEN T6.rC_Bios__Home_Email__c IS NOT NULL THEN 'Home' 
					WHEN T6.rC_Bios__Work_Email__c IS NOT NULL THEN 'Work' 
					WHEN T6.rC_Bios__Other_Email__c IS NOT NULL THEN 'Other' 
					END AS rC_Bios__Preferred_Email__c
			,T6.Fax
			,T6.HomePhone
			,T6.MobilePhone
			,T6.Pager__c  
			,T6.rC_Bios__Home_Email__c
			,T6.rC_Bios__Other_Email__c
			,T6.rC_Bios__Other_Phone__c
			,T6.rC_Bios__Work_Email__c
			,T6.rC_Bios__Work_Phone__c
			,T6.Website__c

			,T6.rC_Bios__Home_Do_Not_Call__c
			,T6.rC_Bios__Mobile_Do_Not_Call__c
			,T6.rC_Bios__Work_Do_Not_Call__c
			--,T6.rC_Bios__Other_Do_Not_Call__c 

			,CAST(T7.Additional_Phone_Numbers__c AS NVARCHAR(4000)) AS Additional_Phone_Numbers__c
			
			,CAST(T8.Agefinder_Birthdate__c AS NVARCHAR(255)) AS Agefinder_Birthdate__c
			,CAST(T8.CMS_Age__c  AS NVARCHAR(255)) AS CMS_Age__c
			,CAST(T8.PhoneFinder__c  AS NVARCHAR(255)) AS PhoneFinder__c
			,CAST(T8.SMCF_Employee_ID__c AS NVARCHAR(255)) AS SMCF_Employee_ID__c 
			,CAST(T8.SAFH_Employee_ID__c AS NVARCHAR(255)) AS SAFH_Employee_ID__c

			
			,CASE WHEN T9.AddrLine1 IS NULL THEN NULL WHEN T9.AddrLine2!='' THEN T9.AddrLine1 +' '+T9.AddrLine2 ELSE T9.AddrLine1 END AS MailingStreet
			,T9.AddrCity AS MailingCity
			,T9.AddrState AS MailingState
			,T9.AddrZIP AS MailingPostalCode
			,T9.AddrCountry MailingCountry
			
			,T10.Merged_from__c
			,T10.Merged_from_ConsID__c
			,Proxy_Contact__c='FALSE'

			--reference only
			,'Cons Ind HeadOfHH' AS zrefSource
			--,T.KeyInd zrefKeyInd
			--,T.ConsID zrefConsID
			--,T.ImportID zrefImportID
			--,T.RE_DB_OwnerShort zrefDBOwner
			--,T2.IRLink	AS zrefIRLink
			--,T2.HH_ImportID AS zrefHH_ImportID 
			--,T2.HH_ConsID AS zrefHH_ConsID
			--,T2.NoHH_ImportID AS zrefNoHH_ImportID
			--,T2.NoHH_ConsId AS zrefNoHH_ConsId
			--,T2.IRIsSpouse AS zrefIsSpouse

			INTO SUTTER_1P_MIGRATION.IMP.CONTACT
			FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T	
			INNER JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T2 ON T.RE_DB_ID=T2.HH_ImportID    --HH_ImportID  is already the new_tgt_importId
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.SolicitCodes T4 ON T.RE_DB_ID=T4.NEW_TGT_IMPORT_ID 
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Phones_seq_1_final T6 ON T.RE_DB_ID=T6.NEW_TGT_IMPORT_ID 
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Phones_seq_2_final T7 ON T.RE_DB_ID=T7.NEW_TGT_IMPORT_ID   
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Attributes_to_Contact T8 ON T.RE_DB_ID=T8.NEW_TGT_IMPORT_ID 
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Address_Pref	T9 ON T.RE_DB_ID=T9.RE_DB_ID 
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.CONSTITUENTS_MERGED_FROM T10 ON T.NEW_TGT_IMPORT_ID=T10.NEW_TGT_IMPORT_ID
 			--858608
		
 
			UNION ALL 
			--CONTACTS: CONSTITUENTS NON_HOUSEHOLD_CONTACT
			SELECT DISTINCT
			 dbo.fnc_OwnerID() AS OwnerID
			,dbo.fnc_RecordType('Contact_Household') AS RecordTypeID
 		 	,CASE WHEN T.ConsID IS NULL OR T.ConsID ='' THEN '' ELSE T.RE_DB_OwnerShort+'-'+T.ConsID END AS	RE_ConsID__c
			,T.RE_DB_OwnerShort+'-'+T.ImportID AS RE_ImportID__c
			,T.RE_DB_OwnerShort+'-'+T.ImportID AS External_ID__c
			,T2.HH_ImportID AS [ACCOUNT:External_ID__c]
			,NULL AS RE_IRImpID__c			
		
			,T.MDM_PTY_ID as MDM_PTY_ID__c

			,CASE WHEN LEN(T.Bday)=10 THEN T.Bday END AS BirthDate
			,CASE WHEN LEN(T.Bday)=10 THEN MONTH(T.BDay ) WHEN LEN(T.Bday)=7 THEN LEFT(T.Bday,2) WHEN LEN(T.Bday)=4 THEN NULL END AS rC_Bios__Birth_Month__c
			,CASE WHEN LEN(T.Bday)=10 THEN DAY(T.BDay ) END AS rC_Bios__Birth_Day__c
			,CASE WHEN LEN(T.Bday)=10 THEN YEAR(T.BDay )  WHEN LEN(T.Bday)=7 THEN RIGHT(T.Bday,4) WHEN LEN(T.Bday)=4 THEN T.Bday END AS rC_Bios__Birth_Year__c
			
			,T.Deceased	AS	rC_Bios__Deceased__c
			,CASE WHEN LEN(T.DecDate)=10 THEN T.DecDate END AS rC_Bios__Deceased_Date__c
			,CASE WHEN LEN(T.DecDate)=10 THEN MONTH(T.DecDate ) WHEN LEN(T.DecDate)=7 THEN LEFT(T.DecDate,2) WHEN LEN(T.DecDate)=4 THEN NULL END AS rC_Bios__Deceased_Month__c
			,CASE WHEN LEN(T.DecDate)=10 THEN DAY(T.DecDate ) END AS rC_Bios__Deceased_Day__c
			,CASE WHEN LEN(T.DecDate)=10 THEN YEAR(T.DecDate )  WHEN LEN(T.DecDate)=7 THEN RIGHT(T.DecDate,4) WHEN LEN(T.DecDate)=4 THEN T.DecDate END AS rC_Bios__Deceased_Year__c

			,T.Ethnicity	AS	rC_Bios__Ethnicity__c
			,T.FirstName	AS	FirstName
			,T.Gender	AS	rC_Bios__Gender__c
			,T.GivesAnon	AS	Gives_Anonymously__c
			,T.NoValidAddr	AS	No_Valid_Address__c
			,CASE WHEN T.Deceased ='TRUE' THEN 'FALSE' WHEN T.IsInactive ='TRUE' THEN 'FALSE' WHEN T.IsInactive ='FALSE' THEN 'TRUE' END AS	rC_Bios__Active__c
			,T.IsSolicitor	AS	Is_Solicitor__c
			,T.LastName	AS	LastName
			,T.MaidName	AS	rC_Bios__Maiden_Name__c
			,T.MrtlStat	AS	rC_Bios__Marital_Status__c
			,T.MidName	AS	rC_Bios__Middle_Name__c
			,T.Nickname	AS	Nick_Name__c
			,T.NoEmail	AS	HasOptedOutOfEmail
			,T.Suff1	AS	Suffix_1__c
			,T.Suff2	AS	Suffix_2__c
			,T.Titl1	AS	Salutation
			,T.Titl2	AS	Title_2__c
			,T.DateAdded	AS	CreatedDate
			-- ,T.RE_DB_OwnerShort	AS	rC_Giving__Primary_Affiliation__c NOTE:formulat field.
			,'FALSE' AS rC_Bios__Preferred_Contact__c
			,CASE WHEN T.Deceased='TRUE' THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Secondary_Contact__c
			
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
			,GETDATE() AS	rC_Giving__Rollup_Soft_Credits__c
		
			,T4.Do_Not_Phone__c
			,T4.Do_Not_Solicit__c
			,T4.Do_Not_Mail__c
			,T4.Do_Not_Contact__c
					
			,CASE	WHEN T6.HomePhone IS NOT NULL THEN 'Home' 
					WHEN T6.rC_Bios__Work_Phone__c IS NOT NULL THEN 'Work' 
					WHEN T6.MobilePhone IS NOT NULL THEN 'Mobile'
					WHEN T6.rC_Bios__Other_Phone__c IS NOT NULL THEN 'Other' ELSE NULL 
					END AS rC_Bios__Preferred_Phone__c
			,CASE	WHEN T6.rC_Bios__Home_Email__c IS NOT NULL THEN 'Home' 
					WHEN T6.rC_Bios__Work_Email__c IS NOT NULL THEN 'Work' 
					WHEN T6.rC_Bios__Other_Email__c IS NOT NULL THEN 'Other' 
					END AS rC_Bios__Preferred_Email__c
			,T6.Fax
			,T6.HomePhone
			,T6.MobilePhone
			,T6.Pager__c  
			,T6.rC_Bios__Home_Email__c
			,T6.rC_Bios__Other_Email__c
			,T6.rC_Bios__Other_Phone__c
			,T6.rC_Bios__Work_Email__c
			,T6.rC_Bios__Work_Phone__c
			,T6.Website__c

			,T6.rC_Bios__Home_Do_Not_Call__c
			,T6.rC_Bios__Mobile_Do_Not_Call__c
			,T6.rC_Bios__Work_Do_Not_Call__c
			--,T6.rC_Bios__Other_Do_Not_Call__c 

			,CAST(T7.Additional_Phone_Numbers__c AS NVARCHAR(4000)) AS Additional_Phone_Numbers__c
			,CAST(T8.Agefinder_Birthdate__c AS NVARCHAR(255)) AS Agefinder_Birthdate__c
			,CAST(T8.CMS_Age__c  AS NVARCHAR(255)) AS CMS_Age__c
			,CAST(T8.PhoneFinder__c  AS NVARCHAR(255)) AS PhoneFinder__c
			,CAST(T8.SMCF_Employee_ID__c AS NVARCHAR(255)) AS  SMCF_Employee_ID__c 
			,CAST(T8.SAFH_Employee_ID__c AS NVARCHAR(255)) AS  SAFH_Employee_ID__c

			,CASE WHEN T9.AddrLine1 IS NULL THEN NULL WHEN T9.AddrLine2!='' THEN T9.AddrLine1 +' '+T9.AddrLine2 ELSE T9.AddrLine1 END AS MailingStreet
			,T9.AddrCity AS MailingCity
			,T9.AddrState AS MailingState
			,T9.AddrZIP AS MailingPostalCode
			,T9.AddrCountry MailingCountry

			,T10.Merged_from__c
			,T10.Merged_from_ConsID__c
			,Proxy_Contact__c='FALSE'

			--reference only
			,'Cons Ind NoHH' AS zrefSource
			---,T.KeyInd refKeyInd
			--,T.ConsID refConsID
			--,T.ImportID refImportID
			--,T.RE_DB_OwnerShort refDBOwner
			--,T2.IRLink	AS zrefIRLink
			--,T2.HH_ImportID AS zrefHH_ImportID 
			--,T2.HH_ConsID AS zrefHH_ConsID
			--,T2.NoHH_ImportID AS zrefNoHH_ImportID
			--,T2.NoHH_ConsId AS zrefNoHH_ConsId
			--,T2.IRIsSpouse AS zrefIsSpouse
			
			FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T	
			INNER JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T2 ON T.RE_DB_ID=T2.NoHH_ImportID	--NoHH_ImportID  is already the new_tgt_importId
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.SolicitCodes T4 ON T.RE_DB_ID=T4.NEW_TGT_IMPORT_ID
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Phones_seq_1_final T6 ON T.RE_DB_ID=T6.NEW_TGT_IMPORT_ID
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Phones_seq_2_final T7 ON T.RE_DB_ID=T7.NEW_TGT_IMPORT_ID
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Attributes_to_Contact T8 ON T.RE_DB_ID=T8.NEW_TGT_IMPORT_ID
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Address_Pref	T9 ON T.RE_DB_ID=T9.RE_DB_ID 
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.CONSTITUENTS_MERGED_FROM T10 ON T.NEW_TGT_IMPORT_ID=T10.NEW_TGT_IMPORT_ID
  				   
		UNION ALL	
		
			--CONTACTS: NON_CONSTITUENT INDIVIDUAL RELATIONSHIPS
			SELECT DISTINCT
			dbo.fnc_OwnerID() AS OwnerID
			,CASE WHEN T8.KeyInd='I' then dbo.fnc_RecordType('Contact_Household') WHEN T8.KeyInd='O' THEN dbo.fnc_RecordType('Contact_Organizational') END AS RecordTypeID
			,NULL AS RE_ConsID__c
			,NULL AS RE_ImportID__c
			,'IR-'+T.NEW_TGT_IMPORT_ID_IR AS External_ID__c
			,CASE WHEN T2.HH_ImportID IS NOT NULL THEN T2.HH_ImportID ELSE T.NEW_TGT_IMPORT_ID_CN END AS [ACCOUNT:External_ID__c]
			,T.NEW_TGT_IMPORT_ID_IR AS RE_IRImpID__c
		
			,T.MDM_PTY_ID AS MDM_PTY_ID__c

			,CASE WHEN LEN(T.IRBDay)=10 THEN T.IRBDay END AS BirthDate
			,CASE WHEN LEN(T.IRBDay)=10 THEN MONTH(T.IRBDay ) WHEN LEN(T.IRBDay)=7 THEN LEFT(T.IRBDay,2) WHEN LEN(T.IRBDay)=4 THEN NULL END AS rC_Bios__Birth_Month__c
			,CASE WHEN LEN(T.IRBDay)=10 THEN DAY(T.IRBDay ) END AS rC_Bios__Birth_Day__c
			,CASE WHEN LEN(T.IRBDay)=10 THEN YEAR(T.IRBDay )  WHEN LEN(T.IRBDay)=7 THEN RIGHT(T.IRBDay,4) WHEN LEN(T.IRBDay)=4 THEN T.IRBDay END AS rC_Bios__Birth_Year__c
			
			,T.IRDeceased	AS	rC_Bios__Deceased__c
			,CASE WHEN LEN(T.IRDecDate)=10 THEN T.IRDecDate END AS rC_Bios__Deceased_Date__c
			,CASE WHEN LEN(T.IRDecDate)=10 THEN MONTH(T.IRDecDate ) WHEN LEN(T.IRDecDate)=7 THEN LEFT(T.IRDecDate,2) WHEN LEN(T.IRDecDate)=4 THEN NULL END AS rC_Bios__Deceased_Month__c
			,CASE WHEN LEN(T.IRDecDate)=10 THEN DAY(T.IRDecDate ) END AS rC_Bios__Deceased_Day__c
			,CASE WHEN LEN(T.IRDecDate)=10 THEN YEAR(T.IRDecDate )  WHEN LEN(T.IRDecDate)=7 THEN RIGHT(T.IRDecDate,4) WHEN LEN(T.IRDecDate)=4 THEN T.IRDecDate END AS rC_Bios__Deceased_Year__c

			,NULL AS rC_Bios__Ethnicity__c
			,T.IRFirstName	AS	FirstName
			,T.IRGender AS rC_Bios__Gender__c
			,NULL AS Gives_Anonymously__c
			,NULL AS No_Valid_Address__c
			,CASE WHEN T.IRDeceased='TRUE' THEN 'FALSE' ELSE 'TRUE' END AS	rC_Bios__Active__c
			,NULL AS Is_Solicitor__c
			,T.IRLastName	AS	LastName
			,T.IRMaidName	AS	rC_Bios__Maiden_Name__c
			,NULL AS rC_Bios__Marital_Status__c
			,T.IRMidName	AS	rC_Bios__Middle_Name__c
			,T.IRNickname	AS	Nick_Name__c
			,NULL AS HasOptedOutOfEmail
			,T.IRSuff1	AS	Suffix_1__c
			,T.IRSuff2	AS	Suffix_2__c
			,T.IRTitl1	AS	Salutation
			,T.IRTitl2	AS	Title_2__c
			,NULL AS CreatedDate
			-- ,T.RE_DB_OwnerShort	AS	rC_Giving__Primary_Affiliation__c NOTE:formulat field.
			,'FALSE' AS	rC_Bios__Preferred_Contact__c
			,T.IRIsSpouse AS rC_Bios__Secondary_Contact__c
	
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
			,GETDATE() AS	rC_Giving__Rollup_Soft_Credits__c
	
			,NULL AS Do_Not_Phone__c
			,NULL AS Do_Not_Solicit__c
			,NULL AS Do_Not_Mail__c
			,NULL AS Do_Not_Contact__c
					
			,CASE	WHEN T6.HomePhone IS NOT NULL THEN 'Home' 
					WHEN T6.rC_Bios__Work_Phone__c IS NOT NULL THEN 'Work' 
					WHEN T6.MobilePhone IS NOT NULL THEN 'Mobile'
					WHEN T6.rC_Bios__Other_Phone__c IS NOT NULL THEN 'Other' ELSE NULL 
					END AS rC_Bios__Preferred_Phone__c
			,CASE	WHEN T6.rC_Bios__Home_Email__c IS NOT NULL THEN 'Home' 
					WHEN T6.rC_Bios__Work_Email__c IS NOT NULL THEN 'Work' 
					WHEN T6.rC_Bios__Other_Email__c IS NOT NULL THEN 'Other' 
					END AS rC_Bios__Preferred_Email__c
			,T6.Fax
			,T6.HomePhone
			,T6.MobilePhone
			,T6.Pager__c  
			,T6.rC_Bios__Home_Email__c
			,T6.rC_Bios__Other_Email__c
			,T6.rC_Bios__Other_Phone__c
			,T6.rC_Bios__Work_Email__c
			,T6.rC_Bios__Work_Phone__c
			,T6.Website__c
			
			,NULL AS rC_Bios__Home_Do_Not_Call__c
			,NULL AS rC_Bios__Mobile_Do_Not_Call__c
			,NULL AS rC_Bios__Work_Do_Not_Call__c
			--,T6.rC_Bios__Other_Do_Not_Call__c 

			,CAST(T7.Additional_Phone_Numbers__c AS NVARCHAR(4000)) AS Additional_Phone_Numbers__c

			,NULL AS Agefinder_Birthdate__c
			,NULL AS CMS_Age__c
			,NULL AS PhoneFinder__c
			,NULL AS SMCF_Employee_ID__c 
			,NULL AS SAFH_Employee_ID__c

			,CASE WHEN T.IRAddrLine1 IS NULL THEN NULL WHEN T.IRAddrLine2!='' THEN T.IRAddrLine1 +' '+T.IRAddrLine2 ELSE T.IRAddrLine1 END AS MailingStreet
			,T.IRAddrCity AS MailingCity
			,T.IRAddrState AS MailingState
			,T.IRAddrZIP AS MailingPostalCode
			,T.IRAddrCountry MailingCountry

			,T10.Merged_from__c
			,NULL AS Merged_from_ConsID__c
			,Proxy_Contact__c='FALSE'

			--reference 
			,'NonCons IndRel' AS zrefSource
			--,NULL  AS refKeyInd
			--,T.ConsID AS refConsID
			--,T.ImportID AS refImportID			
			--,T.RE_DB_OwnerShort refDBOwner
			--,T2.IRLink	AS zrefIRLink
			--,T2.HH_ImportID AS zrefHH_ImportID 
			--,T2.HH_ConsID AS zrefHH_ConsID
			--,T2.NoHH_ImportID AS zrefNoHH_ImportID
			--,T2.NoHH_ConsId AS zrefNoHH_ConsId
			--,T2.IRIsSpouse AS zrefIsSpouse
			
			FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v T
			INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T8 ON T.NEW_TGT_IMPORT_ID_CN=T8.RE_DB_ID 
			INNER JOIN SUTTER_1P_DATA.TBL.SF_IND_RELAT T9 ON T.RE_DB_ID=T9.IRImpID    --(IRImpID is the NEW_TGT_IMPORT_ID_OR)
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Contact_HofH T2 ON T.NEW_TGT_IMPORT_ID_CN=T2.NoHH_ImportID  
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1_final T6 ON T.NEW_TGT_IMPORT_ID_IR=T6.NEW_TGT_IMPORT_ID_IR 
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2_final T7 ON T.NEW_TGT_IMPORT_ID_IR=T7.NEW_TGT_IMPORT_ID_IR 
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.RELATIONSHIPS_MERGED_FROM_IR T10 ON T.NEW_TGT_IMPORT_ID_IR=T10.NEW_TGT_IMPORT_ID_IR
			WHERE (T.NEW_TGT_IMPORT_ID_IRLINK='' OR T.NEW_TGT_IMPORT_ID_IRLINK IS NULL)   
			
	  	 
		--donor proxy contacts 
			INSERT INTO [SUTTER_1P_MIGRATION].IMP.[CONTACT]
			        ( [OwnerID] ,  [RecordTypeID] ,  [RE_ConsID__c] ,  [RE_ImportID__c] ,  [External_ID__c] ,  [ACCOUNT:External_ID__c] ,
			          [RE_IRImpID__c] ,  [MDM_PTY_ID__c] ,  [BirthDate] ,  
			          [rC_Bios__Birth_Month__c] , [rC_Bios__Birth_Day__c] , [rC_Bios__Birth_Year__c] ,
			          [rC_Bios__Deceased__c] ,  [rC_Bios__Deceased_Date__c] ,
			          [rC_Bios__Deceased_Month__c] ,  [rC_Bios__Deceased_Day__c] ,
			          [rC_Bios__Deceased_Year__c] ,  [rC_Bios__Ethnicity__c] ,
			          [FirstName] ,  [rC_Bios__Gender__c] ,
			          [Gives_Anonymously__c] ,  [No_Valid_Address__c] ,
			          [rC_Bios__Active__c] ,  [Is_Solicitor__c] ,
			          [LastName] ,  [rC_Bios__Maiden_Name__c] ,
			          [rC_Bios__Marital_Status__c] ,
			          [rC_Bios__Middle_Name__c] ,
			          [Nick_Name__c] ,
			          [HasOptedOutOfEmail] ,
			          [Suffix_1__c] ,
			          [Suffix_2__c] ,
			          [Salutation] ,
			          [Title_2__c] ,
			          [CreatedDate] ,
			          [rC_Bios__Preferred_Contact__c] ,
			          [rC_Bios__Secondary_Contact__c] ,
			          [rC_Giving__Rollup_Hard_Credits__c] ,
			          [rC_Giving__Rollup_Soft_Credits__c] ,
			          [Do_Not_Phone__c] ,
			          [Do_Not_Solicit__c] ,
			          [Do_Not_Mail__c] ,
			          [Do_Not_Contact__c] ,
			          [rC_Bios__Preferred_Phone__c] ,
			          [rC_Bios__Preferred_Email__c] ,
			          [Fax] ,
			          [HomePhone] ,
			          [MobilePhone] ,
			          [Pager__c] ,
			          [rC_Bios__Home_Email__c] ,
			          [rC_Bios__Other_Email__c] ,
			          [rC_Bios__Other_Phone__c] ,
			          [rC_Bios__Work_Email__c] ,
			          [rC_Bios__Work_Phone__c] ,
			          [Website__c] ,
			          [rC_Bios__Home_Do_Not_Call__c] ,
			          [rC_Bios__Mobile_Do_Not_Call__c] ,
			          [rC_Bios__Work_Do_Not_Call__c] ,
			          [Additional_Phone_Numbers__c] ,
			          [Agefinder_Birthdate__c] ,
			          [CMS_Age__c] ,
			          [PhoneFinder__c] ,
					  [SMCF_Employee_ID__c], 
					  [SAFH_Employee_ID__c], 

			          [MailingStreet] ,
			          [MailingCity] ,
			          [MailingState] ,
			          [MailingPostalCode] ,
			          [MailingCountry] ,
			          [Merged_from__c] ,
			          [Merged_from_ConsID__c] ,
					  [Proxy_Contact__c],
			          [zrefSource]
			        ) 	SELECT DISTINCT
							dbo.fnc_OwnerID() AS OwnerID
						,dbo.fnc_RecordType('Contact_Organizational') AS RecordTypeID
 		 				,NULL AS RE_ConsID__c
						,T2.NEW_TGT_IMPORT_ID AS RE_ImportID__c
						,T2.NEW_TGT_IMPORT_ID AS External_ID__c
						,T2.NEW_TGT_IMPORT_ID AS [ACCOUNT:External_ID__c]
						,NULL AS RE_IRImpID__c			
		
						,NULL MDM_PTY_ID__c
 						,NULL AS BirthDate
						,NULL AS rC_Bios__Birth_Month__c
						,NULL AS rC_Bios__Birth_Day__c
						,NULL AS rC_Bios__Birth_Year__c
 						,'FALSE' AS rC_Bios__Deceased__c
						,NULL AS rC_Bios__Deceased_Date__c
						,NULL AS rC_Bios__Deceased_Month__c
						,NULL AS rC_Bios__Deceased_Day__c
						,NULL AS rC_Bios__Deceased_Year__c
							,NULL AS rC_Bios__Ethnicity__c
						,'MAILING'	AS	FirstName
						,'' AS rC_Bios__Gender__c
						,NULL AS Gives_Anonymously__c
						,NULL AS No_Valid_Address__c
						,NULL AS rC_Bios__Active__c
						,NULL AS Is_Solicitor__c
						,'CONTACT' AS	LastName
						,NULL AS rC_Bios__Maiden_Name__c
						,NULL AS rC_Bios__Marital_Status__c
						,NULL AS rC_Bios__Middle_Name__c
						,NULL AS Nick_Name__c
						,NULL AS HasOptedOutOfEmail
						,NULL AS Suffix_1__c
						,NULL AS Suffix_2__c
						,NULL AS Salutation
						,NULL AS Title_2__c
						,NULL AS CreatedDate
						,'TRUE' AS rC_Bios__Preferred_Contact__c
						,'FALSE' AS rC_Bios__Secondary_Contact__c
			
						,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
						,GETDATE() AS	rC_Giving__Rollup_Soft_Credits__c
		
						,NULL AS Do_Not_Phone__c
						,NULL AS Do_Not_Solicit__c
						,NULL AS Do_Not_Mail__c
						,NULL AS Do_Not_Contact__c
					
						,NULL AS rC_Bios__Preferred_Email__c
						,NULL AS Fax
						,NULL AS HomePhone
						,NULL AS MobilePhone
						,NULL AS Pager__c  
						,NULL AS  rC_Bios__Home_Email__c
						,NULL AS rC_Bios__Other_Email__c
						,NULL AS rC_Bios__Other_Phone__c
						,NULL AS rC_Bios__Work_Email__c
						,NULL AS rC_Bios__Work_Phone__c
						,NULL AS Website__c

						,NULL AS rC_Bios__Home_Do_Not_Call__c
						,NULL AS rC_Bios__Mobile_Do_Not_Call__c
						,NULL AS rC_Bios__Work_Do_Not_Call__c
						,NULL AS rC_Bios__Other_Do_Not_Call__c 

						,NULL AS Additional_Phone_Numbers__c
						,NULL AS Agefinder_Birthdate__c
						,NULL AS CMS_Age__c
						,NULL AS PhoneFinder__c
						,NULL AS SMCF_Employee_ID__c 
						,NULL AS SAFH_Employee_ID__c

						,CASE WHEN T9.AddrLine1 IS NULL THEN NULL WHEN T9.AddrLine2!='' THEN T9.AddrLine1 +' '+T9.AddrLine2 ELSE T9.AddrLine1 END AS MailingStreet
						,T9.AddrCity AS MailingCity
						,T9.AddrState AS MailingState
						,T9.AddrZIP AS MailingPostalCode
						,T9.AddrCountry MailingCountry

						,NULL AS Merged_from__c
						,NULL AS Merged_from_ConsID__c
						,Proxy_Contact__c='TRUE'
		 				,'DonorProxy' AS zrefSource
			 			FROM SUTTER_1P_MIGRATION.tbl.Account_Org T2 
						LEFT JOIN SUTTER_1P_MIGRATION.IMP.CONTACT T3 ON T2.NEW_TGT_IMPORT_ID=T3.[ACCOUNT:External_ID__c] 
						LEFT JOIN SUTTER_1P_MIGRATION.TBL.Address_Pref	T9 ON T2.RE_DB_ID=T9.RE_DB_ID 

						WHERE T3.[ACCOUNT:External_ID__c] IS NULL 
 


END;
	 
BEGIN --REPLACE DOUBLE QUOTES TO SINGLE QUOTES
  --  SELECT  [FirstName] FROM [SUTTER_1P_MIGRATION].[IMP].[CONTACT] WHERE [FirstName] LIKE '%"%';
  --  SELECT  [rC_Bios__Middle_Name__c] FROM [SUTTER_1P_MIGRATION].[IMP].[CONTACT] WHERE [rC_Bios__Middle_Name__c] LIKE '%"%';
  --  SELECT  [LastName] FROM [SUTTER_1P_MIGRATION].[IMP].[CONTACT] WHERE [LastName] LIKE '%"%';
  --  SELECT  [Nick_Name__c] FROM [SUTTER_1P_MIGRATION].[IMP].[CONTACT] WHERE [Nick_Name__c] LIKE '%"%';
  --  SELECT  [rC_Bios__Maiden_Name__c] FROM [SUTTER_1P_MIGRATION].[IMP].[CONTACT] WHERE [rC_Bios__Maiden_Name__c] LIKE '%"%';
  -- SELECT  [Salutation] FROM [SUTTER_1P_MIGRATION].[IMP].[CONTACT] WHERE [Salutation] LIKE '%"%';
  --  SELECT  [Additional_Phone_Numbers__c] FROM [SUTTER_1P_MIGRATION].[IMP].[CONTACT] WHERE [Additional_Phone_Numbers__c] LIKE '%"%';
        
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET [FirstName]=REPLACE([FirstName],'"','''') where [FirstName] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET [rC_Bios__Middle_Name__c]=REPLACE([rC_Bios__Middle_Name__c],'"','''') where [rC_Bios__Middle_Name__c] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET [LastName]=REPLACE([LastName],'"','''') where [LastName] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET [Nick_Name__c]=REPLACE([Nick_Name__c],'"','''') where [Nick_Name__c] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET [rC_Bios__Maiden_Name__c]=REPLACE([rC_Bios__Maiden_Name__c],'"','''') where [rC_Bios__Maiden_Name__c] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET [Salutation]=REPLACE([Salutation],'"','''') where [Salutation] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET [Additional_Phone_Numbers__c]=REPLACE([Additional_Phone_Numbers__c],'"','''') where [Additional_Phone_Numbers__c] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET [HomePhone]=REPLACE([HomePhone],'"','''') where [HomePhone] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET [rC_Bios__Other_Email__c]=REPLACE([rC_Bios__Other_Email__c],'"','''') where [rC_Bios__Other_Email__c] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET [LastName]='Unknown' where [LastName] IS NULL 
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET [MailingStreet]=REPLACE([MailingStreet],'"','''') where [MailingStreet] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET [MailingCity]=REPLACE([MailingCity],'"','''') where [MailingCity] like '%"%'
			
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET rC_Bios__Work_Phone__c=REPLACE(rC_Bios__Work_Phone__c,'"','''') where rC_Bios__Work_Phone__c like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET MobilePhone=REPLACE(MobilePhone,'"','''') where MobilePhone like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET rC_Bios__Other_Phone__c=REPLACE(rC_Bios__Other_Phone__c,'"','''') where rC_Bios__Other_Phone__c like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET rC_Bios__Other_Phone__c=REPLACE(rC_Bios__Other_Phone__c,'"','''') where rC_Bios__Other_Phone__c like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CONTACT] SET rC_Bios__Home_Email__c=REPLACE(rC_Bios__Home_Email__c,'"','''') where rC_Bios__Home_Email__c like '%"%'
 			
	
END

		BEGIN 
			USE [SUTTER_1P_MIGRATION] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'CONTACT'
 		END 

--Update Deceased Year to Unknown.
		BEGIN
			SELECT rC_Bios__Deceased__c, rC_Bios__Deceased_Date__c, rC_Bios__Deceased_Year__c 
			FROM SUTTER_1P_MIGRATION.IMP.CONTACT
			WHERE rC_Bios__Deceased__c='TRUE' AND rC_Bios__Deceased_Date__c IS null
	
			ALTER TABLE SUTTER_1P_MIGRATION.[IMP].[CONTACT]
			ALTER COLUMN rC_Bios__Deceased_Year__c NVARCHAR(20)
	
			UPDATE SUTTER_1P_MIGRATION.[IMP].[CONTACT]
			SET rC_Bios__Deceased_Year__c='Unknown'
			WHERE rC_Bios__Deceased__c='TRUE' AND rC_Bios__Deceased_Date__c IS NULL
		END

		BEGIN--check duplicates 
			SELECT * 
		--	INTO [SUTTER_1P_MIGRATION].tbl.contact_duplicate
			FROM [SUTTER_1P_MIGRATION].[IMP].[CONTACT] 
			
			WHERE External_ID__c IN (SELECT External_ID__c FROM [SUTTER_1P_MIGRATION].[IMP].[CONTACT] GROUP BY External_ID__c HAVING COUNT(*)>1)
			ORDER BY External_ID__c, zrefSource
		END 

 
		BEGIN-- contacts with no valid Account:ExternalID

				SELECT    T1.*, T2.External_ID__c AS Expr1
				FROM         SUTTER_1P_MIGRATION.IMP.CONTACT AS T1 
							LEFT JOIN SUTTER_1P_MIGRATION.IMP.ACCOUNT AS T2 ON T1.[ACCOUNT:External_ID__c] = T2.External_ID__c
				WHERE     (T2.External_ID__c IS NULL) OR (T2.External_ID__c = N'')
				ORDER BY [ACCOUNT:External_ID__c]
	
		END;

		BEGIN --contacts with no account
				SELECT    T1.*
				FROM         SUTTER_1P_MIGRATION.IMP.CONTACT AS T1 
				WHERE [ACCOUNT:External_ID__c] IS null
		END;

	
		BEGIN --contacts with no lastname
				SELECT    T1.*
				FROM         SUTTER_1P_MIGRATION.IMP.CONTACT AS T1 
				WHERE LastName IS null
		END;

		
		BEGIN --contacts with no external id
				SELECT    T1.*
				FROM         SUTTER_1P_MIGRATION.IMP.CONTACT AS T1 
				WHERE External_Id__c IS null
		END;

		--CREATE INDEX TO SORT RECORDS
        BEGIN

			CREATE INDEX idx_contact  ON SUTTER_1P_MIGRATION.IMP.CONTACT([ACCOUNT:External_ID__c]);
		
		END;


		SELECT *		
		INTO SUTTER_1P_MIGRATION.IMP.CONTACT_srt
		FROM SUTTER_1P_MIGRATION.IMP.CONTACT T1
		ORDER BY [ACCOUNT:External_ID__c], EXTERNAL_Id__c
		1201913
		SELECT * FROM SUTTER_1P_MIGRATION.IMP.CONTACT_srt
		
		

		--DUPLICATE SECONDARY CONTACT LIST. 
		SELECT [RE_ConsID__c], [RE_ImportID__c], [RE_IRImpID__c], [External_ID__c], [ACCOUNT:External_ID__c], [rC_Bios__Preferred_Contact__c], [rC_Bios__Secondary_Contact__c], [FirstName], [LastName]
		FROM imp.[CONTACT]
		WHERE [ACCOUNT:External_ID__c]+[rC_Bios__Secondary_Contact__c] 
		IN (SELECT [ACCOUNT:External_ID__c]+[rC_Bios__Secondary_Contact__c]
			FROM imp.[CONTACT] 
			WHERE [rC_Bios__Secondary_Contact__c]='true'
			GROUP BY [ACCOUNT:External_ID__c]+[rC_Bios__Secondary_Contact__c]
			HAVING COUNT(*)>1)
		ORDER BY [ACCOUNT:External_ID__c]


			SELECT * 
			FROM [SUTTER_1P_MIGRATION].[IMP].[CONTACT] 
			WHERE External_ID__c='ABSF-001846' OR External_ID__c='ABSF-001305'
			OR External_ID__c ='ABSF-000160'
			ORDER BY External_ID__c, zrefSource

			SELECT External_ID__c, 
			rC_Bios__Preferred_Contact__c, rC_Bios__Secondary_Contact__c, zrefSource , RE_ImportID__c AS refImportID, RE_ConsID__c AS refConsId
			FROM SUTTER_1P_MIGRATION.imp.contact
			WHERE zrefSource LIKE '%constit%' 
 
			SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.imp.contact
			
		--TEST
			SELECT TOP 100 * FROM SUTTER_1P_MIGRATION.IMP.CONTACT
			WHERE External_Id__c!=[ACCOUNT:External_ID__c]
			ORDER BY [ACCOUNT:External_ID__c]

			SELECT * FROM SUTTER_1P_MIGRATION.IMP.CONTACT
			WHERE external_id__c='ABSF-000578' OR external_id__c='ABSF-M04351'
		
			SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
			WHERE hh_importid='SCAH-03038-079-0201207'

			SELECT * FROM SUTTER_1P_DATA.dbo.hc_ind_relat_v
			WHERE new_tgt_import_id_cn='ABSF-00001-079-0105284'
		
			SELECT * FROM SUTTER_1P_MIGRATION.IMP.CONTACT
			WHERE [ACCOUNT:External_ID__c]='SCAH-03038-079-0201207'

			SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
			WHERE hh_importid='SCAH-03038-079-0201207'


		 
			SELECT * FROM SUTTER_1P_MIGRATION.imp.account
			WHERE external_id__c='SCAH-03038-593-0000278286'


			SELECT * FROM SUTTER_1P_MIGRATION.IMP.ACCOUNT
			WHERE [External_ID__c]='SCAH-03038-079-0201207'

		


 --exceptions

			DROP TABLE SUTTER_1P_MIGRATION.IMP.CONTACT_XCP

			SELECT		T1.*
	--		INTO		SUTTER_1P_MIGRATION.IMP.CONTACT_XCP
			FROM        SUTTER_1P_MIGRATION.IMP.CONTACT AS T1 
			LEFT JOIN	SUTTER_1P_MIGRATION.XTR.CONTACT AS X1 ON T1.External_ID__c = X1.EXTERNAL_ID__C
			WHERE     (X1.EXTERNAL_ID__C IS NULL) OR (X1.EXTERNAL_ID__C = '')
			ORDER BY T1.External_ID__c
			--102

		SELECT DISTINCT EXTERNAL_ID__C FROM  SUTTER_1P_MIGRATION.IMP.CONTACT
		SELECT COUNT(DISTINCT EXTERNAL_ID__C)  FROM  SUTTER_1P_MIGRATION.IMP.CONTACT 
		-- 1,201,863
		-- 1,201,863
		SELECT COUNT(*)  FROM [SUTTER_1P_MIGRATION].XTR.CONTACT
		-- 1,201,863




			SELECT * FROM SUTTER_1P_MIGRATION.imp.contact WHERE 
			external_id__c='WBRX-325N'

			SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.IMP.CONTACT 