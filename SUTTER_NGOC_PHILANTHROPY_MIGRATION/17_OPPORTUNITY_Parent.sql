USE [SUTTER_1P_MIGRATION]
GO

BEGIN
	DROP TABLE SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_parent
	DROP TABLE SUTTER_1P_MIGRATION.TBL.OPPORTUNITY_PARENT
END	

--*********************************************************************************************************-- 
-- need to add code to create recurring in tbl below as their own parent and transaction
-- [SUTTER_1P_MIGRATION].TBL.GiftAttr_employee_recurring
--CHECK whether or not the actual "recurring gift" needs to be left out, or include with the "Recurring Gift Pay-cash"
--*********************************************************************************************************-- 
  
 BEGIN--PARENT OPPORTUNITIES
 				--PART 1:  Includes all GFTypes noted in the chart, excepted for Recurrign and Recurring Pay-Cash indentified by GFAttributes. 
					SELECT  
					--IDs
						dbo.fnc_OwnerID() AS OwnerID         
						,CAST(T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS VARCHAR(40)) AS External_Id__c
						,CAST(T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS VARCHAR(40)) AS RE_GSplitImpID__c
						,CAST(T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitGFImpID AS VARCHAR(40)) AS RE_GFImpID__c
/*EXT_ID*/				,T.[ACCOUNT:External_Id__c]
/*CLOSE DATE*/			,CASE WHEN T4.GFFirstPaymentDate IS NOT NULL 
							  THEN T4.GFFirstPaymentDate 
							  ELSE T.[GFDate] 
						 END AS CloseDate
						,T.[GFDate] AS Gift_Date__c

						,CASE   WHEN (T.[GFType] ='Pledge' OR T.[GFType]='MG Pledge') 
								THEN T.[rC_Giving__Affiliation__c]+' - Pledge - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10)) 
								ELSE T.[rC_Giving__Affiliation__c]+' - Donation - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10))
						 END AS NAME
 						,dbo.fnc_RecordType('Opportunity_'+T.Parent_Giving_RecordType) AS RecordTypeID
					--CampaignId
/*EXT_ID*/				,T.CAMPAIGN_External_Id__c	AS [CAMPAIGN:External_Id__c]
						,CASE WHEN (T.GFType LIKE '%pledge%' AND  GFPledgeBalance>0) OR (T.GFType ='Recurring Gift' AND T.GFStatus='Active') 
							  THEN 'Open' ELSE 'Completed' END AS StageName
						,T.rC_Giving__Activity_Type__c
						,T.[rC_Giving__Affiliation__c]
						,T.[Fundraising_Program__c]
 			 		--amounts
						,CASE WHEN (T.GFType='Pledge' OR T.GFType='MG Pledge') THEN (T.GFTAmt/T.GFInsNumPay) ELSE T.GSplitAmt END AS rC_Giving__Giving_Amount__c
						,CASE WHEN T.GFType ='Gift-in-Kind' THEN NULL ELSE T.GSplitAmt END AS rC_Giving__Expected_Giving_Amount__c
						,CASE WHEN T.GFType ='Gift-in-Kind' THEN NULL ELSE T.rC_Giving__Current_Giving_Amount__c END AS rC_Giving__Current_Giving_Amount__c
/*InKind*/				,CASE WHEN T.GFType ='Gift-in-Kind' THEN T.GSplitAmt ELSE NULL END AS rC_Giving__Projected_Amount__c				
					--allocations
						,NULL AS  rC_Giving__Allocation_Amount__c   --Only populate with total gift amount when using Special Allocations.
/*EXT_ID*/				,T.GAU_External_Id__c	AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]
  					--frequencies
						,CASE WHEN (T.GFType='Pledge' OR T.GFType='MG Pledge' OR T.GFType='Recurring Gift') 
							THEN T.rC_Giving__Giving_Frequency__c ELSE 'One Payment' END AS rC_Giving__Giving_Frequency__c
						,CASE WHEN (T.GFType='Pledge' OR T.GFType='MG Pledge' OR T.GFType='Recurring Gift') 
							THEN T.rC_Giving__Giving_Frequency__c ELSE 'One Payment' END AS	rC_Giving__Payment_Frequency__c
 						,CASE WHEN (T.[GFInsFreq]='9') THEN T.GFInsNumPay ELSE NULL END AS	rC_Giving__Payment_Count__c	   --only on irregular frequencies
					--givingtypes
						,T.rC_Giving__Is_Giving__c
						,T.rC_Giving__Is_Giving_Donation__c
						,T.rC_Giving__Is_Giving_Inkind__c
						,T.rC_Giving__Is_Sustainer__c
						,GETDATE() AS rC_Giving__Update_Transactions__c
						,GETDATE() AS rC_Giving__Rollup_Transactions__c
						,GETDATE() AS rC_Giving__Rollup_Giving__c
					--payment method
						,T.[rC_Giving__Payment_Type__c] AS rC_Giving__Payment_Method__c
/*EXT_ID*/				,T.[PAYMENT_METHOD_External_ID__c] AS [rC_Giving__Payment_Method_Selected__r:External_ID__c]
 					--parent 
/*EXT_ID*/				,T.PARENT_PROPOSAL_External_Id__c AS [rC_Giving__Parent__r:External_ID__c]
					--payment dates
						,T.GFInsStartDate	AS	rC_Giving__First_Payment_Date__c
						,T.GFInsEndDate	AS	rC_Giving__Giving_End_Date__c
						,T.GFInsEndDate	AS	rC_Giving__Payment_End_Date__c
						,CASE WHEN T.GFInsYears = 0 OR T.GFInsYears IS NULL THEN '1' ELSE T.GFInsYears END AS rC_Giving__Giving_Years__c
					--RE gift_v
						,T.RE_Legacy_Financial_Code__c
						,T.BATCH_NUMBER AS RE_Batch_Number__c
						,CASE WHEN T.GFAck='Acknowledged' THEN 'TRUE' ELSE 'FALSE' END	AS rC_Giving__Acknowledged__c
						,T.GFAckDate	AS	rC_Giving__Acknowledged_Date__c
						,T.GFAnon	AS	rC_Giving__Is_Anonymous__c
						,T.GFCheckDate	AS	rC_Giving__Check_Date__c
						,T.GFCheckNum	AS	rC_Giving__Check_Number__c
						,T.GFRef	AS	[Description]
						,T.GFStkIss	AS	Stock_Issuer__c
						,T.GFStkMedPrice	AS	Stock_Median_Price__c
						,T.GFStkNumUnits	AS	rC_Giving__Number_Of_Shares__c
						,T.GFStkSymbol	AS	rC_Giving__Ticker_Symbol__c
 						,T.DateAdded	AS	CreatedDate
						,T.GFConsDesc AS RE_Gift_Constituency_Code__c
						,T.GFLtrCode AS RE_Letter_Code__c
						,T.GFPayMeth AS RE_GFPayMeth__c
  						,T.GFPledgeBalance AS RE_Pledge_Balance__c
						,T.[GFType] AS RE_Gift_Type__c
						,T.RE_Gift_SubType__c
						,CASE WHEN T.[GFType] ='Pledge' OR T.[GFType]='MG Pledge' THEN 'TRUE' ELSE 'FALSE' END AS Pledge__c
   				--attributes (chart-attributes)
				 		,T.DAF_Pledge_Payment__c
						,T.Donation_Page_Name__c
						,T.Donor_Recognition_Name__c
						,T.Gift_Comments__c
						,T.NCHX_Source_Entity__c
						,T.Non_Donation_Payment__c
						,T.Original_Pledge_Amount__c
						,T.Plaque_Wording__c
						,T.Research_Payment__c
						,T.Transaction_ID__c
						,T.Tribute_In_Honor_of__c
						,T.Tribute_In_Memory_of__c
					--gift attribute (chart_giftattribute)
						,T2.[# Hours Per Period] AS Hours_Per_Period__c
						,T2.[Anticipated Payment Type] AS Anticipated_Payment_Type__c	
						,T2.[Considering bequest in] AS Considering_bequest_in__c
						,T2.[Donor Designation-Fund] AS Donor_Designation_Fund__c
						,T2.[Exceed Appeal] AS Exceed_Appeal__c
						,T2.[Exceed Batch Number] AS Exceed_Batch_Number__c
						,T2.[Exceed Campaign] AS Exceed_Campaign__c
						,T2.[Former Appeal] AS Former_Appeal__c
						,T2.[Former Campaign] AS Former_Campaign__c
						,T2.[Former ConCode] AS Former_ConCode__c
						,T2.[Former Fund] AS Former_Fund__c
						,T2.[Former Package] AS Former_Package__c
						,T2.[Gift-in-Kind] AS Gift_in_kind__c
						,T2.[Hour Club] AS Hour_Club__c
						,T2.[Irregular] AS Irregular__c
						,T2.[Lights of Love FY14 RSVP] AS Lights_of_Love_FY14_RSVP__c
						,T2.[Physician Engagement] AS Physician_Engagement__c
						,CASE WHEN T.Production__c IS NULL THEN T2.[Production] ELSE T.Production__c END AS Production__c
						,T2.[Recogntion] AS Recognition__c
						,T2.[Report Exclusion] AS Report_Exclusion__c
						,T2.[Selected Naming Opportunity] AS Selected_Naming_Opporunity__c
						,T2.[Sobrato Challenge?] AS Sobrato_Challenge__c
						,T2.[Source of notification] AS Source_of_notification__c
						,T2.[SphereGiftIdentifier] AS SphereGiftIdentifier__c
 			 		--MISC.
						,T.RE_Split_Gift__c
						,T.GiftSubType__c
						,T.GFPostDate__c  AS Post_Date__c    --renamed for 1P2T
						,T.GFPostStatus__c AS Posted_Status__c   --renamed for 1P2T
  						,T3.[Adjusted_Date__c]
					    ,T3.[Adjustment_Post_Date__c]
						,T3.[Adjustment_Post_Status__c]
						,'FALSE' AS Employee_Recurring_Gift__c	


 					--reference
						--,T1.ID zrefAccountId
						,T.ImportID  zref_ImportID
						,T.Parent_Giving_RecordType AS zrefParentRecordType
						,T.GFType AS zrefGFType
						,T.GFInsFreq AS zrefGFInsFreq
						,T.GFPledgeBalance AS zrefPledgeBal
						,T.GFStatus AS zrefRecurringStatus
	 				INTO SUTTER_1P_MIGRATION.TBL.OPPORTUNITY_PARENT   
					FROM SUTTER_1P_MIGRATION.TBL.GIFT T
 					LEFT JOIN SUTTER_1P_MIGRATION.TBL.Gift_Attribute_1 T2 ON T.GSplitGFImpID = T2.GFIMPID AND T.GSplitRE_DB_OwnerShort=T2.RE_DB_OwnerShort
					LEFT JOIN [SUTTER_1P_MIGRATION].TBL.GIFT_ADJUSTED_DATE T3 ON T.[refGFImpID]=T3.GFImpID AND T.[GSplitRE_DB_OwnerShort]=T3.RE_DB_OwnerShort
					LEFT JOIN [SUTTER_1P_MIGRATION].TBL.Gift_FirstPaymentDate AS T4 ON T.[refGFImpID]=T4.GFImpID AND T.[GSplitRE_DB_OwnerShort]=T4.RE_DB_OwnerShort
					LEFT JOIN [SUTTER_1P_MIGRATION].TBL.GiftAttr_employee_recurring T5 ON  T.GSplitGFImpID = T5.GFIMPID AND T.GSplitRE_DB_OwnerShort=T5.RE_DB_OwnerShort  --DNC gifts with Employee Recurring Attribute
					WHERE T.Parent_Giving_RecordType IS NOT NULL 
					AND (T5.GFImpID IS NULL AND T5.RE_DB_OwnerShort IS NULL)   -- DNC recurring from employee giving attribute 
					--2,214,512
			UNION ALL
				--PART 2: Includes only gifts with EMPLOYEE GIVING attribute to be migrated as 'one time' , i.e. parent/child oppts. 
 					SELECT  
					--IDs
						dbo.fnc_OwnerID() AS OwnerID         
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS External_Id__c
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS RE_GSplitImpID__c
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitGFImpID AS RE_GFImpID__c
/*EXT_ID*/				,T.[ACCOUNT:External_Id__c]
/*CLOSE DATE*/			,CASE WHEN T4.GFFirstPaymentDate IS NOT NULL 
							  THEN T4.GFFirstPaymentDate 
							  ELSE T.[GFDate] 
						 END AS CloseDate
						,T.[GFDate] AS Gift_Date__c
 						,CASE   WHEN (T.[GFType] ='Pledge' OR T.[GFType]='MG Pledge') 
								THEN T.[rC_Giving__Affiliation__c]+' - Pledge - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10)) 
								ELSE T.[rC_Giving__Affiliation__c]+' - Donation - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10))
						 END AS NAME
 						,dbo.fnc_RecordType('Opportunity_Donation') AS RecordTypeID
  					--CampaignId
/*EXT_ID*/				,T.CAMPAIGN_External_Id__c	AS [CAMPAIGN:External_Id__c]
						,CASE WHEN (T.GFType LIKE '%pledge%' AND  GFPledgeBalance>0) OR (T.GFType ='Recurring Gift' AND T.GFStatus='Active') 
							  THEN 'Open' ELSE 'Completed' END AS StageName
						,T.rC_Giving__Activity_Type__c
						,T.[rC_Giving__Affiliation__c]
						,T.[Fundraising_Program__c]
  			 		--amounts
						,CASE WHEN (T.GFType='Pledge' OR T.GFType='MG Pledge') THEN (T.GFTAmt/T.GFInsNumPay) ELSE T.GSplitAmt END AS rC_Giving__Giving_Amount__c
						,CASE WHEN T.GFType ='Gift-in-Kind' THEN NULL ELSE T.GSplitAmt END AS rC_Giving__Expected_Giving_Amount__c
						,CASE WHEN T.GFType ='Gift-in-Kind' THEN NULL ELSE T.rC_Giving__Current_Giving_Amount__c END AS rC_Giving__Current_Giving_Amount__c
/*InKind*/				,CASE WHEN T.GFType ='Gift-in-Kind' THEN T.GSplitAmt ELSE NULL END AS rC_Giving__Projected_Amount__c				
					--allocations
						,NULL AS  rC_Giving__Allocation_Amount__c   --Only populate with total gift amount when using Special Allocations.
/*EXT_ID*/				,T.GAU_External_Id__c	AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]
 					--frequencies
						,CASE WHEN (T.GFType='Pledge' OR T.GFType='MG Pledge' OR T.GFType='Recurring Gift') 
							THEN T.rC_Giving__Giving_Frequency__c ELSE 'One Payment' END AS rC_Giving__Giving_Frequency__c
						,CASE WHEN (T.GFType='Pledge' OR T.GFType='MG Pledge' OR T.GFType='Recurring Gift') 
							THEN T.rC_Giving__Giving_Frequency__c ELSE 'One Payment' END AS	rC_Giving__Payment_Frequency__c
 						,CASE WHEN (T.GFInsFreq='9') THEN T.GFInsNumPay ELSE NULL END AS	rC_Giving__Payment_Count__c
					--givingtypes					   
						,T.rC_Giving__Is_Giving__c
						,T.rC_Giving__Is_Giving_Donation__c
						,T.rC_Giving__Is_Giving_Inkind__c
						,T.rC_Giving__Is_Sustainer__c
						,GETDATE() AS rC_Giving__Update_Transactions__c
						,GETDATE() AS rC_Giving__Rollup_Transactions__c
						,GETDATE() AS rC_Giving__Rollup_Giving__c
						--payment method
						,T.[rC_Giving__Payment_Type__c] AS rC_Giving__Payment_Method__c
/*EXT_ID*/				,T.[PAYMENT_METHOD_External_ID__c] AS [rC_Giving__Payment_Method_Selected__r:External_ID__c]
 					--parent 
/*EXT_ID*/				,T.PARENT_PROPOSAL_External_Id__c AS [rC_Giving__Parent__r:External_ID__c]
					--payment dates
						,T.GFInsStartDate	AS	rC_Giving__First_Payment_Date__c
						,T.GFInsEndDate	AS	rC_Giving__Giving_End_Date__c
						,T.GFInsEndDate	AS	rC_Giving__Payment_End_Date__c
						,CASE WHEN T.GFInsYears = 0 OR T.GFInsYears IS NULL THEN '1' ELSE T.GFInsYears END AS rC_Giving__Giving_Years__c
					--RE gift_v
						,T.RE_Legacy_Financial_Code__c
						,T.BATCH_NUMBER AS RE_Batch_Number__c
						,CASE WHEN T.GFAck='Acknowledged' THEN 'TRUE' ELSE 'FALSE' END	AS rC_Giving__Acknowledged__c
						,T.GFAckDate	AS	rC_Giving__Acknowledged_Date__c
						,T.GFAnon	AS	rC_Giving__Is_Anonymous__c
						,T.GFCheckDate	AS	rC_Giving__Check_Date__c
						,T.GFCheckNum	AS	rC_Giving__Check_Number__c
						,T.GFRef	AS	[Description]
						,T.GFStkIss	AS	Stock_Issuer__c
						,T.GFStkMedPrice	AS	Stock_Median_Price__c
						,T.GFStkNumUnits	AS	rC_Giving__Number_Of_Shares__c
						,T.GFStkSymbol	AS	rC_Giving__Ticker_Symbol__c
						,T.DateAdded	AS	CreatedDate
						,T.GFConsDesc AS RE_Gift_Constituency_Code__c
						,T.GFLtrCode AS RE_Letter_Code__c
						,T.GFPayMeth AS RE_GFPayMeth__c
 						,T.GFPledgeBalance AS RE_Pledge_Balance__c
						,T.[GFType] AS RE_Gift_Type__c
						,T.RE_Gift_SubType__c
						,CASE WHEN T.[GFType] ='Pledge' OR T.[GFType]='MG Pledge' THEN 'TRUE' ELSE 'FALSE' END AS Pledge__c
 					--attributes (chart-attributes)
						,T.DAF_Pledge_Payment__c
						,T.Donation_Page_Name__c
						,T.Donor_Recognition_Name__c
						,T.Gift_Comments__c
						,T.NCHX_Source_Entity__c
						,T.Non_Donation_Payment__c
						,T.Original_Pledge_Amount__c
						,T.Plaque_Wording__c
						,T.Research_Payment__c
						,T.Transaction_ID__c
						,T.Tribute_In_Honor_of__c
						,T.Tribute_In_Memory_of__c
					--gift attribute (chart_giftattribute)
						,T2.[# Hours Per Period] AS Hours_Per_Period__c
						,T2.[Anticipated Payment Type] AS Anticipated_Payment_Type__c	
						,T2.[Considering bequest in] AS Considering_bequest_in__c
						,T2.[Donor Designation-Fund] AS Donor_Designation_Fund__c
						,T2.[Exceed Appeal] AS Exceed_Appeal__c
						,T2.[Exceed Batch Number] AS Exceed_Batch_Number__c
						,T2.[Exceed Campaign] AS Exceed_Campaign__c
						,T2.[Former Appeal] AS Former_Appeal__c
						,T2.[Former Campaign] AS Former_Campaign__c
						,T2.[Former ConCode] AS Former_ConCode__c
						,T2.[Former Fund] AS Former_Fund__c
						,T2.[Former Package] AS Former_Package__c
						,T2.[Gift-in-Kind] AS Gift_in_kind__c
						,T2.[Hour Club] AS Hour_Club__c
						,T2.[Irregular] AS Irregular__c
						,T2.[Lights of Love FY14 RSVP] AS Lights_of_Love_FY14_RSVP__c
						,T2.[Physician Engagement] AS Physician_Engagement__c
						,CASE WHEN T.Production__c IS NULL THEN T2.[Production] ELSE T.Production__c END AS Production__c
						,T2.[Recogntion] AS Recognition__c
						,T2.[Report Exclusion] AS Report_Exclusion__c
						,T2.[Selected Naming Opportunity] AS Selected_Naming_Opporunity__c
						,T2.[Sobrato Challenge?] AS Sobrato_Challenge__c
						,T2.[Source of notification] AS Source_of_notification__c
						,T2.[SphereGiftIdentifier] AS SphereGiftIdentifier__c			
 				 	--MISC.
						,T.RE_Split_Gift__c
						,T.GiftSubType__c
						,T.GFPostDate__c  AS Post_Date__c    --renamed for 1P2T
						,T.GFPostStatus__c AS Posted_Status__c   --renamed for 1P2T
 						,T3.[Adjusted_Date__c]
					    ,T3.[Adjustment_Post_Date__c]
						,T3.[Adjustment_Post_Status__c]
				 		,'TRUE' AS Employee_Recurring_Gift__c	--GFAttrDEsc= true	
					--reference
						--,T1.ID zrefAccountId
						,T.ImportID  zref_ImportID
						,T.Parent_Giving_RecordType AS zrefParentRecordType
						,T.GFType AS zrefGFType
						,T.GFInsFreq AS zrefGFInsFreq
						,T.GFPledgeBalance AS zrefPledgeBal
						,T.GFStatus AS zrefRecurringStatus
					FROM [SUTTER_1P_MIGRATION].TBL.GIFT T
					INNER JOIN [SUTTER_1P_MIGRATION].TBL.GiftAttr_employee_recurring T5 ON  T.GSplitGFImpID = T5.GFIMPID AND T.GSplitRE_DB_OwnerShort=T5.RE_DB_OwnerShort  --CREATE "EMPLOYEE GIVING GIFTS" FROM ATTRIBUTES. 
				 	LEFT JOIN [SUTTER_1P_MIGRATION].TBL.Gift_Attribute_1 T2 ON T.GSplitGFImpID = T2.GFIMPID AND T.GSplitRE_DB_OwnerShort=T2.RE_DB_OwnerShort
					LEFT JOIN [SUTTER_1P_MIGRATION].TBL.GIFT_ADJUSTED_DATE T3 ON T.[refGFImpID]=T3.GFImpID AND T.[GSplitRE_DB_OwnerShort]=T3.RE_DB_OwnerShort
					LEFT JOIN [SUTTER_1P_DELTA].TBL.Gift_FirstPaymentDate AS T4 ON T.[refGFImpID]=T4.GFImpID AND T.[GSplitRE_DB_OwnerShort]=T4.RE_DB_OwnerShort
					WHERE ( T5.[GFType]='Cash'
						    OR T5.[GFType]='Other'
			 				OR T5.[GFType]='Recurring Gift Pay-Cash'  )	   					 
END 	--2,512,731	 
 
BEGIN--check duplicates

		 /*	SELECT GFType, Parent_Giving_RecordType, COUNT(*) c 
					FROM SUTTER_1P_MIGRATION.TBL.GIFT
					GROUP BY GFType, Parent_Giving_RecordType
 		 */  
 				
				DROP TABLE SUTTER_1P_MIGRATION.TBL.z_OPPORTUNITY_PARENT_dupe
 				
				SELECT * 
 		 		INTO SUTTER_1P_MIGRATION.TBL.z_OPPORTUNITY_PARENT_dupe
				FROM SUTTER_1P_MIGRATION.TBL.OPPORTUNITY_PARENT
 				WHERE External_Id__c IN (SELECT External_Id__c FROM SUTTER_1P_MIGRATION.TBL.OPPORTUNITY_PARENT GROUP BY External_Id__c HAVING COUNT(*)>1)
				ORDER BY [External_ID__c], [ACCOUNT:External_ID__c]

				SELECT * FROM SUTTER_1P_MIGRATION.TBL.z_OPPORTUNITY_PARENT_dupe
 
END
 
 
BEGIN--REMOVE DOUBLE QUOTES					
	
	USE [SUTTER_1P_MIGRATION] 
 	EXEC sp_FindStringInTable '%"%', 'TBL', 'OPPORTUNITY_PARENT'  		
  
	UPDATE [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT] SET [Description]=REPLACE([Description],'"','''') where [Description] like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT] SET Stock_Issuer__c=REPLACE(Stock_Issuer__c,'"','''') where Stock_Issuer__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT] SET Stock_Median_Price__c=REPLACE(Stock_Median_Price__c,'"','''') where Stock_Median_Price__c like '%"%'
  	UPDATE [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT] SET [RE_Gift_Constituency_Code__c]=REPLACE([RE_Gift_Constituency_Code__c],'"','''') where [RE_Gift_Constituency_Code__c] like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT] SET Tribute_in_memory_of__c=REPLACE(Tribute_in_memory_of__c,'"','''') where Tribute_in_memory_of__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT] SET Transaction_id__c=REPLACE(Transaction_id__c,'"','''') where Transaction_id__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT] SET Tribute_in_honor_of__c=REPLACE(Tribute_in_honor_of__c,'"','''') where Tribute_in_honor_of__c like '%"%'
 	UPDATE [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT] SET RE_Letter_Code__c=REPLACE(RE_Letter_Code__c,'"','''') where RE_Letter_Code__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT] SET Donation_page_name__c=REPLACE(Donation_page_name__c,'"','''') where Donation_page_name__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT] SET Gift_comments__c=REPLACE(Gift_comments__c,'"','''') where Gift_comments__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT] SET Donor_recognition_name__c=REPLACE(Donor_recognition_name__c,'"','''') where Donor_recognition_name__c like '%"%'

    SELECT * FROM SUTTER_1P_MIGRATION.[TBL].OPPORTUNITY_PARENT WHERE zrefGFType = 'Pledge'
	SELECT COUNT(*) FROM [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT] 

	SELECT * FROM  SUTTER_1P_MIGRATION.TBL.GIFT 
	WHERE GSplitGFImpID='02869-553-0000433218'
	
	SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].[OPPORTUNITY_PARENT]
	WHERE [External_Id__c]='absf-gt-02869-553-0000433218'	   --extid is the gsplitimpid
    	 

END
 
BEGIN-- name/campaign null 
		SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_PARENT WHERE Name IS NULL OR External_Id__c IS NULL
		SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_PARENT WHERE [Campaign:External_Id__c] IS NULL  
		SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_PARENT WHERE RecordTypeId IS null
		SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_PARENT WHERE [rC_Giving__Payment_Method_Selected__r:External_ID__c] IS null
		SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_PARENT WHERE [rC_Giving__GAU__r:rC_Giving__External_ID__c] IS NULL
END    

BEGIN--CREATE IMP tbl.
    	
		SELECT TOP 0 *
		INTO SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_PARENT]
		FROM SUTTER_1P_MIGRATION.TBL.[OPPORTUNITY_PARENT]	
	 
		ALTER TABLE SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_PARENT] 
		ADD zrefMyID int identity(1, 1) primary key;

        ALTER TABLE SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_PARENT] 
	    ADD  zrefNewExternal_Id__c NVARCHAR(30)    	
			
		TRUNCATE TABLE SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_PARENT] 
		
		INSERT  INTO [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PARENT]
				( [OwnerID] ,
				  [External_Id__c] ,
				  [RE_GSplitImpID__c] ,
				  [RE_GFImpID__c] ,
				  [ACCOUNT:External_Id__c] ,
				  [CloseDate] ,
				  [Gift_Date__c] ,
				  [NAME] ,
				  [RecordTypeID] ,
				  [CAMPAIGN:External_Id__c] ,
				  [StageName] ,
				  [rC_Giving__Activity_Type__c] ,
				  [rC_Giving__Affiliation__c] ,
				  [Fundraising_Program__c] ,
				  [rC_Giving__Giving_Amount__c] ,
				  [rC_Giving__Expected_Giving_Amount__c] ,
				  [rC_Giving__Current_Giving_Amount__c] ,
				  [rC_Giving__Projected_Amount__c] ,
				  [rC_Giving__Allocation_Amount__c] ,
				  [rC_Giving__GAU__r:rC_Giving__External_ID__c] ,
				  [rC_Giving__Giving_Frequency__c] ,
				  [rC_Giving__Payment_Frequency__c] ,
				  [rC_Giving__Payment_Count__c] ,
				  [rC_Giving__Is_Giving__c] ,
				  [rC_Giving__Is_Giving_Donation__c] ,
				  [rC_Giving__Is_Giving_Inkind__c] ,
				  [rC_Giving__Is_Sustainer__c] ,
				  [rC_Giving__Update_Transactions__c] ,
				  [rC_Giving__Rollup_Transactions__c] ,
				  [rC_Giving__Rollup_Giving__c] ,
				  [rC_Giving__Payment_Method__c] ,
				  [rC_Giving__Payment_Method_Selected__r:External_ID__c] ,
				  [rC_Giving__Parent__r:External_ID__c] ,
				  [rC_Giving__First_Payment_Date__c] ,
				  [rC_Giving__Giving_End_Date__c] ,
				  [rC_Giving__Payment_End_Date__c] ,
				  [rC_Giving__Giving_Years__c] ,
				  [RE_Legacy_Financial_Code__c] ,
				  [RE_Batch_Number__c] ,
				  [rC_Giving__Acknowledged__c] ,
				  [rC_Giving__Acknowledged_Date__c] ,
				  [rC_Giving__Is_Anonymous__c] ,
				  [rC_Giving__Check_Date__c] ,
				  [rC_Giving__Check_Number__c] ,
				  [Description] ,
				  [Stock_Issuer__c] ,
				  [Stock_Median_Price__c] ,
				  [rC_Giving__Number_Of_Shares__c] ,
				  [rC_Giving__Ticker_Symbol__c] ,
				  [CreatedDate] ,
				  [RE_Gift_Constituency_Code__c] ,
				  [RE_Letter_Code__c] ,
				  [RE_GFPayMeth__c] ,
				  [RE_Pledge_Balance__c] ,
				  [RE_Gift_Type__c] ,
				  [RE_Gift_SubType__c] ,
				  [Pledge__c] ,
				  [DAF_Pledge_Payment__c] ,
				  [Donation_Page_Name__c] ,
				  [Donor_Recognition_Name__c] ,
				  [Gift_Comments__c] ,
				  [NCHX_Source_Entity__c] ,
				  [Non_Donation_Payment__c] ,
				  [Original_Pledge_Amount__c] ,
				  [Plaque_Wording__c] ,
				  [Research_Payment__c] ,
				  [Transaction_ID__c] ,
				  [Tribute_In_Honor_of__c] ,
				  [Tribute_In_Memory_of__c] ,
				  [Hours_Per_Period__c] ,
				  [Anticipated_Payment_Type__c] ,
				  [Considering_bequest_in__c] ,
				  [Donor_Designation_Fund__c] ,
				  [Exceed_Appeal__c] ,
				  [Exceed_Batch_Number__c] ,
				  [Exceed_Campaign__c] ,
				  [Former_Appeal__c] ,
				  [Former_Campaign__c] ,
				  [Former_ConCode__c] ,
				  [Former_Fund__c] ,
				  [Former_Package__c] ,
				  [Gift_in_kind__c] ,
				  [Hour_Club__c] ,
				  [Irregular__c] ,
				  [Lights_of_Love_FY14_RSVP__c] ,
				  [Physician_Engagement__c] ,
				  [Production__c] ,
				  [Recognition__c] ,
				  [Report_Exclusion__c] ,
				  [Selected_Naming_Opporunity__c] ,
				  [Sobrato_Challenge__c] ,
				  [Source_of_notification__c] ,
				  [SphereGiftIdentifier__c] ,
				  [RE_Split_Gift__c] ,
				  [GiftSubType__c] ,
				  [Post_Date__c] ,
				  [Posted_Status__c] ,
				  [Adjusted_Date__c] ,
				  [Adjustment_Post_Date__c] ,
				  [Adjustment_Post_Status__c] ,	   
				  [Employee_Recurring_Gift__c] ,
				  [zref_ImportID] ,
				  [zrefParentRecordType] ,
				  [zrefGFType] ,
				  [zrefGFInsFreq] ,
				  [zrefPledgeBal] ,
				  [zrefRecurringStatus]	,
				  [zrefNewExternal_Id__c]
				)
				SELECT
					[OwnerID] ,
					[External_Id__c] ,
					[RE_GSplitImpID__c] ,
					[RE_GFImpID__c] ,
					[ACCOUNT:External_Id__c] ,
					[CloseDate] ,
					[Gift_Date__c] ,
					[NAME] ,
					[RecordTypeID] ,
					[CAMPAIGN:External_Id__c] ,
					[StageName] ,
					[rC_Giving__Activity_Type__c] ,
					[rC_Giving__Affiliation__c] ,
					[Fundraising_Program__c] ,
					[rC_Giving__Giving_Amount__c] ,
					[rC_Giving__Expected_Giving_Amount__c] ,
					[rC_Giving__Current_Giving_Amount__c] ,
					[rC_Giving__Projected_Amount__c] ,
					[rC_Giving__Allocation_Amount__c] ,
					[rC_Giving__GAU__r:rC_Giving__External_ID__c] ,
					[rC_Giving__Giving_Frequency__c] ,
					[rC_Giving__Payment_Frequency__c] ,
					[rC_Giving__Payment_Count__c] ,
					[rC_Giving__Is_Giving__c] ,
					[rC_Giving__Is_Giving_Donation__c] ,
					[rC_Giving__Is_Giving_Inkind__c] ,
					[rC_Giving__Is_Sustainer__c] ,
					[rC_Giving__Update_Transactions__c] ,
					[rC_Giving__Rollup_Transactions__c] ,
					[rC_Giving__Rollup_Giving__c] ,
					[rC_Giving__Payment_Method__c] ,
					[rC_Giving__Payment_Method_Selected__r:External_ID__c] ,
					[rC_Giving__Parent__r:External_ID__c] ,
					[rC_Giving__First_Payment_Date__c] ,
					[rC_Giving__Giving_End_Date__c] ,
					[rC_Giving__Payment_End_Date__c] ,
					[rC_Giving__Giving_Years__c] ,
					[RE_Legacy_Financial_Code__c] ,
					[RE_Batch_Number__c] ,
					[rC_Giving__Acknowledged__c] ,
					[rC_Giving__Acknowledged_Date__c] ,
					[rC_Giving__Is_Anonymous__c] ,
					[rC_Giving__Check_Date__c] ,
					[rC_Giving__Check_Number__c] ,
					[Description] ,
					[Stock_Issuer__c] ,
					[Stock_Median_Price__c] ,
					[rC_Giving__Number_Of_Shares__c] ,
					[rC_Giving__Ticker_Symbol__c] ,
					[CreatedDate] ,
					[RE_Gift_Constituency_Code__c] ,
					[RE_Letter_Code__c] ,
					[RE_GFPayMeth__c] ,
					[RE_Pledge_Balance__c] ,
					[RE_Gift_Type__c] ,
					[RE_Gift_SubType__c] ,
					[Pledge__c] ,
					[DAF_Pledge_Payment__c] ,
					[Donation_Page_Name__c] ,
					[Donor_Recognition_Name__c] ,
					[Gift_Comments__c] ,
					[NCHX_Source_Entity__c] ,
					[Non_Donation_Payment__c] ,
					[Original_Pledge_Amount__c] ,
					[Plaque_Wording__c] ,
					[Research_Payment__c] ,
					[Transaction_ID__c] ,
					[Tribute_In_Honor_of__c] ,
					[Tribute_In_Memory_of__c] ,
					[Hours_Per_Period__c] ,
					[Anticipated_Payment_Type__c] ,
					[Considering_bequest_in__c] ,
					[Donor_Designation_Fund__c] ,
					[Exceed_Appeal__c] ,
					[Exceed_Batch_Number__c] ,
					[Exceed_Campaign__c] ,
					[Former_Appeal__c] ,
					[Former_Campaign__c] ,
					[Former_ConCode__c] ,
					[Former_Fund__c] ,
					[Former_Package__c] ,
					[Gift_in_kind__c] ,
					[Hour_Club__c] ,
					[Irregular__c] ,
					[Lights_of_Love_FY14_RSVP__c] ,
					[Physician_Engagement__c] ,
					[Production__c] ,
					[Recognition__c] ,
					[Report_Exclusion__c] ,
					[Selected_Naming_Opporunity__c] ,
					[Sobrato_Challenge__c] ,
					[Source_of_notification__c] ,
					[SphereGiftIdentifier__c] ,
					[RE_Split_Gift__c] ,
					[GiftSubType__c] ,
					[Post_Date__c] ,
					[Posted_Status__c] ,
					[Adjusted_Date__c] ,
					[Adjustment_Post_Date__c] ,
					[Adjustment_Post_Status__c] ,
					[Employee_Recurring_Gift__c] ,
					[zref_ImportID] ,
					[zrefParentRecordType] ,
					[zrefGFType] ,
					[zrefGFInsFreq] ,
					[zrefPledgeBal] ,
					[zrefRecurringStatus],
					CASE WHEN (ROW_NUMBER() OVER ( PARTITION BY EXTERNAL_ID__C ORDER BY EXTERNAL_ID__C)-1)>0 
					THEN EXTERNAL_ID__C +'-'+ CAST((ROW_NUMBER() OVER ( PARTITION BY EXTERNAL_ID__C ORDER BY EXTERNAL_ID__C)-1)AS VARCHAR(2)) 
					ELSE EXTERNAL_ID__C 
					END AS [zrefNewExternal_Id__c]
				FROM
					[SUTTER_1P_MIGRATION].TBL.OPPORTUNITY_PARENT
				ORDER BY
					[ACCOUNT:External_ID__c] ,
					[CAMPAIGN:External_Id__c] ,
					CloseDate;
			-- 

		--add suffix to address duplicates.   added above
			/*SELECT  T1.*,
				CASE WHEN (ROW_NUMBER() OVER ( PARTITION BY T1.EXTERNAL_ID__C ORDER BY T1.EXTERNAL_ID__C)-1)>0 
				THEN T1.EXTERNAL_ID__C +'-'+ CAST((ROW_NUMBER() OVER ( PARTITION BY T1.EXTERNAL_ID__C ORDER BY T1.EXTERNAL_ID__C)-1)AS VARCHAR(2)) 
				ELSE T1.EXTERNAL_ID__C 
				END AS [zrefNewExternal_Id__c]
			INTO SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_PARENT]
			FROM SUTTER_1P_MIGRATION.TBL.[OPPORTUNITY_PARENT] T1
			ORDER BY [ACCOUNT:External_ID__c], [CAMPAIGN:External_Id__c], CloseDate
			*/ 

		--test duplicates 
			SELECT [External_ID__c], [zrefNewExternal_Id__c], len ([External_ID__c]) le, LEN([zrefNewExternal_Id__c]) lne
			FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_PARENT]
			WHERE [External_ID__c] IN (SELECT [External_ID__c] FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_PARENT] GROUP BY [External_ID__c] HAVING COUNT(*)>1)
 	 		ORDER BY   [External_ID__c], [zrefNewExternal_Id__c]
		--update ExternalId/GSplitImpId.
  			UPDATE SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_PARENT]
			SET [External_ID__c]= [zrefNewExternal_Id__c], [RE_GSplitImpID__c]= [zrefNewExternal_Id__c]
			WHERE [External_ID__c] IN (SELECT [External_ID__c] FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_PARENT] GROUP BY [External_ID__c] HAVING COUNT(*)>1)
 		 
		--check update ExternalId/GSplitImpId.	
		SELECT [External_ID__c], [zrefNewExternal_Id__c], len ([External_ID__c]) le, LEN([zrefNewExternal_Id__c]) lne
		FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_PARENT]
		WHERE ([External_ID__c] IN (SELECT [External_ID__c] FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_PARENT] GROUP BY [External_ID__c] HAVING COUNT(*)>1))
		OR ([RE_GSplitImpID__c] IN (SELECT [RE_GSplitImpID__c] FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_PARENT] GROUP BY [RE_GSplitImpID__c] HAVING COUNT(*)>1))
 	 	ORDER BY   [External_ID__c], [zrefNewExternal_Id__c]
		 
END  
	   	 
	   SELECT COUNT(*) FROM SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT]  --2512731
	  
	   DROP TABLE SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT_1of3]
	   DROP TABLE SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT_2of3]
	   DROP TABLE SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT_3of3]

	   SELECT * 
	   INTO SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT_1of3]		  -- DROP TABLE SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT_1of3]
	   FROM SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT]
	   WHERE zrefmyId <1000001	
	   GO   --	SELECT zrefmyId FROM SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT_1of3] 	 ORDER BY zrefmyId

	   SELECT * 
	   INTO SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT_2of3]
	   FROM SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT]
	   WHERE zrefmyId >1000000 AND zrefmyId <2000001
	   GO   --	SELECT zrefmyId FROM SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT_2of3] 	 ORDER BY zrefmyId

	   SELECT * 
	   INTO SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT_3of3]
	   FROM SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT]
	   WHERE zrefmyId >2000000		
	   GO   --	SELECT zrefmyId FROM SUTTER_1P_DELTA.IMP.[OPPORTUNITY_PARENT_3of3] 	 ORDER BY zrefmyId

	

BEGIN--exceptions
	--delete tbl
	DROP TABLE SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT_XCP

	--create tbl
	SELECT T.* 
 	INTO SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT_XCP
	FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT T
	LEFT JOIN SUTTER_1P_MIGRATION.XTR.OPPORTUNITY X ON T.External_Id__c=X.External_Id__c
	WHERE X.External_Id__c IS NULL OR X.EXTERNAL_id__c=''
	ORDER BY T.[ACCOUNT:External_Id__c], [T].[rC_Giving__Parent__r:External_ID__c], [T].[CloseDate] ASC

	--review tbl  
	SELECT * FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT_XCP 

END; 


