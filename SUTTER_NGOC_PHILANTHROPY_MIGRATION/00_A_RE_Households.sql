	
	/*
		USE SUTTER_MGEA
		GO
		CREATE SCHEMA TBL
		GO
		CREATE SCHEMA IMP
		GO
		CREATE SCHEMA XTR
		GO
		
		
	*/
	
	USE SUTTER_DATA
	GO

	/*
		DROP TABLE SUTTER_MGEA.tbl.Account_Org
		GO
		DROP TABLE SUTTER_MGEA.tbl.Contact_HofH
		GO
		DROP TABLE SUTTER_MGEA.tbl.Contact_NoHofH
		GO
	*/
	
	--A_Organization Accounts
			--A_tbl_Account_Org:  Constituents that are Organization Constituents;
				SELECT T.KeyInd, T.ConsID, T.ImportID, T.OrgName, T.RE_DB_OwnerShort 
				INTO SUTTER_MGEA.tbl.Account_Org
				FROM SUTTER_DATA.dbo.HC_Constituents_v T
				WHERE T.KeyInd='O'
				GO
				--7,994		

	--_B_C_D Head of Household ACcounts/Contacts
	
			-- DROP TABLE SUTTER_MGEA.tbl.Contact_HofH
			
			--B_tbl_Contact_Cons_HofH:  Constituents that are Head of Household Constituents only;
				SELECT T.RE_DB_OwnerShort, T.KeyInd, T.ImportID,  T.ConsID, T.Deceased,  -- hh
				T3.ImportID AS NonHHImpID, T3.ConsID AS NonHHConsID, T3.Deceased NonHHDeceased,		--non-hh
				T2.IRLink, T2.IRIsHH, T2.IRIsSpouse, T2.IRImpID, T2.RE_DB_OwnerShort AS IR_RE_DB_OwnerShort,
				CAST('' AS nvarchar(20)) AS HH_ImportID, 
				CAST('' AS nvarchar(20)) AS HH_ConsID, 
				CAST('' AS nvarchar(20)) AS NoHH_ImportID, 
				CAST('' AS nvarchar(20)) AS NoHH_ConsID
				
				INTO SUTTER_MGEA.tbl.Contact_HofH
				FROM SUTTER_DATA.dbo.HC_Constituents_v T
				INNER JOIN SUTTER_DATA.dbo.HC_Ind_Relat_v T2 ON T.ImportID = T2.IRLink
				INNER JOIN SUTTER_DATA.dbo.HC_Constituents_v T3 ON T2.ImportID=T3.ImportID
				WHERE (((T.KeyInd)='I') AND ((T2.IRIsHH)='TRUE') AND ((T2.IRIsSpouse)='TRUE'))   
				ORDER BY T.ImportID
				GO
				--832

			--C_tbl_Contact_Cons_NonConsSpouse; Constituents with non-cons Ind Relat Spouse
				INSERT INTO SUTTER_MGEA.tbl.Contact_HofH (RE_DB_OwnerShort, KeyInd, ImportID, ConsID, Deceased, NonHHImpID, NonHHConsID, NonHHDeceased, 
														  IRLink, IRIsHH, IRIsSpouse, IRImpID, IR_RE_DB_OwnerShort, 
														  HH_ImportID, HH_ConsID, NoHH_ImportID, NoHH_ConsID)
				SELECT T.RE_DB_OwnerShort, T.KeyInd,  T.ImportID, T.ConsID, T.Deceased, 
				T2.ImportID AS IRNonHHImpID, T2.ConsID NonHHConsID,  T2.IRDeceased AS NonHHDeceased, 
				'' AS IRLink, T2.IRIsHH, T2.IRIsSpouse, T2.IRImpID, T2.RE_DB_OwnerShort AS IR_RE_DB_OwnerShort,
				NULL AS HH_ImportID, null AS HH_ConsID, null AS NoHH_ImportID, null AS NoHH_ConsID
				FROM SUTTER_DATA.dbo.HC_Constituents_v T
				LEFT JOIN SUTTER_DATA.dbo.HC_Ind_Relat_v T2 ON T.ImportID = T2.ImportID
				LEFT JOIN SUTTER_DATA.dbo.HC_Constituents_v T3 ON T2.ImportID=T3.ImportID
				GROUP BY T.KeyInd, T2.IRIsHH, T.ImportID,  T.RE_DB_OwnerShort, T.ConsID,  T.Deceased, T2.ImportID, T2.ConsID, T2.IRDeceased, T2.IRLink, T2.IRIsSpouse, T2.IRImpID, T2.RE_DB_OwnerShort
				HAVING (((T.KeyInd)='I') AND ((T2.IRLink) Is Null Or (T2.IRLink)='') AND ((T2.IRIsSpouse)='TRUE'))
				ORDER BY T.ImportID
				GO
				--27,416
			
			--D_tbl_Contact_Cons_WNoSpouse: all other non-cons Ind Relat; Ind Constituents with no HofH constituents/relationships;  Ind Constituents with no non-HofH constituents/relationships; 
				INSERT INTO SUTTER_MGEA.tbl.Contact_HofH (RE_DB_OwnerShort, KeyInd, ImportID, ConsID, Deceased, NonHHImpID, NonHHConsID, 
				NonHHDeceased, IRLink, IRIsHH, IRIsSpouse, IRImpID, IR_RE_DB_OwnerShort,
				HH_ImportID, HH_ConsID, NoHH_ImportID, NoHH_ConsID)
				SELECT T.RE_DB_OwnerShort, T.KeyInd, T.ImportID, T.ConsID, T.Deceased,
				T3.NonHHImpID, T3.NonHHConsID, '' AS NonHHDeceased, '' AS IRLink, 'TRUE' AS IRIsHH, 
				'' AS IRIsSpouse, '' AS IRImpID, T3.RE_DB_OwnerShort AS IR_RE_DB_OwnerShort,
				NULL AS HH_ImportID, null AS HH_ConsID, null AS NoHH_ImportID, null AS NoHH_ConsID
				FROM (SUTTER_DATA.dbo.HC_Constituents_v T
				LEFT JOIN SUTTER_MGEA.tbl.Contact_HofH AS T2 ON T.ImportID = T2.ImportID) 
				LEFT JOIN SUTTER_MGEA.tbl.Contact_HofH AS T3 ON T.ImportID = T3.NonHHImpID
				GROUP BY T.KeyInd, T.ImportID, T.RE_DB_OwnerShort, T.ConsID, T.Deceased, T3.NonHHImpID, T3.NonHHConsID, T2.ImportID, T3.RE_DB_OwnerShort
				HAVING (((T.KeyInd)='I') AND ((T3.NonHHImpID) Is Null) AND ((T2.ImportID) Is Null))
				GO
				--80,332

	--_E_Non Head of Household constituents. 	
	/* REPLACED BY UPDATES ON THE HH TABLE.  MAY ONLY USE HH TABLE OR CREATE A NEW SUB TABLE FROM HH AND CALL IT THE SAEM AS THE NOHH 
			-- DROP TABLE SUTTER_MGEA.tbl.Contact_NoHofH	
			--E_tbl_Contact_Cons_NonHofH; Constituents NON-head of Household (to be combined into Head of Household Account. 
				SELECT T2.IRIsHH, T.KeyInd, T.ConsID, T.ImportID, T.RE_DB_OwnerShort, T.Deceased, T2.IRLink, T2.IRImpID, T2.RRImpID, T2.IRISSpouse, T2.RE_DB_OwnerShort AS IR_RE_DB_OwnerShort, 
				T3.ConsID AS HH_ConsID, T3.ImportID AS HH_ImportID , T3.Deceased HH_Deceased, T3.RE_DB_OwnerShort AS HH_RE_DB_OwnerShort
				INTO SUTTER_MGEA.tbl.Contact_NoHofH
				FROM (SUTTER_DATA.dbo.HC_Constituents_v AS T
				INNER JOIN SUTTER_DATA.dbo.HC_Ind_Relat_v AS T2 ON T.ImportID = T2.ImportID) 
				INNER JOIN SUTTER_DATA.dbo.HC_Constituents_v AS T3 ON T2.IRLink = T3.ImportID
				GROUP BY T2.IRIsHH, T.KeyInd, T.ConsID, T.ImportID, T.RE_DB_OwnerShort, T.Deceased, T2.IRLink, T2.IRImpID, T2.RRImpID, T2.IRISSpouse, T2.RE_DB_OwnerShort, 
				T3.ConsID, T3.ImportID, T3.Deceased, T3.RE_DB_OwnerShort
				HAVING (((T2.IRIsHH)='TRUE') AND ((T.KeyInd)='I'))
				ORDER BY T.ConsID
				GO
				--832	 
	*/
	--UPDATE HofH constituent for Account, and Contact Primary and Secondary. 
				
			SELECT * FROM SUTTER_MGEA.tbl.Contact_HofH
			WHERE importid!=NonHHImpID 			 
				
			--HH update 	
			UPDATE SUTTER_MGEA.tbl.Contact_HofH
			SET HH_ImportID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN NonHHImpID ELSE ImportID END WHERE importid!=NonHHImpID 
			GO
			UPDATE SUTTER_MGEA.tbl.Contact_HofH
			SET HH_ConsId= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN NonHHConsID ELSE ConsId END WHERE importid!=NonHHImpID 
			GO
			UPDATE SUTTER_MGEA.tbl.Contact_HofH 
			SET HH_ImportID=ImportID WHERE HH_ImportID IS null
			GO
			UPDATE SUTTER_MGEA.tbl.Contact_HofH 
			SET HH_ConsId=ConsId WHERE HH_ConsID IS null
			GO
			--Non-HH update
			UPDATE SUTTER_MGEA.tbl.Contact_HofH
			SET NoHH_ImportID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN ImportID ELSE NonHHImpID END WHERE importid!=NonHHImpID 
			GO
			UPDATE SUTTER_MGEA.tbl.Contact_HofH
			SET NoHH_ConsID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN ConsID ELSE NonHHConsID END WHERE importid!=NonHHImpID 
			GO
	
	
				SELECT ImportID, ConsID, Deceased, NonHHImpID, NonHHConsID, NonHHDeceased, HH_ImportID, HH_ConsID, NoHH_ImportID, NoHH_ConsID 
				FROM SUTTER_MGEA.tbl.Contact_HofH  
				--WHERE Deceased='false' AND NonHHDeceased='false' AND NoHH_ImportID IS NOT NULL
				--WHERE NoHH_ImportID IS NOT null
				WHERE NonHHImpID ='03947-079-0000511' 
				
				
	/*
		(7994 row(s) affected)

		(832 row(s) affected)

		(27416 row(s) affected)

		(80332 row(s) affected)

		(832 row(s) affected)
	*/	  
		  
		   
	--total (A) Cons Org:		  7,994
	--total (B_C_D) Cons HofH : 108,580
		--B:    832
		--C: 27,416
		--D: 80,332
	--total (E) Cons NonHoH:        832
	-----------------------------------
	--total HC_Constituent_v:   117,406	    
	
	--check count: SELECT COUNT(*) FROM SUTTER_DATA.dbo.HC_Constituents_v -- 117,406

	--SELECT T.ImportID, T.KeyInd, dbo.AccRecordType(T.ImportID) RecordTypeID FROM SUTTER_DATA.dbo.HC_Constituents_v T 
	--ORDER BY t.KeyInd desc

			 
 --account active test
		SELECT Deceased, NonHHDeceased, 
		CASE WHEN Deceased='true' AND NonHHDeceased='true' THEN 'FALSE'  
		WHEN Deceased='true' AND NonHHDeceased=''  THEN 'FALSE'
		ELSE 'TRUE'

		END AS ACCOUNT_ACTIVE,
		
		CASE WHEN Deceased='true' THEN 'FALSE' ELSE 'TRUE' END AS PreferredContact,

		COUNT(*) c
		FROM SUTTER_MGEA.TBL.Contact_HofH
		GROUP BY Deceased, NonHHDeceased
		ORDER BY Deceased, NonHHDeceased 