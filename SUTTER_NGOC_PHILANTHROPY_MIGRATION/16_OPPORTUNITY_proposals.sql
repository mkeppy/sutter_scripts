USE [SUTTER_1P_MIGRATION]
GO

BEGIN
		DROP TABLE SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PROPOSAL
END

BEGIN--PROPOSALS
					SELECT  
					dbo.fnc_OwnerID() AS OwnerID         
					,T1.RE_DB_OwnerShort+'-'+T1.PRImpID	AS	RE_PRImpID__c
					,T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.PRImpID AS	External_Id__c
					,CASE WHEN T4.NoHH_ImportID IS NOT NULL THEN T4.HH_ImportID ELSE T0.NEW_TGT_IMPORT_ID END AS [ACCOUNT:External_ID__c]
					,CAST(CASE WHEN T1.PRDateAsk	IS NULL THEN T1.DateAdded ELSE T1.PRDateAsk END AS DATE) AS	CloseDate
					,T1.PRName AS Name
					,T1.PRPurpose AS Proposal_Purpose__c
					,T1.PRReason AS Proposal_Reason__c
					,dbo.fnc_RecordType('Opportunity_Proposal') AS RecordTypeID
					,'7013B000000LFAoQAO' AS CampaignID -- "Legacy Proposal Campaign" 
					,CASE WHEN (T5.Proposal_Status__c IS NULL AND (CASE WHEN T1.PRDateAsk IS NULL THEN T1.DateAdded ELSE T1.PRDateAsk END) <=GETDATE()) 
								THEN 'Completed' WHEN (T5.Proposal_Status__c IS NULL AND (CASE WHEN T1.PRDateAsk IS NULL THEN T1.DateAdded ELSE T1.PRDateAsk END) >GETDATE()) 
								THEN 'Open' ELSE T5.Proposal_Status__c END AS StageName
					,T1.PRAmtAsk	AS	rC_Giving__Projected_Amount__c
					,NULL AS rC_Giving__Expected_Giving_Amount__c
					,T1.RE_DB_OwnerShort	AS	rC_Giving__Affiliation__c
					,GETDATE()	AS	rC_Giving__Update_Transactions__c
					,GETDATE()	AS	rC_Giving__Rollup_Transactions__c
					,GETDATE()	AS	rC_Giving__Rollup_Giving__c
					,'FALSE' AS rC_Giving__Is_Giving__c
					,NULL AS rC_Giving__Activity_Type__c
					,T3.[GAU External ID] AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]
					
			 		,T1.PRRating    AS  Proposal_Rating__c
 					,T1.DateAdded	AS	CreatedDate
					,CAST(T2.Ask_Readiness__c AS VARCHAR(255)) AS rC_Giving__Ask_Readiness__c 
					,CAST(T2.Prospect_Tier__c AS VARCHAR(255)) AS 	  Prospect_Tier__c
					,CAST(T2.Reason_Prior_to_July_2013__c AS VARCHAR(255)) AS Reason_Prior_to_July_2013__c
					,CAST(T2.Proposal_Priority__c AS VARCHAR(255)) AS Proposal_Priority__c
 
				
					--reference
					,T1.ImportID AS zref_OldImportID
					,T0.NEW_TGT_IMPORT_ID AS zRefNewImportID
					,T4.ImportID AS zref_NoHH_ImportID
					,T4.HH_ImportID AS zref_HH_ImportID 
					
					INTO SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PROPOSAL
					FROM SUTTER_1P_DATA.dbo.HC_Proposal_v T1
					INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T0 ON T1.RE_DB_Id=T0.RE_DB_Id
					LEFT JOIN SUTTER_1P_MIGRATION.TBL.ProposalAttribute_final T2 ON T1.PRImpID=T2.PRAttrPRImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
					LEFT JOIN SUTTER_1P_DATA.dbo.CHART_Fund T3 ON T1.PRFundID=T3.FundID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
					LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T4 ON T0.NEW_TGT_IMPORT_ID=T4.NoHH_ImportID 
					LEFT JOIN SUTTER_1P_DATA.dbo.CHART_PRStatus T5 ON T1.PRStatus=T5.PRStatus AND T1.RE_DB_OwnerShort=T5.RE_DB_OwnerShort
					-- WHERE T1.PRName NOT LIKE '%planned%'  [SUTTER_1P_MIGRATION].IMP.GIVING_SOLICITOR	
 					ORDER BY CloseDate DESC, t1.RE_DB_OwnerShort, T1.PRImpID
									
END	    
					--SELECT x = SUBSTRING('abcdef', 2, 3); (expression, start, end)

					SELECT DISTINCT Proposal_Purpose__c, LEN(Proposal_Purpose__c) l FROM [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL] WHERE RIGHT(Proposal_Purpose__c,2)=';' ORDER BY Proposal_Purpose__c
						UPDATE [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL] 
						SET Proposal_Purpose__c=REPLACE(Proposal_Purpose__c, SUBSTRING(Proposal_Purpose__c, LEN(Proposal_Purpose__c), 1),'')  WHERE RIGHT(Proposal_Purpose__c,2)=';'
 				
						SELECT DISTINCT Proposal_Purpose__c FROM [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL]  
						WHERE Proposal_Purpose__c LIKE '%;%' ORDER BY proposal_purpose__c
					
					SELECT DISTINCT Proposal_Reason__c FROM [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL] WHERE RIGHT(Proposal_Reason__c,2)='; ' order BY Proposal_Reason__c
 						UPDATE [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL] 
						SET Proposal_Reason__c=REPLACE(Proposal_Reason__c, SUBSTRING(Proposal_Reason__c, LEN(Proposal_Reason__c), 1),'')    WHERE RIGHT(Proposal_Reason__c,2)='; '
					
						SELECT DISTINCT Proposal_Reason__c FROM [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL]-- ORDER BY Proposal_Reason__c
						WHERE Proposal_Reason__c LIKE '%;%' ORDER BY Proposal_Reason__c

					SELECT DISTINCT Proposal_Reason__c FROM [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL] WHERE LEFT(Proposal_Reason__c,1)=';'
					UPDATE [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL] 
					SET Proposal_Reason__c='Planned Giving - Thompson &amp; Associates'  
					WHERE Proposal_Reason__c=';  Planned Giving - Thompson &amp; Associates'
				
					SELECT DISTINCT Proposal_Reason__c FROM [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL] WHERE Proposal_Reason__c LIKE '%;%' ORDER BY Proposal_Reason__c

 
		BEGIN 
			USE [SUTTER_1P_MIGRATION] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'OPPORTUNITY_PROPOSAL'
 		END 			
					UPDATE [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL] SET Name=REPLACE(Name,'"','''') where Name like '%"%'
					UPDATE [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL] SET Proposal_Purpose__c=REPLACE(Proposal_Purpose__c,'"','''') where Proposal_Purpose__c LIKE '%"%'
					UPDATE [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL] SET Proposal_Rating__c=REPLACE(Proposal_Rating__c,'"','''') where Proposal_Rating__c like '%"%'
					UPDATE [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL] SET Proposal_Reason__c=REPLACE(Proposal_Reason__c,'"','''') where Proposal_Reason__c LIKE '%"%'
					 
BEGIN	
				SELECT * 
				FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PROPOSAL
				WHERE RE_PRImpID__c IN (SELECT RE_PRImpID__c FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PROPOSAL GROUP BY RE_PRImpID__c HAVING COUNT(*) >1)
END 


	
		 
		
		/*Single gift applied to multiple proposals. 
				
			SELECT * FROM SUTTER_1P_DATA.dbo.HC_Gift_ProposalLink_v
			WHERE GFLink IN (SELECT GFLink FROM SUTTER_1P_DATA.dbo.HC_Gift_ProposalLink_v GROUP BY GFLink HAVING COUNT(*)>1)
			ORDER BY GFLink		
		*/
		
	 