

USE [SUTTER_1P_DATA]
GO

BEGIN
	DROP TABLE [SUTTER_1P_MIGRATION].[TBL].[SOLICITOR_TEAM_MEMBER]
	DROP TABLE [SUTTER_1P_MIGRATION].[IMP].[SOLICITOR_TEAM_MEMBER]
END;	 

BEGIN
		
		SELECT  DISTINCT
			NULL AS [Id]
			,[OwnerID]=[dbo].[fnc_OwnerID]()
			,[dbo].[fnc_RecordType]([T1].[SOLICITOR_TEAM_RECORD_TYPE])  AS [RecordTypeID]
		 	,[RE_ASRImpID__c]= [T].[RE_DB_OwnerShort]+'-'+[T].[ASRImpID]
			,[RE_PRSolPRImpID__c] = NULL
			
			,[Solicitor__c]=CASE WHEN [T1].[SOLICITOR_TEAM_RECORD_TYPE] ='Solicitor_Philanthropy' THEN [X1].[ID] ELSE NULL END  --Internal(Philanthropy)
			,[Solicitor_External_Contact__r:External_ID__c]= CASE WHEN ([T1].[SOLICITOR_TEAM_RECORD_TYPE]='Solicitor_External' AND [T4].[KeyInd]='I') THEN [T4].[NEW_TGT_IMPORT_ID] ELSE NULL END  						--External
			
			,[Account__r:External_ID__c]=CASE WHEN ([T2].[KeyInd]='O') THEN ([T2].[NEW_TGT_IMPORT_ID])
											  WHEN ([T2].[KeyInd]='I') THEN (CASE WHEN [T3].[NoHH_ImportID] IS NOT NULL THEN [T3].[HH_ImportID] ELSE [T2].[NEW_TGT_IMPORT_ID] END) END 
			,[Contact__r:External_ID__c]=CASE WHEN ([T2].[KeyInd]='I') THEN ([T2].[NEW_TGT_IMPORT_ID]) END 
			
			,[Start_Date__c] = CASE WHEN LEN([T].[ASRDateFrom])=4 THEN '01/01/'+[T].[ASRDateFrom]  
							  WHEN LEN([T].[ASRDateFrom])=6 THEN SUBSTRING([T].[ASRDateFrom],5,2)+'/01/'+LEFT([T].[ASRDateFrom],4) 
							  WHEN LEN([T].[ASRDateFrom])=8 THEN SUBSTRING([T].[ASRDateFrom],5,2)+'/'+RIGHT([T].[ASRDateFrom],2)+'/'+LEFT([T].[ASRDateFrom],4)    
							  END
			,[End_Date__c] = CASE WHEN LEN([T].[ASRDateTo])=4 THEN '01/01/'+[T].[ASRDateTo]  
							  WHEN LEN([T].[ASRDateTo])=6 THEN SUBSTRING([T].[ASRDateTo],5,2)+'/01/'+LEFT([T].[ASRDateTo],4) 
							  WHEN LEN([T].[ASRDateTo])=8 THEN SUBSTRING([T].[ASRDateTo],5,2)+'/'+RIGHT([T].[ASRDateTo],2)+'/'+LEFT([T].[ASRDateTo],4)    
							  END 
			,[Notes__c] = CAST([T].[ASRNotes] AS NVARCHAR(4000))
			,[Type__c] = [T1].[SOLICITOR_TEAM_TYPE] 
			,[Affiliation__c] = [T].[RE_DB_OwnerShort] 
			--,CASE WHEN (CAST(T.ASRDateTo AS DATE))<GETDATE() THEN 'FALSE' ELSE 'TRUE' END AS Is_Active__c 
			,[Is_Active__c] = CASE WHEN (CAST((CASE WHEN LEN([T].[ASRDateTo])=4 THEN '01/01/'+[T].[ASRDateTo]  
						WHEN LEN([T].[ASRDateTo])=6 THEN SUBSTRING([T].[ASRDateTo],5,2)+'/01/'+LEFT([T].[ASRDateTo],4) 
						WHEN LEN([T].[ASRDateTo])=8 THEN SUBSTRING([T].[ASRDateTo],5,2)+'/'+RIGHT([T].[ASRDateTo],2)+'/'+LEFT([T].[ASRDateTo],4) END) AS DATE)) < GETDATE() 
				  THEN 'FALSE' ELSE 'TRUE' END 
			,[Archive_Solicitor__c] = [T1].[Name] 
			,[Primary__c] = CASE WHEN [T].[ASRType]='Relationship Manager' AND [T].[ASRDateTo] IS NULL THEN 'TRUE' ELSE 'FALSE' END 

			,T1.[SH_Email] AS zref_SH_Email
			/*--ref
				,T2.KeyInd zrefT2_KeyInd
				,T3.NonHHImpID AS zrefNonHHImpID
				,T3.HH_ImportID AS zrefHH_ImportID
				,T.ASRLink zref_RLink
				,T.ASRDateFrom zrefDateFrom
				,T.ASRDateTo zrefDateTo   
				,T.ASRType AS zrefASRType
			 */
			INTO [SUTTER_1P_MIGRATION].[TBL].[SOLICITOR_TEAM_MEMBER]
		 	FROM [SUTTER_1P_DATA].[dbo].[HC_Cons_Solicitor_v] AS [T]
			INNER JOIN [SUTTER_1P_DATA].[dbo].[CHART_ConsSolicitor] AS [T1] ON [T].[ASRLink]=[T1].[ASRLink] AND 
																			   [T].[ASRType]=[T1].[ASRType] AND 
																			   [T].[RE_DB_OwnerShort]=[T1].[RE_DB_OwnerShort]
			LEFT JOIN [SUTTER_1P_MIGRATION].[XTR].[USERS] AS [X1] ON [T1].[SH_Email]=[X1].[Email]
			
			INNER JOIN [SUTTER_1P_DATA].[dbo].[HC_Constituents_v] AS  [T2] ON [T].[RE_DB_Id]=[T2].[RE_DB_Id]
			LEFT JOIN [SUTTER_1P_MIGRATION].[tbl].[Contact_HofH] AS [T3] ON [T2].[NEW_TGT_IMPORT_ID]=[T3].[NoHH_ImportID]  
			
			INNER JOIN [SUTTER_1P_DATA].[dbo].[HC_Constituents_v] AS [T4] ON [T].[RE_DB_Id_ASRLink]=[T4].[RE_DB_Id]
		
			WHERE [T1].[Source]='ConsSolicitor' 
			--30,353
  
 
	UNION ALL	
 		--MAKE SURE TO REMOVE DUPLICATE DUE TO CHART LAYOUT
		SELECT DISTINCT 
			NULL AS [Id]
			,[OwnerID]=[dbo].[fnc_OwnerID]()
			,[dbo].[fnc_RecordType]([T3].[SOLICITOR_TEAM_RECORD_TYPE])  AS [RecordTypeID]
			,[RE_ASRImpID__c] = NULL 
			,[RE_PRSolPRImpID__c] = [T1].[PRSolPRImpID]
			,Solicitor__c=CASE WHEN T3.SOLICITOR_TEAM_RECORD_TYPE ='Solicitor_Philanthropy' THEN X1.ID ELSE NULL END  --Internal(Philanthropy)
			,[Solicitor_External_Contact__r:External_ID__c]= CASE WHEN (T3.SOLICITOR_TEAM_RECORD_TYPE='Solicitor_External' AND T2.KeyInd='I') THEN [T2].[NEW_TGT_IMPORT_ID] ELSE NULL END  						--External
			
			,[Account__r:External_ID__c]=CASE WHEN (T5.KeyInd='O') THEN (T5.NEW_TGT_IMPORT_ID)
											  WHEN (T5.KeyInd='I') THEN (CASE WHEN T6.NoHH_ImportID IS NOT NULL THEN T6.HH_ImportID ELSE T5.NEW_TGT_IMPORT_ID END) END 
			,[Contact__r:External_ID__c]=CASE WHEN (T5.KeyInd='I') THEN (T5.NEW_TGT_IMPORT_ID) END 
			,[Start_Date__c] = NULL
			,[End_Date__c] = NULL
			,[Notes__c] = CASE WHEN [T1].[PRSolAmount] IS NOT NULL THEN 'Amount Solicited: '+CAST([T1].[PRSolAmount] AS NVARCHAR(30)) ELSE NULL END 
			,[Type__c]='Proposal Solictor'
			,[Affiliation__c] = [T1].[RE_DB_OwnerShort]
			,[Is_Active__c] = 'FALSE'
			,[Archive_Solicitor__c] = T3.Name
			,[Primary__c]='FALSE'
 			,T3.[SH_Email] AS zref_SH_Email
			FROM [SUTTER_1P_DATA].[dbo].[HC_Proposal_Solicitor_v] AS [T1]
			--solicitor
			INNER JOIN [SUTTER_1P_DATA].[DBO].[HC_Constituents_v] AS [T2] ON [T1].[PRSolImpID]=[T2].[ImportID] AND [T1].[RE_DB_OwnerShort]=[T2].[RE_DB_OwnerShort]
			INNER JOIN [SUTTER_1P_DATA].[dbo].[CHART_ConsSolicitor] AS [T3] ON [T1].[PRSolImpID]=[T3].[ASRLink] AND [T1].[RE_DB_OwnerShort]=[T3].[RE_DB_OwnerShort]
			LEFT JOIN [SUTTER_1P_MIGRATION].[XTR].[USERS] AS [X1] ON [T3].[SH_Email]=[X1].[Email]
 			--solicitee 
			INNER JOIN	[SUTTER_1P_DATA].dbo.[HC_Proposal_v] AS T4 ON T1.[PRSolPRImpID]=T4.[PRImpID] AND T1.[RE_DB_OwnerShort]=T4.[RE_DB_OwnerShort]
			INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Constituents_v] AS T5 ON T4.[ImportID]=T5.[ImportID] AND T4.[RE_DB_OwnerShort]=T5.[RE_DB_OwnerShort]			 
			LEFT JOIN [SUTTER_1P_MIGRATION].[tbl].[Contact_HofH] [T6] ON [T5].[NEW_TGT_IMPORT_ID]=[T6].[NoHH_ImportID]


END;
	
BEGIN 

	USE [SUTTER_1P_MIGRATION] 
 	EXEC sp_FindStringInTable '%"%', 'TBL', 'SOLICITOR_TEAM_MEMBER'

	UPDATE [SUTTER_1P_MIGRATION].[TBL].[SOLICITOR_TEAM_MEMBER] set Notes__c=REPLACE(Notes__c,'"','''') where Notes__c LIKE '%"%'


END 

BEGIN-- sort [Account__r:External_ID__c], [Contact__r:External_ID__c]

		CREATE INDEX idx_solicitor_team ON SUTTER_1P_MIGRATION.TBL.[SOLICITOR_TEAM_MEMBER]([Account__r:External_ID__c], [Contact__r:External_ID__c]);
		
		SELECT * 
		INTO SUTTER_1P_MIGRATION.[IMP].[SOLICITOR_TEAM_MEMBER]
		FROM SUTTER_1P_MIGRATION.[TBL].[SOLICITOR_TEAM_MEMBER]
		ORDER BY [Account__r:External_ID__c], [Contact__r:External_ID__c]

		SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.[IMP].[SOLICITOR_TEAM_MEMBER]
END

--TEST ZONE
		SELECT * FROM SUTTER_1P_DATA.dbo.HC_Proposal_Solicitor_v
		SELECT * FROM SUTTER_1P_DATA.dbo.HC_Proposal_v
		SELECT COUNT(*) FROM [SUTTER_1P_MIGRATION].[IMP].[SOLICITOR_TEAM_MEMBER] --30353
		SELECT * FROM [SUTTER_1P_DATA].[dbo].[HC_Cons_Solicitor_v] 
	 
		BEGIN--check dupes
			SELECT * FROM [SUTTER_1P_MIGRATION].[IMP].[SOLICITOR_TEAM_MEMBER]  
			WHERE [RE_ASRIMPID__C] IN (SELECT [RE_ASRIMPID__C] FROM [SUTTER_1P_MIGRATION].[IMP].[SOLICITOR_TEAM_MEMBER]  GROUP BY [RE_ASRIMPID__C] HAVING COUNT(*)>1)
		END 

		SELECT DISTINCT [ASRType] FROM [SUTTER_1P_DATA].[dbo].[HC_Cons_Solicitor_v] ORDER BY [ASRType]

		BEGIN--update notes
			UPDATE [SUTTER_1P_MIGRATION].[IMP].[SOLICITOR_TEAM_MEMBER]  SET [Notes__c] = REPLACE([Notes__c],'"','''') where [Notes__c] like '%"%'
		END 

		BEGIN--test
		  SELECT * FROM [SUTTER_1P_DATA].[dbo].[HC_Cons_Solicitor_v] WHERE [ASRImpID]='03947-519-0000004514'
		  SELECT * FROM [SUTTER_1P_DATA].[dbo].[hc_constituents_v] WHERE [re_Db_id]='SMCF-03947-079-0000103285'
		  SELECT * FROM [SUTTER_1P_MIGRATION].[tbl].[contact_hofh] WHERE [hh_importid]='SMCF-03947-079-0000103285' OR [nohh_importid]='SMCF-03947-079-0000103285'
		  SELECT * FROM [SUTTER_1P_MIGRATION].[imp].[solicitor_team_member] 
		  WHERE [Solicitor__c]='0053B000000QvGSQA0'
		  WHERE RE_ASRIMPID__C='SMCF-03947-519-0000004514'
		END

		SELECT * FROM [SUTTER_1P_DATA].[dbo].[CHART_ConsSolicitor] 
		WHERE [Sh_email]='moorejn@sutterhealth.org'
		UPDATE 	[SUTTER_1P_DATA].[dbo].[CHART_ConsSolicitor] 
		SET [SH_Email]='moorejn@sutterhealth.org'
		WHERE [Sh_email]='moorejn@sutterhealth.rog'
		
		WHERE ASRLink='03947-593-0000145113'
		SELECT * FROM [SUTTER_1P_MIGRATION].[xtr].[users] 
		
																				
		SELECT * FROM [RE7_SH_SMCF].[dbo].[hc_constituents_V] WHERE [RE_DB_OwnerShort] LIKE 'smcf%'