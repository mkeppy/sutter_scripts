USE SUTTER_1P_MIGRATION
GO

BEGIN
		DROP TABLE SUTTER_1P_MIGRATION.IMP.TASK	
 END
  
 
BEGIN
		--TASKS FROM CONS ACTION 
			SELECT DISTINCT 
				 OwnerID=T7.OwnerID
				,WhatId=CASE WHEN T1.KeyInd='O' THEN X1.ID END 
				,WhoId=CASE WHEN T1.KeyInd='I' THEN X2.ID END 
				,RE_ACImpID__c=CAST(T.RE_DB_OwnerShort+'-'+T.ACImpID AS NVARCHAR(30))
				,T.ACDate AS ActivityDate
				,T.ACCat AS Type__c
				,T.ACType AS Archived_Action_Type__c
				,T2.Move__c
				,CASE WHEN T.ACStatus ='NULL' THEN NULL ELSE T.[ACStatus] END AS Archived_Status__c
				,T3.Stage__c
				,[Subject]=CASE WHEN T9.CANoteDesc IS NULL THEN T3.Stage__c ELSE T3.Stage__c +'. '+T9.CANoteDesc END
				,CASE WHEN T.AddedBy ='NULL' THEN NULL ELSE T.AddedBy END AS RE_Action_Added_By__c
				,T.RE_DB_OwnerShort AS Affiliation__c
				,[Status]=CASE WHEN (T.ACDate < GETDATE()) THEN 'Completed' ELSE 'Open' END
				
				--action solicitor
				,CAST(T4.RE_Action_Solicitor__c  AS VARCHAR(4000)) AS RE_Action_Solicitor__c
			
				--attribute-action cahrt_Attribute
				,CAST(T6.RE_Team_Member__c  AS VARCHAR(4000)) AS RE_Team_Member__c
				,CAST(T6.Thank_you_call__c  AS VARCHAR(4000)) AS Thank_you_call__c
				,CAST(T6.Identified_By__c AS VARCHAR(4000)) AS Identified_By__c
				,CAST(T6.Planned_Gift_Amount__c  AS VARCHAR(255)) AS Planned_Gift_Amount__c
				,CAST(T6.Requested_Amount__c AS VARCHAR(255)) AS Requested_Amount__c

				--chart_ActionAttribute
				,T10.Category__c
				,T10.Subcategory__c
				,T10.Planned_Gift_Type__c
				,T10.Attended__c
				,T10.Type_of_Action__c
				,T10.X1_000_000_Visionaries__c
				,T10.X1_000_Level_1__c
				,T10.X10_000_Patrons__c
				,T10.X100_000_Humanitarians__c
				,T10.X2_500_Level_2__c
				,T10.X_25_000_Founders_c__c
				,T10.X5_000_Friends__c
				,T10.X50_000_Benefactors__c
				,T10.X500_000_Philanthropists__c
				,T10.Building_on_Tradition__c
				,T10.Heritage_Circle__c
				,T10.Memorial_Tree__c
				,T10.Past_Patient_Contact__c
				,T10.Campus__c
				,T10.Event_Participation__c
				,T10.Grateful_Patient_Follow_up__c
				,T10.Invitation__c
				,T10.NCHX_Invitation__C
				,T10.Responded_To__c


				--action note
 				,CAST(T5.[Description] AS VARCHAR(4000)) AS [Description]
				
				--reference
				,T1.KeyInd zref_KeyInd
				,T.AddedBy zref_AddedBy
				,NULL AS zrefSeq
		
			INTO SUTTER_1P_MIGRATION.IMP.TASK	
			FROM SUTTER_1P_DATA.DBO.HC_Cons_Action_v T
			INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.RE_DB_ID=T1.RE_DB_ID 
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_ActionStatus T2 ON T.ACStatus=T2.ACStatus AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_ActionType T3 ON T.ACType=T3.ACType AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Cons_Action_Solicitor_1 T4 ON T.ACImpID=T4.ACImpID AND T.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes_1 T5 ON T.ACImpID=T5.CALink AND T.RE_DB_OwnerShort=T5.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes_desc T9 ON T.ACImpID=T9.CALink AND T.RE_DB_OwnerShort=T9.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Attribute_task_1 T6 ON T.ACImpID=T6.ACImpId AND T.RE_DB_OwnerShort=T6.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.ActionSolicitor_OwnerId T7 ON T.ACImpID=T7.ACImpID AND T.RE_DB_OwnerShort=T7.RE_DB_OwnerShort		--Action Solitior 
		 	LEFT JOIN SUTTER_1P_MIGRATION.TBL.ActionAttribute_final AS T10 ON  T.ACImpID=T10.ACImpID AND T.RE_DB_OwnerShort=T10.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.XTR.ACCOUNT X1 ON T1.NEW_TGT_IMPORT_ID=X1.External_Id__c
			LEFT JOIN SUTTER_1P_MIGRATION.XTR.CONTACT X2 ON T1.NEW_TGT_IMPORT_ID=X2.External_Id__c
		 WHERE T3.[Convert]='Yes'
			--810,116
END;		 
 
 SELECT DISTINCT Category__C FROM [SUTTER_1P_MIGRATION].imp.task ORDER BY Category__C
SELECT DISTINCT Subcategory__c FROM [SUTTER_1P_MIGRATION].imp.task ORDER BY Subcategory__c




BEGIN

		BEGIN 
			USE [SUTTER_1P_MIGRATION] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'TASK'
 		END   
		       
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET [Description]=REPLACE([Description],'"','''') where [Description] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET [Subject] =REPLACE([Subject],'"','''') where [Subject] LIKE '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET Type__c =REPLACE(Type__c,'"','''') where Type__c LIKE '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET RE_Action_Solicitor__c =REPLACE(RE_Action_Solicitor__c,'"','''') where RE_Action_Solicitor__c LIKE '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET RE_Team_Member__c =REPLACE(RE_Team_Member__c,'"','''') where RE_Team_Member__c LIKE '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET Thank_you_call__c =REPLACE(Thank_you_call__c,'"','''') where Thank_you_call__c LIKE '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET Archived_Action_Type__c =REPLACE(Archived_Action_Type__c,'"','''') where Archived_Action_Type__c LIKE '%"%'

		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET [Thank_you_call__c]=RTRIM(LTRIM([Thank_you_call__c])) WHERE [Thank_you_call__c] IS NOT null
END
 
BEGIN-- resolve duplicate ExternalId

		SELECT *
		FROM  [SUTTER_1P_MIGRATION].[IMP].[TASK] 
		WHERE RE_ACImpID__c IN (SELECT RE_ACImpID__c FROM SUTTER_1P_MIGRATION.IMP.TASK GROUP BY RE_ACImpID__c HAVING COUNT(*)>1)
		ORDER BY RE_ACImpID__c, OwnerID

		--update zrefSeq with seq #		
		UPDATE T
		SET zrefSeq = rn
		FROM ( 
			   SELECT zrefSeq,
					 ROW_NUMBER() OVER ( PARTITION BY RE_ACImpID__c ORDER BY RE_ACImpID__c, OwnerId) AS rn
			   FROM [SUTTER_1P_MIGRATION].[IMP].[TASK] 
	 		 )  T

		--update RE_ACTIMPID__c 
		SELECT OWnerID, RE_ACIMPID__c, zrefSeq
		FROM  [SUTTER_1P_MIGRATION].[IMP].[TASK]   
		WHERE RE_ACImpID__c IN (SELECT RE_ACImpID__c FROM SUTTER_1P_MIGRATION.IMP.TASK GROUP BY RE_ACImpID__c HAVING COUNT(*)>1)
		ORDER BY RE_ACImpID__c, OwnerID

		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] 
		SET RE_ACImpID__c= RE_ACImpID__c+'-'+CAST(zrefSeq AS nvarchar(3))
		WHERE RE_ACImpID__c IN (SELECT RE_ACImpID__c FROM SUTTER_1P_MIGRATION.IMP.TASK GROUP BY RE_ACImpID__c HAVING COUNT(*)>1)
		AND zrefSeq>1

		SELECT RE_ACIMPID__c, LEN(RE_ACIMPID__c) L 
		FROM [SUTTER_1P_MIGRATION].[IMP].[TASK]   
		ORDER BY L DESC

END 
 

 SELECT *
 FROM SUTTER_1P_MIGRATION.imp.task 
WHERE [RE_ACImpID__c]='SCAH-03038-504-0000027616'
 

 SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.imp.task 
  
  
 SELECT T1.* 
 INTO [SUTTER_1P_MIGRATION].IMP.[TASK_XCP]     
 FROM [SUTTER_1P_MIGRATION].IMP.[TASK] T1
 LEFT JOIN [SUTTER_1P_MIGRATION].XTR.TASK X1 ON T1.RE_ACImpID__c=X1.RE_ACImpID__c
 WHERE X1.RE_ACImpID__c IS NULL  

 SELECT * FROM [SUTTER_1P_MIGRATION].imp.task WHERE [RE_ACImpID__c] LIKE '%MPHF-00001-504-0000020327%'
 
 SELECT * FROM [SUTTER_1P_MIGRATION].xtr.task WHERE re_acimpId__c LIKE '%MPHF-00001-504-0000020327%'

 
 SELECT * FROM [SUTTER_1P_MIGRATION].imp.task_xcp WHERE [RE_ACImpID__c] LIKE '%MPHF-00001-504-0000020327%'
  
---code   ot update with a sequence number,. 

 		update T
		set cn = rn
		from (
			   select cn,
					  row_number() over(order by (select 1)) as rn
			   from TableX
			 ) T


