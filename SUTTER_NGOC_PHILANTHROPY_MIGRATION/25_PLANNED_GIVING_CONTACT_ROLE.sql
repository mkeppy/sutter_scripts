USE SUTTER_1P_DATA
GO
 
 BEGIN
	DROP TABLE [SUTTER_1P_MIGRATION].IMP.PLANNED_GIVING_ROLES
 END;	

 BEGIN--Planned Giving Roles
   		--Beneficiaries 
				--Ind Relationships
				SELECT 	NULL AS rC_Giving__Account__c
						,X1.ID AS rC_Giving__Contact__c
						,T4.[rC_Giving__External_ID__c] AS [rC_Giving__Planned_Giving__r:rC_Giving__External_ID__c]
						,T1.[GFPGBenType] AS rC_Giving__Role__c
						,'Beneficiaries_Ind_Relat' AS zrefSrc
 				INTO [SUTTER_1P_MIGRATION].IMP.PLANNED_GIVING_ROLES
				FROM [SUTTER_1P_DATA].dbo.HC_Planned_Gift_Beneficiaries_v T1
				INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Ind_Relat_v] T2 ON T1.[GFPGBenRelImpID]=T2.[IRImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
				INNER JOIN [SUTTER_1P_MIGRATION].XTR.[CONTACT] X1 ON T2.[NEW_TGT_IMPORT_ID_IR]=X1.[RE_IRIMPID__C]
				INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Gift_SplitGift_v] T3 ON T1.[GFPGBenGiftImpID]=T3.[GFImpID] AND T1.[RE_DB_OwnerShort]=T3.[RE_DB_OwnerShort]
				INNER JOIN [SUTTER_1P_MIGRATION].IMP.[PLANNED_GIVING] T4 ON (T3.[RE_DB_OwnerShort]+'-GT-'+T3.[GSplitImpID])=T4.[rC_Giving__External_ID__c]
			UNION 	
				--Self (Constituent)
				SELECT 	NULL AS rC_Giving__Account__c
						,X1.ID AS rC_Giving__Contact__c
						,T4.[rC_Giving__External_ID__c] AS [rC_Giving__Planned_Giving__r:rC_Giving__External_ID__c]
						,T1.[GFPGBenType] AS rC_Giving__Role__c
						,'Beneficiaries_Self' AS zrefSrc
	 		 
				FROM [SUTTER_1P_DATA].dbo.HC_Planned_Gift_Beneficiaries_v T1
				INNER JOIN [SUTTER_1P_DATA].DBO.HC_Gift_v T2 ON T1.[GFPGBenGiftImpID]=T2.[GFImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
				INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Gift_SplitGift_v] T3 ON T1.[GFPGBenGiftImpID]=T3.[GFImpID] AND T1.[RE_DB_OwnerShort]=T3.[RE_DB_OwnerShort]
				INNER JOIN [SUTTER_1P_MIGRATION].IMP.[PLANNED_GIVING] T4 ON (T3.[RE_DB_OwnerShort]+'-GT-'+T3.[GSplitImpID])=T4.[rC_Giving__External_ID__c]
				INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Constituents_v] T5 ON T2.[ImportID]=T5.[ImportID] AND T2.[RE_DB_OwnerShort]=T5.[RE_DB_OwnerShort]
				INNER JOIN [SUTTER_1P_MIGRATION].XTR.[CONTACT] X1 ON T5.[NEW_TGT_IMPORT_ID]=X1.[EXTERNAL_ID__C]
				WHERE T1.[GFPGBenTypeofRelat]='Self'
   			UNION
   		--Planned Gift Relationship. 
				--Ind Relat
				SELECT 	DISTINCT NULL AS rC_Giving__Account__c
						,CASE WHEN X1.ID IS NULL THEN X2.ID ELSE X1.ID END AS rC_Giving__Contact__c
						,T4.[rC_Giving__External_ID__c] AS [rC_Giving__Planned_Giving__r:rC_Giving__External_ID__c]
						,T1.[GFPGRelTypeofRel] AS rC_Giving__Role__c
						,'PG_Relat_Ind' AS zrefSrc
		 		 
				FROM [SUTTER_1P_DATA].dbo.HC_Planned_Gift_Relationship_v T1
				INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Gift_SplitGift_v] T3 ON T1.[GFPGRelGiftImpID]=T3.[GFImpID] AND T1.[RE_DB_OwnerShort]=T3.[RE_DB_OwnerShort]
				INNER JOIN [SUTTER_1P_MIGRATION].IMP.[PLANNED_GIVING] T4 ON (T3.[RE_DB_OwnerShort]+'-GT-'+T3.[GSplitImpID])=T4.[rC_Giving__External_ID__c]
				LEFT JOIN [SUTTER_1P_DATA].DBO.[HC_Ind_Relat_v] T5 ON T1.[GFPGRelImpID]=T5.[IRImpID] AND T1.[RE_DB_OwnerShort]=T5.[RE_DB_OwnerShort]
				LEFT JOIN [SUTTER_1P_MIGRATION].XTR.[CONTACT] X1 ON T5.[NEW_TGT_IMPORT_ID_IR]=X1.[RE_IRIMPID__C]	
				LEFT JOIN [SUTTER_1P_MIGRATION].XTR.[CONTACT] X2 ON T5.[NEW_TGT_IMPORT_ID_IRLINK]=X2.[EXTERNAL_ID__C]
				WHERE T1.[GFPGRelTypeofRel]='Individual'
			UNION	
				--Org Relat
				SELECT 	 CASE WHEN X1.ID IS NULL THEN X2.ID ELSE X1.ID END AS rC_Giving__Account__c
						,CASE WHEN X1C.ID IS NULL THEN X2C.ID ELSE X1C.ID END AS rC_Giving__Contact__c
						,T4.[rC_Giving__External_ID__c] AS [rC_Giving__Planned_Giving__r:rC_Giving__External_ID__c]
						,T1.[GFPGRelTypeofRel] AS rC_Giving__Role__c
						,'PG_Relat_Org' AS zrefSrc
	 			FROM [SUTTER_1P_DATA].dbo.HC_Planned_Gift_Relationship_v T1
				INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Gift_SplitGift_v] T3 ON T1.[GFPGRelGiftImpID]=T3.[GFImpID] AND T1.[RE_DB_OwnerShort]=T3.[RE_DB_OwnerShort]
				INNER JOIN [SUTTER_1P_MIGRATION].IMP.[PLANNED_GIVING] T4 ON (T3.[RE_DB_OwnerShort]+'-GT-'+T3.[GSplitImpID])=T4.[rC_Giving__External_ID__c]
				LEFT JOIN [SUTTER_1P_DATA].DBO.[HC_Org_Relat_v] T5 ON T1.[GFPGRelImpID]=T5.[ORImpID] AND T1.[RE_DB_OwnerShort]=T5.[RE_DB_OwnerShort]
				LEFT JOIN [SUTTER_1P_MIGRATION].XTR.[ACCOUNT] X1 ON T5.[NEW_TGT_IMPORT_ID_OR]=X1.[RE_ORIMPID__C]
				LEFT JOIN [SUTTER_1P_MIGRATION].XTR.[CONTACT] X1C ON X1.ID=X1C.[ACCOUNTID]
				LEFT JOIN [SUTTER_1P_MIGRATION].XTR.[ACCOUNT] X2 ON T5.[NEW_TGT_IMPORT_ID_ORLINK]=X2.[EXTERNAL_ID__C]
				LEFT JOIN [SUTTER_1P_MIGRATION].XTR.[CONTACT] X2C ON X2.ID=X2C.[ACCOUNTID]
				WHERE T1.[GFPGRelTypeofRel]='Organization'

END;
   

   SELECT * FROM [SUTTER_1P_MIGRATION].IMP.PLANNED_GIVING_ROLES
   ORDER BY [rC_Giving__Role__c]