
USE SUTTER_1P_DATA
GO


BEGIN
		DROP TABLE SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_UPDATE_PARENT_Open
		DROP TABLE SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_UPDATE_PARENT_Completed
END



BEGIN--OPEN TRANSACTION GENERATION (AKA RECORD TOUCH)


				--standard api option OPEN Parent
				SELECT	X1.ID, '' as rC_Giving__Rollup_Transactions__c, '' as rC_Giving__Update_Transactions__c, 'TRUE' RTP_Updated__c, 
						T.StageName AS zrefStageName, 
						X1.EXTERNAL_ID__C zrefExtId, 
						zrefGFType, 
						[T].[CloseDate] AS zrefCloseDate, 
						[T].[rC_Giving__First_Payment_Date__c] AS zrefFirstPaymentDate,
						[T].[rC_Giving__Payment_End_Date__c] AS zrefPaymentEndDate, 
						[T].[rC_Giving__Giving_Years__c] AS zrefYears,
						[T].[rC_Giving__Expected_Giving_Amount__c] AS zrefExpGiveAmt,
						X1.RE_GSPLITIMPID__C AS zrefrefGFSplitImpID, 
						X1.RE_GFIMPID__C AS zrefrefGFImpID
						
				INTO SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_UPDATE_PARENT_Open
				FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT T
				INNER JOIN SUTTER_1P_MIGRATION.[XTR].[OPPORTUNITY] AS X1 ON T.External_Id__c=X1.EXTERNAL_ID__C
				WHERE T.StageName='Open'
				ORDER BY zrefGFType, T.StageName, T.[ACCOUNT:External_Id__c], T.CloseDate, T.[CAMPAIGN:External_Id__c]
	 			-- 1562
				
				--standard api option COMPLETED Parent
				SELECT	X1.ID, '' as rC_Giving__Rollup_Transactions__c, '' as rC_Giving__Update_Transactions__c, 'TRUE' RTP_Updated__c,
						T.StageName AS zrefStageName, X1.EXTERNAL_ID__C zrefExtId, zrefGFType,
						X1.RE_GSPLITIMPID__C AS zrefrefGFSplitImpID, X1.RE_GFIMPID__C AS zrefrefGFImpID
						
			 	INTO SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_UPDATE_PARENT_Completed
				FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT T
				INNER JOIN SUTTER_1P_MIGRATION.[XTR].[OPPORTUNITY] AS X1 ON T.External_Id__c=X1.EXTERNAL_ID__C
				WHERE T.StageName='Completed'
				ORDER BY zrefGFType, T.StageName, T.[ACCOUNT:External_Id__c], T.CloseDate, T.[CAMPAIGN:External_Id__c]
 
END;

 SELECT x.id, t.[zrefGFType], t.[External_Id__c] zrefExtId
 FROM [SUTTER_1P_MIGRATION].imp.[OPPORTUNITY_PARENT] T
 INNER JOIN [SUTTER_1P_MIGRATION].xtr.[OPPORTUNITY] x
 ON t.[External_Id__c]=x.[EXTERNAL_ID__C]
  WHERE t.[zrefGFType] LIKE '%pledge%'
