/*

1. IMPORT TABLE TO SQL.
2. NAME table as "relationship"
3. COLUMN NAMES SHOULD BE: 
   TID (Unique relationship id. e.g IRImpID), 
   ID (1st relationship record id. e.g ImportID), 
   REL_ID (2nd record. e.g IRLink)
    
4. RUN script. script removes duplicated records. table 'relationship' is left with final 'good' records. 
5. TEST 

*/

USE SUTTER_1P_DATA
GO


--  (TID=UniqueID,ID=RelatID,REL_ID=RecipID)
--  select * from dbo.relationship  

	/* REMOVE TABLES */

	-- drop the relationship table and relationship_bak table to start over.
		DROP TABLE SUTTER_1P_MIGRATION.TBL.relationship_ind
		DROP TABLE SUTTER_1P_MIGRATION.TBL.relationship_ind_bak
		DROP TABLE SUTTER_1P_MIGRATION.TBL.relationship_org
		DROP TABLE SUTTER_1P_MIGRATION.TBL.relationship_org_bak


--INDIVIDUAL RELATIONSHIPS DUPLICATES

	/*	1. 2. 3. IMPORT TABLE TO SQL. */ 
	 
		-- This script imports the HC Ind Relat views result into the dbo.relationship table and assigns column headings. 
		 --It uses the new_tgt_ids. 

			SELECT  DISTINCT
			NEW_TGT_IMPORT_ID_IR AS TID,
					NEW_TGT_IMPORT_ID_CN AS ID,
					NEW_TGT_IMPORT_ID_IRLINK AS REL_ID,
					LEFT(NEW_TGT_IMPORT_ID_IR,4) AS RE_DB_OwnerShort,
					NEW_TGT_IMPORT_ID_IR  AS IRImpID,
					NEW_TGT_IMPORT_ID_CN AS ImportID,
					NEW_TGT_IMPORT_ID_IRLINK AS IRLink
			INTO    SUTTER_1P_MIGRATION.TBL.relationship_ind    
			FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
			WHERE NEW_TGT_IMPORT_ID_IRLINK IS NOT NULL AND NEW_TGT_IMPORT_ID_IRLINK<>''
			ORDER BY NEW_TGT_IMPORT_ID_IR
			--236,706

		-- check for possible duplicate primary key. 
			select TID, count(*) 
			from  SUTTER_1P_MIGRATION.tbl.relationship_ind
			group by tid
			having count(*) >1
			--0

		-- create backup of relatinoship table
			SELECT  *
			INTO    SUTTER_1P_MIGRATION.TBL.relationship_ind_bak
			FROM    SUTTER_1P_MIGRATION.TBL.relationship_ind
		
			SELECT * from SUTTER_1P_MIGRATION.tbl.relationship_ind


	/* 4. RUN SCRIPT */ 
			--REMOVE unwanted reciprocal records. 
					WITH
					cteSwap AS
					( --=== Make RelatID and RecipID in the same order for all rows so we can compare apples to apples
					 SELECT TID,
							Compare1 = CASE WHEN ID > REL_ID THEN REL_ID ELSE ID END,
							Compare2 = CASE WHEN ID > REL_ID THEN ID ELSE REL_ID END
					 FROM SUTTER_1P_MIGRATION.TBL.relationship_ind  
					),
					cteEnumerate AS
					( --=== Then number the rows. Notice how we drag UniqueID along for the ride to maintain a "connection" with the original table.
					 SELECT TID,
							RowNum = ROW_NUMBER() OVER(PARTITION BY Compare1, Compare2 ORDER BY TID)
					 FROM cteSwap
					) --=== Delete the dupes from the cascaded CTE.  Yeah... most people don't know that you can actually do this. 
					 DELETE cteEnumerate 
					 WHERE RowNum > 1
					--105,332

	/* 5. CREATE RELATIONSHIPS BASE TABLE WITH DEDUPLICATEDD RECORDS AND THOSE WHERE THE LINK = NULL*/
					
					DROP TABLE SUTTER_1P_MIGRATION.TBL.RELATIONSHIP_IND_FINAL

					SELECT DISTINCT 
					   [TID]
					  ,[ID]
					  ,[REL_ID]
					  ,[RE_DB_OwnerShort]
					  ,[IRImpID]
					  ,[ImportID]
					  ,[IRLink]
					INTO SUTTER_1P_MIGRATION.TBL.RELATIONSHIP_IND_FINAL
					FROM SUTTER_1P_MIGRATION.[TBL].[relationship_ind]
					UNION ALL 
                    SELECT  DISTINCT 
							NEW_TGT_IMPORT_ID_IR AS TID ,
                            NEW_TGT_IMPORT_ID_CN AS ID ,
                            CASE WHEN NEW_TGT_IMPORT_ID_IRLINK = ''
                                      OR NEW_TGT_IMPORT_ID_IRLINK IS NULL THEN NULL
                                 ELSE NEW_TGT_IMPORT_ID_IRLINK
                            END AS REL_ID ,
                            LEFT(NEW_TGT_IMPORT_ID_IR,4) AS RE_DB_OwnerShort ,
                           NEW_TGT_IMPORT_ID_IR AS IRImpID ,
                           NEW_TGT_IMPORT_ID_CN AS ImportID ,
                           NEW_TGT_IMPORT_ID_IRLINK AS IRLink
                    FROM    SUTTER_1P_DATA.dbo.HC_Ind_Relat_v
                    WHERE   ( NEW_TGT_IMPORT_ID_IRLINK = ''
                              OR NEW_TGT_IMPORT_ID_IRLINK IS NULL
                            )
					GO
					--412,172
 
/* 6. TEST  */

			SELECT  T1.TID,
					T1.ID AS T1_ID ,
					T1.REL_ID AS T1_REL_ID ,
					T2.ID AS T2_ID ,
					T2.REL_ID AS T2_REL_ID
					,Compare1 = CASE WHEN T1.ID > T1.REL_ID THEN T1.REL_ID ELSE T1.ID END
					,Compare2 = CASE WHEN T1.ID > T1.REL_ID THEN T1.ID ELSE T1.REL_ID END
					,RowNum = ROW_NUMBER() OVER(PARTITION BY (CASE WHEN T1.ID > T1.REL_ID THEN T1.REL_ID ELSE T1.ID END), (CASE WHEN T1.ID > T1.REL_ID THEN T1.ID ELSE T1.REL_ID END) ORDER BY T1.TID)
			FROM    SUTTER_1P_MIGRATION.TBL.relationship_ind_bak AS T1
					LEFT JOIN SUTTER_1P_MIGRATION.TBL.relationship_ind T2 ON T1.TID = T2.TID
					WHERE T1.RE_DB_OwnerShort ='SMCF' AND (T1.ID='SMCF-000000045' OR T1.REL_ID='SMCF-000000045')
			ORDER BY T1.ID ,
					 T1.REL_ID
					 ,RowNum

			
			SELECT  T1.TID,
					T1.ID AS T1_ID ,
					T1.REL_ID AS T1_REL_ID ,
					T2.ID AS T2_ID ,
					T2.REL_ID AS T2_REL_ID
			FROM    SUTTER_1P_MIGRATION.TBL.relationship_ind_bak AS T1
					LEFT JOIN SUTTER_1P_MIGRATION.TBL.relationship_ind T2 ON T1.TID = T2.TID
			WHERE	T1.ID='SMCF-000000038' OR T1.REL_ID='SMCF-0000000038' OR T1.ID='SMCF-000000373' OR T1.ID='SMCF-000017047' OR T1.ID='SMCF-000016488'
			ORDER BY T1.ID ,
					 T1.REL_ID

		 
--ORGANIZATION RELATIONSHIPS DUPLICATES

	/* REMOVE TABLES */

	-- drop the relationship table and relationship_bak table to start over.

		DROP TABLE SUTTER_1P_MIGRATION.TBL.relationship_org
		DROP TABLE SUTTER_1P_MIGRATION.TBL.relationship_org_bak

	/*	1. 2. 3. IMPORT TABLE TO SQL. */ 
	 
		-- This script imports the HC Ind Relat views result into the dbo.relationship table and assigns column headings.
			SELECT  DISTINCT 
					NEW_TGT_IMPORT_ID_OR AS TID ,
					NEW_TGT_IMPORT_ID_CN AS ID ,
					CASE WHEN NEW_TGT_IMPORT_ID_ORLINK='' OR NEW_TGT_IMPORT_ID_ORLINK IS NULL THEN NULL ELSE NEW_TGT_IMPORT_ID_ORLINK END AS REL_ID ,
					LEFT(NEW_TGT_IMPORT_ID_OR,4) AS RE_DB_OwnerShort ,
					NEW_TGT_IMPORT_ID_OR AS ORImpID ,
					NEW_TGT_IMPORT_ID_CN AS ImportID ,
					NEW_TGT_IMPORT_ID_ORLINK AS ORLink
			INTO    SUTTER_1P_MIGRATION.TBL.relationship_org
			FROM    SUTTER_1P_DATA.dbo.HC_Org_Relat_v
			WHERE   NEW_TGT_IMPORT_ID_ORLINK IS NOT NULL AND NEW_TGT_IMPORT_ID_ORLINK<>''
			--23,092

		-- check for possible duplicate primary key. 
			select TID, count(*) 
			from  SUTTER_1P_MIGRATION.tbl.relationship_org
			group by tid
			having count(*) >1
			--0

		-- create backup of relatinoship table
			SELECT  *
			INTO    SUTTER_1P_MIGRATION.TBL.relationship_org_bak
			FROM    SUTTER_1P_MIGRATION.TBL.relationship_org
		
			SELECT * from SUTTER_1P_MIGRATION.tbl.relationship_org

	/* 4. RUN SCRIPT */ 
			--REMOVE unwanted reciprocal records. 
					WITH
					cteSwap AS
					( --=== Make RelatID and RecipID in the same order for all rows so we can compare apples to apples
					 SELECT TID,
							Compare1 = CASE WHEN ID > REL_ID THEN REL_ID ELSE ID END,
							Compare2 = CASE WHEN ID > REL_ID THEN ID ELSE REL_ID END
					 FROM SUTTER_1P_MIGRATION.TBL.relationship_org  
					),
					cteEnumerate AS
					( --=== Then number the rows. Notice how we drag UniqueID along for the ride to maintain a "connection" with the original table.
					 SELECT TID,
							RowNum = ROW_NUMBER() OVER(PARTITION BY Compare1, Compare2 ORDER BY TID)
					 FROM cteSwap
					) --=== Delete the dupes from the cascaded CTE.  Yeah... most people don't know that you can actually do this. 
					 DELETE cteEnumerate 
					 WHERE RowNum > 1
					--1,336

	/* 5. CREATE RELATIONSHIPS BASE TABLE WITH DEDUPLICATEDD RECORDS AND THOSE WHERE THE LINK = NULL*/
					DROP TABLE SUTTER_1P_MIGRATION.TBL.RELATIONSHIP_ORG_FINAL

					SELECT DISTINCT
					   [TID]
					  ,[ID]
					  ,[REL_ID]
					  ,[RE_DB_OwnerShort]
					  ,[ORImpID]
					  ,[ImportID]
					  ,[ORLink]
					INTO SUTTER_1P_MIGRATION.TBL.RELATIONSHIP_ORG_FINAL
					FROM SUTTER_1P_MIGRATION.[TBL].[relationship_org]
					UNION ALL 
                    SELECT DISTINCT
					        NEW_TGT_IMPORT_ID_OR AS TID ,
                            NEW_TGT_IMPORT_ID_CN AS ID ,
                            CASE WHEN NEW_TGT_IMPORT_ID_ORLINK= ''
                                      OR NEW_TGT_IMPORT_ID_ORLINK IS NULL THEN NULL
                                 ELSE NEW_TGT_IMPORT_ID_ORLINK
                            END AS REL_ID ,
                            LEFT(NEW_TGT_IMPORT_ID_OR,4) AS RE_DB_OwnerShort ,
                            NEW_TGT_IMPORT_ID_OR AS ORImpID ,
                            NEW_TGT_IMPORT_ID_CN AS ImportID ,
                            NEW_TGT_IMPORT_ID_ORLINK AS ORLink
                    FROM    SUTTER_1P_DATA.dbo.HC_Org_Relat_v
                    WHERE   ( NEW_TGT_IMPORT_ID_ORLINK = ''
                              OR NEW_TGT_IMPORT_ID_ORLINK IS NULL
                            )
					GO
					--50,725
/* 6. TEST  */

			SELECT  T1.TID,
					T1.ID AS T1_ID ,
					T1.REL_ID AS T1_REL_ID ,
					T2.ID AS T2_ID ,
					T2.REL_ID AS T2_REL_ID
					,Compare1 = CASE WHEN T1.ID > T1.REL_ID THEN T1.REL_ID ELSE T1.ID END
					,Compare2 = CASE WHEN T1.ID > T1.REL_ID THEN T1.ID ELSE T1.REL_ID END
					,RowNum = ROW_NUMBER() OVER(PARTITION BY (CASE WHEN T1.ID > T1.REL_ID THEN T1.REL_ID ELSE T1.ID END), (CASE WHEN T1.ID > T1.REL_ID THEN T1.ID ELSE T1.REL_ID END) ORDER BY T1.TID)
			FROM    SUTTER_1P_MIGRATION.TBL.relationship_org_bak AS T1
					LEFT JOIN SUTTER_1P_MIGRATION.TBL.relationship_org T2 ON T1.TID = T2.TID
					WHERE T1.RE_DB_OwnerShort ='SMCF' 
					--AND T1.REL_ID IS NULL
					AND (T1.ID='SMCF-000001890' OR T1.REL_ID='SMCF-000001890')
			ORDER BY T1.ID ,
					 T1.REL_ID
					 ,RowNum

			
			SELECT  T1.TID,
					T1.ID AS T1_ID ,
					T1.REL_ID AS T1_REL_ID ,
					T2.ID AS T2_ID ,
					T2.REL_ID AS T2_REL_ID
			FROM    SUTTER_1P_MIGRATION.TBL.relationship_org_bak AS T1
					LEFT JOIN SUTTER_1P_MIGRATION.TBL.relationship_org T2 ON T1.TID = T2.TID
			WHERE	T1.ID='SMCF-000000038' OR T1.REL_ID='SMCF-0000000038' OR T1.ID='SMCF-000000373' OR T1.ID='SMCF-000017047' OR T1.ID='SMCF-000016488'
			ORDER BY T1.ID ,
					 T1.REL_ID

			
			SELECT * FROM SUTTER_1P_MIGRATION.TBL.relationship_org WHERE REL_ID IS NULL
				


						