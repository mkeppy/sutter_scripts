USE SUTTER_1P_MIGRATION
GO

BEGIN
		DROP TABLE SUTTER_1P_MIGRATION.IMP.TASK	
		
END
  
 
BEGIN
		--TASKS FROM CONS ACTION 
			SELECT DISTINCT 
				 CASE WHEN T7.OwnerID IS NULL THEN X3.ID ELSE T7.OwnerID END AS OwnerID
				,WhatId=CASE WHEN T1.KeyInd='O' THEN X1.ID END 
				,WhoId=CASE WHEN T1.KeyInd='I' THEN X2.ID END 
				,CAST(T.RE_DB_OwnerShort+'-'+T.ACImpID AS NVARCHAR(30)) AS  RE_ACImpID__c
				,T.ACDate AS ActivityDate
				,T.ACCat AS Type__c
				,T.ACStatus AS Archived_Status__c
				,T2.Move__c
				,T3.Stage__c
				,CASE WHEN T9.CANoteDesc IS NULL THEN T3.Stage__c ELSE T3.Stage__c +'. '+T9.CANoteDesc END AS [Subject]
				,T.RE_DB_OwnerShort AS Affiliation__c
				,CASE WHEN (T.ACDate < GETDATE()) THEN 'Completed' ELSE 'In Progress' END AS [Status]
				,CAST(T4.RE_Action_Solicitor__c  AS VARCHAR(4000)) AS RE_Action_Solicitor__c
				,CAST(T6.RE_Team_Member__c  AS VARCHAR(4000)) AS RE_Team_Member__c
				,CAST(T6.Thank_you_call__c  AS VARCHAR(4000)) AS Thank_you_call__c
				,CAST(T5.[Description] AS VARCHAR(4000)) AS [Description]
				,T.AddedBy AS RE_Action_Added_By__c
				,T.ACType AS Archived_Action_Type__c
				--reference
				,T1.KeyInd zref_KeyInd
				,T.AddedBy zref_AddedBy
				,NULL AS zrefSeq
		
			INTO SUTTER_1P_MIGRATION.IMP.TASK	
			FROM SUTTER_1P_DATA.DBO.HC_Cons_Action_v T
			INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.RE_DB_ID=T1.RE_DB_ID 
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_ActionStatus T2 ON T.ACStatus=T2.ACStatus AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_ActionType T3 ON T.ACType=T3.ACType AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Cons_Action_Solicitor_1 T4 ON T.ACImpID=T4.ACImpID AND T.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes_1 T5 ON T.ACImpID=T5.CALink AND T.RE_DB_OwnerShort=T5.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes_desc T9 ON T.ACImpID=T9.CALink AND T.RE_DB_OwnerShort=T9.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Attribute_task_1 T6 ON T.ACImpID=T6.ACImpId AND T.RE_DB_OwnerShort=T6.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.AssginedSolicitor_OwnerId T7 ON T.ACImpID=T7.ACImpID AND T.RE_DB_OwnerShort=T7.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_ActionSolicitor T8 ON T.AddedBy=T8.AddedBy AND T.RE_DB_OwnerShort=T8.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.XTR.ACCOUNT X1 ON T1.NEW_TGT_IMPORT_ID=X1.External_Id__c
			LEFT JOIN SUTTER_1P_MIGRATION.XTR.CONTACT X2 ON T1.NEW_TGT_IMPORT_ID=X2.External_Id__c
			LEFT JOIN SUTTER_1P_MIGRATION.XTR.USERS X3 ON T8.SF_User_Email=X3.Email
			WHERE T3.[Convert]='Yes'
			--748646
END; 
 

BEGIN
	--	SELECT [Description] FROM [SUTTER_1P_MIGRATION].[IMP].[TASK] WHERE [Description] LIKE '%"%';        
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET [Description]=REPLACE([Description],'"','''') where [Description] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET [Subject] =REPLACE([Subject],'"','''') where [Subject] LIKE '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET Type__c =REPLACE(Type__c,'"','''') where Type__c LIKE '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET RE_Action_Solicitor__c =REPLACE(RE_Action_Solicitor__c,'"','''') where RE_Action_Solicitor__c LIKE '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET RE_Team_Member__c =REPLACE(RE_Team_Member__c,'"','''') where RE_Team_Member__c LIKE '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET Thank_you_call__c =REPLACE(Thank_you_call__c,'"','''') where Thank_you_call__c LIKE '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] SET Archived_Action_Type__c =REPLACE(Archived_Action_Type__c,'"','''') where Archived_Action_Type__c LIKE '%"%'
END
 
BEGIN-- resolve duplicate ExternalId

		SELECT *
		FROM  [SUTTER_1P_MIGRATION].[IMP].[TASK] 
		WHERE RE_ACImpID__c IN (SELECT RE_ACImpID__c FROM SUTTER_1P_MIGRATION.IMP.TASK GROUP BY RE_ACImpID__c HAVING COUNT(*)>1)
		ORDER BY RE_ACImpID__c, OwnerID

		--update zrefSeq with seq #		
		UPDATE T
		SET zrefSeq = rn
		FROM ( 
			   SELECT zrefSeq,
					 ROW_NUMBER() OVER ( PARTITION BY RE_ACImpID__c ORDER BY RE_ACImpID__c, OwnerId) AS rn
			   FROM [SUTTER_1P_MIGRATION].[IMP].[TASK] 
	 		 )  T

		--update RE_ACTIMPID__c 
		SELECT OWnerID, RE_ACIMPID__c, zrefSeq
		FROM  [SUTTER_1P_MIGRATION].[IMP].[TASK]   
		WHERE RE_ACImpID__c IN (SELECT RE_ACImpID__c FROM SUTTER_1P_MIGRATION.IMP.TASK GROUP BY RE_ACImpID__c HAVING COUNT(*)>1)
		ORDER BY RE_ACImpID__c, OwnerID

		UPDATE [SUTTER_1P_MIGRATION].[IMP].[TASK] 
		SET RE_ACImpID__c= RE_ACImpID__c+'-'+CAST(zrefSeq AS nvarchar(3))
		WHERE RE_ACImpID__c IN (SELECT RE_ACImpID__c FROM SUTTER_1P_MIGRATION.IMP.TASK GROUP BY RE_ACImpID__c HAVING COUNT(*)>1)
		AND zrefSeq>1

END 
 

 SELECT RE_Action_Solicitor__c, LEN(RE_Action_Solicitor__c) l 
 FROM SUTTER_1P_MIGRATION.imp.task 
 ORDER BY l desc
 WHERE RE_ACImpID__c='PAMF-00001-504-0000012455'





---code
 		update T
		set cn = rn
		from (
			   select cn,
					  row_number() over(order by (select 1)) as rn
			   from TableX
			 ) T