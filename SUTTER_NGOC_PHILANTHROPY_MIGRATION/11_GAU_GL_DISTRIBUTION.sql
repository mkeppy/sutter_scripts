USE [SUTTER_1P_MIGRATION]
GO

	SELECT * FROM  [TBL].[GL_Mapping]

	BEGIN--GL mapping combo
		DROP TABLE TBL.GL_mapping_1
		
		SELECT [FundDistrGiftType], 'FundDistrCreditCompany' AS src_map, FundDistrCreditCompany AS 'src_value'
		INTO TBL.GL_mapping_1
		WHERE [Converts]='yes'AND FundDistrCreditCompany IS NOT NULL 
		UNION all
		SELECT [FundDistrGiftType], 'FundDistrCreditAccountingUnit' AS src_map, FundDistrCreditAccountingUnit AS 'src_value' 
		FROM [TBL].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrCreditAccountingUnit IS NOT null 
		UNION ALL 
		SELECT [FundDistrGiftType], 'FundDistrCreditAccount' AS src_map, FundDistrCreditAccount AS 'src_value' 
		FROM [TBL].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrCreditAccount IS NOT null 
		UNION ALL
		SELECT [FundDistrGiftType], 'FundDistrCreditSubAccount' AS src_map, FundDistrCreditSubAccount AS 'src_value' 
		FROM [TBL].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrCreditSubAccount IS NOT null 
		UNION ALL
		SELECT [FundDistrGiftType], 'FundDistrCreditActivity' AS src_map, FundDistrCreditActivity AS 'src_value' 
		FROM [TBL].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrCreditActivity IS NOT null 
		UNION ALL
		SELECT [FundDistrGiftType], 'FundDistrCreditCategory' AS src_map, FundDistrCreditCategory AS 'src_value' 
		FROM [TBL].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrCreditCategory IS NOT null 
		UNION ALL ---DEBIT
		SELECT [FundDistrGiftType], 'FundDistrDebitAccountingUnit' AS src_map, FundDistrDebitAccountingUnit AS 'src_value' 
		FROM [TBL].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrDebitAccountingUnit IS NOT null 
		UNION ALL ---DEBIT
		SELECT [FundDistrGiftType], 'FundDistrDebitAccount' AS src_map, FundDistrDebitAccount AS 'src_value' 
		FROM [TBL].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrDebitAccount IS NOT null 
		UNION ALL ---DEBIT
		SELECT [FundDistrGiftType], 'FundDistrDebitSubAccount' AS src_map, FundDistrDebitSubAccount AS 'src_value' 
		FROM [TBL].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrDebitSubAccount IS NOT null 
 		UNION ALL ---DEBIT
		SELECT [FundDistrGiftType], 'FundDistrDebitActivity' AS src_map, FundDistrDebitActivity AS 'src_value' 
		FROM [TBL].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrDebitActivity IS NOT null 
 		UNION ALL ---DEBIT
		SELECT [FundDistrGiftType], 'FundDistrDebitCategory' AS src_map, FundDistrDebitCategory AS 'src_value' 
		FROM [TBL].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrDebitCategory IS NOT null 
	END		
		
    	SELECT * FROM  TBL.GL_mapping_1
		ORDER BY [FundDistrGiftType]

	BEGIN--GL DISTRIBUTION  
		DROP TABLE [TBL].[GL_Distribution_1]
	
		SELECT [FundID], FundDistrCreditCompany, [FundDistrGiftType], 'FundDistrCreditAccountingUnit' AS src_dist, FundDistrCreditAccountingUnit AS src_dist_val 
		INTO [TBL].[GL_Distribution_1]
		FROM  [TBL].[GL_Distribution]	
		WHERE FundDistrCreditAccountingUnit IS NOT NULL    
		UNION ALL
		SELECT [FundID], FundDistrCreditCompany,  [FundDistrGiftType], 'FundDistrCreditAccount' AS src_dist, FundDistrCreditAccount AS src_dist_val 
		FROM  [TBL].[GL_Distribution]	
		WHERE FundDistrCreditAccount IS NOT NULL    
		UNION ALL
		SELECT [FundID], FundDistrCreditCompany, [FundDistrGiftType], 'FundDistrCreditSubAccount' AS src_dist, FundDistrCreditSubAccount AS src_dist_val 
		FROM  [TBL].[GL_Distribution]	
		WHERE FundDistrCreditSubAccount IS NOT NULL  
		UNION ALL
		SELECT [FundID], FundDistrCreditCompany, [FundDistrGiftType], 'FundDistrCreditActivity' AS src_dist, FundDistrCreditActivity AS src_dist_val 
		FROM  [TBL].[GL_Distribution]	
		WHERE FundDistrCreditActivity IS NOT NULL 
		UNION ALL
		SELECT [FundID], FundDistrCreditCompany, [FundDistrGiftType], 'FundDistrCreditCategory' AS src_dist, FundDistrCreditCategory AS src_dist_val 
		FROM  [TBL].[GL_Distribution]	
		WHERE FundDistrCreditCategory IS NOT NULL 
		UNION ALL --DEBIT
		SELECT [FundID], FundDistrCreditCompany, [FundDistrGiftType], 'FundDistrDebitAccountingUnit' AS src_dist, FundDistrDebitAccountingUnit AS src_dist_val 
		FROM  [TBL].[GL_Distribution]	
		WHERE FundDistrDebitAccountingUnit IS NOT NULL 
		UNION ALL --DEBIT
		SELECT [FundID], FundDistrCreditCompany, [FundDistrGiftType], 'FundDistrDebitAccount' AS src_dist, FundDistrDebitAccount AS src_dist_val 
		FROM  [TBL].[GL_Distribution]	
		WHERE FundDistrDebitAccount IS NOT NULL 
		UNION ALL --DEBIT
		SELECT [FundID], FundDistrCreditCompany, [FundDistrGiftType], 'FundDistrDebitSubAccount' AS src_dist, FundDistrDebitSubAccount AS src_dist_val 
		FROM  [TBL].[GL_Distribution]	
		WHERE FundDistrDebitSubAccount IS NOT NULL 
		UNION ALL --DEBIT
		SELECT [FundID], FundDistrCreditCompany, [FundDistrGiftType], 'FundDistrDebitActivity' AS src_dist, FundDistrDebitActivity AS src_dist_val 
		FROM  [TBL].[GL_Distribution]	
		WHERE FundDistrDebitActivity IS NOT NULL 
		UNION ALL --DEBIT
		SELECT [FundID], FundDistrCreditCompany, [FundDistrGiftType], 'FundDistrDebitCategory' AS src_dist, FundDistrDebitCategory AS src_dist_val 
		FROM  [TBL].[GL_Distribution]	
		WHERE FundDistrDebitCategory IS NOT NULL 
	END 
  	
		SELECT * FROM [TBL].[GL_Distribution_1]	 
		WHERE  fundId='CPMC-09WisheWelln'
		ORDER BY [FundDistrGiftType]

   	BEGIN--GL distribution 2 
		DROP TABLE [TBL].[GL_Distribution_2]
	
		SELECT T1.FundID, T1.[FundDistrGiftType], T1.src_dist , T2.src_value, T1.src_dist_val
		INTO [TBL].[GL_Distribution_2] 
		FROM [TBL].[GL_Distribution_1] AS T1
		INNER JOIN TBL.GL_Mapping_1 AS T2 ON T1.[FundDistrGiftType]=T2.[FundDistrGiftType]
												  AND T1.src_dist =t2.src_map
		--WHERE T1.fundId='CPMC-09WisheWelln'
		ORDER BY T1.FundId, T1.[FundDistrGiftType]
	END 

		SELECT * FROM [TBL].[GL_Distribution_1]	 
		WHERE  fundId='CPMC-09WisheWelln'  AND [FundDistrGiftType]='Pledge - Write Off'
		ORDER BY src_dist
	
		SELECT * FROM TBL.GL_mapping_1
		WHERE [FundDistrGiftType]='Pledge - Write Off'
		ORDER BY src_map
    
		SELECT * FROM [TBL].[GL_Distribution_1]	 
		WHERE  fundId='CPMC-09WisheWelln'  AND [FundDistrGiftType]='Pledge - Write Off'
		ORDER BY src_dist
	
		SELECT * FROM TBL.GL_mapping_1
		WHERE [FundDistrGiftType]='Pledge - Write Off'
		ORDER BY src_map


		SELECT * FROM [TBL].[GL_Distribution_2]	 
		WHERE  fundId='CPMC-09WisheWelln'  AND [FundDistrGiftType]='Pledge - Write Off'

		BEGIN--PHONES_5: CREATE final pivot phone tables --PHONE TYPES from SF_Field_API
			
			DROP TABLE SUTTER_1P_MIGRATION.TBL.[GL_Distribution_final]  
			
			EXEC HC_PivotWizard_p   'FundId',	--fields to include as unique identifier/normally it is just a unique ID field.
										'src_value',									--column that stores all new phone types
										'src_dist_val',										--phone numbers
										'SUTTER_1P_MIGRATION.TBL.[GL_Distribution_final]',	--INTO..     
										'SUTTER_1P_MIGRATION.TBL.[GL_Distribution_2]',		--FROM..
										'src_dist_val is not null'							--WHERE..
	 	END --332009
     
	 SELECT * FROM SUTTER_1P_MIGRATION.TBL.[GL_Distribution_final]
	 ORDER BY FundId