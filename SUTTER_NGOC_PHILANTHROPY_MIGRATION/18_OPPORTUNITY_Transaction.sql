USE [SUTTER_1P_MIGRATION] 
GO  

BEGIN
		DROP TABLE SUTTER_1P_MIGRATION.TBL.OPPORTUNITY_TRANSACTION
		DROP TABLE SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_TRANSACTION
END
		 
BEGIN --TRANSACTIONS OPPORTUNITIES from NON_PAYMENTS (Cash/Other/Stock, etc) 
						SELECT  
							dbo.fnc_OwnerID() AS OwnerID         
		/*EXT_ID*/			,T.[ACCOUNT:External_Id__c]
							,'T-'+T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS External_Id__c
							,'T-'+T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS RE_GSplitImpID__c
							,'T-'+T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitGFImpID AS RE_GFImpID__c
		/*EXT_ID*/			     ,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS [rC_Giving__Parent__r:External_ID__c]
 							,CASE   WHEN (T.[GFType] ='MG Pay-Cash' OR T.[GFType]='Pay-Cash') 
									THEN T.[rC_Giving__Affiliation__c]+' - Installment - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10)) 
									ELSE T.[rC_Giving__Affiliation__c]+' - Transaction - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10))
							END AS NAME
  							,CAST(T.GFDate AS DATE) AS	CloseDate
							,T.[GFDate] AS Gift_Date__c
							,dbo.fnc_RecordType('Opportunity_'+T.Child_Giving_RecordType) AS RecordTypeID
							,'Completed' AS StageName
							,CAST(T.GSplitAmt AS DECIMAL(12,2)) AS Amount 
							,CAST(T.GSplitAmt AS DECIMAL(12,2)) AS rC_Giving__Current_Giving_Amount__c
 							,'Payment' AS rC_Giving__Transaction_Type__c
							,T.[rC_Giving__Affiliation__c]
							,T.[Fundraising_Program__c]
							,T.rC_Giving__Is_Giving_Transaction__c
							,'FALSE' AS rC_Giving__Is_Payment_Refunded__c
						--payment method
							,T.[rC_Giving__Payment_Type__c] AS rC_Giving__Payment_Method__c
		/*EXT_ID*/			,T.[PAYMENT_METHOD_External_ID__c] AS [rC_Giving__Payment_Method_Selected__r:External_ID__c]
						--allocations
							,NULL AS  rC_Giving__Allocation_Amount__c   --Only populate with total gift amount when using Special Allocations.
		/*EXT_ID*/			,NULL AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]	 
						--RE gift_v
							,T.RE_Legacy_Financial_Code__c
							,T.BATCH_NUMBER AS RE_Batch_Number__c
							,CASE WHEN T.GFAck='Acknowledged' THEN 'TRUE' ELSE 'FALSE' END	AS rC_Giving__Acknowledged__c
							,T.GFAckDate	AS	rC_Giving__Acknowledged_Date__c
							,T.GFAnon	AS	rC_Giving__Is_Anonymous__c
							,T.GFCheckDate	AS	rC_Giving__Check_Date__c
							,T.GFCheckNum	AS	rC_Giving__Check_Number__c
							,T.GFRef	AS	[Description]
							,T.GFStkIss	AS	Stock_Issuer__c
							,T.GFStkMedPrice	AS	Stock_Median_Price__c
							,T.GFStkNumUnits	AS	rC_Giving__Number_Of_Shares__c
							,T.GFStkSymbol	AS	rC_Giving__Ticker_Symbol__c
							,CASE WHEN T.Production__c IS NULL THEN T2.[Production] ELSE T.Production__c END AS Production__c
							,T.DateAdded	AS	CreatedDate
							,T.GFConsDesc AS RE_Gift_Constituency_Code__c
							,T.GFLtrCode AS RE_Letter_Code__c
							,T.GFPayMeth AS RE_GFPayMeth__c
							,T.GFPledgeBalance AS RE_Pledge_Balance__c
							,T.[GFType] AS RE_Gift_Type__c
							,T.RE_Gift_SubType__c
  						--attributes (chart-attributes)
							,T.DAF_Pledge_Payment__c
							,T.Donation_Page_Name__c
							,T.Donor_Recognition_Name__c
							,T.Gift_Comments__c
							,T.NCHX_Source_Entity__c
							,T.Non_Donation_Payment__c
							,T.Original_Pledge_Amount__c
							,T.Plaque_Wording__c
							,T.Research_Payment__c
							,T.Transaction_ID__c
							,T.Tribute_In_Honor_of__c
							,T.Tribute_In_Memory_of__c
 						--gift attribute (chart_giftattribute)
							,T2.[# Hours Per Period] AS Hours_Per_Period__c
							,T2.[Anticipated Payment Type] AS Anticipated_Payment_Type__c	
							,T2.[Considering bequest in] AS Considering_bequest_in__c
							,T2.[Donor Designation-Fund] AS Donor_Designation_Fund__c
							,T2.[Exceed Appeal] AS Exceed_Appeal__c
							,T2.[Exceed Batch Number] AS Exceed_Batch_Number__c
							,T2.[Exceed Campaign] AS Exceed_Campaign__c
							,T2.[Former Appeal] AS Former_Appeal__c
							,T2.[Former Campaign] AS Former_Campaign__c
							,T2.[Former ConCode] AS Former_ConCode__c
							,T2.[Former Fund] AS Former_Fund__c
							,T2.[Former Package] AS Former_Package__c
							,T2.[Gift-in-Kind] AS Gift_in_kind__c
							,T2.[Hour Club] AS Hour_Club__c
							,T2.[Irregular] AS Irregular__c
							,T2.[Lights of Love FY14 RSVP] AS Lights_of_Love_FY14_RSVP__c
							,T2.[Physician Engagement] AS Physician_Engagement__c
							,T2.[Recogntion] AS Recognition__c
							,T2.[Report Exclusion] AS Report_Exclusion__c
							,T2.[Selected Naming Opportunity] AS Selected_Naming_Opporunity__c
							,T2.[Sobrato Challenge?] AS Sobrato_Challenge__c
							,T2.[Source of notification] AS Source_of_notification__c
							,T2.[SphereGiftIdentifier] AS SphereGiftIdentifier__c
 						--MISC.
							,T.RE_Split_Gift__c
							,T.GiftSubType__c
							,T.GFPostDate__c  AS Post_Date__c    --renamed for 1P2T
							,T.GFPostStatus__c AS Posted_Status__c --renamed for 1P2T
	 						,T3.[Adjusted_Date__c]
						    ,T3.[Adjustment_Post_Date__c]
							,T3.[Adjustment_Post_Status__c]
					 		,'FALSE' AS Employee_Recurring_Gift__c	
						--reference
							--,X1.ID zrefAccountId
							,T.GFType AS zrefGFType
							,T.GSplitGFImpID AS zrefParentGFImpID
							,T.GSplitGFImpID AS zrefPaymentGFImpID
							,T.GSplitImpID AS zrefPaymentGSplitImpID
							,T.[GSplitRE_DB_OwnerShort] AS zrefRE_DB_Owner
							,NULL zrefGLinkAmt
	 
						INTO SUTTER_1P_MIGRATION.TBL.OPPORTUNITY_TRANSACTION
						FROM SUTTER_1P_MIGRATION.TBL.GIFT T
			 			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Gift_Attribute_1 T2 ON T.GSplitGFImpID = T2.GFIMPID AND T.GSplitRE_DB_OwnerShort=T2.RE_DB_OwnerShort
						LEFT JOIN [SUTTER_1P_MIGRATION].TBL.GIFT_ADJUSTED_DATE T3 ON T.[refGFImpID]=T3.GFImpID AND T.[GSplitRE_DB_OwnerShort]=T3.RE_DB_OwnerShort
   	 					LEFT JOIN [SUTTER_1P_MIGRATION].TBL.GiftAttr_employee_recurring T5 ON  T.GSplitGFImpID = T5.GFIMPID AND T.GSplitRE_DB_OwnerShort=T5.RE_DB_OwnerShort  --DNC recurring with Employee Recurring Attribute

	/*transaction*/		WHERE T.Transaction_Source='NON_PAYMENT'  
						AND  (T5.GFImpID IS NULL AND T5.RE_DB_OwnerShort IS NULL) -- -- DNC recurring from employee giving attribute 
					 
					UNION ALL

			--TRANSACTIONS OPPORTUNITIES from PAYMENTS (Pay-Cash/Pay-Other/Pay-*) 
					SELECT  
						dbo.fnc_OwnerID() AS OwnerID         
	/*EXT_ID*/			,T.[ACCOUNT:External_Id__c]
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS External_Id__c
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS RE_GSplitImpID__c
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitGFImpID AS RE_GFImpID__c
	/*EXT_ID*/			,T2.[GSplitRE_DB_OwnerShort]+'-'+T2.[GSplitRE_DB_Tbl]+'-'+T2.GSplitImpID AS [rC_Giving__Parent__r:External_ID__c]

							,CASE   WHEN (T.[GFType] ='MG Pay-Cash' OR T.[GFType]='Pay-Cash') 
									THEN T.[rC_Giving__Affiliation__c]+' - Installment - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10)) 
									ELSE T.[rC_Giving__Affiliation__c]+' - Transaction - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10))
							END AS NAME

						,CAST(T.GFDate AS DATE) AS	CloseDate
						,T.[GFDate] AS Gift_Date__c
						,dbo.fnc_RecordType('Opportunity_'+T.Child_Giving_RecordType) AS RecordTypeID
						,'Completed' AS StageName
						,CAST(T.GSplitAmt AS DECIMAL(12,2)) AS Amount
						,CAST(T.GSplitAmt AS DECIMAL(12,2)) AS  rC_Giving__Current_Giving_Amount__c 
					
						,'Payment' AS rC_Giving__Transaction_Type__c
						,T.[rC_Giving__Affiliation__c]
						,T.[Fundraising_Program__c]
						,T.rC_Giving__Is_Giving_Transaction__c
						,'FALSE' AS rC_Giving__Is_Payment_Refunded__c
					--payment method
						,T.[rC_Giving__Payment_Type__c] AS rC_Giving__Payment_Method__c
	/*EXT_ID*/			,T.[PAYMENT_METHOD_External_ID__c] AS [rC_Giving__Payment_Method_Selected__r:External_ID__c]
					--allocations
						,NULL AS  rC_Giving__Allocation_Amount__c   --Only populate with total gift amount when using Special Allocations.
	/*EXT_ID*/			,NULL AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]	 
					--RE gift_v
						,T.RE_Legacy_Financial_Code__c
						,T.BATCH_NUMBER AS RE_Batch_Number__c
						,CASE WHEN T.GFAck='Acknowledged' THEN 'TRUE' ELSE 'FALSE' END	AS rC_Giving__Acknowledged__c
						,T.GFAckDate	AS	rC_Giving__Acknowledged_Date__c
						,T.GFAnon	AS	rC_Giving__Is_Anonymous__c
						,T.GFCheckDate	AS	rC_Giving__Check_Date__c
						,T.GFCheckNum	AS	rC_Giving__Check_Number__c
						,T.GFRef	AS	[Description]
						,T.GFStkIss	AS	Stock_Issuer__c
						,T.GFStkMedPrice	AS	Stock_Median_Price__c
						,T.GFStkNumUnits	AS	rC_Giving__Number_Of_Shares__c
						,T.GFStkSymbol	AS	rC_Giving__Ticker_Symbol__c
						,CASE WHEN T.Production__c IS NULL THEN T3.[Production] ELSE T.Production__c END AS Production__c
						,T.DateAdded	AS	CreatedDate
						,T.GFConsDesc AS RE_Gift_Constituency_Code__c
						,T.GFLtrCode AS RE_Letter_Code__c
						,T.GFPayMeth AS RE_GFPayMeth__c
						,T.GFPledgeBalance AS RE_Pledge_Balance__c
						,T.[GFType] AS RE_Gift_Type__c
						,T.RE_Gift_SubType__c

					--attributes (chart-attributes)
						,T.DAF_Pledge_Payment__c
						,T.Donation_Page_Name__c
						,T.Donor_Recognition_Name__c
						,T.Gift_Comments__c
						,T.NCHX_Source_Entity__c
						,T.Non_Donation_Payment__c
						,T.Original_Pledge_Amount__c
						,T.Plaque_Wording__c
						,T.Research_Payment__c
						,T.Transaction_ID__c
						,T.Tribute_In_Honor_of__c
						,T.Tribute_In_Memory_of__c

					--gift attribute (chart_giftattribute)
						,T3.[# Hours Per Period] AS Hours_Per_Period__c
						,T3.[Anticipated Payment Type] AS Anticipated_Payment_Type__c	
						,T3.[Considering bequest in] AS Considering_bequest_in__c
						,T3.[Donor Designation-Fund] AS Donor_Designation_Fund__c
						,T3.[Exceed Appeal] AS Exceed_Appeal__c
						,T3.[Exceed Batch Number] AS Exceed_Batch_Number__c
						,T3.[Exceed Campaign] AS Exceed_Campaign__c
						,T3.[Former Appeal] AS Former_Appeal__c
						,T3.[Former Campaign] AS Former_Campaign__c
						,T3.[Former ConCode] AS Former_ConCode__c
						,T3.[Former Fund] AS Former_Fund__c
						,T3.[Former Package] AS Former_Package__c
						,T3.[Gift-in-Kind] AS Gift_in_kind__c
						,T3.[Hour Club] AS Hour_Club__c
						,T3.[Irregular] AS Irregular__c
						,T3.[Lights of Love FY14 RSVP] AS Lights_of_Love_FY14_RSVP__c
						,T3.[Physician Engagement] AS Physician_Engagement__c
						,T3.[Recogntion] AS Recognition__c
						,T3.[Report Exclusion] AS Report_Exclusion__c
						,T3.[Selected Naming Opportunity] AS Selected_Naming_Opporunity__c
						,T3.[Sobrato Challenge?] AS Sobrato_Challenge__c
						,T3.[Source of notification] AS Source_of_notification__c
						,T3.[SphereGiftIdentifier] AS SphereGiftIdentifier__c
			 	
					--MISC.
						,T.RE_Split_Gift__c
						,T.GiftSubType__c
						,T.GFPostDate__c  AS Post_Date__c    --renamed for 1P2T
						,T.GFPostStatus__c AS Posted_Status__c --renamed for 1P2T
 						,T4.[Adjusted_Date__c]
					    ,T4.[Adjustment_Post_Date__c]
						,T4.[Adjustment_Post_Status__c]
				 		,'FALSE' AS Employee_Recurring_Gift__c	 			
					--reference
					--,X1.ID zrefAccountId
						,T.GFType AS zrefGFType
						,T.GSplitGFImpID AS zrefParentGFImpID
						,T.GSplitGFImpID AS zrefPaymentGFImpID
						,T.GSplitImpID AS zrefPaymentGSplitImpID
						,T.[GSplitRE_DB_OwnerShort] AS zrefRE_DB_Owner
						,T1.GFPaymentAmount AS zrefGLinkAmt
	 
	 				FROM SUTTER_1P_DATA.dbo.HC_Gift_Link_v AS T1 
/*parent*/			INNER JOIN SUTTER_1P_MIGRATION.TBL.GIFT AS T2 ON T1.GFImpID = T2.GSplitGFImpID AND T1.RE_DB_OwnerShort = T2.GSplitRE_DB_OwnerShort
/*transaction*/		INNER JOIN SUTTER_1P_MIGRATION.TBL.GIFT AS T ON (T1.GFPaymentGFImpID = T.GSplitGFImpID AND T1.RE_DB_OwnerShort = T.GSplitRE_DB_OwnerShort
					AND T2.GFSplitSequence = T.GFSplitSequence)   --Using the GSplitSequence for matching split payments and pledges. 
					LEFT JOIN SUTTER_1P_MIGRATION.TBL.Gift_Attribute_1 T3 ON T.GSplitGFImpID = T3.GFIMPID AND T.GSplitRE_DB_OwnerShort=T3.RE_DB_OwnerShort
					LEFT JOIN [SUTTER_1P_MIGRATION].TBL.GIFT_ADJUSTED_DATE T4 ON T.[refGFImpID]=T4.GFImpID AND T.[GSplitRE_DB_OwnerShort]=T4.RE_DB_OwnerShort
	 				LEFT JOIN [SUTTER_1P_MIGRATION].TBL.GiftAttr_employee_recurring T5 ON  T.GSplitGFImpID = T5.GFIMPID AND T.GSplitRE_DB_OwnerShort=T5.RE_DB_OwnerShort  --DNC recurring with Employee Recurring Attribute
 /*transaction*/	WHERE T.Transaction_Source='PAYMENT' 
					AND (T5.GFImpID IS NULL AND T5.RE_DB_OwnerShort IS NULL)  -- DNC recurring from employee giving attribute 
					--219,559
 			UNION ALL 
 				--TRANSACTIONS FROM 'recurring gift 'EMPLOYEE giving. 
  						SELECT  
							dbo.fnc_OwnerID() AS OwnerID         
		/*EXT_ID*/			,T.[ACCOUNT:External_Id__c]
							,CAST('T-'+T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS VARCHAR(40)) AS External_Id__c
							,CAST('T-'+T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS VARCHAR(40)) AS RE_GSplitImpID__c
							,CAST('T-'+T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitGFImpID AS VARCHAR(40)) AS RE_GFImpID__c
		/*EXT_ID*/			,CAST(T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS VARCHAR(40)) AS [rC_Giving__Parent__r:External_ID__c]

							,CASE   WHEN (T.[GFType] ='MG Pay-Cash' OR T.[GFType]='Pay-Cash') 
									THEN T.[rC_Giving__Affiliation__c]+' - Installment - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10)) 
									ELSE T.[rC_Giving__Affiliation__c]+' - Transaction - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10))
							END AS NAME
							,CAST(T.GFDate AS DATE) AS	CloseDate
							,T.[GFDate] AS Gift_Date__c
							,dbo.fnc_RecordType('Opportunity_Transaction') AS RecordTypeID
							,'Completed' AS StageName
							,CAST(T.GSplitAmt AS DECIMAL(12,2)) AS Amount 
							,CAST(T.GSplitAmt AS DECIMAL(12,2)) AS rC_Giving__Current_Giving_Amount__c
					
							,'Payment' AS rC_Giving__Transaction_Type__c
							,T.[rC_Giving__Affiliation__c]
							,T.[Fundraising_Program__c]

							,T.rC_Giving__Is_Giving_Transaction__c
							,'FALSE' AS rC_Giving__Is_Payment_Refunded__c
						--payment method
							,T.[rC_Giving__Payment_Type__c] AS rC_Giving__Payment_Method__c
		/*EXT_ID*/			,T.[PAYMENT_METHOD_External_ID__c] AS [rC_Giving__Payment_Method_Selected__r:External_ID__c]
						--allocations
							,NULL AS  rC_Giving__Allocation_Amount__c   --Only populate with total gift amount when using Special Allocations.
		/*EXT_ID*/			,NULL AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]	 
						--RE gift_v
							,T.RE_Legacy_Financial_Code__c
							,T.BATCH_NUMBER AS RE_Batch_Number__c
							,CASE WHEN T.GFAck='Acknowledged' THEN 'TRUE' ELSE 'FALSE' END	AS rC_Giving__Acknowledged__c
							,T.GFAckDate	AS	rC_Giving__Acknowledged_Date__c
							,T.GFAnon	AS	rC_Giving__Is_Anonymous__c
							,T.GFCheckDate	AS	rC_Giving__Check_Date__c
							,T.GFCheckNum	AS	rC_Giving__Check_Number__c
							,T.GFRef	AS	[Description]
							,T.GFStkIss	AS	Stock_Issuer__c
							,T.GFStkMedPrice	AS	Stock_Median_Price__c
							,T.GFStkNumUnits	AS	rC_Giving__Number_Of_Shares__c
							,T.GFStkSymbol	AS	rC_Giving__Ticker_Symbol__c
							,CASE WHEN T.Production__c IS NULL THEN T2.[Production] ELSE T.Production__c END AS Production__c
							,T.DateAdded	AS	CreatedDate
							,T.GFConsDesc AS RE_Gift_Constituency_Code__c
							,T.GFLtrCode AS RE_Letter_Code__c
							,T.GFPayMeth AS RE_GFPayMeth__c
							,T.GFPledgeBalance AS RE_Pledge_Balance__c
							,T.[GFType] AS RE_Gift_Type__c
							,T.RE_Gift_SubType__c
	
 						--attributes (chart-attributes)
							,T.DAF_Pledge_Payment__c
							,T.Donation_Page_Name__c
							,T.Donor_Recognition_Name__c
							,T.Gift_Comments__c
							,T.NCHX_Source_Entity__c
							,T.Non_Donation_Payment__c
							,T.Original_Pledge_Amount__c
							,T.Plaque_Wording__c
							,T.Research_Payment__c
							,T.Transaction_ID__c
							,T.Tribute_In_Honor_of__c
							,T.Tribute_In_Memory_of__c

							--gift attribute (chart_giftattribute)
							,T2.[# Hours Per Period] AS Hours_Per_Period__c
							,T2.[Anticipated Payment Type] AS Anticipated_Payment_Type__c	
							,T2.[Considering bequest in] AS Considering_bequest_in__c
							,T2.[Donor Designation-Fund] AS Donor_Designation_Fund__c
							,T2.[Exceed Appeal] AS Exceed_Appeal__c
							,T2.[Exceed Batch Number] AS Exceed_Batch_Number__c
							,T2.[Exceed Campaign] AS Exceed_Campaign__c
							,T2.[Former Appeal] AS Former_Appeal__c
							,T2.[Former Campaign] AS Former_Campaign__c
							,T2.[Former ConCode] AS Former_ConCode__c
							,T2.[Former Fund] AS Former_Fund__c
							,T2.[Former Package] AS Former_Package__c
							,T2.[Gift-in-Kind] AS Gift_in_kind__c
							,T2.[Hour Club] AS Hour_Club__c
							,T2.[Irregular] AS Irregular__c
							,T2.[Lights of Love FY14 RSVP] AS Lights_of_Love_FY14_RSVP__c
							,T2.[Physician Engagement] AS Physician_Engagement__c
							,T2.[Recogntion] AS Recognition__c
							,T2.[Report Exclusion] AS Report_Exclusion__c
							,T2.[Selected Naming Opportunity] AS Selected_Naming_Opporunity__c
							,T2.[Sobrato Challenge?] AS Sobrato_Challenge__c
							,T2.[Source of notification] AS Source_of_notification__c
							,T2.[SphereGiftIdentifier] AS SphereGiftIdentifier__c
					 
						--MISC.
							,T.RE_Split_Gift__c
							,T.GiftSubType__c
							,T.GFPostDate__c  AS Post_Date__c    --renamed for 1P2T
							,T.GFPostStatus__c AS Posted_Status__c --renamed for 1P2T
 							,T3.[Adjusted_Date__c]
							,T3.[Adjustment_Post_Date__c]
							,T3.[Adjustment_Post_Status__c]
							,'TRUE' AS Employee_Recurring_Gift__c	--GAttrDesc = true
						--reference
							--,X1.ID zrefAccountId
							,T.GFType AS zrefGFType
							,T.GSplitGFImpID AS zrefParentGFImpID
							,T.GSplitGFImpID AS zrefPaymentGFImpID
							,T.GSplitImpID AS zrefPaymentGSplitImpID
							,T.[GSplitRE_DB_OwnerShort] AS zrefRE_DB_Owner
							,NULL zrefGLinkAmt
					
							--,LEN(X1.Name +': '+ T.[GSplitRE_DB_OwnerShort]+' '+T.Child_Giving_RecordType+' Completed $'+
							--CAST(CAST(T.GSplitAmt AS DECIMAL(12,2))AS NVARCHAR(20))  +' '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10))) AS zrefNameLen

				 		FROM SUTTER_1P_MIGRATION.TBL.GIFT T
	 					INNER JOIN [SUTTER_1P_MIGRATION].TBL.GiftAttr_employee_recurring T5 ON  T.GSplitGFImpID = T5.GFIMPID AND T.GSplitRE_DB_OwnerShort=T5.RE_DB_OwnerShort  --create only for Employee Recurring Attribute
						LEFT JOIN SUTTER_1P_MIGRATION.TBL.Gift_Attribute_1 T2 ON T.GSplitGFImpID = T2.GFIMPID AND T.GSplitRE_DB_OwnerShort=T2.RE_DB_OwnerShort
						LEFT JOIN [SUTTER_1P_MIGRATION].TBL.GIFT_ADJUSTED_DATE T3 ON T.[refGFImpID]=T3.GFImpID AND T.[GSplitRE_DB_OwnerShort]=T3.RE_DB_OwnerShort
						WHERE ( T5.[GFType]='Cash'
						    OR T5.[GFType]='Other'
			 				OR T5.[GFType]='Recurring Gift Pay-Cash'  )
	 
				UNION ALL 
				--WRITE OFFs 
				SELECT  
					dbo.fnc_OwnerID() AS OwnerID         
/*EXT_ID*/			,T1.[ACCOUNT:External_Id__c]
					,T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.GFImpID AS External_Id__c
					,T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.GFImpID AS RE_GSplitImpID__c
					,T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.GFImpID AS RE_GFImpID__c
					,T3.RE_DB_OwnerShort+'-'+T3.RE_DB_Tbl+'-'+T3.GSplitImpID AS [rC_Giving__Parent__r:External_ID__c]

					,T1.RE_DB_OwnerShort+' - Transaction-Uncollectible - '+CAST(CAST(T1.GFDate AS DATE)AS NVARCHAR(10))AS NAME	
					,CAST(T1.GFDate AS DATE) AS	CloseDate
					,T1.[GFDate] AS Gift_Date__c
					,dbo.fnc_RecordType('Opportunity_Transaction') AS RecordTypeID
					,'Uncollectible' AS StageName
					,CAST(T1.GFTAmt AS DECIMAL(12,2)) AS Amount 
					,CAST(T1.GFTAmt AS DECIMAL(12,2)) AS rC_Giving__Current_Giving_Amount__c

					,'Payment' AS rC_Giving__Transaction_Type__c
					,T1.RE_DB_OwnerShort AS rC_Giving__Affiliation__c 
					,NULL AS [Fundraising_Program__c]


					,'TRUE' rC_Giving__Is_Giving_Transaction__c
					,NULL AS rC_Giving__Is_Payment_Refunded__c
					--payment method
						,NULL AS rC_Giving__Payment_Method__c
						,NULL AS [rC_Giving__Payment_Method_Selected__r:External_ID__c]
					--allocations
						,NULL AS  rC_Giving__Allocation_Amount__c   --Only populate with total gift amount when using Special Allocations.
						,NULL AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]		 
					--RE gift_v
						,NULL AS RE_Legacy_Financial_Code__c
						,NULL AS RE_Batch_Number__c
						,NULL AS rC_Giving__Acknowledged__c
						,NULL AS rC_Giving__Acknowledged_Date__c
						,NULL AS rC_Giving__Is_Anonymous__c
						,NULL AS rC_Giving__Check_Date__c
						,NULL AS rC_Giving__Check_Number__c
						,NULL AS [Description]
						,NULL AS Stock_Issuer__c
						,NULL AS Stock_Median_Price__c
						,NULL AS rC_Giving__Number_Of_Shares__c
						,NULL AS rC_Giving__Ticker_Symbol__c
						,NULL AS Production__c
						,T1.DateAdded	AS	CreatedDate
						,NULL AS RE_Gift_Constituency_Code__c
						,NULL as RE_Letter_Code__c 
						,NULL AS RE_GFPayMeth__c
						,NULL AS RE_Pledge_Balance__c
						,NULL AS RE_Gift_Type__c
						,NULL AS RE_Gift_SubType__c

					--attributes (chart-attributes)
						,NULL AS DAF_Pledge_Payment__c
						,NULL AS Donation_Page_Name__c
						,NULL AS Donor_Recognition_Name__c
						,NULL AS Gift_Comments__c
						,NULL AS NCHX_Source_Entity__c
						,NULL AS Non_Donation_Payment__c
						,NULL AS Original_Pledge_Amount__c
						,NULL AS Plaque_Wording__c
						,NULL AS Research_Payment__c
						,NULL AS Transaction_ID__c
						,NULL AS Tribute_In_Honor_of__c
						,NULL AS Tribute_In_Memory_of__c

					--gift attribute (chart_giftattribute)
						, NULL AS Hours_Per_Period__c
						, NULL AS Anticipated_Payment_Type__c	
						, NULL AS Considering_bequest_in__c
						, NULL AS Donor_Designation_Fund__c
						, NULL AS Exceed_Appeal__c
						, NULL AS Exceed_Batch_Number__c
						, NULL AS Exceed_Campaign__c
						, NULL AS Former_Appeal__c
						, NULL AS Former_Campaign__c
						, NULL AS Former_ConCode__c
						, NULL AS Former_Fund__c
						, NULL AS Former_Package__c
						, NULL AS Gift_in_kind__c
						, NULL AS Hour_Club__c
						, NULL AS Irregular__c
						, NULL AS Lights_of_Love_FY14_RSVP__c
						, NULL AS Physician_Engagement__c
						, NULL AS Recognition__c
						, NULL AS Report_Exclusion__c
						, NULL AS Selected_Naming_Opporunity__c
						, NULL AS Sobrato_Challenge__c
						, NULL AS Source_of_notification__c
						, NULL AS SphereGiftIdentifier__c
			  
					--MISC.
						, NULL AS RE_Split_Gift__c
						, NULL AS GiftSubType__c
						, NULL AS Post_Date__c    --renamed for 1P2T
						, NULL AS Posted_Status__c --renamed for 1P2T
 						, NULL AS [Adjusted_Date__c]
					    , NULL AS [Adjustment_Post_Date__c]
						, NULL AS [Adjustment_Post_Status__c]
				 		, 'FALSE' AS Employee_Recurring_Gift__c	
					--reference
						--,X1.ID AS zrefAccountId
						,T1.GFType AS zrefGFType
						,T1.GFLink AS zrefParentGFImpID
						,T1.GFImpID AS zrefPaymentGFImpID
						,T3.GSplitImpID AS zrefPaymentGSplitImpID
						,T3.RE_DB_OwnerShort AS zrefRE_DB_Owner
						, NULL AS zrefGLinkAmt
						--,LEN(X1.Name +'- '+ T1.RE_DB_OwnerShort+'- Transaction- Uncollectible- $'+
	    				--	  CAST(CAST(T1.GFTAmt AS DECIMAL(12,2))AS NVARCHAR(20))+'- '+ 
	    				--	  CAST(CAST(T1.GFDate AS DATE)AS NVARCHAR(10))) AS zrefNameLen
	    				  
				FROM SUTTER_1P_MIGRATION.TBL.GIFT_WRITEOFF T1
/*Parent ID*/	INNER JOIN SUTTER_1P_DATA.DBO.HC_Gift_SplitGift_v T3 ON T1.GFLink=T3.GFImpID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort  
				--LEFT JOIN SUTTER_1P_MIGRATION.XTR.ACCOUNT X1 ON T1.[ACCOUNT:External_Id__c]=X1.EXTERNAL_ID__C 
		 
			UNION ALL
			
			--NEW OPEN TRANSCTION FROM GIFT INSTALLMENTS>>>
				SELECT  
					dbo.fnc_OwnerID() AS OwnerID         
/*EXT_ID*/			,T1.[ACCOUNT:External_Id__c]
					,CAST((T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.GFInsImpID) AS VARCHAR(35)) AS External_Id__c
					,NULL AS RE_GSplitImpID__c
					,NULL AS RE_GFImpID__c
					,CAST((T1.GSplitRE_DB_OwnerShort+'-'+T1.GSplitRE_DB_Tbl+'-'+T1.GSplitImpID) AS VARCHAR(35))  AS [rC_Giving__Parent__r:External_ID__c]

				 
					,T.RE_DB_OwnerShort+' - Installment - '+CAST(CAST(T.GFInsDate AS DATE)AS NVARCHAR(10))AS NAME	
					,CAST(T.GFInsDate AS DATE) AS	CloseDate
					,NULL AS Gift_Date__c
					,dbo.fnc_RecordType('Opportunity_Transaction') AS RecordTypeID
					,'Open' AS StageName
					,CAST(T.GFInsBalance AS DECIMAL(12,2)) AS Amount 
					,CAST(T.GFInsBalance AS DECIMAL(12,2)) AS rC_Giving__Current_Giving_Amount__c

					,'Payment' AS rC_Giving__Transaction_Type__c
					,T.RE_DB_OwnerShort AS rC_Giving__Affiliation__c 
					,NULL AS [Fundraising_Program__c]


					,'TRUE' rC_Giving__Is_Giving_Transaction__c
					,NULL AS rC_Giving__Is_Payment_Refunded__c
					--payment method
 					,T1.[rC_Giving__Payment_Type__c] AS rC_Giving__Payment_Method__c
	/*EXT_ID*/		,T1.[PAYMENT_METHOD_External_ID__c] AS [rC_Giving__Payment_Method_Selected__r:External_ID__c]
					--allocations
						,NULL AS  rC_Giving__Allocation_Amount__c   --Only populate with total gift amount when using Special Allocations.
						,NULL AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]		 
					--RE gift_v
						,NULL AS RE_Legacy_Financial_Code__c
						,NULL AS RE_Batch_Number__c
						,NULL AS rC_Giving__Acknowledged__c
						,NULL AS rC_Giving__Acknowledged_Date__c
						,NULL AS rC_Giving__Is_Anonymous__c
						,NULL AS rC_Giving__Check_Date__c
						,NULL AS rC_Giving__Check_Number__c
						,NULL AS [Description]
						,NULL AS Stock_Issuer__c
						,NULL AS Stock_Median_Price__c
						,NULL AS rC_Giving__Number_Of_Shares__c
						,NULL AS rC_Giving__Ticker_Symbol__c
						,NULL AS Production__c
						,NULL AS	CreatedDate
						,NULL AS RE_Gift_Constituency_Code__c
						,NULL as RE_Letter_Code__c 
						,NULL AS RE_GFPayMeth__c
						,NULL AS RE_Pledge_Balance__c
						,NULL AS RE_Gift_Type__c
						,NULL AS RE_Gift_SubType__c

					--attributes (chart-attributes)
						,NULL AS DAF_Pledge_Payment__c
						,NULL AS Donation_Page_Name__c
						,NULL AS Donor_Recognition_Name__c
						,NULL AS Gift_Comments__c
						,NULL AS NCHX_Source_Entity__c
						,NULL AS Non_Donation_Payment__c
						,NULL AS Original_Pledge_Amount__c
						,NULL AS Plaque_Wording__c
						,NULL AS Research_Payment__c
						,NULL AS Transaction_ID__c
						,NULL AS Tribute_In_Honor_of__c
						,NULL AS Tribute_In_Memory_of__c

					--gift attribute (chart_giftattribute)
						, NULL AS Hours_Per_Period__c
						, NULL AS Anticipated_Payment_Type__c	
						, NULL AS Considering_bequest_in__c
						, NULL AS Donor_Designation_Fund__c
						, NULL AS Exceed_Appeal__c
						, NULL AS Exceed_Batch_Number__c
						, NULL AS Exceed_Campaign__c
						, NULL AS Former_Appeal__c
						, NULL AS Former_Campaign__c
						, NULL AS Former_ConCode__c
						, NULL AS Former_Fund__c
						, NULL AS Former_Package__c
						, NULL AS Gift_in_kind__c
						, NULL AS Hour_Club__c
						, NULL AS Irregular__c
						, NULL AS Lights_of_Love_FY14_RSVP__c
						, NULL AS Physician_Engagement__c
						, NULL AS Recognition__c
						, NULL AS Report_Exclusion__c
						, NULL AS Selected_Naming_Opporunity__c
						, NULL AS Sobrato_Challenge__c
						, NULL AS Source_of_notification__c
						, NULL AS SphereGiftIdentifier__c
		 			--MISC.
						, NULL AS RE_Split_Gift__c
						, NULL AS GiftSubType__c
						, NULL AS Post_Date__c    --renamed for 1P2T
						, NULL AS Posted_Status__c --renamed for 1P2T
 						, NULL AS [Adjusted_Date__c]
					    , NULL AS [Adjustment_Post_Date__c]
						, NULL AS [Adjustment_Post_Status__c]
		 				, 'FALSE' AS Employee_Recurring_Gift__c	
		 
					--reference
						--,X1.ID AS zrefAccountId
						,T1.GFType AS zrefGFType
						,T.GFLink AS zrefParentGFImpID
						,T.GFInsImpID AS zrefPaymentGFImpID
						,NULL  AS zrefPaymentGSplitImpID
						,T.RE_DB_OwnerShort AS zrefRE_DB_Owner
						, NULL AS zrefGLinkAmt
		 		 
				FROM [SUTTER_1P_DATA].DBO.[HC_Gift_Installments_v] T
/*Parent ID*/	INNER JOIN [SUTTER_1P_MIGRATION].TBL.GIFT T1 ON T.GFLink=T1.GSplitGFImpID AND T.RE_DB_OwnerShort=T1.GSplitRE_DB_OwnerShort  
				WHERE T.[GFInsBalance]>0
	 

		---TRANSACTIONS OPPORTUNITIES from PAYMENTS (Pay-Cash/Pay-Other/Pay-*) 
			--paymnents missing from split payment query
					INSERT INTO SUTTER_1P_MIGRATION.[TBL].[OPPORTUNITY_TRANSACTION]
						([OwnerID] ,[ACCOUNT:External_Id__c] ,[External_Id__c] ,[RE_GSplitImpID__c] ,[RE_GFImpID__c] ,[rC_Giving__Parent__r:External_ID__c] 
						,[NAME]
						,[CloseDate] , [Gift_Date__c], [RecordTypeID] ,[StageName] ,[Amount], [rC_Giving__Current_Giving_Amount__c], [rC_Giving__Transaction_Type__c] ,[rC_Giving__Affiliation__c]
						,[Fundraising_Program__c]
						,[rC_Giving__Is_Giving_Transaction__c] ,[rC_Giving__Is_Payment_Refunded__c] ,[rC_Giving__Payment_Method__c] ,[rC_Giving__Payment_Method_Selected__r:External_ID__c]
						,[rC_Giving__Allocation_Amount__c] ,[rC_Giving__GAU__r:rC_Giving__External_ID__c] ,[RE_Legacy_Financial_Code__c] ,[RE_Batch_Number__c]
						,[rC_Giving__Acknowledged__c] ,[rC_Giving__Acknowledged_Date__c] ,[rC_Giving__Is_Anonymous__c] ,[rC_Giving__Check_Date__c]
						,[rC_Giving__Check_Number__c] ,[Description] ,[Stock_Issuer__c] ,[Stock_Median_Price__c] ,[rC_Giving__Number_Of_Shares__c]
						,[rC_Giving__Ticker_Symbol__c] ,[Production__c] ,[CreatedDate] ,[RE_Gift_Constituency_Code__c] , [RE_Letter_Code__c], [RE_GFPayMeth__c]
						,[RE_Pledge_Balance__c], [RE_Gift_Type__c], [RE_Gift_SubType__c]
 						,DAF_Pledge_Payment__c, T.Donation_Page_Name__c, T.Donor_Recognition_Name__c, T.Gift_Comments__c
						,T.NCHX_Source_Entity__c, T.Non_Donation_Payment__c, T.Original_Pledge_Amount__c
						,T.Plaque_Wording__c, T.Research_Payment__c, T.Transaction_ID__c, T.Tribute_In_Honor_of__c, T.Tribute_In_Memory_of__c
 						,[Hours_Per_Period__c],[Anticipated_Payment_Type__c]
						,[Considering_bequest_in__c], [Donor_Designation_Fund__c], [Exceed_Appeal__c], [Exceed_Batch_Number__c],[Exceed_Campaign__c]
						,[Former_Appeal__c],[Former_Campaign__c],[Former_ConCode__c],[Former_Fund__c],[Former_Package__c],[Gift_in_kind__c],[Hour_Club__c]
						,[Irregular__c],[Lights_of_Love_FY14_RSVP__c],[Physician_Engagement__c],[Recognition__c],[Report_Exclusion__c]
						,[Selected_Naming_Opporunity__c],[Sobrato_Challenge__c],[Source_of_notification__c],[SphereGiftIdentifier__c]
				 		,[RE_Split_Gift__c] , [GiftSubType__c], [Post_Date__c], [Posted_Status__c], [Adjusted_Date__c], [Adjustment_Post_Date__c], [Adjustment_Post_Status__c]
						, Employee_Recurring_Gift__c
						, [zrefGFType] ,[zrefParentGFImpID] ,[zrefPaymentGFImpID] ,[zrefPaymentGSplitImpID] ,[zrefRE_DB_Owner],[zrefGLinkAmt])
					SELECT  
						dbo.fnc_OwnerID() AS OwnerID         
	/*EXT_ID*/			,T.[ACCOUNT:External_Id__c]
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS External_Id__c
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS RE_GSplitImpID__c
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitGFImpID AS RE_GFImpID__c
	/*EXT_ID*/			,T2.[GSplitRE_DB_OwnerShort]+'-'+T2.[GSplitRE_DB_Tbl]+'-'+T2.GSplitImpID AS [rC_Giving__Parent__r:External_ID__c]

							,CASE   WHEN (T.[GFType] ='MG Pay-Cash' OR T.[GFType]='Pay-Cash') 
									THEN T.[rC_Giving__Affiliation__c]+' - Installment - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10)) 
									ELSE T.[rC_Giving__Affiliation__c]+' - Transaction - '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10))
							END AS NAME

						,CAST(T.GFDate AS DATE) AS	CloseDate
						,T.[GFDate] AS Gift_Date__c
						,dbo.fnc_RecordType('Opportunity_'+T.Child_Giving_RecordType) AS RecordTypeID
						,'Completed' AS StageName

						,CAST(T.GSplitAmt AS DECIMAL(12,2)) AS Amount
						,CAST(T.GSplitAmt AS DECIMAL(12,2)) AS rC_Giving__Current_Giving_Amount__c 
					
						,'Payment' AS rC_Giving__Transaction_Type__c
						,T.[rC_Giving__Affiliation__c]
						,T.[Fundraising_Program__c]
						,T.rC_Giving__Is_Giving_Transaction__c
						,'FALSE' AS rC_Giving__Is_Payment_Refunded__c
						--payment method
						,T.[rC_Giving__Payment_Type__c] AS rC_Giving__Payment_Method__c
	/*EXT_ID*/			,T.[PAYMENT_METHOD_External_ID__c] AS [rC_Giving__Payment_Method_Selected__r:External_ID__c]
					--allocations
						,NULL AS  rC_Giving__Allocation_Amount__c   --Only populate with total gift amount when using Special Allocations.
	/*EXT_ID*/			,NULL AS [rC_Giving__GAU__r:rC_Giving__External_ID__c]	 
					--RE gift_v
						,T.RE_Legacy_Financial_Code__c
						,T.BATCH_NUMBER AS RE_Batch_Number__c
						,CASE WHEN T.GFAck='Acknowledged' THEN 'TRUE' ELSE 'FALSE' END	AS rC_Giving__Acknowledged__c
						,T.GFAckDate	AS	rC_Giving__Acknowledged_Date__c
						,T.GFAnon	AS	rC_Giving__Is_Anonymous__c
						,T.GFCheckDate	AS	rC_Giving__Check_Date__c
						,T.GFCheckNum	AS	rC_Giving__Check_Number__c
						,T.GFRef	AS	[Description]
						,T.GFStkIss	AS	Stock_Issuer__c
						,T.GFStkMedPrice	AS	Stock_Median_Price__c
						,T.GFStkNumUnits	AS	rC_Giving__Number_Of_Shares__c
						,T.GFStkSymbol	AS	rC_Giving__Ticker_Symbol__c
						,CASE WHEN T.Production__c IS NULL THEN T4.[Production] ELSE T.Production__c END AS Production__c
						,T.DateAdded	AS	CreatedDate
						,T.GFConsDesc AS RE_Gift_Constituency_Code__c
						,T.GFLtrCode AS RE_Letter_Code__c
						,T.GFPayMeth AS RE_GFPayMeth__c
						,T.GFPledgeBalance AS RE_Pledge_Balance__c
						,T.[GFType] AS RE_Gift_Type__c
						,T.RE_Gift_SubType__c

					--attributes (chart-attributes)
						,T.DAF_Pledge_Payment__c
						,T.Donation_Page_Name__c
						,T.Donor_Recognition_Name__c
						,T.Gift_Comments__c
						,T.NCHX_Source_Entity__c
						,T.Non_Donation_Payment__c
						,T.Original_Pledge_Amount__c
						,T.Plaque_Wording__c
						,T.Research_Payment__c
						,T.Transaction_ID__c
						,T.Tribute_In_Honor_of__c
						,T.Tribute_In_Memory_of__c
 		
					--gift attribute (chart_giftattribute)
						,T4.[# Hours Per Period] AS Hours_Per_Period__c
						,T4.[Anticipated Payment Type] AS Anticipated_Payment_Type__c	
						,T4.[Considering bequest in] AS Considering_bequest_in__c
						,T4.[Donor Designation-Fund] AS Donor_Designation_Fund__c
						,T4.[Exceed Appeal] AS Exceed_Appeal__c
						,T4.[Exceed Batch Number] AS Exceed_Batch_Number__c
						,T4.[Exceed Campaign] AS Exceed_Campaign__c
						,T4.[Former Appeal] AS Former_Appeal__c
						,T4.[Former Campaign] AS Former_Campaign__c
						,T4.[Former ConCode] AS Former_ConCode__c
						,T4.[Former Fund] AS Former_Fund__c
						,T4.[Former Package] AS Former_Package__c
						,T4.[Gift-in-Kind] AS Gift_in_kind__c
						,T4.[Hour Club] AS Hour_Club__c
						,T4.[Irregular] AS Irregular__c
						,T4.[Lights of Love FY14 RSVP] AS Lights_of_Love_FY14_RSVP__c
						,T4.[Physician Engagement] AS Physician_Engagement__c
 						,T4.[Recogntion] AS Recognition__c
						,T4.[Report Exclusion] AS Report_Exclusion__c
						,T4.[Selected Naming Opportunity] AS Selected_Naming_Opporunity__c
						,T4.[Sobrato Challenge?] AS Sobrato_Challenge__c
						,T4.[Source of notification] AS Source_of_notification__c
						,T4.[SphereGiftIdentifier] AS SphereGiftIdentifier__c

		 			--MISC.
						,T.RE_Split_Gift__c
						,T.GiftSubType__c
						,T.GFPostDate__c  AS Post_Date__c    --renamed for 1P2T
						,T.GFPostStatus__c AS Posted_Status__c --renamed for 1P2T
 						,T5.[Adjusted_Date__c]
					    ,T5.[Adjustment_Post_Date__c]
						,T5.[Adjustment_Post_Status__c]
 					 	,'FALSE' AS Employee_Recurring_Gift__c	
					--reference
					--,X1.ID zrefAccountId
						,T.GFType AS zrefGFType
						,T.GSplitGFImpID AS zrefParentGFImpID
						,T.GSplitGFImpID AS zrefPaymentGFImpID
						,T.GSplitImpID AS zrefPaymentGSplitImpID
						,T.[GSplitRE_DB_OwnerShort] AS zrefRE_DB_Owner
						,T1.GFPaymentAmount AS zrefGLinkAmt

					FROM SUTTER_1P_DATA.dbo.HC_Gift_Link_v AS T1 
/*parent*/			INNER JOIN SUTTER_1P_MIGRATION.TBL.GIFT AS T2 ON T1.GFImpID = T2.GSplitGFImpID AND T1.RE_DB_OwnerShort = T2.GSplitRE_DB_OwnerShort
/*transaction*/		INNER JOIN SUTTER_1P_MIGRATION.TBL.GIFT AS T ON (T1.GFPaymentGFImpID = T.GSplitGFImpID AND T1.RE_DB_OwnerShort = T.GSplitRE_DB_OwnerShort)
					--AND T2.GFSplitSequence = T.GFSplitSequence)   --disable to help find missing gifts
/*OPP TRX */		LEFT JOIN SUTTER_1P_MIGRATION.TBL.OPPORTUNITY_TRANSACTION T3 ON T.GSplitImpID =T3.zrefPaymentGSplitImpID
					AND T.[GSplitRE_DB_OwnerShort]=T3.zrefRE_DB_Owner
					LEFT JOIN SUTTER_1P_MIGRATION.TBL.Gift_Attribute_1 T4 ON T.GSplitGFImpID = T4.GFIMPID AND T.GSplitRE_DB_OwnerShort=T4.RE_DB_OwnerShort
					LEFT JOIN [SUTTER_1P_MIGRATION].TBL.GIFT_ADJUSTED_DATE T5 ON T.[refGFImpID]=T5.GFImpID AND T.[GSplitRE_DB_OwnerShort]=T5.RE_DB_OwnerShort

/*transaction*/		WHERE T.Transaction_Source='PAYMENT' AND T3.zrefPaymentGSplitImpID IS NULL
					 
END--end create imp txt. 

-- 3,124,666
--     2,481

BEGIN--REMOVE DOUBLE QUOTES					
	--SELECT rC_Giving__Tribute_Description__c FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE rC_Giving__Tribute_Description__c LIKE '%"%'
	--SELECT [Description] FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE [Description] LIKE '%"%'
	--SELECT Stock_Issuer__c FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE Stock_Issuer__c LIKE '%"%'
	--SELECT Stock_Median_Price__c FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE Stock_Median_Price__c LIKE '%"%'
	--SELECT rC_Giving__Tribute_Comments__c FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT WHERE rC_Giving__Tribute_Comments__c LIKE '%"%'
	
	BEGIN 
		USE [SUTTER_1P_MIGRATION] 
 		EXEC sp_FindStringInTable '%"%', 'TBL', 'OPPORTUNITY_TRANSACTION'
 	END 

	UPDATE [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION SET NAME =REPLACE(NAME,'"','''') where NAME like '%"%'
 	UPDATE [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION SET [Description]=REPLACE([Description],'"','''') where [Description] like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION SET Stock_Issuer__c=REPLACE(Stock_Issuer__c,'"','''') where Stock_Issuer__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION SET Stock_Median_Price__c=REPLACE(Stock_Median_Price__c,'"','''') where Stock_Median_Price__c like '%"%'
 	UPDATE [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION SET [RE_Gift_Constituency_Code__c]=REPLACE([RE_Gift_Constituency_Code__c],'"','''') where [RE_Gift_Constituency_Code__c] like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION SET Tribute_in_memory_of__c=REPLACE(Tribute_in_memory_of__c,'"','''') where Tribute_in_memory_of__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION SET Transaction_id__c=REPLACE(Transaction_id__c,'"','''') where Transaction_id__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION SET Tribute_in_honor_of__c=REPLACE(Tribute_in_honor_of__c,'"','''') where Tribute_in_honor_of__c like '%"%'
 	UPDATE [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION SET RE_Letter_Code__c=REPLACE(RE_Letter_Code__c,'"','''') where RE_Letter_Code__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION SET Donation_page_name__c=REPLACE(Donation_page_name__c,'"','''') where Donation_page_name__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION SET Gift_comments__c=REPLACE(Gift_comments__c,'"','''') where Gift_comments__c like '%"%'
	UPDATE [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION SET Donor_recognition_name__c=REPLACE(Donor_recognition_name__c,'"','''') where Donor_recognition_name__c like '%"%'

END 

BEGIN-- name
		SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION WHERE Name IS NULL OR External_Id__c IS NULL
		SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION WHERE RecordTypeId IS null
		SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].OPPORTUNITY_TRANSACTION WHERE [rC_Giving__Payment_Method_Selected__r:External_ID__c] IS NULL AND StageName!='Uncollectible'
END 

BEGIN--check duplicates
 				/*	SELECT GFType, Parent_Giving_RecordType, COUNT(*) c 
					FROM SUTTER_1P_MIGRATION.TBL.GIFT
					GROUP BY GFType, Parent_Giving_RecordType
 				*/  
  				DROP TABLE SUTTER_1P_MIGRATION.TBL.z_OPPORTUNITY_TRANSACTION_dupe
 		
				SELECT *
 				INTO SUTTER_1P_MIGRATION.TBL.z_OPPORTUNITY_TRANSACTION_dupe
				FROM SUTTER_1P_MIGRATION.[TBL].OPPORTUNITY_TRANSACTION
 				WHERE External_Id__c IN (SELECT External_Id__c FROM SUTTER_1P_MIGRATION.[TBL].OPPORTUNITY_TRANSACTION GROUP BY External_Id__c HAVING COUNT(*)>1)
				ORDER BY External_Id__c, [ACCOUNT:External_Id__c]
 
END
    
BEGIN--CREATE IMP tbl.
		
		DROP TABLE  SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION]
		
		--create empty table to later populate and sort    	
		SELECT TOP 0 *
		INTO SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION]
		FROM SUTTER_1P_MIGRATION.TBL.[OPPORTUNITY_TRANSACTION]	
	 
		--add myid as primary key for sorting purposes
		ALTER TABLE SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION] 
		ADD zrefmyID int identity(1, 1) primary key;
	
		ALTER TABLE SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION]
		ADD [zrefNewExternal_Id__c]	VARCHAR(50)
		
		--verify table is empty
		TRUNCATE TABLE SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION] 
		
		--insert records.
   		INSERT INTO [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_TRANSACTION]
				   ([OwnerID]
				   ,[ACCOUNT:External_Id__c]
				   ,[External_Id__c]
				   ,[RE_GSplitImpID__c]
				   ,[RE_GFImpID__c]
				   ,[rC_Giving__Parent__r:External_ID__c]
				   ,[NAME]
				   ,[CloseDate]
				   ,[Gift_Date__c]
				   ,[RecordTypeID]
				   ,[StageName]
				   ,[Amount]
				   ,[rC_Giving__Current_Giving_Amount__c]
				   ,[rC_Giving__Transaction_Type__c]
				   ,[rC_Giving__Affiliation__c]
				   ,[Fundraising_Program__c]
				   ,[rC_Giving__Is_Giving_Transaction__c]
				   ,[rC_Giving__Is_Payment_Refunded__c]
				   ,[rC_Giving__Payment_Method__c]
				   ,[rC_Giving__Payment_Method_Selected__r:External_ID__c]
				   ,[rC_Giving__Allocation_Amount__c]
				   ,[rC_Giving__GAU__r:rC_Giving__External_ID__c]
				   ,[RE_Legacy_Financial_Code__c]
				   ,[RE_Batch_Number__c]
				   ,[rC_Giving__Acknowledged__c]
				   ,[rC_Giving__Acknowledged_Date__c]
				   ,[rC_Giving__Is_Anonymous__c]
				   ,[rC_Giving__Check_Date__c]
				   ,[rC_Giving__Check_Number__c]
				   ,[Description]
				   ,[Stock_Issuer__c]
				   ,[Stock_Median_Price__c]
				   ,[rC_Giving__Number_Of_Shares__c]
				   ,[rC_Giving__Ticker_Symbol__c]
				   ,[Production__c]
				   ,[CreatedDate]
				   ,[RE_Gift_Constituency_Code__c]
				   ,[RE_Letter_Code__c]
				   ,[RE_GFPayMeth__c]
				   ,[RE_Pledge_Balance__c]
				   ,[RE_Gift_Type__c]
				   ,[RE_Gift_SubType__c]
				   ,[DAF_Pledge_Payment__c]
				   ,[Donation_Page_Name__c]
				   ,[Donor_Recognition_Name__c]
				   ,[Gift_Comments__c]
				   ,[NCHX_Source_Entity__c]
				   ,[Non_Donation_Payment__c]
				   ,[Original_Pledge_Amount__c]
				   ,[Plaque_Wording__c]
				   ,[Research_Payment__c]
				   ,[Transaction_ID__c]
				   ,[Tribute_In_Honor_of__c]
				   ,[Tribute_In_Memory_of__c]
				   ,[Hours_Per_Period__c]
				   ,[Anticipated_Payment_Type__c]
				   ,[Considering_bequest_in__c]
				   ,[Donor_Designation_Fund__c]
				   ,[Exceed_Appeal__c]
				   ,[Exceed_Batch_Number__c]
				   ,[Exceed_Campaign__c]
				   ,[Former_Appeal__c]
				   ,[Former_Campaign__c]
				   ,[Former_ConCode__c]
				   ,[Former_Fund__c]
				   ,[Former_Package__c]
				   ,[Gift_in_kind__c]
				   ,[Hour_Club__c]
				   ,[Irregular__c]
				   ,[Lights_of_Love_FY14_RSVP__c]
				   ,[Physician_Engagement__c]
				   ,[Recognition__c]
				   ,[Report_Exclusion__c]
				   ,[Selected_Naming_Opporunity__c]
				   ,[Sobrato_Challenge__c]
				   ,[Source_of_notification__c]
				   ,[SphereGiftIdentifier__c]
				   ,[RE_Split_Gift__c]
				   ,[GiftSubType__c]
				   ,[Post_Date__c]
				   ,[Posted_Status__c]
				   ,[Adjusted_Date__c]
				   ,[Adjustment_Post_Date__c]
	 			   ,[Adjustment_Post_Status__c]
				   ,[Employee_Recurring_Gift__c]
				   ,[zrefGFType]
				   ,[zrefParentGFImpID]
				   ,[zrefPaymentGFImpID]
				   ,[zrefPaymentGSplitImpID]
				   ,[zrefRE_DB_Owner]
				   ,[zrefGLinkAmt]
    			   ,[zrefNewExternal_Id__c])
			SELECT  [OwnerID]
				   ,[ACCOUNT:External_Id__c]
				   ,[External_Id__c]
				   ,[RE_GSplitImpID__c]
				   ,[RE_GFImpID__c]
				   ,[rC_Giving__Parent__r:External_ID__c]
				   ,[NAME]
				   ,[CloseDate]
				   ,[Gift_Date__c]
				   ,[RecordTypeID]
				   ,[StageName]
				   ,[Amount]
				   ,[rC_Giving__Current_Giving_Amount__c]
				   ,[rC_Giving__Transaction_Type__c]
				   ,[rC_Giving__Affiliation__c]
				   ,[Fundraising_Program__c]
				   ,[rC_Giving__Is_Giving_Transaction__c]
				   ,[rC_Giving__Is_Payment_Refunded__c]
				   ,[rC_Giving__Payment_Method__c]
				   ,[rC_Giving__Payment_Method_Selected__r:External_ID__c]
				   ,[rC_Giving__Allocation_Amount__c]
				   ,[rC_Giving__GAU__r:rC_Giving__External_ID__c]
				   ,[RE_Legacy_Financial_Code__c]
				   ,[RE_Batch_Number__c]
				   ,[rC_Giving__Acknowledged__c]
				   ,[rC_Giving__Acknowledged_Date__c]
				   ,[rC_Giving__Is_Anonymous__c]
				   ,[rC_Giving__Check_Date__c]
				   ,[rC_Giving__Check_Number__c]
				   ,[Description]
				   ,[Stock_Issuer__c]
				   ,[Stock_Median_Price__c]
				   ,[rC_Giving__Number_Of_Shares__c]
				   ,[rC_Giving__Ticker_Symbol__c]
				   ,[Production__c]
				   ,[CreatedDate]
				   ,[RE_Gift_Constituency_Code__c]
				   ,[RE_Letter_Code__c]
				   ,[RE_GFPayMeth__c]
				   ,[RE_Pledge_Balance__c]
				   ,[RE_Gift_Type__c]
				   ,[RE_Gift_SubType__c]
				   ,[DAF_Pledge_Payment__c]
				   ,[Donation_Page_Name__c]
				   ,[Donor_Recognition_Name__c]
				   ,[Gift_Comments__c]
				   ,[NCHX_Source_Entity__c]
				   ,[Non_Donation_Payment__c]
				   ,[Original_Pledge_Amount__c]
				   ,[Plaque_Wording__c]
				   ,[Research_Payment__c]
				   ,[Transaction_ID__c]
				   ,[Tribute_In_Honor_of__c]
				   ,[Tribute_In_Memory_of__c]
				   ,[Hours_Per_Period__c]
				   ,[Anticipated_Payment_Type__c]
				   ,[Considering_bequest_in__c]
				   ,[Donor_Designation_Fund__c]
				   ,[Exceed_Appeal__c]
				   ,[Exceed_Batch_Number__c]
				   ,[Exceed_Campaign__c]
				   ,[Former_Appeal__c]
				   ,[Former_Campaign__c]
				   ,[Former_ConCode__c]
				   ,[Former_Fund__c]
				   ,[Former_Package__c]
				   ,[Gift_in_kind__c]
				   ,[Hour_Club__c]
				   ,[Irregular__c]
				   ,[Lights_of_Love_FY14_RSVP__c]
				   ,[Physician_Engagement__c]
				   ,[Recognition__c]
				   ,[Report_Exclusion__c]
				   ,[Selected_Naming_Opporunity__c]
				   ,[Sobrato_Challenge__c]
				   ,[Source_of_notification__c]
				   ,[SphereGiftIdentifier__c]
				   ,[RE_Split_Gift__c]
				   ,[GiftSubType__c]
				   ,[Post_Date__c]
				   ,[Posted_Status__c]
				   ,[Adjusted_Date__c]
				   ,[Adjustment_Post_Date__c]
	 			   ,[Adjustment_Post_Status__c]
				   ,[Employee_Recurring_Gift__c]
				   ,[zrefGFType]
				   ,[zrefParentGFImpID]
				   ,[zrefPaymentGFImpID]
				   ,[zrefPaymentGSplitImpID]
				   ,[zrefRE_DB_Owner]
				   ,[zrefGLinkAmt]
				   ,CASE WHEN (ROW_NUMBER() OVER ( PARTITION BY EXTERNAL_ID__C ORDER BY EXTERNAL_ID__C)-1)>0 
					THEN EXTERNAL_ID__C +'-'+ CAST((ROW_NUMBER() OVER ( PARTITION BY EXTERNAL_ID__C ORDER BY EXTERNAL_ID__C)-1)AS VARCHAR(2)) 
					ELSE EXTERNAL_ID__C 	END 
			FROM [SUTTER_1P_MIGRATION].TBL.[OPPORTUNITY_TRANSACTION]
 			ORDER BY [ACCOUNT:External_ID__c], [rC_Giving__Parent__r:External_ID__c], CloseDate
			-- 3127852
			-- 3127147
	 	
	
				/*ADDED ABOVE--add suffix to address duplicates. 
					SELECT  T1.*,
						CASE WHEN (ROW_NUMBER() OVER ( PARTITION BY T1.EXTERNAL_ID__C ORDER BY T1.EXTERNAL_ID__C)-1)>0 
						THEN T1.EXTERNAL_ID__C +'-'+ CAST((ROW_NUMBER() OVER ( PARTITION BY T1.EXTERNAL_ID__C ORDER BY T1.EXTERNAL_ID__C)-1)AS VARCHAR(2)) 
						ELSE T1.EXTERNAL_ID__C 
						END AS [zrefNewExternal_Id__c]
					INTO SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION]
					FROM SUTTER_1P_MIGRATION.TBL.[OPPORTUNITY_TRANSACTION] T1
					ORDER BY [ACCOUNT:External_ID__c], [rC_Giving__Parent__r:External_ID__c], CloseDate
				*/		
		--check dupes
		SELECT [External_ID__c], [RE_GSplitImpID__c], [zrefNewExternal_Id__c], len ([External_ID__c]) le, LEN([zrefNewExternal_Id__c]) lne
		FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION]
		WHERE [External_ID__c] IN (SELECT [External_ID__c] FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION] GROUP BY [External_ID__c] HAVING COUNT(*)>1)
			OR  [RE_GSplitImpID__c] IN (SELECT [RE_GSplitImpID__c] FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION] GROUP BY [RE_GSplitImpID__c] HAVING COUNT(*)>1)
 	 	ORDER BY [External_ID__c], [zrefNewExternal_Id__c]
  
		--update
  		UPDATE SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION]
		SET [External_ID__c]= [zrefNewExternal_Id__c], [RE_GSplitImpID__c]= [zrefNewExternal_Id__c]
		WHERE [External_ID__c] IN (SELECT [External_ID__c] FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION] GROUP BY [External_ID__c] HAVING COUNT(*)>1)
 		--8336 
END  


BEGIN -- split files. 

	   SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION]  --3127147

 	   DROP TABLE SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION_1of3]
	   DROP TABLE SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION_2of3]
	   DROP TABLE SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION_3of3]

	   SELECT * 
	   INTO SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION_1of3]		  -- DROP TABLE SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_PARENT_1of3]
	   FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION]
	   WHERE zrefmyId <1000001	
	   GO   --	SELECT zrefmyId FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION_1of3] 	 ORDER BY zrefmyId

	   SELECT * 
	   INTO SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION_2of3]
	   FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION]
	   WHERE zrefmyId >1000000 AND zrefmyId <2000001
	   GO   --	SELECT zrefmyId FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION_2of3] 	 ORDER BY zrefmyId

	   SELECT * 
	   INTO SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION_3of3]
	   FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION]
	   WHERE zrefmyId >2000000		
	   GO   --	SELECT zrefmyId FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION_3of3] 	 ORDER BY zrefmyId

		   	  	   SELECT TOP 10000 * FROM SUTTER_1P_MIGRATION.IMP.[OPPORTUNITY_TRANSACTION_2of3] 		 WHERE adjusted_date__c IS NOT null



BEGIN--exceptions
	DROP TABLE SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_TRANSACTION_XCP

	SELECT T.* 
 	INTO SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_TRANSACTION_XCP
	FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_TRANSACTION T
	LEFT JOIN SUTTER_1P_MIGRATION.XTR.OPPORTUNITY X ON T.External_Id__c=X.External_Id__c
	WHERE X.External_Id__c IS NULL OR X.EXTERNAL_id__c=''
	ORDER BY T.[ACCOUNT:External_Id__c], [T].[rC_Giving__Parent__r:External_ID__c], [T].[CloseDate] ASC
  
END; 

 

 
									    
   