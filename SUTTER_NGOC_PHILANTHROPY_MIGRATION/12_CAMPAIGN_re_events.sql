USE [SUTTER_1P_MIGRATION]
GO
			
--ALL RE EVENT data scripted here. 
	--CAMPAIGN 
	--RC_GIVING__CAMPAIGN_COST__C
	--PRODUCT (ITEM)
	--PRICEBOOK ENTRY (junction object between PRODUCT and PRICEBOOK)
	--RC_EVENT__CAMPAIGN_TICKET__C (from EventPrice and EventRegistrationFee)
	--CAMPAIGN_MEMBER (Participants that are constituents)
	
BEGIN 
	DROP TABLE SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT
END 
				 	
BEGIN----CAMPAIGN 
			
				SELECT DISTINCT
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Campaign_Event') AS RecordTypeID
				,T.RE_DB_OwnerShort+'-'+T.EVImpID AS External_ID__c
				,T.RE_DB_OwnerShort+'-'+T.EVID	AS	RE_EVID__c
				,T.EVName	AS	Name
				,CAST(T.EVDesc	AS VARCHAR(MAX)) AS	[Description]
				,T.EVCapacity	AS rC_Event__Registered_Limit__c
				,T.EVCategory	AS rC_Giving__Campaign_Category__c
				,T.EVGoal	AS 	rC_Giving__Expected_Giving_Amount__c		
				,T.EVInactive	AS IsActive
				,T.EVStartDate AS rC_Event__Start_Date_Time__c
				,T.[EVStartTime] AS RE_Event_Start_Time__c
				,T.EVEndDate  AS rC_Event__End_Date_Time__c
				,T.[EVEndTime] AS RE_Event_End_Time__c 
				,NULL AS StartDate
				,NULL AS EndDate
				,T.EVLoc	AS	rC_Event__Primary_Venue__c
				,T1.CAMPAIGN_rC_Event__Event_Type__c	AS	rC_Event__Event_Type__c	
				,T.EVNumberInvited	AS	Number_Invited__c
				,T.EVNumberOfParticipants	AS	Number_of_Participants__c
				,T.EVNumberRegistered	AS	Number_Registered__c
				,'01261000000UsFIAA0' AS CampaignMemberRecordTypeId
				,T.RE_DB_OwnerShort	AS	rC_Giving__Affiliation__c
				,'(GMT-07:00) Pacific Daylight Time (America/Los_Angeles)' AS RC_EVENT__TIME_ZONE__C
				,T.EVImpID zrefEVImpID
				
				INTO SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT
				FROM SUTTER_1P_DATA.DBO.HC_Event_v T
				LEFT JOIN SUTTER_1P_DATA.DBO.CHART_EventType T1 ON T.EVType=T1.EVType AND T.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
END				  --561

BEGIN	 

		BEGIN 
			USE [SUTTER_1P_MIGRATION] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'CAMPAIGN_EVENT'
 		END 

		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT] SET [Description]=REPLACE([Description],'"','''') where [Description] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT] SET [NAME]=REPLACE([NAME],'"','''') where [NAME] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT] SET [rC_Event__Primary_Venue__c]=REPLACE([rC_Event__Primary_Venue__c],'"','''') where [rC_Event__Primary_Venue__c] like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT] SET [rC_Event__Event_Type__c]=REPLACE([rC_Event__Event_Type__c],'"','''') where [rC_Event__Event_Type__c] like '%"%'

		SELECT COUNT(*) FROM [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT]
END

BEGIN--check dupes
				SELECT * FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT
				WHERE External_ID__c IN (SELECT External_ID__c FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT GROUP BY External_ID__c HAVING COUNT(*)>1)
END
 
		 
BEGIN--RC_GIVING__CAMPAIGN_COST__C

				DROP TABLE SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_CAMPAIGN_COST
				
				SELECT 
				 T1.RE_DB_OwnerShort+'-'+T1.EVImpID AS [rC_Giving__Campaign__r:External_ID__c]
				,T.EVExpType	AS	rC_Giving__Expense__c
				,T.EVExpAmount	AS	rC_Giving__Actual_Amount__c
				,T.EVExpBudgetedAmount	AS	rC_Giving__Budgeted_Amount__c
				,T.EVExpComments	AS	rC_Giving__Notes__c
				,T.EVExpVendorName	AS	Vendor_Name__c
				,T.EVExpDate	AS	rC_Giving__Effective_Date__c
				,T.RE_DB_OwnerShort	AS	Affiliation__c
				,T.EventID zrefEVId
				,NULL ID
				INTO SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_CAMPAIGN_COST
				FROM SUTTER_1P_DATA.dbo.HC_Event_Expense_v T
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Event_v T1 ON T.EventID=T1.EVID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				ORDER BY T.EventID, T.EVExpType

				SELECT * FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_CAMPAIGN_COST
END; 
 
BEGIN--PRODUCTS (Label ITEMS)

				DROP TABLE SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_ITEM

				SELECT DISTINCT 
 				 T.EVPriceUnit	AS RE_EVPriceImpID__c
				,T.EVPriceUnit	AS 	Name
				,T.EVPriceUnit	AS 	ProductCode
				--,T.EVPriceRcptAmt	AS Tax_Deductible_Amount__c
				--,T.EVPriceGiftAmt	AS	rC_Giving__Unit_Cost__c
				--,T.EVPriceCom	AS	[Description]
				--,T.RE_DB_OwnerShort	AS	rC_Giving__Affiliation__c
				--,T.EventID	zrefEVId
				INTO SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_ITEM
				FROM SUTTER_1P_DATA.DBO.HC_Event_Price_v T	
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Event_v T1 ON T.EventID=T1.EVID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				ORDER BY T.EVPriceUnit

				SELECT LEN(RE_EVPriceImpID__c) l,* FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_ITEM
			--	WHERE RE_EVPriceImpID__c IN (SELECT RE_EVPriceImpID__c FROM SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_ITEM
				--								 GROUP BY RE_EVPriceImpID__c HAVING COUNT(*)>1)
				ORDER BY l desc
 
 				BEGIN 
					USE [SUTTER_1P_MIGRATION] 
 					EXEC sp_FindStringInTable '%"%', 'IMP', 'CAMPAIGN_EVENT_ITEM'
				END 
			 	UPDATE [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT_ITEM] SET [RE_EVPriceImpID__c]=REPLACE([RE_EVPriceImpID__c],'"','''') where [RE_EVPriceImpID__c]  like '%"%'
			 	UPDATE [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT_ITEM] SET [NAME]=REPLACE([NAME],'"','''') where [NAME] like '%"%'
				UPDATE [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT_ITEM] SET [ProductCode]=REPLACE([ProductCode],'"','''') where [ProductCode] like '%"%'
			

				SELECT COUNT(*) FROM [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT_ITEM]

				SELECT * FROM SUTTER_1P_MIGRATION.IMP.[CAMPAIGN_EVENT_ITEM]
				
				/*  OLD CODE replaced by above to reduce duplicate Items created
				SELECT  
 				T.RE_DB_OwnerShort+'-'+T.EVPriceImpID	AS RE_EVPriceImpID__c
				T.EVPriceUnit	AS 	Name
				,T.EVPriceUnit	AS 	ProductCode
				--,T.EVPriceRcptAmt	AS Tax_Deductible_Amount__c
				--,T.EVPriceGiftAmt	AS	rC_Giving__Unit_Cost__c
				--,T.EVPriceCom	AS	[Description]
				--,T.RE_DB_OwnerShort	AS	rC_Giving__Affiliation__c
				--,T.EventID	zrefEVId
			--	INTO SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_ITEM
				FROM SUTTER_1P_DATA.DBO.HC_Event_Price_v T	
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Event_v T1 ON T.EventID=T1.EVID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				ORDER BY T.EVPriceUnit
				*/ 
 	 
END;

BEGIN--PRICEBOOK_ENTRY (LINK PRODUCT (aka ITEM) TO PRICEBOOK, USING DEFAULT PRICEBOOK RECORD).. PRODUCT links to the CAMPAIGN TICKET object
				SELECT * FROM [SUTTER_1P_MIGRATION].xtr.[PRICEBOOK]

				DROP TABLE SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_PRICEBOOK_ENTRY
				
				SELECT DISTINCT
				T.EVPriceUnit	AS [Product2:RE_EVPriceImpID__c]
				,'01s61000000wQmVAAU' AS Pricebook2Id
				,T.EVPriceGiftAmt AS UnitPrice
				,'TRUE' AS IsActive
				,'FALSE' AS UseStandardPrice
				,NULL AS ID
				,T.EVPriceUnit	AS 	zrefProductCode
				INTO SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_PRICEBOOK_ENTRY       
				FROM SUTTER_1P_DATA.DBO.HC_Event_Price_v T	
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Event_v T1 ON T.EventID=T1.EVID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				ORDER BY T.EVPriceUnit

				 
				BEGIN 
					USE [SUTTER_1P_MIGRATION] 
 					EXEC sp_FindStringInTable '%"%', 'IMP', 'CAMPAIGN_EVENT_PRICEBOOK_ENTRY'
				
					UPDATE [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT_PRICEBOOK_ENTRY] SET [zrefProductCode]=REPLACE([zrefProductCode],'"','''') where [zrefProductCode] like '%"%'
					UPDATE [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT_PRICEBOOK_ENTRY] SET [Product2:RE_EVPriceImpID__c]=REPLACE([Product2:RE_EVPriceImpID__c],'"','''') where [Product2:RE_EVPriceImpID__c] like '%"%'
			
				END 

				SELECT COUNT(*) FROM [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT_PRICEBOOK_ENTRY]
				SELECT * FROM  [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT_PRICEBOOK_ENTRY]
				
END; 

SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Fund_GLDistribution_v] ORDER BY [RE_DB_OwnerShort], [FundID], [FundDistrGiftType]
BEGIN--CAMPAIGN_TICKETS (FROM EVENT PRICE)

				DROP TABLE SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_CAMPAIGN_TICKETS 
		
				SELECT DISTINCT
				T1.RE_DB_OwnerShort+'-'+T1.EVImpID AS [rc_Event__Campaign__r:External_ID__c]
				,T.EVPriceUnit	AS [rC_Event__Product__r:RE_EVPriceImpID__c]
				,T.EVPriceGiftAmt	AS	rC_Event__Purchase_Price__c
				,T.EVPriceCom	AS	rC_Event__Description__c
				,T.[EVPriceRcptAmt] AS Tax_Deductible_Amount__c

				,NULL AS Registration_Fee_Gift_Amount__c
				,NULL AS rC_Event__Purchase_Quantity__c
				,NULL AS rC_Event__Purchase_Fee__c	
				,T.RE_DB_OwnerShort AS Affiliation__c
				,NULL AS ID
				--ref
				,'EVPrice' AS zrefSource
				INTO SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_CAMPAIGN_TICKETS  
				FROM SUTTER_1P_DATA.DBO.HC_Event_Price_v T	
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Event_v T1 ON T.EventID=T1.EVID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				 
			UNION ALL 

				--CAMPAIGN_TICKETS (FROM REGISTRATION FEES)
				SELECT DISTINCT
				T.RE_DB_OwnerShort+'-'+T2.EVImpID AS [rc_Event__Campaign__r:External_ID__c]
				,T3.EVPriceUnit	AS [rC_Event__Product__r:RE_EVPriceImpID__c]
				,SUM(T.REGFeeAmtPaid) AS rC_Event__Purchase_Price__c
				,MAX(T.REGFeeCom) AS	rC_Event__Description__c
				,T3.[EVPriceRcptAmt] AS Tax_Deductible_Amount__c
				,T.REGFeeGiftAmt AS	Registration_Fee_Gift_Amount__c
				,SUM(T.REGFeeNumUnits) AS rC_Event__Purchase_Quantity__c
				,SUM(T.REGFeeRcptAmt-T.REGFeeAppliedAmt) AS rC_Event__Purchase_Fee__c
				--,T.REGFeeUnit AS Name
				,T.RE_DB_OwnerShort AS Affiliation__c
				,NULL AS ID
				--ref
				,'RegFees' AS zrefSource
				FROM SUTTER_1P_DATA.DBO.HC_Event_Registration_Fees_v T	
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Event_Participant_v T1 ON T.REGImpID=T1.REGImpID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Event_v T2 ON T1.EventID=T2.EVID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				INNER JOIN [SUTTER_1P_DATA].dbo.[HC_Event_Price_v] T3 ON T.[EVPriceImpID]=T3.[EVPriceImpID]
				GROUP BY T.RE_DB_OwnerShort, T2.EVImpID, T3.EVPriceUnit, T.REGFeeGiftAmt, T3.[EVPriceRcptAmt]--, T.REGFeeUnit, T.REGFeeAppliedAmt


				BEGIN 
					USE [SUTTER_1P_MIGRATION] 
 					EXEC sp_FindStringInTable '%"%', 'IMP', 'CAMPAIGN_EVENT_CAMPAIGN_TICKETS'

					UPDATE [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT_CAMPAIGN_TICKETS] SET [rC_Event__Description__c]=REPLACE([rC_Event__Description__c],'"','''') where [rC_Event__Description__c] like '%"%'
 					UPDATE [SUTTER_1P_MIGRATION].[IMP].[CAMPAIGN_EVENT_CAMPAIGN_TICKETS] SET [rC_Event__Product__r:RE_EVPriceImpID__c]=REPLACE([rC_Event__Product__r:RE_EVPriceImpID__c],'"','''') where [rC_Event__Product__r:RE_EVPriceImpID__c] like '%"%'
	  			END   
END;  
	  

BEGIN--CAMPAIGN_MEMBER 
 
				SELECT DISTINCT
				--,dbo.fnc_RecordType('CampaignMember_Event Member') AS RecordTypeID --READ ONLY -- CAMPAIGN MEMBER TYPE IS ADDED IN THE "CAMPAIGN" RECORD. 
				T.RE_DB_OwnerShort +'-'+T.REGImpID AS RE_REGImpID__c
				,T2.[Status] AS rC_Event__Attendance_Status__c
				,T2.Participant_Type__c
				,T3.NEW_TGT_IMPORT_ID AS [CONTACT:External_Id__c]
				,T1.RE_DB_OwnerShort+'-'+T1.EVImpID AS [CAMPAIGN:External_Id__c]
				,T1.RE_DB_OwnerShort AS Affiliation__c
				--ref
				,T.REGAttended AS zrefREGAttended
				,T.REGStatus AS zrefREGStatus
				INTO SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_CAMPAIGN_MEMBER   
				FROM SUTTER_1P_DATA.dbo.HC_Event_Participant_v T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T3 ON T.ImportId=T3.ImportID AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				INNER JOIN SUTTER_1P_DATA.dbo.HC_Event_v T1 ON T.EventID=T1.EVID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_1P_DATA.dbo.CHART_ParticipantType T2 ON T2.REGParticipation=T.REGParticipation AND T2.RE_DB_OwnerShort=T.RE_DB_OwnerShort
				WHERE T.ImportId IS NOT NULL AND T3.KeyInd='I'
			 --35720
				
END;


 

--exceptions.
SELECT * from SUTTER_1P_MIGRATION.IMP.CAMPAIGN_EVENT_PRICEBOOK_ENTRY 
ORDER BY [Product2:RE_EVPriceImpID__c]=''
WHERE 