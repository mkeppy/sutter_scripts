 
USE SUTTER_1P_DATA																	  
GO
	 																		 

BEGIN-- check dupe PrefAddr on HC_Constituent
	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Constituents_v]
	WHERE [RE_DB_Id] IN (SELECT [RE_DB_Id] FROM [SUTTER_1P_DATA].dbo.[HC_Constituents_v] GROUP BY [RE_DB_Id] HAVING COUNT(*)>1)
	ORDER BY [RE_DB_Id]
END;
	 
BEGIN--PREFERRED ADDRESS 

		DROP TABLE SUTTER_1P_MIGRATION.TBL.Address_Pref

		SELECT T.RE_DB_OwnerShort, T.ImportID, T.ConsID, T.AddrPref, T.AddrImpID, T.AddrLine1, T.AddrLine2, T.AddrCity, T.AddrState, T.AddrZIP, T.AddrCountry,
				T.RE_DB_ID, T1.NEW_TGT_IMPORT_ID
		INTO SUTTER_1P_MIGRATION.TBL.Address_Pref
		FROM SUTTER_1P_DATA.dbo.HC_Cons_Address_v T
		INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.RE_DB_ID=T1.RE_DB_ID
		WHERE T.AddrPref='TRUE' AND (T.[AddrLine1] IS NOT NULL AND T.[AddrLine1]!='') AND (T.[AddrLine1] NOT LIKE '%deceased%' AND T.[AddrLine1]!='dec')
		--843643   
 		
		BEGIN--check dupe
			SELECT RE_DB_ID, COUNT(*) C
			FROM SUTTER_1P_MIGRATION.TBL.Address_Pref
			GROUP BY RE_DB_ID
			HAVING COUNT(*)>1
			ORDER BY C desc
		END 
		 
			
END
	 
BEGIN--CONSTITUENT SOLICIT CODES  -- sf_object 2 do not solicit codes

			-- SELECT * FROM SUTTER_1P_DATA.dbo.CHART_ConsSolicitCodes where sf_object_2 is not null
  			--create base table 
			DROP TABLE SUTTER_1P_MIGRATION.TBL.SolicitCodes
		     
			 SELECT DISTINCT
				--	T.ImportID ,
				--	T.RE_DB_OwnerShort,
				--	T1.RE_DB_Id,
					T1.NEW_TGT_IMPORT_ID,
					MAX(T2.Do_Not_Phone__c) AS Do_Not_Phone__c ,
					MAX(T2.Do_Not_Solicit__c) AS Do_Not_Solicit__c ,
					MAX(T2.Do_Not_Mail__c) AS Do_Not_Mail__c ,
					MAX(T2.Do_Not_Contact__c) AS Do_Not_Contact__c 
					--T.SolicitCode oldSolicitCode
			 INTO   SUTTER_1P_MIGRATION.TBL.SolicitCodes		--
			 FROM   SUTTER_1P_DATA.dbo.HC_Cons_SolicitCodes_v T
					INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
					INNER JOIN SUTTER_1P_DATA.dbo.CHART_ConsSolicitCodes T2 ON T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
																  AND T.SolicitCode = T2.SolicitCode
			 WHERE  T2.SF_Object_2 != ''  AND T2.[SF_Object_2] IS NOT null
			 GROUP BY --T.ImportID , 
					  --T.RE_DB_OwnerShort,
					  --T1.RE_DB_Id,
					  T1.NEW_TGT_IMPORT_ID
			 ORDER BY T1.NEW_TGT_IMPORT_ID
					--T.ImportID;  --16772
		
		BEGIN --check duplicates	
				SELECT NEW_TGT_IMPORT_ID, COUNT(*) c
				FROM SUTTER_1P_MIGRATION.TBL.SolicitCodes
				GROUP BY NEW_TGT_IMPORT_ID
				HAVING COUNT(*) >1
		END;

		SELECT * FROM SUTTER_1P_MIGRATION.TBL.SolicitCodes 
END;
 
BEGIN--CONSTITUENT PHONES
 		-- SELECT COUNT(*) FROM SUTTER_1P_DATA.DBO.HC_Cons_Phone_v  --96260
 		-- SELECT * FROM SUTTER_1P_MIGRATION.dbo.CHART_PhoneType
		--SELECT * FROM SUTTER_1P_DATA.dbo.HC_Cons_Phone_v 


		BEGIN
			DROP TABLE SUTTER_1P_MIGRATION.TBL.Phones
			DROP TABLE SUTTER_1P_MIGRATION.TBL.Phones_seq
			DROP TABLE SUTTER_1P_MIGRATION.TBL.Phones_seq_1
			DROP TABLE SUTTER_1P_MIGRATION.TBL.Phones_seq_2
			DROP TABLE SUTTER_1P_MIGRATION.TBL.Phones_seq_1_final
			DROP TABLE SUTTER_1P_MIGRATION.TBL.Phones_seq_2_final 
		END	
		 
		BEGIN--PHONES_1: REMOVE DEDUPLICATE PHONE NUMBER and create base table
			
 
				SELECT DISTINCT
						T.RE_DB_OwnerShort ,
						T.ConsID ,
						T.ImportID ,
						T.RE_DB_Id,
						T.NEW_TGT_IMPORT_ID,
						T.KeyInd ,
						T2.PhoneType ,
						T2.PhoneNum ,
						T3.SF_Object ,
						T3.SF_Field_API,
						T2.PhoneDoNotCall
				INTO    SUTTER_1P_MIGRATION.TBL.Phones    -- select * from SUTTER_1P_MIGRATION.TBL.Phones
				FROM    SUTTER_1P_DATA.dbo.HC_Constituents_v T
						INNER JOIN SUTTER_1P_DATA.dbo.HC_Cons_Phone_v T2 ON T.ImportID = T2.ImportID
																  AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
						INNER JOIN SUTTER_1P_DATA.dbo.HC_Cons_Address_v T4 ON T2.PhoneAddrImpID = T4.AddrImpID    
																  AND T2.RE_DB_OwnerShort = T4.RE_DB_OwnerShort
						INNER JOIN SUTTER_1P_DATA.dbo.CHART_PhoneType T3 ON T2.PhoneType = T3.PhoneType
																  AND T2.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
																  AND T.KeyInd = T3.KeyInd
				WHERE T4.AddrPref='TRUE'
				UNION ALL 
				SELECT DISTINCT   --code to set to true the do not call fields. this way, the pivot fnc won't create dupe rows. 
						T.RE_DB_OwnerShort ,
						T.ConsID ,
						T.ImportID ,
						T.RE_DB_Id,
						T.NEW_TGT_IMPORT_ID,
						T.KeyInd ,
						T2.PhoneType ,
						'TRUE' AS PhoneNum ,
						T3.SF_Object ,
						CASE WHEN T3.SF_Field_API='HomePhone' AND T2.PhoneDoNotCall='TRUE' THEN  'rC_Bios__Home_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='MobilePhone' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Mobile_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='rC_Bios__Work_Phone__c' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Work_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='OtherPhone' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Other_Do_Not_Call__c' ELSE NULL END AS SF_Field_API,
						T2.PhoneDoNotCall
				FROM    SUTTER_1P_DATA.dbo.HC_Constituents_v T
						INNER JOIN SUTTER_1P_DATA.dbo.HC_Cons_Phone_v T2 ON T.ImportID = T2.ImportID
																  AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
						INNER JOIN SUTTER_1P_DATA.dbo.HC_Cons_Address_v T4 ON T2.PhoneAddrImpID = T4.AddrImpID    
																  AND T2.RE_DB_OwnerShort = T4.RE_DB_OwnerShort
						INNER JOIN SUTTER_1P_DATA.dbo.CHART_PhoneType T3 ON T2.PhoneType = T3.PhoneType
																  AND T2.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
																  AND T.KeyInd = T3.KeyInd
				WHERE T4.AddrPref='TRUE' AND T2.PhoneDoNotCall='TRUE'
				
				UNION ALL -- FOR RECORDS FROM RE 7.95 (CVRX and WBRX)
				SELECT --DISTINCT
						T.RE_DB_OwnerShort ,
						T.ConsID ,
						T.ImportID ,
						T.RE_DB_Id,
						T.NEW_TGT_IMPORT_ID,
						T.KeyInd ,
						T2.PhoneType ,
						T2.PhoneNum ,
						T3.SF_Object ,
						T3.SF_Field_API,
						T2.PhoneDoNotCall
				FROM    SUTTER_1P_DATA.dbo.HC_Constituents_v T
						INNER JOIN SUTTER_1P_DATA.dbo.HC_Cons_Phone_v T2 ON T.ImportID = T2.ImportID
																  AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
						INNER JOIN SUTTER_1P_DATA.dbo.CHART_PhoneType T3 ON T2.PhoneType = T3.PhoneType
																  AND T2.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
																  AND T.KeyInd = T3.KeyInd
				WHERE [T2].[RE_DB_OwnerShort]='WBRX' OR [T2].[RE_DB_OwnerShort]='CVRX'
 
				UNION ALL 
				SELECT DISTINCT   --code to set to true the do not call fields. this way, the pivot fnc won't create dupe rows. 
						T.RE_DB_OwnerShort ,
						T.ConsID ,
						T.ImportID ,
						T.RE_DB_Id,
						T.NEW_TGT_IMPORT_ID,
						T.KeyInd ,
						T2.PhoneType ,
						'TRUE' AS PhoneNum ,
						T3.SF_Object ,
						CASE WHEN T3.SF_Field_API='HomePhone' AND T2.PhoneDoNotCall='TRUE' THEN  'rC_Bios__Home_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='MobilePhone' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Mobile_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='rC_Bios__Work_Phone__c' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Work_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='OtherPhone' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Other_Do_Not_Call__c' ELSE NULL END AS SF_Field_API,
						T2.PhoneDoNotCall
				FROM    SUTTER_1P_DATA.dbo.HC_Constituents_v T
						INNER JOIN SUTTER_1P_DATA.dbo.HC_Cons_Phone_v T2 ON T.ImportID = T2.ImportID
																  AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
						LEFT JOIN SUTTER_1P_DATA.dbo.CHART_PhoneType T3 ON T2.PhoneType = T3.PhoneType
																  AND T2.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
																  AND T.KeyInd = T3.KeyInd
				WHERE T2.PhoneDoNotCall='TRUE' AND  [T2].[RE_DB_OwnerShort]='WBRX' OR [T2].[RE_DB_OwnerShort]='CVRX'
 
 
		END;   --516447
		
		BEGIN--PHONES_2: ASSIGN SEQUENCE and BASE TABLES 
				--PHONES where SEQUENCE >1 WILL BE ADDED in the same table WITH THE ADDITIONAL PHONE NUMBERS, as they'll be consolidated with in the same txt field. 
				--DO NOT INCLUDE ADDITIONAL PHONE NUMBER TYPES this time around. they'll be added later. 
			    
				SELECT 	
				T.RE_DB_OwnerShort, T.ConsID, T.ImportID, T.RE_DB_Id, T.NEW_TGT_IMPORT_ID, T.KeyInd, T.PhoneType, T.PhoneNum, T.SF_Object, T.SF_Field_API, T.PhoneDoNotCall
				,ROW_NUMBER() OVER ( PARTITION BY  T.KeyInd, T.NEW_TGT_IMPORT_ID,  T.SF_Object, T.SF_Field_API
										ORDER BY T.KeyInd, T.NEW_TGT_IMPORT_ID, T.PhoneType ) AS Seq 
				INTO SUTTER_1P_MIGRATION.TBL.Phones_seq		   	
				FROM SUTTER_1P_MIGRATION.TBL.Phones T 
				WHERE (T.SF_Field_API!='Additional_Phone_Numbers__c')   -- do not include additional phones
		
		
		END; --434227
			 
		BEGIN--PHONES_3: create base table for pivot tbl of PHONES where Seq =1
			
				SELECT * 
				INTO SUTTER_1P_MIGRATION.TBL.Phones_seq_1		
				FROM SUTTER_1P_MIGRATION.TBL.Phones_seq
				WHERE Seq='1'
				--414879 
		END;
		
		BEGIN--PHONES_4: create base table for pivot tbl of ADDITIONAL phones;
			 --includes additional numbers where Seq>1  AND those mapped to additional_phone_numbers.
				
					SELECT  *
					INTO SUTTER_1P_MIGRATION.TBL.Phones_seq_2    --all other additional phones.
					FROM    SUTTER_1P_MIGRATION.TBL.Phones_seq
					WHERE   Seq != '1'						--includes numbers where seq>1
					UNION
					SELECT  * , '1' Seq
					FROM    SUTTER_1P_MIGRATION.TBL.Phones
					WHERE   (SF_Field_API = 'Additional_Phone_Numbers__c')  --includes additional numbers. 
					--46895
		END;		
			
		BEGIN--PHONES_5: CREATE final pivot phone tables --PHONE TYPES from SF_Field_API
			  
			EXEC HC_PivotWizard_p   'NEW_TGT_IMPORT_ID',	--fields to include as unique identifier/normally it is just a unique ID field.
										'SF_Field_API',									--column that stores all new phone types
										'PhoneNum',										--phone numbers
										'SUTTER_1P_MIGRATION.TBL.Phones_seq_1_final',			--INTO..     
										'SUTTER_1P_MIGRATION.TBL.Phones_seq_1',					--FROM..
										'PhoneNum is not null'							--WHERE..
	 	END --333848
        
			SELECT * FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_1_final 
			WHERE new_tgt_import_id IN (SELECT NEW_TGT_IMPORT_ID FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_1_final  GROUP BY NEW_TGT_IMPORT_ID HAVING COUNT(*)>1)
			ORDER BY NEW_TGT_IMPORT_ID


		BEGIN--PHONES_6: ADDITIONAL PHONE NUMBERS field
					SELECT DISTINCT T.NEW_TGT_IMPORT_ID,  
						STUFF(( SELECT ' '+ T1.PhoneType +': '+ T1.PhoneNum +'; ' + CHAR(10)
								FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_2 T1
									WHERE T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID AND T1.KeyInd=T.KeyInd
									ORDER BY T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportId
								FOR
									XML PATH('')
									  ), 1, 1, '') AS Additional_Phone_Numbers__c
					INTO SUTTER_1P_MIGRATION.TBL.Phones_seq_2_final   
					FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_2 T
					ORDER BY T.NEW_TGT_IMPORT_ID
		END 	--62346
						 

					/*	BEGIN --test pivot results ACCOUNT
								SELECT RE_DB_OwnerShort, ImportID, KeyInd, COUNT(*) c 
								FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_1
								GROUP BY RE_DB_OwnerShort, ImportID, KeyInd
								HAVING COUNT(*)>1
								ORDER BY c DESC 
								
								SELECT importID, COUNT(*) C FROM SUTTER_1P_MIGRATION.TBL.Phones GROUP BY importid  ORDER BY c desc
								SELECT * FROM SUTTER_1P_MIGRATION.TBL.Phones  where re_db_id!=new_tgt_import_id order by new_Tgt_import_id
								

								SELECT * FROM SUTTER_1P_MIGRATION.TBL.Phones 
								WHERE NEW_TGT_IMPORT_ID='SMCF-000000794'
								SELECT * FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_1		
								WHERE NEW_TGT_IMPORT_ID='SMCF-000000794'
								SELECT * FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_1_final	
								WHERE NEW_TGT_IMPORT_ID='SMCF-000000794'
								SELECT * FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_2		
								WHERE NEW_TGT_IMPORT_ID='SMCF-000000794'
								SELECT * FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_2_final	
								WHERE NEW_TGT_IMPORT_ID='SMCF-000000794'
					
								SELECT * FROM SUTTER_1P_MIGRATION.TBL.Phones 
								WHERE NEW_TGT_IMPORT_ID='ABSF-00001-079-0117809'
								SELECT * FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_1		
								WHERE NEW_TGT_IMPORT_ID='ABSF-00001-079-0117809'
								SELECT * FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_1_final	
								WHERE NEW_TGT_IMPORT_ID='ABSF-00001-079-0117809'
								SELECT * FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_2		
								WHERE NEW_TGT_IMPORT_ID='ABSF-00001-079-0117809'
								SELECT * FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_2_final	
								WHERE NEW_TGT_IMPORT_ID='ABSF-00001-079-0117809'

								SELECT * FROM SUTTER_1P_MIGRATION.TBL.Phones_seq_1
								WHERE rC_Bios__Home_Do_Not_Call__c IS NOT NULL 
								OR rC_Bios__Mobile_Do_Not_Call__c IS NOT NULL 
								OR rC_Bios__Work_Do_Not_Call__c IS NOT NULL 
								OR rC_Bios__Other_Do_Not_Call__c IS NOT NULL 
								ORDER BY ImportID
					
						END;*/
	 
	 
				BEGIN--test for duplicates
							SELECT  NEW_TGT_IMPORT_ID ,
									COUNT(*) c
							FROM    SUTTER_1P_MIGRATION.TBL.Phones_seq_2_final
							GROUP BY NEW_TGT_IMPORT_ID
							HAVING  COUNT(*) > 1
							ORDER BY NEW_TGT_IMPORT_ID
						 
				END;
		 			
END;					
	
BEGIN--INDIVIDUAL RELATIONSHIP PHONES
 		-- SELECT COUNT(*) FROM SUTTER_1P_DATA.DBO.HC_Cons_Phone_v  --96260
 		-- SELECT * FROM SUTTER_1P_MIGRATION.dbo.CHART_PhoneType
		
		
		BEGIN
			DROP TABLE SUTTER_1P_MIGRATION.TBL.IRPhones
			DROP TABLE SUTTER_1P_MIGRATION.TBL.IRPhones_seq
			DROP TABLE SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1
			DROP TABLE SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2
			DROP TABLE SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1_final
			DROP TABLE SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2_final 
		END	
		
		
			 
		BEGIN--PHONES_1: REMOVE DEDUPLICATE PHONE NUMBER and create base table
			
					SELECT DISTINCT
						T.RE_DB_OwnerShort ,
						T.IRPhoneIRImpID,
						T3.RE_DB_ID,
						T3.NEW_TGT_IMPORT_ID_IR,
						T.IRPhoneType,
						T.IRPhoneNum,
						T2.SF_Field_API
					INTO    SUTTER_1P_MIGRATION.TBL.IRPhones    -- DROP TABLE SUTTER_1P_MIGRATION.TBL.IRPhones
					FROM    SUTTER_1P_DATA.dbo.HC_Ind_Relat_Phone_v T
					INNER JOIN SUTTER_1P_DATA.dbo.CHART_IRPhoneType T2 ON T.IRPhoneType= T2.IRPhoneType AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
					INNER JOIN SUTTER_1P_DATA.dbo.HC_Ind_Relat_v T3 ON T.IRPhoneIRImpID=T3.IRImpID AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
					WHERE T3.IRLink='' OR T3.IRLink IS null 
	            	ORDER BY T3.NEW_TGT_IMPORT_ID_IR,  T.IRPhoneType
       				--129462
  		END;		 

		BEGIN--PHONES_2: ASSIGN SEQUENCE and BASE TABLES 
				--PHONES where SEQUENCE >1 WILL BE ADDED in the same table WITH THE ADDITIONAL PHONE NUMBERS, as they'll be consolidated with in the same txt field. 
				--DO NOT INCLUDE ADDITIONAL PHONE NUMBER TYPES this time around. they'll be added later. 
			    
				SELECT 	
				T.RE_DB_OwnerShort, T.RE_DB_ID, T.NEW_TGT_IMPORT_ID_IR, T.IRPhoneIRImpID, T.IRPhoneType, T.IRPhoneNum, T.SF_Field_API
				,ROW_NUMBER() OVER ( PARTITION BY T.NEW_TGT_IMPORT_ID_IR, T.SF_Field_API
										ORDER BY T.NEW_TGT_IMPORT_ID_IR, T.IRPhoneType ) AS Seq 
				INTO SUTTER_1P_MIGRATION.TBL.IRPhones_seq			--DROP TABLE SUTTER_1P_MIGRATION.TBL.IRPhones_seq
				FROM SUTTER_1P_MIGRATION.TBL.IRPhones T 
			
				WHERE (T.SF_Field_API!='Additional_Phone_Numbers__c')   -- do not include additional phones
				--120261
		END;
		
		BEGIN--PHONES_3: create base table for pivot tbl of PHONES where Seq =1
			
				SELECT * 
				INTO SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1		--DROP TABLE SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1
				FROM SUTTER_1P_MIGRATION.TBL.IRPhones_seq
				WHERE Seq='1'
				--116771 
		END;
		
		BEGIN--PHONES_4: create base table for pivot tbl of ADDITIONAL phones;
			 --includes additional numbers where Seq>1  AND those mapped to additional_phone_numbers.
				
					SELECT  *
					INTO SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2    --DROP TABLE SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2 --all other additional phones.
					FROM    SUTTER_1P_MIGRATION.TBL.IRPhones_seq
					WHERE   Seq != '1'						--includes numbers where seq>1
					UNION
					SELECT  * , '1' Seq
					FROM    SUTTER_1P_MIGRATION.TBL.IRPhones
					WHERE   (SF_Field_API = 'Additional_Phone_Numbers__c')  --includes additional numbers. 
		END;		
		--11868
			
		BEGIN--PHONES_5: --CREATE final pivot phone tables --PHONE TYPES from SF_Field_API
			  
			  
				EXEC HC_PivotWizard_p   'NEW_TGT_IMPORT_ID_IR',				--fields to include as unique identifier/normally it is just an ID field.
										'SF_Field_API',									--column that stores all new phone types
										'IRPhoneNum',									--phone numbers
										'SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1_final',			--INTO..     -- DROP TABLE SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1_final
										'SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1',				--FROM..
										'IRPhoneNum is not null'						--WHERE..
				--95367
		
		END;
 
		BEGIN--PHONE_6: ADDITIONAL PHONE NUMBERS field
					 
					SELECT DISTINCT T.NEW_TGT_IMPORT_ID_IR  ,
						STUFF(( SELECT ' '+T1.IRPhoneType +': '+ T1.IRPhoneNum +'; ' + CHAR(10)
								FROM SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2 T1
									WHERE T1.NEW_TGT_IMPORT_ID_IR=T.NEW_TGT_IMPORT_ID_IR  
									ORDER BY T1.RE_DB_OwnerShort, T1.IRPhoneIRImpID
								FOR
									XML PATH('')
									  ), 1, 1, '') AS Additional_Phone_Numbers__c
					INTO SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2_final   --DROP TABLE SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2_final   
					FROM SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2 T
					WHERE T.IRPhoneNum IS NOT null 
					ORDER BY T.NEW_TGT_IMPORT_ID_IR 
				--10683
		END;
		
			
				BEGIN --test pivot results ACCOUNT
						SELECT NEW_TGT_IMPORT_ID_IR, COUNT(*) c 
						FROM SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1_final
						GROUP BY NEW_TGT_IMPORT_ID_IR
						HAVING COUNT(*)>1
						ORDER BY c DESC 
						
						SELECT importID, COUNT(*) C FROM SUTTER_1P_MIGRATION.TBL.Phones GROUP BY importid  ORDER BY c desc
						
						SELECT * FROM SUTTER_1P_MIGRATION.TBL.IRPhones 
						WHERE NEW_TGT_IMPORT_ID_IR='HOTV-00001-518-0000019696'
						SELECT * FROM SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1		
						WHERE NEW_TGT_IMPORT_ID_IR='HOTV-00001-518-0000019696'
						SELECT * FROM SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1_final	
						WHERE NEW_TGT_IMPORT_ID_IR='HOTV-00001-518-0000019696'
						SELECT * FROM SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2		
						WHERE NEW_TGT_IMPORT_ID_IR='HOTV-00001-518-0000019696'
						SELECT * FROM SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2_final	
						WHERE NEW_TGT_IMPORT_ID_IR='HOTV-00001-518-0000019696'
				END;

			  
						
				BEGIN--test for duplicates
							SELECT  NEW_TGT_IMPORT_ID_IR,
									COUNT(*) c
							FROM    SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1_final
							GROUP BY NEW_TGT_IMPORT_ID_IR
							HAVING  COUNT(*) > 1
							ORDER BY NEW_TGT_IMPORT_ID_IR
							
							SELECT  NEW_TGT_IMPORT_ID_IR  ,
									COUNT(*) c
							FROM    SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2_final
							GROUP BY NEW_TGT_IMPORT_ID_IR 
							HAVING  COUNT(*) > 1
							ORDER BY NEW_TGT_IMPORT_ID_IR
				END; 
 
END;	
	
BEGIN--ORGANIZATION RELATIONSHIP PHONES
 		-- SELECT COUNT(*) FROM SUTTER_1P_DATA.DBO.HC_Org_Relat_Phone_v --11525
 		-- SELECT * FROM SUTTER_1P_MIGRATION.dbo.CHART_ORPhoneType
			 
		BEGIN
			DROP TABLE SUTTER_1P_MIGRATION.TBL.ORPhones
			DROP TABLE SUTTER_1P_MIGRATION.TBL.ORPhones_seq
			DROP TABLE SUTTER_1P_MIGRATION.TBL.ORPhones_seq_1
			DROP TABLE SUTTER_1P_MIGRATION.TBL.ORPhones_seq_2
			DROP TABLE SUTTER_1P_MIGRATION.TBL.ORPhones_seq_1_final
			DROP TABLE SUTTER_1P_MIGRATION.TBL.ORPhones_seq_2_final 
		END	
			
	
		BEGIN--PHONES_1: REMOVE DEDUPLICATE PHONE NUMBER and create base table
			
					SELECT DISTINCT
						T.RE_DB_OwnerShort ,
						T.ORPhoneORImpID ,
						T3.RE_DB_Id,
						T3.NEW_TGT_IMPORT_ID_OR,
						T.ORPhoneType,
						T.ORPhoneNum,
						T2.SF_Field_API
					INTO    SUTTER_1P_MIGRATION.TBL.ORPhones    -- DROP TABLE SUTTER_1P_MIGRATION.TBL.ORPhones
					FROM    SUTTER_1P_DATA.dbo.HC_Org_Relat_Phone_v T
					INNER JOIN SUTTER_1P_DATA.dbo.CHART_ORPhoneType T2 ON T.ORPhoneType= T2.ORPhoneType AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
					INNER JOIN SUTTER_1P_DATA.dbo.HC_Org_Relat_v T3 ON T.ORPhoneORImpID=T3.ORImpID  AND T.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
					WHERE T3.ORLink='' OR ORLink IS null 
					ORDER BY NEW_TGT_IMPORT_ID_OR, T.ORPhoneType
       				--15975
		END;		 

		BEGIN--PHONES_2: ASSIGN SEQUENCE and BASE TABLES 
				--PHONES where SEQUENCE >1 WILL BE ADDED in the same table WITH THE ADDITIONAL PHONE NUMBERS, as they'll be consolidated with in the same txt field. 
				--DO NOT INCLUDE ADDITIONAL PHONE NUMBER TYPES this time around. they'll be added later. 
			    
				SELECT 	
				T.RE_DB_OwnerShort, T.ORPhoneORImpID, T.RE_DB_Id,
						T.NEW_TGT_IMPORT_ID_OR, T.ORPhoneType, T.ORPhoneNum, T.SF_Field_API
				,ROW_NUMBER() OVER ( PARTITION BY T.NEW_TGT_IMPORT_ID_OR, T.SF_Field_API
										ORDER BY T.NEW_TGT_IMPORT_ID_OR, T.ORPhoneType ) AS Seq 
				INTO SUTTER_1P_MIGRATION.TBL.ORPhones_seq			--DROP TABLE SUTTER_1P_MIGRATION.TBL.ORPhones_seq
				FROM SUTTER_1P_MIGRATION.TBL.ORPhones T 
			
				WHERE (T.SF_Field_API!='Additional_Phone_Numbers__c')   -- do not include additional phones
				--14794
		END;
		
		BEGIN--PHONES_3: create base table for pivot tbl of PHONES where Seq =1
			
				SELECT * 
				INTO SUTTER_1P_MIGRATION.TBL.ORPhones_seq_1		--DROP TABLE SUTTER_1P_MIGRATION.TBL.ORPhones_seq_1
				FROM SUTTER_1P_MIGRATION.TBL.ORPhones_seq
				WHERE Seq='1'
				--14462
		END;
		
		BEGIN--PHONES_4: create base table for pivot tbl of ADDITIONAL phones;
			 --includes additional numbers where Seq>1  AND those mapped to additional_phone_numbers.
				
					SELECT  *
					INTO SUTTER_1P_MIGRATION.TBL.ORPhones_seq_2    --DROP TABLE SUTTER_1P_MIGRATION.TBL.ORPhones_seq_2 --all other additional phones.
					FROM    SUTTER_1P_MIGRATION.TBL.ORPhones_seq
					WHERE   Seq != '1'						--includes numbers where seq>1
					UNION
					SELECT  * , '1' Seq
					FROM    SUTTER_1P_MIGRATION.TBL.ORPhones
					WHERE   (SF_Field_API = 'Additional_Phone_Numbers__c')  --includes additional numbers. 
		END;	
		--1481	
			
		BEGIN--PHONES_5: --CREATE final pivot phone tables --PHONE TYPES from SF_Field_API
			  
			  
				EXEC HC_PivotWizard_p   'NEW_TGT_IMPORT_ID_OR',				--fields to include as unique identifier/normally it is just an ID field.
										'SF_Field_API',									--column that stores all new phone types
										'ORPhoneNum',									--phone numbers
										'SUTTER_1P_MIGRATION.TBL.ORPhones_seq_1_final',			--INTO..     -- DROP TABLE SUTTER_1P_MIGRATION.TBL.ORPhones_seq_1_final
										'SUTTER_1P_MIGRATION.TBL.ORPhones_seq_1',				--FROM..
										'ORPhoneNum is not null'						--WHERE..
				--9893
			
		END;

		BEGIN--PHONE_6: ADDITIONAL PHONE NUMBERS field
					 
			SELECT DISTINCT T.NEW_TGT_IMPORT_ID_OR,  
				STUFF(( SELECT ' '+T1.ORPhoneType +': '+ T1.ORPhoneNum +'; ' + CHAR(10)
						FROM SUTTER_1P_MIGRATION.TBL.ORPhones_seq_2 T1
							WHERE T1.NEW_TGT_IMPORT_ID_OR=T.NEW_TGT_IMPORT_ID_OR
							ORDER BY T1.NEW_TGT_IMPORT_ID_OR
						FOR
							XML PATH('')
								), 1, 1, '') AS Additional_Phone_Numbers__c
			INTO SUTTER_1P_MIGRATION.TBL.ORPhones_seq_2_final   --DROP TABLE SUTTER_1P_MIGRATION.TBL.ORPhones_seq_2_final   
			FROM SUTTER_1P_MIGRATION.TBL.ORPhones_seq_2 T
			WHERE T.ORPhoneNum IS NOT null 
			ORDER BY T.NEW_TGT_IMPORT_ID_OR 
		
		END;
		--1368
			
				BEGIN --test pivot results ACCOUNT
						SELECT NEW_TGT_IMPORT_ID_OR, COUNT(*) c 
						FROM SUTTER_1P_MIGRATION.TBL.ORPhones_seq_1
						GROUP BY NEW_TGT_IMPORT_ID_OR
						HAVING COUNT(*)>1
						ORDER BY c DESC 
						
						SELECT * FROM SUTTER_1P_MIGRATION.TBL.ORPhones 
						WHERE NEW_TGT_IMPORT_ID_OR='EMCF-06307-700-0008186'
					 
						SELECT * FROM SUTTER_1P_MIGRATION.TBL.ORPhones_seq_1		
						WHERE NEW_TGT_IMPORT_ID_OR='EMCF-06307-700-0008186'
						 
						SELECT * FROM SUTTER_1P_MIGRATION.TBL.ORPhones_seq_1_final	
						WHERE NEW_TGT_IMPORT_ID_OR='EMCF-06307-700-0008186'
						 
						SELECT * FROM SUTTER_1P_MIGRATION.TBL.ORPhones_seq_2		
						WHERE NEW_TGT_IMPORT_ID_OR='EMCF-06307-700-0008186'
					 
						SELECT * FROM SUTTER_1P_MIGRATION.TBL.ORPhones_seq_2_final	
						WHERE NEW_TGT_IMPORT_ID_OR='EMCF-06307-700-0008186'
			 	
				END;
		 
	 					
				BEGIN--test for duplicates
							SELECT  NEW_TGT_IMPORT_ID_OR, COUNT(*) c
							FROM    SUTTER_1P_MIGRATION.TBL.ORPhones_seq_1_final
							GROUP BY NEW_TGT_IMPORT_ID_OR 
							HAVING  COUNT(*) > 1
							ORDER BY NEW_TGT_IMPORT_ID_OR
							
							SELECT  NEW_TGT_IMPORT_ID_OR, COUNT(*) c
							FROM    SUTTER_1P_MIGRATION.TBL.ORPhones_seq_2_final
							GROUP BY NEW_TGT_IMPORT_ID_OR
							HAVING  COUNT(*) > 1
							ORDER BY NEW_TGT_IMPORT_ID_OR
				END;		

END;	

BEGIN--EDUCATION RELATIONSHIPS TABLES 			 
		
		BEGIN
			DROP TABLE SUTTER_1P_MIGRATION.TBL.ESRSchoolName	 
			DROP TABLE SUTTER_1P_MIGRATION.TBL.Education_Major 
			DROP TABLE SUTTER_1P_MIGRATION.TBL.Education_Minor 
		END;
	
		BEGIN--BASE TABLE EDUCATION RELATIONSHIPS 
			
		
				SELECT	T.RE_DB_OwnerShort
						,T.ESRImpID
						,T.ESRSchoolName AS Name
						,CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ISNULL(UPPER(T.ESRSchoolName),' '))), 3, 20) AS NVARCHAR(20)) 
						AS Unique_MD5	
				INTO SUTTER_1P_MIGRATION.TBL.ESRSchoolName	 
				FROM SUTTER_1P_DATA.dbo.HC_Educ_Relat_v T
				ORDER BY T.ESRSchoolName
	 	END; 


		BEGIN--UPDATE DATES TO COMPLY WITH TRANSLATION RULES
				SELECT ESRDateGrad, ESRDateLeft
				FROM SUTTER_1P_DATA.dbo.HC_Educ_Relat_v 
				GROUP BY ESRDateGrad, ESRDateLeft
				
				UPDATE SUTTER_1P_DATA.dbo.HC_Educ_Relat_v  SET ESRDateGrad=ESRDateLeft WHERE ESRDateGrad IS NULL 
		END;

		BEGIN--EDUCATION RELATIONSHIP MAJOR TO MULTIPICKLIST

						SELECT DISTINCT T.RE_DB_OwnerShort, T.ESRMajESRImpID,  
							STUFF(( SELECT '; ' + T1.ESRMajMajor 
									FROM SUTTER_1P_DATA.dbo.HC_Educ_Relat_Major_v T1
										WHERE T1.ESRMajESRImpID=T.ESRMajESRImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
										GROUP BY T1.ESRMajMajor
									FOR
										XML PATH('')
										  ), 1, 1, '') AS Education_Major__c
						INTO SUTTER_1P_MIGRATION.TBL.Education_Major    
						FROM SUTTER_1P_DATA.dbo.HC_Educ_Relat_Major_v T
						WHERE T.ESRMajMajor IS NOT NULL
						ORDER BY T.RE_DB_OwnerShort, T.ESRMajESRImpID
		END;


		BEGIN--EDUCATION RELATIONSHIP MINOR TO MULTIPICKLIST

						SELECT DISTINCT T.RE_DB_OwnerShort, T.ESRMinESRImpID,  
							STUFF(( SELECT '; ' + T1.ESRMinMinor 
									FROM SUTTER_1P_DATA.dbo.HC_Educ_Relat_Minor_v T1
										WHERE T1.ESRMinESRImpID=T.ESRMinESRImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
										GROUP BY T1.ESRMinMinor
									FOR
										XML PATH('')
										  ), 1, 1, '') AS Education_Minor__c
						INTO SUTTER_1P_MIGRATION.TBL.Education_Minor     
						FROM SUTTER_1P_DATA.dbo.HC_Educ_Relat_Minor_v T
						WHERE T.ESRMinMinor IS NOT NULL
						ORDER BY T.RE_DB_OwnerShort, T.ESRMinESRImpID
		END;
END;
  
BEGIN--CONSTITUENT ATTRIBUTES TO CONTACT 
		
		SELECT * FROM SUTTER_1P_DATA.dbo.CHART_Attributes T2 WHERE [T2].[SF_Object_2]='contact'
		
		DROP TABLE SUTTER_1P_MIGRATION.TBL.Attribute_contact
		
		DROP TABLE SUTTER_1P_MIGRATION.TBL.Attributes_to_Contact
		
		
		BEGIN 	
				--BASE TABLE CONS ATTR TO CONTACT  -- remove duplicates if present
				SELECT DISTINCT T.RE_DB_OwnerShort, T.ImportID, T1.RE_DB_Id, T1.NEW_TGT_IMPORT_ID, T2.SF_Object_2, T2.SF_Field_2, 
				CASE WHEN (T2.SF_Field_2='Agefinder_Birthdate__c' AND LEN(T.CAttrDesc)='6') THEN right(T.CAttrDesc,2)+'/'+left(T.CAttrDesc,4) 
					 WHEN (T2.SF_Field_2='Agefinder_Birthdate__c' AND LEN(T.CAttrDesc)='8' AND T.CAttrDesc NOT LIKE '%/%') 
						  THEN SUBSTRING(T.CAttrDesc, 5, 2)+'/'+right(T.CAttrDesc,2)+'/'+left(T.CAttrDesc,4) 
					 ELSE T.CAttrDesc END AS SF_FieldValue ,T.CAttrDesc refCAttrDesc ,LEN(T.CAttrDesc) refFieldLenght
				
				INTO SUTTER_1P_MIGRATION.TBL.Attribute_contact
				FROM SUTTER_1P_DATA.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_DATA.dbo.CHART_Attributes T2 ON T.CAttrCat=T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T1.KeyInd='I' AND T2.SF_Object_2='CONTACT' AND T.CAttrDesc IS NOT null
				 
			UNION
			--ADDR ATTR TO CONTACT
				SELECT DISTINCT T1.RE_DB_OwnerShort, T1.ImportID, T3.RE_DB_Id, T3.NEW_TGT_IMPORT_ID,  T2.SF_Object_2, T2.SF_Field_2, 
					t.CADAttrCom AS SF_FieldValue, T.CADAttrDesc refCAttrDesc, LEN(T.CADAttrCom) refFieldLength
				FROM SUTTER_1P_DATA.dbo.HC_Cons_Address_Attr_v T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Cons_Address_v T1 ON T.CADAttrAddrImpID=T1.AddrImpID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T3 ON T1.ImportID=T3.ImportID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_DATA.dbo.CHART_Attributes T2 ON T.CADAttrCat =T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.SF_Object_2='CONTACT'  
		END; 
		--92418
	
		BEGIN
        
			DROP TABLE SUTTER_1P_MIGRATION.TBL.Attribute_contact_b
			SELECT NEW_TGT_IMPORT_ID, SF_Object_2, SF_Field_2, MAX(SF_FieldValue) AS SF_FieldValue
			INTO SUTTER_1P_MIGRATION.TBL.Attribute_contact_b
			FROM SUTTER_1P_MIGRATION.TBL.Attribute_contact
			GROUP BY NEW_TGT_IMPORT_ID, SF_Object_2, SF_Field_2
 		END
        
		BEGIN--ATTRIBUTE to CONTACT - MULTIPICKLIST

				DROP TABLE SUTTER_1P_MIGRATION.TBL.Attributes_to_Contact 

				SELECT DISTINCT T.NEW_TGT_IMPORT_ID,
					STUFF(( SELECT '; ' + T1.SF_FieldValue 
							FROM SUTTER_1P_MIGRATION.TBL.Attribute_contact T1
								WHERE T1.SF_Field_2='Agefinder_Birthdate__c' AND T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID
								GROUP BY T1.SF_FieldValue
							FOR
								XML PATH('')
								  ), 1, 1, '') AS Agefinder_Birthdate__c
								  
					,STUFF(( SELECT '; ' + T1.SF_FieldValue 
							FROM SUTTER_1P_MIGRATION.TBL.Attribute_contact T1
								WHERE T1.SF_Field_2='CMS_Age__c' AND T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID
								GROUP BY T1.SF_FieldValue
							FOR
								XML PATH('')
								  ), 1, 1, '') AS CMS_Age__c
					,STUFF(( SELECT '; ' + T1.SF_FieldValue 
							FROM SUTTER_1P_MIGRATION.TBL.Attribute_contact T1
								WHERE T1.SF_Field_2='PhoneFinder__c' AND T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID
								GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
							FOR
								XML PATH('')
								  ), 1, 1, '') AS PhoneFinder__c
					,STUFF(( SELECT '; ' + T1.SF_FieldValue 
							FROM SUTTER_1P_MIGRATION.TBL.Attribute_contact T1
								WHERE T1.SF_Field_2='SMCF_Employee_ID__c' AND T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID
								GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
							FOR
								XML PATH('')
								  ), 1, 1, '') AS SMCF_Employee_ID__c
					,STUFF(( SELECT '; ' + T1.SF_FieldValue 
							FROM SUTTER_1P_MIGRATION.TBL.Attribute_contact T1
								WHERE T1.SF_Field_2='SAFH_Employee_ID__c' AND T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID
								GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
							FOR
								XML PATH('')
								  ), 1, 1, '') AS SAFH_Employee_ID__c
		 		INTO SUTTER_1P_MIGRATION.TBL.Attributes_to_Contact 
				FROM SUTTER_1P_MIGRATION.TBL.Attribute_contact_b T
				ORDER BY T.NEW_TGT_IMPORT_ID


		END;
		
		
		BEGIN--CHECK DUPLICATES
			SELECT *
			FROM SUTTER_1P_MIGRATION.TBL.Attributes_to_Contact 
			WHERE NEW_TGT_IMPORT_ID IN (SELECT NEW_TGT_IMPORT_ID FROM SUTTER_1P_MIGRATION.TBL.Attributes_to_Contact  GROUP BY NEW_TGT_IMPORT_ID HAVING COUNT(*)>1)
			ORDER BY NEW_TGT_IMPORT_ID
		END;   50868

END;

BEGIN--ACTION ATTRIBUTES TO TASK and new object ACTION

			 DROP TABLE SUTTER_1P_MIGRATION.TBL.Attribute_task
			 DROP TABLE SUTTER_1P_MIGRATION.TBL.Attribute_task_1
			
			
			--CREATE BASE TABLE
			SELECT DISTINCT T.RE_DB_OwnerShort, T.ACImpID,  T2.SF_Object_2, T2.SF_Field_2, 
			CASE WHEN T.ACAttrCat='Thank-you Call' THEN 'TRUE' ELSE T.ACAttrDesc END AS SF_FieldValue, 
			T.ACAttrDesc AS refCAttrDesc, t.ACAttrCom,
			LEN(T.ACAttrDesc) refFieldLength 
			INTO SUTTER_1P_MIGRATION.TBL.Attribute_task
			FROM SUTTER_1P_DATA.dbo.HC_Cons_Action_Attr_v T
			INNER JOIN SUTTER_1P_DATA.dbo.CHART_Attributes T2 ON T.ACAttrCat =T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.[type]='ACTION' AND T2.SF_Object_2 IS NOT NULL AND T2.[SF_Object_2]!=''	
 			ORDER BY T.RE_DB_OwnerShort, T.ACImpID, T2.SF_Field_2

			SELECT * FROM SUTTER_1P_MIGRATION.TBL.Attribute_task 
			WHERE SF_FieldValue IS null

			DELETE SUTTER_1P_MIGRATION.TBL.Attribute_task 
			WHERE SF_FieldValue IS null

			--check duplicates. 
			 		BEGIN
					SELECT * FROM SUTTER_1P_MIGRATION.tbl.Attribute_task
					WHERE (RE_DB_OwnerShort+ACImpID+SF_Object_2+[SF_Field_2])
						IN (SELECT RE_DB_OwnerShort+ACImpID+SF_Object_2+[SF_Field_2]
							FROM SUTTER_1P_MIGRATION.tbl.Attribute_task
							GROUP BY RE_DB_OwnerShort+ACImpID+SF_Object_2+[SF_Field_2]
							HAVING COUNT(*)>1)
					ORDER BY RE_DB_OwnerShort+ACImpID+SF_Object_2+[SF_Field_2]
					END

	  			
			--CREATE FINAL TABLE. -- multi-select picklists. "Fields" need to be treated separated. 
			SELECT DISTINCT T.RE_DB_OwnerShort, T.ACImpID,  
				STUFF(( SELECT '; ' + T1.SF_FieldValue 
						FROM SUTTER_1P_MIGRATION.TBL.Attribute_task T1
							WHERE T1.SF_Field_2='RE_Team_Member__c' AND T1.ACImpID=T.ACImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS RE_Team_Member__c
				,STUFF(( SELECT '; ' + T1.SF_FieldValue 
						FROM SUTTER_1P_MIGRATION.TBL.Attribute_task T1
							WHERE T1.SF_Field_2='Thank_you_call__c' AND T1.ACImpID=T.ACImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS Thank_you_call__c
				,STUFF(( SELECT '; ' + T1.SF_FieldValue 
						FROM SUTTER_1P_MIGRATION.TBL.Attribute_task T1
							WHERE T1.SF_Field_2='Identified_By__c' AND T1.ACImpID=T.ACImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS Identified_By__c
				,STUFF(( SELECT '; ' + T1.SF_FieldValue 
						FROM SUTTER_1P_MIGRATION.TBL.Attribute_task T1
							WHERE T1.SF_Field_2='Planned_Gift_Amount__c' AND T1.ACImpID=T.ACImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS Planned_Gift_Amount__c
				,STUFF(( SELECT '; ' + T1.SF_FieldValue 
						FROM SUTTER_1P_MIGRATION.TBL.Attribute_task T1
							WHERE T1.SF_Field_2='Requested_Amount__c' AND T1.ACImpID=T.ACImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS Requested_Amount__c

			INTO SUTTER_1P_MIGRATION.TBL.Attribute_task_1  
			FROM SUTTER_1P_MIGRATION.TBL.Attribute_task T
			WHERE T.SF_FieldValue IS NOT NULL    
			 ORDER BY T.RE_DB_OwnerShort, T.ACImpID
	 	  
		BEGIN 
			UPDATE SUTTER_1P_MIGRATION.TBL.Attribute_task_1  SET RE_Team_Member__c=LTRIM(RE_Team_Member__c) 
			UPDATE SUTTER_1P_MIGRATION.TBL.Attribute_task_1  SET Thank_you_call__c=LTRIM(Thank_you_call__c) 
		END

		--check duplicates. 
		BEGIN
			SELECT T.RE_DB_OwnerShort, T.ACImpID, COUNT(*) C
			FROM SUTTER_1P_MIGRATION.TBL.Attribute_task_1 AS T
			GROUP BY T.RE_DB_OwnerShort, T.ACImpID
			ORDER BY C DESC
		END
 END;

BEGIN--ACTION ATTRIBUTES TO TASK and new object ACTION --CHART_ActionAttribute.

			SELECT * FROM SUTTER_1P_DATA.dbo.CHART_ActionAttribute WHERE [CONVERT]='YES' AND SF_Field_2_Value ='NULL'
			 
			DROP TABLE SUTTER_1P_MIGRATION.TBL.ActionAttribute
			DROP TABLE SUTTER_1P_MIGRATION.TBL.ActionAttribute_final
			--CREATE BASE TABLE
			BEGIN	
					SELECT DISTINCT T.RE_DB_OwnerShort, T.ACImpID,  T2.SF_Object_1 AS SF_Object, T2.SF_Field_1 AS SF_Field, T2.SF_Field_1_Value AS SF_Field_Value 
					INTO SUTTER_1P_MIGRATION.TBL.ActionAttribute
					FROM SUTTER_1P_DATA.dbo.HC_Cons_Action_Attr_v T
					INNER JOIN SUTTER_1P_DATA.dbo.CHART_ActionAttribute T2 ON T.ACAttrCat =T2.ACAttrCat AND T.ACAttrDesc =T2.ACAttrDesc AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
					WHERE T2.[Convert]='Yes' AND T2.SF_Field_1_Value  IS NOT NULL AND T2.SF_Field_1_Value !=''
 					UNION ALL
					SELECT DISTINCT T.RE_DB_OwnerShort, T.ACImpID,  T2.SF_Object_1 AS SF_Object, T2.SF_Field_2 AS SF_Field, SF_Field_2_Value AS SF_Field_Value 
					 
					FROM SUTTER_1P_DATA.dbo.HC_Cons_Action_Attr_v T
					INNER JOIN SUTTER_1P_DATA.dbo.CHART_ActionAttribute T2 ON T.ACAttrCat =T2.ACAttrCat AND T.ACAttrDesc =T2.ACAttrDesc AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
					WHERE T2.[Convert]='Yes' AND T2.SF_Field_2_Value  IS NOT NULL AND T2.SF_Field_2_Value !=''
			END 
			
			--SELECT distinct sf_field FROM SUTTER_1P_MIGRATION.TBL.ActionAttribute order by sf_field
			-- 
			SELECT DISTINCT SF_Field_Value FROM SUTTER_1P_MIGRATION.TBL.ActionAttribute ORDER BY SF_Field_Value

			BEGIN--FINAL TABLE
					EXEC HC_PivotWizard_p   'RE_DB_OwnerShort, ACImpID',				--fields to include as unique identifier/normally it is just an ID field.
								'SF_Field',												--column that stores all new fields
								'SF_Field_Value',										--values 
								'SUTTER_1P_MIGRATION.TBL.ActionAttribute_final',		--INTO..     
								'SUTTER_1P_MIGRATION.TBL.ActionAttribute',				--FROM..
								'SF_Field is not null'								--WHERE..
							--571
			END;
 
			--CHECK DUPES
			SELECT * 
			FROM SUTTER_1P_MIGRATION.TBL.ActionAttribute_final T1
			INNER JOIN (SELECT RE_DB_OwnerShort, ACImpID FROM  SUTTER_1P_MIGRATION.TBL.ActionAttribute_final
						GROUP BY RE_DB_OwnerShort, ACImpID HAVING COUNT(*)>1) T2 ON T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort AND T1.ACImpID=T2.ACImpID  
			ORDER BY T1.RE_DB_OwnerShort, T1.ACImpID
   
END; 

BEGIN--CONSTITUENT ACTION SOLICITOR TO MULTI-PICKLIST

		 DROP TABLE SUTTER_1P_MIGRATION.TBL.Cons_Action_Solicitor
		 DROP TABLE SUTTER_1P_MIGRATION.TBL.Cons_Action_Solicitor_1


		BEGIN--BASE TABLE	
			SELECT DISTINCT T.RE_DB_OwnerShort, T.ACImpID, T1.ImportID, T1.Name
			INTO SUTTER_1P_MIGRATION.TBL.Cons_Action_Solicitor
			FROM SUTTER_1P_DATA.dbo.HC_Cons_Action_Solicitor_v T
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T1 ON T.ACSolImpID=T1.ImportID AND  T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			ORDER BY T.RE_DB_OwnerShort, T.ACImpID, T1.ImportID
		END;
		
		BEGIN--MULTI-SELECT PICKLIST
			SELECT DISTINCT T.RE_DB_OwnerShort, T.ACImpID
				,STUFF(( SELECT '; ' + T1.Name 
						FROM SUTTER_1P_MIGRATION.TBL.Cons_Action_Solicitor T1
							WHERE T1.ACImpID=T.ACImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.Name ORDER BY T1.Name DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS RE_Action_Solicitor__c
			INTO SUTTER_1P_MIGRATION.TBL.Cons_Action_Solicitor_1 -- select * from SUTTER_1P_MIGRATION.TBL.Cons_Action_Solicitor_1
			FROM SUTTER_1P_MIGRATION.TBL.Cons_Action_Solicitor T
			ORDER BY T.RE_DB_OwnerShort, T.ACImpID
		END;
		
		BEGIN 
			UPDATE SUTTER_1P_MIGRATION.TBL.Cons_Action_Solicitor_1  SET RE_Action_Solicitor__c=LTRIM(RE_Action_Solicitor__c) 
		END
		
END;
 
BEGIN--[CHART_ActionSolicitor] ACTION SOLICITOR TO OWNER ID (Assigned TO)
/* NOT NEEDED. ROW_NUMBER() OVER ( PARTITION BY T1.RE_DB_OwnerShort, T1.ACImpID
									ORDER BY T1.RE_DB_OwnerShort, T1.ACImpID, T1.ACSolImpID) AS Seq,*/


			DROP TABLE SUTTER_1P_MIGRATION.TBL.ActionSolicitor_OwnerId
			--ACTIONS W/ACTION SOLICITOR 
			SELECT	T1.RE_DB_OwnerShort, 
					T1.ACImpID, 
 					T2.SF_User_Email,
					T3.ID AS OwnerId,
					T1.ACSolImpID, 
					T.[AddedBy],
					T2.[Notes] AS zrefNotes	,
					T2.[SF_User_Email] AS zrefSFEmail

			INTO SUTTER_1P_MIGRATION.TBL.ActionSolicitor_OwnerId
			FROM [SUTTER_1P_DATA].dbo.[HC_Cons_Action_v] T 
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Cons_Action_Solicitor_v T1 ON T.[ACImpID]=T1.[ACImpID] AND T.[RE_DB_OwnerShort]=T1.[RE_DB_OwnerShort]
			INNER JOIN SUTTER_1P_DATA.[dbo].[CHART_ActionSolicitor] T2 ON T1.ACSolImpID=T2.ACSolImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			INNER JOIN SUTTER_1P_MIGRATION.XTR.USERS T3 ON T2.SF_User_Email=T3.EMAIL
			--242964
 
	 		--ACTIONS WITHOUT/ACTION SOLICITOR, BUT ADDED BY AVAILABLE.
			INSERT INTO SUTTER_1P_MIGRATION.TBL.ActionSolicitor_OwnerId
						([RE_DB_OwnerShort],  [ACImpID],  SF_User_Email,  OwnerId,  ACSolImpID,  [AddedBy], zrefNotes, zrefSFEmail)
			SELECT DISTINCT T1.[RE_DB_OwnerShort], 
							T1.[ACImpID],  
							T2.SF_User_Email, 
							X3.ID AS OwnerId, 
							NULL AS ACSolImpID,
							t1.[AddedBy]  ,
							T2.[Notes] AS zrefNotes	,
							T2.[SF_User_Email] AS zrefSFEmail

			FROM	   [SUTTER_1P_DATA].dbo.[HC_Cons_Action_v] T1 
			LEFT JOIN  [SUTTER_1P_DATA].dbo.HC_Cons_Action_Solicitor_v T ON T1.[ACImpID]=T.[ACImpID] AND T1.[RE_DB_OwnerShort]=T.[RE_DB_OwnerShort]
 			LEFT JOIN [SUTTER_1P_DATA].dbo.[CHART_ActionSolicitor] T2 ON T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort] AND T1.[AddedBy]=T2.[AddedBy]
			LEFT JOIN SUTTER_1P_MIGRATION.XTR.USERS X3 ON T2.SF_User_Email=X3.Email
			WHERE (T.[ACImpID] IS NULL AND T.[RE_DB_OwnerShort] IS NULL)   -- RECORDS NOT IN TABLE ABOVE. THOSE HAVE A ACTION SOLICITOR ASSIGNED. 
			AND T1.[AddedBy]!='NULL'  --EXCLUDE RECORDS WHERE ADDEDBY = NULl to PREVENT DUPLICATES FROM CHART 
		--585554	 
		 			 
			-- ALL OTHERS WITH NO ACTION SOLICITOR, NO ADDEDBY, BUT THE ADDED BY = NULL. 
 			INSERT INTO SUTTER_1P_MIGRATION.TBL.ActionSolicitor_OwnerId
						([RE_DB_OwnerShort],  [ACImpID],  SF_User_Email,  OwnerId,  ACSolImpID,  [AddedBy], zrefNotes, zrefSFEmail)
			SELECT DISTINCT T1.[RE_DB_OwnerShort], 
							T1.[ACImpID],  
							'pdst@sutterhealth.org' AS SF_User_Email, 
							'00561000001hr8LAAQ' AS OwnerId, 
							NULL AS ACSolImpID,
							t1.[AddedBy] ,
							NULL AS zrefNotes, 
							NULL AS zrefSFEmail
			FROM	   [SUTTER_1P_DATA].dbo.[HC_Cons_Action_v] T1 
	 		LEFT JOIN SUTTER_1P_MIGRATION.TBL.ActionSolicitor_OwnerId T ON T1.[ACImpID]=T.[ACImpID] AND T1.[RE_DB_OwnerShort]=T.[RE_DB_OwnerShort]
			WHERE T.[ACImpID] IS NULL AND T.[RE_DB_OwnerShort] IS NULL 
		
			-- SELECT * FROM SUTTER_1P_MIGRATION.TBL.ActionSolicitor_OwnerId T  WHERE T.RE_DB_OwnerShort='MPHF' AND T.[ACImpID]='00001-504-0000006648'
 
		--CHECK DUPES
			SELECT * 
			FROM SUTTER_1P_MIGRATION.TBL.ActionSolicitor_OwnerId T1
			INNER JOIN (SELECT [ACImpID], RE_DB_OwnerShort FROM SUTTER_1P_MIGRATION.TBL.ActionSolicitor_OwnerId 
						GROUP BY [ACImpID], RE_DB_OwnerShort HAVING COUNT(*)>1) AS T2 ON T1.[ACImpID]=T2.[ACImpID] AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			ORDER BY T1.RE_DB_OwnerShort , T1.[ACImpID]

			--CHECK NISSING ACTION
 			SELECT * 
			FROM  [SUTTER_1P_DATA].dbo.[HC_Cons_Action_v] T1
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.ActionSolicitor_OwnerId T2 ON T1.[ACImpID]=T2.[ACImpID] AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE  T2.[ACImpID] IS NULL AND T2.[RE_DB_OwnerShort] IS NULL

END

BEGIN--CONSTITUENT ACTION NOTES compiled
		
		BEGIN 
			DROP TABLE SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes  
			DROP TABLE SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes_1
		END
		

		BEGIN--BASE TABLE
			SELECT T.RE_DB_OwnerShort, T.CALink, 
			[Description] = 'Category: '+ T1.ACCat +'; ' + CHAR(10) +
							+'Date: ' + CASE WHEN LEN(T.CANoteDate)=8 THEN CAST(CONVERT(DATE, T.CANoteDate, 101) AS NVARCHAR(20)) 
										     WHEN LEN(T.CANoteDate)=6 THEN CAST(CONVERT(DATE, (T.CANoteDate+'01'), 101) AS NVARCHAR(20)) 
											 WHEN LEN(T.CANoteDate)=4 THEN CAST(CONVERT(DATE, (T.CANoteDate+'0101'), 101) AS NVARCHAR(20)) END  +  CHAR(10) +
							+'Type: ' + T.CANoteType +'; ' + CHAR(10) +
							+ CASE WHEN T.CANoteTitle IS NOT NULL THEN COALESCE('Author: ' + LTRIM(RTRIM(T.CANoteAuthor)), '') ELSE '' END +  CHAR(10) +  --+'Author: ' + T.CANoteAuthor +'; ' + CHAR(10) +
							+ CASE WHEN T.CANoteTitle IS NOT NULL THEN COALESCE('Title: ' + LTRIM(RTRIM(T.CANoteTitle)), '') ELSE '' END +  CHAR(10) +
							+ CASE WHEN T.CANoteDesc IS NOT NULL THEN COALESCE('Desc: ' + LTRIM(RTRIM(T.CANoteDesc)), '') ELSE '' END +  CHAR(10) +
							+ CASE WHEN T.CANoteNotes IS NOT NULL THEN COALESCE('Notes: ' + LTRIM(RTRIM(T.CANoteNotes)), '') ELSE '' END
			INTO SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes  
			FROM SUTTER_1P_DATA.DBO.HC_Cons_Action_Notes_v T
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Cons_Action_v T1 ON T.CALink=T1.ACImpID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			ORDER BY T.CALink, T.CANoteDate DESC
		END;
	 		
		BEGIN--MULTI-SELECT PICKLIST (CONCATENATE MULTIPLE NOTES. 
			SELECT DISTINCT T.RE_DB_OwnerShort, T.CALink
				,STUFF(( SELECT '. ' + CHAR(10)+ T1.[Description] 
						FROM SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes T1
							WHERE T1.CALink=T.CALink AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.[Description] --ORDER BY T1.[Description] DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS [Description]
			INTO SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes_1		-- drop table SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes_1
			FROM SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes T
			ORDER BY T.RE_DB_OwnerShort, T.CALink
		END;


			 /* TEST 
			 SELECT CALink, COUNT (*) c  FROM SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes GROUP BY CALink HAVING COUNT(*)>1 ORDER BY C DESC
			 SELECT CALink, COUNT (*) c  FROM SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes_1 GROUP BY CALink HAVING COUNT(*)>1 ORDER BY C DESC
			 SELECT * FROM SUTTER_1P_MIGRATION.TBL.CONS_ACTION_NOTES WHERE CALINK ='03947-504-0000015601'
			 SELECT * FROM SUTTER_1P_MIGRATION.TBL.CONS_ACTION_NOTES_1 WHERE CALINK ='03947-504-0000015601' or CALINK='03947-504-0000045452'
		
			 */  
END;

BEGIN--CONSTITUENT ACTION NOTE. MOST RECENT ACTION NOTE TO ADD "DESCRIPTION" INTO TASK SUBJECT LINE.
			--BASE TABLE
			
			DROP TABLE SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes_desc
			
			SELECT T.RE_DB_OwnerShort, T.CALink, T.CANoteDate, T.CANoteDesc,
			ROW_NUMBER() OVER ( PARTITION BY T.RE_DB_OwnerShort, T.CALink
											ORDER BY T.RE_DB_OwnerShort, T.CALink, 
											CASE WHEN LEN(T.CANoteDate)=8 THEN CAST(CONVERT(DATE, T.CANoteDate, 101) AS NVARCHAR(20)) 
										     WHEN LEN(T.CANoteDate)=6 THEN CAST(CONVERT(DATE, (T.CANoteDate+'01'), 101) AS NVARCHAR(20)) 
											 WHEN LEN(T.CANoteDate)=4 THEN CAST(CONVERT(DATE, (T.CANoteDate+'0101'), 101) AS NVARCHAR(20)) END ) AS Seq 
			INTO SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes_desc 
			FROM SUTTER_1P_DATA.DBO.HC_Cons_Action_Notes_v T
			WHERE T.CANoteDesc IS NOT NULL AND T.CANoteDesc!=''
			ORDER BY T.RE_DB_OwnerShort, T.CALink, CASE WHEN LEN(T.CANoteDate)=8 THEN CAST(CONVERT(DATE, T.CANoteDate, 101) AS NVARCHAR(20)) 
										     WHEN LEN(T.CANoteDate)=6 THEN CAST(CONVERT(DATE, (T.CANoteDate+'01'), 101) AS NVARCHAR(20)) 
											 WHEN LEN(T.CANoteDate)=4 THEN CAST(CONVERT(DATE, (T.CANoteDate+'0101'), 101) AS NVARCHAR(20)) END DESC
		
				--CHECK FOR SEQ NUMBERS (MULTIPLE NOTES/ACTION)
				SELECT * FROM SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes_desc WHERE SEQ!='1'
				--DELETE SEQ NOT 1
				DELETE SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes_desc WHERE SEQ!='1'
				--CHECK DUPLICATE
				SELECT T.RE_DB_OwnerShort, T.CALink, COUNT(*) c
				FROM SUTTER_1P_MIGRATION.TBL.Cons_Action_Notes_desc  T
				GROUP BY T.RE_DB_OwnerShort, T.CALink 
				HAVING COUNT(*)>1


END;
 
BEGIN--ADDITIONAL_ADD_SAL

	USE SUTTER_1P_DATA
	 
	
	--TBL ADDITIONAL ADD SAL combined in one row when 2 of Sames AddSal type are added. 

		--remove tables
		BEGIN	
					DROP TABLE SUTTER_1P_MIGRATION.TBL.AddtlAddSal
					DROP TABLE SUTTER_1P_MIGRATION.TBL.AddtlAddSal_1
					DROP TABLE SUTTER_1P_MIGRATION.TBL.AddtlAddSal_final
		END;

		--create master table
		BEGIN 
					SELECT T1.AddSalImpID, T1.ImportID, T1.RE_DB_Tbl, T1.RE_DB_OwnerShort, T3.RE_DB_ID, T3.NEW_TGT_IMPORT_ID,
					T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.ImportID +'-'+T2.SALUTATION_TYPE AS UniqueId, 
					T1.AddSalText, T2.SALUTATION_TYPE, T2.SF_FIELD , T2.Additional_Recognition_Subtype__c
					INTO SUTTER_1P_MIGRATION.TBL.AddtlAddSal
					FROM SUTTER_1P_DATA.dbo.HC_Cons_Addl_Addressee_v T1
					INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T3 ON T1.ImportID=T3.ImportID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
					INNER JOIN SUTTER_1P_DATA.[dbo].[CHART_AddSalType] T2 ON T1.AddSalType=T2.AddSalType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
					WHERE T2.[Convert]='Yes'
					ORDER BY T1.RE_DB_OwnerShort, T1.ImportID, T2.SALUTATION_TYPE, SF_FIELD DESC  
		END;		--462631
						 
			--test
			BEGIN 
					SELECT * FROM SUTTER_1P_MIGRATION.TBL.AddtlAddSal ORDER BY UniqueId
			END; 
	 
		 
		--append tables in prep for pivot
		BEGIN 
			SELECT  DISTINCT T1.UNIQUEID, T1.UNIQUEID AS SF_Value, 'UniqueImportID' SF_Field, T1.RE_DB_OwnerShort, T1.RE_DB_Tbl, T1.ImportID, T1.RE_DB_ID, T1.NEW_TGT_IMPORT_ID
			INTO SUTTER_1P_MIGRATION.tbl.AddtlAddSal_1-- DROP TABLE RE_WMF.dbo.TBL_AddAddSal_append
			FROM SUTTER_1P_MIGRATION.tbl.AddtlAddSal T1
			UNION ALL
			SELECT  DISTINCT T1.UNIQUEID, T1.AddSalText AS SF_Value, SF_Field, T1.RE_DB_OwnerShort, T1.RE_DB_Tbl, T1.ImportID, T1.RE_DB_ID, T1.NEW_TGT_IMPORT_ID
			FROM SUTTER_1P_MIGRATION.tbl.AddtlAddSal T1
			UNION ALL
			SELECT  DISTINCT T1.UNIQUEID, T1.Additional_Recognition_Subtype__c AS SF_Value, 'Additional_Recognition_Subtype__c' SF_Field, T1.RE_DB_OwnerShort, T1.RE_DB_Tbl, T1.ImportID, T1.RE_DB_ID, T1.NEW_TGT_IMPORT_ID
			FROM SUTTER_1P_MIGRATION.tbl.AddtlAddSal T1
			WHERE T1. Additional_Recognition_Subtype__c IS NOT null
			UNION ALL
			SELECT  DISTINCT T1.UNIQUEID, T1.SALUTATION_TYPE AS SF_Value, 'rc_Bios_Salutation_Type__c' SF_Field, T1.RE_DB_OwnerShort, T1.RE_DB_Tbl, T1.ImportID, T1.RE_DB_ID, T1.NEW_TGT_IMPORT_ID
			FROM SUTTER_1P_MIGRATION.tbl.AddtlAddSal T1
			UNION ALL
 			SELECT DISTINCT T.UNIQUEID 
				,STUFF(( SELECT '; ' + T1.SALUTATION_TYPE
						FROM SUTTER_1P_MIGRATION.tbl.AddtlAddSal T1
							WHERE T1.UNIQUEID=T.UNIQUEID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.SALUTATION_TYPE 
							ORDER BY T1.SALUTATION_TYPE DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS SF_Value,
			'SALUTATION_TYPE' SF_Field, T.RE_DB_OwnerShort, T.RE_DB_Tbl, T.ImportID, T.RE_DB_ID, T.NEW_TGT_IMPORT_ID
			FROM SUTTER_1P_MIGRATION.tbl.AddtlAddSal T
		END;
			  
		--create final table
		BEGIN	
				EXEC SUTTER_1P_DATA.dbo.HC_PivotWizard_p 'UNIQUEID, ImportId, RE_DB_OwnerShort, RE_DB_ID, NEW_TGT_IMPORT_ID',
													'SF_Field',
													'SF_Value',
													'SUTTER_1P_MIGRATION.tbl.AddtlAddSal_final',   --DROP TABLE  SUTTER_1P_MIGRATION.tbl.AddtlAddSal_final
													'SUTTER_1P_MIGRATION.tbl.AddtlAddSal_1'
		END							
											
			--test	
			BEGIN 
				SELECT * FROM SUTTER_1P_MIGRATION.tbl.AddtlAddSal WHERE ImportID='04164-079-0001027' ORDER BY UniqueId
				SELECT * FROM SUTTER_1P_MIGRATION.tbl.AddtlAddSal_1 WHERE ImportID='04164-079-0001027' ORDER BY UniqueId
				SELECT * FROM SUTTER_1P_MIGRATION.tbl.AddtlAddSal_final  WHERE ImportID='04164-079-0001027' ORDER BY UniqueId
			END; 

			 
			SELECT * FROM SUTTER_1P_MIGRATION.tbl.AddtlAddSal_final
			WHERE re_Db_id IN (SELECT re_db_id   FROM SUTTER_1P_MIGRATION.tbl.AddtlAddSal_final GROUP BY re_db_id HAVING count(*)>5)
			ORDER BY re_Db_id
			/*****************************************************************************/
			--CHECK DATA FOR THE Additional_Recognition_Subtype__c FIELDS ADDED for the FINAL CONVERSION


END;


----GIFTS*******************************************************
BEGIN --FUND ATTRIBUTES
 
	SELECT DISTINCT T.[RE_DB_OwnerShort], T.[FundID], T.[FundAttrCat], T.[FundAttrDesc] 
	INTO [SUTTER_1P_MIGRATION].TBL.GAU_FundAttribute
	FROM [SUTTER_1P_DATA].dbo.[HC_Fund_Attr_v] T
	INNER JOIN [SUTTER_1P_DATA].dbo.[CHART_attributes] T2 ON T.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort] AND T.[FundAttrCat]=T2.[category]
	WHERE T2.[type]='Fund' AND T2.[convert]='yes'
 
	--check duplicates
		SELECT T.[RE_DB_OwnerShort]+T.[FundID], COUNT(*) c
		FROM [SUTTER_1P_MIGRATION].TBL.GAU_FundAttribute AS T
		GROUP BY T.[RE_DB_OwnerShort]+T.[FundID]
		ORDER BY c desc
   
END

BEGIN--FUND ATTRIBUTES TABLE ENTRY
	
 
		--FUND ATTRIBUTES TABLE ENTRY ONLY

			DROP TABLE  [SUTTER_1P_MIGRATION].TBL.FundAttribute_tableentry	
			DROP TABLE  [SUTTER_1P_MIGRATION].TBL.FundAttribute_tableentry_final
			
			--only TableEntry Attr. non-table entries are mapped in the CHART_ATTRIBUTES. 		 
				SELECT [RE_DB_OwnerShort], [FundID], [FundAttrCat],  
						CASE WHEN [FundAttrDate] IS NOT NULL THEN [FundAttrDesc]+'; '+[FundAttrDate] ELSE [FundAttrDesc] END AS SF_FieldValue 
				INTO [SUTTER_1P_MIGRATION].TBL.FundAttribute_tableentry
				FROM  [SUTTER_1P_DATA].dbo.[HC_Fund_Attr_v]
				WHERE [DataType]='TableEntry' AND [FundAttrDesc] IS NOT null
				ORDER BY [RE_DB_OwnerShort], [FundID], [FundAttrCat], [FundAttrDesc]
		  

			--CREATE final pivot fund attr table  
			  
			EXEC HC_PivotWizard_p       'RE_DB_OwnerShort, FundID',	--fields to include as unique identifier/normally it is just a unique ID field.
										'FundAttrCat',									--column that stores all new phone types
										'SF_FieldValue',										--phone numbers
										'[SUTTER_1P_MIGRATION].TBL.FundAttribute_tableentry_final',			--INTO..     
										'[SUTTER_1P_MIGRATION].TBL.FundAttribute_tableentry',					--FROM..
										'SF_FieldValue is not null '							--WHERE..
	 	 
			--audits 
				 SELECT fundid, COUNT(*) l FROM [SUTTER_1P_MIGRATION].TBL.FundAttribute_tableentry
				 GROUP BY fundid 
				 HAVING COUNT(*)>1
				 ORDER BY l desc 
				 
				 
				 SELECT * FROM [SUTTER_1P_MIGRATION].TBL.FundAttribute_tableentry_final
				 WHERE fundid='CPMC-PMHVNeurology'				 
				 ORDER BY RE_DB_OwnerShort, FundID

				 SELECT * FROM [SUTTER_1P_MIGRATION].TBL.FundAttribute_tableentry 
				 WHERE fundid='CPMC-PMHVNeurology'

				--check duplicates
				SELECT T.[RE_DB_OwnerShort]+T.[FundID], COUNT(*) c
				FROM [SUTTER_1P_MIGRATION].TBL.FundAttribute_tableentry_final AS T
				GROUP BY T.[RE_DB_OwnerShort]+T.[FundID]
				ORDER BY c DESC
                
				SELECT DISTINCT fundattrcat 
				FROM [SUTTER_1P_MIGRATION].TBL.FundAttribute_tableentry
				ORDER BY [FundAttrCat]

				SELECT fundattrcat, SF_FieldValue, LEN(SF_FieldValue) l
				FROM [SUTTER_1P_MIGRATION].TBL.FundAttribute_tableentry
				ORDER BY fundattrcat, l desc

 END 
	 

BEGIN--Gift Attribute Employee giving  (these gifts will be created as one-time giving (ea payment will be created as its own parent with transaction) 

	DROP TABLE [SUTTER_1P_MIGRATION].TBL.GiftAttr_employee_recurring

	SELECT DISTINCT T1.[GFImpID], T1.[GFAttrCat], T1.[GFAttrDesc], T2.[GFType], T2.[RE_DB_OwnerShort]
	INTO [SUTTER_1P_MIGRATION].TBL.GiftAttr_employee_recurring
	FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Attr_v] T1
	INNER JOIN [SUTTER_1P_DATA].dbo.HC_gift_v T2 ON T1.[GFImpID]=T2.GFImpID AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
	WHERE GFAttrCat='Employee Payroll' AND [GFAttrDesc]='Recurring Gift Pay-Cash'	 --561,787
 
    
	SELECT * FROM [SUTTER_1P_MIGRATION].TBL.GiftAttr_employee_recurring 
	WHERE [GFType] = 'Gift-in-Kind'
	OR [GFType] = 'Pay-Cash'
	OR [GFType] = 'Pledge'

	DELETE [SUTTER_1P_MIGRATION].TBL.GiftAttr_employee_recurring 
	WHERE [GFType] = 'Gift-in-Kind'
	OR [GFType] = 'Pay-Cash'
	OR [GFType] = 'Pledge'   --57,302

	SELECT COUNT(*) FROM  [SUTTER_1P_MIGRATION].TBL.GiftAttr_employee_recurring   --504,485

	SELECT GFType, COUNT(*) 
	FROM  [SUTTER_1P_MIGRATION].TBL.GiftAttr_employee_recurring 
	GROUP BY [GFType]
 		/*
		Cash		273961
		Other		4074
		Pay-Other	1
		Pay-Stock/Property	1
		Recurring Gift	1699
		Recurring Gift Pay-Cash	224747
		Stock/Property	2
		 */
    

END;

BEGIN-- Gift adjusted date -- gifts may have multiple adjustmentts. The lastest gift ajustment date was used. 
       
	   DROP TABLE  [SUTTER_1P_MIGRATION].TBL.GIFT_ADJUSTED_DATE

		SELECT [RE_DB_OwnerShort], 
				[GFImpID], 
				MAX(ADJDate) AS Adjusted_Date__c,
				MAX(ADJGLPostDate) AS Adjustment_Post_Date__c,
				MAX([ADJGLPostStatus]) AS Adjustment_Post_Status__c
		INTO [SUTTER_1P_MIGRATION].TBL.GIFT_ADJUSTED_DATE
		FROM SUTTER_1P_DATA.dbo.HC_Gift_Adjustments_v			    
		GROUP BY [RE_DB_OwnerShort], [GFImpID]
		ORDER BY [RE_DB_OwnerShort], [GFImpID]
	 
	 	--CHECK DUPES
			SELECT * FROM [SUTTER_1P_MIGRATION].TBL.GIFT_ADJUSTED_DATE
			WHERE ([RE_DB_OwnerShort]+'-'+[GFImpID]) IN (SELECT ([RE_DB_OwnerShort]+'-'+[GFImpID])
														FROM [SUTTER_1P_MIGRATION].TBL.GIFT_ADJUSTED_DATE
														GROUP BY ([RE_DB_OwnerShort]+'-'+[GFImpID])
														HAVING COUNT(*)>1)
			SELECT T1.*
			FROM [SUTTER_1P_MIGRATION].TBL.GIFT_ADJUSTED_DATE T1
			INNER JOIN (SELECT [RE_DB_OwnerShort], [GFImpID]
						FROM [SUTTER_1P_MIGRATION].TBL.GIFT_ADJUSTED_DATE 
						GROUP BY [RE_DB_OwnerShort], [GFImpID]
						HAVING COUNT(*)>1) T2 ON T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort] AND T1.[GFImpID]=T1.[GFImpID]
 
			
			 
END;

BEGIN--GIFTS: LEGACY FINANCIAL CODES 

		 DROP TABLE SUTTER_1P_MIGRATION.TBL.GiftLegacyFinancialCodes		
		
 
		--BASE TABLE
		
		SELECT T.RE_DB_OwnerShort, T.GSplitImpID, T.GSplitFund, T.GSplitCamp, T.GSplitAppeal, T.GSplitPkg,
		[RE_Legacy_Financial_Code__c]=	CASE WHEN T.GSplitFund IS NOT NULL THEN COALESCE('Fund: ' + LTRIM(RTRIM(T.GSplitFund)), '') ELSE 'N/A' END + CHAR(10) +
										CASE WHEN T.GSplitCamp IS NOT NULL THEN COALESCE('; Campaign: ' + LTRIM(RTRIM(T.GSplitCamp)), '') ELSE 'N/A' END + CHAR(10) +
										CASE WHEN T.GSplitAppeal IS NOT NULL THEN COALESCE('; Appeal: ' + LTRIM(RTRIM(T.GSplitAppeal)), '') ELSE 'N/A' END + CHAR(10) +
										CASE WHEN T.GSplitPkg IS NOT NULL THEN COALESCE('; Package: ' + LTRIM(RTRIM(T.GSplitPkg)), '') ELSE 'N/A' END + CHAR(10)
		INTO SUTTER_1P_MIGRATION.TBL.GiftLegacyFinancialCodes								
		FROM SUTTER_1P_DATA.dbo.HC_Gift_SplitGift_v T
		 --3,168,960
		 
		--UPDATE - REPLACE "NULL" WITH "N/A"
		SELECT * FROM SUTTER_1P_MIGRATION.TBL.GiftLegacyFinancialCodes WHERE [RE_Legacy_Financial_Code__c] LIKE '%NULL%'
		
		UPDATE SUTTER_1P_MIGRATION.TBL.GiftLegacyFinancialCodes 
			SET [RE_Legacy_Financial_Code__c] =REPLACE([RE_Legacy_Financial_Code__c] ,'NULL','N/A') 
			WHERE [RE_Legacy_Financial_Code__c] LIKE '%NULL%'
 
		SELECT [RE_Legacy_Financial_Code__c], LEN([RE_Legacy_Financial_Code__c] ) l FROM SUTTER_1P_MIGRATION.TBL.GiftLegacyFinancialCodes  ORDER BY l desc
		--len 116

END;

BEGIN--GIFTS: TOTAL PAYMENT AMT (current giving amt) 
	--DROP TABLE SUTTER_1P_MIGRATION.TBL.TotalPaymentAmt
	 
	DROP TABLE SUTTER_1P_MIGRATION.TBL.TotalPaymentAmt	

	SELECT RE_DB_OwnerShort, GFType, GFImpId, SUM(GFPaymentAmount) TotalPaymentAmt, COUNT(*) TotalNumberPayments
	INTO SUTTER_1P_MIGRATION.TBL.TotalPaymentAmt	
	FROM SUTTER_1P_DATA.dbo.HC_Gift_Link_v
	GROUP BY RE_DB_OwnerShort, GFType, GFImpId
	ORDER BY RE_DB_OwnerShort, GFType, GFImpId
	
	
END;

---DNC..> GIFT TRIBUTE FIELDS--MIGRATING TO CUSTOM OBJECT. 
/*BEGIN--GIFTS: TRIBUTE

		BEGIN
			--DELETE BASE TABLE
			DROP TABLE SUTTER_1P_MIGRATION.TBL.GiftTribute
			DROP TABLE SUTTER_1P_MIGRATION.TBL.GiftTribute_1
			DROP TABLE SUTTER_1P_MIGRATION.TBL.GiftTribute_23
			DROP TABLE SUTTER_1P_MIGRATION.TBL.GiftTribute_23_final			SELECT * FROM SUTTER_1P_MIGRATION.TBL.GiftTribute_1
			DROP TABLE SUTTER_1P_MIGRATION.TBL.GiftTribute_3
			DROP TABLE SUTTER_1P_MIGRATION.TBL.GiftTribute_note
		END;

		BEGIN--CREATE BASE TABLE
			SELECT	DISTINCT
					ROW_NUMBER() OVER ( PARTITION BY (T1.RE_DB_ID)
											ORDER BY T1.RE_DB_ID,
											CAST((CASE LEN(T2.TRDateFrom) WHEN '4' THEN '01/01/'+T2.[TRDateFrom]
												WHEN '6' THEN RIGHT(T2.[TRDateFrom],2)+'/01/'+ LEFT(T2.[TRDateFrom],4) 
												WHEN '8' THEN SUBSTRING(T2.[TRDateFrom],5,2) +'/'+ SUBSTRING(T2.[TRDateFrom],7,2) +'/'+ LEFT(T2.[TRDateFrom],4) 
												ELSE NULL END) AS DATE) DESC)  AS Seq 
					,T1.RE_DB_ID
					,T1.RE_DB_OwnerShort
					,T1.GFTLink
					,T2.RE_DB_OwnerShort+'-'+T2.RE_DB_Tbl+'-'+T2.TRImpID AS RE_TRImpID__c
					,CASE WHEN T1.GFTribType IS NOT NULL THEN T1.GFTribType ELSE T2.TRType END AS rC_Giving__Tribute_Type__c
					,CASE WHEN T1.GFTribDesc IS NOT NULL THEN T1.GFTribDesc WHEN T2.TRDesc IS NOT NULL THEN T2.TRDesc WHEN (T3.[NEW_TGT_IMPORT_ID] IS NOT NULL AND T3.RE_DB_ID=T3.[NEW_TGT_IMPORT_ID]) THEN T3.FirstName +' '+ T3.LastName END AS rC_Giving__Tribute_Description__c
					,'TRUE' AS rC_Giving__Is_Tribute__c
					,CASE LEN(T2.TRDateFrom) WHEN '4' THEN '01/01/'+T2.[TRDateFrom]
									WHEN '6' THEN RIGHT(T2.[TRDateFrom],2)+'/01/'+ LEFT(T2.[TRDateFrom],4) 
									WHEN '8' THEN SUBSTRING(T2.[TRDateFrom],5,2) +'/'+ SUBSTRING(T2.[TRDateFrom],7,2) +'/'+ LEFT(T2.[TRDateFrom],4) 
									ELSE NULL END AS  rC_Giving__Tribute_Effective_Date__c
					,CAST(T2.TRNotes AS NVARCHAR(4000)) AS rC_Giving__Tribute_Comments__c

					--ref
					,T1.TribImpID, T1.TribGiftImpID
					,T3.NEW_TGT_IMPORT_ID
					,T3.[KeyInd]
					
			INTO SUTTER_1P_MIGRATION.TBL.GiftTribute
			FROM SUTTER_1P_DATA.dbo.HC_Gift_Tribute_v T1
			LEFT JOIN SUTTER_1P_DATA.dbo.HC_Cons_Tribute_v T2 ON T1.TribImpID=T2.TRImpID and T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T3 ON T2.ImportID=T3.ImportID AND T2.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
		END; 
					--CHECK MULTIPLE TRIBUTES / GIFT
						SELECT * 
						FROM SUTTER_1P_MIGRATION.TBL.GiftTribute
						WHERE RE_DB_ID IN (SELECT RE_DB_ID FROM SUTTER_1P_MIGRATION.TBL.GiftTribute GROUP BY RE_DB_ID HAVING COUNT(*)>1)
						ORDER BY RE_DB_ID, Seq
  
						
		BEGIN--CREATE TABLE OF UNIQUE TRIBUTES 1.
					SELECT * 
					INTO SUTTER_1P_MIGRATION.TBL.GiftTribute_1
					FROM SUTTER_1P_MIGRATION.TBL.GiftTribute
					WHERE SEQ=1
					ORDER BY  RE_DB_ID, seq
					--477608

					--CHECK MULTIPLE TRIBUTES / GIFT
					SELECT * 
					FROM SUTTER_1P_MIGRATION.TBL.GiftTribute_1
					WHERE RE_DB_ID IN (SELECT RE_DB_ID FROM SUTTER_1P_MIGRATION.TBL.GiftTribute_1 GROUP BY RE_DB_ID HAVING COUNT(*)>1)
					ORDER BY RE_DB_ID, Seq

		END;	
		
		BEGIN--CREATE LIST OF TRIBUTE 2 and TRIBUTE 3 Ids for custom fields on Opportunities. 
					DROP TABLE SUTTER_1P_MIGRATION.TBL.GiftTribute_23
					
					SELECT *, CASE Seq WHEN '2' THEN 'Tribute_2__r:RE_TRImpID__c' WHEN '3' THEN 'Tribute_3__r:RE_TRImpID__c' END AS SF_Field
				    INTO SUTTER_1P_MIGRATION.TBL.GiftTribute_23
					FROM SUTTER_1P_MIGRATION.TBL.GiftTribute
					WHERE SEQ=2 OR SEQ=3
					ORDER BY  RE_DB_ID, seq
					--33346
		
					--CHECK MULTIPLE TRIBUTES / GIFT
						SELECT * 
						FROM SUTTER_1P_MIGRATION.TBL.GiftTribute_23  --ORDER BY RE_DB_ID, Seq
						WHERE RE_DB_ID IN (SELECT RE_DB_ID FROM SUTTER_1P_MIGRATION.TBL.GiftTribute_23 GROUP BY RE_DB_ID HAVING COUNT(*)>1)
						ORDER BY RE_DB_ID, Seq
  
			BEGIN--FINAL TABLE
				EXEC HC_PivotWizard_p   'RE_DB_ID, RE_DB_OwnerShort, GFTLink',				--fields to include as unique identifier/normally it is just an ID field.
				'SF_Field',									--column that stores all new phone types
				'RE_TRImpID__c',								--phone numbers
				'SUTTER_1P_MIGRATION.TBL.GiftTribute_23_final',	--INTO..     
				'SUTTER_1P_MIGRATION.TBL.GiftTribute_23',	--FROM..
				'RE_TRImpID__c is not null'						--WHERE..
			END;

		END;

	 
					 
		BEGIN--CREATE TABLE OF NON UNIQUE TRIBUTES FOR NOTES
				SELECT * 
				INTO SUTTER_1P_MIGRATION.TBL.GiftTribute_2
				FROM SUTTER_1P_MIGRATION.TBL.GiftTribute
				WHERE SEQ>3
				ORDER BY  RE_DB_ID, Seq
		END;		--41381
				
		BEGIN--CONCATENATE MULTIPLE FIELDS
				SELECT  DISTINCT 
						T1.RE_DB_OwnerShort
						,T1.RE_DB_ID
						,T1.GFTLink
						,TributeInfo=CASE WHEN T1.rC_Giving__Tribute_Type__c IS NOT NULL 
						    THEN COALESCE(' Type: ' + LTRIM(RTRIM(T1.rC_Giving__Tribute_Type__c)), '') 
							ELSE ' Type: N/A' END + CHAR(10) +
						CASE WHEN T1.rC_Giving__Tribute_Effective_Date__c IS NOT NULL 
							THEN COALESCE(' Date: ' + LTRIM(RTRIM(T1.rC_Giving__Tribute_Effective_Date__c)), '') 
							ELSE ' Date: N/A' END + CHAR(10) +
						CASE WHEN T1.rC_Giving__Tribute_Description__c IS NOT NULL 
							THEN COALESCE(' Description: ' + LTRIM(RTRIM(T1.rC_Giving__Tribute_Description__c)), '') 
							ELSE ' Description: N/A' END + CHAR(10) + 
						CASE WHEN T1.rC_Giving__Tribute_Comments__c IS NOT NULL 
							THEN COALESCE('; Notes: ' + LTRIM(RTRIM(T1.rC_Giving__Tribute_Comments__c)), '') 
							ELSE '; Notes: N/A' END 
				INTO SUTTER_1P_MIGRATION.TBL.GiftTribute_3    
				FROM SUTTER_1P_MIGRATION.TBL.GiftTribute_2 T1
				ORDER BY T1.RE_DB_ID 
		END;  --41258
		
		BEGIN--MULTI-SELECT PICKLIST (CONCATENATE MULTIPLE  GIFT TRIBUTE NOTES. 
			SELECT DISTINCT T.RE_DB_OwnerShort, T.GFTLink, T.RE_DB_ID
				,STUFF(( SELECT '. ' + CHAR(10)+ T1.TributeInfo 
						FROM SUTTER_1P_MIGRATION.TBL.GiftTribute_3 T1
							WHERE T1.GFTLink=T.GFTLink AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.TributeInfo --ORDER BY T1.[Description] DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS TributeInfo
			INTO SUTTER_1P_MIGRATION.TBL.GiftTribute_note  
			FROM SUTTER_1P_MIGRATION.TBL.GiftTribute_3 T
			ORDER BY T.RE_DB_OwnerShort, T.GFTLink
		--23654
				SELECT * FROM SUTTER_1P_MIGRATION.TBL.GiftTribute_note
				WHERE RE_DB_ID IN (SELECT RE_DB_ID FROM SUTTER_1P_MIGRATION.TBL.GiftTribute_note GROUP BY RE_DB_ID HAVING COUNT(*)>1)
				ORDER BY RE_DB_ID, [RE_DB_OwnerShort]
		
		END;
END;	 */

BEGIN--ProposalAttribute
 	SELECT * FROM  [SUTTER_1P_DATA].dbo.[CHART_ProposalAttr] 	  
	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Proposal_Attr_v]
	DROP TABLE [SUTTER_1P_MIGRATION].TBL.ProposalAttribute
	DROP TABLE [SUTTER_1P_MIGRATION].TBL.ProposalAttribute_final

	SELECT T1.[RE_DB_OwnerShort], [T1].[PRAttrPRImpID], T2.[SF_Field], T2.[SF_Value]
	INTO [SUTTER_1P_MIGRATION].TBL.ProposalAttribute
	FROM [SUTTER_1P_DATA].dbo.[HC_Proposal_Attr_v] T1
	INNER JOIN [SUTTER_1P_DATA].dbo.[CHART_ProposalAttr] T2 ON T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort] 
															AND T1.[PRAttrCat]=T2.[PRAttrCat]
															AND t1.[PRAttrDesc]=t2.[PRAttrDesc]
	WHERE T2.[Convert]='Yes' AND T2.[SF_Object]='Opportunity'
	UNION 
	SELECT T1.[RE_DB_OwnerShort], [T1].[PRAttrPRImpID], T2.[SF_Field2] AS [SF_Field], T2.[SF_Value2 Text] AS [SF_Value]
	FROM [SUTTER_1P_DATA].dbo.[HC_Proposal_Attr_v] T1
	INNER JOIN [SUTTER_1P_DATA].dbo.[CHART_ProposalAttr] T2 ON T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort] 
															AND T1.[PRAttrCat]=T2.[PRAttrCat]
															AND t1.[PRAttrDesc]=t2.[PRAttrDesc]
	WHERE T2.[Convert]='Yes' AND T2.[SF_Object2]='Opportunity'



	SELECT DISTINCT sf_field FROM [SUTTER_1P_MIGRATION].TBL.ProposalAttribute

	SELECT * FROM SUTTER_1P_MIGRATION.TBL.ProposalAttribute

 

			SELECT DISTINCT T.RE_DB_OwnerShort, T.[PRAttrPRImpID],  
				STUFF(( SELECT '; ' + T1.[SF_Value] 
						FROM SUTTER_1P_MIGRATION.TBL.ProposalAttribute T1
							WHERE T1.[SF_Field]='Ask_Readiness__c' AND T1.[PRAttrPRImpID]=T.[PRAttrPRImpID] AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.[SF_Value] ORDER BY T1.[SF_Value] DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS Ask_Readiness__c
				,STUFF(( SELECT '; ' + T1.[SF_Value] 
						FROM SUTTER_1P_MIGRATION.TBL.ProposalAttribute T1
							WHERE T1.[SF_Field]='Prospect_Tier__c' AND T1.[PRAttrPRImpID]=T.[PRAttrPRImpID] AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.[SF_Value] ORDER BY T1.[SF_Value] DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS Prospect_Tier__c
  				,STUFF(( SELECT '; ' + T1.[SF_Value] 
						FROM SUTTER_1P_MIGRATION.TBL.ProposalAttribute T1
							WHERE T1.[SF_Field]='Proposal_Priority__c' AND T1.[PRAttrPRImpID]=T.[PRAttrPRImpID] AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.[SF_Value] ORDER BY T1.[SF_Value] DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS Proposal_Priority__c
  				,STUFF(( SELECT '; ' + T1.[SF_Value] 
						FROM SUTTER_1P_MIGRATION.TBL.ProposalAttribute T1
							WHERE T1.[SF_Field]='Reason_Prior_to_July_2013__c' AND T1.[PRAttrPRImpID]=T.[PRAttrPRImpID] AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.[SF_Value] ORDER BY T1.[SF_Value] DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS Reason_Prior_to_July_2013__c
    
				INTO SUTTER_1P_MIGRATION.TBL.ProposalAttribute_final  
				FROM SUTTER_1P_MIGRATION.TBL.ProposalAttribute T
				WHERE T.[SF_Field] IS NOT NULL    
				ORDER BY T.RE_DB_OwnerShort, T.[PRAttrPRImpID]
	   
				SELECT * FROM SUTTER_1P_MIGRATION.TBL.ProposalAttribute_final WHERE [PRAttrPRImpID] ='00001-589-0000001637'
  
BEGIN--GIFTS: GFT/PROPOSAL LINK 
		
		-- DROP TABLE SUTTER_1P_MIGRATION.TBL.PROPOSAL_GIFT_LINK
		-- BASE TABLE- include only records where 1 gift is applied to 1 proposal. 
		
		SELECT T2.RE_DB_OwnerShort, T2.GSplitImpID, T2.GFImpID, T1.GFLink, T1.PRImpID, T1.Amount, T1.UpdatePRAmtFunded, T1.PRName
		INTO SUTTER_1P_MIGRATION.TBL.PROPOSAL_GIFT_LINK				
		FROM SUTTER_1P_DATA.dbo.HC_Gift_ProposalLink_v T1
		INNER JOIN SUTTER_1P_DATA.DBO.HC_Gift_SplitGift_v T2 ON T1.GFLink=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
		WHERE GFLink IN (SELECT GFLink FROM SUTTER_1P_DATA.dbo.HC_Gift_ProposalLink_v GROUP BY GFLink HAVING COUNT(*)=1)
		AND [T1].[PRName] NOT LIKE '%planned%'
		ORDER BY T2.RE_DB_OwnerShort, T2.GSplitImpID, T2.GFImpID, T1.PRImpID
		--12269
		--12236 (PRName not like "planned")

		
END;

BEGIN--GIFTS: SPLITS (add RE_Split_Gift__c checkbox on opportunities created from gifts with splits)

		SELECT RE_DB_OwnerShort, GFImpID, COUNT(*) CountOFSplits, 'TRUE' AS RE_Split_Gift__c 
		INTO SUTTER_1P_MIGRATION.TBL.GIFT_SPLIT
		FROM SUTTER_1P_DATA.dbo.HC_Gift_SplitGift_v
		GROUP BY RE_DB_OwnerShort, GFImpID 
		HAVING COUNT(*)>1 
		
END;

BEGIN--GIFTS: WRITE OFF 
	 
			DROP TABLE SUTTER_1P_MIGRATION.TBL.GIFT_WRITEOFF    
			
			SELECT	DISTINCT 
					T4.HH_ImportID
				    ,T4.NoHH_ImportID
					,CASE WHEN T4.NoHH_ImportID IS NOT NULL THEN T4.HH_ImportID ELSE T2.[NEW_TGT_IMPORT_ID] END AS [ACCOUNT:External_Id__c]
					,T1.*
		    INTO SUTTER_1P_MIGRATION.TBL.GIFT_WRITEOFF    
		 	FROM SUTTER_1P_DATA.dbo.HC_Gift_WriteOff_v T1
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T2 ON T1.ImportID=T2.ImportID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH AS T4 ON T2.[NEW_TGT_IMPORT_ID]=T4.NoHH_ImportID  
			
END 
  
BEGIN--GIFT ATTRIBUTES-CHART_Attribute

		BEGIN	
			 DROP TABLE SUTTER_1P_MIGRATION.TBL.Attribute_opportunity_gift 
			 DROP TABLE SUTTER_1P_MIGRATION.TBL.Attribute_opportunity_gift_1
	 	END;

		SELECT * FROM SUTTER_1P_MIGRATION.TBL.Attribute_opportunity_gift

		BEGIN--GIFT ATTRIBUTE-BASE TABLE 
			SELECT DISTINCT T.RE_DB_OwnerShort, T.GFImpID,  T2.SF_Object_2, T2.SF_Field_2, 
			CASE WHEN T3.Name IS NOT NULL THEN T3.Name ELSE t.GFAttrDesc END AS SF_FieldValue, T.GFAttrDesc AS refCAttrDesc, 
			LEN(T.GFAttrDesc) refFieldLength 
			INTO SUTTER_1P_MIGRATION.TBL.Attribute_opportunity_gift
			FROM SUTTER_1P_DATA.dbo.HC_Gift_Attr_v T
			INNER JOIN SUTTER_1P_DATA.dbo.CHART_Attributes T2 ON T.GFAttrCat =T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T3 ON T.GFAttrDesc=T3.ConsID AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
			WHERE T2.SF_Object_2='OPPORTUNITY'  
			ORDER BY T.RE_DB_OwnerShort, T.GFImpID, T2.SF_Field_2
		END;  --85,686

		BEGIN--FINAL TABLE
			EXEC HC_PivotWizard_p   'RE_DB_OwnerShort, GFImpID',				--fields to include as unique identifier/normally it is just an ID field.
						'SF_Field_2',									--column that stores all new phone types
						'SF_FieldValue',								--phone numbers
						'SUTTER_1P_MIGRATION.TBL.Attribute_opportunity_gift_1',	--INTO..     
						'SUTTER_1P_MIGRATION.TBL.Attribute_opportunity_gift',	--FROM..
						'SF_FieldValue is not null'						--WHERE..
					--571
		END;
	
	--	SELECT * FROM SUTTER_1P_MIGRATION.TBL.Attribute_opportunity_gift_1
		
	 
END; 

BEGIN--GIFT ATTRIBUTE-CHART_GiftAttribute
	
	BEGIN
		SELECT * FROM SUTTER_1P_DATA.DBO.CHART_GiftAttribute
		SELECT * FROM SUTTER_1P_MIGRATION.TBL.Gift_Attribute_1 

		DROP TABLE SUTTER_1P_MIGRATION.TBL.Gift_Attribute	
		DROP TABLE SUTTER_1P_MIGRATION.TBL.Gift_Attribute_1 
	
	END

	BEGIN--BASE TABLE 	
		SELECT DISTINCT
		T1.RE_DB_OwnerShort, T1.GFImpID, T1.GFAttrCat, T1.GFAttrDesc, T1.GFAttrDate, T1.GFAttrCom, [T2].[SF_Object], [T2].[SF_Field_Name], T2.[SF_Field_Value]
		INTO SUTTER_1P_MIGRATION.TBL.Gift_Attribute		
		FROM SUTTER_1P_DATA.dbo.HC_Gift_Attr_v T1
		INNER JOIN SUTTER_1P_DATA.DBO.CHART_GiftAttribute T2 ON T1.GFAttrCat=T2.GFAttrCat AND T1.GFAttrDesc=T2.GFAttrDesc AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
		WHERE T2.[Convert]='Yes'
	END;

	BEGIN--FINAL TABLE
			EXEC HC_PivotWizard_p   'RE_DB_OwnerShort, GFImpID',					--fields to include as unique identifier/normally it is just a unique ID field.
									'SF_Field_Name',								--column that stores all new phone types
									'SF_Field_Value',								--phone numbers
									'SUTTER_1P_MIGRATION.TBL.Gift_Attribute_1',	    --INTO..     
									'SUTTER_1P_MIGRATION.TBL.Gift_Attribute',		--FROM..
									'SF_Field_Value is not null'					--WHERE..
	
	END;
	
	--check dupes
			SELECT RE_DB_OwnerShort, GFImpID, COUNT(*) c 
			FROM SUTTER_1P_MIGRATION.TBL.Gift_Attribute_1 
	 		GROUP BY RE_DB_OwnerShort, GFImpID
			--HAVING COUNT(*)>1
			ORDER BY c DESC

			SELECT * FROM SUTTER_1P_MIGRATION.TBL.Gift_Attribute_1 
END;
  
BEGIN --FIRST PAYMENT DATE TO BE POPULATED ON CLOSEDATE OF PARENT OPP MIGRATED FROM PLEDGE/RECURRIGN GIFTS

		--FIRST PAYMENT DATE TO BE POPULATED ON CLOSEDATE OF PARENT OPP MIGRATED FROM PLEDGE/RECURRIGN GIFTS
		SELECT		[T1].[RE_DB_OwnerShort], T1.GFType, T1.GFImpID, [T2].GFDate, 
					MIN(T3.GFDate) AS GFFirstPaymentDate
		INTO		SUTTER_1P_MIGRATION.TBL.Gift_FirstPaymentDate
		FROM        dbo.HC_Gift_Link_v AS T1 
					LEFT JOIN dbo.HC_Gift_v AS T2 ON T1.GFImpID = T2.GFImpID AND T1.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
					LEFT JOIN dbo.HC_Gift_v AS T3 ON T1.GFPaymentGFImpID = T3.GFImpID AND T1.RE_DB_OwnerShort = T3.RE_DB_OwnerShort  
		GROUP BY	T1.[RE_DB_OwnerShort],T1.GFType, T1.GFImpID, [T2].GFDate			
		ORDER BY T1.[RE_DB_OwnerShort], T1.GFType, T1.GFImpID

		--check dupes
			SELECT [T1].[RE_DB_OwnerShort] +'-'+T1.GFImpID AS RE_DB_ID_Gift, COUNT(*) c
			FROM SUTTER_1P_MIGRATION.TBL.Gift_FirstPaymentDate AS T1
			GROUP BY [T1].[RE_DB_OwnerShort] +'-'+T1.GFImpID 
			ORDER BY c desc

				SELECT *
				FROM SUTTER_1P_MIGRATION.TBL.Gift_FirstPaymentDate

END	

BEGIN --DELETE --GIFTS: BASE TABLE 
			DROP TABLE SUTTER_1P_MIGRATION.TBL.GIFT
END 

BEGIN--CREATE GIFTS: BASE TABLE 
	 		SELECT DISTINCT 
			--constituent 
				T20.KeyInd, T20.Name,
				CASE WHEN T21.NoHH_ImportID IS NOT NULL THEN T21.HH_ImportID ELSE T20.[NEW_TGT_IMPORT_ID]  END AS [ACCOUNT:External_Id__c],
			--GSplitGift
				T.GSplitImpID, T.GFImpID GSplitGFImpID, T.GFSplitSequence, T.GSplitAmt, T.GSplitFund, T.GSplitCamp, T.GSplitAppeal, T.GSplitPkg, 
				T.RE_DB_OwnerShort GSplitRE_DB_OwnerShort, T.RE_DB_Tbl GSplitRE_DB_Tbl,
				T.RE_DB_OwnerShort+'-'+T.GSplitImpID AS RE_GFSplitImportID_ID,
			--Gift
				T1.BATCH_NUMBER, T1.GFDate, T1.GFType, T1.GFTAmt, T1.GFPledgeBalance, T1.GFStatus, T1.GFAck, T1.GFAckDate, T1.GFAnon, T1.GFConsDesc,
				CASE WHEN LEN(T1.GFCheckDate)=10 THEN T1.GFCheckDate END AS GFCheckDate,
				T1.GFCheckNum, T1.ImportID, T1.GFPayMeth, T1.GFRef, T1.GFLtrCode,
				T1.GFStkIss, T1.GFStkMedPrice, T1.GFStkNumUnits, T1.GFStkSymbol, T1.GFSubType, 
				T1.GFInsYears, T1.GFInsEndDate, T1.GFInsFreq, T1.GFInsNumPay, T1.GFInsStartDate, T1.DateAdded
			--GAU
				,T2.rC_Giving__External_ID__c AS GAU_External_Id__c		--rC_Giving__GAU__c
--			--Campaign
				,T3.PRIMARY_CAMPAIGN_SOURCE AS CAMPAIGN_External_Id__c	--CampaignId
				,T3.[rC_Giving_Fundraising_Program__c] AS Fundraising_Program__c
				,T3.rC_Giving__Affiliation__c
			--GiftTpe
				,T4.Parent_Giving_RecordType, T4.rC_Giving__Activity_Type__c, T4.rC_Giving__Is_Giving__c, T4.rC_Giving__Is_Giving_Donation__c	
				,T4.rC_Giving__Is_Giving_Inkind__c, T4.rC_Giving__Is_Sustainer__c, T4.Child_Giving_RecordType
				,T4.rC_Giving__Is_Giving_Transaction__c, T4.Transaction_Source
			--GSubType
				,T5.Production__c
				,T5.RE_Gift_Subtype__c
			--PaymentMethod
				,T6.rC_Giving__Payment_Type__c
				,T6.External_Id__c PAYMENT_METHOD_External_ID__c
				,T6.ACCOUNT_External_ID__c AS PAYMENT_METHOD_ACCOUNT_External_ID__c
			--attributes 
				--(chart-attributes; type = gift)
					,T7.DAF_Pledge_Payment__c
					,T7.Donation_Page_Name__c
					,T7.Donor_Recognition_Name__c
					,T7.Gift_Comments__c
					,T7.NCHX_Source_Entity__c
					,T7.[Non-Donation_Payment__c] AS Non_Donation_Payment__c
					,T7.Original_Pledge_Amount__c
					,T7.Plaque_Wording__c
					,T7.Research_Payment__c
					,T7.Transaction_ID__c
					,T7.Tribute_In_Honor_of__c
					,T7.Tribute_In_Memory_of__c

			--legacy codes
				,T9.RE_Legacy_Financial_Code__c
			--frequency
				,T10.rC_Giving__Giving_Frequency__c
			--rC_Giving__Current_Giving_Amount__c
				,CASE WHEN T11.TotalPaymentAmt IS NOT NULL THEN T11.TotalPaymentAmt ELSE T.GSplitAmt END AS rC_Giving__Current_Giving_Amount__c
			--parent opp Proposal
				,T12.RE_DB_OwnerShort+'-PP-'+T12.PRImpID AS  PARENT_PROPOSAL_External_Id__c

			/* DNC GIFT TRIBUTE --> going to own custom object
 				,T13.rC_Giving__Tribute_Type__c
				,T13.rC_Giving__Tribute_Description__c	
 				,T13.rC_Giving__Is_Tribute__c
 				,T13.rC_Giving__Tribute_Effective_Date__c
 				,T13.rC_Giving__Tribute_Comments__c
 
 				--DNC,T13.RE_TRImpID__c AS [Tribute__r:RE_TRImpID__c]
 				--DNC,T23.[Tribute_2__r:RE_TRImpID__c]
				--DNC,T23.[Tribute_3__r:RE_TRImpID__c]
             */
			--PLANNED GIFT field
			      ,T1.[GFPlannedGiftID]
				  ,T1.[GFDiscountRt]
				  ,T1.[GFMaturityYr]
				  ,T1.[GFFlexibleDef]
				  ,T1.[GFTRealized]
				  ,T1.[GFVehicle]
				  ,T1.[GFNumUnits]
				  ,T1.[GFPayoutAmt]
				  ,T1.[GFPayoutPct]
				  ,T1.[GFPIFName]
				  ,T1.[GFPGStatus]
				  ,T1.[GFRemainderAsOfDate]
				  ,T1.[GFRemainderValue]
				  ,T1.[GFPPolicyType]
				  ,T1.[GFPPolicyNum]
				  ,T1.[GFPPolicyFaceAmt]
				  ,T1.[GFPPolicyPremium]
				  ,T1.[GFNetPresentValue]
				  ,T1.[GFNetPresentValueAsOf]
				  ,T1.[GFRevocable]
				  ,T1.[GFTermEndDt]
				  ,T1.[GFTermType]
				  ,T1.[GFPIFTotalUnits]
				  ,T1.[GFTrustTaxID]
 
			--MISC.
				,T15.RE_Split_Gift__c
				,T16.GiftSubType__c
				,T1.GFPostDate AS GFPostDate__c 
				,T1.GFPostStat AS GFPostStatus__c
				,T17.Adjusted_Date__c
			--ref
				,T1.GFImpID AS refGFImpID
				,T1.ImportID AS refOldImportId
				,T20.[NEW_TGT_IMPORT_ID] AS refNewImportId
				,T21.HH_ImportID 
				,T21.NoHH_ImportID
	 
	  		INTO SUTTER_1P_MIGRATION.TBL.GIFT
			FROM SUTTER_1P_DATA.DBO.HC_Gift_SplitGift_v AS T
			INNER JOIN SUTTER_1P_DATA.DBO.HC_Gift_v AS T1 ON T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort AND T.GFImpID=T1.GFImpID
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T20 ON T1.[RE_DB_Id]=T20.[RE_DB_Id] 
			LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH AS T21 ON T20.[NEW_TGT_IMPORT_ID]=T21.[NoHH_ImportID]  
	 
			LEFT JOIN SUTTER_1P_MIGRATION.IMP.GAU AS T2 ON T.RE_DB_OwnerShort=T2.[zrefREOwner] AND T.GSplitFund=T2.zrefFundId 
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.CAMPAIGN AS T3 ON T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort 
														AND T.GSplitFund=T3.FundId
														AND T.GSplitCamp=T3.CampId 
														AND T.GSplitAppeal=T3.AppealId 
														AND T.GSplitPkg=T3.PackageId
		 	 
			LEFT JOIN SUTTER_1P_DATA.dbo.CHART_GiftType		AS T4 ON T1.GFType=T4.GFType 
			LEFT JOIN SUTTER_1P_DATA.dbo.CHART_GiftSubType		AS T5 ON T1.RE_DB_OwnerShort=T5.RE_DB_OwnerShort AND T1.GFSubType=T5.GFSubType  
			--PAYMETHOD		
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.PAYMENT_METHOD AS T6 ON T1.RE_DB_OwnerShort=T6.RE_DB_OwnerShort  AND T1.GFImpID=T6.GFImpID 
							AND (CASE WHEN T21.NoHH_ImportID IS NOT NULL THEN T21.HH_ImportID ELSE T20.[NEW_TGT_IMPORT_ID]  END)=T6.[ACCOUNT_External_ID__c]
			--
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Attribute_opportunity_gift_1 AS T7 ON T1.RE_DB_OwnerShort=T7.RE_DB_OwnerShort AND T1.GFImpID=T7.GFImpID  ---chart_attribute
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.GiftLegacyFinancialCodes AS T9 ON T.RE_DB_OwnerShort=T9.RE_DB_OwnerShort AND T.GSplitImpID=T9.GSplitImpID
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_Pledge_Frequency AS T10 ON T1.GFInsFreq = T10.GFInsFreq
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.TotalPaymentAmt AS  T11  ON T1.RE_DB_OwnerShort=T11.RE_DB_OwnerShort AND T1.GFImpID=T11.GFImpID
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.PROPOSAL_GIFT_LINK AS T12 ON T.GSplitImpID=T12.GSplitImpID AND T.RE_DB_OwnerShort=T12.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.GIFT_SPLIT AS T15 ON T.GFImpID=T15.GFImpID AND T.RE_DB_OwnerShort=T15.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_DATA.dbo.CHART_GiftCode AS T16 ON T1.GFGiftCode=T16.GFGiftCode AND T1.RE_DB_OwnerShort=T16.RE_DB_OwnerShort
			LEFT JOIN [SUTTER_1P_MIGRATION].TBL.GIFT_ADJUSTED_DATE AS T17 ON T.[GFImpID]=T17.GFImpID AND T.RE_DB_OwnerShort=T17.RE_DB_OwnerShort
				
				--tribute__1


				--	DNC LEFT JOIN SUTTER_1P_MIGRATION.TBL.GiftTribute_1 AS T13 ON T1.GFImpID=T13.GFTLink AND T1.RE_DB_OwnerShort=T13.RE_DB_OwnerShort
				--	tribute 2&3
				--	DNC LEFT JOIN SUTTER_1P_MIGRATION.TBL.GiftTribute_23_final AS T23 ON T1.GFImpID=T23.GFTLink AND T1.RE_DB_OwnerShort=T23.RE_DB_OwnerShort
				--  3,169,456
 		 		 
				 
END;  --end gift table 3190498


		BEGIN--CHECK DUPES 
         
			DROP TABLE SUTTER_1P_MIGRATION.TBL.zGIFT_dupes
		   
		    SELECT  *
            --INTO    SUTTER_1P_MIGRATION.TBL.zGIFT_dupes
            FROM    SUTTER_1P_MIGRATION.TBL.GIFT
            WHERE   RE_GFSplitImportID_ID IN (
                    SELECT  RE_GFSplitImportID_ID
                    FROM    [SUTTER_1P_MIGRATION].[TBL].[GIFT]
                    GROUP BY RE_GFSplitImportID_ID
                    HAVING  COUNT(*) > 1 )
            ORDER BY RE_GFSplitImportID_ID, [ACCOUNT:External_ID__c]

		 
			SELECT [GFType], COUNT(*) C
			FROM SUTTER_1P_MIGRATION.TBL.zGIFT_dupes
			GROUP BY [GFType]
			ORDER BY [GFType]

			--CHECK DUPES USING 2 FIELDS
            SELECT  T1.*
            FROM    SUTTER_1P_MIGRATION.TBL.GIFT T1
                    INNER JOIN ( SELECT T2.GSplitImpID ,
                                        T2.GSplitRE_DB_OwnerShort
                                 FROM   SUTTER_1P_MIGRATION.TBL.GIFT T2
                                 GROUP BY T2.GSplitImpID ,
                                        T2.GSplitRE_DB_OwnerShort
                                 HAVING COUNT(*) > 1
                               ) T2 ON T1.GSplitImpID = T2.GSplitImpID  AND T1.GSplitRE_DB_OwnerShort = T2.GSplitRE_DB_OwnerShort
            ORDER BY GSplitImpID ,
                    GSplitRE_DB_OwnerShort DESC;
		END;

		BEGIN--CHECK MISSING GFSPLITIMPID  (Missing gifts are because the Constituent is missing from the data table and RE)
            SELECT  T1.*
            FROM    [SUTTER_1P_DATA].[dbo].[HC_Gift_SplitGift_v] AS T1
                    LEFT JOIN [SUTTER_1P_MIGRATION].[TBL].[GIFT] AS T2 ON T1.[GSplitImpID] = T2.GSplitImpID
                                                              AND [T1].[RE_DB_OwnerShort] = T2.GSplitRE_DB_OwnerShort
            WHERE   T2.GSplitImpID IS NULL;
		
			 
		END

	END
 
BEGIN--TRIBUTE CONTACT (ACKNOWLEDGEE)
		DROP TABLE [SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT
		DROP TABLE [SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT_2
		DROP TABLE [SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT_final	
		
		
		BEGIN--Notificant 1
			SELECT DISTINCT
			  [Tribute__r:RE_TRImpID__c] = T2.RE_DB_OwnerShort+'-'+T2.RE_DB_Tbl+'-'+T2.TRImpID
			, [Contact__r:External_ID__c] = CASE WHEN (T3.NEW_TGT_IMPORT_ID_IRLINK!='' AND T3.NEW_TGT_IMPORT_ID_IRLINK IS NOT NULL) THEN (T4.NEW_TGT_IMPORT_ID) 
											WHEN (T3.NEW_TGT_IMPORT_ID_IRLINK='' OR T3.NEW_TGT_IMPORT_ID_IRLINK IS NULL)  THEN ('IR-'+T3.NEW_TGT_IMPORT_ID_IR) END
 			, T1.TARKeyInd AS zrefTAKey
			, T1.TAName AS zrefTAName 
			, T1.ConsId AS zrefConsId
			, T1.TATRImpID
	 		INTO [SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT
			FROM [SUTTER_1P_DATA].dbo.HC_Tribute_Acknowledgee_v AS T1
			INNER JOIN [SUTTER_1P_DATA].dbo.[HC_Cons_Tribute_v] AS T2 ON T1.TATRImpID = T2.[TRImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
 			INNER JOIN [SUTTER_1P_DATA].dbo.HC_Ind_Relat_v AS T3  ON T1.TARImpID=T3.[IRImpID] AND T1.[RE_DB_OwnerShort] = T3.[RE_DB_OwnerShort]
			INNER JOIN SUTTER_1P_MIGRATION.TBL.RELATIONSHIP_IND_FINAL T0 ON T3.RE_DB_Id=T0.IRImpID  
			LEFT JOIN [SUTTER_1P_DATA].dbo.HC_Constituents_v T4 ON T3.NEW_TGT_IMPORT_ID_IRLINK=T4.NEW_TGT_IMPORT_ID
 			WHERE  T1.TARKeyInd ='I'
 			ORDER BY T1.TATRImpID 
			--36,734
			--33,428
	    
				--check duplicate
			SELECT * FROM 	 [SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT
			WHERE [Tribute__r:RE_TRImpID__c]='ABSF-TR-RE7.0_8269'

			--check > 4
			SELECT * FROM 	 [SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT
 			WHERE [Tribute__r:RE_TRImpID__c] IN (SELECT [Tribute__r:RE_TRImpID__c] FROM [SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT 
													GROUP BY  [Tribute__r:RE_TRImpID__c]  HAVING COUNT (*)>13)
			ORDER BY  [Tribute__r:RE_TRImpID__c] 
		END 

		BEGIN--Notificant 2
			SELECT 
				[Tribute__r:RE_TRImpID__c]
			  ,	[Contact__r:External_ID__c] AS SF_Field_Value
			  , 'Notificant_'+ CAST(ROW_NUMBER() OVER ( PARTITION BY [Tribute__r:RE_TRImpID__c] ORDER BY [Tribute__r:RE_TRImpID__c]) AS VARCHAR(2)) 
							 +'__r:External_ID__c' AS SF_Field_Api
			INTO [SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT_2
			FROM  [SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT
		END 

		BEGIN--Notificant final
			  
			EXEC HC_PivotWizard_p   '[Tribute__r:RE_TRImpID__c]',	--fields to include as unique identifier/normally it is just a unique ID field.
										'SF_Field_Api',									--column that stores all new phone types
										'SF_Field_Value',										--phone numbers
										'[SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT_2_final',			--INTO..     
										'[SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT_2',					--FROM..
										'SF_Field_Value is not null'							--WHERE..
			
			SELECT * FROM [SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT_2_final
			WHERE [Notificant_11__r:External_Id__c] IS NOT NULL
        
		
			SELECT * FROM [SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT_2_final
			WHERE [Tribute__r:RE_TRImpID__c] IN (SELECT [Tribute__r:RE_TRImpID__c]  FROM [SUTTER_1P_MIGRATION].TBL.TRIBUTE_CONTACT_2_final
													GROUP BY [Tribute__r:RE_TRImpID__c]  HAVING COUNT(*)>1)
		
		
END  