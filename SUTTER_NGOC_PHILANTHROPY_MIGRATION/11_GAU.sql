
USE SUTTER_1P_DATA
GO

BEGIN
	DROP TABLE SUTTER_1P_MIGRATION.IMP.GAU
END


BEGIN
				SELECT
					dbo.fnc_OwnerID() AS OwnerID
					,T.[GAU External ID] AS rC_Giving__External_ID__c
					,T.GAU_Name AS Name
					,T.GAU_Acknowledgement_Text__c AS Acknowledgement_Text__c
					,T.GAU_IsRestricted AS IsRestricted__c
					,T.GAU_Region__c AS Region__c
					,T.GAU_Affiliation AS Affiliation__c
					,T.GAU_Restriction_Type__c AS Restriction_Type__c
					,CAST(T1.FundNote AS VARCHAR(MAX)) AS Comments__c
					,T1.FundStartDate AS Start_Date__c
					,T1.FundEndDate AS End_Date__c
					,T2.FundAttrDesc AS Historic_Fund_ID__c
 					,T1.FundID AS zrefFundId
					,T1.FundDesc AS zrefFundDesc
					,T1.RE_DB_OwnerShort AS zrefREOwner
					,LEN(T.GAU_Name) AS zrefNAMELen
				INTO SUTTER_1P_MIGRATION.IMP.GAU
				FROM SUTTER_1P_DATA.dbo.CHART_Fund AS T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Fund_v T1 ON T.FundID=T1.FundID AND T.RE_DB_OwnerShort=T1.[RE_DB_OwnerShort]
				LEFT JOIN [SUTTER_1P_MIGRATION].TBL.GAU_FundAttribute T2 ON T1.[FundID]=T2.FundId AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
				WHERE T.[GAU External ID] IS NOT NULL
				ORDER BY T.[GAU External ID] 
				--4968
END;   
				INSERT INTO [SUTTER_1P_MIGRATION].[IMP].[GAU]
				   ([OwnerID]
				   ,[rC_Giving__External_ID__c]
				   ,[Name]
				   ,[Acknowledgement_Text__c]
				   ,[IsRestricted__c]
				   ,[Region__c]
				   ,[Affiliation__c]
				   ,[Restriction_Type__c]
				   ,[Comments__c]
				   ,[Start_Date__c]
				   ,[Historic_Fund_ID__c]
				   ,[End_Date__c]
				   ,[zrefFundId]
				   ,[zrefFundDesc]
				   ,[zrefREOwner]
				   ,[zrefNAMELen])
				SELECT DISTINCT 
					dbo.fnc_OwnerID() AS OwnerID
						,T1.[PrimaryGAU_ExternalID__c] AS rC_Giving__External_ID__c
						,T1.PrimaryGAU_Name__c AS Name
						,NULL AS [Acknowledgement_Text__c]
						,NULL AS [IsRestricted__c]
						,NULL AS [Region__c]
						,LEFT(T1.[PrimaryGAU_ExternalID__c],4) AS [Affiliation__c]
						,NULL AS [Restriction_Type__c]
						,NULL AS [Comments__c]
						,NULL AS [Start_Date__c]
						,NULL AS [End_Date__c]
						,NULL AS [Historic_Fund_ID__c]
						,'' AS [zrefFundId]
						,'' AS [zrefFundDesc]
						,NULL AS [zrefREOwner]
						,NULL AS [zrefNAMELen]
					FROM [SUTTER_1P_DATA].dbo.[CHART_Campaign] T1
					LEFT JOIN SUTTER_1P_MIGRATION.IMP.GAU T2 ON T1.[PrimaryGAU_ExternalID__c]=T2.[rC_Giving__External_ID__c]
					WHERE T2.[rC_Giving__External_ID__c] IS NULL AND T1.[PrimaryGAU_ExternalID__c] IS NOT NULL 
	


BEGIN--check for duplicates
	
				--external id
				SELECT * 
				FROM SUTTER_1P_MIGRATION.IMP.GAU
				WHERE rC_Giving__External_ID__c IN (SELECT rC_Giving__External_ID__c FROM SUTTER_1P_MIGRATION.IMP.GAU
													GROUP BY rC_Giving__External_ID__c HAVING COUNT(*)>1)
				ORDER BY rC_Giving__External_ID__c

				--name
				SELECT * 
				FROM SUTTER_1P_MIGRATION.IMP.GAU
				WHERE Name IN (SELECT Name FROM SUTTER_1P_MIGRATION.IMP.GAU
							   GROUP BY Name HAVING COUNT(*)>1)
				ORDER BY Name
			
			
END; 
 
BEGIN-- check len

		SELECT Name, LEN(Name) l
		FROM [SUTTER_1P_MIGRATION].IMP.[GAU]
		WHERE LEN(name)>80
		ORDER BY l desc

		
		SELECT [rC_Giving__Description__c], LEN([rC_Giving__Description__c]) l
		FROM [SUTTER_1P_MIGRATION].IMP.[GAU]
		ORDER BY l desc
 
BEGIN 
		USE [SUTTER_1P_MIGRATION] 
 		EXEC sp_FindStringInTable '%"%', 'IMP', 'GAU'
  
	 	UPDATE [SUTTER_1P_MIGRATION].[IMP].GAU SET rC_Giving__Description__c=REPLACE(rC_Giving__Description__c,'"','''') where rC_Giving__Description__c like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].GAU SET Name=REPLACE(Name,'"','''') where Name like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].GAU SET Acknowledgement_Text__c=REPLACE(Acknowledgement_Text__c,'"','''') where Acknowledgement_Text__c like '%"%'
		UPDATE [SUTTER_1P_MIGRATION].[IMP].GAU SET zrefFundDesc=REPLACE(zrefFundDesc,'"','''') where zrefFundDesc like '%"%'
		 
		SELECT COUNT(*) FROM [SUTTER_1P_MIGRATION].[IMP].GAU 
END
  
  SELECT * FROM [SUTTER_1P_MIGRATION].imp.[GAU] WHERE name='MPHF Greatest Need' OR name ='CPMC CPMC Foundation Unrestricted Gift'
