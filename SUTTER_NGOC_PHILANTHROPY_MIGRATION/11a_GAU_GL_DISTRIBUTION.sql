USE [SUTTER_1P_DATA]
GO

	SELECT * FROM [SUTTER_1P_DATA].dbo.GL_Mapping
	SELECT * FROM [SUTTER_1P_DATA].dbo.GL_Distribution_cpmcf
	SELECT * FROM [SUTTER_1P_DATA].dbo.GL_Distribution_all  
 
BEGIN--consolidate tables. 
	DROP TABLE [SUTTER_1P_DATA].[TBL].[GL_Distribution]
	
	SELECT [FundID]
		  ,[FundDesc]
		  ,[FundDistrGiftType]
		  ,[FundDistrCreditCompany]
		  ,[FundDistrCreditAccountingUnit]
		  ,[FundDistrCreditAccount]
		  ,[FundDistrCreditSubAccount]
		  ,[FundDistrCreditActivity]
		  ,[FundDistrCreditCategory]
		  ,[FundDistrDebitCompany]
		  ,[FundDistrDebitAccountingUnit]
		  ,[FundDistrDebitAccount]
		  ,[FundDistrDebitSubAccount]
		  ,[FundDistrDebitActivity]
		  ,[FundDistrDebitCategory]
  	  INTO [SUTTER_1P_DATA].[TBL].[GL_Distribution]
	  FROM [SUTTER_1P_DATA].[dbo].GL_Distribution_cpmc
	  WHERE FundID IS NOT NULL 
	  UNION ALL
	  SELECT [FundID]
		  ,[FundDesc]
		  ,[FundDistrGiftType]
		  ,[FundDistrCreditCompany]
		  ,[FundDistrCreditAccountingUnit]
		  ,[FundDistrCreditAccount]
		  ,[FundDistrCreditSubAccount]
		  ,[FundDistrCreditActivity]
		  ,[FundDistrCreditCategory]
		  ,[FundDistrDebitCompany]
		  ,[FundDistrDebitAccountingUnit]
		  ,[FundDistrDebitAccount]
		  ,[FundDistrDebitSubAccount]
		  ,[FundDistrDebitActivity]
		  ,[FundDistrDebitCategory]
  	  FROM [SUTTER_1P_DATA].[dbo].GL_Distribution_no_cpmc
 	  WHERE FundID IS NOT NULL 
END 	--37135


	SELECT DISTINCT FundDistrCreditSubAccount 
	FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]
	ORDER BY FundDistrCreditSubAccount
 
	UPDATE  [SUTTER_1P_DATA].[TBL].[GL_Distribution]
	SET  FundDistrCreditSubAccount ='0000'
	WHERE FundDistrCreditSubAccount='0'
  
  	UPDATE  [SUTTER_1P_DATA].[TBL].[GL_Distribution]
	SET  FundDistrCreditSubAccount ='0001'
	WHERE FundDistrCreditSubAccount='1'

  	UPDATE  [SUTTER_1P_DATA].[TBL].[GL_Distribution]
	SET  FundDistrCreditSubAccount ='0102'
	WHERE FundDistrCreditSubAccount='102'

	SELECT DISTINCT FundDistrDebitSubAccount 
	FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]
	ORDER BY FundDistrDebitSubAccount
 
	UPDATE  [SUTTER_1P_DATA].[TBL].[GL_Distribution]
	SET  FundDistrDebitSubAccount ='0000'
	WHERE FundDistrDebitSubAccount='0'
  
  	UPDATE  [SUTTER_1P_DATA].[TBL].[GL_Distribution]
	SET  FundDistrDebitSubAccount ='0001'
	WHERE FundDistrDebitSubAccount='1'

  	UPDATE  [SUTTER_1P_DATA].[TBL].[GL_Distribution]
	SET  FundDistrDebitSubAccount ='0102'
	WHERE FundDistrDebitSubAccount='102'
	
	
	
	BEGIN--GL mapping combo
		DROP TABLE [SUTTER_1P_DATA].TBL.GL_mapping_1
		
		SELECT [FundDistrGiftType], 'FundDistrCreditCompany' AS src_map, FundDistrCreditCompany AS 'src_value'
		INTO [SUTTER_1P_DATA].TBL.GL_mapping_1
		FROM [SUTTER_1P_DATA].[dbo].[GL_Mapping] 
 		WHERE [Converts]='yes'AND FundDistrCreditCompany IS NOT NULL 
		UNION all
		SELECT [FundDistrGiftType], 'FundDistrCreditAccountingUnit' AS src_map, FundDistrCreditAccountingUnit AS 'src_value' 
		FROM [SUTTER_1P_DATA].[dbo].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrCreditAccountingUnit IS NOT null 
		UNION ALL 
		SELECT [FundDistrGiftType], 'FundDistrCreditAccount' AS src_map, FundDistrCreditAccount AS 'src_value' 
		FROM [SUTTER_1P_DATA].[dbo].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrCreditAccount IS NOT null 
		UNION ALL
		SELECT [FundDistrGiftType], 'FundDistrCreditSubAccount' AS src_map, FundDistrCreditSubAccount AS 'src_value' 
		FROM [SUTTER_1P_DATA].[dbo].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrCreditSubAccount IS NOT null 
		UNION ALL
		SELECT [FundDistrGiftType], 'FundDistrCreditActivity' AS src_map, FundDistrCreditActivity AS 'src_value' 
		FROM [SUTTER_1P_DATA].[dbo].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrCreditActivity IS NOT null 
		UNION ALL
		SELECT [FundDistrGiftType], 'FundDistrCreditCategory' AS src_map, FundDistrCreditCategory AS 'src_value' 
		FROM [SUTTER_1P_DATA].[dbo].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrCreditCategory IS NOT null 
		UNION ALL ---DEBIT
		SELECT [FundDistrGiftType], 'FundDistrDebitAccountingUnit' AS src_map, FundDistrDebitAccountingUnit AS 'src_value' 
		FROM [SUTTER_1P_DATA].[dbo].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrDebitAccountingUnit IS NOT null 
		UNION ALL ---DEBIT
		SELECT [FundDistrGiftType], 'FundDistrDebitCompany' AS src_map, FundDistrDebitCompany AS 'src_value'
		FROM [SUTTER_1P_DATA].[dbo].[GL_Mapping] 
 		WHERE [Converts]='yes'AND FundDistrDebitCompany IS NOT NULL 
		UNION ALL ---DEBIT
		SELECT [FundDistrGiftType], 'FundDistrDebitAccount' AS src_map, FundDistrDebitAccount AS 'src_value' 
		FROM [SUTTER_1P_DATA].[dbo].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrDebitAccount IS NOT null 
		UNION ALL ---DEBIT
		SELECT [FundDistrGiftType], 'FundDistrDebitSubAccount' AS src_map, FundDistrDebitSubAccount AS 'src_value' 
		FROM [SUTTER_1P_DATA].[dbo].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrDebitSubAccount IS NOT null 
 		UNION ALL ---DEBIT
		SELECT [FundDistrGiftType], 'FundDistrDebitActivity' AS src_map, FundDistrDebitActivity AS 'src_value' 
		FROM [SUTTER_1P_DATA].[dbo].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrDebitActivity IS NOT null 
 		UNION ALL ---DEBIT
		SELECT [FundDistrGiftType], 'FundDistrDebitCategory' AS src_map, FundDistrDebitCategory AS 'src_value' 
		FROM [SUTTER_1P_DATA].[dbo].[GL_Mapping] 
		WHERE [Converts]='yes'AND FundDistrDebitCategory IS NOT null 
	END		
		 	 

	BEGIN--GL DISTRIBUTION  
		DROP TABLE [SUTTER_1P_DATA].[TBL].[GL_Distribution_1]
	
		SELECT DISTINCT [FundID], [FundDistrGiftType], 'FundDistrCreditAccountingUnit' AS src_dist, FundDistrCreditAccountingUnit AS src_dist_val 
		INTO [SUTTER_1P_DATA].[TBL].[GL_Distribution_1]
		FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]	
 		UNION ALL
		SELECT DISTINCT [FundID], [FundDistrGiftType], 'FundDistrCreditCompany' AS src_dist, FundDistrCreditCompany AS src_dist_val 
		FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]	
 		UNION ALL
		SELECT DISTINCT  [FundID],  [FundDistrGiftType], 'FundDistrCreditAccount' AS src_dist, FundDistrCreditAccount AS src_dist_val 
		FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]	
 		UNION ALL
		SELECT DISTINCT  [FundID], [FundDistrGiftType], 'FundDistrCreditSubAccount' AS src_dist, FundDistrCreditSubAccount AS src_dist_val 
		FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]	
 		UNION ALL
		SELECT DISTINCT  [FundID], [FundDistrGiftType], 'FundDistrCreditActivity' AS src_dist, FundDistrCreditActivity AS src_dist_val 
		FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]	
 		UNION ALL
		SELECT DISTINCT  [FundID], [FundDistrGiftType], 'FundDistrCreditCategory' AS src_dist, FundDistrCreditCategory AS src_dist_val 
		FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]	
	 	UNION ALL --DEBIT
		SELECT DISTINCT  [FundID], [FundDistrGiftType], 'FundDistrDebitAccountingUnit' AS src_dist, FundDistrDebitAccountingUnit AS src_dist_val 
		FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]	
 		UNION ALL --DEBIT
		SELECT DISTINCT  [FundID], [FundDistrGiftType], 'FundDistrDebitCompany' AS src_dist, FundDistrDebitCompany AS src_dist_val 
		FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]	
   		UNION ALL --DEBIT
		SELECT DISTINCT  [FundID], [FundDistrGiftType], 'FundDistrDebitAccount' AS src_dist, FundDistrDebitAccount AS src_dist_val 
		FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]	
 		UNION ALL --DEBIT
		SELECT DISTINCT  [FundID], [FundDistrGiftType], 'FundDistrDebitSubAccount' AS src_dist, FundDistrDebitSubAccount AS src_dist_val 
		FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]	
 		UNION ALL --DEBIT
		SELECT DISTINCT  [FundID], [FundDistrGiftType], 'FundDistrDebitActivity' AS src_dist, FundDistrDebitActivity AS src_dist_val 
		FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]	
 		UNION ALL --DEBIT
		SELECT DISTINCT  [FundID], [FundDistrGiftType], 'FundDistrDebitCategory' AS src_dist, FundDistrDebitCategory AS src_dist_val 
		FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution]	
 	END --427340
  		  

   	BEGIN--GL distribution 2 
		DROP TABLE [SUTTER_1P_DATA].[TBL].[GL_Distribution_2] 
		 
		SELECT DISTINCT T1.FundID, T1.[FundDistrGiftType], T1.src_dist , T2.src_value, T1.src_dist_val
		INTO [SUTTER_1P_DATA].[TBL].[GL_Distribution_2] 
		FROM [SUTTER_1P_DATA].[TBL].[GL_Distribution_1] AS T1
		INNER JOIN TBL.GL_Mapping_1 AS T2 ON T1.[FundDistrGiftType]=T2.[FundDistrGiftType]
												  AND T1.src_dist =t2.src_map
	 	ORDER BY T1.FundId, T1.[FundDistrGiftType]
	END --40156																		 
	 
		BEGIN--PHONES_5: CREATE final pivot phone tables --PHONE TYPES from SF_Field_API
			
			DROP TABLE [SUTTER_1P_DATA].TBL.[GL_Distribution_final]  
			
			EXEC HC_PivotWizard_p   'FundId',	--fields to include as unique identifier/normally it is just a unique ID field.
										'src_value',									--column that stores all new phone types
										'src_dist_val',										--phone numbers
										'SUTTER_1P_DATA.TBL.[GL_Distribution_final]',	--INTO..     
										'SUTTER_1P_DATA.TBL.[GL_Distribution_2]',		--FROM..
										'src_dist_val is not null'							--WHERE..
	 	END --332009
     
		 SELECT * FROM [SUTTER_1P_DATA].TBL.[GL_Distribution_final]
		 WHERE [FundId] IN (SELECT fundid FROM [SUTTER_1P_DATA].TBL.[GL_Distribution_final] GROUP BY fundid HAVING COUNT(*)>1)
		 ORDER BY [FundId]
		
		  SELECT * FROM [SUTTER_1P_DATA].TBL.[GL_Distribution_final]
		  ORDER BY [FundID]

SELECT DISTINCT fundid FROM  [SUTTER_1P_DATA].[TBL].[GL_Distribution]
SELECT DISTINCT fundid FROM  [SUTTER_1P_DATA].[TBL].[GL_Distribution_1]
SELECT DISTINCT fundid FROM  [SUTTER_1P_DATA].[TBL].[GL_Distribution_2] 
							    
SELECT * FROM  [SUTTER_1P_DATA].[TBL].[GL_Distribution]	 WHERE [FundDistrGiftType] IS null
