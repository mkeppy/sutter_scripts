 /*
 --1. case 1: there may be an errro on hte current giving amont for record SAFH-GT-04121-553-0000139279 and SAFH-GT-04121-553-0000139284
 --2. case 2: SAFH-GT-04121-553-0000138454: the Giving Giving Amount was = to the total pledge amount. acccording to RC, it should be the installment amt. 
	the Expected giving amount is the total pledge amot 

*/

--OPEN TRANSACTION GENERATION PROCESS tests
 
	 SELECT * FROM SUTTER_MGEA_MIGRATION.[IMP].[OPPORTUNITY_PARENT] WHERE stagename='open' 
	 AND 
--       external_id__c='SAFH-GT-04121-553-0000138454' 
--	OR 
--		external_id__c='SAFH-GT-04121-553-0000139279'
--	OR 
--		external_id__c='SAFH-GT-04121-553-0000139284'
--	or 
--		external_id__c='SMCF-GT-03947-553-0000300770'
--	or 
--		external_id__c='SMCF-GT-03947-553-0000300888'
--	or 
--		external_id__c='SMCF-GT-03947-553-0000305969'
--	or 
--		external_id__c='SMCF-GT-03947-553-0000348678'
--	OR 
		external_id__c='SMCF-GT-03947-553-0000350675'
	 ORDER by external_id__c

	 SELECT 
       [ACCOUNT:External_Id__c]
      ,[External_Id__c]
      ,[rC_Giving__Parent__r:External_ID__c]
      ,[NAME]
      ,[CloseDate]
      ,[RecordTypeID]
      ,[StageName]
      ,[Amount]
      ,[rC_Giving__Transaction_Type__c]
      ,[rC_Giving__Affiliation__c]
      ,[rC_Giving__Is_Giving_Transaction__c]
      ,[zrefGFType]
      ,[zrefParentGFImpID]
      ,[zrefPaymentGFImpID]
      ,[zrefPaymentGSplitImpID]
      ,[zrefRE_DB_Owner]
      ,[zrefGLinkAmt]
 
	 FROM SUTTER_MGEA_MIGRATION.[IMP].[OPPORTUNITY_TRANSACTION] WHERE 
		   [rC_Giving__Parent__r:External_ID__c]='SAFH-GT-04121-553-0000138454'
		OR 
		[rC_Giving__Parent__r:External_ID__c]='SAFH-GT-04121-553-0000139279'
		OR 
		[rC_Giving__Parent__r:External_ID__c]='SAFH-GT-04121-553-0000139284'
		or 
		[rC_Giving__Parent__r:External_ID__c]='SMCF-GT-03947-553-0000300770'
		or 
		[rC_Giving__Parent__r:External_ID__c]='SMCF-GT-03947-553-0000300888'
		or 
		[rC_Giving__Parent__r:External_ID__c]='SMCF-GT-03947-553-0000305969'
		or 
		[rC_Giving__Parent__r:External_ID__c]='SMCF-GT-03947-553-0000348678'
		OR 
		[rC_Giving__Parent__r:External_ID__c]='SMCF-GT-03947-553-0000350675'
	 ORDER BY [rC_Giving__Parent__r:External_ID__c] 


	SELECT T.RE_DB_OwnerShort, T.GFType, T2.GSplitImpID, T.GFImpId, SUM(T2.GSplitAmt) TotalPaymentAmt, COUNT(*) TotalNumberPayments
	FROM SUTTER_MGEA_DATA.dbo.HC_Gift_Link_v T
	INNER JOIN SUTTER_MGEA_DATA.dbo.HC_Gift_SplitGift_v T2 ON T.GFImpID=T2.GFImpID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	WHERE T2.GSplitImpID='04121-553-0000138454' 
	or T2.GSplitImpID='04121-553-0000139279'
	OR T2.GSplitImpID='04121-553-0000139284'
	OR T2.GSplitImpID='03947-553-0000300770'
	or T2.GSplitImpID='03947-553-0000300888'
	or T2.GSplitImpID='03947-553-0000305969'
	or T2.GSplitImpID='03947-553-0000348678'
	OR T2.GSplitImpID='03947-553-0000350675'
	GROUP BY T.RE_DB_OwnerShort, T2.GSplitImpID, T.GFType, T.GFImpId
	ORDER BY T.RE_DB_OwnerShort, T2.GSplitImpID, T.GFType, T.GFImpId

	
	SELECT * FROM SUTTER_MGEA_DATA.dbo.HC_Gift_SplitGift_v WHERE GSplitImpID='03947-553-0000305969' OR GFImpId='04121-553-0000139284'
	SELECT * FROM SUTTER_MGEA_DATA.dbo.HC_Gift_Link_v WHERE GFImpID='04121-553-0000139284'
	SELECT * FROM SUTTER_MGEA_DATA.dbo.HC_Gift_v WHERE GFImpID='04121-553-0000139284'
	SELECT * FROM SUTTER_MGEA_DATA.dbo.HC_Gift_Installments_v WHERE GFLink='04121-553-0000139284'

	SELECT GFImpId, GFTAmt,GFInsNumPay, GFInsFreq, GFType, (GFTAmt/GFInsNumPay) giving_giving_amt
	FROM SUTTER_MGEA_DATA.dbo.HC_Gift_v WHERE GFImpID='03947-545-0000445796'

	SELECT * FROM SUTTER_MGEA_MIGRATION.DBO.CHART_GiftType  WHERE parent_giving_recordType IS NOT NULL
    


