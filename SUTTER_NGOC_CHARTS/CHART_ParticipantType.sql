USE SUTTER_1P_DATA
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ParticipantType]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.ParticipantType
	
--CHARTS.PARTICIPANT_TYPE
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			INTO SUTTER_1P_DATA.CHART.ParticipantType 
			FROM RE7_SH_ABSF.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_CVRX.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_DMHF.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_EMCF.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_HOTV.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_MPHF.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_PAMF.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_SAFH.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_SAHF.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_SCAH.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_SDHF.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_SMCF.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_SMFX.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_SRMC.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_SSCF.CHART.ParticipantType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
			FROM RE7_SH_WBRX.CHART.ParticipantType
			GO
			
