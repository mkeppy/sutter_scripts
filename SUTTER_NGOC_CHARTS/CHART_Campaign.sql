USE SUTTER_1P_DATA
GO

 
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Campaign]') and objectproperty(id, N'IsTable') = 1)
	drop table  SUTTER_1P_DATA.CHART.Campaign
	
		 --CHARTS.Campaign (a.k.a.CampFundAppPack)
			SELECT [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			INTO SUTTER_1P_DATA.[CHART].Campaign
			FROM RE7_SH_ABSF.[CHART].[FundCampAppPack]	
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_CVRX.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_DMHF.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_EMCF.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_HOTV.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_MPHF.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_PAMF.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_SAFH.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_SAHF.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_SCAH.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_SDHF.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_SMCF.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_SMFX.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_SRMC.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_SSCF.[CHART].[FundCampAppPack]
			GO
			INSERT INTO SUTTER_1P_DATA.[CHART].Campaign( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
			SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
			FROM RE7_SH_WBRX.[CHART].[FundCampAppPack]
			GO

