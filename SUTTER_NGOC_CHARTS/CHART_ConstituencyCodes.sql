USE SUTTER_1P_DATA
GO
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConstituencyCodes]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.ConstituencyCodes
	
	 
		--CHARTS.CONSTITUENCY_CODES
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			INTO SUTTER_1P_DATA.CHART.ConstituencyCodes   
			FROM RE7_SH_ABSF.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_CVRX.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_DMHF.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_EMCF.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_HOTV.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_MPHF.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_PAMF.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_SAFH.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_SAHF.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_SCAH.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_SDHF.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_SMCF.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_SMFX.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_SRMC.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_SSCF.CHART.ConstituencyCodes
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
			SELECT RE_DB_OwnerShort, KeyInD, 
			CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
			FROM RE7_SH_WBRX.CHART.ConstituencyCodes
			GO
