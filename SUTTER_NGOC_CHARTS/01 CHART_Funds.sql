--DATABASES on [HPZ220\SS2008R2XP]
--RE7_ABSF;RE7_CPMC;RE7_DMHF;RE7_EMCF;RE7_MHLB;RE7_MPHF;RE7_PAMF;RE7_SAFH;RE7_SAHF;RE7_SCAH;RE7_SDHF;RE7_SMCF;RE7_SMFn;RE7_SRMC;RE7_SSCF




SELECT 'ABSF' [CLIENT], FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions
INTO CHART.Fund
FROM [HPZ220\SS2008R2XP].RE7_ABSF.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'CPMC' [CLIENT], FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions
FROM [HPZ220\SS2008R2XP].RE7_CPMC.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'DMHF' [CLIENT], FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions  
FROM [HPZ220\SS2008R2XP].RE7_DMHF.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'EMCF' [CLIENT], FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions  
FROM [HPZ220\SS2008R2XP].RE7_EMCF.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'MHLB' [CLIENT] ,  FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions --collate SQL_Latin1_General_CP1_CI_AS --REVIEW DATA 
FROM [HPZ220\SS2008R2XP].RE7_MHLB.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'MPHF' [CLIENT] ,  FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions --collate SQL_Latin1_General_CP1_CI_AS --REVIEW DATA 
FROM [HPZ220\SS2008R2XP].RE7_MPHF.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'PAMF' [CLIENT] ,  FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions --collate SQL_Latin1_General_CP1_CI_AS --REVIEW DATA 
FROM [HPZ220\SS2008R2XP].RE7_PAMF.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'SAFH' [CLIENT] ,  FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions --collate SQL_Latin1_General_CP1_CI_AS --REVIEW DATA 
FROM [HPZ220\SS2008R2XP].RE7_SAFH.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'SAHF' [CLIENT] ,  FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions --collate SQL_Latin1_General_CP1_CI_AS --REVIEW DATA 
FROM [HPZ220\SS2008R2XP].RE7_SAHF.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'SCAH' [CLIENT] ,  FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions --collate SQL_Latin1_General_CP1_CI_AS --REVIEW DATA 
FROM [HPZ220\SS2008R2XP].RE7_SCAH.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'SDHF' [CLIENT] ,  FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions --collate SQL_Latin1_General_CP1_CI_AS --REVIEW DATA 
FROM [HPZ220\SS2008R2XP].RE7_SDHF.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'SMCF' [CLIENT] ,  FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions --collate SQL_Latin1_General_CP1_CI_AS --REVIEW DATA 
FROM [HPZ220\SS2008R2XP].RE7_SMCF.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'SMFN' [CLIENT] ,  FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions --collate SQL_Latin1_General_CP1_CI_AS --REVIEW DATA 
FROM [HPZ220\SS2008R2XP].RE7_SMFN.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'SRMC' [CLIENT] ,  FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions --collate SQL_Latin1_General_CP1_CI_AS --REVIEW DATA 
FROM [HPZ220\SS2008R2XP].RE7_SRMC.CHART.Fund

INSERT INTO CHART.Fund (client, FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions)
SELECT 'SSCF' [CLIENT] ,  FundID, FundDesc, Count, GiftSum, Gifts, Proposals, ConsActions --collate SQL_Latin1_General_CP1_CI_AS --REVIEW DATA 
FROM [HPZ220\SS2008R2XP].RE7_SSCF.CHART.Fund


-- CHECK 
--  SELECT * FROM CHART.Fund ORDER BY CLIENT

 
--EXPORT CHART TO XLS
 /*
	Exec master..xp_cmdshell 
	'bcp "select * from RE_SUTTER_TEST.CHART.Fund ORDER BY CLIENT" queryout "S:\Conversion Worksheets\CHART_FUND.xls" -c -T -SHPZ220\SS2008R2 ' 

 */ 
 
 /*
(714 row(s) affected)

(1895 row(s) affected)

(52 row(s) affected)

(46 row(s) affected)

(242 row(s) affected)

(227 row(s) affected)

(353 row(s) affected)

(111 row(s) affected)

(26 row(s) affected)

(313 row(s) affected)

(71 row(s) affected)

(387 row(s) affected)

(68 row(s) affected)

(146 row(s) affected)

(103 row(s) affected)

*/