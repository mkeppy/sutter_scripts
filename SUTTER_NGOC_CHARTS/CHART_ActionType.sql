USE SUTTER_1P_DATA
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ActionType]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.ActionType

	--CHART.ACTION TYPE
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			INTO SUTTER_1P_DATA.CHART.ActionType 
			FROM RE7_SH_ABSF.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_CVRX.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_DMHF.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_EMCF.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_HOTV.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_MPHF.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_PAMF.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_SAFH.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_SAHF.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_SCAH.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_SDHF.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_SMCF.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_SMFX.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_SRMC.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_SSCF.CHART.ActionType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
			FROM RE7_SH_WBRX.CHART.ActionType
			GO
			 