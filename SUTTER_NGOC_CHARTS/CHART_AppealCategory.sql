USE SUTTER_1P_DATA
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[AppealCategory]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.AppealCategory
	

		--CHARTS.AppealCategory
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			INTO SUTTER_1P_DATA.CHART.AppealCategory 
			FROM RE7_SH_ABSF.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_CVRX.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_DMHF.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_EMCF.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_HOTV.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_MPHF.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_PAMF.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SAFH.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SAHF.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SCAH.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SDHF.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SMCF.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SMFX.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SRMC.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SSCF.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
			SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
			FROM RE7_SH_WBRX.dbo.HC_Appeal_v
			GROUP BY RE_DB_OwnerShort, AppCategory
			GO
