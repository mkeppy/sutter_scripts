USE SUTTER_1P_DATA
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[CampaignCategory]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.CampaignCategory
	

		--CHARTS.CampaignCategory
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			INTO SUTTER_1P_DATA.CHART.CampaignCategory 
			FROM RE7_SH_ABSF.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_CVRX.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_DMHF.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_EMCF.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_HOTV.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_MPHF.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_PAMF.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SAFH.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SAHF.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SCAH.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SDHF.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SMCF.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SMFX.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SRMC.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SSCF.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.CampaignCategory (RE_DB_OwnerShort, CampCategory, [count])
			SELECT RE_DB_OwnerShort, CampCategory, COUNT(*) AS [Count]
			FROM RE7_SH_WBRX.dbo.HC_Campaign_v
			GROUP BY RE_DB_OwnerShort, CampCategory
			GO
