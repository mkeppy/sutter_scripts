USE [SUTTER_1P_DATA]
GO

	 
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GFVehicle]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.GFVehicle




					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
				    INTO SUTTER_1P_DATA.CHART.GFVehicle
					FROM RE7_SH_ABSF.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!='' 
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle 
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle,  COUNT(*) AS [Count]
					FROM RE7_SH_CVRX.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
 					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_DMHF.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_EMCF.dbo.HC_Gift_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_HOTV.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_MPHF.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_PAMF.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_SAFH.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_SAHF.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_SCAH.dbo.HC_Gift_v T1
  					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_SDHF.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_SMCF.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_SMFX.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_SRMC.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_SSCF.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.GFVehicle (RE_DB_OwnerShort, GFVehicle, [count])
					SELECT T1.RE_DB_OwnerShort, T1.GFVehicle, COUNT(*) AS [Count]
					FROM RE7_SH_WBRX.dbo.HC_Gift_v T1
 					WHERE T1.GFVehicle IS NOT NULL AND T1.[GFVehicle]!=''
					GROUP by T1.RE_DB_OwnerShort, T1.GFVehicle
					GO

