USE SUTTER_1P_DATA
GO
			 

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ActionAttribute]') and OBJECTPROPERTY(id, N'IsTable') = 1)
	drop table CHART.[ActionAttribute]
	
--CHARTS.ActionAttribute
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			INTO SUTTER_1P_DATA.CHART.ActionAttribute   
			FROM RE7_SH_ABSF.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_CVRX.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_DMHF.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_EMCF.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_HOTV.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_MPHF.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_PAMF.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_SAFH.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_SAHF.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_SCAH.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_SDHF.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_SMCF.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_SMFX.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_SRMC.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_SSCF.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionAttribute (RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType, [count]
			FROM RE7_SH_WBRX.CHART.ActionAttribute
			WHERE DataType='TableEntry'
			GO
		