/*

USE SUTTER_1P_DATA
GO


--CHARTS OF FUND, CAMP, APPEAL, PKG from Gift_v (NON-SPLIT) and ConsAppeal

	--CHART_FundCampaignAppealPackage  (GIFT and CONS_APPEALS records combined)

	--CONS APPEALS
	 
		SELECT t.RE_DB_OwnerShort, NULL AS FundId, NULL AS FundDesc, null as CampID, null as CampDesc, t.CAPAppealId AppealID, dbo.HC_Appeal_v.AppDesc, 
		t.CAPPAckageId PackageID ,  dbo.HC_Package_v.PackDesc, count(*) as Count, null as totalamt

		INTO dbo.HC_FundCampaignAppealPackage_non_split_t-- drop table dbo.HC_FundCampaignAppealPackage_non_split_t
		FROM [dbo].[HC_Cons_Appeal_v] t
		LEFT OUTER JOIN dbo.HC_Package_v ON T.RE_DB_OwnerShort = dbo.HC_Package_v.RE_DB_OwnerShort 
					AND T.CAPAppealId= dbo.HC_Package_v.AppealID 
					AND T.CAPPackageId= dbo.HC_Package_v.PackageID 
		LEFT OUTER JOIN dbo.HC_Appeal_v ON T.RE_DB_OwnerShort = dbo.HC_Appeal_v.RE_DB_OwnerShort 
					AND T.CAPAppealId= dbo.HC_Appeal_v.AppealID
		group by t.RE_DB_OwnerShort, t.CAPAppealId, t.CAPPAckageId, dbo.HC_Appeal_v.AppDesc,  dbo.HC_Package_v.PackDesc

		UNION all

	--GIFTS
		SELECT	dbo.HC_Gift_v.RE_DB_OwnerShort, 
				LTRIM(RTRIM(dbo.HC_Gift_v.FundID)) AS FundID, dbo.HC_Fund_v.FundDesc, 
				LTRIM(RTRIM(dbo.HC_Gift_v.CampID)) AS CampID, dbo.HC_Campaign_v.CampDesc, 
				LTRIM(RTRIM(dbo.HC_Gift_v.GFAppeal)) AS AppealID, dbo.HC_Appeal_v.AppDesc, 
				LTRIM(RTRIM(dbo.HC_Gift_v.PackageID)) as PackageID, dbo.HC_Package_v.PackDesc, 
				COUNT(*) RecordCount, sum(dbo.HC_Gift_v.GFTAmt) as totalamt
		
		FROM    dbo.HC_Gift_v 
		LEFT JOIN dbo.HC_Fund_v ON	   dbo.HC_Gift_v.RE_DB_OwnerShort = dbo.HC_Fund_v.RE_DB_OwnerShort AND dbo.HC_Gift_v.FundID= dbo.HC_Fund_v.FundID 
		LEFT JOIN dbo.HC_Campaign_v ON dbo.HC_Gift_v.RE_DB_OwnerShort = dbo.HC_Campaign_v.RE_DB_OwnerShort AND dbo.HC_Gift_v.CampID = dbo.HC_Campaign_v.CampID 
		LEFT JOIN dbo.HC_Appeal_v ON   dbo.HC_Gift_v.RE_DB_OwnerShort = dbo.HC_Appeal_v.RE_DB_OwnerShort AND dbo.HC_Gift_v.GFAppeal = dbo.HC_Appeal_v.AppealID
		LEFT JOIN dbo.HC_Package_v ON  dbo.HC_Gift_v.RE_DB_OwnerShort = dbo.HC_Package_v.RE_DB_OwnerShort AND dbo.HC_Gift_v.GFAppeal = dbo.HC_Package_v.AppealID 
																			AND dbo.HC_Gift_v.PackageID = dbo.HC_Package_v.PackageID 
		GROUP BY dbo.HC_Gift_v.FundID, dbo.HC_Fund_v.FundDesc, dbo.HC_Gift_v.CampID, dbo.HC_Campaign_v.CampDesc, dbo.HC_Gift_v.GFAppeal, dbo.HC_Appeal_v.AppDesc, 
							  dbo.HC_Gift_v.PackageID, dbo.HC_Package_v.PackDesc, dbo.HC_Gift_v.RE_DB_OwnerShort
		 
		GO 

		

				GO
				SET ANSI_NULLS OFF
				GO
				SET QUOTED_IDENTIFIER OFF
				GO

				-- CHART Campaign_1P
				SET ANSI_NULLS ON
				GO
				SET QUOTED_IDENTIFIER ON
				GO
				
				SELECT	s.RE_DB_OwnerShort, 
						CASE WHEN s.FundID IS NULL or s.FundID='' THEN 'NULL' ELSE s.FundID END AS FundID, s.FundDesc, 
						CASE WHEN s.CampID IS NULL or s.CampID ='' THEN 'NULL' ELSE s.CampID END AS CampID, s.CampDesc, 
						CASE WHEN s.AppealID  IS NULL or s.AppealID ='' THEN 'NULL' ELSE s.AppealID END AS AppealID, s.AppDesc, 
						CASE WHEN s.PackageId  IS NULL or s.PackageId ='' THEN 'NULL' ELSE s.PackageId END AS PackageID, s.PackDesc,
						sum(s.[Count]) as [Count], convert(decimal(18,2), sum(s.totalamt)) as [Sum]
				INTO SUTTER_1P_DATA.CHART.Campaign_1P 
				FROM dbo.HC_FundCampaignAppealPackage_non_split_t s
				GROUP BY s.RE_DB_OwnerShort,s.FundId, s.FundDesc, s.CampID, s.AppealID, s.CampDesc, s.AppDesc, s.PackageId, s.PackDesc

				GO
				SET ANSI_NULLS OFF
				GO
				SET QUOTED_IDENTIFIER OFF
				GO
	

	 SELECT * FROM SUTTER_1P_DATA.CHART.Campaign_1P WHERE campid LIKE '%;%' OR appealid LIKE '%;%' OR packageid LIKE '%;%''''


	 */