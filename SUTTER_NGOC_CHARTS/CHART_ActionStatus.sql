USE SUTTER_1P_DATA
GO
 

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ActionStatus]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.ActionStatus
	
	--CHART.ACTION STATUS
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			INTO SUTTER_1P_DATA.CHART.ActionStatus 
			FROM RE7_SH_ABSF.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_CVRX.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_DMHF.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_EMCF.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_HOTV.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_MPHF.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_PAMF.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_SAFH.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_SAHF.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_SCAH.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_SDHF.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_SMCF.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_SMFX.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_SRMC.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_SSCF.CHART.ActionStatus
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
			FROM RE7_SH_WBRX.CHART.ActionStatus
			GO
	