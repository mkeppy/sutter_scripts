 

USE SUTTER_1P_DATA
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ProposalAttribute]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.[ProposalAttribute]

--CHART.PRStatus
		
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]
			FROM  RE7_SH_ABSF.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_CVRX.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_DMHF.CHART.[ProposalAttribute]
			GO			
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_EMCF.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_HOTV.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_MPHF.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_PAMF.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_SAFH.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_SAHF.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_SCAH.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_SDHF.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_SMCF.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_SMFX.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_SRMC.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_SSCF.CHART.[ProposalAttribute]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.[ProposalAttribute]([RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count])
			SELECT [RE_DB_OwnerShort], [PRAttrCat], [PRAttrDesc], [Count]
			FROM  RE7_SH_WBRX.CHART.[ProposalAttribute]
			GO
