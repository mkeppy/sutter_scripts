USE SUTTER_1P_DATA
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[RelatRecipType]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.RelatRecipType

		--CHART.RelatRecipType. For REFERENCE to convert RELATIONSHIP CATEGORIES.
									
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
		 	INTO SUTTER_1P_DATA.CHART.RelatRecipType
			FROM RE7_SH_ABSF.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_CVRX.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_DMHF.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_EMCF.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_HOTV.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_MPHF.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_PAMF.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_SAFH.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_SAHF.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_SCAH.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_SDHF.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_SMCF.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_SMFX.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_SRMC.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_SSCF.CHART.RelatRecipType
			
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
			SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
			FROM RE7_SH_WBRX.CHART.RelatRecipType
			
			GO
			
			
			
			
			
			