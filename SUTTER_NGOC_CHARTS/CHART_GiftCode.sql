USE SUTTER_1P_DATA
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftCode]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.GiftCode 
	
--CHARTS.GIFT_CODE
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			INTO SUTTER_1P_DATA.CHART.GiftCode 
			FROM RE7_SH_ABSF.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_CVRX.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_DMHF.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_EMCF.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_HOTV.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_MPHF.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_PAMF.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_SAFH.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_SAHF.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_SCAH.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_SDHF.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_SMCF.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_SMFX.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_SRMC.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_SSCF.CHART.GiftCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
			FROM RE7_SH_WBRX.CHART.GiftCode
			GO
