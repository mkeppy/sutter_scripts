
USE SUTTER_1P_DATA
GO

--Review charts to find new values
--1. Export CHARTS from excel to Access. 
--2. IMPORT CHARTS from SQL SQL Server. These charts are named with the DBO schema, i.e. DBO.CHART_name
--3. New charts from views and data consolidation are 


--charts from Excel (SQL_Latin1_General_CP1_CI_AS)
--charts from sql s (Latin1_General_CI_AI)
		   

--CHART_ACTION_ATTRIBUTE

	SELECT  T1.RE_DB_OwnerShort, T1.ACAttrCat, T1.ACAttrDesc, T1.DataType, T1.count, T2.RE_DB_OwnerShort AS DB, T2.ACAttrCat AS CAT, 
                     T2.ACAttrDesc AS DES
	FROM	CHART.[ActionAttribute] AS T1 
			LEFT JOIN dbo.CHART_ActionAttribute AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RE_DB_OwnerShort 
													AND T1.ACAttrCat COLLATE SQL_Latin1_General_CP1_CI_AS = T2.ACAttrCat 
													AND T1.ACAttrDesc COLLATE SQL_Latin1_General_CP1_CI_AS = T2.ACAttrDesc
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.ACAttrCat IS NULL) AND (T2.ACAttrDesc IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.ACAttrCat, T1.ACAttrDesc


         

--CHART_ACTION_SOLICITOR

		UPDATE [SUTTER_1P_DATA].CHART.ActionSolicitor SET AddedBy='NULL' WHERE AddedBy='' OR AddedBy IS NULL
		UPDATE [SUTTER_1P_DATA].CHART.ActionSolicitor SET ACSolImpID='NULL' WHERE ACSolImpID='' OR ACSolImpID IS NULL
	
		SELECT T1.RE_DB_OWNERSHORT, T1.AddedBy, T1.ACSolImpID, T1.CONSID, T1.NAME, T1.ISSOLICITOR, T1.[COUNT]
		FROM [SUTTER_1P_DATA].CHART.ActionSolicitor AS T1
		LEFT JOIN[SUTTER_1P_DATA].DBO.CHART_ActionSolicitor AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RE_DB_OwnerShort
												AND  T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS = T2.AddedBy
												AND T1.ACSolImpID COLLATE SQL_Latin1_General_CP1_CI_AS = T2.ACSolImpID
		WHERE T2.RE_DB_OwnerShort IS NULL AND T2.AddedBy IS NULL AND T2.ACSolImpID IS NULL
		ORDER BY T1.RE_DB_OwnerShort, T1.Name 




--CHART_ACStatus

	SELECT	T1.RE_DB_OwnerShort, T1.ACStatus, T1.count 
	FROM    CHART.ActionStatus AS T1 
	LEFT  JOIN dbo.CHART_ActionStatus AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RE_DB_OwnerShort 
											AND T1.ACStatus COLLATE SQL_Latin1_General_CP1_CI_AS = T2.ACStatus
	WHERE	T2.RE_DB_OwnerShort IS NULL AND T2.ACStatus IS NULL
	ORDER BY T1.RE_DB_OwnerShort, T1.ACStatus


 --CHART_ACTION TYPE
	
	SELECT	T1.RE_DB_OwnerShort, T1.ACType, T1.count 
	FROM	CHART.ActionType AS T1 
			LEFT JOIN dbo.CHART_ActionType AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RE_DB_OwnerShort AND T1.ACType COLLATE SQL_Latin1_General_CP1_CI_AS = T2.ACType
	WHERE	T2.ACType IS NULL
	ORDER BY T1.RE_DB_OwnerShort, T1.ACType
	

--CHART_ADDRESS_TYPE
	SELECT	T1.RE_DB_OwnerShort, T1.AddrType, T1.count, T2.RE_DB_OwnerShort AS DB, T2.AddrType AS AT
	FROM	CHART.AddressType AS T1 
			LEFT JOIN dbo.CHART_AddressType AS T2 ON T1.RE_DB_OwnerShort  COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.RE_DB_OwnerShort AND T1.AddrType  COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.AddrType
	WHERE	(T2.RE_DB_OwnerShort IS NULL) AND (T2.AddrType IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.AddrType
	

--CHART_ADDSALTYPE
	SELECT T1.RE_DB_OwnerShort, T1.AddSalType, T1.[Count]
	FROM CHART.AddSalType AS T1
	LEFT JOIN dbo.CHART_AddSalType T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RE_DB_OwnerShort 
										AND T1.AddSalType COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.AddSalType
	WHERE T2.RE_DB_OwnerShort IS NULL AND T2.AddSalType IS NULL 
	ORDER BY T1.RE_DB_OwnerShort, T1.AddSalType


--CHART_APPEAL CATEGORY

	UPDATE CHART.AppealCategory SET AppCategory='NULL' where AppCategory IS NULL OR AppCategory=''
	UPDATE dbo.CHART_AppealCategory SET AppCategory='NULL' where AppCategory IS NULL OR AppCategory=''

	SELECT T1.RE_DB_OwnerShort, T1.AppCategory, T1.[Count]
	FROM CHART.AppealCategory AS T1 
	LEFT JOIN dbo.CHART_AppealCategory AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RE_DB_OwnerShort
											AND T1.AppCategory COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.AppCategory
	WHERE T2.RE_DB_OwnerShort IS NULL AND T2.AppCategory IS NULL
		AND T1.AppCategory IS NOT NULL	


--CHART_ATTRIBUTE
	SELECT	T1.RE_DB_OwnerShort, T1.[type], T1.datatype, T1.category, T1.tablename, T1.[required], T1.[unique], T1.active, T1.count, 
			T2.RE_DB_OwnerShort AS DB, T2.[type] AS TY, T2.datatype AS DT, T2.category AS CAT
	FROM    CHART.Attributes AS T1 
			LEFT JOIN dbo.CHART_Attributes AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.RE_DB_OwnerShort 
												AND T1.[type] COLLATE SQL_Latin1_General_CP1_CI_AS = T2.[type] 
												AND T1.datatype COLLATE SQL_Latin1_General_CP1_CI_AS = T2.datatype 
												AND T1.category COLLATE SQL_Latin1_General_CP1_CI_AS = T2.category
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.[type] IS NULL) AND (T2.datatype IS NULL) AND (T2.category IS NULL)  
	ORDER BY T1.RE_DB_OwnerShort, T1.[type], T1.category
	
 
--CHART CAMPAIGN
	SELECT  T1.RE_DB_OwnerShort, T1.FundID, T1.FundDesc, T1.CampID, T1.CampDesc, T1.AppealID, T1.AppDesc, T1.PackageID, T1.PackDesc, T1.Count, 
                      T1.Sum --, T2.RE_DB_OwnerShort AS DB, T2.FundID AS F, T2.CampID AS C, T2.AppealID AS A, T2.PackageID AS P
	FROM	[SUTTER_1P_DATA].CHART.Campaign AS T1 
			LEFT JOIN [SUTTER_1P_DATA].dbo.CHART_Campaign AS T2 ON T1.PackageID  COLLATE SQL_Latin1_General_CP1_CI_AS = T2.PackageID 
											  AND T1.AppealID  COLLATE SQL_Latin1_General_CP1_CI_AS = T2.AppealID 
											  AND T1.CampID  COLLATE SQL_Latin1_General_CP1_CI_AS = T2.CampID 
											  AND T1.FundID  COLLATE SQL_Latin1_General_CP1_CI_AS = T2.FundID 
											  AND T1.RE_DB_OwnerShort  COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RE_DB_OwnerShort
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.FundID IS NULL) AND (T2.CampID IS NULL) AND (T2.AppealID IS NULL) AND (T2.PackageID IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.FundID, T1.CampID, T1.AppealID, T1.PackageID
	
		SELECT * FROM [SUTTER_1P_DATA].dbo.[CHART_Fund] WHERE [FundID]='BHEBA Diab Res Proj'
  
--CAMPAIGN CAGEGORY
	SELECT T1.* 
	FROM [CHART].[CampaignCategory] AS T1
	LEFT JOIN dbo.chart_CampaignCategory  AS T2 ON T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort] AND T1.[CampCategory]=T2.[CampCategory]
	WHERE T2.CampCategory IS null AND T1.[CampCategory] IS NOT null
	ORDER BY T1.[RE_DB_OwnerShort], T1.[CampCategory]

	   
--CHART CONS ATTRIBUTE 

	SELECT  T1.RE_DB_OwnerShort, T1.CAttrCat, T1.CAttrDesc, T1.DataType, T1.count, T2.RE_DB_OwnerShort AS DB, T2.CAttrCat AS CAT, 
                     T2.CAttrDesc AS DES
	FROM	CHART.ConsAttribute AS T1 
			LEFT JOIN dbo.CHART_ConsAttributes AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RE_DB_OwnerShort 
													AND T1.CAttrCat COLLATE SQL_Latin1_General_CP1_CI_AS = T2.CAttrCat 
													AND T1.CAttrDesc COLLATE SQL_Latin1_General_CP1_CI_AS = T2.CAttrDesc
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.CAttrCat IS NULL) AND (T2.CAttrDesc IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.CAttrCat, T1.CAttrDesc
	
	

 
--CHART CONS SOLICIT CODE
	SELECT	T1.RE_DB_OwnerShort, T1.SolicitCode, T1.Count 
	FROM	CHART.ConsSolicitCode AS T1 
			LEFT JOIN dbo.CHART_ConsSolicitCodes AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RE_DB_OwnerShort 
															AND T1.SolicitCode COLLATE SQL_Latin1_General_CP1_CI_AS= T2.SolicitCode
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.SolicitCode IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.SolicitCode
	
	
--CHART CONS SOLICITOR
	SELECT	T1.RE_DB_OwnerShort, T1.ASRLink, T1.Name, T1.ASRType, T1.Count 
	FROM	CHART.ConsSolicitor AS T1 
			LEFT JOIN dbo.CHART_ConsSolicitor AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RE_DB_OwnerShort 
															AND T1.ASRLink COLLATE SQL_Latin1_General_CP1_CI_AS= T2.ASRLink
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.ASRLink IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.ASRLink
	     
		--SOLICITORS ON PROPOSAL NOT ADDED AS CONSITUENT SOLICITOR (ASSIGNED SOLICITOR RELATIONSHIP)
		--RUN AND MANUALLY ADD NEW VALUES pertaiing to PROPOSAL SOOLICITOR 
		SELECT    CHART.ProposalSolicitor.RE_DB_OwnerShort, CHART.ProposalSolicitor.PRSolImpID, dbo.HC_Constituents_v.Name, NULL AS ASRType, 
							  CHART.ProposalSolicitor.[count], dbo.CHART_ConsSolicitor.ASRLink, dbo.CHART_ConsSolicitor.RE_DB_OwnerShort AS Expr1
		FROM         dbo.HC_Constituents_v RIGHT OUTER JOIN
							  CHART.ProposalSolicitor ON dbo.HC_Constituents_v.RE_DB_OwnerShort = CHART.ProposalSolicitor.RE_DB_OwnerShort AND 
							  dbo.HC_Constituents_v.ImportID = CHART.ProposalSolicitor.PRSolImpID LEFT OUTER JOIN
							  dbo.CHART_ConsSolicitor ON CHART.ProposalSolicitor.RE_DB_OwnerShort = dbo.CHART_ConsSolicitor.RE_DB_OwnerShort AND 
							  CHART.ProposalSolicitor.PRSolImpID = dbo.CHART_ConsSolicitor.ASRLink
		WHERE     (dbo.CHART_ConsSolicitor.ASRLink IS NULL) AND (dbo.CHART_ConsSolicitor.RE_DB_OwnerShort IS NULL)
		ORDER BY CHART.ProposalSolicitor.RE_DB_OwnerShort, CHART.ProposalSolicitor.PRSolImpID




--CHART CONSTITUENCY CODES
	SELECT	T1.RE_DB_OwnerShort, T1.KeyInD, T1.ConsCode, T1.ConsCodeDesc, T1.count 
           
	FROM	CHART.ConstituencyCodes AS T1 
			LEFT JOIN dbo.CHART_ConstituencyCodes AS T2 ON T1.ConsCode COLLATE SQL_Latin1_General_CP1_CI_AS = T2.ConsCode 
														AND T1.KeyInD COLLATE SQL_Latin1_General_CP1_CI_AS = T2.KeyInD 
														AND T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RE_DB_OwnerShort
	WHERE	(T2.RE_DB_OwnerShort IS NULL) AND (T2.KeyInD IS NULL) AND (T2.ConsCode IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.KeyInD, T1.ConsCode
	
	
--CHART EVENT TYPE
	SELECT	T1.RE_DB_OwnerShort, T1.EVType, T1.Count, T2.RE_DB_OwnerShort AS DB, T2.EVType AS ET
	FROM    CHART.EventType AS T1 
			LEFT JOIN dbo.CHART_EventType AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RE_DB_OwnerShort 
												AND T1.EVType COLLATE SQL_Latin1_General_CP1_CI_AS= T2.EVType
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.EVType IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.EVType
	
	
--CHART FUND
	SELECT T1.RE_DB_OwnerShort, T1.FundID, T1.FundDesc, T1.FundType, T1.FundCategory, T1.FundIsRestricted, T1.GiftSum, T1.Gifts, T1.Proposals, 
			T1.ConsActions, T1.Count--, T2.RE_DB_OwnerShort AS DB, T2.FundID AS FU
	FROM	[SUTTER_1P_DATA].CHART.Fund AS T1 
			LEFT JOIN [SUTTER_1P_DATA].dbo.CHART_Fund AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.RE_DB_OwnerShort   
			AND T1.FundID COLLATE SQL_Latin1_General_CP1_CI_AS    = T2.FundID
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.FundID IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.FundID
												  
		  SELECT * FROM [SUTTER_1P_DATA].dbo.chart_fund AS t1
		  LEFT JOIN [SUTTER_1P_DATA].chart.fund AS t2 ON t1.[FundID]=t2.[FundID] AND t1.[RE_DB_OwnerShort]=t2.[RE_DB_OwnerShort]
		  WHERE t2.[FundID] IS null
--CHART GIFT PAYMENT METHOD 
		--- NOT NEEDED. 
	
	
--CHART GIFT ATTRIBUTE
	
	SELECT	T1.RE_DB_OwnerShort, T1.GFAttrCat, T1.GFAttrDesc, T1.DataType, T1.count, T2.RE_DB_OwnerShort AS DB, T2.GFAttrCat AS CT, 
            T2.GFAttrDesc AS DE
	FROM	CHART.GiftAttribute AS T1 
			LEFT JOIN dbo.CHART_GiftAttribute AS T2 ON T1.GFAttrDesc COLLATE SQL_Latin1_General_CP1_CI_AS = T2.GFAttrDesc 
			AND T1.GFAttrCat COLLATE SQL_Latin1_General_CP1_CI_AS= T2.GFAttrCat AND T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS= T2.RE_DB_OwnerShort
	WHERE	(T2.RE_DB_OwnerShort IS NULL) AND (T2.GFAttrCat IS NULL) AND (T2.GFAttrDesc IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.GFAttrCat, T1.GFAttrDesc
	
	
--CHART GIFT CODE
	SELECT	T1.RE_DB_OwnerShort, T1.GFGiftCode, T1.count 
	FROM	CHART.GiftCode AS T1 
			LEFT JOIN dbo.CHART_GiftCode AS T2 ON T1.RE_DB_OwnerShort  COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.RE_DB_OwnerShort 
											AND T1.GFGiftCode  COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.GFGiftCode
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.GFGiftCode IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.GFGiftCode
	
	
--CHART GIFT SUBTYPE
	
	SELECT  T1.RE_DB_OwnerShort, T1.GFSubType, T1.count, T2.RE_DB_OwnerShort AS DB, T2.GFSubType AS GST
	FROM    CHART.GiftSubType AS T1 
			LEFT OUTER JOIN dbo.CHART_GiftSubType AS T2 ON T1.RE_DB_OwnerShort  COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.RE_DB_OwnerShort 
														AND T1.GFSubType  COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.GFSubType
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.GFSubType IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.GFSubType
	
	
--CHART GIFT TYPE CHECK 
	
		SELECT    T1.GFType, SUM(T1.Count) AS Expr1, SUM(T1.Sum) AS Expr2 --, T2.GFType AS gt
		FROM         CHART.GiftType AS T1 
				--	LEFT JOIN dbo.CHART_GiftType AS T2 ON T1.GFType  COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.GFType
		--WHERE     T2.GFType IS NULL OR T2.GFType=''
		GROUP BY T1.GFType--, T2.GFType
		ORDER BY T1.GFType
	 
 --CHART GFVehicle
		SELECT [GFVehicle], COUNT(*) c
		FROM CHART.GFVehicle
		GROUP BY [GFVehicle] 
		ORDER BY [GFVehicle] 


--CHART IRPHONE TYPE
	
	SELECT		T1.RE_DB_OwnerShort, T1.IRPhoneType, T1.count, T2.RE_DB_OwnerShort AS DB, T2.IRPhoneType AS P
	FROM        CHART.IRPhoneType AS T1 
				LEFT JOIN dbo.CHART_IRPhoneType AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.RE_DB_OwnerShort 
														AND T1.IRPhoneType COLLATE SQL_Latin1_General_CP1_CI_AS = T2.IRPhoneType
	WHERE		(T2.RE_DB_OwnerShort IS NULL) AND (T2.IRPhoneType IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.IRPhoneType
	

--CHART MEMBERSHIPS
	UPDATE CHART.MEMBERSHIPS 
	SET MEPROGRAM='NULL' WHERE MEPROGRAM IS NULL OR MEPROGRAM=''
		 
	SELECT T1.* 
	FROM CHART.Memberships AS T1
	LEFT JOIN DBO.[CHART_Memberships] AS T2 ON T1.MECAT =T2.[MECat] AND T1.MEPROGRAM=T2.[MEProgram] AND T1.RE_DB_OWNERSHORT=T2.[RE_DB_OwnerShort]
	WHERE T2.MECAT IS NULL




--CHART ORPHONE TYPE
						
	SELECT	T1.RE_DB_OwnerShort, T1.ORPhoneType, T1.count, T2.RE_DB_OwnerShort AS DB, T2.ORPhoneType AS P
	FROM    CHART.ORPhoneType AS T1 
			LEFT JOIN dbo.CHART_ORPhoneType AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.RE_DB_OwnerShort 
												AND T1.ORPhoneType COLLATE SQL_Latin1_General_CP1_CI_AS = T2.ORPhoneType
	WHERE   (T2.RE_DB_OwnerShort IS NULL) AND (T2.ORPhoneType IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.ORPhoneType

--CHART PACKAGE CATEGORY

	SELECT T1.* FROM CHART.PACKAGECATEGORY	AS T1
	LEFT JOIN DBO.[CHART_PackageCategory] AS T2 ON T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort AND T1.PackCategory=T2.[PackCategory]
	WHERE T2.PackCategory IS NULL 
	
--CHART PHONE TYPE
	SELECT	T1.RE_DB_OwnerShort, T1.KeyInd, T1.PhoneType, T1.count, T2.RE_DB_OwnerShort AS DB, T2.KeyInd AS KI, T2.PhoneType AS PT
	FROM    CHART.PhoneType AS T1 
			LEFT JOIN dbo.CHART_PhoneType AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.RE_DB_OwnerShort 
													AND T1.KeyInd COLLATE SQL_Latin1_General_CP1_CI_AS = T2.KeyInd 
													AND T1.PhoneType COLLATE SQL_Latin1_General_CP1_CI_AS = T2.PhoneType
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.KeyInd IS NULL) AND (T2.PhoneType IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.KeyInd, T1.PhoneType
	
	
--CHART PARTICIPANT TYPE
	SELECT     TOP (100) PERCENT T1.RE_DB_OwnerShort, T1.REGParticipation, T1.count, T2.RE_DB_OwnerShort AS DB, T2.REGParticipation AS RE
	FROM         CHART.ParticipantType AS T1 
				LEFT JOIN dbo.CHART_ParticipantType AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS   = T2.RE_DB_OwnerShort 
														AND T1.REGParticipation COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.REGParticipation
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.REGParticipation IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.REGParticipation
	
	
	--CHART_PROPOSAL ATTRIBUTE
		SELECT T1.* 
		FROM [CHART].[ProposalAttribute]   T1
		LEFT JOIN dbo.[CHART_ProposalAttr] AS T2 ON T1.[RE_DB_OwnerShort]=t2.[RE_DB_OwnerShort] 
												AND t1.[PRAttrCat]=t2.[PRAttrCat]
												AND t1.[PRAttrDesc]=t2.[PRAttrDesc]
	    WHERE t2.[RE_DB_OwnerShort] IS NULL AND t2.[PRAttrCat] IS NULL AND t2.[PRAttrDesc] IS null



	--CHART PROPOSAL STATUS
	SELECT		T1.RE_DB_OwnerShort, T1.PRStatus, T1.Count, T2.RE_DB_OwnerShort AS DB, T2.PRStatus AS R
	FROM        CHART.PRStatus AS T1 
				LEFT JOIN dbo.CHART_PRStatus AS T2 ON T1.PRStatus COLLATE SQL_Latin1_General_CP1_CI_AS   = T2.PRStatus 
													AND T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.RE_DB_OwnerShort
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.PRStatus IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.PRStatus
	
	

	--CHART PROSPECT WILL NOT GIVE 
	SELECT		T1.RE_DB_OwnerShort, T1.[PWNGCode], T1.[COUNT] 
	FROM        CHART.Prospect_WNG AS T1 
				LEFT JOIN dbo.CHART_Prospect_WNG AS T2 ON T1.[PWNGCode] COLLATE SQL_Latin1_General_CP1_CI_AS   = T2.[PWNGCode] 
													AND T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.RE_DB_OwnerShort
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.[PWNGCode] IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.[PWNGCode]
	 
	--CHART PROSPECT philathropic insterests
	SELECT		T1.RE_DB_OwnerShort, T1.[PPICode], T1.[COUNT]--, T2.RE_DB_OwnerShort AS DB, T2.[PPICode] AS R
	FROM        CHART.Prospect_PhilIntCode AS T1 
				LEFT JOIN dbo.CHART_Prospect_PhilIntCode AS T2 ON T1.[PPICode] COLLATE SQL_Latin1_General_CP1_CI_AS   = T2.[PPICode] 
													AND T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS  = T2.RE_DB_OwnerShort
	WHERE     (T2.RE_DB_OwnerShort IS NULL) AND (T2.[PPICode] IS NULL)
	ORDER BY T1.RE_DB_OwnerShort, T1.[PPICode]


--CHART RELAT RECIP TYPE
	
	SELECT	T1.RE_DB_OwnerShort, T1.KeyInd, T1.RelationshipIndicator, T1.Relationship, T1.Reciprocal, T1.CountOf
	FROM	CHART.RelatRecipType AS T1 
			LEFT JOIN dbo.CHART_RelatRecipType AS T2 ON T1.RE_DB_OwnerShort COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RE_DB_OwnerShort
													AND T1.KeyInd COLLATE SQL_Latin1_General_CP1_CI_AS = T2.KeyInd
													AND T1.RelationshipIndicator COLLATE SQL_Latin1_General_CP1_CI_AS = T2.RelationshipIndicator
													AND T1.Relationship COLLATE SQL_Latin1_General_CP1_CI_AS = T2.Relationship 
													AND T1.Reciprocal COLLATE SQL_Latin1_General_CP1_CI_AS = T2.Reciprocal
	WHERE (T2.RE_DB_OwnerShort IS NULL) AND (T2.KeyInd IS NULL) AND (T2.RelationshipIndicator IS NULL) AND (T2.Relationship IS NULL) AND (T2.Reciprocal IS NULL)
	 