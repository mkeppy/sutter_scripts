USE SUTTER_1P_DATA
GO
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[EventType]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.EventType 
	
		--CHARTS.EVENT_TYPE
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			INTO SUTTER_1P_DATA.CHART.EventType 
			FROM RE7_SH_ABSF.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_CVRX.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_DMHF.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_EMCF.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_HOTV.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_MPHF.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_PAMF.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_SAFH.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_SAHF.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_SCAH.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_SDHF.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_SMCF.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_SMFX.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_SRMC.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_SSCF.CHART.EventType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
			SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
			FROM RE7_SH_WBRX.CHART.EventType
			GO
