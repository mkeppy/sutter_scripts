USE SUTTER_MGEA_DATA
GO

			if schema_id('CHART') IS NULL EXECUTE('CREATE SCHEMA CHART') 
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ActionStatus]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[ActionStatus]
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ActionType]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[ActionType]
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[AddressType]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[AddressType]
			IF exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Appeal]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[Appeal]
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Attributes]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[Attributes]			
			IF exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Campaign]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[Campaign]
			IF exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[CampAppPack]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[CampAppPack]
			IF exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConsAttribute]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[ConsAttribute]
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConstituencyCodes]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[ConstituencyCodes]
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[EventType]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[EventType]
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Fund]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[Fund]
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftAttribute]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[GiftAttribute]
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftCode]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[GiftCode]
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftSubType]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[GiftSubType]
			
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ParticipantType]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[ParticipantType]
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[PhoneType]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[PhoneType]
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[SolicitCode]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[SolicitCode]
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftType_combo]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[GiftType_combo]
			if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftType]') and objectproperty(id, N'isTable') = 1)
			drop table  [CHART].[GiftType]

--ACTION SOLICITOR
	SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
	INTO SUTTER_MGEA_DATA.CHART.ActionSolicitor   -- drop table SUTTER_MGEA_DATA.CHART.ActionSolicitor
	FROM RE7_SH_SAFH.CHART.ActionSolicitor
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
	SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
	FROM RE7_SH_SAFH.dbo.HC_Cons_Action_v T1
	LEFT JOIN RE7_SH_SAFH.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	LEFT JOIN RE7_SH_SAFH.DBO.HC_Users_v T3 ON T1.AddedBy=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
	WHERE T2.ACImpID IS NULL 
	GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
	SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
	FROM RE7_SH_SMCF.CHART.ActionSolicitor
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
	SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
	FROM RE7_SH_SMCF.dbo.HC_Cons_Action_v T1
	LEFT JOIN RE7_SH_SMCF.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	LEFT JOIN RE7_SH_SMCF.DBO.HC_Users_v T3 ON T1.AddedBy=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
	WHERE T2.ACImpID IS NULL 
	GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription
	GO

	SELECT * FROM SUTTER_MGEA_DATA.CHART.ActionSolicitor WHERE AddedBy ='' OR AddedBy IS NULL
    UPDATE SUTTER_MGEA_DATA.CHART.ActionSolicitor SET AddedBy = 'NULL' WHERE AddedBy ='' OR AddedBy IS NULL
	SELECT * FROM SUTTER_MGEA_DATA.CHART.ActionSolicitor WHERE ACSolImpID ='' OR ACSolImpID IS NULL
    UPDATE SUTTER_MGEA_DATA.CHART.ActionSolicitor SET ACSolImpID = 'NULL' WHERE ACSolImpID ='' OR ACSolImpID IS NULL
	
	SELECT * FROM SUTTER_MGEA_DATA.DBO.CHART_ActionSolicitor WHERE AddedBy ='' OR AddedBy IS NULL
    UPDATE SUTTER_MGEA_DATA.DBO.CHART_ActionSolicitor SET AddedBy = 'NULL' WHERE AddedBy ='' OR AddedBy IS NULL
	SELECT * FROM SUTTER_MGEA_DATA.DBO.CHART_ActionSolicitor WHERE ACSolImpID ='' OR ACSolImpID IS NULL
	UPDATE SUTTER_MGEA_DATA.DBO.CHART_ActionSolicitor SET ACSolImpID = 'NULL' WHERE ACSolImpID ='' OR ACSolImpID IS NULL


--ACTION STATUS
	SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
	INTO SUTTER_MGEA_DATA.CHART.ActionStatus 
	FROM RE7_SH_SAFH.CHART.ActionStatus
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.ActionStatus (RE_DB_OwnerShort, ACStatus, [count])
	SELECT RE_DB_OwnerShort, CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, [count]
	FROM RE7_SH_SMCF.CHART.ActionStatus
	GO
	
--ACTION TYPE
	SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
	INTO SUTTER_MGEA_DATA.CHART.ActionType 
	FROM RE7_SH_SAFH.CHART.ActionType
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.ActionType (RE_DB_OwnerShort, ACType, [count])
	SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, [count]
	FROM RE7_SH_SMCF.CHART.ActionType
	GO
	
--ADDRESS TYPE
	SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
	INTO SUTTER_MGEA_DATA.CHART.AddressType 
	FROM RE7_SH_SAFH.CHART.AddressType
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
	SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
	FROM RE7_SH_SMCF.CHART.AddressType

--ADDSALTYPE	
	SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
	INTO SUTTER_MGEA_DATA.CHART.AddSalType 
	FROM RE7_SH_SAFH.CHART.AddSalType
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
	SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
	FROM RE7_SH_SMCF.CHART.AddSalType
	GO
	
--CHARTS.AppealCategory
	SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
	INTO SUTTER_MGEA_DATA.CHART.AppealCategory 
	FROM RE7_SH_SAFH.dbo.HC_Appeal_v
	GROUP BY RE_DB_OwnerShort, AppCategory
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.AppealCategory (RE_DB_OwnerShort, AppCategory, [count])
	SELECT RE_DB_OwnerShort, AppCategory, COUNT(*) AS [Count]
	FROM RE7_SH_SMCF.dbo.HC_Appeal_v
	GROUP BY RE_DB_OwnerShort, AppCategory
	GO
	
--CHARTS.ATTRIBUTES
--DO NOT INCLUDE TableEntry CATEGORIES. TABLEENTRY Categories will be mapped in their own charts
	SELECT RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count] 
	INTO SUTTER_MGEA_DATA.CHART.Attributes 
	FROM RE7_SH_SAFH.CHART.Attributes
	WHERE DataType!='TableEntry'
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
	SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
	FROM RE7_SH_SMCF.CHART.Attributes
	WHERE DataType!='TableEntry'
	GO

--CHARTS.Campaign (a.k.a.CampFundAppPack)
	SELECT [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
	INTO SUTTER_MGEA_DATA.[CHART].[Campaign]
	FROM RE7_SH_SAFH.[CHART].[FundCampAppPack]	
	GO
	INSERT INTO SUTTER_MGEA_DATA.[CHART].[Campaign]( [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum])
	SELECT  [RE_DB_OwnerShort], [FundID],[FundDesc],[CampID],[CampDesc],[AppealID],[AppDesc],[PackageID],[PackDesc],[Count],[Sum]
	FROM RE7_SH_SMCF.[CHART].[FundCampAppPack]
	GO

--CHARTS.C0NS_ATTRIBUTES
--INCLUDE ONLY DATATYPE = TABLEENTRY. ALL OTHER DATA TYPES WILL BE MAPPED AS IS IN THEIR OWN CHART. 
	SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
	INTO SUTTER_MGEA_DATA.CHART.ConsAttribute   
	FROM RE7_SH_SAFH.CHART.ConsAttribute
	WHERE DataType='TableEntry'
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
	SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
	FROM RE7_SH_SMCF.CHART.ConsAttribute
	WHERE DataType='TableEntry'
	GO

--CHARTS.SOLICIT CODES 
	SELECT RE_DB_OwnerShort, SolicitCode, [Count]
	INTO SUTTER_MGEA_DATA.CHART.ConsSolicitCode 
	FROM RE7_SH_SAFH.CHART.SolicitCode
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
	SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
	FROM RE7_SH_SMCF.CHART.SolicitCode
	GO	
	
--CHART.CONSSOLICITOR
	SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
	INTO SUTTER_MGEA_DATA.CHART.ConsSolicitor
	FROM RE7_SH_SAFH.dbo.HC_Cons_Solicitor_v T1
	INNER JOIN RE7_SH_SAFH.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID
	GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType 
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
	SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType,  COUNT(*) AS [Count]
	FROM RE7_SH_SMCF.dbo.HC_Cons_Solicitor_v T1
	INNER JOIN RE7_SH_SMCF.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID
	GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
	GO	

--CHARTS.CONSTITUENCY_CODES
	SELECT RE_DB_OwnerShort, KeyInD, 
	CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
	INTO SUTTER_MGEA_DATA.CHART.ConstituencyCodes   
	FROM RE7_SH_SAFH.CHART.ConstituencyCodes
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.ConstituencyCodes (RE_DB_OwnerShort, KeyInD, ConsCode, ConsCodeDesc, [count])
	SELECT RE_DB_OwnerShort, KeyInD, 
	CASE WHEN ConsCode IS NULL THEN 'NULL' ELSE ConsCode END AS ConsCode, ConsCodeDesc, RecordCount AS [count]
	FROM RE7_SH_SMCF.CHART.ConstituencyCodes
	GO

--CHARTS.EVENT_TYPE
	SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
	INTO SUTTER_MGEA_DATA.CHART.EventType 
	FROM RE7_SH_SAFH.CHART.EventType
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.EventType (RE_DB_OwnerShort, EVType, [Count])
	SELECT RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, [Count]
	FROM RE7_SH_SMCF.CHART.EventType
	GO

--CHARTS.FUND
	SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
	INTO SUTTER_MGEA_DATA.CHART.Fund   
	FROM RE7_SH_SAFH.CHART.Fund
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
	SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
	FROM RE7_SH_SMCF.CHART.Fund
	GO

--CHART.GFPayMeth
	SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
	INTO SUTTER_MGEA_DATA.CHART.GFPayMeth_combo		
	FROM RE7_SH_SAFH.CHART.GFPayMethod
	GO 
	INSERT INTO SUTTER_MGEA_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
	SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
	FROM RE7_SH_SMCF.CHART.GFPayMethod
	GO 

--CHARTS.GIFT_ATTRIBUTES
	SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
	INTO SUTTER_MGEA_DATA.CHART.GiftAttribute   
	FROM RE7_SH_SAFH.CHART.GiftAttribute
	WHERE DataType='TableEntry'
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
	SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
	FROM RE7_SH_SMCF.CHART.GiftAttribute
	WHERE DataType='TableEntry'
	GO

--CHARTS.GIFT_CODE
	SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
	INTO SUTTER_MGEA_DATA.CHART.GiftCode 
	FROM RE7_SH_SAFH.CHART.GiftCode
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.GiftCode (RE_DB_OwnerShort, GFGiftCode, [count])
	SELECT RE_DB_OwnerShort, CASE WHEN GFGiftCode IS NULL THEN 'NULL' ELSE GFGiftCode END AS GFGiftCode, [count]
	FROM RE7_SH_SMCF.CHART.GiftCode
	GO	

--CHARTS.GiftSubType
	SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
	INTO SUTTER_MGEA_DATA.CHART.GiftSubType 
	FROM RE7_SH_SAFH.CHART.GiftSubType
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
	SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
	FROM RE7_SH_SMCF.CHART.GiftSubType
	GO

--CHART.GIFT_TYPE
	SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
	INTO SUTTER_MGEA_DATA.CHART.GiftType 		
	FROM RE7_SH_SAFH.CHART.GiftType
	GO 
	INSERT INTO SUTTER_MGEA_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
	SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
	FROM RE7_SH_SMCF.CHART.GiftType
	GO 

--CHARTS.IR_PHONE_TYPE
	SELECT RE_DB_OwnerShort, IRPhoneType, [count]
	INTO SUTTER_MGEA_DATA.CHART.IRPhoneType      
	FROM RE7_SH_SAFH.CHART.IRPhoneType
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
	SELECT RE_DB_OwnerShort, IRPhoneType, [count]
	FROM RE7_SH_SMCF.CHART.IRPhoneType
	GO

--CHARTS.OR_PHONE_TYPE
	SELECT RE_DB_OwnerShort, ORPhoneType, [count]
	INTO SUTTER_MGEA_DATA.CHART.ORPhoneType      
	FROM RE7_SH_SAFH.CHART.ORPhoneType
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
	SELECT RE_DB_OwnerShort, ORPhoneType, [count]
	FROM RE7_SH_SMCF.CHART.ORPhoneType
	GO

--CHARTS.PARTICIPANT_TYPE
	SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
	INTO SUTTER_MGEA_DATA.CHART.ParticipantType 
	FROM RE7_SH_SAFH.CHART.ParticipantType
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.ParticipantType (RE_DB_OwnerShort, REGParticipation, [count])
	SELECT RE_DB_OwnerShort, CASE WHEN REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, [count]
	FROM RE7_SH_SMCF.CHART.ParticipantType
	GO

--CHARTS.IR_PHONE_TYPE
	SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
	INTO SUTTER_MGEA_DATA.CHART.PhoneType      
	FROM RE7_SH_SAFH.CHART.PhoneType
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
	SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
	FROM RE7_SH_SMCF.CHART.PhoneType
	GO	

--CHART.PRStatus
	SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
	INTO SUTTER_MGEA_DATA.CHART.PRStatus
	FROM  RE7_SH_SAFH.CHART.[ProposalStatus]
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
	SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
	FROM  RE7_SH_SMCF.CHART.[ProposalStatus]
	GO
			
--CHART.RelatRecipType. For REFERENCE to convert RELATIONSHIP CATEGORIES.
	SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
	INTO SUTTER_MGEA_DATA.CHART.RelatRecipType
	FROM RE7_SH_SAFH.CHART.RelatRecipType
	GO
	INSERT INTO SUTTER_MGEA_DATA.CHART.RelatRecipType ([RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf])
	SELECT [RE_DB_OwnerShort], [KeyInd], [RelationshipIndicator], [Relationship], [Reciprocal], [CountOf] 
	FROM RE7_SH_SMCF.CHART.RelatRecipType
	GO


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	