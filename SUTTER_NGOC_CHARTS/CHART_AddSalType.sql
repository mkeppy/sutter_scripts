USE SUTTER_1P_DATA
GO

 
  

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[AddSalType]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.AddSalType

		--CHART.AddSalType
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			INTO SUTTER_1P_DATA.CHART.AddSalType 
			FROM RE7_SH_ABSF.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_CVRX.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_DMHF.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_EMCF.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_HOTV.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_MPHF.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_PAMF.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_SAFH.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_SAHF.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_SCAH.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_SDHF.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_SMCF.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_SMFX.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_SRMC.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_SSCF.CHART.AddSalType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddSalType (RE_DB_OwnerShort, AddSalType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddSalType IS NULL THEN 'NULL' ELSE AddSalType END AS AddSalType, [count]
			FROM RE7_SH_WBRX.CHART.AddSalType
			GO
