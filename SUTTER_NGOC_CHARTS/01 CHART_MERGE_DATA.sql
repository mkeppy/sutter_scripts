--GO TO Query--> SQL CMD Mode 
--CVRX and WBRX sql files have an update view for PhoneNumers to account for the RE 7.95 version. 
--RUN SQL VIEWS ON ALL DBS.

SET NOCOUNT ON
GO
		  
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_ActionAttribute.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_ActionSolicitor.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_ActionStatus.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_ActionType.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_AddressType.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_AddSalType.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_AppealCategory.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_Attributes.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_Campaign.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_CAMPAIGN_1P_nosplits.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_CampaignCategory.SQL"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_ConsAttribute.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_ConsSolicitCode.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_ConsSolicitor.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_ConstituencyCodes.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_EventType.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_Fund.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_GFPayMeth.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_GFVehicle.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_GiftAttribute.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_GiftCode.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_GiftSubType.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_GiftType.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_IRPhoneType.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_Memberships.SQL"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_ORPhoneType.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_PackCategory.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_ParticipantType.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_PhoneType.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_ProposalAttribute.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_ProposalSolicitor.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_Prospect_PhilIntCode.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_Prospect_WNG.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_PRStatus.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_CHARTS\CHART_RelatRecipType.sql"
	
	 