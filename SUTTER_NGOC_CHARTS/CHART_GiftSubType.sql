USE SUTTER_1P_DATA
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftSubType]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.GiftSubType
	
		--CHARTS.GiftSubType
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			INTO SUTTER_1P_DATA.CHART.GiftSubType 
			FROM RE7_SH_ABSF.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_CVRX.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_DMHF.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_EMCF.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_HOTV.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_MPHF.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_PAMF.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_SAFH.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_SAHF.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_SCAH.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_SDHF.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_SMCF.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_SMFX.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_SRMC.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_SSCF.CHART.GiftSubType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftSubType (RE_DB_OwnerShort, GFSubType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN GFSubType IS NULL THEN 'NULL' ELSE GFSubType END AS GFSubType, [count]
			FROM RE7_SH_WBRX.CHART.GiftSubType
			GO
