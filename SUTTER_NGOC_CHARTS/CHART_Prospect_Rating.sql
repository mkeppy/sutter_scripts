 
	  
 
USE SUTTER_1P_DATA
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Prospect_Rating]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.Prospect_Rating
	

		--CHARTS.Prospect_Rating
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			INTO SUTTER_1P_DATA.CHART.Prospect_Rating 
			FROM RE7_SH_ABSF.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_CVRX.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_DMHF.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_EMCF.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_HOTV.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_MPHF.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_PAMF.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_SAFH.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_SAHF.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_SCAH.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_SDHF.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_SMCF.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_SMFX.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_SRMC.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_SSCF.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Prospect_Rating (RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, [count])
			SELECT RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc, COUNT(*) AS [Count]
			FROM RE7_SH_WBRX.dbo.HC_Prospect_Rating_v
			GROUP BY RE_DB_OwnerShort, PRateCat, PRateSource, PRateDesc
			GO


			SELECT * FROM CHART.Prospect_Rating
			ORDER BY [RE_DB_OwnerShort], pratecat, PRateSource, PRateDesc
