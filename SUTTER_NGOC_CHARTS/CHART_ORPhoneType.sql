USE SUTTER_1P_DATA
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ORPhoneType]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.ORPhoneType 
	
	--CHARTS.OR_PHONE_TYPE
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			INTO SUTTER_1P_DATA.CHART.ORPhoneType      
			FROM RE7_SH_ABSF.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_CVRX.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_DMHF.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_EMCF.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_HOTV.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_MPHF.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_PAMF.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_SAFH.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_SAHF.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_SCAH.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_SDHF.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_SMCF.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_SMFX.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_SRMC.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_SSCF.CHART.ORPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ORPhoneType (RE_DB_OwnerShort, ORPhoneType, [count])
			SELECT RE_DB_OwnerShort, ORPhoneType, [count]
			FROM RE7_SH_WBRX.CHART.ORPhoneType
			GO
				

