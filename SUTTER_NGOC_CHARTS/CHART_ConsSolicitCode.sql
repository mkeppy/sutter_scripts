USE SUTTER_1P_DATA
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConsSolicitCode]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.ConsSolicitCode
	
		--CHARTS.SOLICIT CODES 
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]
			INTO SUTTER_1P_DATA.CHART.ConsSolicitCode 
			FROM RE7_SH_ABSF.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_CVRX.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_DMHF.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_EMCF.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_HOTV.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_MPHF.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_PAMF.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_SAFH.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_SAHF.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_SCAH.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_SDHF.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_SMCF.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_SMFX.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_SRMC.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_SSCF.CHART.SolicitCode
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitCode (RE_DB_OwnerShort, SolicitCode, [Count])
			SELECT RE_DB_OwnerShort, SolicitCode, [Count]  
			FROM RE7_SH_WBRX.CHART.SolicitCode
			GO
 