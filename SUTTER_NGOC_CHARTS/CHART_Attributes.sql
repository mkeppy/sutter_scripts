
USE SUTTER_1P_DATA
GO

--if exists, then remove.
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Attributes]') and objectproperty(id, N'isTable') = 1)
drop table  [CHART].[Attributes]

		--CHARTS.ATTRIBUTES
		--DO NOT INCLUDE TableEntry CATEGORIES. TABLEENTRY Categories will be mapped in their own charts
			SELECT RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count] 
			INTO SUTTER_1P_DATA.CHART.Attributes 
			FROM RE7_SH_ABSF.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_CVRX.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_DMHF.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_EMCF.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_HOTV.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_MPHF.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_PAMF.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_SAFH.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_SAHF.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_SCAH.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_SDHF.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_SMCF.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_SMFX.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_SRMC.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_SSCF.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Attributes (RE_DB_OwnerShort, type, datatype, category, tablename, required, [unique], active, [count])
			SELECT [RE_DB_OwnerShort], type, datatype, category, tablename, required, [unique], active, [count]  
			FROM RE7_SH_WBRX.CHART.Attributes
			WHERE DataType!='TableEntry'
			GO
