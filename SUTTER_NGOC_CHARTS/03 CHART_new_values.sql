
--Managing new vules.

--1. LIST NEW VALUES from SQL VIEWS. 
		select * from dbo.CHART_new_ActionStatus_v
		select * from dbo.CHART_new_ActionType_v
		select * from dbo.CHART_new_AddressType_v
		select * from dbo.CHART_new_Attributes_v order by RE_DB_OwnerShort, [type], [category]
    	select * from dbo.CHART_new_CampaignAppeal_v order by RE_DB_OwnerShort, CampID, AppealID
		select * from dbo.CHART_new_ConsAttributes_v
		select * from dbo.CHART_new_ConsSolicitCode_v
		select * from dbo.CHART_new_ConstituencyCodes_v
		select * from dbo.CHART_new_EventType_v
		select * from dbo.CHART_new_Fund_v
		select * from dbo.CHART_new_GiftAttribute_v
		select * from dbo.CHART_new_GiftCode_v
		select * from dbo.CHART_new_GiftSubType_v
		select * from dbo.CHART_new_ParticipantType_v
		select * from dbo.CHART_new_PhoneType_v

--2. IMPORT NEW VALUES FROM SPREADSHEETS AFTER MAPPING
--3. CHECK VALUES IMPORTED
		select * from CHART.new_ActionStatus
		select * from CHART.new_ActionType
		select * from CHART.new_AddressType
		select * from CHART.new_Attributes
    	select * from CHART.new_CampaignAppeal
		select * from CHART.new_ConsAttributes
		select * from CHART.new_ConsSolicitCode
		select * from CHART.new_ConstituencyCodes
		select * from CHART.new_EventType
		select * from CHART.new_Fund
		select * from CHART.new_GiftAttribute
		select * from CHART.new_GiftCode
		select * from CHART.new_GiftSubType
		select * from CHART.new_ParticipantType
		select * from CHART.new_PhoneType


--4. TEST/REVIEW CLEANUP IF NEEDED... THEN PROCEED TO APPEND TO CHARTMs.
	select * from chart.new_ParticipantType 
	--delete chart.new_ParticipantType where RE_DB_OwnerShort='PAMF'
	--delete chart.new_ParticipantType where RE_DB_OwnerShort='MPHF' and REGParticipation !='Jr. Ballroom'
		select * from CHARTM.Test_ParticipantType where RE_DB_OwnerShort='MPHF' order by RE_DB_OwnerShort, REGParticipation
		select * from CHARTM.Test_ParticipantType where RE_DB_OwnerShort='PAMF' order by RE_DB_OwnerShort, REGParticipation
		select * from CHARTM.Test_ParticipantType where RE_DB_OwnerShort='SRMC' order by RE_DB_OwnerShort, REGParticipation


		select * from dbo.CHART_new_PhoneType_v order by RE_DB_OwnerSHort, phonetype
		select * from CHART.new_PhoneType order by RE_DB_OwnerSHort, phonetype
		
		
		select * from CHART.new_Attributes
		--delete CHART.new_Attributes where [type]='CONSTITUENT' and datatype='TableEntry'
		--delete CHART.new_Attributes where [type]='GIFT' and datatype='TableEntry'
		
				
		select * from chartm.Test_GiftAttribute 
		where RE_DB_OwnerShort='SRMC'  
		order by RE_DB_OwnerShort, GFAttrCat, GFAttrDesc 
		
		select * from CHART.new_GiftAttribute order by RE_DB_OwnerShort, GFAttrCat, GFAttrDesc
		--  delete CHART.new_GiftAttribute where RE_DB_OwnerShort='ABSF'
		--  delete CHART.new_GiftAttribute where RE_DB_OwnerShort='MPHF' and GFattrCat='Donor Designation-Fund'
		 -- delete CHART.new_GiftAttribute where RE_DB_OwnerShort='PAMF'
		 -- delete CHART.new_GiftAttribute where RE_DB_OwnerShort='SMCF' and GFAttrDesc='NULL'
		-- delete CHART.new_GiftAttribute where RE_DB_OwnerShort='SRMC' and GFAttrDesc='NULL'
		select * from dbo.CHART_new_GiftAttribute_v
		
		select * from chart.GiftAttribute where RE_DB_OwnerShort='SRMC'  
		order by RE_DB_OwnerShort, GFAttrCat, GFAttrDesc 
		
		
		select * from dbo.CHART_new_CampaignAppeal_v order by RE_DB_OwnerShort, CampID, AppealID
		select * from CHART.new_CampaignAppeal order by RE_DB_OwnerShort, CampID, AppealID
		
		
		select * from CHARTM.Test_CampaignAppeal 
		where AppealID is null
		order by RE_DB_OwnerShort, CampID, AppealID 	
		
		update CHARTM.Test_CampaignAppeal 
		set AppealID = '207000' 
		where RE_DB_OwnerShort = 'EMCF' and AppealID is null And AppDesc='UNITED WAY (207000)'
		
		update CHARTM.Test_CampaignAppeal 
		set AppealID = '305004' 
		where RE_DB_OwnerShort = 'EMCF' and AppealID is null And AppDesc='Grateful patient'
		
		update CHARTM.Test_CampaignAppeal 
		set AppealID = '30' 
		where RE_DB_OwnerShort = 'MPHF' and AppealID is null And AppDesc='2013 October Direct Mail Follow-up to September Appeal'
		
		update CHARTM.Test_CampaignAppeal 
		set AppealID = '35' 
		where RE_DB_OwnerShort = 'MPHF' and AppealID is null And AppDesc='2013 May Direct Mail Appeal'
		
		update CHARTM.Test_CampaignAppeal 
		set AppealID = '37' 
		where RE_DB_OwnerShort = 'MPHF' and AppealID is null And AppDesc='2013 July Direct Mail Appeal'
		
		update CHARTM.Test_CampaignAppeal 
		set AppealID = '39' 
		where RE_DB_OwnerShort = 'MPHF' and AppealID is null And AppDesc='2013 September Direct Mail Appeal'

		update CHARTM.Test_CampaignAppeal 
		set AppealID = '1303' 
		where RE_DB_OwnerShort = 'PAMF' and AppealID is null And AppDesc like '2013 March Guardian Angel/Doctor%'
		
		update CHARTM.Test_CampaignAppeal 
		set AppealID = '1304' 
		where RE_DB_OwnerShort = 'PAMF' and AppealID is null And AppDesc='April 2013 Spring Renewal - Priorities Brochure'
		
		update CHARTM.Test_CampaignAppeal 
		set AppealID = '1305' 
		where RE_DB_OwnerShort = 'PAMF' and AppealID is null And AppDesc='Spring Acquisition - Brachytherapy and Healthy Breast'
		
		update CHARTM.Test_CampaignAppeal 
		set AppealID = '1309' 
		where RE_DB_OwnerShort = 'PAMF' and AppealID is null And AppDesc='Fall Acquisition and Renewal'
		
		update CHARTM.Test_CampaignAppeal 
		set AppealID = '1310' 
		where RE_DB_OwnerShort = 'PAMF' and AppealID is null And AppDesc='Fall Reminder'
	 
		
		
--5. APPEND NEW VALUES TO CHARTMs. 
		INSERT INTO CHARTM.Test_ActionStatus (DB_TYPE, RE_DB_OwnerShort, ACStatus, [COUNT], [CONVERT], NewACStatus)
		SELECT 'Source' AS [DB_TYPE], RE_DB_OwnerShort, ACStatus, [COUNT], [CONVERT], NewACStatus 
		FROM CHART.new_ActionStatus
		GO

		INSERT INTO CHARTM.Test_ActionType (DB_TYPE, RE_DB_OwnerShort, ACType, [COUNT], [CONVERT], NewACType, NewACAttrCat, NewACAttrDesc)
		SELECT 'Source' AS [DB_TYPE], RE_DB_OwnerShort, ACType, [COUNT], [CONVERT], NewACType, NewACAttrCat, NewACAttrDesc 
		FROM CHART.new_ActionType
		GO

		INSERT INTO CHARTM.Test_AddressType (DB_TYPE, RE_DB_OwnerShort, AddrType, [COUNT], [CONVERT], NewAddressType, AddrSendMail, NewCADAttrCat, NewCADAttrDesc)
		SELECT 'Source' AS [DB_TYPE], RE_DB_OwnerShort, AddrType, [COUNT], [CONVERT], NewAddressType, AddrSendMail, NewCADAttrCat, NewCADAttrDesc 
		FROM CHART.new_AddressType
		GO

		INSERT INTO CHARTM.Test_Attributes (DB_TYPE, RE_DB_OwnerShort, [Type], [datatype], [category], tablename, [required], [unique], [active], [COUNT], [CONVERT], NewCATCat, NewCATDesc, NewCATDate, NewCAttrComm, AddSalType, AliasType, Alias, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes)
		SELECT 'Source' AS [DB_TYPE], RE_DB_OwnerShort, [Type], [datatype], [category], tablename, [required], [unique], [active], [COUNT], [CONVERT], NewCATCat, NewCATDesc, NewCATDate, NewCAttrComm, AddSalType, AliasType, Alias, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes
		FROM CHART.new_Attributes
		GO

		INSERT INTO CHARTM.Test_CampaignAppeal (DB_TYPE, RE_DB_OwnerShort, [CampID], [CampDesc], [AppealID], AppDesc, [Count], [Sum], NewCampaignID, NewCampaignDesc, NewAppealID, NewAppealDesc, NewPackageID, NewPackageDesc, NewGftAttrCat1, NewGftAttrDesc1, NewGftAttrCat2, NewGftAttrDesc2, NewCampAttrCat1, NewCampAttrDesc1, NewCampAttrCat2, NewCampAttrDesc2, NewAppealAttrCat1, NewAppealAttrDesc1, NewAppealAttrCat2, NewAppealAttrDesc2, Notes )
		SELECT 'Source' AS DB_TYPE, RE_DB_OwnerShort, [CampID], [CampDesc], [AppealID], AppDesc, [Count], [Sum], NewCampaignID, NewCampaignDesc, NewAppealID, NewAppealDesc, NewPackageID, NewPackageDesc, NewGftAttrCat1, NewGftAttrDesc1, NewGftAttrCat2, NewGftAttrDesc2, NewCampAttrCat1, NewCampAttrDesc1, NewCampAttrCat2, NewCampAttrDesc2, NewAppealAttrCat1, NewAppealAttrDesc1, NewAppealAttrCat2, NewAppealAttrDesc2, Notes 
		FROM CHART.new_CampaignAppeal
		GO
		
		INSERT INTO CHARTM.Test_ConsAttributes (DB_TYPE, RE_DB_OwnerShort, [CAttrCat], [CAttrDesc], [DataType], [Count], [Convert], NewCATCat, NewCATDesc, [NewCATCom], PPICode, NewCAttrCat1, NewCAttrDesc1, NewConsCode, ConsNoteType, ConsNoteNotes, AliasType, EventID, EventName, EventParticipationRecord, Registration, Invite, ParticipantHasAttended, Participation, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes)
		SELECT 'Source' AS DB_TYPE, RE_DB_OwnerShort, [CAttrCat], [CAttrDesc], [DataType], [Count], [Convert], NewCATCat, NewCATDesc, [NewCATCom], PPICode, NewCAttrCat1, NewCAttrDesc1, NewConsCode, ConsNoteType, ConsNoteNotes, AliasType, EventID, EventName, EventParticipationRecord, Registration, Invite, ParticipantHasAttended, Participation, FinancialInformationType, FinancialInfoSource, FinancialInfoDateAssessed, FinancialInfoNotes, FinancialInfoAmount, RatingSource, RatingDate, RatingCategory, RatingCategoryDataType, RatingDescription, RatingNotes, Notes 
		FROM CHART.new_ConsAttributes
		GO
		
		INSERT INTO CHARTM.Test_ConsSolicitCodes (DB_TYPE, RE_DB_OwnerShort, [SolicitCode], [Count], [Convert], CATCat, CATDesc, CATComment, NoValidAddr, Deceased, CATCat2, CATDesc2, CATComment2, Notes)
		SELECT 'Source' AS DB_TYPE, RE_DB_OwnerShort, [SolicitCode], [Count], [Convert], CATCat, CATDesc, CATComment, NoValidAddr, Deceased, CATCat2, CATDesc2, CATComment2, Notes
		FROM CHART.new_ConsSolicitCode
		GO
		
		INSERT INTO CHARTM.Test_ConstituencyCodes (DB_TYPE, RE_DB_OwnerShort, [ConsCodeShort], [ConsCodeDesc], [RecordCount], [Convert], NewCons, NewCAttrCat, NewCAttrDesc, NewCAttrCom, NewSolCode, Deceased, NewCAttrCat1, NewCAttrDesc1, Notes)
		SELECT 'Source' AS DB_TYPE, RE_DB_OwnerShort, [ConsCodeShort], [ConsCodeDesc], [RecordCount], [Convert], NewCons, NewCAttrCat, NewCAttrDesc, NewCAttrCom, NewSolCode, Deceased, NewCAttrCat1, NewCAttrDesc1, Notes
		FROM CHART.new_ConstituencyCodes
		GO
		
		INSERT INTO CHARTM.Test_EventType (DB_TYPE, RE_DB_OwnerShort, [EVType], [Count], [Convert], NewEventType, Notes)
		SELECT 'Source' AS DB_TYPE, RE_DB_OwnerShort, [EVType], [Count], [Convert], NewEventType, Notes
		FROM CHART.new_EventType
		GO
		
		INSERT INTO CHARTM.Test_Fund (DB_TYPE, RE_DB_OwnerShort, [FundID], FundDesc, FundType, FundCategory, FundIsRestricted, [Count], NewFundID, NewFundDesc, NewFundType, NewFundCategory, NewFundIsRestricted, NewFundAttrCat1, NewFundAttrDesc1, NewFundAttrCat2, NewFundAttrDesc2, NewFundAttrCat3, NewFundAttrDesc3, NewFundAttrCat4, NewFundAttrDesc4 )
		SELECT 'Source' AS DB_TYPE, RE_DB_OwnerShort, [FundID], FundDesc, FundType, FundCategory, FundIsRestricted, [Count], NewFundID, NewFundDesc, NewFundType, NewFundCategory, NewFundIsRestricted, NewFundAttrCat1, NewFundAttrDesc1, NewFundAttrCat2, NewFundAttrDesc2, NewFundAttrCat3, NewFundAttrDesc3, NewFundAttrCat4, NewFundAttrDesc4 
		FROM CHART.new_Fund
		GO

		INSERT INTO CHARTM.Test_GiftAttribute (DB_TYPE, RE_DB_OwnerShort, [GFAttrCat], [GFAttrDesc], [DataType], [Count], [Convert], NewGATCategory, NewGATDesc, NewGATCom, NewGiftSubType)
		SELECT 'Source' AS DB_TYPE, RE_DB_OwnerShort, [GFAttrCat], [GFAttrDesc], [DataType], [Count], [Convert], NewGATCategory, NewGATDesc, NewGATCom, NewGiftSubType
		FROM CHART.new_GiftAttribute
		GO
	
		INSERT INTO CHARTM.Test_GiftCode (DB_TYPE, RE_DB_OwnerShort, [GFGiftCode], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc )
		SELECT 'Source' AS DB_TYPE, RE_DB_OwnerShort, [GFGiftCode], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc 
		FROM CHART.new_GiftCode
		GO
	
		INSERT INTO CHARTM.Test_GiftSubType (DB_TYPE, RE_DB_OwnerShort, [GFSubType], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc)
		SELECT 'Source' AS DB_TYPE, RE_DB_OwnerShort, [GFSubType], [Count], [Convert], NewGiftSubType, NewGiftAttribCategory, NewGiftAttribDesc
		FROM CHART.new_GiftSubType
		GO
	
		INSERT INTO CHARTM.Test_ParticipantType (DB_TYPE, RE_DB_OwnerShort, [REGParticipation], [Count], [Convert], NewREGParticipation, REGAttended, REGAttrCat, REGAttrDesc, Notes)
		SELECT 'Source' AS DB_TYPE, RE_DB_OwnerShort, [REGParticipation], [Count], [Convert], NewREGParticipation, REGAttended, REGAttrCat, REGAttrDesc, Notes
		FROM CHART.new_ParticipantType
		GO
	
		INSERT INTO CHARTM.Test_PhoneType (DB_TYPE, RE_DB_OwnerShort, [PhoneType], [Count], [Convert], NewPhoneType )
		SELECT 'Source' AS DB_TYPE, RE_DB_OwnerShort, [PhoneType], [Count], [Convert], NewPhoneType 
		FROM CHART.new_PhoneType
		GO
	