USE SUTTER_1P_DATA
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[PhoneType]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.PhoneType
	
	--CHARTS.IR_PHONE_TYPE
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			INTO SUTTER_1P_DATA.CHART.PhoneType      
			FROM RE7_SH_ABSF.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_CVRX.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_DMHF.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_EMCF.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_HOTV.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_MPHF.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_PAMF.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_SAFH.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_SAHF.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_SCAH.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_SDHF.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_SMCF.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_SMFX.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_SRMC.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_SSCF.CHART.PhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PhoneType (RE_DB_OwnerShort, KeyInd, PhoneType, [count])
			SELECT RE_DB_OwnerShort, KeyInd, PhoneType, [count]
			FROM RE7_SH_WBRX.CHART.PhoneType
			GO
				
				 
