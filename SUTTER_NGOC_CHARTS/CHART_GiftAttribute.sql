USE SUTTER_1P_DATA
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftAttribute]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.GiftAttribute
	
--CHARTS.GIFT_ATTRIBUTES
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			INTO SUTTER_1P_DATA.CHART.GiftAttribute   
			FROM RE7_SH_ABSF.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_CVRX.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_DMHF.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_EMCF.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_HOTV.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_MPHF.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_PAMF.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_SAFH.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_SAHF.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_SCAH.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_SDHF.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_SMCF.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_SMFX.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_SRMC.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_SSCF.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.GiftAttribute (RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType, [count]
			FROM RE7_SH_WBRX.CHART.GiftAttribute
			WHERE DataType='TableEntry'
			GO
		