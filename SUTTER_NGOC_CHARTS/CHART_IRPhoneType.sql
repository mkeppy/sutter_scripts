USE SUTTER_1P_DATA
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[IRPhoneType]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.IRPhoneType
	
	--CHARTS.IR_PHONE_TYPE
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			INTO SUTTER_1P_DATA.CHART.IRPhoneType      
			FROM RE7_SH_ABSF.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_CVRX.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_DMHF.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_EMCF.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_HOTV.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_MPHF.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_PAMF.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_SAFH.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_SAHF.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_SCAH.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_SDHF.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_SMCF.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_SMFX.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_SRMC.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_SSCF.CHART.IRPhoneType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.IRPhoneType (RE_DB_OwnerShort, IRPhoneType, [count])
			SELECT RE_DB_OwnerShort, IRPhoneType, [count]
			FROM RE7_SH_WBRX.CHART.IRPhoneType
			GO
				
