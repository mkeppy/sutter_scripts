USE SUTTER_1P_DATA
GO
 

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConsSolicitor]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.ConsSolicitor


					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					INTO SUTTER_1P_DATA.CHART.ConsSolicitor
					FROM [SUTTER_1P_DATA].dbo.HC_Cons_Solicitor_v T1
					INNER JOIN [SUTTER_1P_DATA].dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID	AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType 
					GO

				 
			  /*
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					INTO SUTTER_1P_DATA.CHART.ConsSolicitor
					FROM RE7_SH_ABSF.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_ABSF.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID  AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType 
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType,  COUNT(*) AS [Count]
					FROM RE7_SH_CVRX.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_CVRX.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					FROM RE7_SH_DMHF.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_DMHF.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID	AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					INNER JOIN RE7_SH_EMCF.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID  AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					FROM RE7_SH_HOTV.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_HOTV.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					FROM RE7_SH_MPHF.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_MPHF.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID  AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					FROM RE7_SH_PAMF.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_PAMF.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID	 AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					FROM RE7_SH_SAFH.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_SAFH.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID	AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					FROM RE7_SH_SAHF.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_SAHF.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID  AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					FROM RE7_SH_SCAH.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_SCAH.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID  AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					FROM RE7_SH_SDHF.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_SDHF.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID  AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					FROM RE7_SH_SMCF.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_SMCF.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID  AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					FROM RE7_SH_SMFX.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_SMFX.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID  AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					FROM RE7_SH_SRMC.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_SRMC.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID  AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					FROM RE7_SH_SSCF.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_SSCF.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID  AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ConsSolicitor (RE_DB_OwnerShort, ASRLink, Name, ASRType, [count])
					SELECT T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType, COUNT(*) AS [Count]
					FROM RE7_SH_WBRX.dbo.HC_Cons_Solicitor_v T1
					INNER JOIN RE7_SH_WBRX.dbo.HC_Constituents_v T2 ON T1.ASRLink=T2.ImportID	AND T1.RE_DB_OWNERSHORT=T2.RE_DB_OWNERSHORT
					GROUP by T1.RE_DB_OwnerShort, T1.ASRLink, T2.Name, T1.ASRType
					GO

					   */