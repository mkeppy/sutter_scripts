USE SUTTER_1P_DATA
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[AddressType]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.AddressType

		--CHART.ADDRESS TYPE
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			INTO SUTTER_1P_DATA.CHART.AddressType 
			FROM RE7_SH_ABSF.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_CVRX.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_DMHF.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_EMCF.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_HOTV.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_MPHF.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_PAMF.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_SAFH.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_SAHF.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_SCAH.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_SDHF.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_SMCF.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_SMFX.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_SRMC.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_SSCF.CHART.AddressType
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.AddressType (RE_DB_OwnerShort, AddrType, [count])
			SELECT RE_DB_OwnerShort, CASE WHEN AddrType IS NULL THEN 'NULL' ELSE AddrType END AS AddrType, [count]
			FROM RE7_SH_WBRX.CHART.AddressType
			GO
