USE SUTTER_1P_DATA
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftType]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.GiftType
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftType_combo]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.GiftType_combo
	
	--CHART.GIFT_TYPE
		 
			
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			INTO SUTTER_1P_DATA.CHART.GiftType 		
			FROM RE7_SH_ABSF.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_CVRX.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_DMHF.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_EMCF.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_HOTV.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_MPHF.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_PAMF.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_SAFH.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_SAHF.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_SCAH.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_SDHF.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_SMCF.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_SMFX.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_SRMC.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_SSCF.CHART.GiftType
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GiftType (RE_DB_OwnerShort, GFType, [Count], [Sum])
			SELECT RE_DB_OwnerShort, GFType, [Count], [Sum]
			FROM RE7_SH_WBRX.CHART.GiftType
			GO 

			
		 

			
			SELECT GFType, SUM([Count]) AS [Count], SUM([Sum]) AS [Sum]
			INTO SUTTER_1P_DATA.CHART.GiftType_combo
			FROM SUTTER_1P_DATA.CHART.GiftType
			GROUP BY GFType
			ORDER BY GFType


			-- SELECT * FROM SUTTER_1P_DATA.CHART.GiftType_combo ORDER BY GFType