USE SUTTER_1P_DATA
GO
  													 
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ProposalSolicitor]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.ProposalSolicitor




					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					INTO SUTTER_1P_DATA.CHART.ProposalSolicitor
					FROM RE7_SH_ABSF.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID 
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID,  COUNT(*) AS [Count]
					FROM RE7_SH_CVRX.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort,T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_DMHF.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_EMCF.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_HOTV.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_MPHF.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_PAMF.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_SAFH.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_SAHF.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_SCAH.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_SDHF.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_SMCF.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_SMFX.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_SRMC.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_SSCF.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO

					INSERT INTO SUTTER_1P_DATA.CHART.ProposalSolicitor (RE_DB_OwnerShort, PRSolImpID, [count])
					SELECT T1.RE_DB_OwnerShort, T1.PRSolImpID, COUNT(*) AS [Count]
					FROM RE7_SH_WBRX.dbo.HC_Proposal_Solicitor_v T1
 					GROUP by T1.RE_DB_OwnerShort, T1.PRSolImpID
					GO