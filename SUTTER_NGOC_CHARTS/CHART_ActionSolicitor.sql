 

USE SUTTER_1P_DATA
GO

 
--COLLATE SQL_Latin1_General_CP1_CI_AS 
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ActionSolicitor]') and objectproperty(id, N'IsTable') = 1)
	 
BEGIN
	
		--CHARTS.ACTION SOLICITOR 
			--RE7_SH_ABSF
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			INTO SUTTER_1P_DATA.CHART.ActionSolicitor 
			FROM RE7_SH_ABSF.CHART.ActionSolicitor
			 
			 
				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_ABSF.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_ABSF.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_ABSF.DBO.HC_Users_v T3 ON T1.AddedBy=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription

			--RE7_SH_CVRX
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_CVRX.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_CVRX.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_CVRX.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_CVRX.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription

			--RE7_SH_DMHF
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_DMHF.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_DMHF.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_DMHF.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_DMHF.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription

			--RE7_SH_EMCF
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_EMCF.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_EMCF.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_EMCF.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_EMCF.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription

			--RE7_SH_HOTV
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_HOTV.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_HOTV.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_HOTV.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_HOTV.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription

			--RE7_SH_MPHF
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_MPHF.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_MPHF.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_MPHF.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_MPHF.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription

			--RE7_SH_PAMF 
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_PAMF.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_PAMF.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_PAMF.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_PAMF.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription
			 
			--RE7_SH_SAFH
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_SAFH.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_SAFH.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_SAFH.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_SAFH.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription
	
			--RE7_SH_SAHF
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_SAHF.CHART.ActionSolicitor


				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_SAHF.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_SAHF.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_SAHF.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription
	
			--RE7_SH_SCAH
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_SCAH.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_SCAH.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_SCAH.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_SCAH.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription

			--RE7_SH_SDHF
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_SDHF.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_SDHF.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_SDHF.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_SDHF.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription

			--RE7_SH_SMCF
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_SMCF.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_SMCF.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_SMCF.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_SMCF.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription

			--RE7_SH_SMFX
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_SMFX.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_SMFX.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_SMFX.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_SMFX.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription

			--RE7_SH_SRMC
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_SRMC.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_SRMC.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_SRMC.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_SRMC.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription

			--RE7_SH_SSCF
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_SSCF.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_SSCF.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_SSCF.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_SSCF.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription

			--RE7_SH_WBRX
			INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
			SELECT RE_DB_OwnerShort, CAST('' AS NVARCHAR(60)) AS AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count]
			FROM RE7_SH_WBRX.CHART.ActionSolicitor

				INSERT INTO SUTTER_1P_DATA.CHART.ActionSolicitor (RE_DB_OwnerShort, AddedBy, ACSolImpID, ConsID, Name, IsSolicitor, [Count])
				SELECT T1.RE_DB_OwnerShort, T1.AddedBy, NULL AS ACSolImpID, NULL AS ConsID, T3.UserDescription AS Name, 'False' AS IsSolicitor, COUNT(*) [count]
				FROM RE7_SH_WBRX.dbo.HC_Cons_Action_v T1
				LEFT JOIN RE7_SH_WBRX.dbo.HC_Cons_Action_Solicitor_v T2 ON T1.ACImpID=T2.ACImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN RE7_SH_WBRX.DBO.HC_Users_v T3 ON T1.AddedBy COLLATE SQL_Latin1_General_CP1_CI_AS=T3.UserName AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				WHERE T2.ACImpID IS NULL 
				GROUP BY T1.RE_DB_OwnerShort, T1.AddedBy, T3.UserDescription

END 