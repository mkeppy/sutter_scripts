USE SUTTER_1P_DATA
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GFPayMeth]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.GFPayMeth
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GFPayMeth_combo]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.GFPayMeth_combo 
	
	
		--CHART.GFPayMeth
		
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo		
			FROM RE7_SH_ABSF.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_CVRX.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_DMHF.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_EMCF.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_HOTV.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_MPHF.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_PAMF.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_SAFH.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_SAHF.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_SCAH.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_SDHF.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_SMCF.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_SMFX.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_SRMC.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_SSCF.CHART.GFPayMethod
			GO 
			INSERT INTO SUTTER_1P_DATA.CHART.GFPayMeth_combo (RE_DB_OwnerShort, GFPayMeth, [Count])
			SELECT RE_DB_OwnerShort, GFPayMeth, [Count]
			FROM RE7_SH_WBRX.CHART.GFPayMethod
			GO 
			 

			SELECT GFPayMeth, SUM([Count]) AS [Count]
			INTO SUTTER_1P_DATA.CHART.GFPayMeth
			FROM SUTTER_1P_DATA.CHART.GFPayMeth_combo 
			GROUP BY GFPayMeth
			ORDER BY GFPayMeth	

				--SELECT * FROM SUTTER_1P_DATA.CHART.GFPayMeth_combo  ORDER BY GFPayMeth
				--SELECT * FROM SUTTER_1P_DATA.CHART.GFPayMeth  ORDER BY GFPayMeth
