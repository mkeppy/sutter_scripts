USE SUTTER_1P_DATA
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConsAttribute]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.ConsAttribute
	
		--CHARTS.C0NS_ATTRIBUTES
		--INCLUDE ONLY DATATYPE = TABLEENTRY. ALL OTHER DATA TYPES WILL BE MAPPED AS IS IN THEIR OWN CHART. 
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			INTO SUTTER_1P_DATA.CHART.ConsAttribute   
			FROM RE7_SH_ABSF.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_CVRX.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_DMHF.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_EMCF.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_HOTV.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_MPHF.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_PAMF.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_SAFH.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_SAHF.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_SCAH.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_SDHF.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_SMCF.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_SMFX.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_SRMC.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_SSCF.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.ConsAttribute (RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType, [count])
			SELECT RE_DB_OwnerShort, CAttrCat, CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, [count]
			FROM RE7_SH_WBRX.CHART.ConsAttribute
			WHERE DataType='TableEntry'
			GO
