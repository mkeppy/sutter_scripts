 
USE SUTTER_1P_DATA
GO
	 
if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[PackageCategory]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.PackageCategory
	

		--CHARTS.PackageCategory
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			INTO SUTTER_1P_DATA.CHART.PackageCategory 
			FROM RE7_SH_ABSF.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_CVRX.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_DMHF.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_EMCF.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_HOTV.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_MPHF.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_PAMF.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SAFH.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SAHF.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SCAH.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SDHF.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SMCF.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SMFX.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SRMC.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_SSCF.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PackageCategory (RE_DB_OwnerShort, PackCategory, [count])
			SELECT RE_DB_OwnerShort, PackCategory, COUNT(*) AS [Count]
			FROM RE7_SH_WBRX.dbo.HC_Package_v
			GROUP BY RE_DB_OwnerShort, PackCategory
			GO
