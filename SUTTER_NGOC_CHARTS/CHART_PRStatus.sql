USE SUTTER_1P_DATA
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[PRStatus]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.PRStatus

--CHART.PRStatus
		
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			INTO SUTTER_1P_DATA.CHART.PRStatus
			FROM  RE7_SH_ABSF.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_CVRX.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_DMHF.CHART.[ProposalStatus]
			GO			
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_EMCF.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_HOTV.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_MPHF.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_PAMF.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_SAFH.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_SAHF.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_SCAH.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_SDHF.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_SMCF.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_SMFX.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_SRMC.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_SSCF.CHART.[ProposalStatus]
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.PRStatus([RE_DB_OwnerShort], [PRStatus], [Count])
			SELECT [RE_DB_OwnerShort], [PRStatus], [Count]
			FROM  RE7_SH_WBRX.CHART.[ProposalStatus]
			GO
