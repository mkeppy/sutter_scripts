USE [SUTTER_1P_DATA]
GO
						  
 	  

if exists (select * FROM dbo.sysobjects where id = object_id(N'[CHART].[Memberships]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.Memberships
	
	--CHARTS.Memberships
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			INTO SUTTER_1P_DATA.CHART.Memberships      
			FROM RE7_SH_ABSF.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_CVRX.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_DMHF.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_EMCF.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_HOTV.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_MPHF.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_PAMF.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_SAFH.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_SAHF.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_SCAH.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_SDHF.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_SMCF.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_SMFX.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_SRMC.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_SSCF.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Memberships (RE_DB_OwnerShort, MECat, MEProgram, [count])
			SELECT RE_DB_OwnerShort, MECat, MEProgram, COUNT(*) AS [count]
			FROM RE7_SH_WBRX.[dbo].[HC_Membership_v]
			GROUP BY RE_DB_OwnerShort, MECat, MEProgram 
			GO
				
				 
