USE SUTTER_1P_DATA
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Fund]') and objectproperty(id, N'IsTable') = 1)
	drop table CHART.Fund 
	
		--CHARTS.FUND
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			INTO SUTTER_1P_DATA.CHART.Fund   
			FROM RE7_SH_ABSF.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_CVRX.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_DMHF.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_EMCF.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_HOTV.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_MPHF.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_PAMF.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_SAFH.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_SAHF.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_SCAH.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_SDHF.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_SMCF.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_SMFX.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_SRMC.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_SSCF.CHART.Fund
			GO
			INSERT INTO SUTTER_1P_DATA.CHART.Fund (RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count])
			SELECT RE_DB_OwnerShort, FundID, FundDesc, FundType, FundCategory, FundIsRestricted, GiftSum, Gifts, Proposals, ConsActions, [Count]
			FROM RE7_SH_WBRX.CHART.Fund
			GO
