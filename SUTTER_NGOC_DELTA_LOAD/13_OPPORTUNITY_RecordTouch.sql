
USE SUTTER_1P_DELTA
GO


BEGIN
		DROP TABLE SUTTER_1P_DELTA.IMP.OPPORTUNITY_PARENT_UPDATE
 
END



BEGIN--OPEN TRANSACTION GENERATION (AKA RECORD TOUCH)


				--standard api option OPEN Parent
				SELECT	'' AS ID, 
						T.EXTERNAL_ID__C,
						'' as rC_Giving__Rollup_Transactions__c, '' as rC_Giving__Update_Transactions__c, 'TRUE' RTP_Updated__c, 
						T.StageName AS zrefStageName, 
 						zrefGFType, 
						[T].[CloseDate] AS zrefCloseDate, 
						[T].[rC_Giving__First_Payment_Date__c] AS zrefFirstPaymentDate,
						[T].[rC_Giving__Payment_End_Date__c] AS zrefPaymentEndDate, 
						[T].[rC_Giving__Giving_Years__c] AS zrefYears,
						[T].[rC_Giving__Expected_Giving_Amount__c] AS zrefExpGiveAmt,
						T.RE_GSPLITIMPID__C AS zrefrefGFSplitImpID, 
						T.RE_GFIMPID__C AS zrefrefGFImpID
						
				INTO SUTTER_1P_DELTA.IMP.OPPORTUNITY_PARENT_UPDATE
				FROM SUTTER_1P_DELTA.IMP.OPPORTUNITY_PARENT T
				--INNER JOIN SUTTER_1P_DELTA.[XTR].[OPPORTUNITY] AS X1 ON T.External_Id__c=X1.EXTERNAL_ID__C
			 	ORDER BY zrefGFType, T.StageName, T.[ACCOUNT:External_Id__c], T.CloseDate, T.[CAMPAIGN:External_Id__c]
	 
	 
 
END;

 SELECT x.id, t.[zrefGFType], t.[External_Id__c] zrefExtId
 FROM [SUTTER_1P_DELTA].imp.[OPPORTUNITY_PARENT] T
 INNER JOIN [SUTTER_1P_DELTA].xtr.[OPPORTUNITY] x
 ON t.[External_Id__c]=x.[EXTERNAL_ID__C]
  WHERE t.[zrefGFType] LIKE '%pledge%'
