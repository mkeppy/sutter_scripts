

USE [SUTTER_1P_DELTA]
GO

BEGIN
	DROP TABLE SUTTER_1P_DELTA.IMP.CONTACT
END 

BEGIN--CONTACTS

			--CONTACTS: CONSTITUENTS HOUSEHOLD_CONTACTS
			SELECT DISTINCT
			 dbo.fnc_OwnerID() AS OwnerID
			,dbo.fnc_RecordType('Contact_Household') AS RecordTypeID
 		 	,CASE WHEN T.ConsID IS NULL OR T.ConsID ='' THEN '' ELSE T.RE_DB_OwnerShort+'-'+T.ConsID END AS	RE_ConsID__c
			,T.RE_DB_OwnerShort+'-'+T.ImportID AS RE_ImportID__c
			,T.RE_DB_OwnerShort+'-'+T.ImportID AS External_ID__c
			,T.RE_DB_OwnerShort+'-'+T.ImportID AS [ACCOUNT:External_ID__c]
			,NULL AS RE_IRImpID__c
 			,null as MDM_PTY_ID__c

			,CASE WHEN LEN(T.Bday)=10 THEN T.Bday END AS BirthDate
			,CASE WHEN LEN(T.Bday)=10 THEN MONTH(T.BDay ) WHEN LEN(T.Bday)=7 THEN LEFT(T.Bday,2) WHEN LEN(T.Bday)=4 THEN NULL END AS rC_Bios__Birth_Month__c
			,CASE WHEN LEN(T.Bday)=10 THEN DAY(T.BDay ) END AS rC_Bios__Birth_Day__c
			,CASE WHEN LEN(T.Bday)=10 THEN YEAR(T.BDay )  WHEN LEN(T.Bday)=7 THEN RIGHT(T.Bday,4) WHEN LEN(T.Bday)=4 THEN T.Bday END AS rC_Bios__Birth_Year__c
			
			,T.Deceased	AS	rC_Bios__Deceased__c
			,CASE WHEN LEN(T.DecDate)=10 THEN T.DecDate END AS rC_Bios__Deceased_Date__c
			,CASE WHEN LEN(T.DecDate)=10 THEN MONTH(T.DecDate ) WHEN LEN(T.DecDate)=7 THEN LEFT(T.DecDate,2) WHEN LEN(T.DecDate)=4 THEN NULL END AS rC_Bios__Deceased_Month__c
			,CASE WHEN LEN(T.DecDate)=10 THEN DAY(T.DecDate ) END AS rC_Bios__Deceased_Day__c
			,CASE WHEN LEN(T.DecDate)=10 THEN YEAR(T.DecDate )  WHEN LEN(T.DecDate)=7 THEN RIGHT(T.DecDate,4) WHEN LEN(T.DecDate)=4 THEN T.DecDate END AS rC_Bios__Deceased_Year__c

			,T.Ethnicity	AS	rC_Bios__Ethnicity__c
			,T.FirstName	AS	FirstName
			,T.Gender	AS	rC_Bios__Gender__c
			,T.GivesAnon	AS	Gives_Anonymously__c
			,T.NoValidAddr	AS	No_Valid_Address__c
			,CASE WHEN T.Deceased ='TRUE' THEN 'FALSE' WHEN T.IsInactive ='TRUE' THEN 'FALSE' WHEN T.IsInactive ='FALSE' THEN 'TRUE' END AS	rC_Bios__Active__c
			,T.IsSolicitor	AS	Is_Solicitor__c
			,T.LastName	AS	LastName
			,T.MaidName	AS	rC_Bios__Maiden_Name__c
			,T.MrtlStat	AS	rC_Bios__Marital_Status__c
			,T.MidName	AS	rC_Bios__Middle_Name__c
			,T.Nickname	AS	Nick_Name__c
			,T.NoEmail	AS	HasOptedOutOfEmail
			,T.Suff1	AS	Suffix_1__c
			,T.Suff2	AS	Suffix_2__c
			,T.Titl1	AS	Salutation
			,T.Titl2	AS	Title_2__c
			,T.DateAdded	AS	CreatedDate
	 		,CASE WHEN T.Deceased='TRUE' THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Preferred_Contact__c
			,'FALSE' AS rC_Bios__Secondary_Contact__c
			
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
			,GETDATE() AS	rC_Giving__Rollup_Soft_Credits__c
			,T4.Do_Not_Phone__c
			,T4.Do_Not_Solicit__c
			,T4.Do_Not_Mail__c
			,T4.Do_Not_Contact__c
			,CASE	WHEN T6.HomePhone IS NOT NULL THEN 'Home' 
					WHEN T6.rC_Bios__Work_Phone__c IS NOT NULL THEN 'Work' 
					WHEN T6.MobilePhone IS NOT NULL THEN 'Mobile'
					WHEN T6.rC_Bios__Other_Phone__c IS NOT NULL THEN 'Other' ELSE NULL 
					END AS rC_Bios__Preferred_Phone__c
			,CASE	WHEN T6.rC_Bios__Home_Email__c IS NOT NULL THEN 'Home' 
					--WHEN T6.rC_Bios__Work_Email__c IS NOT NULL THEN 'Work' 
					WHEN T6.rC_Bios__Other_Email__c IS NOT NULL THEN 'Other' 
					END AS rC_Bios__Preferred_Email__c
			,T6.Fax
			,T6.HomePhone
			,T6.MobilePhone
			,NULL AS Pager__c  
			,T6.rC_Bios__Home_Email__c
			,T6.rC_Bios__Other_Email__c
			,T6.rC_Bios__Other_Phone__c
			,NULL AS rC_Bios__Work_Email__c
			,T6.rC_Bios__Work_Phone__c
			,T6.Website__c
	 
			,CAST(T7.Additional_Phone_Numbers__c AS NVARCHAR(4000)) AS Additional_Phone_Numbers__c
			
			,CAST(T8.Agefinder_Birthdate__c AS NVARCHAR(255)) AS Agefinder_Birthdate__c
			,CAST(T8.CMS_Age__c  AS NVARCHAR(255)) AS CMS_Age__c
			,CAST(T8.PhoneFinder__c  AS NVARCHAR(255)) AS PhoneFinder__c
			,CAST(T8.SMCF_Employee_ID__c AS NVARCHAR(255)) AS SMCF_Employee_ID__c 
			,CAST(T8.SAFH_Employee_ID__c AS NVARCHAR(255)) AS SAFH_Employee_ID__c

			
			,CASE WHEN T9.AddrLine1 IS NULL THEN NULL WHEN T9.AddrLine2!='' THEN T9.AddrLine1 +' '+T9.AddrLine2 ELSE T9.AddrLine1 END AS MailingStreet
			,T9.AddrCity AS MailingCity
			,T9.AddrState AS MailingState
			,T9.AddrZIP AS MailingPostalCode
			,T9.AddrCountry MailingCountry
			
	 		,Proxy_Contact__c='FALSE'

			--reference only
			,'Cons Ind HeadOfHH' AS zrefSource
	 
  			INTO SUTTER_1P_DELTA.IMP.CONTACT
			FROM SUTTER_1P_DELTA.dbo.HC_Constituents_v T	
			INNER JOIN SUTTER_1P_DELTA.tbl.Contact_HofH T2 ON T.RE_DB_ID=T2.HH_ImportID    --HH_ImportID  is already the new_tgt_importId
			LEFT JOIN SUTTER_1P_DELTA.TBL.SolicitCodes T4 ON T.RE_DB_ID=T4.NEW_TGT_IMPORT_ID 
			LEFT JOIN SUTTER_1P_DELTA.TBL.Phones_seq_1_final T6 ON T.RE_DB_ID=T6.NEW_TGT_IMPORT_ID 
			LEFT JOIN SUTTER_1P_DELTA.TBL.Phones_seq_2_final T7 ON T.RE_DB_ID=T7.NEW_TGT_IMPORT_ID   
			LEFT JOIN SUTTER_1P_DELTA.TBL.Attributes_to_Contact T8 ON T.RE_DB_ID=T8.NEW_TGT_IMPORT_ID 
			LEFT JOIN SUTTER_1P_DELTA.TBL.Address_Pref	T9 ON T.RE_DB_ID=T9.RE_DB_ID 
	 		-- (550 row(s) affected)
	 
	 
	 	--donor proxy contacts 
			INSERT INTO [SUTTER_1P_DELTA].IMP.[CONTACT]
			        ( [OwnerID] ,  [RecordTypeID] ,  [RE_ConsID__c] ,  [RE_ImportID__c] ,  [External_ID__c] ,  [ACCOUNT:External_ID__c] ,
			          [RE_IRImpID__c] ,  [MDM_PTY_ID__c] ,  [BirthDate] ,  
			          [rC_Bios__Birth_Month__c] , [rC_Bios__Birth_Day__c] , [rC_Bios__Birth_Year__c] ,
			          [rC_Bios__Deceased__c] ,  [rC_Bios__Deceased_Date__c] ,
			          [rC_Bios__Deceased_Month__c] ,  [rC_Bios__Deceased_Day__c] ,
			          [rC_Bios__Deceased_Year__c] ,  [rC_Bios__Ethnicity__c] ,
			          [FirstName] ,  [rC_Bios__Gender__c] ,
			          [Gives_Anonymously__c] ,  [No_Valid_Address__c] ,
			          [rC_Bios__Active__c] ,  [Is_Solicitor__c] ,
			          [LastName] ,  [rC_Bios__Maiden_Name__c] ,
			          [rC_Bios__Marital_Status__c] ,
			          [rC_Bios__Middle_Name__c] ,
			          [Nick_Name__c] ,
			          [HasOptedOutOfEmail] ,
			          [Suffix_1__c] ,
			          [Suffix_2__c] ,
			          [Salutation] ,
			          [Title_2__c] ,
			          [CreatedDate] ,
			          [rC_Bios__Preferred_Contact__c] ,
			          [rC_Bios__Secondary_Contact__c] ,
			          [rC_Giving__Rollup_Hard_Credits__c] ,
			          [rC_Giving__Rollup_Soft_Credits__c] ,
			          [Do_Not_Phone__c] ,
			          [Do_Not_Solicit__c] ,
			          [Do_Not_Mail__c] ,
			          [Do_Not_Contact__c] ,
			          [rC_Bios__Preferred_Phone__c] ,
			          [rC_Bios__Preferred_Email__c] ,
			          [Fax] ,
			          [HomePhone] ,
			          [MobilePhone] ,
			          [Pager__c] ,
			          [rC_Bios__Home_Email__c] ,
			          [rC_Bios__Other_Email__c] ,
			          [rC_Bios__Other_Phone__c] ,
			          [rC_Bios__Work_Email__c] ,
			          [rC_Bios__Work_Phone__c] ,
			          [Website__c] ,
			          [Additional_Phone_Numbers__c] ,
			          [Agefinder_Birthdate__c] ,
			          [CMS_Age__c] ,
			          [PhoneFinder__c] ,
					  [SMCF_Employee_ID__c], 
					  [SAFH_Employee_ID__c], 

			          [MailingStreet] ,
			          [MailingCity] ,
			          [MailingState] ,
			          [MailingPostalCode] ,
			          [MailingCountry] ,
			 		  [Proxy_Contact__c],
			          [zrefSource]
			        ) 	SELECT DISTINCT
							dbo.fnc_OwnerID() AS OwnerID
						,dbo.fnc_RecordType('Contact_Organizational') AS RecordTypeID
 		 				,NULL AS RE_ConsID__c
						,T2.NEW_TGT_IMPORT_ID AS RE_ImportID__c
						,T2.NEW_TGT_IMPORT_ID AS External_ID__c
						,T2.NEW_TGT_IMPORT_ID AS [ACCOUNT:External_ID__c]
						,NULL AS RE_IRImpID__c			
		
						,NULL MDM_PTY_ID__c
 						,NULL AS BirthDate
						,NULL AS rC_Bios__Birth_Month__c
						,NULL AS rC_Bios__Birth_Day__c
						,NULL AS rC_Bios__Birth_Year__c
 						,'FALSE' AS rC_Bios__Deceased__c
						,NULL AS rC_Bios__Deceased_Date__c
						,NULL AS rC_Bios__Deceased_Month__c
						,NULL AS rC_Bios__Deceased_Day__c
						,NULL AS rC_Bios__Deceased_Year__c
							,NULL AS rC_Bios__Ethnicity__c
						,'MAILING'	AS	FirstName
						,'' AS rC_Bios__Gender__c
						,'FALSE' AS Gives_Anonymously__c
						,'FALSE' AS No_Valid_Address__c
						,'TRUE' AS rC_Bios__Active__c
						,'FALSE' AS Is_Solicitor__c
						,'CONTACT' AS	LastName
						,NULL AS rC_Bios__Maiden_Name__c
						,NULL AS rC_Bios__Marital_Status__c
						,NULL AS rC_Bios__Middle_Name__c
						,NULL AS Nick_Name__c
						,'FALSE' AS HasOptedOutOfEmail
						,NULL AS Suffix_1__c
						,NULL AS Suffix_2__c
						,NULL AS Salutation
						,NULL AS Title_2__c
						,NULL AS CreatedDate
						,'TRUE' AS rC_Bios__Preferred_Contact__c
						,'FALSE' AS rC_Bios__Secondary_Contact__c
			
						,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
						,GETDATE() AS	rC_Giving__Rollup_Soft_Credits__c
		
						,NULL AS Do_Not_Phone__c
						,NULL AS Do_Not_Solicit__c
						,NULL AS Do_Not_Mail__c			   
						,NULL AS Do_Not_Contact__c
						,NULL AS rC_Bios__Preferred_Phone__c
						,NULL AS rC_Bios__Preferred_Email__c
						,NULL AS Fax
						,NULL AS HomePhone
						,NULL AS MobilePhone
						,NULL AS Pager__c  
						,NULL AS rC_Bios__Home_Email__c
						,NULL AS rC_Bios__Other_Email__c
						,NULL AS rC_Bios__Other_Phone__c
						,NULL AS rC_Bios__Work_Email__c
						,NULL AS rC_Bios__Work_Phone__c
						,NULL AS Website__c
						,NULL AS Additional_Phone_Numbers__c
						,NULL AS Agefinder_Birthdate__c
						,NULL AS CMS_Age__c
						,NULL AS PhoneFinder__c
						,NULL AS SMCF_Employee_ID__c 
						,NULL AS SAFH_Employee_ID__c

						,CASE WHEN T9.AddrLine1 IS NULL THEN NULL WHEN T9.AddrLine2!='' THEN T9.AddrLine1 +' '+T9.AddrLine2 ELSE T9.AddrLine1 END AS MailingStreet
						,T9.AddrCity AS MailingCity
						,T9.AddrState AS MailingState
						,T9.AddrZIP AS MailingPostalCode
						,T9.AddrCountry MailingCountry

						,Proxy_Contact__c='TRUE'
		 				,'DonorProxy' AS zrefSource
			 			FROM SUTTER_1P_DELTA.tbl.Account_Org T2 
						LEFT JOIN SUTTER_1P_DELTA.IMP.CONTACT T3 ON T2.NEW_TGT_IMPORT_ID=T3.[ACCOUNT:External_ID__c] 
						LEFT JOIN SUTTER_1P_DELTA.TBL.Address_Pref	T9 ON T2.RE_DB_ID=T9.RE_DB_ID 

						WHERE T3.[ACCOUNT:External_ID__c] IS NULL 
 


END; --	(561 row(s) affected)  --(34 row(s) affected)
	 
BEGIN --REPLACE DOUBLE QUOTES TO SINGLE QUOTES
  --  SELECT  [FirstName] FROM [SUTTER_1P_DELTA].[IMP].[CONTACT] WHERE [FirstName] LIKE '%"%';
  --  SELECT  [rC_Bios__Middle_Name__c] FROM [SUTTER_1P_DELTA].[IMP].[CONTACT] WHERE [rC_Bios__Middle_Name__c] LIKE '%"%';
  --  SELECT  [LastName] FROM [SUTTER_1P_DELTA].[IMP].[CONTACT] WHERE [LastName] LIKE '%"%';
  --  SELECT  [Nick_Name__c] FROM [SUTTER_1P_DELTA].[IMP].[CONTACT] WHERE [Nick_Name__c] LIKE '%"%';
  --  SELECT  [rC_Bios__Maiden_Name__c] FROM [SUTTER_1P_DELTA].[IMP].[CONTACT] WHERE [rC_Bios__Maiden_Name__c] LIKE '%"%';
  -- SELECT  [Salutation] FROM [SUTTER_1P_DELTA].[IMP].[CONTACT] WHERE [Salutation] LIKE '%"%';
  --  SELECT  [Additional_Phone_Numbers__c] FROM [SUTTER_1P_DELTA].[IMP].[CONTACT] WHERE [Additional_Phone_Numbers__c] LIKE '%"%';
        
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET [FirstName]=REPLACE([FirstName],'"','''') where [FirstName] like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET [rC_Bios__Middle_Name__c]=REPLACE([rC_Bios__Middle_Name__c],'"','''') where [rC_Bios__Middle_Name__c] like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET [LastName]=REPLACE([LastName],'"','''') where [LastName] like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET [Nick_Name__c]=REPLACE([Nick_Name__c],'"','''') where [Nick_Name__c] like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET [rC_Bios__Maiden_Name__c]=REPLACE([rC_Bios__Maiden_Name__c],'"','''') where [rC_Bios__Maiden_Name__c] like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET [Salutation]=REPLACE([Salutation],'"','''') where [Salutation] like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET [Additional_Phone_Numbers__c]=REPLACE([Additional_Phone_Numbers__c],'"','''') where [Additional_Phone_Numbers__c] like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET [HomePhone]=REPLACE([HomePhone],'"','''') where [HomePhone] like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET [rC_Bios__Other_Email__c]=REPLACE([rC_Bios__Other_Email__c],'"','''') where [rC_Bios__Other_Email__c] like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET [LastName]='Unknown' where [LastName] IS NULL 
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET [MailingStreet]=REPLACE([MailingStreet],'"','''') where [MailingStreet] like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET [MailingCity]=REPLACE([MailingCity],'"','''') where [MailingCity] like '%"%'
			
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET rC_Bios__Work_Phone__c=REPLACE(rC_Bios__Work_Phone__c,'"','''') where rC_Bios__Work_Phone__c like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET MobilePhone=REPLACE(MobilePhone,'"','''') where MobilePhone like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET rC_Bios__Other_Phone__c=REPLACE(rC_Bios__Other_Phone__c,'"','''') where rC_Bios__Other_Phone__c like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET rC_Bios__Other_Phone__c=REPLACE(rC_Bios__Other_Phone__c,'"','''') where rC_Bios__Other_Phone__c like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[CONTACT] SET rC_Bios__Home_Email__c=REPLACE(rC_Bios__Home_Email__c,'"','''') where rC_Bios__Home_Email__c like '%"%'
 			
	
END

		BEGIN 
			USE [SUTTER_1P_DELTA] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'CONTACT'
 		END 

--Update Deceased Year to Unknown.
		BEGIN
			SELECT rC_Bios__Deceased__c, rC_Bios__Deceased_Date__c, rC_Bios__Deceased_Year__c 
			FROM SUTTER_1P_DELTA.IMP.CONTACT
			WHERE rC_Bios__Deceased__c='TRUE' AND rC_Bios__Deceased_Date__c IS NULL AND rC_Bios__Deceased_Year__c IS null
	
			ALTER TABLE SUTTER_1P_DELTA.[IMP].[CONTACT]
			ALTER COLUMN rC_Bios__Deceased_Year__c NVARCHAR(20)
	
			UPDATE SUTTER_1P_DELTA.[IMP].[CONTACT]
			SET rC_Bios__Deceased_Year__c='Unknown'
			WHERE rC_Bios__Deceased__c='TRUE' AND rC_Bios__Deceased_Date__c IS NULL AND rC_Bios__Deceased_Year__c IS null
		
		END

		BEGIN--check duplicates 
			SELECT * 
		--	INTO [SUTTER_1P_DELTA].tbl.contact_duplicate
			FROM [SUTTER_1P_DELTA].[IMP].[CONTACT] 
			
			WHERE External_ID__c IN (SELECT External_ID__c FROM [SUTTER_1P_DELTA].[IMP].[CONTACT] GROUP BY External_ID__c HAVING COUNT(*)>1)
			ORDER BY External_ID__c, zrefSource
		END 

 
		BEGIN-- contacts with no valid Account:ExternalID

				SELECT    T1.*, T2.External_ID__c AS Expr1
				FROM         SUTTER_1P_DELTA.IMP.CONTACT AS T1 
							LEFT JOIN SUTTER_1P_DELTA.IMP.ACCOUNT AS T2 ON T1.[ACCOUNT:External_ID__c] = T2.External_ID__c
				WHERE     (T2.External_ID__c IS NULL) OR (T2.External_ID__c = N'')
				ORDER BY [ACCOUNT:External_ID__c]
	
		END;

		BEGIN --contacts with no account
				SELECT    T1.*
				FROM         SUTTER_1P_DELTA.IMP.CONTACT AS T1 
				WHERE [ACCOUNT:External_ID__c] IS null
		END;

	
		BEGIN --contacts with no lastname
				SELECT    T1.*
				FROM         SUTTER_1P_DELTA.IMP.CONTACT AS T1 
				WHERE LastName IS null
		END;

		
		BEGIN --contacts with no external id
				SELECT    T1.*
				FROM         SUTTER_1P_DELTA.IMP.CONTACT AS T1 
				WHERE External_Id__c IS null
		END;

		 

		--DUPLICATE SECONDARY CONTACT LIST. 
		SELECT [RE_ConsID__c], [RE_ImportID__c], [RE_IRImpID__c], [External_ID__c], [ACCOUNT:External_ID__c], [rC_Bios__Preferred_Contact__c], [rC_Bios__Secondary_Contact__c], [FirstName], [LastName]
		FROM imp.[CONTACT]
		WHERE [ACCOUNT:External_ID__c]+[rC_Bios__Secondary_Contact__c] 
		IN (SELECT [ACCOUNT:External_ID__c]+[rC_Bios__Secondary_Contact__c]
			FROM imp.[CONTACT] 
			WHERE [rC_Bios__Secondary_Contact__c]='true'
			GROUP BY [ACCOUNT:External_ID__c]+[rC_Bios__Secondary_Contact__c]
			HAVING COUNT(*)>1)
		ORDER BY [ACCOUNT:External_ID__c]

	   
		--TEST
		 
			SELECT * FROM SUTTER_1P_DELTA.IMP.CONTACT
	 


 --exceptions

			DROP TABLE SUTTER_1P_DELTA.IMP.CONTACT_XCP

			SELECT		T1.*
		--	INTO		SUTTER_1P_DELTA.IMP.CONTACT_XCP
			FROM        SUTTER_1P_DELTA.IMP.CONTACT AS T1 
			LEFT JOIN	SUTTER_1P_DELTA.XTR.CONTACT AS X1 ON T1.External_ID__c = X1.EXTERNAL_ID__C
			WHERE     (X1.EXTERNAL_ID__C IS NULL) OR (X1.EXTERNAL_ID__C = '')
			ORDER BY T1.External_ID__c
			--102

		SELECT DISTINCT EXTERNAL_ID__C FROM  SUTTER_1P_DELTA.IMP.CONTACT
		SELECT COUNT(DISTINCT EXTERNAL_ID__C)  FROM  SUTTER_1P_DELTA.IMP.CONTACT 
 
		SELECT COUNT(*)  FROM [SUTTER_1P_DELTA].XTR.CONTACT



	   