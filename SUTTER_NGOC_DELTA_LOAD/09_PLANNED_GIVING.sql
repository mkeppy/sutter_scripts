USE SUTTER_1P_DELTA
GO

BEGIN
	DROP TABLE SUTTER_1P_DELTA.IMP.PLANNED_GIVING
END 
 
BEGIN--PLANNED GIVING
		 		SELECT  
					--IDs
						dbo.fnc_OwnerID() AS OwnerID  
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS RE_GSplitImpID__c
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitGFImpID AS RE_GFImpID__c
						,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS rC_Giving__External_ID__c
						,T.GFVehicle +': '+ CAST(CAST(T.GFDate AS DATE)AS NVARCHAR(10)) Name       
						,T.[GSplitAmt] AS rC_Giving__Giving_Amount__c
						,T.[GSplitAmt] AS rC_Giving__Estimated_Giving_Amount__c
/*EXT_ID*/				,T.[CAMPAIGN_External_Id__c]	AS [rC_Giving__Campaign__r:External_ID__c]						
/*EXT_ID*/				,T.[ACCOUNT:External_Id__c]	AS [rC_Giving__Account__r:External_ID__c]					
						,T.[GFDate] AS rC_Giving__Date_Station_Notified_Of_Intention__c
						,T.refNewImportID AS [rC_Giving__Contact__r:External_ID__c]

						,NULL AS Proposal_Date_Asked__c
/*EXT_ID*/				,T.GAU_External_Id__c	AS [General_Accounting_Unit__r:rC_Giving__External_ID__c]
						,NULL AS Proposal_Purpose__c
						,NULL AS Proposal_Rating__c
						,NULL AS Proposal_Reason__c

						,CASE WHEN T.GFPGStatus IS NULL OR T.GFPGStatus ='' THEN 'Intention' ELSE T.GFPGStatus END AS rC_Giving__Stage__c
						,T.DateAdded	AS	CreatedDate
						,T.[GSplitRE_DB_OwnerShort] AS rC_Giving__Affiliation__c 
						,CASE T.GFVehicle 
							WHEN 'Bequest'					THEN dbo.fnc_RecordType('Planned_Giving_Bequest') 
							WHEN 'Gift Annuity'				THEN dbo.fnc_RecordType('Planned_Giving_Charitable_Gift_Annuity')
							WHEN 'Lead Unitrust'			THEN dbo.fnc_RecordType('Planned_Giving_Charitable_Lead_Unitrust')
							WHEN 'Life Insurance'			THEN dbo.fnc_RecordType('Planned_Giving_Life_Insurance')
							WHEN 'Other Planned Gift'		THEN dbo.fnc_RecordType('Planned_Giving_Bequest')
							WHEN 'Pooled Income Fund'		THEN dbo.fnc_RecordType('Planned_Giving_Pooled_Income_Fund')
							WHEN 'Remainder Annuity Trust'	THEN dbo.fnc_RecordType('Planned_Giving_Charitable_Remainder_Annuity_Trust')
							WHEN 'Remainder Unitrust'		THEN dbo.fnc_RecordType('Planned_Giving_Charitable_Remainder_Unitrust')
							WHEN 'Retained Life Estate'		THEN dbo.fnc_RecordType('Planned_Giving_Retained_Life_Estate')
						  END AS RecordTypeID  

						,NULL AS rC_Giving__Rollup_Giving__c
		  
					--gift attribute (chart_giftattribute)
						--,T2.[Considering bequest in] AS Considering_bequest_in__c
						,T2.[Production] AS Production__c
						--,T2.[Report Exclusion] AS Report_Exclusion__c
						--,T2.[Source of notification] AS Source_of_notification__c
					--MISC.
						,T.RE_Split_Gift__c
						,T.GFType AS RE_Gift_Type__c
						,T.RE_Gift_SubType__c
						,T.GFPostDate__c AS Post_Date__c
						,T.GFPostStatus__c AS Post_Status__c
						,T.GFConsDesc AS RE_Gift_Constituency_Code__c
						,T.GFPayMeth AS RE_Gift_Payment_Method__c
						,T.GFRef AS rC_Giving__Notes__c
						,T.GFTRealized AS RE_Gift_Realized__c	
						,T.RE_Legacy_Financial_Code__c

					--reference
					,'RE Gift' AS zrefSrc
					,T.ImportID AS zref_ImportID	
					,T.HH_ImportID AS zref_NoHH_ImportID	
					,T.NoHH_ImportID AS zref_HH_ImportID 	
					,NULL	 AS zrefPRImpID
					,LEFT(T.RE_GFSplitImportID_ID,4) AS zrefRE_DB_OwnerShort	
 					INTO SUTTER_1P_DELTA.[IMP].[PLANNED_GIVING] 
					FROM SUTTER_1P_DELTA.TBL.GIFT T
					--LEFT JOIN SUTTER_1P_DELTA.XTR.ACCOUNT T1 ON T.[ACCOUNT:External_Id__c]=T1.EXTERNAL_ID__C 
					LEFT JOIN SUTTER_1P_DELTA.TBL.Gift_Attribute_1 T2 ON T.GSplitGFImpID = T2.GFIMPID AND T.GSplitRE_DB_OwnerShort=T2.RE_DB_OwnerShort
					WHERE T.GFType  ='Planned Gift'
 
END;				 
 

SELECT * FROM SUTTER_1P_DELTA.[IMP].[PLANNED_GIVING] 
 --double quotes
 BEGIN 
		USE SUTTER_1P_DELTA 
 		EXEC sp_FindStringInTable '%"%', 'IMP', 'PLANNED_GIVING'
 
	 	UPDATE SUTTER_1P_DELTA.[IMP].[PLANNED_GIVING] SET rC_Giving__Notes__c=REPLACE(rC_Giving__Notes__c,'"','''') where rC_Giving__Notes__c like '%"%'

 END 
 
 SELECT GFType, COUNT(*) c
 FROM SUTTER_1P_DELTA.TBL.GIFT
 GROUP BY GFType
	SELECT * FROM [SUTTER_1P_DELTA].imp.campaign	  