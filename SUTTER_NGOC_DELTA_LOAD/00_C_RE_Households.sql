 	USE [SUTTER_1P_DELTA]
	GO
	IF schema_id('TBL') IS NULL EXECUTE('CREATE SCHEMA TBL')
	GO
	IF schema_id('IMP') IS NULL EXECUTE('CREATE SCHEMA IMP') 
	GO
	IF schema_id('CHART') IS NULL EXECUTE('CREATE SCHEMA CHART') 
	GO

	/*
		DROP TABLE [SUTTER_1P_DELTA].TBL.Account_Org
		GO
		DROP TABLE [SUTTER_1P_DELTA].TBL.Contact_HofH
		GO
		DROP TABLE [SUTTER_1P_DELTA].TBL.Contact_NoHofH
		GO
	*/
    	 
	--A_Organization Accounts
			--A_tbl_Account_Org:  Constituents that are Organization Constituents;
				SELECT * FROM [SUTTER_1P_DELTA].dbo.HC_Constituents_v 
			
				DROP TABLE [SUTTER_1P_DELTA].TBL.Account_Org
				
				SELECT T.KeyInd, T.ConsID, T.[RE_DB_Id] AS ImportID, T.OrgName, T.RE_DB_OwnerShort,  T.[RE_DB_Id],  T.[RE_DB_Id] AS NEW_TGT_IMPORT_ID
				INTO [SUTTER_1P_DELTA].TBL.Account_Org
				FROM [SUTTER_1P_DELTA].dbo.HC_Constituents_v T
				WHERE T.KeyInd='O'
				GO
				--34
				
				--need table from final conversion to migrate NEW gifts/address to existing Acc/Cont
	 			SELECT * 
				INTO  [SUTTER_1P_DELTA].TBL.Account_Org_final_conv
				FROM  [SUTTER_1P_MIGRATION].TBL.Account_Org			   

	--_B_C_D Head of Household ACcounts/Contacts
 			--B_tbl_Contact_Cons_HofH:  Constituents that are Head of Household Constituents only;
				DROP TABLE  [SUTTER_1P_DELTA].TBL.Contact_HofH
			
			
				SELECT T.RE_DB_OwnerShort, T.KeyInd, T.[RE_DB_Id] AS ImportID,  T.ConsID, T.Deceased,  -- hh
				NULL AS  NonHHImpID, NULL AS  NonHHDeceased,		--non-hh
				NULL AS IRLink, NULL  AS IRIsHH, NULL AS IRIsSpouse, NULL AS IRImpID,  
				T.[RE_DB_Id] AS HH_ImportID, 
				T.ConsID AS HH_ConsID, 
				CAST('' AS nvarchar(50)) AS NoHH_ImportID, 
				CAST('' AS nvarchar(50)) AS NoHH_ConsID,
				'delta' AS scr
				,ROW_NUMBER() OVER ( PARTITION BY T.ImportID  ORDER BY T.ImportID ) AS Seq 
				INTO [SUTTER_1P_DELTA].TBL.Contact_HofH
				FROM [SUTTER_1P_DELTA].dbo.HC_Constituents_v T
				WHERE ((T.KeyInd)='I')  
				ORDER BY T.ImportID
				GO
			
 		 	   	--need table from final conversion to migrate NEW gifts/address to existing Acc/Cont
	 			SELECT * 
				INTO  [SUTTER_1P_DELTA].TBL.Contact_HofH_final_conv
				FROM  [SUTTER_1P_MIGRATION].TBL.Contact_HofH	   
	  