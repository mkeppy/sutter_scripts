
USE SUTTER_1P_DELTA
GO 

 
 				 
BEGIN
		DROP TABLE SUTTER_1P_MIGRATION.TBL.PREFERENCE
		DROP TABLE SUTTER_1P_MIGRATION.IMP.PREFERENCE
END 

 
 BEGIN--BASE TABLE PREFERENCES 

		--CONSTITUENT ATTRIBUTE from CHART_ATTRIBUTES 
				
				--HC_Cons_Attributes_v
				SELECT DISTINCT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.CAttrImpID)
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN (T1.KeyInd='O') THEN T1.NEW_TGT_IMPORT_ID 
														   WHEN (T1.KeyInd='I' AND T2.PREFERENCE_Category='Ratings' AND T3.NoHH_ImportID IS NOT NULL) THEN T3.HH_ImportID 
														   ELSE NULL END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T1.NEW_TGT_IMPORT_ID END 
				,rC_Bios__Category__c=T2.PREFERENCE_Category
				,rC_Bios__Subcategory__c=T2.PREFERENCE_Subcategory
				,rC_Bios__Type__c=T2.PREFERENCE_Type
				,rC_Bios__Subtype__c=T2.PREFERENCE_Subtype
				,rC_Bios__Value__c=CASE WHEN T2.PREFERENCE_Value IS NULL OR T2.PREFERENCE_Value='' THEN T.CAttrDesc ELSE  T2.PREFERENCE_Value END 
				,rC_Bios__Comments__c=T.CAttrCom
				,rC_Bios__Start_Date__c=T.CAttrDate
				,rC_Bios__End_Date__c=T.CAttrDate
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.[RE_DB_OwnerShort] 
				,NULL AS rC_Bios__Active__c
				,NULL AS rC_Bios__Status__c
				,NULL AS Capacity_Rating__c	
				,NULL AS Net_Worth__c
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c
				,NULL AS RE_MEHisImportID__c
				,NULL AS RE_MembershipID__c
				,NULL AS RE_HisMembershipID__c
				,NULL AS RE_MELifetime__c
				,NULL AS Annual_Income__c
				,NULL AS P2G_Score__c
	    
				--reference
				,T1.KeyInd AS zrefKeyInd
				,T.CAttrImpID AS zrefRecImportID
				,'ConsAttribute_1' AS zrefRecSource
				INTO SUTTER_1P_MIGRATION.TBL.PREFERENCE
				FROM SUTTER_1P_DELTA.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_1P_DELTA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T3 ON T1.NEW_TGT_IMPORT_ID=T3.NoHH_ImportID  
				INNER JOIN SUTTER_1P_DELTA.dbo.CHART_Attributes T2 ON T.CAttrCat=T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.SF_Object_1='RC_BIOS_PREFERENCE__C' AND T.CAttrDesc IS NOT NULL AND T2.[Convert]='Yes'
		   
			UNION ALL 
				
		--CONSTITUENT ATTRIBUTE from CHART_CONS_ATTRIBUTES 
				SELECT DISTINCT 
				dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType(T2.PREFERENCE_RecordType) AS RecordTypeID
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.CAttrImpID)
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN (T1.KeyInd='O') THEN T1.NEW_TGT_IMPORT_ID 
														   WHEN (T1.KeyInd='I' AND T2.PREFERENCE_Category='Ratings' AND T3.NoHH_ImportID IS NOT NULL) THEN T3.HH_ImportID 
														   ELSE NULL END	

				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T1.NEW_TGT_IMPORT_ID END 
				,rC_Bios__Category__c=T2.PREFERENCE_Category
				,rC_Bios__Subcategory__c=T2.PREFERENCE_Subcategory
				,rC_Bios__Type__c=T2.PREFERENCE_Type
				,rC_Bios__Subtype__c=T2.PREFERENCE_Subtype
				,rC_Bios__Value__c= T2.PREFERENCE_Value  
				,rC_Bios__Comments__c=T.CAttrCom
				,rC_Bios__Start_Date__c=T.CAttrDate
				,rC_Bios__End_Date__c=NULL
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.[RE_DB_OwnerShort]
				,T2.PREFERENCE_Active AS rC_Bios__Active__c  
				,NULL AS rC_Bios__Status__c
				,T2.PREFERENCE_Capacity_Rating__c AS Capacity_Rating__c
				,T2.PREFERENCE_Net_Worth__c AS Net_Worth__c
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c
				,NULL AS RE_MEHisImportID__c
				,NULL AS RE_MembershipID__c
				,NULL AS RE_HisMembershipID__c
				,NULL AS RE_MELifetime__c
				,NULL AS Annual_Income__c
				,NULL AS P2G_Score__c
		    
				--reference
				,T1.KeyInd AS zrefKeyInd
				,T.CAttrImpID AS zrefRecImportID
				,'ConsAttribute_2' AS zrefRecSource
				FROM SUTTER_1P_DELTA.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_1P_DELTA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T3 ON T1.NEW_TGT_IMPORT_ID=T3.NoHH_ImportID  
				INNER JOIN SUTTER_1P_DELTA.dbo.CHART_ConsAttributes T2 ON T.CAttrCat=T2.CAttrCat AND T.CAttrDesc=T2.CAttrDesc AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.PREFERENCE_RecordType IS NOT NULL AND T2.PREFERENCE_RecordType !='' AND T2.[Convert]='Yes'
				 
			UNION ALL 
				
		--CONSTITUENT CODE 
				SELECT DISTINCT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				,rC_Bios__External_ID__c= CASE WHEN T.ConsCodeImpID IS NULL THEN T.RE_DB_Tbl+'-'+T1.NEW_TGT_IMPORT_ID ELSE (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.ConsCodeImpID) END 
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T1.NEW_TGT_IMPORT_ID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T1.NEW_TGT_IMPORT_ID END 
				,rC_Bios__Category__c='Constituent Type'
				,rC_Bios__Subcategory__c=T2.PREFERENCE_Subcategory
				,rC_Bios__Type__c=T2.PREFERENCE_Type
				,rC_Bios__Subtype__c=T2.PREFERENCE_Subtype
				,rC_Bios__Value__c=CASE WHEN T2.PREFERENCE_Value IS NULL OR T2.PREFERENCE_Value='' THEN T.ConsCodeDesc ELSE  T2.PREFERENCE_Value END	
				,rC_Bios__Comments__c=NULL
				,rC_Bios__Start_Date__c=CASE WHEN LEN(T.ConsCodeDateFrom)=10 THEN T.ConsCodeDateFrom 
					WHEN LEN (T.ConsCodeDateFrom)=7 THEN (LEFT(T.ConsCodeDateFrom,2) +'/01/'+ RIGHT(T.ConsCodeDateFrom,4))
					WHEN LEN (T.ConsCodeDateFrom)=4 THEN '01/01/'+T.ConsCodeDateFrom   ELSE NULL END  
				,rC_Bios__End_Date__c=CASE WHEN LEN(T.ConsCodeDateTo)=10 THEN T.ConsCodeDateTo 
					WHEN LEN (T.ConsCodeDateTo)=7 THEN (LEFT(T.ConsCodeDateTo,2) +'/01/'+ RIGHT(T.ConsCodeDateTo,4))
					WHEN LEN (T.ConsCodeDateTo)=4 THEN '01/01/'+T.ConsCodeDateTo   ELSE NULL END  
				,rC_Bios__Priority__c=T2.PREFERENCE_Priority
				,rC_Bios__Affiliation__c=T.[RE_DB_OwnerShort]
				,rC_Bios__Active__c = T2.PREFERENCE_Active  
				,NULL AS rC_Bios__Status__c
				,NULL AS Capacity_Rating__c
				,NULL AS Net_Worth__c
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c
				,NULL AS RE_MEHisImportID__c
				,NULL AS RE_MembershipID__c
				,NULL AS RE_HisMembershipID__c
				,NULL AS RE_MELifetime__c
				,NULL AS Annual_Income__c
				,NULL AS P2G_Score__c


				--reference
				,T1.KeyInd AS zrefKeyInd
				,T.ConsCodeImpID AS zrefRecImportID
				,'ConsCode' AS zrefRecSource
				FROM SUTTER_1P_DELTA.dbo.HC_Cons_ConsCode_v T
				INNER JOIN SUTTER_1P_DELTA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_1P_DELTA.dbo.CHART_ConstituencyCodes T2 ON T.ConsCode=T2.ConsCode AND T1.KeyInd=T2.KeyInD AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.[Convert]='Yes'
	  		
			UNION ALL 	 
		
		--CONSTITUENT SOLICIT CODE 
				SELECT DISTINCT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+CAST(T.ID AS NVARCHAR(20)))
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T1.NEW_TGT_IMPORT_ID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T1.NEW_TGT_IMPORT_ID END 
				,rC_Bios__Category__c=T2.PREFERENCE_Category
				,rC_Bios__Subcategory__c=T2.PREFERENCE_SubCategory
				,rC_Bios__Type__c=T2.[PREFERENCE_Type]
				,rC_Bios__Subtype__c=T2.[PREFERENCE_Subtype]
				,rC_Bios__Value__c=T2.PREFERENCE_Value 
				,rC_Bios__Comments__c=NULL
				,rC_Bios__Start_Date__c=NULL
				,rC_Bios__End_Date__c=NULL
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.[RE_DB_OwnerShort]
				,NULL rC_Bios__Active__c  
				,NULL AS rC_Bios__Status__c
				,NULL AS Capacity_Rating__c
				,NULL AS Net_Worth__c
				,NULL AS Emergency_Contact_Name__c
				,NULL AS Emergency_Contact_Phone__c
				,NULL AS Emergency_Contact_Relationship__c
				,NULL AS RE_MEHisImportID__c
				,NULL AS RE_MembershipID__c
				,NULL AS RE_HisMembershipID__c
				,NULL AS RE_MELifetime__c
				,NULL AS Annual_Income__c
				,NULL AS P2G_Score__c

 
				--reference
				,T1.KeyInd AS zrefKeyInd
				,CAST(T.ID AS NVARCHAR(20))AS zrefRecImportID
				,'ConsSolCode' AS zrefRecSource
				FROM SUTTER_1P_DELTA.dbo.HC_Cons_SolicitCodes_v T
				INNER JOIN SUTTER_1P_DELTA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_1P_DELTA.dbo.CHART_ConsSolicitCodes T2 ON T.SolicitCode=T2.SolicitCode AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.[Convert]='Yes' AND T2.SF_Object_1 ='RC_BIOS_PREFERENCE__C'
			
 

				  
END--end of create tbl	3384391

BEGIN -- check len of dates 
		SELECT DISTINCT LEN(rC_Bios__Start_Date__c) len_startdate, zrefRecSource
		FROM SUTTER_1P_DELTA.TBL.[PREFERENCE]
		WHERE rC_Bios__Start_Date__c IS NOT NULL 
		ORDER BY len_startdate
END 

BEGIN-- check category null
		SELECT * FROM SUTTER_1P_DELTA.TBL.[PREFERENCE]
		WHERE [rC_Bios__Category__c] IS NULL

		DELETE SUTTER_1P_DELTA.TBL.[PREFERENCE]
		WHERE [rC_Bios__Category__c] IS NULL

END
  
 
BEGIN--PREFERENCE IMP

			CREATE INDEX idx_preference ON SUTTER_1P_MIGRATION.TBL.PREFERENCE([rC_Bios__Account__r:External_ID__c],[rC_Bios__Contact__r:External_ID__c], rC_Bios__Value__c);

			SELECT DISTINCT *
			INTO SUTTER_1P_MIGRATION.IMP.PREFERENCE
			FROM SUTTER_1P_MIGRATION.TBL.PREFERENCE
			ORDER BY [rC_Bios__Account__r:External_ID__c],[rC_Bios__Contact__r:External_ID__c], rC_Bios__Value__c
			--  

			/*--DNC rC_Bios__Code_Value__c per email 03/18/2016	  CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ISNULL(UPPER(rC_Bios__Category__c),' ')+ISNULL(UPPER(rC_Bios__Subcategory__c),' ')+
							--ISNULL(UPPER(rC_Bios__Type__c),' ')+ISNULL(UPPER(rC_Bios__Subtype__c),' '))), 3, 50) AS NVARCHAR(50)) 
							--AS  rC_Bios__Code_Value__c
			*/
END

BEGIN--CHECK DUPLICATES
		SELECT DISTINCT * 
		FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE
		WHERE rC_Bios__External_ID__c IN (SELECT DISTINCT rC_Bios__External_ID__c FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE GROUP BY rC_Bios__External_ID__c HAVING COUNT(*)>1)
		ORDER BY rC_Bios__External_ID__c

    	SELECT DISTINCT * 
		FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE
		WHERE rC_Bios__External_ID__c IS NULL 

		SELECT DISTINCT * 
		FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE
		WHERE [rC_Bios__Account__r:External_ID__c] IS NULL AND [rC_Bios__Contact__r:External_ID__c] is NULL 


END
    

BEGIN--RecordType
	SELECT * FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE --WHERE RecordTypeID='0123B0000008UaZQAU' AND rC_Bios__Category__c!='Prospect information'
	WHERE RecordTypeID IS null
END 
 


BEGIN--CHECK FIELD LENGTH
	SELECT DISTINCT	 rC_Bios__Category__c, LEN(rC_Bios__Category__c) ln
	FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE
	WHERE LEN(rC_Bios__Category__c)>40
	ORDER BY ln DESC
	
	SELECT DISTINCT	 rC_Bios__SubCategory__c, LEN(rC_Bios__SubCategory__c) ln
	FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE
	WHERE LEN(rC_Bios__SubCategory__c)>40
	ORDER BY ln DESC
	
	SELECT DISTINCT	 rC_Bios__Type__c, LEN(rC_Bios__Type__c) ln
	FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE
	WHERE LEN(rC_Bios__Type__c)>40
	ORDER BY ln DESC
	
	SELECT DISTINCT	 rC_Bios__SubType__c, LEN(rC_Bios__SubType__c) ln
	FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE
	WHERE LEN(rC_Bios__SubType__c)>40
	ORDER BY ln DESC
	
END

BEGIN
		-- SELECT DISTINCT rC_Bios__External_ID__c, rC_Bios__Comments__c FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE WHERE rC_Bios__Comments__c LIKE '%"%'
		
		UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE SET rC_Bios__Comments__c=REPLACE(rC_Bios__Comments__c,'"','''') where rC_Bios__Comments__c like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE SET rC_Bios__Category__c=REPLACE(rC_Bios__Category__c,'"','''') where rC_Bios__Category__c like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE SET rC_Bios__Subcategory__c=REPLACE(rC_Bios__Subcategory__c,'"','''') where rC_Bios__Subcategory__c like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE SET rC_Bios__Type__c=REPLACE(rC_Bios__Type__c,'"','''') where rC_Bios__Type__c like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE SET rC_Bios__Subtype__c=REPLACE(rC_Bios__Subtype__c,'"','''') where rC_Bios__Subtype__c like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE SET rC_Bios__Value__c=REPLACE(rC_Bios__Value__c,'"','''') where rC_Bios__Value__c like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE SET rC_Bios__Status__c=REPLACE(rC_Bios__Status__c,'"','''') where rC_Bios__Status__c like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE SET Capacity_Rating__c=REPLACE(Capacity_Rating__c,'"','''') where Capacity_Rating__c like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE SET Emergency_Contact_Name__c=REPLACE(Emergency_Contact_Name__c,'"','''') where Emergency_Contact_Name__c like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE SET Emergency_Contact_Phone__c=REPLACE(Emergency_Contact_Phone__c,'"','''') where Emergency_Contact_Phone__c like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE SET Emergency_Contact_Relationship__c=REPLACE(Emergency_Contact_Relationship__c,'"','''') where Emergency_Contact_Relationship__c like '%"%'

		BEGIN 
			USE SUTTER_1P_DELTA 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'PREFERENCE'
 		END 
 
		--SHORTEN "BLACKBAUD ANALYTICS' CUSTOM MODELING SERVICES" to comply with the 40 char max
		UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE 
		SET rC_Bios__Type__c='Blackbaud Analytics'+''''+ ' CMS'
		WHERE rC_Bios__Category__c='Prospect information'	
		AND rC_Bios__Subcategory__c	='Rating'
		AND rC_Bios__Type__c LIKE 'Blackbaud Analytics%'
	 	 
		UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE 
		SET RC_BIOS__SUBTYPE__C ='A.Lucchetti Women'+''''+'s & Children'+''''+'s Center'
		WHERE RC_BIOS__SUBTYPE__C LIKE 'Anderson L%'

END		


BEGIN--UPDATE DATES

 	SELECT rC_Bios__Start_Date__c, LEN(rC_Bios__Start_Date__c) dl, COUNT(*) c
	,CASE WHEN LEN(rC_Bios__Start_Date__c) =4 THEN '01/01/'+rC_Bios__Start_Date__c 
		  WHEN LEN(rC_Bios__Start_Date__c) =7 THEN LEFT(rC_Bios__Start_Date__c,2)+'/01/'+RIGHT(rC_Bios__Start_Date__c,4) 
		  END AS nd
	FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE
	WHERE rC_Bios__Start_Date__c IS NOT NULL AND (LEN(rC_Bios__Start_Date__c) =4 OR LEN(rC_Bios__Start_Date__c) =7)
	GROUP BY rC_Bios__Start_Date__c
	ORDER BY dl
 
    UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE
	SET rC_Bios__Start_Date__c='01/01/'+rC_Bios__Start_Date__c 
	WHERE rC_Bios__Start_Date__c IS NOT NULL AND LEN(rC_Bios__Start_Date__c) =4

	UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE
	SET rC_Bios__Start_Date__c=LEFT(rC_Bios__Start_Date__c,2)+'/01/'+RIGHT(rC_Bios__Start_Date__c,4) 
	WHERE rC_Bios__Start_Date__c IS NOT NULL AND LEN(rC_Bios__Start_Date__c) =7

	UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE
	SET rC_Bios__End_Date__c='01/01/'+rC_Bios__End_Date__c 
	WHERE rC_Bios__End_Date__c IS NOT NULL AND LEN(rC_Bios__End_Date__c) =4

	UPDATE SUTTER_1P_MIGRATION.IMP.PREFERENCE
	SET rC_Bios__End_Date__c=LEFT(rC_Bios__End_Date__c,2)+'/01/'+RIGHT(rC_Bios__End_Date__c,4) 
	WHERE rC_Bios__End_Date__c IS NOT NULL AND LEN(rC_Bios__End_Date__c) =7

	SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE


				   
	SELECT [rC_Bios__Category__c], zrefRecSource , COUNT(*)
	FROM SUTTER_1P_DELTA.IMP.[PREFERENCE]
	--WHERE [rC_Bios__Category__c] LIKE '%ratin%'
	GROUP BY [rC_Bios__Category__c], zrefRecSource
	ORDER BY [rC_Bios__Category__c], zrefRecSource

	UPDATE SUTTER_1P_DELTA.IMP.[PREFERENCE]
	SET  rC_Bios__Category__c='Rating'
	WHERE rC_Bios__Category__c='Ratings'

	SELECT DISTINCT [rC_Bios__Start_Date__c] 
	FROM  SUTTER_1P_DELTA.IMP.[PREFERENCE]
	ORDER BY [rC_Bios__Start_Date__c] 

END

BEGIN--exceptions
 
 DROP TABLE SUTTER_1P_DELTA.IMP.[PREFERENCE_XCP]
 
	SELECT T1.* 
	INTO SUTTER_1P_DELTA.IMP.[PREFERENCE_XCP]
	FROM SUTTER_1P_DELTA.IMP.[PREFERENCE] T1
	LEFT JOIN SUTTER_1P_DELTA.XTR.PREFERENCE X1 ON T1.[rC_Bios__External_ID__c]=X1.[rC_Bios__External_ID__c]
	WHERE X1.[rC_Bios__External_ID__c] IS NULL 
	ORDER BY   [rC_Bios__Account__r:External_ID__c],[rC_Bios__Contact__r:External_ID__c], rC_Bios__Value__c

END 
    