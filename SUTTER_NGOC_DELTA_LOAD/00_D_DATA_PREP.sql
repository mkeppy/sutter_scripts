 
USE [SUTTER_1P_DELTA]																	  
GO
	 

BEGIN-- check dupe PrefAddr on HC_Constituent
	SELECT * FROM [SUTTER_1P_DELTA].dbo.[HC_Constituents_v]
	WHERE [RE_DB_Id] IN (SELECT [RE_DB_Id] FROM [SUTTER_1P_DELTA].dbo.[HC_Constituents_v] GROUP BY [RE_DB_Id] HAVING COUNT(*)>1)
	ORDER BY [RE_DB_Id]
END;
	 
BEGIN--PREFERRED ADDRESS 

		---DROP TABLE [SUTTER_1P_DELTA].TBL.Address_Pref
		DROP TABLE [SUTTER_1P_DELTA].TBL.Address_Pref

		SELECT T.RE_DB_OwnerShort, T.ImportID, T.ConsID, T.AddrPref, T.AddrImpID, T.AddrLine1, T.AddrLine2, T.AddrCity, T.AddrState, T.AddrZIP, T.AddrCountry,
				T.RE_DB_ID, T1.NEW_TGT_IMPORT_ID
		INTO [SUTTER_1P_DELTA].TBL.Address_Pref
		FROM [SUTTER_1P_DELTA].dbo.HC_Cons_Address_v T
		INNER JOIN [SUTTER_1P_DELTA].DBO.HC_Constituents_v T1 ON T.RE_DB_ID=T1.RE_DB_ID
		WHERE T.AddrPref='TRUE' AND (T.[AddrLine1] IS NOT NULL AND T.[AddrLine1]!='') AND (T.[AddrLine1] NOT LIKE '%deceased%' AND T.[AddrLine1]!='dec')
		-- (493 row(s) affected)   
 		
		BEGIN--check dupe
			SELECT RE_DB_ID, COUNT(*) C
			FROM [SUTTER_1P_DELTA].TBL.Address_Pref
			GROUP BY RE_DB_ID
			HAVING COUNT(*)>1
			ORDER BY C desc
		END 
		 
			
END
	 
BEGIN--CONSTITUENT SOLICIT CODES  -- sf_object 2 do not solicit codes

			-- SELECT * FROM [SUTTER_1P_DELTA].dbo.CHART_ConsSolicitCodes where sf_object_2 is not null
  			--create base table 
			DROP TABLE [SUTTER_1P_DELTA].TBL.SolicitCodes
		    
			SELECT * 
			INTO [SUTTER_1P_DELTA].dbo.[CHART_ConsSolicitCodes]
			FROM [SUTTER_1P_DATA].dbo.[CHART_ConsSolicitCodes] 

			 SELECT DISTINCT
				--	T.ImportID ,
				--	T.RE_DB_OwnerShort,
				--	T1.RE_DB_Id,
					T1.NEW_TGT_IMPORT_ID,
					MAX(T2.Do_Not_Phone__c) AS Do_Not_Phone__c ,
					MAX(T2.Do_Not_Solicit__c) AS Do_Not_Solicit__c ,
					MAX(T2.Do_Not_Mail__c) AS Do_Not_Mail__c ,
					MAX(T2.Do_Not_Contact__c) AS Do_Not_Contact__c 
					--T.SolicitCode oldSolicitCode
			 INTO   [SUTTER_1P_DELTA].TBL.SolicitCodes		--
			 FROM   [SUTTER_1P_DELTA].dbo.HC_Cons_SolicitCodes_v T
					INNER JOIN [SUTTER_1P_DELTA].dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
					INNER JOIN [SUTTER_1P_DELTA].dbo.CHART_ConsSolicitCodes T2 ON T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
																  AND T.SolicitCode = T2.SolicitCode
			 WHERE  T2.SF_Object_2 != ''  AND T2.[SF_Object_2] IS NOT null
			 GROUP BY --T.ImportID , 
					  --T.RE_DB_OwnerShort,
					  --T1.RE_DB_Id,
					  T1.NEW_TGT_IMPORT_ID
			 ORDER BY T1.NEW_TGT_IMPORT_ID
					--T.ImportID;  --(134 row(s) affected)
		
		BEGIN --check duplicates	
				SELECT NEW_TGT_IMPORT_ID, COUNT(*) c
				FROM [SUTTER_1P_DELTA].TBL.SolicitCodes
				GROUP BY NEW_TGT_IMPORT_ID
				HAVING COUNT(*) >1
		END;

		SELECT * FROM [SUTTER_1P_DELTA].TBL.SolicitCodes 

		SELECT * FROM [SUTTER_1P_DELTA].dbo.CHART_ConsSolicitCodes
END;
 

BEGIN--CONSTITUENT PHONES
 		-- SELECT COUNT(*) FROM [SUTTER_1P_DELTA].DBO.HC_Cons_Phone_v  --96260
 		-- SELECT * FROM [SUTTER_1P_DELTA].dbo.CHART_PhoneType
		--SELECT * FROM [SUTTER_1P_DELTA].dbo.HC_Cons_Phone_v 
			SELECT * 
			INTO [SUTTER_1P_DELTA].dbo.CHART_PhoneType
			FROM [SUTTER_1P_DATA].dbo.CHART_PhoneType 


		BEGIN
			DROP TABLE [SUTTER_1P_DELTA].TBL.Phones
			DROP TABLE [SUTTER_1P_DELTA].TBL.Phones_seq
			DROP TABLE [SUTTER_1P_DELTA].TBL.Phones_seq_1
			DROP TABLE [SUTTER_1P_DELTA].TBL.Phones_seq_2
			DROP TABLE [SUTTER_1P_DELTA].TBL.Phones_seq_1_final
			DROP TABLE [SUTTER_1P_DELTA].TBL.Phones_seq_2_final 
		END	
		 
		BEGIN--PHONES_1: REMOVE DEDUPLICATE PHONE NUMBER and create base table
				
 
				SELECT DISTINCT
						T.RE_DB_OwnerShort ,
						T.ConsID ,
						T.ImportID ,
						T.RE_DB_Id,
						T.NEW_TGT_IMPORT_ID,
						T.KeyInd ,
						T2.PhoneType ,
						T2.PhoneNum ,
						T3.SF_Object ,
						T3.SF_Field_API,
						T2.PhoneDoNotCall
				INTO    [SUTTER_1P_DELTA].TBL.Phones    -- select * from [SUTTER_1P_DELTA].TBL.Phones
				FROM    [SUTTER_1P_DELTA].dbo.HC_Constituents_v T
						INNER JOIN [SUTTER_1P_DELTA].dbo.HC_Cons_Phone_v T2 ON T.ImportID = T2.ImportID
																  AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
						INNER JOIN [SUTTER_1P_DELTA].dbo.HC_Cons_Address_v T4 ON T2.PhoneAddrImpID = T4.AddrImpID    
																  AND T2.RE_DB_OwnerShort = T4.RE_DB_OwnerShort
						INNER JOIN [SUTTER_1P_DELTA].dbo.CHART_PhoneType T3 ON T2.PhoneType = T3.PhoneType
																  AND T2.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
																  AND T.KeyInd = T3.KeyInd
				WHERE T4.AddrPref='TRUE'
				UNION ALL 
				SELECT DISTINCT   --code to set to true the do not call fields. this way, the pivot fnc won't create dupe rows. 
						T.RE_DB_OwnerShort ,
						T.ConsID ,
						T.ImportID ,
						T.RE_DB_Id,
						T.NEW_TGT_IMPORT_ID,
						T.KeyInd ,
						T2.PhoneType ,
						'TRUE' AS PhoneNum ,
						T3.SF_Object ,
						CASE WHEN T3.SF_Field_API='HomePhone' AND T2.PhoneDoNotCall='TRUE' THEN  'rC_Bios__Home_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='MobilePhone' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Mobile_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='rC_Bios__Work_Phone__c' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Work_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='OtherPhone' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Other_Do_Not_Call__c' ELSE NULL END AS SF_Field_API,
						T2.PhoneDoNotCall
				FROM    [SUTTER_1P_DELTA].dbo.HC_Constituents_v T
						INNER JOIN [SUTTER_1P_DELTA].dbo.HC_Cons_Phone_v T2 ON T.ImportID = T2.ImportID
																  AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
						INNER JOIN [SUTTER_1P_DELTA].dbo.HC_Cons_Address_v T4 ON T2.PhoneAddrImpID = T4.AddrImpID    
																  AND T2.RE_DB_OwnerShort = T4.RE_DB_OwnerShort
						INNER JOIN [SUTTER_1P_DELTA].dbo.CHART_PhoneType T3 ON T2.PhoneType = T3.PhoneType
																  AND T2.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
																  AND T.KeyInd = T3.KeyInd
				WHERE T4.AddrPref='TRUE' AND T2.PhoneDoNotCall='TRUE'
				
				UNION ALL -- FOR RECORDS FROM RE 7.95 (CVRX and WBRX)
				SELECT --DISTINCT
						T.RE_DB_OwnerShort ,
						T.ConsID ,
						T.ImportID ,
						T.RE_DB_Id,
						T.NEW_TGT_IMPORT_ID,
						T.KeyInd ,
						T2.PhoneType ,
						T2.PhoneNum ,
						T3.SF_Object ,
						T3.SF_Field_API,
						T2.PhoneDoNotCall
				FROM    [SUTTER_1P_DELTA].dbo.HC_Constituents_v T
						INNER JOIN [SUTTER_1P_DELTA].dbo.HC_Cons_Phone_v T2 ON T.ImportID = T2.ImportID
																  AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
						INNER JOIN [SUTTER_1P_DELTA].dbo.CHART_PhoneType T3 ON T2.PhoneType = T3.PhoneType
																  AND T2.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
																  AND T.KeyInd = T3.KeyInd
				WHERE [T2].[RE_DB_OwnerShort]='WBRX' OR [T2].[RE_DB_OwnerShort]='CVRX'
 
				UNION ALL 
				SELECT DISTINCT   --code to set to true the do not call fields. this way, the pivot fnc won't create dupe rows. 
						T.RE_DB_OwnerShort ,
						T.ConsID ,
						T.ImportID ,
						T.RE_DB_Id,
						T.NEW_TGT_IMPORT_ID,
						T.KeyInd ,
						T2.PhoneType ,
						'TRUE' AS PhoneNum ,
						T3.SF_Object ,
						CASE WHEN T3.SF_Field_API='HomePhone' AND T2.PhoneDoNotCall='TRUE' THEN  'rC_Bios__Home_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='MobilePhone' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Mobile_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='rC_Bios__Work_Phone__c' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Work_Do_Not_Call__c'
							 WHEN T3.SF_Field_API='OtherPhone' AND T2.PhoneDoNotCall='TRUE' THEN 'rC_Bios__Other_Do_Not_Call__c' ELSE NULL END AS SF_Field_API,
						T2.PhoneDoNotCall
				FROM    [SUTTER_1P_DELTA].dbo.HC_Constituents_v T
						INNER JOIN [SUTTER_1P_DELTA].dbo.HC_Cons_Phone_v T2 ON T.ImportID = T2.ImportID
																  AND T.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
						LEFT JOIN [SUTTER_1P_DELTA].dbo.CHART_PhoneType T3 ON T2.PhoneType = T3.PhoneType
																  AND T2.RE_DB_OwnerShort = T3.RE_DB_OwnerShort
																  AND T.KeyInd = T3.KeyInd
				WHERE T2.PhoneDoNotCall='TRUE' AND  [T2].[RE_DB_OwnerShort]='WBRX' OR [T2].[RE_DB_OwnerShort]='CVRX'
 
 
		END;   --(270 row(s) affected)
		
		BEGIN--PHONES_2: ASSIGN SEQUENCE and BASE TABLES 
				--PHONES where SEQUENCE >1 WILL BE ADDED in the same table WITH THE ADDITIONAL PHONE NUMBERS, as they'll be consolidated with in the same txt field. 
				--DO NOT INCLUDE ADDITIONAL PHONE NUMBER TYPES this time around. they'll be added later. 
			    
				SELECT 	
				T.RE_DB_OwnerShort, T.ConsID, T.ImportID, T.RE_DB_Id, T.NEW_TGT_IMPORT_ID, T.KeyInd, T.PhoneType, T.PhoneNum, T.SF_Object, T.SF_Field_API, T.PhoneDoNotCall
				,ROW_NUMBER() OVER ( PARTITION BY  T.KeyInd, T.NEW_TGT_IMPORT_ID,  T.SF_Object, T.SF_Field_API
										ORDER BY T.KeyInd, T.NEW_TGT_IMPORT_ID, T.PhoneType ) AS Seq 
				INTO [SUTTER_1P_DELTA].TBL.Phones_seq		   	
				FROM [SUTTER_1P_DELTA].TBL.Phones T 
				WHERE (T.SF_Field_API!='Additional_Phone_Numbers__c')   -- do not include additional phones
		
		
		END; --(245 row(s) affected)
			 
		BEGIN--PHONES_3: create base table for pivot tbl of PHONES where Seq =1
			
				SELECT * 
				INTO [SUTTER_1P_DELTA].TBL.Phones_seq_1		
				FROM [SUTTER_1P_DELTA].TBL.Phones_seq
				WHERE Seq='1'
				
 
		END; --(244 row(s) affected)
		
		BEGIN--PHONES_4: create base table for pivot tbl of ADDITIONAL phones;
			 --includes additional numbers where Seq>1  AND those mapped to additional_phone_numbers.
				
					SELECT  *
					INTO [SUTTER_1P_DELTA].TBL.Phones_seq_2    --all other additional phones.
					FROM    [SUTTER_1P_DELTA].TBL.Phones_seq
					WHERE   Seq != '1'						--includes numbers where seq>1
					UNION
					SELECT  * , '1' Seq
					FROM    [SUTTER_1P_DELTA].TBL.Phones
					WHERE   (SF_Field_API = 'Additional_Phone_Numbers__c')  --includes additional numbers. 
					
		END;--(2 row(s) affected)		
			
		BEGIN--PHONES_5: CREATE final pivot phone tables --PHONE TYPES from SF_Field_API
			  
			EXEC HC_PivotWizard_p   'NEW_TGT_IMPORT_ID',	--fields to include as unique identifier/normally it is just a unique ID field.
										'SF_Field_API',									--column that stores all new phone types
										'PhoneNum',										--phone numbers
										'[SUTTER_1P_DELTA].TBL.Phones_seq_1_final',			--INTO..     
										'[SUTTER_1P_DELTA].TBL.Phones_seq_1',					--FROM..
										'PhoneNum is not null'							--WHERE..
	 	END --(192 row(s) affected) 
        
			SELECT * FROM [SUTTER_1P_DELTA].TBL.Phones_seq_1_final 
			WHERE new_tgt_import_id IN (SELECT NEW_TGT_IMPORT_ID FROM [SUTTER_1P_DELTA].TBL.Phones_seq_1_final  GROUP BY NEW_TGT_IMPORT_ID HAVING COUNT(*)>1)
			ORDER BY NEW_TGT_IMPORT_ID
			 -- 0 records 

		BEGIN--PHONES_6: ADDITIONAL PHONE NUMBERS field
					SELECT DISTINCT T.NEW_TGT_IMPORT_ID,  
						STUFF(( SELECT ' '+ T1.PhoneType +': '+ T1.PhoneNum +'; ' + CHAR(10)
								FROM [SUTTER_1P_DELTA].TBL.Phones_seq_2 T1
									WHERE T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID AND T1.KeyInd=T.KeyInd
									ORDER BY T1.RE_DB_OwnerShort, T1.KeyInd, T1.ImportId
								FOR
									XML PATH('')
									  ), 1, 1, '') AS Additional_Phone_Numbers__c
					INTO [SUTTER_1P_DELTA].TBL.Phones_seq_2_final   
					FROM [SUTTER_1P_DELTA].TBL.Phones_seq_2 T
					ORDER BY T.NEW_TGT_IMPORT_ID
		END;--(2 row(s) affected)
						 
		 
				BEGIN--test for duplicates
							SELECT  NEW_TGT_IMPORT_ID ,
									COUNT(*) c
							FROM    [SUTTER_1P_DELTA].TBL.Phones_seq_2_final
							GROUP BY NEW_TGT_IMPORT_ID
							HAVING  COUNT(*) > 1
							ORDER BY NEW_TGT_IMPORT_ID
						 
				END;--0
		 			
END;					
 
  
BEGIN--CONSTITUENT ATTRIBUTES TO CONTACT 
		
		SELECT * FROM [SUTTER_1P_DELTA].dbo.CHART_Attributes T2 WHERE [T2].[SF_Object_2]='contact'
		
		DROP TABLE [SUTTER_1P_DELTA].TBL.Attribute_contact
		
		DROP TABLE [SUTTER_1P_DELTA].TBL.Attributes_to_Contact
		
			SELECT * 
			INTO [SUTTER_1P_DELTA].dbo.CHART_Attributes
			FROM [SUTTER_1P_DATA].dbo.CHART_Attributes 

		
		BEGIN 	
				--BASE TABLE CONS ATTR TO CONTACT  -- remove duplicates if present
				SELECT DISTINCT T.RE_DB_OwnerShort, T.ImportID, T1.RE_DB_Id, T1.NEW_TGT_IMPORT_ID, T2.SF_Object_2, T2.SF_Field_2, 
				CASE WHEN (T2.SF_Field_2='Agefinder_Birthdate__c' AND LEN(T.CAttrDesc)='6') THEN right(T.CAttrDesc,2)+'/'+left(T.CAttrDesc,4) 
					 WHEN (T2.SF_Field_2='Agefinder_Birthdate__c' AND LEN(T.CAttrDesc)='8' AND T.CAttrDesc NOT LIKE '%/%') 
						  THEN SUBSTRING(T.CAttrDesc, 5, 2)+'/'+right(T.CAttrDesc,2)+'/'+left(T.CAttrDesc,4) 
					 ELSE T.CAttrDesc END AS SF_FieldValue ,T.CAttrDesc refCAttrDesc ,LEN(T.CAttrDesc) refFieldLenght
				
				INTO [SUTTER_1P_DELTA].TBL.Attribute_contact
				FROM [SUTTER_1P_DELTA].dbo.HC_Cons_Attributes_v T
				INNER JOIN [SUTTER_1P_DELTA].DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				LEFT JOIN [SUTTER_1P_DELTA].dbo.CHART_Attributes T2 ON T.CAttrCat=T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T1.KeyInd='I' AND T2.SF_Object_2='CONTACT' AND T.CAttrDesc IS NOT null
				 
		 
		END; 
		--(0 row(s) affected)
	
		BEGIN
        
			DROP TABLE [SUTTER_1P_DELTA].TBL.Attribute_contact_b
			SELECT NEW_TGT_IMPORT_ID, SF_Object_2, SF_Field_2, MAX(SF_FieldValue) AS SF_FieldValue
			INTO [SUTTER_1P_DELTA].TBL.Attribute_contact_b
			FROM [SUTTER_1P_DELTA].TBL.Attribute_contact
			GROUP BY NEW_TGT_IMPORT_ID, SF_Object_2, SF_Field_2
 		END	 --(0 row(s) affected)
        
		BEGIN--ATTRIBUTE to CONTACT - MULTIPICKLIST

				DROP TABLE [SUTTER_1P_DELTA].TBL.Attributes_to_Contact 

				SELECT DISTINCT T.NEW_TGT_IMPORT_ID,
					STUFF(( SELECT '; ' + T1.SF_FieldValue 
							FROM [SUTTER_1P_DELTA].TBL.Attribute_contact T1
								WHERE T1.SF_Field_2='Agefinder_Birthdate__c' AND T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID
								GROUP BY T1.SF_FieldValue
							FOR
								XML PATH('')
								  ), 1, 1, '') AS Agefinder_Birthdate__c
								  
					,STUFF(( SELECT '; ' + T1.SF_FieldValue 
							FROM [SUTTER_1P_DELTA].TBL.Attribute_contact T1
								WHERE T1.SF_Field_2='CMS_Age__c' AND T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID
								GROUP BY T1.SF_FieldValue
							FOR
								XML PATH('')
								  ), 1, 1, '') AS CMS_Age__c
					,STUFF(( SELECT '; ' + T1.SF_FieldValue 
							FROM [SUTTER_1P_DELTA].TBL.Attribute_contact T1
								WHERE T1.SF_Field_2='PhoneFinder__c' AND T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID
								GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
							FOR
								XML PATH('')
								  ), 1, 1, '') AS PhoneFinder__c
					,STUFF(( SELECT '; ' + T1.SF_FieldValue 
							FROM [SUTTER_1P_DELTA].TBL.Attribute_contact T1
								WHERE T1.SF_Field_2='SMCF_Employee_ID__c' AND T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID
								GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
							FOR
								XML PATH('')
								  ), 1, 1, '') AS SMCF_Employee_ID__c
					,STUFF(( SELECT '; ' + T1.SF_FieldValue 
							FROM [SUTTER_1P_DELTA].TBL.Attribute_contact T1
								WHERE T1.SF_Field_2='SAFH_Employee_ID__c' AND T1.NEW_TGT_IMPORT_ID=T.NEW_TGT_IMPORT_ID
								GROUP BY T1.SF_FieldValue ORDER BY T1.SF_FieldValue DESC
							FOR
								XML PATH('')
								  ), 1, 1, '') AS SAFH_Employee_ID__c
		 		INTO [SUTTER_1P_DELTA].TBL.Attributes_to_Contact 
				FROM [SUTTER_1P_DELTA].TBL.Attribute_contact_b T
				ORDER BY T.NEW_TGT_IMPORT_ID


		END;--(0 row(s) affected)
		
						 
		BEGIN--CHECK DUPLICATES
			SELECT *
			FROM [SUTTER_1P_DELTA].TBL.Attributes_to_Contact 
			WHERE NEW_TGT_IMPORT_ID IN (SELECT NEW_TGT_IMPORT_ID FROM [SUTTER_1P_DELTA].TBL.Attributes_to_Contact  GROUP BY NEW_TGT_IMPORT_ID HAVING COUNT(*)>1)
			ORDER BY NEW_TGT_IMPORT_ID
		END; --0  
 
 END   
  


BEGIN--CONSTITUENT ACTION SOLICITOR TO MULTI-PICKLIST
 
		 DROP TABLE [SUTTER_1P_DELTA].TBL.Cons_Action_Solicitor
		 DROP TABLE [SUTTER_1P_DELTA].TBL.Cons_Action_Solicitor_1


		BEGIN--BASE TABLE	
			SELECT DISTINCT T.RE_DB_OwnerShort, T.ACImpID, T1.ImportID, T1.Name, T1.[NEW_TGT_IMPORT_ID]
		 	INTO [SUTTER_1P_DELTA].TBL.Cons_Action_Solicitor
			FROM [SUTTER_1P_DELTA].dbo.HC_Cons_Action_Solicitor_v T
			LEFT JOIN [SUTTER_1P_DELTA].dbo.HC_Constituents_v_final T1 ON T.ACSolImpID=T1.ImportID AND  T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			ORDER BY T.RE_DB_OwnerShort, T.ACImpID, T1.ImportID
		END; --(61 row(s) affected)

		
		BEGIN--MULTI-SELECT PICKLIST
			SELECT DISTINCT T.RE_DB_OwnerShort, T.ACImpID
				,STUFF(( SELECT '; ' + T1.Name 
						FROM [SUTTER_1P_DELTA].TBL.Cons_Action_Solicitor T1
							WHERE T1.ACImpID=T.ACImpID AND T1.RE_DB_OwnerShort=T.RE_DB_OwnerShort 
							GROUP BY T1.Name ORDER BY T1.Name DESC
						FOR
							XML PATH('')
							  ), 1, 1, '') AS RE_Action_Solicitor__c
			INTO [SUTTER_1P_DELTA].TBL.Cons_Action_Solicitor_1 -- select * from SUTTER_1P_MIGRATION.TBL.Cons_Action_Solicitor_1
			FROM [SUTTER_1P_DELTA].TBL.Cons_Action_Solicitor T
			ORDER BY T.RE_DB_OwnerShort, T.ACImpID
		END; --(50 row(s) affected)
		
		BEGIN 
			UPDATE [SUTTER_1P_DELTA].TBL.Cons_Action_Solicitor_1  SET RE_Action_Solicitor__c=LTRIM(RE_Action_Solicitor__c) 
		END	 --(50 row(s) affected)
		
		SELECT * FROM  [SUTTER_1P_DELTA].TBL.Cons_Action_Solicitor_1
		
END;

----GIFTS*******************************************************
   
BEGIN--Gift Attribute Employee giving  (these gifts will be created as one-time giving (ea payment will be created as its own parent with transaction) 

	DROP TABLE [SUTTER_1P_DELTA].TBL.GiftAttr_employee_recurring

	SELECT DISTINCT T1.[GFImpID], T1.[GFAttrCat], T1.[GFAttrDesc], T2.[GFType], T2.[RE_DB_OwnerShort]
	INTO [SUTTER_1P_DELTA].TBL.GiftAttr_employee_recurring
	FROM [SUTTER_1P_DELTA].dbo.[HC_Gift_Attr_v] T1
	INNER JOIN [SUTTER_1P_DELTA].dbo.HC_gift_v T2 ON T1.[GFImpID]=T2.GFImpID AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
	WHERE GFAttrCat='Employee Payroll' AND [GFAttrDesc]='Recurring Gift Pay-Cash'
 	--(2453 row(s) affected)

	SELECT [GFType], COUNT(*) 
	FROM [SUTTER_1P_DELTA].TBL.GiftAttr_employee_recurring 
	GROUP BY [GFType]
	ORDER BY [GFType]

	DELETE [SUTTER_1P_DELTA].TBL.GiftAttr_employee_recurring  
    WHERE [GFType]='Pay-Cash'	 --INCORRECT GIFT TYPE CODED 

	SELECT COUNT(*) 
	FROM [SUTTER_1P_DELTA].TBL.GiftAttr_employee_recurring
	--2,394


	SELECT * FROM [SUTTER_1P_DELTA].TBL.GiftAttr_employee_recurring
	SELECT * FROM [SUTTER_1P_DELTA].dbo.[HC_Gift_Attr_v]


	
END;  

 	   

BEGIN--GIFTS: LEGACY FINANCIAL CODES 

		 DROP TABLE [SUTTER_1P_DELTA].TBL.GiftLegacyFinancialCodes		
		
 
		--BASE TABLE
		
		SELECT T.RE_DB_OwnerShort, T.GSplitImpID, T.GSplitFund, T.GSplitCamp, T.GSplitAppeal, T.GSplitPkg,
		[RE_Legacy_Financial_Code__c]=	CASE WHEN T.GSplitFund IS NOT NULL THEN COALESCE('Fund: ' + LTRIM(RTRIM(T.GSplitFund)), '') ELSE 'N/A' END + CHAR(10) +
										CASE WHEN T.GSplitCamp IS NOT NULL THEN COALESCE('; Campaign: ' + LTRIM(RTRIM(T.GSplitCamp)), '') ELSE 'N/A' END + CHAR(10) +
										CASE WHEN T.GSplitAppeal IS NOT NULL THEN COALESCE('; Appeal: ' + LTRIM(RTRIM(T.GSplitAppeal)), '') ELSE 'N/A' END + CHAR(10) +
										CASE WHEN T.GSplitPkg IS NOT NULL THEN COALESCE('; Package: ' + LTRIM(RTRIM(T.GSplitPkg)), '') ELSE 'N/A' END + CHAR(10)
		INTO [SUTTER_1P_DELTA].TBL.GiftLegacyFinancialCodes								
		FROM [SUTTER_1P_DELTA].dbo.HC_Gift_SplitGift_v T
		 --(9879 row(s) affected)
		 
		--UPDATE - REPLACE "NULL" WITH "N/A"
		SELECT * FROM [SUTTER_1P_DELTA].TBL.GiftLegacyFinancialCodes WHERE [RE_Legacy_Financial_Code__c] LIKE '%NULL%'
		
		UPDATE [SUTTER_1P_DELTA].TBL.GiftLegacyFinancialCodes 
			SET [RE_Legacy_Financial_Code__c] =REPLACE([RE_Legacy_Financial_Code__c] ,'NULL','N/A') 
			WHERE [RE_Legacy_Financial_Code__c] LIKE '%NULL%'
 		 --(6400 row(s) affected)
		
		SELECT [RE_Legacy_Financial_Code__c], LEN([RE_Legacy_Financial_Code__c] ) l FROM [SUTTER_1P_DELTA].TBL.GiftLegacyFinancialCodes  ORDER BY l desc
		--len 108

END;

BEGIN--GIFTS: TOTAL PAYMENT AMT (current giving amt) 
	--DROP TABLE [SUTTER_1P_DELTA].TBL.TotalPaymentAmt
	 
	DROP TABLE [SUTTER_1P_DELTA].TBL.TotalPaymentAmt	

	SELECT RE_DB_OwnerShort, GFType, GFImpId, SUM(GFPaymentAmount) TotalPaymentAmt, COUNT(*) TotalNumberPayments
	INTO [SUTTER_1P_DELTA].TBL.TotalPaymentAmt	
	FROM [SUTTER_1P_DELTA].dbo.HC_Gift_Link_v
	GROUP BY RE_DB_OwnerShort, GFType, GFImpId
	ORDER BY RE_DB_OwnerShort, GFType, GFImpId
	--(57356 row(s) affected)
	
END;		 
       

BEGIN--GIFTS: SPLITS (add RE_Split_Gift__c checkbox on opportunities created from gifts with splits)
		DROP TABLE [SUTTER_1P_DELTA].TBL.GIFT_SPLIT

		SELECT RE_DB_OwnerShort, GFImpID, COUNT(*) CountOFSplits, 'TRUE' AS RE_Split_Gift__c 
		INTO [SUTTER_1P_DELTA].TBL.GIFT_SPLIT
		FROM [SUTTER_1P_DELTA].dbo.HC_Gift_SplitGift_v
		GROUP BY RE_DB_OwnerShort, GFImpID 
		HAVING COUNT(*)>1 
		
END; ---(438 row(s) affected)


BEGIN--GIFTS: WRITE OFF 
	 
			DROP TABLE [SUTTER_1P_DELTA].TBL.GIFT_WRITEOFF    
			
			SELECT	DISTINCT 
					T4.HH_ImportID
				    ,T4.NoHH_ImportID
					,CASE WHEN T4.NoHH_ImportID IS NOT NULL THEN T4.HH_ImportID ELSE T2.[NEW_TGT_IMPORT_ID] END AS [ACCOUNT:External_Id__c]
					,T1.*
		    INTO [SUTTER_1P_DELTA].TBL.GIFT_WRITEOFF    
		 	FROM [SUTTER_1P_DELTA].dbo.HC_Gift_WriteOff_v T1
			INNER JOIN [SUTTER_1P_DELTA].dbo.HC_Constituents_v T2 ON T1.ImportID=T2.ImportID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN [SUTTER_1P_DELTA].tbl.Contact_HofH AS T4 ON T2.[NEW_TGT_IMPORT_ID]=T4.NoHH_ImportID  
			--(0 row(s) affected)
END 
  
BEGIN--GIFT ATTRIBUTES-CHART_Attribute

		BEGIN	
			 DROP TABLE [SUTTER_1P_DELTA].TBL.Attribute_opportunity_gift 
			 DROP TABLE [SUTTER_1P_DELTA].TBL.Attribute_opportunity_gift_1
	 	END;

	
		BEGIN--GIFT ATTRIBUTE-BASE TABLE 
			SELECT DISTINCT T.RE_DB_OwnerShort, T.GFImpID,  T2.SF_Object_2, T2.SF_Field_2, 
			CASE WHEN T3.Name IS NOT NULL THEN T3.Name ELSE t.GFAttrDesc END AS SF_FieldValue, T.GFAttrDesc AS refCAttrDesc, 
			LEN(T.GFAttrDesc) refFieldLength 
			INTO [SUTTER_1P_DELTA].TBL.Attribute_opportunity_gift
			FROM [SUTTER_1P_DELTA].dbo.HC_Gift_Attr_v T
			INNER JOIN [SUTTER_1P_DELTA].dbo.CHART_Attributes T2 ON T.GFAttrCat =T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN [SUTTER_1P_DELTA].DBO.HC_Constituents_v T3 ON T.GFAttrDesc=T3.ConsID AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
			WHERE T2.SF_Object_2='OPPORTUNITY'  
			ORDER BY T.RE_DB_OwnerShort, T.GFImpID, T2.SF_Field_2
		END;  --(273 row(s) affected)

			SELECT * FROM [SUTTER_1P_DELTA].TBL.Attribute_opportunity_gift


		BEGIN--FINAL TABLE
			EXEC HC_PivotWizard_p   'RE_DB_OwnerShort, GFImpID',				--fields to include as unique identifier/normally it is just an ID field.
						'SF_Field_2',									--column that stores all new phone types
						'SF_FieldValue',								--phone numbers
						'[SUTTER_1P_DELTA].TBL.Attribute_opportunity_gift_1',	--INTO..     
						'[SUTTER_1P_DELTA].TBL.Attribute_opportunity_gift',	--FROM..
						'SF_FieldValue is not null'						--WHERE..
					 
		END; --(102 row(s) affected)
	
	 
	SELECT * FROM [SUTTER_1P_DELTA].TBL.Attribute_opportunity_gift_1
		
	 
END; 

BEGIN--GIFT ATTRIBUTE-CHART_GiftAttribute
	
	BEGIN
		SELECT * FROM [SUTTER_1P_DELTA].DBO.CHART_GiftAttribute		 
		SELECT * FROM [SUTTER_1P_DELTA].TBL.Gift_Attribute_1 

		DROP TABLE [SUTTER_1P_DELTA].TBL.Gift_Attribute	
		DROP TABLE [SUTTER_1P_DELTA].TBL.Gift_Attribute_1 
	
	END

	BEGIN--BASE TABLE 	
		SELECT DISTINCT
		T1.RE_DB_OwnerShort, T1.GFImpID, T1.GFAttrCat, T1.GFAttrDesc, T1.GFAttrDate, T1.GFAttrCom, [T2].[SF_Object], [T2].[SF_Field_Name], T2.[SF_Field_Value]
		INTO [SUTTER_1P_DELTA].TBL.Gift_Attribute		
		FROM [SUTTER_1P_DELTA].dbo.HC_Gift_Attr_v T1
		INNER JOIN [SUTTER_1P_DELTA].DBO.CHART_GiftAttribute T2 ON T1.GFAttrCat=T2.GFAttrCat AND T1.GFAttrDesc=T2.GFAttrDesc AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
		WHERE T2.[Convert]='Yes'
	END; --(2608 row(s) affected)

	BEGIN--FINAL TABLE
			EXEC HC_PivotWizard_p   'RE_DB_OwnerShort, GFImpID',					--fields to include as unique identifier/normally it is just a unique ID field.
									'SF_Field_Name',								--column that stores all new phone types
									'SF_Field_Value',								--phone numbers
									'[SUTTER_1P_DELTA].TBL.Gift_Attribute_1',	    --INTO..     
									'[SUTTER_1P_DELTA].TBL.Gift_Attribute',		--FROM..
									'SF_Field_Value is not null'					--WHERE..
	
	END;--(149 row(s) affected)

	
	--check dupes
			SELECT RE_DB_OwnerShort, GFImpID, COUNT(*) c 
			FROM [SUTTER_1P_DELTA].TBL.Gift_Attribute_1 
	 		GROUP BY RE_DB_OwnerShort, GFImpID
			HAVING COUNT(*)>1
			ORDER BY c DESC
			 -- 0 
			SELECT * FROM [SUTTER_1P_DELTA].TBL.Gift_Attribute_1 
END;
  
BEGIN --FIRST PAYMENT DATE TO BE POPULATED ON CLOSEDATE OF PARENT OPP MIGRATED FROM PLEDGE/RECURRIGN GIFTS
			DROP TABLE [SUTTER_1P_DELTA].TBL.Gift_FirstPaymentDate

		--FIRST PAYMENT DATE TO BE POPULATED ON CLOSEDATE OF PARENT OPP MIGRATED FROM PLEDGE/RECURRIGN GIFTS
			SELECT		[T1].[RE_DB_OwnerShort], T1.GFType, T1.GFImpID, [T2].GFDate, 
						MIN(T3.GFDate) AS GFFirstPaymentDate
			INTO		[SUTTER_1P_DELTA].TBL.Gift_FirstPaymentDate
			FROM        dbo.HC_Gift_Link_v AS T1 
						LEFT JOIN dbo.HC_Gift_v AS T2 ON T1.GFImpID = T2.GFImpID AND T1.RE_DB_OwnerShort = T2.RE_DB_OwnerShort
						LEFT JOIN dbo.HC_Gift_v AS T3 ON T1.GFPaymentGFImpID = T3.GFImpID AND T1.RE_DB_OwnerShort = T3.RE_DB_OwnerShort  
			GROUP BY	T1.[RE_DB_OwnerShort],T1.GFType, T1.GFImpID, [T2].GFDate			
			ORDER BY T1.[RE_DB_OwnerShort], T1.GFType, T1.GFImpID

		--check dupes
			SELECT [T1].[RE_DB_OwnerShort] +'-'+T1.GFImpID AS RE_DB_ID_Gift, COUNT(*) c
			FROM [SUTTER_1P_DELTA].TBL.Gift_FirstPaymentDate AS T1
			GROUP BY [T1].[RE_DB_OwnerShort] +'-'+T1.GFImpID 
			ORDER BY c desc
			--(57356 row(s) affected)
				SELECT *
				FROM [SUTTER_1P_DELTA].TBL.Gift_FirstPaymentDate

END	

BEGIN --TBL GAU
				DROP TABLE  [SUTTER_1P_DELTA].TBL.GAU
				
				
				SELECT
					dbo.fnc_OwnerID() AS OwnerID
					,T.[GAU External ID] AS rC_Giving__External_ID__c
					,T.GAU_Name AS Name
					,T.GAU_Acknowledgement_Text__c AS Acknowledgement_Text__c
					,T.GAU_IsRestricted AS IsRestricted__c
					,T.GAU_Region__c AS Region__c
					,T.GAU_Affiliation AS Affiliation__c
					,T.GAU_Restriction_Type__c AS Restriction_Type__c
					,MAX(CAST(T1.FundNote AS VARCHAR(MAX))) AS Comments__c
					,MAX(T1.FundStartDate) AS Start_Date__c
					,MAX(T1.FundEndDate) AS End_Date__c
  					,CASE WHEN T1.RE_DB_OwnerShort='PAMF' THEN T1.FundCategory  ELSE NULL END AS Campus_Name__c	 
  				 	--ref
					,MAX(T.[FundID]) AS zrefFundId
					,T.[RE_DB_OwnerShort] AS zrefREOwner
 				INTO [SUTTER_1P_DELTA].TBL.GAU
				FROM SUTTER_1P_DATA.dbo.CHART_Fund AS T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Fund_v T1 ON T.FundID=T1.FundID AND T.RE_DB_OwnerShort=T1.[RE_DB_OwnerShort]
  				WHERE T.[GAU External ID] IS NOT NULL
				GROUP BY   T.RE_DB_OwnerShort, T.[GAU External ID] , T1.RE_DB_OwnerShort 
					,T.GAU_Name  
					,T.GAU_Acknowledgement_Text__c  
					,T.GAU_IsRestricted  
					,T.GAU_Region__c 
					,T.GAU_Affiliation  
					,T.GAU_Restriction_Type__c  
			 		,T1.FundCategory  
	  			ORDER BY T.[GAU External ID] 
END; 			--(4995 row(s) affected) 

BEGIN--CAMPAIGN
		DROP TABLE [SUTTER_1P_DELTA].TBL.CAMPAIGN 
		
		SELECT * 
		INTO [SUTTER_1P_DELTA].TBL.CAMPAIGN
		FROM [SUTTER_1P_MIGRATION].TBL.[CAMPAIGN]

END 

BEGIN --DELETE --GIFTS: BASE TABLE 
			DROP TABLE [SUTTER_1P_DELTA].TBL.GIFT
END 


/****************CHECK CORRECT CONSTITUENT IS BECOMING THE ACCOUNT

		SELECT T1.[GFImpID], T1.[RE_DB_Id], 
		CASE WHEN T22.[NEW_TGT_IMPORT_ID] IS NOT NULL THEN 'NEWCONS'+T22.[NEW_TGT_IMPORT_ID] 
			 WHEN T21.NoHH_ImportID IS NOT NULL THEN T21.HH_ImportID ELSE T20.[NEW_TGT_IMPORT_ID]  END AS [ACCOUNT:External_Id__c] ,
		
		T20.[NEW_TGT_IMPORT_ID] AS new_cons_20,
		'NEWCONS'+T22.[NEW_TGT_IMPORT_ID] AS new_cons_22
		FROM [SUTTER_1P_DELTA].DBO.HC_Gift_SplitGift_v AS T
			INNER JOIN [SUTTER_1P_DELTA].DBO.HC_Gift_v AS T1 ON T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort AND T.GFImpID=T1.GFImpID
			LEFT JOIN [SUTTER_1P_DELTA].dbo.HC_Constituents_v_final T20 ON T1.[RE_DB_Id]=T20.[RE_DB_Id] 
			LEFT JOIN [SUTTER_1P_DELTA].tbl.Contact_HofH_final_conv AS T21 ON T20.[NEW_TGT_IMPORT_ID]=T21.[NoHH_ImportID]  
			LEFT JOIN [SUTTER_1P_DELTA].dbo.HC_Constituents_v T22 ON T1.[RE_DB_Id]=T22.[RE_DB_Id] 
*/	 
 		  

BEGIN--CREATE GIFTS: BASE TABLE 
	 		DROP TABLE [SUTTER_1P_DELTA].TBL.GIFT 

			SELECT DISTINCT 
			--constituent 
				T20.KeyInd, T20.Name,
				CASE WHEN T22.[NEW_TGT_IMPORT_ID] IS NOT NULL THEN T22.[NEW_TGT_IMPORT_ID] 
				 WHEN T21.NoHH_ImportID IS NOT NULL THEN T21.HH_ImportID ELSE T20.[NEW_TGT_IMPORT_ID]  END AS [ACCOUNT:External_Id__c] ,
			--GSplitGift
				T.GSplitImpID, T.GFImpID GSplitGFImpID, T.GFSplitSequence, T.GSplitAmt, T.GSplitFund, T.GSplitCamp, T.GSplitAppeal, T.GSplitPkg, 
				T.RE_DB_OwnerShort GSplitRE_DB_OwnerShort, T.RE_DB_Tbl GSplitRE_DB_Tbl,
				T.RE_DB_OwnerShort+'-'+T.GSplitImpID AS RE_GFSplitImportID_ID,
			--Gift
				T1.BATCH_NUMBER, T1.GFDate, T1.GFType, T1.GFTAmt, T1.GFPledgeBalance, T1.GFStatus, T1.GFAck, T1.GFAckDate, T1.GFAnon, T1.GFConsDesc,
				CASE WHEN LEN(T1.GFCheckDate)=10 THEN T1.GFCheckDate END AS GFCheckDate,
				T1.GFCheckNum, T1.ImportID, T1.GFPayMeth, T1.GFRef, T1.GFLtrCode,
				T1.GFStkIss, T1.GFStkMedPrice, T1.GFStkNumUnits, T1.GFStkSymbol, T1.GFSubType, 
				T1.GFInsYears, T1.GFInsEndDate, T1.GFInsFreq, T1.GFInsNumPay, T1.GFInsStartDate, T1.DateAdded
			--GAU
				,T2.rC_Giving__External_ID__c AS GAU_External_Id__c		--rC_Giving__GAU__c
--			--Campaign
				,T3.PRIMARY_CAMPAIGN_SOURCE AS CAMPAIGN_External_Id__c	--CampaignId
				,T3.[rC_Giving_Fundraising_Program__c] AS Fundraising_Program__c
				,T3.rC_Giving__Affiliation__c
			--GiftTpe
				,T4.Parent_Giving_RecordType, T4.rC_Giving__Activity_Type__c, T4.rC_Giving__Is_Giving__c, T4.rC_Giving__Is_Giving_Donation__c	
				,T4.rC_Giving__Is_Giving_Inkind__c, T4.rC_Giving__Is_Sustainer__c, T4.Child_Giving_RecordType
				,T4.rC_Giving__Is_Giving_Transaction__c, T4.Transaction_Source
			--GSubType
				,T5.Production__c
				,T5.RE_Gift_Subtype__c
			--PaymentMethod
				,T6.rC_Giving__Payment_Type__c
				,T6.External_Id__c PAYMENT_METHOD_External_ID__c
				,T6.ACCOUNT_External_ID__c AS PAYMENT_METHOD_ACCOUNT_External_ID__c
			--attributes 
				--(chart-attributes; type = gift)
					,NULL AS DAF_Pledge_Payment__c
					,T7.Donation_Page_Name__c
					,T7.Donor_Recognition_Name__c
					,T7.Gift_Comments__c
					,NULL AS NCHX_Source_Entity__c
					,NULL AS  Non_Donation_Payment__c
					,NULL AS Original_Pledge_Amount__c
					,T7.Plaque_Wording__c
					,NULL AS Research_Payment__c
					,T7.Transaction_ID__c
					,NULL AS Tribute_In_Honor_of__c
					,NULL AS Tribute_In_Memory_of__c

			--legacy codes
				,T9.RE_Legacy_Financial_Code__c
			--frequency
				,T10.rC_Giving__Giving_Frequency__c
			--rC_Giving__Current_Giving_Amount__c
				,CASE WHEN T11.TotalPaymentAmt IS NOT NULL THEN T11.TotalPaymentAmt ELSE T.GSplitAmt END AS rC_Giving__Current_Giving_Amount__c
 	  
			--PLANNED GIFT field
			      ,T1.[GFPlannedGiftID]
				  ,T1.[GFDiscountRt]
				  ,T1.[GFMaturityYr]
				  ,T1.[GFFlexibleDef]
				  ,T1.[GFTRealized]
				  ,T1.[GFVehicle]
				  ,T1.[GFNumUnits]
				  ,T1.[GFPayoutAmt]
				  ,T1.[GFPayoutPct]
				  ,T1.[GFPIFName]
				  ,T1.[GFPGStatus]
				  ,T1.[GFRemainderAsOfDate]
				  ,T1.[GFRemainderValue]
				  ,T1.[GFPPolicyType]
				  ,T1.[GFPPolicyNum]
				  ,T1.[GFPPolicyFaceAmt]
				  ,T1.[GFPPolicyPremium]
				  ,T1.[GFNetPresentValue]
				  ,T1.[GFNetPresentValueAsOf]
				  ,T1.[GFRevocable]
				  ,T1.[GFTermEndDt]
				  ,T1.[GFTermType]
				  ,T1.[GFPIFTotalUnits]
				  ,T1.[GFTrustTaxID]
 
			--MISC.
				,T15.RE_Split_Gift__c
				,T16.GiftSubType__c
				,T1.GFPostDate AS GFPostDate__c 
				,T1.GFPostStat AS GFPostStatus__c
	 		--ref
				,T1.GFImpID AS refGFImpID
				,T1.ImportID AS refOldImportId
				,T20.[NEW_TGT_IMPORT_ID] AS refNewImportId
				,T21.HH_ImportID 
				,T21.NoHH_ImportID
				,T22.[NEW_TGT_IMPORT_ID] newdeltaImportID
	  
	  		INTO [SUTTER_1P_DELTA].TBL.GIFT
			FROM [SUTTER_1P_DELTA].DBO.HC_Gift_SplitGift_v AS T
				INNER JOIN [SUTTER_1P_DELTA].DBO.HC_Gift_v AS T1 ON T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort AND T.GFImpID=T1.GFImpID
			--constituent
				LEFT JOIN [SUTTER_1P_DELTA].dbo.HC_Constituents_v_final T20 ON T1.[RE_DB_Id]=T20.[RE_DB_Id] 
				LEFT JOIN [SUTTER_1P_DELTA].tbl.Contact_HofH_final_conv AS T21 ON T20.[NEW_TGT_IMPORT_ID]=T21.[NoHH_ImportID]  
				LEFT JOIN [SUTTER_1P_DELTA].dbo.HC_Constituents_v T22 ON T1.[RE_DB_Id]=T22.[RE_DB_Id] 
	 		--gau/camp
				LEFT JOIN [SUTTER_1P_DELTA].TBL.GAU AS T2 ON T.RE_DB_OwnerShort=T2.zrefREOwner AND T.GSplitFund=T2.zrefFundId 
				LEFT JOIN [SUTTER_1P_DELTA].TBL.CAMPAIGN AS T3 ON T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort 
															AND T.GSplitFund=T3.FundId
															AND T.GSplitCamp=T3.CampId 
															AND T.GSplitAppeal=T3.AppealId 
															AND T.GSplitPkg=T3.PackageId
 				LEFT JOIN [SUTTER_1P_DELTA].dbo.CHART_GiftType AS T4 ON T1.GFType=T4.GFType 
				LEFT JOIN [SUTTER_1P_DELTA].dbo.CHART_GiftSubType AS T5 ON T1.RE_DB_OwnerShort=T5.RE_DB_OwnerShort AND T1.GFSubType=T5.GFSubType  
			--PAYMETHOD		
				LEFT JOIN [SUTTER_1P_DELTA].TBL.PAYMENT_METHOD AS T6 ON T1.RE_DB_OwnerShort=T6.RE_DB_OwnerShort  AND T1.GFImpID=T6.GFImpID 
								AND (CASE WHEN T22.[NEW_TGT_IMPORT_ID] IS NOT NULL THEN T22.[NEW_TGT_IMPORT_ID] 
										  WHEN T21.NoHH_ImportID IS NOT NULL THEN T21.HH_ImportID 
										  ELSE T20.[NEW_TGT_IMPORT_ID]  END)=T6.[ACCOUNT_External_ID__c]
	 		--misc
				LEFT JOIN [SUTTER_1P_DELTA].TBL.Attribute_opportunity_gift_1 AS T7 ON T1.RE_DB_OwnerShort=T7.RE_DB_OwnerShort AND T1.GFImpID=T7.GFImpID  ---chart_attribute
				LEFT JOIN [SUTTER_1P_DELTA].TBL.GiftLegacyFinancialCodes AS T9 ON T.RE_DB_OwnerShort=T9.RE_DB_OwnerShort AND T.GSplitImpID=T9.GSplitImpID
				LEFT JOIN [SUTTER_1P_DELTA].DBO.CHART_Pledge_Frequency AS T10 ON T1.GFInsFreq = T10.GFInsFreq
				LEFT JOIN [SUTTER_1P_DELTA].TBL.TotalPaymentAmt AS  T11  ON T1.RE_DB_OwnerShort=T11.RE_DB_OwnerShort AND T1.GFImpID=T11.GFImpID
				LEFT JOIN [SUTTER_1P_DELTA].TBL.GIFT_SPLIT AS T15 ON T.GFImpID=T15.GFImpID AND T.RE_DB_OwnerShort=T15.RE_DB_OwnerShort
				LEFT JOIN [SUTTER_1P_DELTA].dbo.CHART_GiftCode AS T16 ON T1.GFGiftCode=T16.GFGiftCode AND T1.RE_DB_OwnerShort=T16.RE_DB_OwnerShort
 			--(9879 row(s) affected)	
			 
 		
END;  --end gift table 
		--check number of gifts.
		SELECT T.[GSplitImpID]
		FROM [SUTTER_1P_DELTA].DBO.HC_Gift_SplitGift_v AS T
		INNER JOIN [SUTTER_1P_DELTA].DBO.HC_Gift_v AS T1 ON T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort AND T.GFImpID=T1.GFImpID
		 --9879
 		
		SELECT * FROM [SUTTER_1P_DELTA].TBL.GIFT
		WHERE [ACCOUNT:External_ID__c]	IS  NULL
		OR PAYMENT_METHOD_ACCOUNT_External_ID__c IS NULL 
		
	   SELECT * FROM [SUTTER_1P_DELTA].TBL.GIFT WHERE [ACCOUNT:External_ID__c] LIKE '%ABSF-02869-079-0132831%'	
	   SELECT * FROM  [SUTTER_1P_DELTA].TBL.PAYMENT_METHOD  WHERE [ACCOUNT_External_ID__c]='ABSF-02869-079-0132831'
	   SELECT * FROM [SUTTER_1P_DELTA].dbo.HC_Constituents_v WHERE [NEW_TGT_IMPORT_ID]='ABSF-02869-079-0132831'
	   SELECT * FROM [SUTTER_1P_DELTA].dbo.hc_gift_v WHERE [RE_DB_OwnerShort]='ABSF' AND [ImportID]='02869-079-0132831'

	   SELECT * FROM [SUTTER_1P_DELTA].TBL.GIFT 

	   SELECT GFType, Parent_Giving_RecordType, [child_Giving_RecordType], COUNT(*)
	   FROM [SUTTER_1P_DELTA].TBL.GIFT 
	   GROUP BY GFType, Parent_Giving_RecordType, [child_Giving_RecordType] 
	   ORDER BY GFType, Parent_Giving_RecordType, [child_Giving_RecordType] 
	
	
	BEGIN--CHECK DUPES 
         
			DROP TABLE [SUTTER_1P_DELTA].TBL.zGIFT_dupes
		   
		    SELECT  *
            --INTO    [SUTTER_1P_DELTA].TBL.zGIFT_dupes
            FROM    [SUTTER_1P_DELTA].TBL.GIFT
            WHERE   RE_GFSplitImportID_ID IN (
                    SELECT  RE_GFSplitImportID_ID
                    FROM    [SUTTER_1P_DELTA].[TBL].[GIFT]
                    GROUP BY RE_GFSplitImportID_ID
                    HAVING  COUNT(*) > 1 )
            ORDER BY RE_GFSplitImportID_ID, [ACCOUNT:External_ID__c]

		 
			SELECT [GFType], COUNT(*) C
			FROM [SUTTER_1P_DELTA].TBL.zGIFT_dupes
			GROUP BY [GFType]
			ORDER BY [GFType]

			--CHECK DUPES USING 2 FIELDS
            SELECT  T1.*
            FROM    [SUTTER_1P_DELTA].TBL.GIFT T1
                    INNER JOIN ( SELECT T2.GSplitImpID ,
                                        T2.GSplitRE_DB_OwnerShort
                                 FROM   [SUTTER_1P_DELTA].TBL.GIFT T2
                                 GROUP BY T2.GSplitImpID ,
                                        T2.GSplitRE_DB_OwnerShort
                                 HAVING COUNT(*) > 1
                               ) T2 ON T1.GSplitImpID = T2.GSplitImpID  AND T1.GSplitRE_DB_OwnerShort = T2.GSplitRE_DB_OwnerShort
            ORDER BY GSplitImpID ,
                    GSplitRE_DB_OwnerShort DESC;
		END;

		BEGIN--CHECK MISSING GFSPLITIMPID  (Missing gifts are because the Constituent is missing from the data table and RE)
            SELECT  T1.*
            FROM    [SUTTER_1P_DELTA].[dbo].[HC_Gift_SplitGift_v] AS T1
                    LEFT JOIN [SUTTER_1P_DELTA].[TBL].[GIFT] AS T2 ON T1.[GSplitImpID] = T2.GSplitImpID
                                                              AND [T1].[RE_DB_OwnerShort] = T2.GSplitRE_DB_OwnerShort
            WHERE   T2.GSplitImpID IS NULL;
	 	END



BEGIN 
	SELECT T2.*
	FROM [SUTTER_1P_DELTA].dbo.[HC_Gift_v] AS T1
	INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Gift_SoftCredit_v] AS T2 ON T1.[GFImpID]=T2.[GFLink]
														--368
	SELECT T2.*
	FROM [SUTTER_1P_DELTA].dbo.[HC_Gift_v] AS T1
	INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Gift_Tribute_v] AS T2 ON T1.[GFImpID]=T2.[GFTLink]
 														--745
	SELECT DISTINCT T2.[TribImpID]
	FROM [SUTTER_1P_DELTA].dbo.[HC_Gift_v] AS T1
	INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Gift_Tribute_v] AS T2 ON T1.[GFImpID]=T2.[GFTLink]
 														--381
END 