USE SUTTER_1P_DELTA
GO



BEGIN 
	  DROP TABLE SUTTER_1P_DELTA.IMP.ACCOUNT
END
 
BEGIN--ACCOUNTS
 
			--ACCOUNTS: CONSTITUENTS ORG_ACCOUNTS
			SELECT DISTINCT
			 dbo.fnc_OwnerID() AS OwnerID
			,dbo.fnc_RecordType('Account_Organizational') AS RecordTypeID
 		 	,CASE WHEN T.ConsID IS NULL OR T.ConsID ='' THEN '' ELSE (T.RE_DB_OwnerShort+'-'+T.ConsID) END AS	RE_ConsID__c
			,T2.NEW_TGT_IMPORT_ID AS External_ID__c
			,T2.NEW_TGT_IMPORT_ID AS RE_ImportID__c		
 		
	 		,T.Name	AS	Name
			,T.GivesAnon	AS	Gives_Anonymously__c
			,T.NoValidAddr	AS	No_Valid_Address__c
			,T.IsInactive	AS	rC_Bios__Active__c
			,T.OrgMatch	AS	Employee_Matching__c
			,T.[MtcFactor] AS   rC_Giving__Match_Ratio__c
			,T.PrimAddText	AS	rC_Bios__Salutation__c
			,T.NoEmail	AS	RE_HasOptedOutOfEmail__c
			,T.DateAdded	AS	CreatedDate
			,T.RE_DB_OwnerShort	AS	rC_Giving__Primary_Affiliation__c
			,'TRUE'	AS	rC_Giving__Track_Affiliations__c
			,'TRUE' AS	rC_Giving__Track_Hard_Credits__c
			,'TRUE' AS	rC_Giving__Track_Soft_Credits__c
			,'TRUE' AS	rC_Giving__Track_Summaries__c
			,GETDATE()  AS	rC_Giving__Rollup_Affiliations__c
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
		
 			,T4.Do_Not_Phone__c
			,T4.Do_Not_Solicit__c
			,T4.Do_Not_Mail__c
			,T4.Do_Not_Contact__c
			,T6.Phone
			,T6.RE_Email__c
			,T6.Fax
			,T6.RE_Home__c
			,NULL AS Pager__c
			,T6.Website
			,CAST(T7.Additional_Phone_Numbers__c AS NVARCHAR(4000)) AS Additional_Phone_Numbers__c

			,CASE WHEN T8.AddrLine1 IS NULL THEN NULL WHEN T8.AddrLine2!='' THEN T8.AddrLine1 +' '+T8.AddrLine2 ELSE T8.AddrLine1 END AS BillingStreet
			,T8.AddrCity AS BillingCity
			,T8.AddrState AS BillingState
			,T8.AddrZIP AS BillingPostalCode
			,T8.AddrCountry BillingCountry
	 
			--reference only
			,'Constituent Org' AS refSource
			,T.KeyInd refKeyInd
			,T.ConsID refConsID
			,T.ImportID refImportID
			,NULL AS refORImpID
			,NULL AS refESRImpID
			,T.RE_DB_OwnerShort refDBOwner
			,LEN(T7.Additional_Phone_Numbers__c) AS refAddphonelen
		 
			INTO SUTTER_1P_DELTA.IMP.ACCOUNT  -- drop table SUTTER_1P_DELTA.IMP.ACCOUNT_NEW_HH
			FROM SUTTER_1P_DELTA.dbo.HC_Constituents_v T	
			INNER JOIN SUTTER_1P_DELTA.tbl.Account_Org T2 ON T.RE_DB_ID=T2.NEW_TGT_IMPORT_ID 
 			LEFT JOIN SUTTER_1P_DELTA.TBL.SolicitCodes T4 ON T.RE_DB_ID=T4.NEW_TGT_IMPORT_ID
			LEFT JOIN SUTTER_1P_DELTA.TBL.Phones_seq_1_final T6 ON T.RE_DB_ID=T6.NEW_TGT_IMPORT_ID  
			LEFT JOIN SUTTER_1P_DELTA.TBL.Phones_seq_2_final T7 ON T.RE_DB_ID=T7.NEW_TGT_IMPORT_ID  
			LEFT JOIN SUTTER_1P_DELTA.TBL.Address_Pref	T8 ON T.RE_DB_ID=T8.RE_DB_ID
 	 		--34 ok

		UNION ALL
			--ACCOUNTS: CONSTITUENTS IND HEAD OF HOUSEHOLD
			SELECT DISTINCT
			 dbo.fnc_OwnerID() AS OwnerID
			,dbo.fnc_RecordType('Account_Household') AS RecordTypeID
 		 	,CASE WHEN T.ConsID IS NULL OR T.ConsID ='' THEN '' ELSE (T.RE_DB_OwnerShort+CHAR(10)+T.ConsID) END AS	RE_ConsID__c
			,(T.RE_DB_OwnerShort+'-'+T.ImportID) AS External_ID__c
			,(T.RE_DB_OwnerShort+'-'+T.ImportID) AS RE_ImportID__c
 	  
			,T.Name	AS	Name
			,T.GivesAnon	AS	Gives_Anonymously__c
			,T.NoValidAddr	AS	No_Valid_Address__c
			,CASE WHEN T2.Deceased='TRUE'  THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Active__c
			,T.OrgMatch	AS	Employee_Matching__c
			,T.[MtcFactor] AS rC_Giving__Match_Ratio__c
			,T.PrimAddText	AS	rC_Bios__Salutation__c
			,T.NoEmail	AS	RE_HasOptedOutOfEmail__c
			,T.DateAdded	AS	CreatedDate
			,T.RE_DB_OwnerShort	AS	rC_Giving__Primary_Affiliation__c
			,'TRUE'	AS	rC_Giving__Track_Affiliations__c
			,'TRUE' AS	rC_Giving__Track_Hard_Credits__c
			,'TRUE' AS	rC_Giving__Track_Soft_Credits__c
			,'TRUE' AS	rC_Giving__Track_Summaries__c
			,GETDATE()  AS	rC_Giving__Rollup_Affiliations__c
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
	 		,T4.Do_Not_Phone__c
			,T4.Do_Not_Solicit__c
			,T4.Do_Not_Mail__c
			,T4.Do_Not_Contact__c
			,T6.Phone
			,T6.RE_Email__c
			,T6.Fax
			,T6.RE_Home__c
			,NULL AS Pager__c
			,T6.Website
			,CAST(T7.Additional_Phone_Numbers__c AS NVARCHAR(4000)) AS Additional_Phone_Numbers__c
			,CASE WHEN T8.AddrLine1 IS NULL THEN NULL WHEN T8.AddrLine2!='' THEN T8.AddrLine1 +' '+T8.AddrLine2 ELSE T8.AddrLine1 END AS BillingStreet
			,T8.AddrCity AS BillingCity
			,T8.AddrState AS BillingState
			,T8.AddrZIP AS BillingPostalCode
			,T8.AddrCountry AS BillingCountry
 
			--reference only
			,'Constituent Ind' AS refSource
			,T.KeyInd refKeyInd
			,T.ConsID refConsID
			,T.ImportID refImportID
			,NULL AS refORImpID
			,NULL AS refESRImpID
			,T.RE_DB_OwnerShort refDBOwner
	  		,LEN(T7.Additional_Phone_Numbers__c) AS refaddphonelen
			
			FROM SUTTER_1P_DELTA.dbo.HC_Constituents_v T	
			INNER JOIN SUTTER_1P_DELTA.tbl.Contact_HofH T2 ON T.RE_DB_ID=T2.HH_ImportID --HH Acct Added new field HH_ImportID to the tbl.Contact_HofH. HH_ImportID is already the NEW_TGT_IMPORT_ID
	 		LEFT JOIN SUTTER_1P_DELTA.TBL.SolicitCodes T4 ON T.RE_DB_ID=T4.NEW_TGT_IMPORT_ID
			LEFT JOIN SUTTER_1P_DELTA.TBL.Phones_seq_1_final T6 ON T.RE_DB_ID=T6.NEW_TGT_IMPORT_ID 
			LEFT JOIN SUTTER_1P_DELTA.TBL.Phones_seq_2_final T7 ON T.RE_DB_ID=T7.NEW_TGT_IMPORT_ID  
			LEFT JOIN SUTTER_1P_DELTA.TBL.Address_Pref	T8 ON T.RE_DB_ID=T8.RE_DB_ID
	 	 	--550
	 
	
END	-- 	(584 row(s) affected)
    

BEGIN --REPLACE DOUBLE QUOTES TO SINGLE QUOTES
			-- select [name] from [SUTTER_1P_DELTA].[IMP].[ACCOUNT] where [name] like '%"%'
			update [SUTTER_1P_DELTA].[IMP].ACCOUNT set [name]=REPLACE([name],'"','''') where [name] like '%"%'
 			update [SUTTER_1P_DELTA].[IMP].ACCOUNT set [rC_Bios__Salutation__c]=REPLACE([rC_Bios__Salutation__c],'"','''') where [rC_Bios__Salutation__c] like '%"%'
 			update [SUTTER_1P_DELTA].[IMP].ACCOUNT set [BillingStreet]=REPLACE([BillingStreet],'"','''') where [BillingStreet] LIKE '%"%'
 			update [SUTTER_1P_DELTA].[IMP].ACCOUNT set [BillingCity]=REPLACE([BillingCity],'"','''') where [BillingCity] LIKE '%"%'
	 		update [SUTTER_1P_DELTA].[IMP].ACCOUNT set Phone=REPLACE(Phone,'"','''') where Phone LIKE '%"%'
			update [SUTTER_1P_DELTA].[IMP].ACCOUNT set Additional_Phone_Numbers__c=REPLACE(Additional_Phone_Numbers__c,'"','''') where Additional_Phone_Numbers__c LIKE '%"%'

END

BEGIN 
			USE [SUTTER_1P_DELTA] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'ACCOUNT'
END 

BEGIN

		BEGIN--check duplicates 
			SELECT * FROM [SUTTER_1P_DELTA].[IMP].[ACCOUNT] 
			WHERE External_ID__c IN (SELECT External_ID__c FROM [SUTTER_1P_DELTA].[IMP].[ACCOUNT] GROUP BY External_ID__c HAVING COUNT(*)>1)
			ORDER BY refSource, External_ID__c
		END 	 

		--	SELECT count(*) FROM [SUTTER_1P_DELTA].[IMP].[ACCOUNT] 584

	 

END

BEGIN--UPDATE ACCOUNT AFFILIATION
	--WBRX
		--delete table
			DROP TABLE SUTTER_1P_DELTA.TBL.WBRX_NEW_AFFILIATION
		--create tbl
			SELECT T1.RE_DB_OwnerShort, MIN(T2.SolicitCode) AS SolicitCode, T1.NEW_TGT_IMPORT_ID
			,NEW_AFFILIATION= CASE WHEN (LEFT(T1.NEW_TGT_IMPORT_ID,4)='WBRX') THEN MIN(T3.SolicitCode) ELSE LEFT(T1.NEW_TGT_IMPORT_ID,4) END 
			INTO SUTTER_1P_DELTA.TBL.WBRX_NEW_AFFILIATION  
			FROM SUTTER_1P_DELTA.dbo.HC_Constituents_v AS T1
			LEFT JOIN SUTTER_1P_DELTA.dbo.HC_Cons_SolicitCodes_v T2 ON T1.ImportID=T2.ImportID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN [SUTTER_1P_DELTA].dbo.[CHART_ConsSolicitCodes] AS T3 ON T2.SolicitCode = T3.[SolicitCode]
			WHERE T1.RE_DB_OwnerShort='WBRX' 
			GROUP BY T1.RE_DB_OwnerShort,  T1.NEW_TGT_IMPORT_ID 
			ORDER BY T1.NEW_TGT_IMPORT_ID
			 --(104 row(s) affected)
		--check dupes
			 SELECT * FROM SUTTER_1P_DELTA.TBL.WBRX_NEW_AFFILIATION 
			 WHERE NEW_TGT_IMPORT_ID IN (SELECT NEW_TGT_IMPORT_ID FROM SUTTER_1P_DELTA.TBL.WBRX_NEW_AFFILIATION  GROUP BY NEW_TGT_IMPORT_ID HAVING COUNT(*)>1)
			 ORDER BY NEW_TGT_IMPORT_ID
		--check nulls
			 SELECT * FROM SUTTER_1P_DELTA.TBL.WBRX_NEW_AFFILIATION  
			 WHERE NEW_AFFILIATION IS NULL 
			--update nulls
			 UPDATE SUTTER_1P_DELTA.TBL.WBRX_NEW_AFFILIATION 
			 SET NEW_AFFILIATION=LEFT(NEW_TGT_IMPORT_ID,4)
			 WHERE NEW_AFFILIATION IS NULL 
		--check list	 
			SELECT * FROM SUTTER_1P_DELTA.TBL.WBRX_NEW_AFFILIATION  
			 ORDER BY NEW_AFFILIATION
	--CVRX
		--delete table
			DROP TABLE SUTTER_1P_DELTA.TBL.CVRX_NEW_AFFILIATION
		--create tbl
			SELECT T1.RE_DB_OwnerShort, MIN(T2.SolicitCode) AS SolicitCode, T1.NEW_TGT_IMPORT_ID
			,NEW_AFFILIATION= CASE WHEN (LEFT(T1.NEW_TGT_IMPORT_ID,4)='CVRX') THEN MIN(T3.SolicitCode) ELSE LEFT(T1.NEW_TGT_IMPORT_ID,4) END 
			INTO SUTTER_1P_DELTA.TBL.CVRX_NEW_AFFILIATION  
			FROM SUTTER_1P_DELTA.dbo.HC_Constituents_v AS T1
			LEFT JOIN SUTTER_1P_DELTA.dbo.HC_Cons_SolicitCodes_v T2 ON T1.ImportID=T2.ImportID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN [SUTTER_1P_DELTA].dbo.[CHART_ConsSolicitCodes] AS T3 ON T2.SolicitCode = T3.[SolicitCode]
			WHERE T1.RE_DB_OwnerShort='CVRX' AND T3.[SF_Object_2] IS NOT NULL 
			GROUP BY T1.RE_DB_OwnerShort,  T1.NEW_TGT_IMPORT_ID 
			ORDER BY T1.NEW_TGT_IMPORT_ID
			--(30 row(s) affected)
			
			SELECT * FROM  [SUTTER_1P_DELTA].dbo.[CHART_ConsSolicitCodes]  ORDER BY [RE_DB_OwnerShort]

		--check dupes
			 SELECT * FROM SUTTER_1P_DELTA.TBL.CVRX_NEW_AFFILIATION 
			 WHERE NEW_TGT_IMPORT_ID IN (SELECT NEW_TGT_IMPORT_ID FROM SUTTER_1P_DELTA.TBL.CVRX_NEW_AFFILIATION  GROUP BY NEW_TGT_IMPORT_ID HAVING COUNT(*)>1)
			 ORDER BY NEW_TGT_IMPORT_ID
		--check nulls
			 SELECT * FROM SUTTER_1P_DELTA.TBL.CVRX_NEW_AFFILIATION  
			 WHERE NEW_AFFILIATION IS NULL 
			--update nulls
			 UPDATE SUTTER_1P_DELTA.TBL.CVRX_NEW_AFFILIATION 
			 SET NEW_AFFILIATION=LEFT(NEW_TGT_IMPORT_ID,4)
			 WHERE NEW_AFFILIATION IS NULL 
		--check list	 
			SELECT * FROM SUTTER_1P_DELTA.TBL.CVRX_NEW_AFFILIATION  
			 ORDER BY NEW_AFFILIATION

END 

BEGIN--update ACCOUNT
 	   --WBRX
		SELECT T1.External_ID__c, T1.rC_Giving__Primary_Affiliation__c, T2.NEW_TGT_IMPORT_ID, T2.NEW_AFFILIATION, LEN(T2.NEW_AFFILIATION) l
		FROM [SUTTER_1P_DELTA].[IMP].[ACCOUNT] t1
		INNER JOIN SUTTER_1P_DELTA.TBL.WBRX_NEW_AFFILIATION T2 ON T1.External_ID__c = T2.NEW_TGT_IMPORT_ID
	 	ORDER BY l desc
		UPDATE [SUTTER_1P_DELTA].[IMP].[ACCOUNT]
		SET rC_Giving__Primary_Affiliation__c = T2.NEW_AFFILIATION
		FROM [SUTTER_1P_DELTA].[IMP].[ACCOUNT] t1
		INNER JOIN SUTTER_1P_DELTA.TBL.WBRX_NEW_AFFILIATION T2 ON T1.External_ID__c = T2.NEW_TGT_IMPORT_ID
	  
	  --CVRX
		SELECT T1.External_ID__c, T1.rC_Giving__Primary_Affiliation__c, T2.NEW_TGT_IMPORT_ID, T2.NEW_AFFILIATION, LEN(T2.NEW_AFFILIATION) l
		FROM [SUTTER_1P_DELTA].[IMP].[ACCOUNT] t1
		INNER JOIN SUTTER_1P_DELTA.TBL.CVRX_NEW_AFFILIATION T2 ON T1.External_ID__c = T2.NEW_TGT_IMPORT_ID
	 	ORDER BY l desc
		UPDATE [SUTTER_1P_DELTA].[IMP].[ACCOUNT]
		SET rC_Giving__Primary_Affiliation__c = T2.NEW_AFFILIATION
		FROM [SUTTER_1P_DELTA].[IMP].[ACCOUNT] t1
		INNER JOIN SUTTER_1P_DELTA.TBL.CVRX_NEW_AFFILIATION T2 ON T1.External_ID__c = T2.NEW_TGT_IMPORT_ID
		--(30 row(s) affected)
END; 



BEGIN--INACTIVE ACCOUNTS  ([ACCOUNT:External_ID__c]) 
	 --If all of the related Contacts have the Deceased flag equal to true then the rC Bios Active flag would be set to false.
 
	DROP TABLE [SUTTER_1P_DELTA].[TBL].[Account_Inactive] 

	SELECT [ACCOUNT:External_ID__c], rC_Bios__Deceased__c, COUNT(*) C
	INTO [SUTTER_1P_DELTA].[TBL].[Account_Inactive] 
	FROM [SUTTER_1P_DELTA].[IMP].[CONTACT]
	GROUP BY [ACCOUNT:External_ID__c], rC_Bios__Deceased__c
	ORDER BY [ACCOUNT:External_ID__c], rC_Bios__Deceased__c

	   SELECT [ACCOUNT:External_ID__c], COUNT(*) c 
	   FROM [SUTTER_1P_DELTA].[TBL].[Account_Inactive] 
	   GROUP BY [ACCOUNT:External_ID__c]
	   HAVING COUNT(*)>1
	   ORDER BY c desc
END 

BEGIN--ACCOUNT UPDATE: INACTIVE  
		--INACTIVE ACCOUNTS
			SELECT * FROM [SUTTER_1P_DELTA].[TBL].[Account_Inactive] WHERE rC_Bios__Deceased__c='TRUE'


	 		SELECT DISTINCT T1.[External_ID__c], T1.rC_Bios__Active__c
			FROM [SUTTER_1P_DELTA].[IMP].[ACCOUNT] t1
			INNER JOIN [SUTTER_1P_DELTA].[TBL].[Account_Inactive] T2 ON T1.External_ID__c = T2.[ACCOUNT:External_ID__c]
			WHERE T2.rC_Bios__Deceased__c='TRUE'

			UPDATE [SUTTER_1P_DELTA].[IMP].[ACCOUNT]
			SET rC_Bios__Active__c = 'FALSE'
			FROM [SUTTER_1P_DELTA].[IMP].[ACCOUNT] t1
			INNER JOIN [SUTTER_1P_DELTA].[TBL].[Account_Inactive] T2 ON T1.External_ID__c = T2.[ACCOUNT:External_ID__c]
			WHERE T2.rC_Bios__Deceased__c='TRUE'
	  															 
END  

   

SELECT * FROM [SUTTER_1P_DELTA].IMP.ACCOUNT 
WHERE rc_giving__primary_Affiliation__c ='CVRX'
WHERE  RE_CONSID__C='HOTV-85214' OR RE_CONSID__C='HOTV-85235'


SELECT * FROM [SUTTER_1P_DELTA].dbo.[HC_Cons_SolicitCodes_v] WHERE [RE_DB_Id] LIKE 'CVRX%'
  SELECT * FROM [SUTTER_1P_DELTA].IMP.ACCOUNT  WHERE external_id__c='CVRX-00001-593-0000064377'

SELECT * FROM [SUTTER_1P_DELTA].DBO.[HC_Cons_Phone_v] 
WHERE [RE_DB_OwnerShort] = 'HOTV' AND ([ImportID]='00001-593-0000079866' OR [ImportID]='00001-593-0000079535')

 
 --exceptions not imported.
	DROP TABLE SUTTER_1P_DELTA.IMP.ACCOUNT_XCP
	
	SELECT		T1.*
	INTO		SUTTER_1P_DELTA.IMP.ACCOUNT_XCP
	FROM        SUTTER_1P_DELTA.IMP.ACCOUNT AS T1 
	LEFT JOIN	SUTTER_1P_DELTA.XTR.ACCOUNT AS X1 ON T1.External_ID__c = X1.EXTERNAL_ID__C
	WHERE     (X1.EXTERNAL_ID__C IS NULL) OR (X1.EXTERNAL_ID__C = '')
	 
  	 

	  