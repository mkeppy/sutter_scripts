 

USE [SUTTER_1P_MIGRATION]
GO
				 
BEGIN
	DROP TABLE  [SUTTER_1P_DELTA_migration].IMP.TRIBUTE
END


BEGIN	
	--TRIBUTE
		SELECT
			NULL AS Id
			,T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.TRImpID AS RE_TRImpID__c
			,T.TRType AS Tribute_Type__c
 			,CASE WHEN T2.KeyInd='O' THEN T2.NEW_TGT_IMPORT_ID END AS [Tribute_Account__r:External_Id__c]
			,CASE WHEN T2.KeyInd='I' THEN T2.NEW_TGT_IMPORT_ID END AS [Tribute_Contact__r:External_Id__c]
 			,CASE LEN(T.TRDateFrom) WHEN '4' THEN '01/01/'+T.[TRDateFrom]
									WHEN '6' THEN RIGHT(T.[TRDateFrom],2)+'/01/'+ LEFT(T.[TRDateFrom],4) 
									WHEN '8' THEN SUBSTRING(T.[TRDateFrom],5,2) +'/'+ SUBSTRING(T.[TRDateFrom],7,2) +'/'+ LEFT(T.[TRDateFrom],4) 
									ELSE NULL END AS Tribute_Start_Date__c
 			,CASE LEN(T.TRDateTo) WHEN '4' THEN '01/01/'+T.TRDateTo
									WHEN '6' THEN RIGHT(T.TRDateTo,2)+'/01/'+ LEFT(T.TRDateTo,4) 
									WHEN '8' THEN SUBSTRING(T.TRDateTo,5,2) +'/'+ SUBSTRING(T.TRDateTo,7,2) +'/'+ LEFT(T.TRDateTo,4) 
									ELSE NULL END AS Tribute_End_Date__c
 
 			,T.TRDesc AS Description__c
 			,CAST(T.TRNotes AS VARCHAR(8000)) AS Comments__c
			,T.RE_DB_OwnerShort AS Affiliation__c
			  ,T3.[Notificant_1__r:External_ID__c]
			  ,T3.[Notificant_2__r:External_ID__c]
			  ,T3.[Notificant_3__r:External_ID__c]
			  ,T3.[Notificant_4__r:External_ID__c]
			  ,T3.[Notificant_5__r:External_ID__c]
			  ,T3.[Notificant_6__r:External_ID__c]
			  ,T3.[Notificant_7__r:External_ID__c]
			  ,T3.[Notificant_8__r:External_ID__c]
			  ,T3.[Notificant_9__r:External_ID__c]
			  ,T3.[Notificant_10__r:External_ID__c]
			  ,T3.[Notificant_11__r:External_ID__c]

			,T.TRImpID AS zrefTRImpId
			,T.[ImportID] AS zrefImportId
			,T.[RE_DB_OwnerShort] AS zrefRE_DB_Owner
			,T.[ConsID] AS zrefConsID
			,T2.[KeyInd] AS zrefKeyInd
			,T.[TRDateFrom] AS zrefFrom
			,T.[TRDateTo] AS zrefTo
		INTO [SUTTER_1P_DELTA_migration].IMP.TRIBUTE
		FROM [SUTTER_1P_DATA].dbo.[HC_Cons_Tribute_v] AS T
		INNER JOIN [SUTTER_1P_DATA].dbo.[HC_Constituents_v] AS T2 ON T.[ImportID]=T2.[ImportID] AND T.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
		LEFT JOIN  [SUTTER_1P_MIGRATION].[TBL].[TRIBUTE_CONTACT_2_final] AS T3 ON (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.TRImpID)=T3.[Tribute__r:RE_TRImpID__c]
		LEFT JOIN [SUTTER_1P_MIGRATION].xtr.[TRIBUTE] X ON (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.TRImpID)=X.[RE_TRIMPID__C]
		WHERE X.[RE_TRIMPID__C] IS NULL 

		--144

END 

BEGIN --UPDATEs
  		BEGIN 
			UPDATE [SUTTER_1P_DELTA_migration].IMP.TRIBUTE SET Description__c=REPLACE(Description__c,'"','''') where Description__c like '%"%'
			UPDATE [SUTTER_1P_DELTA_migration].IMP.TRIBUTE SET Comments__c=REPLACE(Comments__c,'"','''') where Comments__c like '%"%'
		END
		
		BEGIN 
			USE [SUTTER_1P_DELTA_migration] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'TRIBUTE'
 		END
END 
 
 SELECT * FROM  [SUTTER_1P_DELTA_migration].IMP.TRIBUTE 
 WHERE [Notificant_1__r:External_ID__c] IS NOT NULL   AND 

 SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Cons_Tribute_v] 
 WHERE [TRImpID]='DR11-012910-383'


  


SELECT * FROM [SUTTER_1P_DATA].[dbo].[HC_Tribute_Acknowledgee_v]

