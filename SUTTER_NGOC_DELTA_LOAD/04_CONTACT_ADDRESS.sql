USE SUTTER_1P_DELTA
GO

BEGIN
			
			DROP TABLE SUTTER_1P_DELTA.IMP.CONTACT_ADDRESS
END


BEGIN--CONTACT ADDRESS
   			--CONSTITUENT INDIVIDUAL HEAD OF HOUSEHOLD 	; NEW ADDRESS ON NEW CONSTITUENT
			SELECT DISTINCT
			T.HH_ImportID  AS [rC_Bios__Contact__r:External_Id__c]
			,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c
			,T2.rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='False' THEN 'False' 
		          WHEN (T1.AddrPref='True' AND T0.RE_DB_ID!=T0.NEW_TGT_IMPORT_ID) THEN 'False' ELSE T1.AddrPref END AS rC_Bios__Preferred_Mailing__c
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c
			,T1.[AddrCounty] AS County__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			,NULL AS rC_Bios__Do_Not_Mail__c

			--refernce
			,'Contact_HofH_new_addr_new_cons' AS zrefSource
			,T0.ImportID AS zrefOldImportID
			,T0.NEW_TGT_IMPORT_ID AS zrefNewImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq
 
 		 	INTO SUTTER_1P_DELTA.IMP.CONTACT_ADDRESS
			FROM SUTTER_1P_DELTA.TBL.Contact_HofH T
			INNER JOIN SUTTER_1P_DELTA.dbo.HC_Constituents_v T0 ON T.HH_ImportID=T0.NEW_TGT_IMPORT_ID
			INNER JOIN SUTTER_1P_DELTA.DBO.HC_Cons_Address_v T1 ON T0.RE_DB_ID=T1.RE_DB_ID
		 	LEFT JOIN SUTTER_1P_DELTA.DBO.CHART_AddressType T2 ON T1.AddrType=T2.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.[Convert]='Yes' AND (T1.[AddrLine1] IS NOT NULL AND T1.[AddrLine1]!='' AND T1.[AddrLine1] NOT LIKE '%deceased%' AND T1.[AddrLine1]!='dec')
 		UNION ALL	
 			--CONSTITUENT INDIVIDUAL NON-HEAD OF HOUSEHOLD ; NEW ADDDRESS ON NEW CONS 
			SELECT DISTINCT
			T.NoHH_ImportID  AS [rC_Bios__Contact__r:External_Id__c]
			,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c
			,T2.rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='False' THEN 'False' 
		          WHEN (T1.AddrPref='True' AND T0.RE_DB_ID!=T0.NEW_TGT_IMPORT_ID) THEN 'False' ELSE T1.AddrPref END AS rC_Bios__Preferred_Mailing__c
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
			,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c
			,T1.[AddrCounty] AS County__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			,NULL AS rC_Bios__Do_Not_Mail__c
   			--reference
			,'Contact_NHofH_new_addr_new_cons' AS zrefSource
			,T0.ImportID AS zrefOldImportID
			,T0.NEW_TGT_IMPORT_ID AS zrefNewImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq
	 			
			FROM SUTTER_1P_DELTA.TBL.Contact_HofH T
			INNER JOIN SUTTER_1P_DELTA.dbo.HC_Constituents_v T0 ON T.NoHH_ImportID=T0.NEW_TGT_IMPORT_ID
			INNER JOIN SUTTER_1P_DELTA.DBO.HC_Cons_Address_v T1 ON T0.RE_DB_ID=T1.RE_DB_ID
		 	LEFT JOIN SUTTER_1P_DELTA.DBO.CHART_AddressType T2 ON T1.AddrType=T2.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_DELTA.TBL.Contact_HofH TA ON T.NoHH_ImportID=TA.HH_ImportID
			WHERE T2.[Convert]='Yes'  AND TA.HH_ImportID IS NULL AND (T1.[AddrLine1] IS NOT NULL AND T1.[AddrLine1]!='' AND T1.[AddrLine1] NOT LIKE '%deceased%' AND T1.[AddrLine1]!='dec')
 					 
		UNION ALL
		  	--CONSTITUENT INDIVIDUAL HEAD OF HOUSEHOLD 	; NEW ADDRESS ON EXISTING CONSTITUENT
			SELECT DISTINCT
			T.HH_ImportID  AS [rC_Bios__Contact__r:External_Id__c]
			,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c
			,T2.rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='False' THEN 'False' 
		          WHEN (T1.AddrPref='True' AND T0.RE_DB_ID!=T0.NEW_TGT_IMPORT_ID) THEN 'False' ELSE T1.AddrPref END AS rC_Bios__Preferred_Mailing__c
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c
			,T1.[AddrCounty] AS County__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			,NULL AS rC_Bios__Do_Not_Mail__c

			--refernce
			,'Contact_HofH_new_addr_old_cons' AS zrefSource
			,T0.ImportID AS zrefOldImportID
			,T0.NEW_TGT_IMPORT_ID AS zrefNewImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq
  			FROM SUTTER_1P_DELTA.TBL.Contact_HofH_final_conv T
			INNER JOIN SUTTER_1P_DELTA.dbo.HC_Constituents_v_final T0 ON T.HH_ImportID=T0.NEW_TGT_IMPORT_ID
			INNER JOIN SUTTER_1P_DELTA.DBO.HC_Cons_Address_v T1 ON T0.RE_DB_ID=T1.RE_DB_ID
		 	LEFT JOIN SUTTER_1P_DELTA.DBO.CHART_AddressType T2 ON T1.AddrType=T2.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.[Convert]='Yes' AND (T1.[AddrLine1] IS NOT NULL AND T1.[AddrLine1]!='' AND T1.[AddrLine1] NOT LIKE '%deceased%' AND T1.[AddrLine1]!='dec')
 		UNION ALL	
 			--CONSTITUENT INDIVIDUAL NON-HEAD OF HOUSEHOLD 	; NEW ADDRESS ON EXISTING CONSTITUENT
			SELECT DISTINCT
			T.NoHH_ImportID  AS [rC_Bios__Contact__r:External_Id__c]
			,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c
			,T2.rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='False' THEN 'False' 
		          WHEN (T1.AddrPref='True' AND T0.RE_DB_ID!=T0.NEW_TGT_IMPORT_ID) THEN 'False' ELSE T1.AddrPref END AS rC_Bios__Preferred_Mailing__c
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
			,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c
			,T1.[AddrCounty] AS County__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			,NULL AS rC_Bios__Do_Not_Mail__c

			--reference
			,'Contact_NHofH_new_addr_new_cons' AS zrefSource
			,T0.ImportID AS zrefOldImportID
			,T0.NEW_TGT_IMPORT_ID AS zrefNewImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq						 
	 			
			FROM SUTTER_1P_DELTA.TBL.Contact_HofH_final_conv T
			INNER JOIN SUTTER_1P_DELTA.dbo.HC_Constituents_v_final T0 ON T.NoHH_ImportID=T0.NEW_TGT_IMPORT_ID
			INNER JOIN SUTTER_1P_DELTA.DBO.HC_Cons_Address_v T1 ON T0.RE_DB_ID=T1.RE_DB_ID
		 	LEFT JOIN SUTTER_1P_DELTA.DBO.CHART_AddressType T2 ON T1.AddrType=T2.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_1P_DELTA.TBL.Contact_HofH TA ON T.NoHH_ImportID=TA.HH_ImportID
			WHERE T2.[Convert]='Yes'  AND TA.HH_ImportID IS NULL AND (T1.[AddrLine1] IS NOT NULL AND T1.[AddrLine1]!='' AND T1.[AddrLine1] NOT LIKE '%deceased%' AND T1.[AddrLine1]!='dec')
 		
		
			 UNION ALL 		
		--CONSTITUENT DONOR PROXY from  ORG CONS with no ind relat. 
			SELECT DISTINCT  
			 T.NEW_TGT_IMPORT_ID   AS [rC_Bios__Contact__r:External_Id__c]
	 		,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c 
			,T2.rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='False' THEN 'False' 
			      WHEN (T1.AddrPref='True' AND T0.RE_DB_ID!=T0.NEW_TGT_IMPORT_ID) THEN 'False' ELSE T1.AddrPref END AS rC_Bios__Preferred_Mailing__c
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c
			,T1.[AddrCounty] AS County__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			, NULL AS rC_Bios__Do_Not_Mail__c
	 		--refernce
			,'Account_Org_donor_proxy_contact' AS zrefSource
			,T0.RE_DB_ID AS zrefOldImportID
			,T.ImportID AS zrefNewImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq
			
		 	FROM [SUTTER_1P_DELTA].TBL.Account_Org T
			INNER JOIN [SUTTER_1P_DELTA].dbo.HC_Constituents_v T0 ON T.ImportID=T0.NEW_TGT_IMPORT_ID
			INNER JOIN [SUTTER_1P_DELTA].DBO.HC_Cons_Address_v T1 ON T0.RE_DB_ID=T1.RE_DB_ID 
		 	LEFT JOIN [SUTTER_1P_DELTA].DBO.CHART_AddressType T2 ON T1.AddrType=T2.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.[Convert]='Yes' 
			AND (T1.[AddrLine1] IS NOT NULL AND T1.[AddrLine1]!='' AND T1.[AddrLine1] NOT LIKE '%deceased%' AND T1.[AddrLine1]!='dec')
		  
					 
END; -- (541 row(s) affected)

BEGIN--REMOVE DOUBLE QUOTES

	--  SELECT [rC_Bios__Original_Street_Line_1__c] FROM [SUTTER_1P_DELTA].[IMP].[ACCOUNT_ADDRES] WHERE [rC_Bios__Original_Street_Line_1__c] LIKE '%"%';
	--  SELECT [rC_Bios__Original_Street_Line_2__c] FROM [SUTTER_1P_DELTA].[IMP].[ACCOUNT_ADDRES] WHERE [rC_Bios__Original_Street_Line_2__c] LIKE '%"%';        
    --  SELECT [rC_Bios__Original_City__c] FROM [SUTTER_1P_DELTA].[IMP].[ACCOUNT_ADDRES] WHERE [rC_Bios__Original_City__c] LIKE '%"%';        
    
    UPDATE [SUTTER_1P_DELTA].IMP.[CONTACT_ADDRESS] SET rC_Bios__Original_Street_Line_1__c=REPLACE(rC_Bios__Original_Street_Line_1__c,'"','''') where rC_Bios__Original_Street_Line_1__c like '%"%'
	UPDATE [SUTTER_1P_DELTA].IMP.[CONTACT_ADDRESS] SET rC_Bios__Original_Street_Line_2__c=REPLACE(rC_Bios__Original_Street_Line_2__c,'"','''') where rC_Bios__Original_Street_Line_2__c like '%"%'
	UPDATE [SUTTER_1P_DELTA].IMP.[CONTACT_ADDRESS] SET rC_Bios__Original_City__c=REPLACE(rC_Bios__Original_City__c,'"','''') where rC_Bios__Original_City__c like '%"%'

	USE [SUTTER_1P_DELTA] 
 	EXEC sp_FindStringInTable '%"%', 'IMP', 'CONTACT_ADDRESS'


END


BEGIN--check duplicate CONTACTADDRESS
 
	 SELECT *
	 FROM SUTTER_1P_DELTA.IMP.[CONTACT_ADDRESS]	  
	 WHERE rC_Bios__External_ID__c IN (SELECT rC_Bios__External_ID__c FROM SUTTER_1P_DELTA.IMP.[CONTACT_ADDRESS]	  
									GROUP BY rC_Bios__External_ID__c HAVING COUNT(*)>1)
	 ORDER BY rC_Bios__External_ID__c 

END

   			 
	SELECT COUNT(*) FROM SUTTER_1P_DELTA.IMP.[CONTACT_ADDRESS]	
 
 
BEGIN-- check exceptions

	SELECT T.*
	INTO SUTTER_1P_DELTA.IMP.[CONTACT_ADDRESS_XCP]
	FROM SUTTER_1P_DELTA.IMP.[CONTACT_ADDRESS]	T
	LEFT JOIN SUTTER_1P_DELTA.XTR.CONTACT_ADDRESS X ON T.rC_Bios__External_ID__c =X.rC_Bios__External_ID__c
	WHERE X.rC_Bios__External_ID__c IS NULL OR X.rC_Bios__External_ID__c=''
	ORDER BY T.rC_Bios__External_ID__c 


END 
	   