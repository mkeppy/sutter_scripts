

--SALUTATIONS 
USE SUTTER_1P_DELTA
GO

BEGIN
	DROP TABLE SUTTER_1P_DELTA.IMP.SALUTATION
END


BEGIN--SALUTATIONS from CONSTITUENT records . 
		
		--CONSTITUENT
				SELECT DISTINCT
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Salutation_Salutation') AS RecordTypeID
				,T.RE_DB_Tbl+'-'+T.RE_DB_ID AS External_ID__c
				,CASE WHEN T2.NoHH_ImportID IS NOT NULL THEN T2.HH_ImportID ELSE T.NEW_TGT_IMPORT_ID END AS [rC_Bios__Account__r:External_ID__c]
				,NULL [rC_Bios__Contact__r:External_ID__c]
				,CAST(T.PrimAddText AS NVARCHAR(255)) AS Name
				,'Primary Addressee/Salutation' AS rC_Bios__Salutation_Type__c
				,CAST(T.PrimAddText AS NVARCHAR(255)) rC_Bios__Salutation_Line_1__c
				,CASE WHEN T.KeyInd='I' THEN CAST(T.PrimSalText AS NVARCHAR(255)) END AS rC_Bios__Inside_Salutation__c
				,CAST('' AS VARCHAR(255)) AS Additional_Recognition_Subtype__c
				,CASE WHEN T2.NoHH_ImportID IS NOT NULL THEN 'FALSE' WHEN T.RE_DB_ID!=T.NEW_TGT_IMPORT_ID THEN 'FALSE' ELSE 'TRUE' END AS rC_Bios__Preferred_Salutation__c    --only Prefered for HofHH. 
				,T.[RE_DB_OwnerShort] AS Affiliation__c
			 	--reference
				,'Constituent' AS zrefSource	
				,T.RE_DB_ID AS zrefOldImportId
				,T.NEW_TGT_IMPORT_ID AS zrefNewImportID
				,T2.HH_ImportID zrefHHImportID
				,T2.NoHH_ImportID zrefNoHHImportID
				,T.RE_DB_Tbl zrefTable
				,NULL zrefIRImpID		
			 	INTO SUTTER_1P_DELTA.IMP.SALUTATION
				FROM SUTTER_1P_DELTA.dbo.HC_Constituents_v AS T
				LEFT JOIN SUTTER_1P_DELTA.tbl.Contact_HofH T2 ON T.[NEW_TGT_IMPORT_ID]=T2.NoHH_ImportID  
 			 
    
END; --595 row(s) affected)

		   
 
BEGIN--CHECK DUPLICATES
		SELECT LEN(External_id__c+'-'+ [rC_Bios__Account__r:External_ID__c]) l, (External_id__c+'-'+ [rC_Bios__Account__r:External_ID__c]) nId
			   ,* 
		FROM SUTTER_1P_DELTA.IMP.SALUTATION
		WHERE External_ID__c IN (SELECT External_ID__c FROM SUTTER_1P_DELTA.IMP.SALUTATION GROUP BY External_ID__c HAVING COUNT(*)>1)
		ORDER BY zrefSource, External_ID__c 
 
		UPDATE SUTTER_1P_DELTA.IMP.SALUTATION 
		SET External_ID__c=(External_id__c+'-'+ [rC_Bios__Account__r:External_ID__c])
		WHERE External_ID__c IN (SELECT External_ID__c FROM SUTTER_1P_DELTA.IMP.SALUTATION GROUP BY External_ID__c HAVING COUNT(*)>1)
END 

BEGIN
  			USE SUTTER_1P_DELTA 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'SALUTATION'
 		 
		--SELECT NAME FROM SUTTER_1P_DELTA.IMP.SALUTATION WHERE NAME LIKE '%"%'
		UPDATE SUTTER_1P_DELTA.IMP.SALUTATION SET NAME=REPLACE(NAME,'"','''') where NAME like '%"%'
		UPDATE SUTTER_1P_DELTA.IMP.SALUTATION SET rC_Bios__Salutation_Line_1__c=REPLACE(rC_Bios__Salutation_Line_1__c,'"','''') where rC_Bios__Salutation_Line_1__c like '%"%'
		UPDATE SUTTER_1P_DELTA.IMP.SALUTATION SET rC_Bios__Inside_Salutation__c=REPLACE(rC_Bios__Inside_Salutation__c,'"','''') where rC_Bios__Inside_Salutation__c like '%"%'

END		
	    
 --exceptions

 SELECT T1.*, T2.[EXTERNAL_ID__C] AS zrefErrExt, T2.[RC_BIOS__ACCOUNT__R EXTERNAL_ID__C] AS zrefErrAcc, T2.[RC_BIOS__CONTACT__R EXTERNAL_ID__C] AS zrefErrCon
-- INTO SUTTER_1P_DELTA.IMP.SALUTATION_xcp
 FROM SUTTER_1P_DELTA.IMP.SALUTATION_srt T1
 INNER JOIN  SUTTER_1P_DELTA.[dbo].[zzIMP_SALUTATION_error] T2 ON T1.[External_ID__c]=T2.[External_ID__c]

 
 

  SELECT * FROM SUTTER_1P_DELTA.IMP.SALUTATION 
  WHERE [rC_Bios__Account__r:External_ID__c]='HOTV-15164'