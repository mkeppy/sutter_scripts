 USE [SUTTER_1P_DELTA]
 GO
  		
		DROP TABLE  [SUTTER_1P_DELTA].IMP.NOTES
															   
  --GIFT NOTES (PARENT)
		SELECT	ParentId= X1.ID
				,Title=    	  CASE WHEN T1.RE_DB_OwnerShort IS NOT NULL THEN  COALESCE(' ' + LTRIM(RTRIM(T1.RE_DB_OwnerShort)), '') ELSE ' ' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteType IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.GFNoteType)), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDate IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(SUBSTRING(T1.GFNoteDate, 5,2) +'/'+ SUBSTRING(T1.GFNoteDate, 7,2) +'/'+LEFT(T1.GFNoteDate , 4))), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDesc IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.GFNoteDesc)), '') ELSE '' END
				
				
				,Body =    	  CASE WHEN T1.GFNoteAuth IS NOT NULL THEN  COALESCE('Author: ' + LTRIM(RTRIM(T1.GFNoteAuth)), '') ELSE 'Author: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteTitle IS NOT NULL THEN COALESCE('; Title: ' + LTRIM(RTRIM(T1.GFNoteTitle)), '') ELSE '; Title: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDesc IS NOT NULL THEN COALESCE('; Desc: ' + LTRIM(RTRIM(T1.GFNoteDesc)), '') ELSE '; Desc: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteNotes IS NOT NULL THEN COALESCE('; Notes: ' + LTRIM(RTRIM(CAST(T1.GFNoteNotes AS NVARCHAR(4000)))), '') ELSE '; Notes: N/A' END
				,'GiftsNotes_parent' AS zrefrecordSource	
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.GFLink AS zrefImportID
			    ,X1.External_ID__c AS zrefExternalID
				,NULL AS  zrefNewImportID
		INTO [SUTTER_1P_DELTA].IMP.NOTES
		FROM SUTTER_1P_DELTA.dbo.HC_Gift_Notes_v T1
		INNER JOIN SUTTER_1P_DELTA.dbo.HC_Gift_SplitGift_v T2 ON T1.GFLink=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	    INNER JOIN SUTTER_1P_DELTA.IMP.OPPORTUNITY_PARENT T3 ON (T2.RE_DB_OwnerShort+'-'+T2.RE_DB_Tbl+'-'+T2.GSplitImpID) =T3.External_Id__c
	    INNER JOIN [SUTTER_1P_MIGRATION].XTR.OPPORTUNITY X1 ON T3.External_Id__c=X1.External_ID__c
	     
	    UNION ALL
	--GIFT NOTES (TRANSACTION)
		SELECT	ParentId= X1.ID
				,Title=    	  CASE WHEN T1.RE_DB_OwnerShort IS NOT NULL THEN  COALESCE(' ' + LTRIM(RTRIM(T1.RE_DB_OwnerShort)), '') ELSE ' ' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteType IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.GFNoteType)), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDate IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(SUBSTRING(T1.GFNoteDate, 5,2) +'/'+ SUBSTRING(T1.GFNoteDate, 7,2) +'/'+LEFT(T1.GFNoteDate , 4))), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDesc IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.GFNoteDesc)), '') ELSE '' END

				,Body =    	  CASE WHEN T1.GFNoteAuth IS NOT NULL THEN  COALESCE('Author: ' + LTRIM(RTRIM(T1.GFNoteAuth)), '') ELSE 'Author: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteTitle IS NOT NULL THEN COALESCE('; Title: ' + LTRIM(RTRIM(T1.GFNoteTitle)), '') ELSE '; Title: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDesc IS NOT NULL THEN COALESCE('; Desc: ' + LTRIM(RTRIM(T1.GFNoteDesc)), '') ELSE '; Desc: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteNotes IS NOT NULL THEN COALESCE('; Notes: ' + LTRIM(RTRIM(CAST(T1.GFNoteNotes AS NVARCHAR(4000)))), '') ELSE '; Notes: N/A' END
				,'GiftsNotes_trx' AS zrefrecordSource	
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.GFLink AS zrefImportID
			    ,X1.External_ID__c AS zrefExternalID
				,NULL AS zrefNewImportID
		FROM SUTTER_1P_DELTA.dbo.HC_Gift_Notes_v T1
		INNER JOIN SUTTER_1P_DELTA.dbo.HC_Gift_SplitGift_v T2 ON T1.GFLink=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	    INNER JOIN SUTTER_1P_DELTA.IMP.OPPORTUNITY_TRANSACTION T3 ON (T2.RE_DB_OwnerShort+'-'+T2.RE_DB_Tbl+'-'+T2.GSplitImpID) =T3.External_Id__c
	    INNER JOIN [SUTTER_1P_MIGRATION].XTR.OPPORTUNITY X1 ON T3.External_Id__c=X1.External_ID__c
	   
	   UNION ALL 
		
		--GIFT NOTES (PLANNED GIVING)
		SELECT	ParentId= X1.ID
				
				,Title=    	  CASE WHEN T1.RE_DB_OwnerShort IS NOT NULL THEN  COALESCE(' ' + LTRIM(RTRIM(T1.RE_DB_OwnerShort)), '') ELSE ' ' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteType IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.GFNoteType)), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDate IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(SUBSTRING(T1.GFNoteDate, 5,2) +'/'+ SUBSTRING(T1.GFNoteDate, 7,2) +'/'+LEFT(T1.GFNoteDate , 4))), '') ELSE '-' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDesc IS NOT NULL THEN COALESCE('-' + LTRIM(RTRIM(T1.GFNoteDesc)), '') ELSE '' END

				,Body =    	  CASE WHEN T1.GFNoteAuth IS NOT NULL THEN  COALESCE('Author: ' + LTRIM(RTRIM(T1.GFNoteAuth)), '') ELSE 'Author: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteTitle IS NOT NULL THEN COALESCE('; Title: ' + LTRIM(RTRIM(T1.GFNoteTitle)), '') ELSE '; Title: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteDesc IS NOT NULL THEN COALESCE('; Desc: ' + LTRIM(RTRIM(T1.GFNoteDesc)), '') ELSE '; Desc: N/A' END + CHAR(10) +
							+ CASE WHEN T1.GFNoteNotes IS NOT NULL THEN COALESCE('; Notes: ' + LTRIM(RTRIM(CAST(T1.GFNoteNotes AS NVARCHAR(4000)))), '') ELSE '; Notes: N/A' END
				,'GiftsNotes_trx' AS zrefrecordSource	
				,T1.RE_DB_OwnerShort AS zrefREDB
				,T1.GFLink AS zrefImportID
			    ,X1.RE_GSPLITIMPID__C AS zrefExternalID
				,NULL AS zrefNewImportID
		FROM SUTTER_1P_DELTA.dbo.HC_Gift_Notes_v T1
		INNER JOIN SUTTER_1P_DELTA.dbo.HC_Gift_SplitGift_v T2 ON T1.GFLink=T2.GFImpID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
	    INNER JOIN SUTTER_1P_DELTA.IMP.PLANNED_GIVING T3 ON (T2.RE_DB_OwnerShort+'-'+T2.RE_DB_Tbl+'-'+T2.GSplitImpID) =T3.rC_Giving__External_ID__c
	    LEFT JOIN SUTTER_1P_DELTA.XTR.PLANNED_GIVING X1 ON T3.rC_Giving__External_ID__c=X1.RE_GSPLITIMPID__C

			

BEGIN--remove quotes
		USE SUTTER_1P_DELTA 
 		EXEC sp_FindStringInTable '%"%', 'IMP', 'NOTES'
	 	UPDATE SUTTER_1P_DELTA.IMP.NOTES SET Body=REPLACE(Body,'"','''') where Body like '%"%'
		UPDATE SUTTER_1P_DELTA.IMP.NOTES SET Title=REPLACE(Title,'"','''') where Title like '%"%'
		
		SELECT TOP 10 * FROM SUTTER_1P_DELTA.IMP.NOTES
		
		SELECT LEN(body) l, * FROM SUTTER_1P_DELTA.IMP.NOTES 
		WHERE LEN(body)>32000
		ORDER BY l asc

		UPDATE [SUTTER_1P_MIGRATION].IMP.NOTE SET Body=[Title]
		WHERE LEN(body)>32000
END