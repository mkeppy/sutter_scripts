/*  CONSTITUENTS (Bio 1 and Bio 2)	New Constituents: Includes only bigraphical data on Bio 1 and Bio 2 tabs (Names, birthdate and deceased date). 
									It does not include other dependent record types such as Cons Attributes, ConsNotes, etc.
									Existing Constituents: Only new records on existing constituents will be included in the Delta. 
									This includes new addresses, gifts, phones on new addresses and new actions. If any other changes are made, users will need to manually add this data post go live.
	CONSTITUENT ADDRESSES			Any NEW address records on both NEW and EXISTING constituents.
	CONSTITUENT CODES				These will be added on NEW constituent only. Updates and new values on existing RE constituents will NOT be included.
	CONSTITUENT SOLICIT CODES		These will be added on NEW constituent only. Updates and new values on existing RE constituents will NOT be included.
	PHONES							Phones: Only phone records added to NEW Address records will be migrated in the Delta Load. 
									Users should not update phones on existing address records in RE during the Delta. If new ones are added, users will need to manually add these to RE post go live.
	ACTIONS							Existing Actions: Users should not update any existing Actions in RE after the final data is sent. 
									New Actions: Only new action records will be included in the Delta Load for both new and existing constituents.
 	GIFTS							Gift Adjustments and Ammendments: No gift adjustments or ammendments will be migrated in the Delta Load. If users enter these in RE to facilitate reconciliation, they should run reports in RE and manually adjust the gifts in NGOC following Go Live for data entered July 25, 2016 � August 23, 2016.
	GIFT ATTRIBUTES					New Gifts: Existing Gift Attributes on NEW gifts will be migrated during the Delta. 
									Existing Gifts: If SH users add any existing gift attributes to existing gifts, those will not be migrated onto the gift in NGOC.
	GIFT NOTES						New Gifts: Gift Notes on NEW gifts will be migrated during the Delta. 
									Existing Gifts: If SH users add any gift notes to existing gifts, those will not be migrated onto the gift in NGOC.
*/
 		   										
USE [SUTTER_1P_DELTA]
GO

--CONSTITUENTS -- NEW RECORDS
	/*used new constituents based on comparison of final data vs complete data for the delta.	
		SELECT * 
		--INTO [SUTTER_1P_DELTA].DBO.[HC_Constituents_v]	
		FROM [SUTTER_1P_DATA].DBO.[HC_Constituents_v]
		WHERE CAST([DateAdded] AS DATE) > '07/24/2016'
		ORDER BY CAST([DateAdded] AS DATE) DESC
	*/

	DROP TABLE [SUTTER_1P_DELTA].DBO.[HC_Constituents_v]	
	
	SELECT T1.* 
	INTO [SUTTER_1P_DELTA].DBO.[HC_Constituents_v]	
	FROM [SUTTER_1P_DATA].DBO.[HC_Constituents_v] AS T1
	LEFT JOIN [SUTTER_1P_DELTA].[dbo].HC_Constituents_v_final AS T2 ON T1.[ImportID]=T2.[ImportID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
	WHERE (T2.[ImportID] IS NULL AND T2.[RE_DB_OwnerShort] IS NULL)
	ORDER BY CAST(T1.[DateAdded] AS DATE) ASC
	--count: (584 row(s) affected)
    

--CONSTITUENT ADDRESS -- NEW RECORDS ON NEW CONS AND EXISTING CONST
	DROP TABLE [SUTTER_1P_DELTA].dbo.[HC_Cons_Address_v]
	
	SELECT *
	INTO [SUTTER_1P_DELTA].DBO.[HC_Cons_Address_v]	
 	FROM [SUTTER_1P_DATA].DBO.[HC_Cons_Address_v]  
	WHERE (CAST([DateAdded] AS DATE) > '07/24/2016') 
	OR ([RE_DB_OwnerShort]='SCAH' AND Importid='03038-079-0010387')
	OR ([RE_DB_OwnerShort]='ABSF' and ImportID='02869-593-0000338496')
	OR ([RE_DB_OwnerShort]='CVRX' and ImportID='00001-593-0000053076')
	OR ([RE_DB_OwnerShort]='SMCF' and ImportID='03947-593-0000138205')
	OR ([RE_DB_OwnerShort]='WBRX' and ImportID='05502-593-0000310423')
	OR ([RE_DB_OwnerShort]='HOTV' and ImportID='00001-593-0000078228')
	OR ([RE_DB_OwnerShort]='SMCF' and ImportID='03947-593-0000146055')
	OR ([RE_DB_OwnerShort]='SCAH' and ImportID='03038-593-0000291686')
	OR ([RE_DB_OwnerShort]='HOTV' and ImportID='00001-593-0000079535')	 
	ORDER BY CAST([DateAdded] AS DATE) ASC
	-- (635 row(s) affected)  9 individual records that are new but not with the delta load timing. 
    
	--check
	   SELECT [T1].[RE_DB_OwnerShort], [T1].[ImportID], [T1].[DateAdded], [T1].[DateLastChanged] 
	   FROM [SUTTER_1P_DELTA].dbo.[HC_Constituents_v] AS T1
	   LEFT JOIN [SUTTER_1P_DELTA].dbo.[HC_Cons_Address_v] AS T2 ON T1.[ImportID]=T2.[ImportID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
		WHERE T2.[ImportID] IS null
	   ORDER BY CAST(T1.[DateAdded] AS DATE) ASC


--CONSTITUENCY CODES -- NEW RECORDS ON NEW CONS.
	DROP TABLE [SUTTER_1P_DELTA].DBO.[HC_Cons_ConsCode_v]	
	
	SELECT  T1.*
	INTO [SUTTER_1P_DELTA].DBO.[HC_Cons_ConsCode_v]	
	FROM [SUTTER_1P_DELTA].DBO.[HC_Constituents_v] AS T
	INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Cons_ConsCode_v] AS T1 ON T.[ImportID]=T1.[ImportID] AND T.[RE_DB_OwnerShort]=T1.[RE_DB_OwnerShort]
	WHERE T1.[ConsCodeImpID] IS NOT null 
	 --(461 row(s) affected)


--CONSTITUENCY ATTRIBUTES -- NEW RECORDS ON NEW CONS. (MK: marcelo added in case SH asks for it later) 
	DROP TABLE  [SUTTER_1P_DELTA].DBO.[HC_Cons_Attributes_v]
	
	SELECT  T1.*
	INTO [SUTTER_1P_DELTA].DBO.[HC_Cons_Attributes_v]
	FROM [SUTTER_1P_DELTA].DBO.[HC_Constituents_v] AS T
	INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Cons_Attributes_v] AS T1 ON T.[ImportID]=T1.[ImportID] AND T.[RE_DB_OwnerShort]=T1.[RE_DB_OwnerShort]
  	 --	  (205 row(s) affected)

--CONSTITUENCY SOLICIT CODES -- NEW RECORDS ON NEW CONS.
	DROP TABLE  [SUTTER_1P_DELTA].DBO.[HC_Cons_SolicitCodes_v]

	SELECT  T1.*
	INTO [SUTTER_1P_DELTA].DBO.[HC_Cons_SolicitCodes_v]	
	FROM [SUTTER_1P_DELTA].DBO.[HC_Constituents_v] AS T
	INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Cons_SolicitCodes_v] AS T1 ON T.[ImportID]=T1.[ImportID] AND T.[RE_DB_OwnerShort]=T1.[RE_DB_OwnerShort]
 	--(152 row(s) affected)

--CONSTITUENT PHONES  - NEW RECORDS ON NEW ADDRESS RECORDS 
	 DROP TABLE [SUTTER_1P_DELTA].DBO.[HC_Cons_Phone_v] 

	SELECT T1.*
	INTO [SUTTER_1P_DELTA].DBO.[HC_Cons_Phone_v] 
	FROM [SUTTER_1P_DATA].DBO.[HC_Cons_Phone_v] AS T1
	INNER JOIN [SUTTER_1P_DELTA].DBO.[HC_Cons_Address_v]  AS T2 ON T1.[PhoneAddrImpID]=T2.[AddrImpID] AND T1.[RE_DB_OwnerShort] = T2.[RE_DB_OwnerShort]
 	UNION ALL 
	--CVRX/WBRX ON RE 7.95
	SELECT T1.*
	FROM [SUTTER_1P_DATA].DBO.[HC_Cons_Phone_v] AS T1	
	INNER JOIN [SUTTER_1P_DELTA].DBO.[HC_Constituents_v]  AS T2 ON T1.[ImportID]=T2.[ImportID] AND T1.[RE_DB_OwnerShort] = T2.[RE_DB_OwnerShort]
	WHERE  	T1.[PhoneAddrImpID] IS NULL 
	 -- (273 row(s) affected)


-- CONSTITUENT ACTIONS	  - NEW RECORDS	ON NEW CONS AND EXISTING CONS 
	DROP TABLE SUTTER_1P_DELTA.DBO.[HC_Cons_Action_v]

	SELECT * 
	INTO [SUTTER_1P_DELTA].DBO.[HC_Cons_Action_v]
	FROM [SUTTER_1P_DATA].DBO.[HC_Cons_Action_v]
	WHERE CAST([DateAdded] AS DATE) > '07/24/2016' 
	--228



--- CONSTITUENT ACTION SOLICITOR
	DROP TABLE [SUTTER_1P_DELTA].dbo.HC_Cons_Action_Solicitor_v	
	
	SELECT T1.* 
	INTO [SUTTER_1P_DELTA].dbo.HC_Cons_Action_Solicitor_v
	FROM [SUTTER_1P_DATA].dbo.HC_Cons_Action_Solicitor_v  AS T1
	INNER JOIN [SUTTER_1P_DELTA].DBO.[HC_Cons_Action_v] AS T2 ON T1.[ACImpID]=T2.[ACImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
	 --(61 row(s) affected)


 --CAMP FUND PACK APPEAL

 SELECT * 
 INTO [SUTTER_1P_DELTA].dbo.[HC_Appeal_v]
 FROM [SUTTER_1P_DATA].dbo.[HC_Appeal_v]

 SELECT * 
 INTO [SUTTER_1P_DELTA].dbo.[HC_Campaign_v]
 FROM [SUTTER_1P_DATA].dbo.[HC_Campaign_v]

 SELECT *
 INTO [SUTTER_1P_DELTA].dbo.[HC_Package_v] 
 FROM [SUTTER_1P_DATA].dbo.[HC_Package_v]

 SELECT * 
 INTO [SUTTER_1P_DELTA].dbo.[HC_Fund_v]
 FROM [SUTTER_1P_DATA].dbo.[HC_Fund_v]
	    

--GIFTS -- NEW RECORDS ON NEW CONST AND EXISTING CONST.	  
 	/* same as with Constituents.  
	SELECT * 
	--INTO [SUTTER_1P_DELTA].DBO.[HC_Gift_v]
	FROM [SUTTER_1P_DATA].DBO.[HC_Gift_v] 
	WHERE CAST([DateAdded] AS DATE) > '07/24/2016'
	ORDER BY CAST([DateAdded] AS DATE) DESC
	--9304						   */ --did not use date. there were a handful more records by comparing new vs old data tables by Ids,

	DROP TABLE [SUTTER_1P_DELTA].DBO.[HC_Gift_v]

	SELECT T1.* 
	INTO [SUTTER_1P_DELTA].DBO.[HC_Gift_v]
	FROM [SUTTER_1P_DATA].DBO.[HC_Gift_v] AS T1
	LEFT JOIN [SUTTER_1P_MIGRATION].TBL.GIFT AS T2 ON T1.[GFImpID]=T2.[GSplitGFImpID] AND T1.[RE_DB_OwnerShort]= T2.[GSplitRE_DB_OwnerShort]
	WHERE T2.[GSplitGFImpID] IS null
	--(9337 row(s) affected) (vs. 9,304 based on the date)

	--GIFTS SPLIT -- NEW RECORDS ON NEW CONST AND EXISTING CONST.	  
		DROP TABLE [SUTTER_1P_DELTA].DBO.[HC_Gift_SplitGift_v] 		
		
		SELECT T2.*
		INTO [SUTTER_1P_DELTA].DBO.[HC_Gift_SplitGift_v]
		FROM [SUTTER_1P_DELTA].DBO.[HC_Gift_v] AS T1
		INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Gift_SplitGift_v] AS T2 ON T1.[GFImpID]=T2.[GFImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
	 	--(9879 row(s) affected) 

	 --GIFT LINK  (need to include the entire table)
		DROP TABLE  [SUTTER_1P_DELTA].DBO.[HC_Gift_Link_v]

		SELECT *
		INTO [SUTTER_1P_DELTA].DBO.[HC_Gift_Link_v]
		FROM [SUTTER_1P_DATA].DBO.[HC_Gift_Link_v] 	   

	--GIFT WRITE OFF 
		DROP TABLE [SUTTER_1P_DELTA].DBO.[HC_Gift_WriteOff_v]
		
		SELECT T1.*
		INTO [SUTTER_1P_DELTA].DBO.[HC_Gift_WriteOff_v]
		FROM [SUTTER_1P_DATA].DBO.[HC_Gift_WriteOff_v] AS T1
	 	WHERE CAST(T1.[DateAdded] AS DATE) > '07/24/2016'
		ORDER BY CAST(T1.[DateAdded] AS DATE) DESC

	--GIFT INSTALLMENT	
	
		DROP TABLE [SUTTER_1P_DELTA].DBO.[HC_Gift_Installments_v]
			
		SELECT T1.*
		INTO [SUTTER_1P_DELTA].DBO.[HC_Gift_Installments_v]
		FROM [SUTTER_1P_DATA].DBO.[HC_Gift_Installments_v] AS T1
		INNER JOIN [SUTTER_1P_DELTA].DBO.HC_gift_v AS T2 ON T2.[GFImpID]=T1.[GFLink] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
	 	--(123 row(s) affected)
    
				  
--GIFT ATTRIBUTE  ---NEW RECORDS ON NEW GIFTS
		DROP TABLE [SUTTER_1P_DELTA].DBO.[HC_Gift_Attr_v] 		
		
		SELECT T2.*
	--	INTO [SUTTER_1P_DELTA].DBO.[HC_Gift_Attr_v]
		FROM [SUTTER_1P_DELTA_migration].DBO.[HC_Gift_v] AS T1
		INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Gift_Attr_v] AS T2 ON T1.[GFImpID]=T2.[GFImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
	 	-- (4092 row(s) affected)
					 
--GIFT NOTES	--NEW RECORDS ON NEW GIFTS
		DROP TABLE [SUTTER_1P_DELTA].DBO.[HC_Gift_Notes_v]
	 
 		SELECT T2.*
		--INTO [SUTTER_1P_DELTA].DBO.[HC_Gift_Notes_v]
		FROM [SUTTER_1P_DELTA_migration].DBO.[HC_Gift_v] AS T1
		INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Gift_Notes_v] AS T2 ON T1.[GFImpID]=T2.[GFLink] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
		--(66 row(s) affected) 
   
 --GIFT SOFT_CREDITS	--NEW RECORDS ON NEW GIFTS
		DROP TABLE [SUTTER_1P_DELTA].DBO.[HC_Gift_SoftCredit_v]
	 
 		SELECT T2.*
		INTO [SUTTER_1P_DELTA_migration].DBO.[HC_Gift_SoftCredit_v]
		FROM [SUTTER_1P_DELTA_migration].DBO.[HC_Gift_v] AS T1
		INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Gift_SoftCredit_v] AS T2 ON T1.[GFImpID]=T2.[GFLink] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
		--(294 row(s) affected) 

 --GIFT TRIBUTE	--NEW RECORDS ON NEW GIFTS
		DROP TABLE [SUTTER_1P_DELTA].DBO.[HC_Gift_Tribute_v]
	 
 		SELECT T2.*
	 	INTO [SUTTER_1P_DELTA_migration].DBO.[HC_Gift_Tribute_v]
		FROM [SUTTER_1P_DELTA_migration].DBO.[HC_Gift_v] AS T1
		INNER JOIN [SUTTER_1P_DATA].DBO.[HC_Gift_Tribute_v] AS T2 ON T1.[GFImpID]=T2.[GFTLink] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
		--(689 row(s) affected) 

--XTR_ACCOUNT
	SELECT X.* 
	INTO [SUTTER_1P_DELTA].dbo.XTR_ACCOUNT
	FROM [SUTTER_1P_DELTA].dbo.[HC_Constituents_v] AS T
	INNER JOIN [SUTTER_1P_MIGRATION].xtr.[ACCOUNT] AS X ON T.[NEW_TGT_IMPORT_ID]=X.[EXTERNAL_ID__C]

	
--XTR_CONTACT
	SELECT X.* 
	INTO [SUTTER_1P_DELTA].dbo.XTR_CONTACT
	FROM [SUTTER_1P_DELTA].dbo.[HC_Constituents_v] AS T
	INNER JOIN [SUTTER_1P_MIGRATION].xtr.[CONTACT] AS X ON T.[NEW_TGT_IMPORT_ID]=X.[EXTERNAL_ID__C]
