	   
USE [SUTTER_1P_DELTA]
GO


/****************************************************************************************/	
--ADD RE_DB_Id to table to use for linking with combo external Id. 
-- this is only required when XTR files are needed for Data Loader Inserts.

BEGIN--ADD RE_DB_ID field
		--CONSTITUENT 
			ALTER TABLE [SUTTER_1P_DELTA].dbo.HC_Constituents_v
			ADD RE_DB_Id VARCHAR(30)
				
			UPDATE [SUTTER_1P_DELTA].dbo.HC_Constituents_v
			SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
						
		--CONSTITUENT ADDRESS 
			ALTER TABLE [SUTTER_1P_DELTA].dbo.HC_Cons_Address_v
			ADD RE_DB_Id VARCHAR(30)
						 
			UPDATE [SUTTER_1P_DELTA].dbo.HC_Cons_Address_v
			SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
							 
		--CONSTITUENT CODE 
			ALTER TABLE [SUTTER_1P_DELTA].DBO.HC_Cons_ConsCode_v
			ADD RE_DB_Id VARCHAR(30)	
			 			
			UPDATE [SUTTER_1P_DELTA].dbo.HC_Cons_ConsCode_v
			SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID

		--CONSTITUENT CODE 
			ALTER TABLE [SUTTER_1P_DELTA].DBO.HC_Cons_SolicitCodes_v
			ADD RE_DB_Id VARCHAR(30)	
			 			
			UPDATE [SUTTER_1P_DELTA].dbo.HC_Cons_SolicitCodes_v
			SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
		  				 
		--GIFT
			ALTER TABLE [SUTTER_1P_DELTA].dbo.HC_Gift_v
			ADD RE_DB_Id VARCHAR(30)
						
			UPDATE [SUTTER_1P_DELTA].dbo.HC_Gift_v
			SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
				  
		--CONS ACTION 
			ALTER TABLE [SUTTER_1P_DELTA].dbo.HC_Cons_Action_v
			ADD RE_DB_Id VARCHAR(30)
						 
			UPDATE [SUTTER_1P_DELTA].dbo.HC_Cons_Action_v
			SET RE_DB_Id=RE_DB_OwnerShort+'-'+ImportID
END 					 
 
BEGIN--NEW_TGT_IMPORT_ID: Constituents
 
	ALTER TABLE [SUTTER_1P_DELTA].dbo.HC_Constituents_v
	ADD NEW_TGT_IMPORT_ID VARCHAR(50)
	GO			
    UPDATE  [SUTTER_1P_DELTA].dbo.HC_Constituents_v 
	SET NEW_TGT_IMPORT_ID=RE_DB_Id
	GO 

END;
 
BEGIN--UPDATE CONS ADDRESS COUNTRY

	/*SELECT [AddrState], [AddrCountry], count(*) 
	FROM [SUTTER_1P_DELTA].DBO.HC_Cons_Address_v
	WHERE --country is null and 
	(AddrState ='AL' or AddrState='AK' or AddrState='AZ' or AddrState='AR'  or AddrState='CA' or AddrState='CO' or AddrState='CT' or AddrState='DE' or AddrState='DC' or AddrState='FL'
	or AddrState='GA' or AddrState='HI' or AddrState='ID' or AddrState='IL' or AddrState='IN' or AddrState='IA' or AddrState='KS' or AddrState='KY' or AddrState='LA' or AddrState='ME' or AddrState='MD'
	or AddrState='MA' or AddrState='MI' or AddrState='MN' or AddrState='MS' or AddrState='MO' or AddrState='MT' or AddrState='NE' or AddrState='NV' or AddrState='NH' or AddrState='NJ' or AddrState='NM' 
	or AddrState='NY' or AddrState='NC' or AddrState='ND' or AddrState='OH' or AddrState='OK' or AddrState='OR' or AddrState='PA' or AddrState='RI' or AddrState='SC' or AddrState='SD' or AddrState='TN'
	or AddrState='TX' or AddrState='UT' or AddrState='VT' or AddrState='VA' or AddrState='WA' or AddrState='WV' or AddrState='WI' or AddrState='WY')
	GROUP BY [AddrState], [AddrCountry]   --'United States'
	*/ 	 
	UPDATE [SUTTER_1P_DELTA].DBO.HC_Cons_Address_v SET [AddrCountry] = 'United States' WHERE --country is null AND 
	(AddrState ='AL' or AddrState='AK' or AddrState='AZ' or AddrState='AR'  or AddrState='CA' or AddrState='CO' or AddrState='CT' or AddrState='DE' or AddrState='DC' or AddrState='FL'
	or AddrState='GA' or AddrState='HI' or AddrState='ID' or AddrState='IL' or AddrState='IN' or AddrState='IA' or AddrState='KS' or AddrState='KY' or AddrState='LA' or AddrState='ME' or AddrState='MD'
	or AddrState='MA' or AddrState='MI' or AddrState='MN' or AddrState='MS' or AddrState='MO' or AddrState='MT' or AddrState='NE' or AddrState='NV' or AddrState='NH' or AddrState='NJ' or AddrState='NM' 
	or AddrState='NY' or AddrState='NC' or AddrState='ND' or AddrState='OH' or AddrState='OK' or AddrState='OR' or AddrState='PA' or AddrState='RI' or AddrState='SC' or AddrState='SD' or AddrState='TN'
	or AddrState='TX' or AddrState='UT' or AddrState='VT' or AddrState='VA' or AddrState='WA' or AddrState='WV' or AddrState='WI' or AddrState='WY')

END;

BEGIN--UPDATE CONS ATTR DESC TO 'NULL'

		SELECT * FROM [SUTTER_1P_DELTA].DBO.HC_Cons_Attributes_v WHERE CAttrDesc IS NULL
		UPDATE [SUTTER_1P_DELTA].DBO.HC_Cons_Attributes_v SET CAttrDesc='NULL' WHERE CAttrDesc IS NULL	

END; 

BEGIN--UPDATE GIFT ATTR DESC TO 'NULL'

		SELECT * FROM [SUTTER_1P_DELTA].DBO.[HC_Gift_Attr_v] WHERE [GFAttrDesc] IS NULL
		UPDATE [SUTTER_1P_DELTA].DBO.[HC_Gift_Attr_v] SET [GFAttrDesc]='NULL' WHERE [GFAttrDesc] IS NULL	

END;

BEGIN--UPDATE GIFT: CAMPAIGN, APPEAL, PACKAGE TO "NULL"

	--	SELECT GSplitCamp, GSplitAppeal, GSplitPkg, GSplitFund
	--	FROM [SUTTER_1P_DELTA].DBO.HC_Gift_SplitGift_v
	--	WHERE GSplitCamp IS NULL OR GSplitAppeal IS NULL OR GSplitPkg IS NULL OR GSplitFund	IS NULL
	--	GROUP BY GSplitCamp, GSplitAppeal, GSplitPkg, GSplitFund
		
	UPDATE [SUTTER_1P_DELTA].DBO.HC_Gift_SplitGift_v SET GSplitCamp='NULL' WHERE GSplitCamp IS NULL
	UPDATE [SUTTER_1P_DELTA].DBO.HC_Gift_SplitGift_v SET GSplitAppeal='NULL' WHERE GSplitAppeal IS NULL
	UPDATE [SUTTER_1P_DELTA].DBO.HC_Gift_SplitGift_v SET GSplitPkg='NULL' WHERE GSplitPkg IS NULL
		
END;
 
BEGIN--UPDATE CREDIT CARD TYPE
		--SELECT GFPayMeth, GFCCType, COUNT(*) C 
		--FROM [SUTTER_1P_DELTA].DBO.HC_Gift_v
		--GROUP BY GFPayMeth, GFCCType
		--ORDER BY GFPayMeth, GFCCType
		
	UPDATE [SUTTER_1P_DELTA].DBO.HC_Gift_v SET GFCCType='MasterCard' WHERE GFCCType='Master Card'
	UPDATE [SUTTER_1P_DELTA].DBO.HC_Gift_v SET GFCCType='American Express' WHERE GFCCType='Amer Express' OR [GFCCType]='Amex'
	UPDATE [SUTTER_1P_DELTA].DBO.HC_Gift_v SET GFCCType='Discover' WHERE GFCCType='DISC' 
	UPDATE [SUTTER_1P_DELTA].DBO.HC_Gift_v SET GFCCType=NULL WHERE GFCCType='Visa' AND GFPayMeth='Personal Check'
	UPDATE [SUTTER_1P_DELTA].DBO.HC_Gift_v SET GFCCType=NULL WHERE (GFCCType='No credit card'   OR GFCCType='PAYD' OR GFCCType='VCHR' OR GFCCType='?' OR [GFCCType]='Credit Card' OR [GFCCType]='MACC')
		
END; 

BEGIN--UPDATE payment info where paytype is not credit card. 

		/*SELECT GFImpId, GFPayMeth, GFCCType, GFCCNum, GFCardholderName, GFCCExpOn, GFAuthCode
		FROM [SUTTER_1P_DELTA].dbo.hc_gift_v 
		WHERE GFPayMeth!='Credit Card' AND (GFCCType IS NOT NULL 
											OR GFCCNum IS NOT NULcIS NOT NULL 
											OR GFCCExpOn IS NOT NULL
											OR GFAuthCode IS NOT NULL)
		*/
	UPDATE [SUTTER_1P_DELTA].dbo.hc_gift_v SET GFCCNum=NULL, GFCardholderName=NULL
	WHERE GFPayMeth!='Credit Card' AND (GFCCType IS NOT NULL 
										OR GFCCNum IS NOT NULL 
										OR GFCardholderName IS NOT NULL 
										OR GFCCExpOn IS NOT NULL
										OR GFAuthCode IS NOT NULL)
END; 	

BEGIN--CONS_ACTION AddedBY to use on chart_actionsolicitor column addedby
		/*SELECT * FROM [[SUTTER_1P_DELTA]].dbo.[HC_Cons_Action_v]
		WHERE [AddedBy] IS NULL
		*/
		UPDATE  [SUTTER_1P_DELTA].dbo.[HC_Cons_Action_v]
		SET [AddedBy]='NULL'
		WHERE [AddedBy] IS NULL
END;
   
 BEGIN--CONS_ACTION TYPE   
		UPDATE  [SUTTER_1P_DELTA].dbo.[HC_Cons_Action_v]
		SET [ACType]='NULL'
		WHERE [ACType] IS NULL
END;
   