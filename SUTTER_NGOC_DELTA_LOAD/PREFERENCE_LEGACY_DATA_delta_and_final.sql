BEGIN--BASE TABLE PREFERENCES 													 

		--CONSTITUENT ATTRIBUTE from CHART_ATTRIBUTES 
				
				--HC_Cons_Attributes_v
				SELECT DISTINCT 
				rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.CAttrImpID)
				,RE_Category__c= T.CAttrCat
				,RE_Description__c=T.[CAttrDesc]

				--reference
 				,'ConsAttribute_1' AS zrefRecSource
				INTO SUTTER_1P_DELTA_migration.IMP.PREFERENCE_UPDATE_legacy_delta
				FROM SUTTER_1P_DELTA_migration.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_1P_DELTA_migration.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_DELTA_migration.tbl.Contact_HofH T3 ON T1.NEW_TGT_IMPORT_ID=T3.NoHH_ImportID  
				INNER JOIN SUTTER_1P_DELTA_migration.dbo.CHART_Attributes T2 ON T.CAttrCat=T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.SF_Object_1='RC_BIOS_PREFERENCE__C' AND T.CAttrDesc IS NOT NULL AND T2.[Convert]='Yes'
		   
			UNION ALL 
				
		--CONSTITUENT ATTRIBUTE from CHART_CONS_ATTRIBUTES 
				SELECT DISTINCT 
				rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.CAttrImpID)
				,RE_Category__c= T.CAttrCat
				,RE_Description__c=T.[CAttrDesc]
		    
				--reference
				,'ConsAttribute_2' AS zrefRecSource
				FROM SUTTER_1P_DELTA_migration.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_1P_DELTA_migration.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_DELTA_migration.tbl.Contact_HofH T3 ON T1.NEW_TGT_IMPORT_ID=T3.NoHH_ImportID  
				INNER JOIN SUTTER_1P_DELTA_migration.dbo.CHART_ConsAttributes T2 ON T.CAttrCat=T2.CAttrCat AND T.CAttrDesc=T2.CAttrDesc AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.PREFERENCE_RecordType IS NOT NULL AND T2.PREFERENCE_RecordType !='' AND T2.[Convert]='Yes'
				 
			UNION ALL 
				
		--CONSTITUENT CODE 
				SELECT DISTINCT 
				rC_Bios__External_ID__c= CASE WHEN T.ConsCodeImpID IS NULL THEN T.RE_DB_Tbl+'-'+T1.NEW_TGT_IMPORT_ID ELSE (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.ConsCodeImpID) END 
				,RE_Category__c= T.ConsCode
				,RE_Description__c=T.[ConsCodeDesc]
 
				--reference
				,'ConsCode' AS zrefRecSource
				FROM SUTTER_1P_DELTA_migration.dbo.HC_Cons_ConsCode_v T
				INNER JOIN SUTTER_1P_DELTA_migration.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_1P_DELTA_migration.dbo.CHART_ConstituencyCodes T2 ON T.ConsCode=T2.ConsCode AND T1.KeyInd=T2.KeyInD AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.[Convert]='Yes'
	  		
			UNION ALL 	 
		
		--CONSTITUENT SOLICIT CODE 
				SELECT DISTINCT 
				rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+CAST(T.ID AS NVARCHAR(20)))
				,RE_Category__c= T.[SolicitCode]
				,RE_Description__c=NULL
 
				--reference
				,'ConsSolCode' AS zrefRecSource
				FROM SUTTER_1P_DELTA_migration.dbo.HC_Cons_SolicitCodes_v T
				INNER JOIN SUTTER_1P_DELTA_migration.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_1P_DELTA_migration.dbo.CHART_ConsSolicitCodes T2 ON T.SolicitCode=T2.SolicitCode AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.[Convert]='Yes' AND T2.SF_Object_1 ='RC_BIOS_PREFERENCE__C'

END 		

BEGIN--final data

		--CONSTITUENT CODE 
				SELECT DISTINCT 
				 rC_Bios__External_ID__c= CASE WHEN T.ConsCodeImpID IS NULL THEN T.RE_DB_Tbl+'-'+T1.NEW_TGT_IMPORT_ID ELSE (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.ConsCodeImpID) END 
				,RE_Category__c= T.ConsCode
				,RE_Description__c=T.[ConsCodeDesc]

				--reference
				,T.ConsCodeImpID AS zrefRecImportID
				,'ConsCode' AS zrefRecSource
				INTO [SUTTER_1P_MIGRATION].IMP.PREFERENCE_UPDATE_legacy_final
				FROM SUTTER_1P_DATA.dbo.HC_Cons_ConsCode_v T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_1P_DATA.dbo.CHART_ConstituencyCodes T2 ON T.ConsCode=T2.ConsCode AND T1.KeyInd=T2.KeyInD AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				INNER JOIN [SUTTER_1P_MIGRATION].IMP.[PREFERENCE] T3 ON  (CASE WHEN T.ConsCodeImpID IS NULL THEN T.RE_DB_Tbl+'-'+T1.NEW_TGT_IMPORT_ID ELSE (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.ConsCodeImpID) END)=T3.rC_Bios__External_ID__c
				WHERE T2.[Convert]='Yes'
 			UNION ALL 	 
 		--CONSTITUENT SOLICIT CODE 
				SELECT DISTINCT 
				 rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+CAST(T.ID AS NVARCHAR(20)))
				,RE_Category__c= T.[SolicitCode]
				,RE_Description__c=NULL
 
  				--reference
 				,CAST(T.ID AS NVARCHAR(20))AS zrefRecImportID
				,'ConsSolCode' AS zrefRecSource
				FROM SUTTER_1P_DATA.dbo.HC_Cons_SolicitCodes_v T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_1P_DATA.dbo.CHART_ConsSolicitCodes T2 ON T.SolicitCode=T2.SolicitCode AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				INNER JOIN [SUTTER_1P_MIGRATION].IMP.[PREFERENCE] T3 ON  (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+CAST(T.ID AS NVARCHAR(20)))=T3.rC_Bios__External_ID__c
 				WHERE T2.[Convert]='Yes' AND T2.SF_Object_1 ='RC_BIOS_PREFERENCE__C'
END 		

	    BEGIN 
			USE SUTTER_1P_DELTA_migration 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'PREFERENCE_UPDATE_legacy_delta'
 		END 

		UPDATE SUTTER_1P_DELTA_migration.[IMP].PREFERENCE_UPDATE_legacy_delta SET [RE_Description__c]=REPLACE([RE_Description__c],'"','''') where [RE_Description__c] like '%"%'

 
	    BEGIN 
			USE [SUTTER_1P_MIGRATION] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'PREFERENCE_UPDATE_legacy_final'
 		END 

		UPDATE [SUTTER_1P_MIGRATION].[IMP].PREFERENCE_UPDATE_legacy_final SET [RE_Description__c]=REPLACE([RE_Description__c],'"','''') where [RE_Description__c] like '%"%'

