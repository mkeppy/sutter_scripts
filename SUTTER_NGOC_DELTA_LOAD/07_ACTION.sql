					USE SUTTER_1P_DELTA
GO

BEGIN
		DROP TABLE SUTTER_1P_DELTA.IMP.[ACTION]	
		DROP TABLE SUTTER_1P_DELTA.IMP.[ACTION_PARTICIPANT]
		
END
 

BEGIN
		--ACTION object
			SELECT DISTINCT 
					[Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T1.[NEW_TGT_IMPORT_ID] 
								WHEN (T1.KeyInd='I' AND T12.NoHH_ImportID IS NOT NULL) THEN T12.HH_ImportID ELSE T1.NEW_TGT_IMPORT_ID END
			
					,External_ID__c=CAST(T.RE_DB_OwnerShort+'-'+T.ACImpID AS NVARCHAR(30))
					,RE_ACImpID__c=CAST(T.RE_DB_OwnerShort+'-'+T.ACImpID AS NVARCHAR(30))
					,Date__c=T.ACDate  
					,Attribute_Category__c=T.ACCat  
		 			,T2.Move__c
					,CASE WHEN T.ACStatus ='NULL' THEN NULL ELSE T.[ACStatus] END AS Archived_Status__c
					,[Subject]= T3.Stage__c  
					,T3.Stage__c
					,CASE WHEN T.AddedBy ='NULL' THEN NULL ELSE T.AddedBy END AS RE_Action_Added_By__c
					,T.RE_DB_OwnerShort AS Affiliation__c
					,[Status]=CASE WHEN (T.ACDate < GETDATE()) THEN 'Completed' ELSE 'In Progress' END
	 	
					--action solicitor
					,CAST(T4.RE_Action_Solicitor__c  AS VARCHAR(4000)) AS RE_Action_Solicitor__c
			 		
					--reference
					,T1.KeyInd zref_KeyInd
					,T.AddedBy zref_AddedBy
					,T.[RE_DB_OwnerShort] zref_RE_DB_OwnerShort
					,NULL AS zrefSeq
		 
		 		INTO SUTTER_1P_DELTA.IMP.[ACTION]	
				FROM SUTTER_1P_DELTA.DBO.HC_Cons_Action_v T
				INNER JOIN SUTTER_1P_DELTA.DBO.HC_Constituents_v T1 ON T.RE_DB_ID=T1.RE_DB_ID 
		 		LEFT JOIN SUTTER_1P_DELTA.tbl.Contact_HofH T12 ON T1.[NEW_TGT_IMPORT_ID]=T12.NoHH_ImportID  
				LEFT JOIN SUTTER_1P_DELTA.DBO.CHART_ActionStatus T2 ON T.ACStatus=T2.ACStatus AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_DELTA.DBO.CHART_ActionType T3 ON T.ACType=T3.ACType AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_DELTA.TBL.Cons_Action_Solicitor_1 T4 ON T.ACImpID=T4.ACImpID AND T.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_DELTA.DBO.XTR_ACCOUNT X1 ON T1.NEW_TGT_IMPORT_ID=X1.External_Id__c
				LEFT JOIN SUTTER_1P_DELTA.DBO.XTR_CONTACT X2 ON T1.NEW_TGT_IMPORT_ID=X2.External_Id__c
				WHERE T3.[Convert]='Yes' AND T.[ProposalImpID] IS NOT NULL AND T.[ProposalImpID]!='' 
			
			--16207

BEGIN
	 	UPDATE [SUTTER_1P_DELTA].[IMP].[ACTION] SET Comments__C=REPLACE(Comments__C,'"','''') where Comments__C like '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[ACTION] SET [Subject] =REPLACE([Subject],'"','''') where [Subject] LIKE '%"%'
	 	UPDATE [SUTTER_1P_DELTA].[IMP].[ACTION] SET RE_Action_Solicitor__c =REPLACE(RE_Action_Solicitor__c,'"','''') where RE_Action_Solicitor__c LIKE '%"%'
		UPDATE [SUTTER_1P_DELTA].[IMP].[ACTION] SET RE_Team_Member__c =REPLACE(RE_Team_Member__c,'"','''') where RE_Team_Member__c LIKE '%"%'

		BEGIN 
			USE [SUTTER_1P_DELTA] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'ACTION'
 		END
END
 
BEGIN-- resolve duplicate ExternalId

		SELECT *
		FROM  [SUTTER_1P_DELTA].[IMP].[ACTION] 
		WHERE External_ID__c IN (SELECT External_ID__c FROM SUTTER_1P_DELTA.IMP.[ACTION] GROUP BY External_ID__c HAVING COUNT(*)>1)
		ORDER BY External_ID__c 

		--update zrefSeq with seq #		
		UPDATE T
		SET zrefSeq = rn
		FROM ( 
			   SELECT zrefSeq,
					 ROW_NUMBER() OVER ( PARTITION BY External_ID__c ORDER BY External_ID__c) AS rn
			   FROM [SUTTER_1P_DELTA].[IMP].[ACTION] 
	 		 )  T
  
		--update External_ID__c 
		SELECT [Account__r:External_ID__c], External_ID__c, zrefSeq
		FROM  [SUTTER_1P_DELTA].[IMP].[ACTION]   
		WHERE External_ID__c IN (SELECT External_ID__c FROM SUTTER_1P_DELTA.IMP.[ACTION] GROUP BY External_ID__c HAVING COUNT(*)>1)
		ORDER BY External_ID__c, [Account__r:External_ID__c]

		UPDATE [SUTTER_1P_DELTA].[IMP].[ACTION] 
		SET External_ID__c= External_ID__c+'-'+CAST(zrefSeq AS nvarchar(3))
		WHERE External_ID__c IN (SELECT External_ID__c FROM SUTTER_1P_DELTA.IMP.[ACTION] GROUP BY External_ID__c HAVING COUNT(*)>1)
		AND zrefSeq>1
 	 
	 SELECT COUNT(*) FROM [SUTTER_1P_DELTA].[IMP].[ACTION]   

END 


BEGIN--ACTION PARTICIPANT   
	 			SELECT DISTINCT
				[Action__r:External_ID__c]=T.External_Id__c
  				,RecordType=CASE WHEN T2.[ACImpID] IS NULL THEN dbo.fnc_RecordType('Action_Participant__c_Philanthropy') 
								ELSE dbo.fnc_RecordType('Action_Participant__c_External') end
				,User__c=CASE WHEN T2.[ACImpID] IS NULL THEN X3.ID  END
				,Contact__c=CASE WHEN T2.[ACImpID] IS NOT NULL THEN X2.ID END 
		  		
				,T2.[ACSolImpID] AS zrefACSolImpId
				INTO SUTTER_1P_DELTA.IMP.[ACTION_PARTICIPANT]	
				FROM [SUTTER_1P_DELTA].[IMP].[ACTION]  T
				LEFT JOIN [SUTTER_1P_DELTA].dbo.[HC_Cons_Action_Solicitor_v] T2 ON T.[RE_ACImpID__c]=T2.RE_DB_ID_AC 
				LEFT JOIN SUTTER_1P_DELTA.DBO.HC_Constituents_v T1 ON T2.RE_DB_ID=T1.RE_DB_ID  
		 		LEFT JOIN SUTTER_1P_DELTA.DBO.CHART_ActionSolicitor T8 ON T.zref_AddedBy=T8.AddedBy AND T.zref_RE_DB_OwnerShort=T8.RE_DB_OwnerShort		   --ACtion AddedBy 
		 		LEFT JOIN SUTTER_1P_DELTA.XTR.CONTACT X2 ON T1.NEW_TGT_IMPORT_ID=X2.External_Id__c
				LEFT JOIN SUTTER_1P_DELTA.XTR.USERS X3 ON T8.SF_User_Email=X3.Email 
END		
 
 
 SELECT RE_Action_Solicitor__c, LEN(RE_Action_Solicitor__c) l 
 FROM [SUTTER_1P_DELTA].IMP.[ACTION]
 ORDER BY l desc

 SELECT * FROM [SUTTER_1P_DELTA].imp.[ACTION] WHERE [Proposal__r:External_ID__c] IS NOT  null
  
  SELECT * FROM [SUTTER_1P_DELTA].imp.[OPPORTUNITY_PROPOSAL] WHERE [External_Id__c]='smcf-pp-03947-589-0000000760' 