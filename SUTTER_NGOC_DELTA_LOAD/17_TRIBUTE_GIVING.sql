
USE [SUTTER_1P_DELTA_migration]
GO

BEGIN
    DROP TABLE [SUTTER_1P_DELTA_migration].TBL.TRIBUTE_GIVING	  SELECT COUNT(*) FROM [SUTTER_1P_DELTA_migration].TBL.TRIBUTE_GIVING
	DROP TABLE [SUTTER_1P_DELTA_migration].IMP.TRIBUTE_GIVING_new
END
	 			 

BEGIN--tbl base
	  		--parent opts
			SELECT 	DISTINCT
	  			NULL AS ID
				,[Tribute__r:RE_TRImpID__c] = T2.[RE_DB_OwnerShort]+'-'+T2.RE_DB_Tbl+'-'+T2.TRImpID 
				,[Giving__r:External_Id__c] = CAST(T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.[GSplitImpID] AS VARCHAR(40))   
 				,T3.RE_TRImpID__c AS zref_delta_RE_TRImpID__c
				,T4.RE_TRImpID__c AS zref_final_RE_TRImpID__c

			 	INTO [SUTTER_1P_DELTA_migration].TBL.TRIBUTE_GIVING_test
				FROM  [SUTTER_1P_DELTA_migration].TBL.GIFT	 AS T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Gift_Tribute_v AS T1 ON T.[GSplitGFImpID] =  T1.GFTLink AND T.[GSplitRE_DB_OwnerShort]=T1.[RE_DB_OwnerShort]
				INNER JOIN [SUTTER_1P_DATA].dbo.[HC_Cons_Tribute_v] AS T2 ON T1.[TribImpID] = T2.[TRImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]	
				LEFT JOIN [SUTTER_1P_DELTA_migration].IMP.[TRIBUTE] AS T3 ON T2.[TRImpID]=T3.[zrefTRImpId] AND T2.[RE_DB_OwnerShort]=T3.[zrefRE_DB_Owner]
	  			LEFT JOIN [SUTTER_1P_MIGRATION].IMP.[TRIBUTE] AS T4 ON T2.[TRImpID]=T4.[zrefTRImpId] AND T2.[RE_DB_OwnerShort]=T4.[zrefRE_DB_Owner]		
				WHERE T.Parent_Giving_RecordType IS NOT NULL 
 		 	UNION
				SELECT 	DISTINCT
	  			NULL AS ID
				,[Tribute__r:RE_TRImpID__c] = T2.[RE_DB_OwnerShort]+'-'+T2.RE_DB_Tbl+'-'+T2.TRImpID 
				,[Giving__r:External_Id__c] = CAST(T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.[GSplitImpID] AS VARCHAR(40))   
 				,T3.RE_TRImpID__c AS zref_delta_RE_TRImpID__c
				,T4.RE_TRImpID__c AS zref_final_RE_TRImpID__c

				FROM [SUTTER_1P_DELTA_migration].TBL.GIFT T
				INNER JOIN [SUTTER_1P_MIGRATION].TBL.GiftAttr_employee_recurring T5 ON  T.GSplitGFImpID = T5.GFIMPID AND T.GSplitRE_DB_OwnerShort=T5.RE_DB_OwnerShort  --CREATE "EMPLOYEE GIVING GIFTS" FROM ATTRIBUTES. 
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Gift_Tribute_v AS T1 ON T.[GSplitGFImpID] =  T1.GFTLink AND T.[GSplitRE_DB_OwnerShort]=T1.[RE_DB_OwnerShort]
				INNER JOIN [SUTTER_1P_DATA].dbo.[HC_Cons_Tribute_v] AS T2 ON T1.[TribImpID] = T2.[TRImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]	
				LEFT JOIN [SUTTER_1P_DELTA_migration].IMP.[TRIBUTE] AS T3 ON T2.[TRImpID]=T3.[zrefTRImpId] AND T2.[RE_DB_OwnerShort]=T3.[zrefRE_DB_Owner]
			  	LEFT JOIN [SUTTER_1P_MIGRATION].IMP.[TRIBUTE] AS T4 ON T2.[TRImpID]=T4.[zrefTRImpId] AND T2.[RE_DB_OwnerShort]=T4.[zrefRE_DB_Owner]		
    				WHERE ( T5.[GFType]='Cash' OR T5.[GFType]='Other' OR T5.[GFType]='Recurring Gift Pay-Cash'  )	   
	 		UNION	
			--tran oppts	
			  	SELECT 	DISTINCT
	  			NULL AS ID
				,[Tribute__r:RE_TRImpID__c] = T2.[RE_DB_OwnerShort]+'-'+T2.RE_DB_Tbl+'-'+T2.TRImpID 
				,[Giving__r:External_Id__c] = ('T-'+T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.[GSplitImpID])
  				
				,T3.RE_TRImpID__c AS zref_delta_RE_TRImpID__c
				,T4.RE_TRImpID__c AS zref_final_RE_TRImpID__c
				FROM  [SUTTER_1P_DELTA_migration].TBL.GIFT	 AS T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Gift_Tribute_v AS T1 ON T.[GSplitGFImpID] =  T1.GFTLink AND T.[GSplitRE_DB_OwnerShort]=T1.[RE_DB_OwnerShort]
				INNER JOIN [SUTTER_1P_DATA].dbo.[HC_Cons_Tribute_v] AS T2 ON T1.[TribImpID] = T2.[TRImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]	
				LEFT JOIN [SUTTER_1P_DELTA_migration].IMP.[TRIBUTE] AS T3 ON T2.[TRImpID]=T3.[zrefTRImpId] AND T2.[RE_DB_OwnerShort]=T3.[zrefRE_DB_Owner]
			  	LEFT JOIN [SUTTER_1P_MIGRATION].IMP.[TRIBUTE] AS T4 ON T2.[TRImpID]=T4.[zrefTRImpId] AND T2.[RE_DB_OwnerShort]=T4.[zrefRE_DB_Owner]		

			  	WHERE T.Transaction_Source='NON_PAYMENT'
 			UNION
			  	SELECT 	DISTINCT
	  			NULL AS ID
				,[Tribute__r:RE_TRImpID__c] = T2.[RE_DB_OwnerShort]+'-'+T2.RE_DB_Tbl+'-'+T2.TRImpID 
				,[Giving__r:External_Id__c] = (T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.[GSplitImpID])
  				
				,T3.RE_TRImpID__c AS zref_delta_RE_TRImpID__c
				,T4.RE_TRImpID__c AS zref_final_RE_TRImpID__c
				FROM  [SUTTER_1P_DELTA_migration].TBL.GIFT	 AS T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Gift_Tribute_v AS T1 ON T.[GSplitGFImpID] =  T1.GFTLink AND T.[GSplitRE_DB_OwnerShort]=T1.[RE_DB_OwnerShort]
				INNER JOIN [SUTTER_1P_DATA].dbo.[HC_Cons_Tribute_v] AS T2 ON T1.[TribImpID] = T2.[TRImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]	
				LEFT JOIN [SUTTER_1P_DELTA_migration].IMP.[TRIBUTE] AS T3 ON T2.[TRImpID]=T3.[zrefTRImpId] AND T2.[RE_DB_OwnerShort]=T3.[zrefRE_DB_Owner]
			  	LEFT JOIN [SUTTER_1P_MIGRATION].IMP.[TRIBUTE] AS T4 ON T2.[TRImpID]=T4.[zrefTRImpId] AND T2.[RE_DB_OwnerShort]=T4.[zrefRE_DB_Owner]		

			  	WHERE T.Transaction_Source='PAYMENT' 
				 
 END--

 ---test 
 
 SELECT * FROM [SUTTER_1P_DELTA_migration].TBL.TRIBUTE_GIVING_test 
 WHERE [Tribute__r:RE_TRImpID__c] LIKE '%05502-602-0000028251%' 

BEGIN--IMP_TRIBUTE GIVING table

			SELECT T.ID, T.[Tribute__r:RE_TRImpID__c],  [Giving__r:External_Id__c]
			INTO [SUTTER_1P_DELTA_migration].IMP.TRIBUTE_GIVING_new_test
			FROM  [SUTTER_1P_DELTA_migration].TBL.TRIBUTE_GIVING_test AS T
			INNER JOIN [SUTTER_1P_DELTA_migration].IMP.[OPPORTUNITY_PARENT] AS T1 ON T.[Giving__r:External_Id__c] = T1.[External_Id__c]
			UNION 
			SELECT T.ID, T.[Tribute__r:RE_TRImpID__c],  [Giving__r:External_Id__c]
			FROM  [SUTTER_1P_DELTA_migration].TBL.TRIBUTE_GIVING_test AS T
			INNER JOIN [SUTTER_1P_DELTA_migration].IMP.[OPPORTUNITY_TRANSACTION] AS T1 ON T.[Giving__r:External_Id__c] = T1.[External_Id__c]
	 
END--end IMP tbl 
	    
		SELECT * FROM [SUTTER_1P_DELTA_migration].IMP.TRIBUTE_GIVING_new WHERE [Giving__r:External_Id__c] LIKE '%05502-553-0000534540%'
		SELECT * FROM [SUTTER_1P_DELTA_migration].IMP.TRIBUTE_GIVING_new_test WHERE [Giving__r:External_Id__c] LIKE '%05502-553-0000534540%'
	
		SELECT T1.[Tribute__r:RE_TRImpID__c], T1.[Giving__r:External_Id__c] 
		 FROM [SUTTER_1P_DELTA_migration].IMP.TRIBUTE_GIVING_new_test T1
		LEFT JOIN    [SUTTER_1P_DELTA_migration].IMP.TRIBUTE_GIVING_new AS T2 ON  T1.[Tribute__r:RE_TRImpID__c] = T2.[Tribute__r:RE_TRImpID__c]
																			AND T1.[Giving__r:External_Id__c] = T2.[Giving__r:External_Id__c]
		WHERE T2.[Tribute__r:RE_TRImpID__c] IS NULL AND  T2.[Giving__r:External_Id__c] IS NULL 


		SELECT COUNT(*) FROM   [SUTTER_1P_DELTA_migration].IMP.TRIBUTE_GIVING_new 
		
		SELECT COUNT(*) FROM   [SUTTER_1P_DELTA_migration].IMP.TRIBUTE_GIVING_new_test
	--	   DROP TABLE [SUTTER_1P_MIGRATION].IMP.TRIBUTE_GIVING_new
		
		   SELECT NULL as ID, X1.ID AS Tribute__c, X2.ID AS Giving__c 
		--   INTO [SUTTER_1P_MIGRATION].IMP.TRIBUTE_GIVING
		   FROM [SUTTER_1P_MIGRATION].IMP.TRIBUTE_GIVING_new AS T
		   LEFT JOIN [SUTTER_1P_MIGRATION].XTR.TRIBUTE AS X1 ON T.[Tribute__r:RE_TRImpID__c]=X1.RE_TRImpID__c
		   LEFT JOIN [SUTTER_1P_MIGRATION].XTR.[OPPORTUNITY] AS X2 ON T.[Giving__r:External_Id__c]=X2.[EXTERNAL_ID__C]
			


BEGIN--test				
			SELECT COUNT(*) FROM  SUTTER_1P_DATA.DBO.HC_Gift_Tribute_v
			SELECT COUNT(*) FROM  [SUTTER_1P_MIGRATION].IMP.TRIBUTE_GIVING_new	--533,639
		 	SELECT COUNT(*) FROM SUTTER_1P_DATA.DBO.HC_Gift_Tribute_v
			SELECT TOP 10 * FROM [SUTTER_1P_MIGRATION].imp.[OPPORTUNITY_PARENT]
			SELECT TOP 10 * FROM [SUTTER_1P_MIGRATION].imp.[OPPORTUNITY_TRANSACTION]						 

			SELECT COUNT(*) FROM [SUTTER_1P_MIGRATION].TBL.TRIBUTE_GIVING	  -- 1,045,852
		 	SELECT DISTINCT [Tribute__r:RE_TRImpID__c],  [Giving__r:External_Id__c] FROM  [SUTTER_1P_MIGRATION].TBL.TRIBUTE_GIVING
			SELECT * FROM  [SUTTER_1P_MIGRATION].IMP.TRIBUTE_GIVING_new	 WHERE [Giving__r:External_Id__c]='T-CVRX-GT-00001-553-0000127446'
	 
END


	SELECT * FROM  SUTTER_1P_DATA.DBO.HC_Gift_Tribute_v