USE SUTTER_1P_DELTA
GO
 

BEGIN
    DROP TABLE SUTTER_1P_DELTA.TBL.PAYMENT_METHOD
 	DROP TABLE SUTTER_1P_DELTA.IMP.PAYMENT_METHOD 
END

BEGIN--BASE TABLE PAYMENT METHODS 
  	 	--NEW PAYMENT METHODS FROM NEW CONSTITUENTS	
				SELECT 
				dbo.fnc_OwnerID() AS OwnerID
				,ROW_NUMBER() OVER (PARTITION BY (T4.NEW_TGT_IMPORT_ID) 
									ORDER BY  (T4.NEW_TGT_IMPORT_ID),
												CASE WHEN (T1.GFType LIKE '%Stock%') THEN '3'
												  WHEN (T1.GFPayMeth LIKE '%Check%' OR T1.GFPayMeth='Cash') THEN '1'
												  WHEN (T1.GFPayMeth ='Credit Card') THEN '2'
												  WHEN (T1.GFPayMeth ='Direct Debit') THEN '2'
												  WHEN (T1.GFPayMeth ='Other') THEN '4'
												  ELSE '1' END) AS zrefSeq 
				,CASE	WHEN (T1.GFType LIKE '%Stock%') THEN '3'
						WHEN (T1.GFPayMeth LIKE '%Check%' OR T1.GFPayMeth='Cash') THEN '1'
						WHEN (T1.GFPayMeth ='Credit Card') THEN '2'
						WHEN (T1.GFPayMeth ='Direct Debit') THEN '2'
						WHEN (T1.GFPayMeth ='Other') THEN '4'
						ELSE '1' END AS zrefPaySeq
				,T4.NEW_TGT_IMPORT_ID  AS ACCOUNT_External_ID__c
				,CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ISNULL(UPPER(T4.NEW_TGT_IMPORT_ID),' ')+   --Head of Household
						ISNULL(UPPER(CASE WHEN (T1.GFType LIKE '%Stock%') THEN 'Securities'
										  WHEN (T1.GFPayMeth LIKE '%Check%' OR T1.GFPayMeth='Cash') THEN 'Cash/Check'
										  WHEN (T1.GFPayMeth ='Credit Card') THEN 'Third Party Charge'
										  WHEN (T1.GFPayMeth ='Direct Debit') THEN 'Third Party Charge'
										  WHEN (T1.GFPayMeth ='Other') THEN 'Other'
										  ELSE 'Cash/Check' END ),' ')+ 
						ISNULL(UPPER(T1.GFCCType),' ')+ 
						ISNULL(UPPER(T1.GFCCNum),' ')+
						ISNULL(UPPER(T1.GFCardholderName),' ')+
						ISNULL(UPPER(T1.GFCCExpOn),' ')+
						ISNULL(UPPER(T1.GFAuthCode),' '))), 3, 50) AS NVARCHAR(50)) 
					AS External_Id__c
				,CASE WHEN T1.GFType LIKE '%stock%' THEN dbo.fnc_RecordType('Payment_Method_Securities') ELSE dbo.fnc_RecordType('Payment_Method_'+T2.rC_Giving__Payment_Type__c) END AS RecordTypeID	
				,CASE WHEN T1.GFType LIKE '%stock%' THEN 'Securities' ELSE T2.rC_Giving__Payment_Type__c END AS Name
				,CASE WHEN T1.GFType LIKE '%stock%' THEN 'Securities' ELSE T2.rC_Giving__Payment_Type__c END AS rC_Giving__Payment_Type__c
				,CASE WHEN T2.rC_Giving__Payment_Type__c ='Cash/Check' then 'TRUE' else 'FALSE' end as rC_Giving__Is_Default__c
				,CASE WHEN T2.rC_Giving__Payment_Type__c ='Cash/Check' then 'TRUE' else 'FALSE' end as rC_Giving__Is_Active__c
				,T1.GFCCType AS rC_Giving__Card_Issuer__c
				,RIGHT(T1.GFCCNum,4) AS rC_Giving__Card_Number_Last_4__c 
				,T1.GFCardholderName AS rC_Giving__Card_Holder_Name__c
				,RIGHT(T1.GFCCExpOn,2)+'/'+LEFT(T1.GFCCExpOn, 4) AS rC_Giving__Card_Expiration__c
				,T1.GFAuthCode AS rC_Connect__Authentication_Value__c
				--ref
				,PayTypeSeq= CASE WHEN (T1.GFType LIKE '%Stock%') THEN '3'
										  WHEN (T1.GFPayMeth LIKE '%Check%' OR T1.GFPayMeth='Cash') THEN '1'
										  WHEN (T1.GFPayMeth ='Credit Card') THEN '2'
										  WHEN (T1.GFPayMeth ='Direct Debit') THEN '2'
										  WHEN (T1.GFPayMeth ='Other') THEN '4'
										  ELSE '1' END 
				,T1.GFPayMeth
				,T1.GFType
				,T1.GFImpID
				,T1.RE_DB_Id 
				,T1.[RE_DB_OwnerShort]
				,T4.NEW_TGT_IMPORT_ID 
				,CAST('' AS VARCHAR(30)) AS HH_ImportID
				,CAST('' AS VARCHAR(30)) AS NoHH_ImportId
				,'gifts__new_cons' AS  src
			 	INTO [SUTTER_1P_DELTA].TBL.PAYMENT_METHOD	
				FROM [SUTTER_1P_DELTA].dbo.HC_Gift_v T1
				INNER JOIN [SUTTER_1P_DELTA].dbo.CHART_GFPayMeth AS T2 ON T1.GFPayMeth=T2.GFPayMeth
				INNER JOIN [SUTTER_1P_DELTA].dbo.HC_Constituents_v AS T4 ON T1.RE_DB_Id=T4.RE_DB_Id				
 			UNION
				--PAyment methods on new gifts from exsiting constituents mgirated during the final conversion 
				SELECT 
				dbo.fnc_OwnerID() AS OwnerID
				,ROW_NUMBER() OVER (PARTITION BY (CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.HH_ImportID ELSE T4.NEW_TGT_IMPORT_ID  END) 
									ORDER BY  (CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.HH_ImportID ELSE T4.NEW_TGT_IMPORT_ID  END),
												CASE WHEN (T1.GFType LIKE '%Stock%') THEN '3'
												  WHEN (T1.GFPayMeth LIKE '%Check%' OR T1.GFPayMeth='Cash') THEN '1'
												  WHEN (T1.GFPayMeth ='Credit Card') THEN '2'
												  WHEN (T1.GFPayMeth ='Direct Debit') THEN '2'
												  WHEN (T1.GFPayMeth ='Other') THEN '4'
												  ELSE '1' END) AS zrefSeq 
				,CASE	WHEN (T1.GFType LIKE '%Stock%') THEN '3'
						WHEN (T1.GFPayMeth LIKE '%Check%' OR T1.GFPayMeth='Cash') THEN '1'
						WHEN (T1.GFPayMeth ='Credit Card') THEN '2'
						WHEN (T1.GFPayMeth ='Direct Debit') THEN '2'
						WHEN (T1.GFPayMeth ='Other') THEN '4'
						ELSE '1' END AS zrefPaySeq
				,CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.HH_ImportID ELSE T4.NEW_TGT_IMPORT_ID  END AS ACCOUNT_External_ID__c
				,CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', ISNULL(UPPER(CASE WHEN T3.NoHH_ImportID IS NOT NULL THEN T3.HH_ImportID ELSE T4.NEW_TGT_IMPORT_ID END),' ')+   --Head of Household
						ISNULL(UPPER(CASE WHEN (T1.GFType LIKE '%Stock%') THEN 'Securities'
										  WHEN (T1.GFPayMeth LIKE '%Check%' OR T1.GFPayMeth='Cash') THEN 'Cash/Check'
										  WHEN (T1.GFPayMeth ='Credit Card') THEN 'Third Party Charge'
										  WHEN (T1.GFPayMeth ='Direct Debit') THEN 'Third Party Charge'
										  WHEN (T1.GFPayMeth ='Other') THEN 'Other'
										  ELSE 'Cash/Check' END ),' ')+ 
						ISNULL(UPPER(T1.GFCCType),' ')+ 
						ISNULL(UPPER(T1.GFCCNum),' ')+
						ISNULL(UPPER(T1.GFCardholderName),' ')+
						ISNULL(UPPER(T1.GFCCExpOn),' ')+
						ISNULL(UPPER(T1.GFAuthCode),' '))), 3, 50) AS NVARCHAR(50)) 
					AS External_Id__c
				,CASE WHEN T1.GFType LIKE '%stock%' THEN dbo.fnc_RecordType('Payment_Method_Securities') ELSE dbo.fnc_RecordType('Payment_Method_'+T2.rC_Giving__Payment_Type__c) END AS RecordTypeID	
				,CASE WHEN T1.GFType LIKE '%stock%' THEN 'Securities' ELSE T2.rC_Giving__Payment_Type__c END AS Name
				,CASE WHEN T1.GFType LIKE '%stock%' THEN 'Securities' ELSE T2.rC_Giving__Payment_Type__c END AS rC_Giving__Payment_Type__c
				,CASE WHEN T2.rC_Giving__Payment_Type__c ='Cash/Check' then 'TRUE' else 'FALSE' end as rC_Giving__Is_Default__c
				,CASE WHEN T2.rC_Giving__Payment_Type__c ='Cash/Check' then 'TRUE' else 'FALSE' end as rC_Giving__Is_Active__c
				,T1.GFCCType AS rC_Giving__Card_Issuer__c
				,RIGHT(T1.GFCCNum,4) AS rC_Giving__Card_Number_Last_4__c 
				,T1.GFCardholderName AS rC_Giving__Card_Holder_Name__c
				,RIGHT(T1.GFCCExpOn,2)+'/'+LEFT(T1.GFCCExpOn, 4) AS rC_Giving__Card_Expiration__c
				,T1.GFAuthCode AS rC_Connect__Authentication_Value__c
				--ref
				,PayTypeSeq= CASE WHEN (T1.GFType LIKE '%Stock%') THEN '3'
										  WHEN (T1.GFPayMeth LIKE '%Check%' OR T1.GFPayMeth='Cash') THEN '1'
										  WHEN (T1.GFPayMeth ='Credit Card') THEN '2'
										  WHEN (T1.GFPayMeth ='Direct Debit') THEN '2'
										  WHEN (T1.GFPayMeth ='Other') THEN '4'
										  ELSE '1' END 
				,T1.GFPayMeth
				,T1.GFType
				,T1.GFImpID
				,T1.RE_DB_Id 
				,T1.[RE_DB_OwnerShort]
				,T4.NEW_TGT_IMPORT_ID 
				,T3.HH_ImportID HH_ImportID
				,T3.NoHH_ImportID AS NoHH_ImportId
				,'gifts__old_cons' AS  src
  			 
				FROM SUTTER_1P_DELTA.dbo.HC_Gift_v T1	    --new gifts on existing constituents migrated during the final conversion. 
				INNER JOIN SUTTER_1P_DELTA.dbo.CHART_GFPayMeth AS T2 ON T1.GFPayMeth=T2.GFPayMeth
				INNER JOIN SUTTER_1P_DELTA.dbo.HC_Constituents_v_final AS T4 ON T1.RE_DB_Id=T4.RE_DB_Id				
				LEFT JOIN SUTTER_1P_DELTA.tbl.Contact_HofH_final_conv AS T3 ON  T4.NEW_TGT_IMPORT_ID  = T3.NoHH_ImportID 
		 
 END; --(9335 row(s) affected)	 			  
		   
		--UPDATE DEFAULT

				SELECT		
					zrefSeq
					,zrefPaySeq
					,T1.OwnerID
		  			,T1.ACCOUNT_External_ID__c AS [rC_Giving__Account__r:External_Id__c]
					,T1.External_Id__c
					,T1.RecordTypeID
					,T1.Name 
					,T1.rC_Giving__Payment_Type__c 
					,T1.rC_Giving__Is_Default__c
					,T1.rC_Giving__Is_Active__c 
				FROM SUTTER_1P_DELTA.TBL.PAYMENT_METHOD T1
				ORDER BY ACCOUNT_External_ID__c

				UPDATE SUTTER_1P_DELTA.TBL.PAYMENT_METHOD
				SET rC_Giving__Is_Default__c='TRUE', rC_Giving__Is_Active__c='TRUE'
				WHERE zrefSeq='1'
				 
				UPDATE SUTTER_1P_DELTA.TBL.PAYMENT_METHOD
				SET rC_Giving__Is_Default__c='FALSE', rC_Giving__Is_Active__c='FALSE'
				WHERE zrefSeq!='1'
		
		--REcords not on payment method because CONSTITUENT DOES NOT EXIST ON CONSTITUENT TABLE>>
			SELECT DISTINCT T1.[RE_DB_Id] FROM [SUTTER_1P_DELTA].dbo.hc_gift_v AS T1
			LEFT JOIN SUTTER_1P_DELTA.TBL.PAYMENT_METHOD AS T2 ON T1.[GFImpID]=T2.[GFImpID] AND T1.[RE_DB_OwnerShort]=t2.[RE_DB_OwnerShort]
			WHERE t2.[GFImpID] IS null	 

			SELECT * FROM SUTTER_1P_DELTA.TBL.PAYMENT_METHOD	 		

BEGIN--IMP table 
				SELECT 
					T1.OwnerID
		  			,T1.ACCOUNT_External_ID__c AS [rC_Giving__Account__r:External_Id__c]
					,T1.External_Id__c
					,T1.RecordTypeID
					,T1.Name 
					,T1.rC_Giving__Payment_Type__c 
					,MAX(T1.rC_Giving__Is_Default__c) AS rC_Giving__Is_Default__c
					,MAX(T1.rC_Giving__Is_Active__c) AS rC_Giving__Is_Active__c 
					,T1.rC_Giving__Card_Issuer__c
					,T1.rC_Giving__Card_Number_Last_4__c
					,T1.rC_Giving__Card_Holder_Name__c
					,T1.rC_Giving__Card_Expiration__c
					,T1.rC_Connect__Authentication_Value__c
				INTO SUTTER_1P_DELTA.IMP.PAYMENT_METHOD    
				FROM SUTTER_1P_DELTA.TBL.PAYMENT_METHOD T1
				LEFT JOIN [SUTTER_1P_MIGRATION].TBL.PAYMENT_METHOD AS T2 ON T1.External_Id__c=T2.External_Id__c	  
						 -- do not include payment methods from final conversion. 
				WHERE T2.External_Id__c IS NULL 
				GROUP BY T1.OwnerID
		  				,T1.ACCOUNT_External_ID__c
						,T1.External_Id__c
						,T1.RecordTypeID
						,T1.Name 
						,T1.rC_Giving__Payment_Type__c 
						,T1.rC_Giving__Card_Issuer__c
						,T1.rC_Giving__Card_Number_Last_4__c
						,T1.rC_Giving__Card_Holder_Name__c
						,T1.rC_Giving__Card_Expiration__c
						,T1.rC_Connect__Authentication_Value__c
				ORDER BY T1.ACCOUNT_External_ID__c

END	--end of imp_paymeth 	(632 row(s) affected)	

 
		BEGIN--check duplicates. 
				SELECT    *
				FROM      SUTTER_1P_DELTA.IMP.PAYMENT_METHOD  --ORDER BY [rc_Giving__Account__r:External_Id__C], zrefseq
				WHERE     External_Id__c IN (
					        SELECT  External_Id__c
						    FROM     SUTTER_1P_DELTA.IMP.PAYMENT_METHOD 
							GROUP BY External_Id__c
							HAVING  COUNT(*) > 1 )
				ORDER BY  External_Id__c 
		 
		END;
		SELECT DISTINCT src, [GFPayMeth] FROM SUTTER_1P_DELTA.TBL.PAYMENT_METHOD  ORDER BY src, [GFPayMeth]

		SELECT T1.* FROM SUTTER_1P_DELTA.TBL.PAYMENT_METHOD AS T1
		INNER JOIN [SUTTER_1P_DELTA].IMP.[PAYMENT_METHOD] AS T2 ON T1.[External_Id__c]=T2.[External_Id__c]
																AND T1.[ACCOUNT_External_ID__c]=T2.[rC_Giving__Account__r:External_Id__c]
																
		SELECT * FROM SUTTER_1P_DELTA.TBL.PAYMENT_METHOD WHERE [External_Id__c]='e8dbb2bc9b31e63fb0b4a46d196ed542'
 		SELECT * FROM SUTTER_1P_DELTA.IMP.PAYMENT_METHOD WHERE [External_Id__c]='e8dbb2bc9b31e63fb0b4a46d196ed542'
		
		SELECT * FROM [SUTTER_1P_MIGRATION].IMP.[PAYMENT_METHOD] WHERE [rC_Giving__Account__r:External_Id__c]='WBRX-05502-593-0000288236'

		BEGIN--check defaults
		
				SELECT [rC_Giving__Account__r:External_Id__c]
					   ,rC_Giving__Is_Default__c
					   ,COUNT(*) c
				FROM SUTTER_1P_DELTA.IMP.PAYMENT_METHOD 
				WHERE   rC_Giving__Is_Default__c = 'TRUE'
				GROUP BY [rC_Giving__Account__r:External_Id__c]
					   ,rC_Giving__Is_Default__c
				HAVING COUNT(*)>1
				ORDER BY c DESC

				--IMP
				SELECT * FROM SUTTER_1P_DELTA.IMP.PAYMENT_METHOD
				WHERE [rC_Giving__Account__r:External_Id__c]='ABSF-02869-079-0130709'  
				--TBL
				SELECT * FROM SUTTER_1P_DELTA.TBL.PAYMENT_METHOD
				WHERE ACCOUNT_External_ID__c='ABSF-02869-079-0130709' ORDER BY ACCOUNT_External_ID__c, zrefseq

				SELECT * FROM SUTTER_1P_DELTA.IMP.PAYMENT_METHOD
				WHERE [rC_Giving__Account__r:External_Id__c]='PAMF-00001-079-0013673'
 			   --TBL
				SELECT * FROM SUTTER_1P_DELTA.TBL.PAYMENT_METHOD
				WHERE ACCOUNT_External_ID__c='PAMF-00001-079-0013673' ORDER BY ACCOUNT_External_ID__c, zrefseq

				
		END;
				SELECT COUNT(*) FROM SUTTER_1P_DELTA.IMP.PAYMENT_METHOD

				SELECT * FROM [SUTTER_1P_MIGRATION].IMP.[PAYMENT_METHOD] WHERE [rC_Giving__Account__r:External_Id__c]='ABSF-00001-079-0120677'

	  
SELECT * FROM SUTTER_1P_DELTA.IMP.PAYMENT_METHOD WHERE [rC_Giving__Account__r:External_Id__c]='ABSF-02869-593-0000429028'  

SELECT [ACCOUNT_External_ID__c], [NEW_TGT_IMPORT_ID], [HH_ImportID], [NoHH_ImportID], [RE_DB_OwnerShort], 
		[GFImpID],  External_id__c, Name, [rC_Giving__Payment_Type__c], [src] 
FROM SUTTER_1P_DELTA.TBL.PAYMENT_METHOD WHERE [NEW_TGT_IMPORT_ID]='ABSF-02869-593-0000429028'

SELECT [ACCOUNT_External_ID__c], [NEW_TGT_IMPORT_ID], [HH_ImportID], [NoHH_ImportID], [RE_DB_OwnerShort], [GFImpID],  External_id__c, Name, [rC_Giving__Payment_Type__c] 
FROM [SUTTER_1P_MIGRATION].TBL.[PAYMENT_METHOD]  WHERE [NEW_TGT_IMPORT_ID]='ABSF-02869-593-0000429028'
				
 
SELECT * FROM [SUTTER_1P_MIGRATION].IMP.[OPPORTUNITY_PARENT]	   --SCAH-03038-593-0000282621
WHERE [RE_GFImpID__c] LIKE '%02594-545-0000334846%'