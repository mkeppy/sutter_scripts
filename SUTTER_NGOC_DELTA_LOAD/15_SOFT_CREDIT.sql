USE [SUTTER_1P_DELTA_migration]
GO

BEGIN

	DROP TABLE [SUTTER_1P_DELTA_migration].TBL.OPPORTUNITY_CONTACT_CREDIT
	DROP TABLE [SUTTER_1P_DELTA_migration].IMP.OPPORTUNITY_CONTACT_CREDIT

END
   
BEGIN--GIFT SOFTCREDIT
				/*	Link to Parent Opportunity only. 
					IF linked to a gift with [GFType] like '%pay%' then, link to the Parent Oppty associated to that Donation through [HC_Gift_Link_v].[GFPaymentGFImpID]
				*/
				SELECT 
					NULL AS ID
					,[rC_Giving__Account__r:External_ID__c] =CASE WHEN T2.KeyInd='O' THEN T2.[NEW_TGT_IMPORT_ID]
																  WHEN T2.KeyInd='I' AND T7.NoHH_ImportID IS NOT NULL 
																  THEN T7.HH_ImportID ELSE T2.[NEW_TGT_IMPORT_ID] END
					,[rC_Giving__Contact__r:External_ID__c] = CASE WHEN T2.KeyInd='I' THEN T2.[NEW_TGT_IMPORT_ID] END
					,[rC_Giving__Amount__c] = T1.SoftCredAmt
					,[rC_Giving__Contact_Role__c] = 'Soft Credit'
					,[rC_Giving__Distribution__c] = CASE WHEN (CASE WHEN T3.GFType LIKE '%pay%' THEN T6.[GSplitAmt] ELSE T5.[GSplitAmt] END) = 0 THEN '100'
												    ELSE ((T1.SoftCredAmt)/(CASE WHEN T3.GFType LIKE '%pay%' THEN T6.[GSplitAmt] ELSE T5.[GSplitAmt] END)*100) END 

					,[rC_Giving__Opportunity__r:External_ID__c] = CASE WHEN T3.GFType LIKE '%pay%' THEN T6.RE_DB_OwnerShort+'-'+T6.RE_DB_Tbl+'-'+T6.GSplitImpID 
																	ELSE T5.RE_DB_OwnerShort+'-'+T5.RE_DB_Tbl+'-'+T5.GSplitImpID END 
					,[rC_Giving__Type__c] = CASE WHEN T2.KeyInd='O' THEN 'Account Only' WHEN T2.KeyInd='I' THEN 'Account & Contact' END
					,[rC_Giving__Is_Fixed__c] = 'FALSE'   ---RC data dictionary note: "Will not be using as the functionality for Fixed is no longer supported"

					,[Solicitor_Amount__c] = NULL 
					,[Affiliation__c] = T1.RE_DB_OwnerShort
					,[Source_Credit_Type__c] = CASE WHEN T3.GFType LIKE '%pay%' THEN 'Transaction Soft Credit' 
												WHEN T3.GFType NOT LIKE '%pay%' THEN 'Donation Soft Credit' end
						--reference
					,T2.KeyInd zrefKeyInd
					,T7.NoHH_ImportID AS zrefNoHH_ImportID
					,T1.GFLink zrefGiftImpID
					,T3.GFType AS zrefGFType
					,T4.GFImpID AS zrefPledgeGFImpID
					,T6.GSplitImpID AS zrefPlegeGSplitGFImpID
					,T4.GFPaymentGFImpID AS zrefGFPaymentGFImpID
					,T5.GSplitImpID zrefPaymentGSplitImpID
					,'Gift_SoftCredit' zrefSrc
					,NULL AS zrefSolImpID
						
					INTO [SUTTER_1P_DELTA_migration].TBL.OPPORTUNITY_CONTACT_CREDIT
					FROM [SUTTER_1P_DELTA_migration].dbo.HC_Gift_SoftCredit_v T1 
					INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T2 ON T1.SoftCredRecip=T2.ImportID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
					LEFT JOIN [SUTTER_1P_DELTA_migration].tbl.Contact_HofH_final_conv T7 ON T2.[NEW_TGT_IMPORT_ID]=T7.NoHH_ImportID
				 	--GIFT
					INNER JOIN SUTTER_1P_DATA.DBO.HC_Gift_v T3 ON T1.GFLink=T3.GFImpID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort
					LEFT JOIN SUTTER_1P_DATA.DBO.HC_Gift_Link_v T4 ON T3.GFImpID=T4.GFPaymentGFImpID AND T3.RE_DB_OwnerShort=T4.RE_DB_OwnerShort
					LEFT JOIN SUTTER_1P_DATA.DBO.HC_Gift_SplitGift_v T5 ON T1.GFLink=T5.GFImpID AND T1.RE_DB_OwnerShort=T5.RE_DB_OwnerShort  /*non payment*/
					LEFT JOIN SUTTER_1P_DATA.DBO.HC_Gift_SplitGift_v T6 ON T4.GFImpID=T6.GFImpID AND T4.RE_DB_OwnerShort=T6.RE_DB_OwnerShort /*parent split*/
						AND T5.GFSplitSequence = T6.GFSplitSequence
					WHERE (CASE WHEN T3.GFType LIKE '%pay%' THEN T6.RE_DB_OwnerShort+'-'+T6.RE_DB_Tbl+'-'+T6.GSplitImpID 
								ELSE T5.RE_DB_OwnerShort+'-'+T5.RE_DB_Tbl+'-'+T5.GSplitImpID END) IS NOT NULL   --remove null OpportunityExternal Id from duplicates created from Splits. 
					--IF linked to a gift with [GFType] like '%pay%' then, link to the Parent Oppty associated to that Donation through [HC_Gift_Link_v].[GFPaymentGFImpID]
 					ORDER BY [rC_Giving__Account__r:External_ID__c] 
 
						SELECT * FROM  [SUTTER_1P_DELTA_migration].tbl.Contact_HofH
						SELECT * FROM  [SUTTER_1P_DELTA_migration].tbl.Contact_HofH_final_conv																													  

						SELECT * FROM [SUTTER_1P_DELTA_migration].TBL.OPPORTUNITY_CONTACT_CREDIT

	
BEGIN--The [Source_Credit_Type__c] will keep one-time and payments separate. 
			DROP TABLE [SUTTER_1P_DELTA_migration].[IMP].[OPPORTUNITY_CONTACT_CREDIT]
			
			SELECT DISTINCT 
				   [External_Id__c] = [rC_Giving__Opportunity__r:External_ID__c] +'-'+ 
										CAST((ROW_NUMBER() OVER (PARTITION BY [rC_Giving__Opportunity__r:External_ID__c] 
											                     ORDER BY [rC_Giving__Opportunity__r:External_ID__c], 
																 [rC_Giving__Account__r:External_ID__c], [rC_Giving__Contact__r:External_ID__c])) AS NVARCHAR(30))
				  ,[rC_Giving__Account__r:External_ID__c]
				  ,[rC_Giving__Contact__r:External_ID__c]
				  ,[rC_Giving__Amount__c] = SUM([rC_Giving__Amount__c])
				  ,[rC_Giving__Contact_Role__c]
				  ,'100' AS [rC_Giving__Distribution__c]
				  ,[rC_Giving__Opportunity__r:External_ID__c]
				  ,[rC_Giving__Type__c]
				  ,[rC_Giving__Is_Fixed__c]
				  ,[Solicitor_Amount__c] = SUM([Solicitor_Amount__c]) 
				  ,[Affiliation__c]
				  ,[Source_Credit_Type__c]
			  INTO [SUTTER_1P_DELTA_migration].[IMP].[OPPORTUNITY_CONTACT_CREDIT]	
			  FROM [SUTTER_1P_DELTA_migration].[TBL].[OPPORTUNITY_CONTACT_CREDIT]	 AS T
			  INNER JOIN [SUTTER_1P_MIGRATION].XTR.[OPPORTUNITY] AS X ON T.[rC_Giving__Opportunity__r:External_ID__c]=X.[EXTERNAL_ID__C]
			  GROUP BY
				   [rC_Giving__Account__r:External_ID__c]
				  ,[rC_Giving__Contact__r:External_ID__c]
				  ,[rC_Giving__Contact_Role__c]
  				  ,[rC_Giving__Opportunity__r:External_ID__c]
				  ,[rC_Giving__Type__c]
				  ,[rC_Giving__Is_Fixed__c]
				  ,[Affiliation__c]
				  ,[Source_Credit_Type__c]
 END
 			 
		SELECT COUNT(*) FROM  [SUTTER_1P_DELTA_migration].[IMP].[OPPORTUNITY_CONTACT_CREDIT]	  --114074
		SELECT * FROM [SUTTER_1P_DELTA_migration].IMP.OPPORTUNITY_CONTACT_CREDIT
		WHERE [rC_Giving__Opportunity__r:External_ID__c] IN (SELECT [rC_Giving__Opportunity__r:External_ID__c] FROM [SUTTER_1P_DELTA_migration].IMP.OPPORTUNITY_CONTACT_CREDIT 
								GROUP BY [rC_Giving__Opportunity__r:External_ID__c] HAVING COUNT(*)>1)
		ORDER BY [rC_Giving__Opportunity__r:External_ID__c]
					
		SELECT * FROM [SUTTER_1P_DELTA_migration].IMP.OPPORTUNITY_CONTACT_CREDIT
		WHERE [External_ID__c] IN (SELECT [External_ID__c] FROM [SUTTER_1P_DELTA_migration].IMP.OPPORTUNITY_CONTACT_CREDIT 
								GROUP BY [External_ID__c] HAVING COUNT(*)>1)
		ORDER BY [External_ID__c]
		
		SELECT * FROM [SUTTER_1P_DELTA_migration].IMP.OPPORTUNITY_CONTACT_CREDIT 
		WHERE [rC_Giving__Account__r:External_ID__c] IS NULL OR 
			  [rC_Giving__Contact__r:External_ID__c] IS NULL 				
   		 
			/* DNC,, Will convert Proposal SOlicitor to Solicitor Team Member	
			 --leave code to reuse.
				UNION ALL
 				
				--PROPOSAL SOLICITOR
						SELECT 
						NULL AS ID
						,[rC_Giving__Account__r:External_ID__c] = CASE WHEN T2.KeyInd='O' THEN T2.[NEW_TGT_IMPORT_ID] END
						,[rC_Giving__Contact__r:External_ID__c] = CASE WHEN T2.KeyInd='I' THEN T2.[NEW_TGT_IMPORT_ID] END
						,[rC_Giving__Amount__c] = T1.PRSolAmount
						,[rC_Giving__Contact_Role__c] = 'Peer to Peer Credit'
						,[rC_Giving__Distribution__c] = '100'
						,[rC_Giving__Opportunity__r:External_ID__c] = T1.RE_DB_OwnerShort+'-PP-'+T1.PRSolPRImpID  
						,[rC_Giving__Type__c] = CASE WHEN T2.KeyInd='O' THEN 'Account Only' WHEN T2.KeyInd='I' THEN 'Contact Only' END
						,[rC_Giving__Is_Fixed__c] = 'FALSE'   ---RC data dictionary note: "Will not be using as the functionality for Fixed is no longer supported"
						,[Solicitor_Amount__c] = T1.PRSolAmount
						,[Affiliation__c] = T1.RE_DB_OwnerShort
						,[Source_Credit_Type__c] = 'Proposal Soft Credit' 

						--reference
						,T2.KeyInd AS zrefKeyInd
						,NULL AS zrefNoHH_ImportID
						,T1.PRSolPRImpID AS zrefGiftImpID
						,NULL AS zrefGFType
						,NULL AS zrefPledgeGFImpID
						,NULL AS zrefPlegeGSplitGFImpID
						,NULL AS zGFPaymentGFImpID
						,NULL AS zrefPaymentGSplitImpID
						,'Prosal_Solicitor' zrefSrc
						,NULL AS zrefSolImpID
						FROM SUTTER_1P_DATA.dbo.HC_Proposal_Solicitor_v T1
						INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T2 ON T1.PRSolImpID=T2.ImportID AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
						INNER JOIN [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PROPOSAL] T3 ON (T1.RE_DB_OwnerShort+'-'+T1.[PRSolPRImpID])=T3.[RE_PRImpID__c]
				 */