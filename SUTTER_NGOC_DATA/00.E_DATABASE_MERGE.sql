
USE SUTTER_1P_DATA
GO

--01.
	-- go to --> QUERY--> select "SQLCMC Mode"  to activate sqlcmd. 

--02. 

-- Connect to HCTECHSERV\SQL2008R2
-- Update .sql files path (D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA)	


--03.

--DELETE data tables. 

					

			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_OrgName_v]') and objectproperty(id, N'IsScalarFunction') = 1)
				drop function [dbo].[HC_OrgName_v]

			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Appeal_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Appeal_v]  
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Campaign_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Campaign_v]
				 
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Action_Attr_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Action_Attr_v] 
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Action_Notes_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Action_Notes_v] 
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Action_Solicitor_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Action_Solicitor_v] 
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Action_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Action_v] 

			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Addl_Addressee_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Addl_Addressee_v] 
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Address_Attr_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Address_Attr_v] 
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Alias_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Alias_v] 
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Address_NonPref_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Address_NonPref_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Appeal_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Appeal_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Attributes_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Attributes_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_ConsCode_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_ConsCode_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Constituents_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Constituents_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Notes_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Notes_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Phone_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Phone_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_SolicitCodes_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_SolicitCodes_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Solicitor_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Solicitor_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Tribute_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Tribute_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Country_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Country_v]

			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Educ_Relat_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Educ_Relat_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Educ_Relat_Attribute_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Educ_Relat_Attribute_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Educ_Relat_Major_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Educ_Relat_Major_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Educ_Relat_Minor_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Educ_Relat_Minor_v]
				
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Attr_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Event_Attr_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Event_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Award_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Event_Award_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Expense_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Event_Expense_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Menu_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Event_Menu_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Notepad_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Event_Notepad_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Price_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Event_Price_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Participant_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Event_Participant_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Registration_Fees_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Event_Registration_Fees_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Participant_Attr_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Event_Participant_Attr_v]
					
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Fund_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Fund_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Attr_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_Attr_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Installments_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_Installments_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Main]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_Main]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_PledgePay]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_PledgePay]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Link_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_Link_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Notes_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_Notes_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_SoftCredit_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_SoftCredit_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Solicitor_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_Solicitor_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_SplitGift_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_SplitGift_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Tribute_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_Tribute_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_MatchingLink_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_MatchingLink_v]	
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_WriteOff_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_WriteOff_v]	
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Benefits_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_Benefits_v]	


			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Ind_Relat]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Ind_Relat]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Ind_Relat_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Ind_Relat_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Ind_Relat_Phone]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Ind_Relat_Phone]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Ind_Relat_Phone_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Ind_Relat_Phone_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Ind_Relat_Attr_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Ind_Relat_Attr_v]

			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat_Phone]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Org_Relat_Phone]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat_Phone_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Org_Relat_Phone_v]	
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Org_Relat]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Org_Relat_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat_Attr_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Org_Relat_Attr_v]
				
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Package_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Package_v]

			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Proposal_Attr_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Proposal_Attr_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Proposal_Notes_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Proposal_Notes_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Proposal_Solicitor_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Proposal_Solicitor_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Proposal_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Proposal_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Prospect_Other_Gifts_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Prospect_Other_Gifts_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Prospect_Philanthropic_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Prospect_Philanthropic_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Prospect_Financial_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Prospect_Financial_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Prospect_Rating_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Prospect_Rating_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Prospect_WillNotGive_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Prospect_WillNotGive_v]

			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_CampaignFund_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_CampaignFund_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_CampaignAppeal_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_CampaignAppeal_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_CampaignSolicitor_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_CampaignSolicitor_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Bank_Financial_Institution_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Bank_Financial_Institution_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Bank_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Bank_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Address_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Address_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_PivotWizard_p]') and objectproperty(id, N'IsProcedure') = 1)
				drop procedure [dbo].[HC_PivotWizard_p]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_PivotWizard_IRPhones_p]') and objectproperty(id, N'IsProcedure') = 1)
				drop procedure [dbo].[HC_PivotWizard_IRPhones_p]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_PivotWizard_ORPhones_p]') and objectproperty(id, N'IsProcedure') = 1)
				drop procedure [dbo].[HC_PivotWizard_ORPhones_p]

			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Phone_t]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Cons_Phone_t]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Ind_Relat_Phone_t]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].HC_Ind_Relat_Phone_t
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat_Phone_t]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].HC_Org_Relat_Phone_t
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Fund_Relationship_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Fund_Relationship_v] 
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Users_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Users_v]

			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Volunteer_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Volunteer_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Volunteer_Type_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Volunteer_Type_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Volunteer_Availability_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Volunteer_Availability_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Volunteer_Assignment_Interest_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Volunteer_Assignment_Interest_v]

			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Adjustments_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_Adjustments_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Amendments_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_Amendments_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Appeal_Expenses_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Appeal_Expenses_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Appeal_Benefits_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Appeal_Benefits_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Package_Benefits_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Package_Benefits_v]

			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Fund_GLDistribution_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Fund_GLDistribution_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Planned_Gift_Asset_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Planned_Gift_Asset_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_ProposalLink_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Gift_ProposalLink_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat_NonConsIR_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Org_Relat_NonConsIR_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Planned_Gift_Beneficiaries_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Planned_Gift_Beneficiaries_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Planned_Gift_Relationship_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Planned_Gift_Relationship_v]
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Media_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].[HC_Media_v]
				  
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Fund_Attr_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].HC_Fund_Attr_v
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Campaign_Attr_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].HC_Campaign_Attr_v
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Appeal_Attr_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].HC_Appeal_Attr_v
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Package_Attr_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].HC_Package_Attr_v
			  
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Membership_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].HC_Membership_v
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Membership_History_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].HC_Membership_History_v
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Membership_Benefits_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].HC_Membership_Benefits_v
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Membership_Linked_Gift_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].HC_Membership_Linked_Gift_v
			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Membeship_Solicitor_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].HC_Membeship_Solicitor_v

			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Annotation_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].HC_Cons_Annotation_v

			if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Tribute_Acknowledgee_v]') and objectproperty(id, N'IsTable') = 1)
				drop table [dbo].HC_Tribute_Acknowledgee_v
   	 

--04. Consolidate data
	
		SET NOCOUNT ON
		GO
		    
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Appeal_Attr.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Appeal_Benefits.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Appeal_Expenses.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Appeal.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Bank_Financ_Ins.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Campaign_Attr.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Campaign.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Campaign_Appeal.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Campaign_Fund.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Campaign_Solicitor.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Cons Action Attr.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Cons Action Notes.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Cons Action Solicitor.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Cons Action.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent AdditAddSal.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent AddressAttr.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent Address.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent Alias.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent Annotation.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent Appeal.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent Attributes.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent Bank.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent Codes.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent Notes.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent Phones.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent SolicitCode.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent Solicitor.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Constituent Tribute.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Education Relat Attr.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Education Relat Major.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Education Relat Minor.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Education Relat.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Event Attr.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Event Award.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Event Expense.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Event Menu.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Event Notes.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Event Participant Attr.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Event Participant.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Event Price.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Event Registration Fees.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Event.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Fund_Attr.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Fund GL Distribution.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Fund Relat.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Fund.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Adjustments.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Amendments.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Attribute.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Benefits.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Installment.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Link.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Matching Link.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Notes.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Proposal Link.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Softcredit.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Solicitor.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Split.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Tribute.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Gift Writeoff.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Ind Relat Attr.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Ind Relat Phone.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Ind Relat.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Media.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Membership.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Membership Benefits.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Membership History.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Membership Linked Gifts.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Membership Solicitor.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Org Relat Attr.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Org_Relat_NonConsIR.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Org Relat Phone.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Org Relat.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Package_Attr.sql"				
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Package_Benefits.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Package.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Planned_Gift_Asset.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Planned_Gift_Beneficiaries.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Planned_Gift_Relationship.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Proposal Attr.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Proposal Notes.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Proposal Solicitor.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Proposal.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Prospect Financial.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Prospect Other Gift.sql"  --DATA ISSUES NEED TO RUN AGAIN
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Prospect Philanthropic.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Prospect Rating.sql"		--DATA ISSUES NEED TO RUN AGAIN
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Prospect WillNotGive.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\SecurityUsers.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Volunteer Assignment Interest.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Volunteer Availability.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Volunteer Type.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Volunteer.sql"
		:r "D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_DATA\Tribute Acknowledgee.sql"


	
		
