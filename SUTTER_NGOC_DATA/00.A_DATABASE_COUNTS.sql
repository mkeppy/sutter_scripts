-- select and execute
		
	USE RE7_SH_SMCF
	GO 
	
	
		DROP TABLE  dbo.ViewCountTracker 
		GO	
		DROP TABLE #Data
		GO 
		
        CREATE TABLE dbo.ViewCountTracker
            (
              ViewName CHAR(255) ,
              recordcount INT
            );
        SET NOCOUNT ON;
        DECLARE @ViewName AS NVARCHAR(128);
        DECLARE @TmpQuery AS NVARCHAR(500);
        DECLARE @Out3 AS INT;
        DECLARE Cur_Views CURSOR
        FOR
            SELECT  SCHEMA_NAME(schema_id) + '.' + name AS "Table_Name"
            FROM    RE7_SH_SMCF.[sys].[all_views];  
        OPEN Cur_Views;
        FETCH NEXT FROM Cur_Views INTO @ViewName;
        WHILE @@Fetch_Status = 0
            BEGIN
				--SELECT  @Query = 'SELECT COUNT(*) AS [Count] FROM ' + @ViewName
				--EXECUTE(@Query)
                CREATE TABLE #Data ( var INT );
                SELECT  @TmpQuery = 'SELECT COUNT(*) AS [Count] FROM '
                        + @ViewName; 
                INSERT  #Data
                        EXEC ( @TmpQuery
                            );
                SELECT  @Out3 = var
                FROM    #Data;
                PRINT @ViewName;
                PRINT @Out3;
                INSERT  INTO dbo.ViewCountTracker
                VALUES  ( @ViewName, @Out3 );
                DROP TABLE #Data;
                FETCH NEXT FROM Cur_Views INTO @ViewName;
            END;
        CLOSE Cur_Views;
		GO
        DEALLOCATE Cur_Views;
		GO


        SELECT  *
        FROM    RE7_SH_SMCF.dbo.ViewCountTracker
        WHERE   ViewName LIKE 'dbo.HC_%_v'
        ORDER BY ViewName;
		
	 
			