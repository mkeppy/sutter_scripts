   

    --HC_Planned_Gift_Beneficiaries_v @ dbo.[HC_Planned_Gift_Beneficiaries_v]
	--Compile multiple databases into 1 in SS2008R2. 
	--RE7_SH_ABSF;RE7_SH_DMHF;RE7_SH_EMCF;RE7_SH_CVRX;RE7_SH_MPHF;RE7_SH_PAMF;RE7_SH_SAFH;RE7_SH_SAHF;RE7_SH_SCAH;RE7_SH_SDHF;RE7_SH_SMCF;RE7_SH_SMFn;RE7_SH_SRMC;RE7_SH_SSCF
	 

--RE7_SH_SMCF
  SELECT * INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Planned_Gift_Beneficiaries_v]

--RE7_SH_SAFH
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Planned_Gift_Beneficiaries_v]


--RE7_SH_ABSF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_ABSF.dbo.[HC_Planned_Gift_Beneficiaries_v]

--RE7_SH_WBRX
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_WBRX.dbo.[HC_Planned_Gift_Beneficiaries_v]
--RE7_SH_HOTV
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_HOTV.dbo.[HC_Planned_Gift_Beneficiaries_v]

--RE7_SH_DMHF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_DMHF.dbo.[HC_Planned_Gift_Beneficiaries_v]

--RE7_SH_EMCF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_EMCF.dbo.[HC_Planned_Gift_Beneficiaries_v]

--RE7_SH_CVRX
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_CVRX.dbo.[HC_Planned_Gift_Beneficiaries_v]

--RE7_SH_MPHF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_MPHF.dbo.[HC_Planned_Gift_Beneficiaries_v]

--RE7_SH_PAMF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_PAMF.dbo.[HC_Planned_Gift_Beneficiaries_v]


--RE7_SH_SAHF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAHF.dbo.[HC_Planned_Gift_Beneficiaries_v]

--RE7_SH_SCAH
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SCAH.dbo.[HC_Planned_Gift_Beneficiaries_v]

--RE7_SH_SDHF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SDHF.dbo.[HC_Planned_Gift_Beneficiaries_v]


--RE7_SH_SMFX
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SMFX.dbo.[HC_Planned_Gift_Beneficiaries_v]

--RE7_SH_SRMC
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SRMC.dbo.[HC_Planned_Gift_Beneficiaries_v]

--RE7_SH_SSCF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SSCF.dbo.[HC_Planned_Gift_Beneficiaries_v]

 
--SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] 
/*
 
SELECT  * FROM SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] 
SELECT RE_DB_OwnerShort, COUNT(*) FROM SUTTER_1P_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] group by RE_DB_OwnerShort

*/

  