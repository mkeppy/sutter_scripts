
/*
Changes log.
2012-07-20: added chart.ind_relat_attr and chart.org_relat_attr
2012-07-18: Added chart.ORContactType.

*/
USE RE7_SH_ABSF
GO
		IF schema_id('CHART') IS NULL EXECUTE('CREATE SCHEMA CHART') 
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[AddSalType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[AddSalType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[AddressType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[AddressType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[AddrState]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[AddrState]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[AddrCountry]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[AddrCountry]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[AddrAttribute]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[AddrAttribute]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[PhoneType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[PhoneType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[PhoneType_alt]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[PhoneType_alt]

		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConsNoteType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ConsNoteType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConsTributeType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ConsTributeType]

		IF exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[CampAppPack]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[CampAppPack]
		IF exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[FundCampAppPack]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[FundCampAppPack]
		
		IF exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[AppealPackage]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[AppealPackage]
			
		IF exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GSplit_CampApp]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GSplit_CampApp]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GSplit_CampFund]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GSplit_CampFund]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GSplit_CampFundApp]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GSplit_CampFundApp]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GSplit_CampFundAppPack]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GSplit_CampFundAppPack]

		IF exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Fund]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[Fund]
		IF exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Fund_GL_Distribution]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[Fund_GL_Distribution]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Campaign]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[Campaign]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Appeal]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[Appeal]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftNoteType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GiftNoteType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Suffix]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[Suffix]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Title]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[Title]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConsAttribute]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ConsAttribute]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConsCode]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ConsCode]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[SolicitCode]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[SolicitCode]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftAttribute]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GiftAttribute]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftTributeType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GiftTributeType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GiftType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftConsCode]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GiftConsCode]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftSubType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GiftSubType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftCode]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GiftCode]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GiftTypeSubTypeCode]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GiftTypeSubTypeCode]	
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GFLtrCode]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GFLtrCode]	
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GFPayMethod]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GFPayMethod]	
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[GFTypePayMethod]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[GFTypePayMethod] 
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[EventType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[EventType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ParticipantType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ParticipantType]

		IF exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_tbl_RelationshipTypes_t]') and objectproperty(id, N'IsTable') = 1)
			drop table [dbo].[HC_tbl_RelationshipTypes_t]
		if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_CampaignAppealPackage_t]') and objectproperty(id, N'IsTable') = 1)
			drop table [dbo].[HC_CampaignAppealPackage_t]
		if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_FundCampaignAppealPackage_t]') and objectproperty(id, N'IsTable') = 1)
			drop table [dbo].[HC_FundCampaignAppealPackage_t]
		IF exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_AppealPackage_t]') and objectproperty(id, N'IsTable') = 1)
			drop table [dbo].[HC_AppealPackage_t]
			

		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[RelationshipType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[RelationshipType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[RelatRecipType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[RelatRecipType]

		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ActionCategoryType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ActionCategoryType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ActionType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ActionType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ActionAttribute]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ActionAttribute]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ActionStatus]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ActionStatus]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConsProspectClass]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ConsProspectClass]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[IRContactType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[IRContactType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ORContactType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ORContactType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[IndRelatAttr]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[IndRelatAttr]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[OrgRelatAttr]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[OrgRelatAttr]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Attributes]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[Attributes]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[Users]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[Users]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ASRType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ASRType]
						
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[AppealPackage]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[AppealPackage]

		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConstituencyCodes]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ConstituencyCodes]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ProposalAttribute]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ProposalAttribute]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ProposalStatus]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ProposalStatus]

		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ProspectRating]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ProspectRating]

		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[MediaType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[MediaType]

		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ParticipantAttr]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ParticipantAttr]
		  
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[EventAttr]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[EventAttr]
		 
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ConsAlias]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ConsAlias]
		  
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ActionSolicitor]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ActionSolicitor]
		
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[IRPhoneType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[IRPhoneType]
		if exists (select * from dbo.sysobjects where id = object_id(N'[CHART].[ORPhoneType]') and objectproperty(id, N'IsView') = 1)
			drop view [CHART].[ORPhoneType]
		  
		
	
--CHART Additional Addressee Salutation Type
		SET ANSI_NULLS ON;
		GO
		SET QUOTED_IDENTIFIER ON;
		GO
		CREATE VIEW [CHART].[AddSalType]
		AS
			SELECT  RE_DB_OwnerShort ,
					AddSalType ,
					COUNT(*) AS Count
			FROM    dbo.HC_Cons_Addl_Addressee_v
			GROUP BY RE_DB_OwnerShort ,
					AddSalType;
		 
		GO
		SET ANSI_NULLS OFF;
		GO
		SET QUOTED_IDENTIFIER OFF;
		GO

--CHART Address Type  All
		SET ANSI_NULLS ON;
		GO
		SET QUOTED_IDENTIFIER ON;
		GO
		CREATE VIEW [CHART].[AddressType]
		AS
			SELECT  RE_DB_OwnerShort ,
					AddrType ,
					COUNT(*) AS Count
			FROM    dbo.HC_Cons_Address_v
			GROUP BY RE_DB_OwnerShort ,
					AddrType;

		GO
		SET ANSI_NULLS OFF;
		GO
		SET QUOTED_IDENTIFIER OFF;
		GO

		 

--CHART AddressState 
        SET ANSI_NULLS ON;
		GO
        SET QUOTED_IDENTIFIER ON;
		GO
        CREATE VIEW [CHART].[AddrState]
        AS
            SELECT  RE_DB_OwnerShort ,
                    AddrState ,
                    COUNT(*) AS Count
            FROM    dbo.HC_Cons_Address_v
            GROUP BY RE_DB_OwnerShort ,
                    AddrState;

		GO
        SET ANSI_NULLS OFF;
		GO
        SET QUOTED_IDENTIFIER OFF;
		GO

 
-- CHART Countries  
        SET ANSI_NULLS ON;
		GO
        SET QUOTED_IDENTIFIER ON;
		GO
        CREATE VIEW [CHART].[AddrCountry]
        AS
            SELECT  RE_DB_OwnerShort ,
                    AddrCountry ,
                    COUNT(*) AS Count
            FROM    dbo.HC_Cons_Address_v
            GROUP BY RE_DB_OwnerShort ,
                    AddrCountry;
		 
		GO
        SET ANSI_NULLS OFF;
		GO
        SET QUOTED_IDENTIFIER OFF;
		GO


-- CHART AddrAttribute    
        SET ANSI_NULLS ON;
		GO
        SET QUOTED_IDENTIFIER ON;
		GO
        CREATE VIEW [CHART].[AddrAttribute]
        AS
            SELECT  RE_DB_OwnerShort ,
                    CADAttrCat ,
                    CASE WHEN CADAttrDesc IS NULL THEN 'NULL' ELSE CADAttrDesc END AS CADAttrDesc,
                    DataType ,
                    COUNT(*) AS Count
            FROM    dbo.HC_Cons_Address_Attr_v
            GROUP BY RE_DB_OwnerShort ,
                    CADAttrCat ,
                    CADAttrDesc ,
                    DataType;
				
		GO
        SET ANSI_NULLS OFF;
		GO
        SET QUOTED_IDENTIFIER OFF;
		GO


--CHART PhoneTypePrefAddr 
        SET ANSI_NULLS ON;
		GO
        SET QUOTED_IDENTIFIER ON;
		GO
        CREATE VIEW [CHART].[PhoneType]
        AS
            SELECT  p.RE_DB_OwnerShort ,
					c.KeyInd,
					p.PhoneType ,
                    COUNT(*) AS Count
            FROM    dbo.HC_Cons_Phone_v p
            INNER JOIN dbo.HC_Constituents_v c ON p.ImportID=c.ImportID
            INNER JOIN dbo.HC_Cons_Address_v a ON p.PhoneAddrImpID=a.AddrImpID
            WHERE a.AddrPref='TRUE'
            GROUP BY p.RE_DB_OwnerShort , c.KeyInd, p.PhoneType
		GO
        SET ANSI_NULLS OFF;
		GO
        SET QUOTED_IDENTIFIER OFF;
		GO



--CHART PhoneType_NONPrefAddr 
        SET ANSI_NULLS ON;
		GO
        SET QUOTED_IDENTIFIER ON;
		GO
        CREATE VIEW [CHART].[PhoneType_alt]
        AS
            SELECT  p.RE_DB_OwnerShort ,
					c.KeyInd,
					p.PhoneType ,
                    COUNT(*) AS Count
            FROM    dbo.HC_Cons_Phone_v p
            INNER JOIN dbo.HC_Constituents_v c ON p.ImportID=c.ImportID
            INNER JOIN dbo.HC_Cons_Address_v a ON p.PhoneAddrImpID=a.AddrImpID
            WHERE a.AddrPref='FALSE'
            GROUP BY p.RE_DB_OwnerShort , c.KeyInd, p.PhoneType
            
		 
		GO
        SET ANSI_NULLS OFF;
		GO
        SET QUOTED_IDENTIFIER OFF;
		GO


--CHART IRPhoneType  -- includes only NON-cons IndRelat
        SET ANSI_NULLS ON;
		GO
        SET QUOTED_IDENTIFIER ON;
		GO
        CREATE VIEW [CHART].[IRPhoneType]
        AS
            SELECT  p.RE_DB_OwnerShort ,
					p.IRPhoneType ,
                    COUNT(*) AS Count
            FROM    dbo.HC_Ind_Relat_Phone_v p
            INNER JOIN dbo.HC_Ind_Relat_v r ON p.IRPhoneIRImpID=r.IRImpID
			WHERE r.IRLink IS null OR r.IRLink=''     
            GROUP BY p.RE_DB_OwnerShort, p.IRPhoneType
     	 
		GO
        SET ANSI_NULLS OFF;
		GO
        SET QUOTED_IDENTIFIER OFF;
		GO

--CHART ORPhoneType  -- includes only NON-cons ORGRelat
        SET ANSI_NULLS ON;
		GO
        SET QUOTED_IDENTIFIER ON;
		GO
        CREATE VIEW [CHART].[ORPhoneType]
        AS
            SELECT  p.RE_DB_OwnerShort ,
					p.ORPhoneType ,
                    COUNT(*) AS Count
            FROM    dbo.HC_Org_Relat_Phone_v p
            INNER JOIN dbo.HC_Org_Relat_v r ON p.ORPhoneORImpID=r.ORImpID
			WHERE r.ORLink IS null OR r.ORLink=''     
            GROUP BY p.RE_DB_OwnerShort, p.ORPhoneType
     	 
		GO
        SET ANSI_NULLS OFF;
		GO
        SET QUOTED_IDENTIFIER OFF;
		GO




-- CHART Constituent Notepad Type    
        SET ANSI_NULLS ON;
		GO
        SET QUOTED_IDENTIFIER ON;
		GO
        CREATE VIEW [CHART].[ConsNoteType]
        AS
            SELECT  RE_DB_OwnerShort ,
                    NoteType ,
                    COUNT(*) Count
            FROM    HC_Cons_Notes_v
            GROUP BY RE_DB_OwnerShort ,
                    NoteType;

		GO
        SET ANSI_NULLS OFF;
		GO
        SET QUOTED_IDENTIFIER OFF;
		GO
 

-- CHART Constituent Tribute Type    
        SET ANSI_NULLS ON;
		GO
        SET QUOTED_IDENTIFIER ON;
		GO
        CREATE VIEW [CHART].[ConsTributeType]
        AS
            SELECT  RE_DB_OwnerShort ,
                    TRType ,
                    COUNT(*) CountOfTRType
            FROM    dbo.HC_Cons_Tribute_v
            GROUP BY RE_DB_OwnerShort ,
                    TRType;

		GO

        SET ANSI_NULLS OFF;
		GO
        SET QUOTED_IDENTIFIER OFF;
		GO




-- CHART Fund   --  select all FUNDS from main FUND tbl and tags which ones are used on gifts, proposals, actions.
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[Fund]
		AS
 				SELECT  f.RE_DB_OwnerShort, f.FundID, f.FundDesc,   count(*) as Count, 
 				CONVERT(decimal(18,2), sum(s.GSplitAmt)) as [GiftSum],
				case when s.GSplitFund is null then 'No' else 'Yes' end as Gifts,
				case when p.PRFundID is null then 'No' else 'Yes' end as Proposals,
				case when a.ACFund  is null then 'No' else 'Yes' end as ConsActions,
				f.FundType, f.FundCategory, f.FundIsRestricted
				FROM dbo.HC_Fund_v f 
				left join dbo.HC_Gift_SplitGift_v s on f.fundid= s.GSplitFund AND f.RE_DB_OwnerShort=s.RE_DB_OwnerShort
				left join (SELECT DISTINCT PRFundID, RE_DB_OwnerShort FROM dbo.HC_Proposal_v) p on p.PRFundID = f.FundID AND p.RE_DB_OwnerShort = f.RE_DB_OwnerShort  
				left join (SELECT DISTINCT ACFund, RE_DB_OwnerShort FROM dbo.HC_Cons_Action_v) a on a.ACFund = f.FundID  AND a.RE_DB_OwnerShort = f.RE_DB_OwnerShort    
				GROUP BY f.RE_DB_OwnerShort, f.fundID, f.FundDesc, p.PRFundID, s.GSplitFund, a.ACFund, f.FundType, f.FundCategory, f.FundIsRestricted
		 
		 
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO
		
-- CHART [Fund_GL_Distribution]   
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[Fund_GL_Distribution]
		AS
 				SELECT f.RE_DB_OwnerShort, f.FundDistrGiftType 
 				FROM dbo.HC_Fund_GLDistribution_v f 
				WHERE f.FundDistrGiftType IS NOT NULL 
				GROUP BY f.RE_DB_OwnerShort, f.FundDistrGiftType
		 
		 
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO
				
		 

-- CHART APPEAL --  select all APPEALS from main Appeal tbl and tags which ones are used on gifts, cons appeals, actions.
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[Appeal]
		AS
			select  c.RE_DB_OwnerShort, c.AppealID, c.AppDesc, count(*) as [Count], convert(decimal(18,2), sum(s.GSplitAmt)) as [GiftSum],
			case when s.GSplitAppeal is null then 'No' else 'Yes' end as Gifts,
			case when r.CAPAppealID is null then 'No' else 'Yes' end as ConsAppeals,
			case when a.ACAppeal  is null then 'No' else 'Yes' end as ConsActions

			from dbo.HC_Appeal_v c
			left join dbo.HC_Gift_SplitGift_v s on c.AppealID = s.GSplitAppeal AND c.RE_DB_OwnerShort = s.RE_DB_OwnerShort  
			left join (SELECT DISTINCT CAPAppealID, RE_DB_OwnerShort FROM dbo.HC_Cons_Appeal_v) r on r.CAPAppealID = c.AppealID  AND r.RE_DB_OwnerShort = c.RE_DB_OwnerShort  
			left join (SELECT DISTINCT ACAppeal, RE_DB_OwnerShort FROM dbo.HC_Cons_Action_v) a on a.ACAppeal= c.AppealID AND a.RE_DB_OwnerShort = c.RE_DB_OwnerShort  
			group by c.RE_DB_OwnerShort, c.AppealID, c.AppDesc, s.GSplitAppeal, r.CAPAppealID, a.ACAppeal
		 
		 

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO



-- CHART CAMPAIGN -- select all CAMPAIGNS from main camp tbl and tags which ones are used on gifts, prop, actions. 
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[Campaign]
		AS
			select  c.RE_DB_OwnerShort, c.CampID, c.CampDesc, count(*) as [Count], convert(decimal(18,2), sum(s.GSplitAmt)) as [GiftSum],
			case when s.GSplitCamp is null then 'No' else 'Yes' end as Gifts,
			case when p.PRCampID is null then 'No' else 'Yes' end as Proposals,
			case when a.ACCampaign  is null then 'No' else 'Yes' end as ConsActions

			from dbo.HC_Campaign_v c 
			left join dbo.HC_Gift_SplitGift_v s on c.CampID = s.GSplitCamp AND c.RE_DB_OwnerShort = s.RE_DB_OwnerShort  
			left join (SELECT DISTINCT PRCampID, RE_DB_OwnerShort FROM dbo.HC_Proposal_v) p on p.PRCampID = c.CampID AND c.RE_DB_OwnerShort = p.RE_DB_OwnerShort  
			left join (SELECT DISTINCT ACCampaign, RE_DB_OwnerShort FROM dbo.HC_Cons_Action_v) a on a.ACCampaign= c.CampID AND a.RE_DB_OwnerShort = c.RE_DB_OwnerShort
			group by c.RE_DB_OwnerShort, c.CampID, s.GSplitCamp, c.CampDesc, p.PRCampID, a.ACCampaign
					 

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO



-- CHART [GSplit_CampApp] 
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GSplit_CampApp]
		AS

		select s.RE_DB_OwnerShort, 
		CASE WHEN s.GSplitCamp IS NULL THEN 'NULL' ELSE s.GSplitCamp END AS CampID, c.CampDesc, 
		CASE WHEN s.GSplitAppeal IS NULL THEN 'NULL' ELSE s.GSplitAppeal END AS AppealID, a.AppDesc, count(*) as Count, convert(decimal(18,2), sum(s.GSplitAmt)) as [Sum]
		from dbo.HC_Gift_SplitGift_v s
		left join dbo.HC_Campaign_v c on s.GSplitCamp = c.CampID  AND c.RE_DB_OwnerShort = s.RE_DB_OwnerShort   
		left join dbo.HC_Appeal_v   a on s.GSplitAppeal = a.AppealID AND a.RE_DB_OwnerShort = s.RE_DB_OwnerShort  
		group by s.RE_DB_OwnerShort, s.GSplitCamp, s.GSplitAppeal, c.CampDesc, a.AppDesc
		 

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO

-- CHART CampaignFundAppeal
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GSplit_CampFundApp]
		AS

		select	s.RE_DB_OwnerShort, 
				CASE WHEN s.GSplitCamp IS NULL THEN 'NULL' ELSE s.GSplitCamp END AS CampID, c.CampDesc, 
				s.GSplitFund AS FundID, f.FundDesc,
				CASE WHEN s.GSplitAppeal IS NULL THEN 'NULL' ELSE s.GSplitAppeal END AS AppealID, a.AppDesc, 
				count(*) as Count, convert(decimal(18,2), sum(s.GSplitAmt)) as [Sum]

		from dbo.HC_Gift_SplitGift_v s
		left join dbo.HC_Campaign_v c on s.GSplitCamp =   c.CampID	AND c.RE_DB_OwnerShort = s.RE_DB_OwnerShort 
		left join dbo.HC_Fund_v     f on s.GSplitFund =   f.FundID	AND f.RE_DB_OwnerShort = s.RE_DB_OwnerShort 
		left join dbo.HC_Appeal_v   a on s.GSplitAppeal = a.AppealID AND a.RE_DB_OwnerShort = s.RE_DB_OwnerShort 

		group by s.RE_DB_OwnerShort, s.GSplitCamp, s.GSplitAppeal, c.CampDesc, a.AppDesc, s.GSplitFund, f.FundDesc


		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO



		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO

-- CHART CampaignFundAppeal
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GSplit_CampFundAppPack]
		AS

		select	s.RE_DB_OwnerShort, 
				CASE WHEN s.GSplitCamp IS NULL THEN 'NULL' ELSE s.GSplitCamp END AS CampID, c.CampDesc, 
				s.GSplitFund AS FundID, f.FundDesc,
				CASE WHEN s.GSplitAppeal IS NULL THEN 'NULL' ELSE s.GSplitAppeal END AS AppealID, a.AppDesc, 
				CASE WHEN s.GSplitPkg IS NULL THEN 'NULL' ELSE s.GSplitPkg  END AS PackageID,
				count(*) as Count, convert(decimal(18,2), sum(s.GSplitAmt)) as [Sum]
		from dbo.HC_Gift_SplitGift_v s
		left join dbo.HC_Campaign_v c on s.GSplitCamp = c.CampID AND c.RE_DB_OwnerShort = s.RE_DB_OwnerShort 
		left join dbo.HC_Fund_v     f on s.GSplitFund = f.FundID	AND f.RE_DB_OwnerShort = s.RE_DB_OwnerShort 
		left join dbo.HC_Appeal_v   a on s.GSplitAppeal = a.AppealID AND a.RE_DB_OwnerShort = s.RE_DB_OwnerShort 
		left join (SELECT DISTINCT PackageID, RE_DB_OwnerShort FROM dbo.HC_Package_v) p on p.PackageID=s.GSplitPkg AND p.RE_DB_OwnerShort = s.RE_DB_OwnerShort 
		group by s.RE_DB_OwnerShort, s.GSplitCamp, s.GSplitAppeal, s.GSplitFund, s.GSplitPkg, c.CampDesc, a.AppDesc, f.FundDesc 


		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO



-- [CHART].[GSplit_CampaignFund]
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GSplit_CampFund]
		AS
		select s.RE_DB_OwnerShort, CASE WHEN s.GSplitCamp IS NULL THEN 'NULL' ELSE s.GSplitCamp END AS CampID, c.CampDesc, 
		s.GSplitFund AS FundID, a.FundDesc, count(*) as Count, convert(decimal(18,2), sum(s.GSplitAmt)) as [Sum]

		from dbo.HC_Gift_SplitGift_v s
		left join dbo.HC_Campaign_v c on s.GSplitCamp = c.CampID AND c.RE_DB_OwnerShort=s.RE_DB_OwnerShort
		left join dbo.HC_Fund_v   a on s.GSplitFund = a.FundID AND a.RE_DB_OwnerShort=s.RE_DB_OwnerShort
		group by s.RE_DB_OwnerShort, s.GSplitCamp, s.GSplitFund, c.CampDesc, a.FundDesc

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO


--CHARTS OF Campaign and appeals and pacakge from gsplit and cons appeals

	--CHART_CampaignAppealPackage  (GIFT and CONS_APPEALS records combined)

	--CONS APPEALS
		select t.RE_DB_OwnerShort,  null as CampID, null as CampDesc, t.CAPAppealId AppealID, dbo.HC_Appeal_v.AppDesc, 
		t.CAPPAckageId PackageID ,  dbo.HC_Package_v.PackDesc, count(*) as Count, null as totalamt

		INTO dbo.HC_CampaignAppealPackage_t-- drop table dbo.HC_CampaignAppealPackage_t
		FROM [dbo].[HC_Cons_Appeal_v] t
		LEFT OUTER JOIN dbo.HC_Package_v ON T.RE_DB_OwnerShort = dbo.HC_Package_v.RE_DB_OwnerShort AND T.CAPAppealId= dbo.HC_Package_v.AppealID AND T.CAPPackageId= dbo.HC_Package_v.PackageID 
		LEFT OUTER JOIN dbo.HC_Appeal_v ON T.RE_DB_OwnerShort = dbo.HC_Appeal_v.RE_DB_OwnerShort AND T.CAPAppealId= dbo.HC_Appeal_v.AppealID
		group by t.RE_DB_OwnerShort, t.CAPAppealId, t.CAPPAckageId, dbo.HC_Appeal_v.AppDesc,  dbo.HC_Package_v.PackDesc

		UNION all

	--GIFTS
		SELECT     dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort, dbo.HC_Gift_SplitGift_v.GSplitCamp as CampID, dbo.HC_Campaign_v.CampDesc, 
							  dbo.HC_Gift_SplitGift_v.GSplitAppeal as AppealID, dbo.HC_Appeal_v.AppDesc, dbo.HC_Gift_SplitGift_v.GSplitPkg as PackageID, 
							  dbo.HC_Package_v.PackDesc, COUNT(*) RecordCount, sum(dbo.HC_Gift_SplitGift_v.GSplitAmt) as totalamt
		FROM         dbo.HC_Gift_SplitGift_v 
		LEFT OUTER JOIN dbo.HC_Campaign_v ON dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort = dbo.HC_Campaign_v.RE_DB_OwnerShort AND dbo.HC_Gift_SplitGift_v.GSplitCamp = dbo.HC_Campaign_v.CampID 
		LEFT OUTER JOIN dbo.HC_Package_v ON dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort = dbo.HC_Package_v.RE_DB_OwnerShort AND dbo.HC_Gift_SplitGift_v.GSplitAppeal = dbo.HC_Package_v.AppealID AND dbo.HC_Gift_SplitGift_v.GSplitPkg = dbo.HC_Package_v.PackageID 
		LEFT OUTER JOIN dbo.HC_Appeal_v ON dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort = dbo.HC_Appeal_v.RE_DB_OwnerShort AND dbo.HC_Gift_SplitGift_v.GSplitAppeal = dbo.HC_Appeal_v.AppealID
		GROUP BY dbo.HC_Gift_SplitGift_v.GSplitCamp, dbo.HC_Campaign_v.CampDesc, dbo.HC_Gift_SplitGift_v.GSplitAppeal, dbo.HC_Appeal_v.AppDesc, 
							  dbo.HC_Gift_SplitGift_v.GSplitPkg, dbo.HC_Package_v.PackDesc, dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort
		 
		GO 
				GO
				SET ANSI_NULLS OFF
				GO
				SET QUOTED_IDENTIFIER OFF
				GO

				-- CHART CampaignAppealPackage
				SET ANSI_NULLS ON
				GO
				SET QUOTED_IDENTIFIER ON
				GO
				CREATE VIEW [CHART].[CampAppPack]
				AS
				SELECT	s.RE_DB_OwnerShort, 
						CASE WHEN s.CampID IS NULL or s.CampID ='' THEN 'NULL' ELSE s.CampID END AS CampID, s.CampDesc, 
						CASE WHEN s.AppealID  IS NULL or s.AppealID ='' THEN 'NULL' ELSE s.AppealID END AS AppealID, s.AppDesc, 
						CASE WHEN s.PackageId  IS NULL or s.PackageId ='' THEN 'NULL' ELSE s.PackageId END AS PackageID, s.PackDesc,
						sum(s.[Count]) as [Count], convert(decimal(18,2), sum(s.totalamt)) as [Sum]

				FROM dbo.HC_CampaignAppealPackage_t s
				GROUP BY s.RE_DB_OwnerShort, s.CampID, s.AppealID, s.CampDesc, s.AppDesc, s.PackageId, s.PackDesc

				GO
				SET ANSI_NULLS OFF
				GO
				SET QUOTED_IDENTIFIER OFF
				GO
	  



--CHARTS OF FUND, CAMP, APPEAL, PKG from GSPlit and ConsAppeal

	--CHART_FundCampaignAppealPackage  (GIFT and CONS_APPEALS records combined)

	--CONS APPEALS
		select t.RE_DB_OwnerShort, NULL AS FundId, NULL AS FundDesc, null as CampID, null as CampDesc, t.CAPAppealId AppealID, dbo.HC_Appeal_v.AppDesc, 
		t.CAPPAckageId PackageID ,  dbo.HC_Package_v.PackDesc, count(*) as Count, null as totalamt

		INTO dbo.HC_FundCampaignAppealPackage_t-- drop table dbo.HC_CampaignAppealPackage_t
		FROM [dbo].[HC_Cons_Appeal_v] t
		LEFT OUTER JOIN dbo.HC_Package_v ON T.RE_DB_OwnerShort = dbo.HC_Package_v.RE_DB_OwnerShort AND T.CAPAppealId= dbo.HC_Package_v.AppealID AND T.CAPPackageId= dbo.HC_Package_v.PackageID 
		LEFT OUTER JOIN dbo.HC_Appeal_v ON T.RE_DB_OwnerShort = dbo.HC_Appeal_v.RE_DB_OwnerShort AND T.CAPAppealId= dbo.HC_Appeal_v.AppealID
		group by t.RE_DB_OwnerShort, t.CAPAppealId, t.CAPPAckageId, dbo.HC_Appeal_v.AppDesc,  dbo.HC_Package_v.PackDesc

		UNION all

	--GIFTS
		SELECT	dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort, 
				dbo.HC_Gift_SplitGift_v.GSplitFund AS FundID, dbo.HC_Fund_v.FundDesc, 
				dbo.HC_Gift_SplitGift_v.GSplitCamp as CampID, dbo.HC_Campaign_v.CampDesc, 
				dbo.HC_Gift_SplitGift_v.GSplitAppeal as AppealID, dbo.HC_Appeal_v.AppDesc, 
				dbo.HC_Gift_SplitGift_v.GSplitPkg as PackageID, dbo.HC_Package_v.PackDesc, 
				COUNT(*) RecordCount, sum(dbo.HC_Gift_SplitGift_v.GSplitAmt) as totalamt
		
		FROM    dbo.HC_Gift_SplitGift_v 
		LEFT OUTER JOIN dbo.HC_Fund_v ON	 dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort = dbo.HC_Fund_v.RE_DB_OwnerShort AND dbo.HC_Gift_SplitGift_v.GSplitFund= dbo.HC_Fund_v.FundID 
		LEFT OUTER JOIN dbo.HC_Campaign_v ON dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort = dbo.HC_Campaign_v.RE_DB_OwnerShort AND dbo.HC_Gift_SplitGift_v.GSplitCamp = dbo.HC_Campaign_v.CampID 
		LEFT OUTER JOIN dbo.HC_Package_v ON  dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort = dbo.HC_Package_v.RE_DB_OwnerShort AND dbo.HC_Gift_SplitGift_v.GSplitAppeal = dbo.HC_Package_v.AppealID AND dbo.HC_Gift_SplitGift_v.GSplitPkg = dbo.HC_Package_v.PackageID 
		LEFT OUTER JOIN dbo.HC_Appeal_v ON   dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort = dbo.HC_Appeal_v.RE_DB_OwnerShort AND dbo.HC_Gift_SplitGift_v.GSplitAppeal = dbo.HC_Appeal_v.AppealID
		GROUP BY dbo.HC_Gift_SplitGift_v.GSplitFund, dbo.HC_Fund_v.FundDesc, dbo.HC_Gift_SplitGift_v.GSplitCamp, dbo.HC_Campaign_v.CampDesc, dbo.HC_Gift_SplitGift_v.GSplitAppeal, dbo.HC_Appeal_v.AppDesc, 
							  dbo.HC_Gift_SplitGift_v.GSplitPkg, dbo.HC_Package_v.PackDesc, dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort
		 
		GO 
				GO
				SET ANSI_NULLS OFF
				GO
				SET QUOTED_IDENTIFIER OFF
				GO

				-- CHART CampaignAppealPackage
				SET ANSI_NULLS ON
				GO
				SET QUOTED_IDENTIFIER ON
				GO
				CREATE VIEW [CHART].[FundCampAppPack]
				AS
				SELECT	s.RE_DB_OwnerShort, 
						CASE WHEN s.FundID IS NULL or s.FundID='' THEN 'NULL' ELSE s.FundID END AS FundID, s.FundDesc, 
						CASE WHEN s.CampID IS NULL or s.CampID ='' THEN 'NULL' ELSE s.CampID END AS CampID, s.CampDesc, 
						CASE WHEN s.AppealID  IS NULL or s.AppealID ='' THEN 'NULL' ELSE s.AppealID END AS AppealID, s.AppDesc, 
						CASE WHEN s.PackageId  IS NULL or s.PackageId ='' THEN 'NULL' ELSE s.PackageId END AS PackageID, s.PackDesc,
						sum(s.[Count]) as [Count], convert(decimal(18,2), sum(s.totalamt)) as [Sum]

				FROM dbo.HC_FundCampaignAppealPackage_t s
				GROUP BY s.RE_DB_OwnerShort,s.FundId, s.FundDesc, s.CampID, s.AppealID, s.CampDesc, s.AppDesc, s.PackageId, s.PackDesc

				GO
				SET ANSI_NULLS OFF
				GO
				SET QUOTED_IDENTIFIER OFF
				GO
	






-- CHART AppealPackage

	--CONS APPEALS
		select t.RE_DB_OwnerShort, t.CAPAppealId AppealID, dbo.HC_Appeal_v.AppDesc, 
		t.CAPPAckageId PackageID, dbo.HC_Package_v.PackDesc, count(*) as Count, null as totalamt

		INTO dbo.HC_AppealPackage_t-- drop table dbo.HC_CampaignAppealPackage_t
		FROM [dbo].[HC_Cons_Appeal_v] t
		LEFT OUTER JOIN dbo.HC_Package_v ON T.RE_DB_OwnerShort = dbo.HC_Package_v.RE_DB_OwnerShort AND T.CAPAppealId= dbo.HC_Package_v.AppealID AND T.CAPPackageId= dbo.HC_Package_v.PackageID 
		LEFT OUTER JOIN dbo.HC_Appeal_v ON T.RE_DB_OwnerShort = dbo.HC_Appeal_v.RE_DB_OwnerShort AND T.CAPAppealId= dbo.HC_Appeal_v.AppealID
		group by t.RE_DB_OwnerShort, t.CAPAppealId, t.CAPPAckageId, dbo.HC_Appeal_v.AppDesc,  dbo.HC_Package_v.PackDesc

		UNION all

	--GIFTS
		SELECT     dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort, 
							  dbo.HC_Gift_SplitGift_v.GSplitAppeal as AppealID, dbo.HC_Appeal_v.AppDesc, dbo.HC_Gift_SplitGift_v.GSplitPkg as PackageID, 
							  dbo.HC_Package_v.PackDesc, COUNT(*) RecordCount, sum(dbo.HC_Gift_SplitGift_v.GSplitAmt) as totalamt
		FROM         dbo.HC_Gift_SplitGift_v 
		LEFT OUTER JOIN dbo.HC_Package_v ON dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort = dbo.HC_Package_v.RE_DB_OwnerShort AND dbo.HC_Gift_SplitGift_v.GSplitAppeal = dbo.HC_Package_v.AppealID AND dbo.HC_Gift_SplitGift_v.GSplitPkg = dbo.HC_Package_v.PackageID 
		LEFT OUTER JOIN dbo.HC_Appeal_v ON dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort = dbo.HC_Appeal_v.RE_DB_OwnerShort AND dbo.HC_Gift_SplitGift_v.GSplitAppeal = dbo.HC_Appeal_v.AppealID
		GROUP BY dbo.HC_Gift_SplitGift_v.GSplitAppeal, dbo.HC_Appeal_v.AppDesc, 
							  dbo.HC_Gift_SplitGift_v.GSplitPkg, dbo.HC_Package_v.PackDesc, dbo.HC_Gift_SplitGift_v.RE_DB_OwnerShort
		 
		GO 
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO



		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		
		CREATE VIEW [CHART].[AppealPackage]
		AS
		
				SELECT	s.RE_DB_OwnerShort, 
				CASE WHEN s.AppealID  IS NULL or s.AppealID ='' THEN 'NULL' ELSE s.AppealID END AS AppealID, s.AppDesc, 
				CASE WHEN s.PackageId  IS NULL or s.PackageId ='' THEN 'NULL' ELSE s.PackageId END AS PackageID, s.PackDesc,
				sum(s.[Count]) as [Count], convert(decimal(18,2), sum(s.totalamt)) as [Sum]

				FROM dbo.HC_AppealPackage_t s
				GROUP BY s.RE_DB_OwnerShort, s.AppealID, s.AppDesc, s.PackageId, s.PackDesc
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO

 






-- CHART Gift Notepad Type  
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GiftNoteType]
		AS
		select RE_DB_OwnerShort, GFNoteType, count(*) as Count
		from dbo.HC_Gift_Notes_v
		group by RE_DB_OwnerShort, GFNoteType

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO



-- CHART Suffix  
        SET ANSI_NULLS ON;
		GO
        SET QUOTED_IDENTIFIER ON;
		GO
        CREATE VIEW [CHART].[Suffix]
        AS
            SELECT  RE_DB_OwnerShort ,
                    Suff1 ,
                    COUNT(*) AS Count
            FROM    dbo.HC_Constituents_v
            GROUP BY RE_DB_OwnerShort ,
                    Suff1;
		 

		GO
        SET ANSI_NULLS OFF;
		GO
        SET QUOTED_IDENTIFIER OFF;
		GO



-- CHART Title
        SET ANSI_NULLS ON;
		GO
        SET QUOTED_IDENTIFIER ON;
		GO
        CREATE VIEW [CHART].[Title]
        AS
            SELECT  RE_DB_OwnerShort ,
                    Titl1 ,
                    COUNT(*) AS Count
            FROM    dbo.HC_Constituents_v
            GROUP BY RE_DB_OwnerShort ,
                    Titl1;
		 

		GO
        SET ANSI_NULLS OFF;
		GO
        SET QUOTED_IDENTIFIER OFF;
		GO



-- CHART Constituent Attributes  
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[ConsAttribute]
		AS
		select RE_DB_OwnerShort, CAttrCat, 
		CASE WHEN CAttrDesc IS NULL THEN 'NULL' ELSE CAttrDesc END AS CAttrDesc, DataType, count(*) as Count
		from dbo.HC_Cons_Attributes_v
		group by RE_DB_OwnerShort, CAttrCat, CAttrDesc, DataType
		 
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO

-- CHART Constituency Codes  
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[ConsCode]
		AS
		select T1.RE_DB_OwnerShort, T2.KeyInd, 
		CASE WHEN T1.ConsCode IS NULL THEN 'NULL' ELSE T1.ConsCode END AS ConsCode, T1.ConsCodeDesc, count(*) as Count
		from dbo.HC_Cons_ConsCode_v T1
		INNER JOIN dbo.HC_Constituents_v T2 ON T1.ImportID=T2.ImportID
		group by T1.RE_DB_OwnerShort, T1.ConsCode, T1.ConsCodeDesc, T2.KeyInd

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO

-- CHART Solicit Codes  
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[SolicitCode]
		AS
		select RE_DB_OwnerShort, SolicitCode, count(*) as Count
		from dbo.HC_Cons_SolicitCodes_v
		group by RE_DB_OwnerShort, SolicitCode

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO 

-- CHART Gift Attributes   
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GiftAttribute]
		AS
		select RE_DB_OwnerShort, GFAttrCat, 
		CASE WHEN GFAttrDesc IS NULL THEN 'NULL' ELSE GFAttrDesc END AS GFAttrDesc, DataType, count(*) as [Count]
		from dbo.HC_Gift_Attr_v
		group by RE_DB_OwnerShort, GFAttrCat, GFAttrDesc, DataType
		 

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO
		
		
-- CHART Gift Tribute Type   
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GiftTributeType]
		AS
		select RE_DB_OwnerShort, GFTribType, count(*) as Count
		from dbo.HC_Gift_Tribute_v
		group by RE_DB_OwnerShort, GFTribType
		 

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO
		
		
-- CHART Gift Type   
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GiftType]
		AS
		select g.RE_DB_OwnerShort, g.GFType, count(*) as [Count], convert(decimal(18,2), SUM(GFTAmt)) as [Sum]
		from dbo.HC_Gift_v g 
		group by g.RE_DB_OwnerShort, g.GFType


		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO


	
-- CHART [GFTypePayMethod]
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GFTypePayMethod]
		AS
		select g.RE_DB_OwnerShort, g.GFType, g.GFPayMeth, COUNT(*) as [Count] 
		from dbo.HC_Gift_v g 
		group by g.RE_DB_OwnerShort, g.GFType, g.GFPayMeth


		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO


-- CHART [GFPayMethod]
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GFPayMethod]
		AS
		select g.RE_DB_OwnerShort, g.GFPayMeth, COUNT(*) as [Count] 
		from dbo.HC_Gift_v g 
		group by g.RE_DB_OwnerShort, g.GFPayMeth


		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO



-- CHART [GiftTypeSubTypeCode]
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GiftTypeSubTypeCode]
		AS
		select g.RE_DB_OwnerShort,  g.GFType, 
		CASE WHEN g.GFSubType IS NULL THEN 'NULL' ELSE g.GFSubType END AS GFSubType, 
		CASE WHEN g.GFGiftCode IS NULL THEN 'NULL' ELSE g.GFGiftCode END AS GFGiftCode, 
		COUNT(*) as [Count], convert(decimal(18,2), SUM(GFTAmt)) as [Sum]
		from dbo.HC_Gift_v g 
		group by g.RE_DB_OwnerShort, g.GFType, g.GFSubType, g.GFGiftCode


		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO


-- CHART Gift SubType   
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GiftSubType]
		AS
		select RE_DB_OwnerShort, GFSubType, count(*) as Count
		from dbo.HC_Gift_v
		group by RE_DB_OwnerShort, GFSubType
		 
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO
		
		
-- CHART Gift Code    
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GiftCode]
		AS
		select RE_DB_OwnerShort, GFGiftCode, count(*) as Count
		from dbo.HC_Gift_v
		group by RE_DB_OwnerShort, GFGiftCode
		 

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO
 
 
 
 
 
-- CHART Event Type    
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[EventType]
		AS
		select RE_DB_OwnerShort, CASE WHEN EVType IS NULL THEN 'NULL' ELSE EVType END AS EVType, Count(*) Count
		from dbo.HC_Event_v
		group by RE_DB_OwnerShort, EVType
		  

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO


-- CHART Participation Type    
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[ParticipantType]
		AS
		select RE_DB_OwnerShort, CASE when REGParticipation IS NULL THEN 'NULL' ELSE REGParticipation END AS REGParticipation, Count(*) Count
		from dbo.HC_Event_Participant_v
		group by RE_DB_OwnerShort, REGParticipation
		 

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO


-- CHART Relationship Types  (1 column)

		SELECT  dbo.HC_OrgInit_f() [RE_DB_OwnerShort], T_19.LONGDESCRIPTION AS Relationships, Count(T_19.LONGDESCRIPTION) As CountOf
		INTO HC_tbl_RelationshipTypes_t        --drop table HC_tbl_RelationshipTypes_t                
		FROM dbo.CONSTIT_RELATIONSHIPS AS CONSTIT_RELATIONSHIPS 
			 INNER JOIN  dbo.RECORDS AS T_1 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_1.ID
			 LEFT JOIN  dbo.QUERY_IND_RELATIONSHIPS AS T_2 ON CONSTIT_RELATIONSHIPS.ID = T_2.ID
		     
			 LEFT JOIN dbo.TABLEENTRIES AS T_18 ON T_2.RECIP_RELATION_CODE = T_18.TABLEENTRIESID 
			 LEFT JOIN dbo.TABLEENTRIES AS T_19 ON T_2.RELATION_CODE = T_19.TABLEENTRIESID

		GROUP BY  T_19.LONGDESCRIPTION

		UNION

		SELECT dbo.HC_OrgInit_f() [RE_DB_OwnerShort], T_18.LONGDESCRIPTION AS Relationships, Count(T_18.LONGDESCRIPTION) As CountOf
		                       
		FROM dbo.CONSTIT_RELATIONSHIPS AS CONSTIT_RELATIONSHIPS 
			 INNER JOIN  dbo.RECORDS AS T_1 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_1.ID
			 LEFT JOIN  dbo.QUERY_IND_RELATIONSHIPS AS T_2 ON CONSTIT_RELATIONSHIPS.ID = T_2.ID
		     
			 LEFT JOIN dbo.TABLEENTRIES AS T_18 ON T_2.RECIP_RELATION_CODE = T_18.TABLEENTRIESID 
			 LEFT JOIN dbo.TABLEENTRIES AS T_19 ON T_2.RELATION_CODE = T_19.TABLEENTRIESID

		GROUP BY T_18.LONGDESCRIPTION


		UNION 

		SELECT dbo.HC_OrgInit_f() [RE_DB_OwnerShort], T_19.LONGDESCRIPTION AS Relationships, Count(T_19.LONGDESCRIPTION) As CountOf
		                       
		FROM dbo.CONSTIT_RELATIONSHIPS AS CONSTIT_RELATIONSHIPS 
			 INNER JOIN  dbo.RECORDS AS T_1 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_1.ID
			 LEFT JOIN  dbo.QUERY_ORG_RELATIONSHIPS AS T_2 ON CONSTIT_RELATIONSHIPS.ID = T_2.ID
			 
			 LEFT JOIN dbo.TABLEENTRIES AS T_18 ON T_2.RECIP_RELATION_CODE = T_18.TABLEENTRIESID 
			 LEFT JOIN dbo.TABLEENTRIES AS T_19 ON T_2.RELATION_CODE = T_19.TABLEENTRIESID

		GROUP BY T_19.LONGDESCRIPTION

		UNION 

		SELECT dbo.HC_OrgInit_f() [RE_DB_OwnerShort], T_18.LONGDESCRIPTION AS Relationships, Count(T_18.LONGDESCRIPTION) As CountOf
		                       
		FROM dbo.CONSTIT_RELATIONSHIPS AS CONSTIT_RELATIONSHIPS 
			 INNER JOIN  dbo.RECORDS AS T_1 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_1.ID
			 LEFT JOIN  dbo.QUERY_ORG_RELATIONSHIPS AS T_2 ON CONSTIT_RELATIONSHIPS.ID = T_2.ID
			 
			 LEFT JOIN dbo.TABLEENTRIES AS T_18 ON T_2.RECIP_RELATION_CODE = T_18.TABLEENTRIESID 
			 LEFT JOIN dbo.TABLEENTRIES AS T_19 ON T_2.RELATION_CODE = T_19.TABLEENTRIESID

		GROUP BY T_18.LONGDESCRIPTION 


		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO

		-- SELECT * from hc_tbl_relationshiptypes order by relationships

		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[RelationshipType]
		AS
		SELECT RE_DB_OwnerShort, Relationships, Sum(CountOf) as Count
		FROM HC_tbl_RelationshipTypes_t
		GROUP BY RE_DB_OwnerShort, Relationships 
		 
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO

		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
					
			-- CHART_RELAT_RECIP (RELATIONSHIP AND RECIPROCAL VALUES DIVIDED BY IND AND ORG RELAT)
				CREATE VIEW [CHART].[RelatRecipType]	
				AS
					SELECT	T.RE_DB_OwnerShort, 
							T2.KeyInd,
							'Individual' RelationshipIndicator, 
							CASE WHEN T.IRRelat IS NULL OR T.IRRelat ='' THEN 'NULL' ELSE T.IRRelat END AS Relationship,  
							CASE WHEN T.IRRecip IS NULL OR T.IRRecip ='' THEN 'NULL' ELSE T.IRRecip END AS Reciprocal, 
							COUNT(*) As CountOf
					FROM dbo.HC_Ind_Relat_v AS T 
					INNER JOIN dbo.HC_Constituents_v T2 ON T.ImportID=T2.ImportID
					GROUP BY T2.KeyInd, T.RE_DB_OwnerShort , T.IRRelat,  T.IRRecip 
					
					UNION
					
					SELECT	T.RE_DB_OwnerShort, 
							T2.KeyInd,
							'Organization' RelationshipIndicator, 
							CASE WHEN T.ORRelat IS NULL OR T.ORRelat ='' THEN 'NULL' ELSE T.ORRelat END AS Relationship,  
							CASE WHEN T.ORRecip IS NULL OR T.ORRecip ='' THEN 'NULL' ELSE T.ORRecip END AS Reciprocal, 
							COUNT(*) As CountOf
					FROM dbo.HC_Org_Relat_v AS T 
					INNER JOIN dbo.HC_Constituents_v T2 ON T.ImportID=T2.ImportID
					GROUP BY T2.KeyInd, T.RE_DB_OwnerShort , T.ORRelat, T.ORRecip

			GO
			SET ANSI_NULLS OFF
			GO
			SET QUOTED_IDENTIFIER OFF
			GO





-- CHART ActionTypeCategory

		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[ActionCategoryType]
		AS
		SELECT RE_DB_OwnerShort, ACCat, 
		CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, Count(*) as Count
		FROM dbo.HC_Cons_Action_v
		GROUP BY RE_DB_OwnerShort, ACCat, ACType
		 

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO

-- CHART ActionType

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[ActionType]
		AS
		SELECT RE_DB_OwnerShort, CASE WHEN ACType IS NULL THEN 'NULL' ELSE ACType END AS ACType, Count(*) as Count
		FROM dbo.HC_Cons_Action_v
		GROUP BY RE_DB_OwnerShort,  ACType
		 


		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO


-- CHART ActionAttribute

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[ActionAttribute]
		AS
		SELECT RE_DB_OwnerShort, ACAttrCat, 
		CASE WHEN ACAttrDesc IS NULL THEN 'NULL' ELSE ACAttrDesc END AS ACAttrDesc, DataType, Count(*) as Count
		FROM dbo.HC_Cons_Action_Attr_v
		GROUP BY RE_DB_OwnerShort, ACAttrCat, ACAttrDesc, DataType
		 


		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO





---Chart Action Status 
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[ActionStatus]
		AS
		SELECT RE_DB_OwnerShort, 
		CASE WHEN ACStatus IS NULL THEN 'NULL' ELSE ACStatus END AS ACStatus, Count(*) as Count
		FROM dbo.HC_Cons_Action_v
		GROUP BY RE_DB_OwnerShort, ACStatus
		 
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO


---CHART_ConsProspectClass

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO

		CREATE VIEW [CHART].[ConsProspectClass]
		AS
		SELECT RE_DB_OwnerShort, ProspectClass, Count(*) as Count
		FROM dbo.HC_Constituents_v 
		GROUP BY RE_DB_OwnerShort, ProspectClass

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO


	


-- CHART_ORContactType
		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		
		CREATE VIEW [CHART].[ORContactType]
		AS
			SELECT		RE_DB_OwnerShort, CASE WHEN [ORContactType] is null THEN 'NULL' ELSE [ORContactType]  END AS [ORContactType],
						CAST(count (*) AS VARCHAR(10)) as Count
			FROM		dbo.HC_Org_Relat_v
			GROUP BY	RE_DB_OwnerShort, [ORContactType] 

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO


-- CHART_IRContactType

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO

		CREATE VIEW [CHART].[IRContactType]
		AS
			SELECT		RE_DB_OwnerShort, CASE WHEN [IRContactType] is null THEN 'NULL' ELSE [IRContactType]  END AS [IRContactType],
						CAST(count (*) AS VARCHAR(10)) as Count
			FROM		dbo.HC_Ind_Relat_v
			GROUP BY	RE_DB_OwnerShort, [IRContactType] 
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	
			
	

--CHART_IND_RELAT_ATTR

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
			
		CREATE VIEW [CHART].[IndRelatAttr]
		AS
			SELECT RE_DB_OwnerShort, IRAttrCat, 
			CASE WHEN IRAttrDesc IS NULL THEN 'NULL' ELSE IRAttrDesc END AS IRAttrDesc
			, DataType, COUNT(*) [Count]
			FROM dbo.HC_Ind_Relat_Attr_v
			GROUP BY RE_DB_OwnerShort, IRAttrCat, IRAttrDesc, DataType
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	



--CHART_ORG_RELAT_ATTR

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
			
		CREATE VIEW [CHART].[OrgRelatAttr]
		AS
			SELECT RE_DB_OwnerShort, ORAttrCat, 
			CASE WHEN ORAttrDesc IS NULL THEN 'NULL' ELSE ORAttrDesc END AS ORAttrDesc, DataType, COUNT(*) [Count]
			FROM dbo.HC_Org_Relat_Attr_v
			GROUP BY RE_DB_OwnerShort, ORAttrCat, ORAttrDesc, DataType  
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	





--[CHART].[Attributes]
			SET ANSI_NULLS ON 
			GO
			SET QUOTED_IDENTIFIER ON
			GO
				
			CREATE VIEW [CHART].[Attributes]
			AS

			SELECT		--dbo.AttributeTypes.RECORDTYPE [TypeID], dbo.AttributeTypes.TYPEOFDATA [DataTypeID],
						dbo.HC_OrgInit_f() [RE_DB_OwnerShort],  
						case dbo.AttributeTypes.RECORDTYPE	
						when 1 then 'CONSTITUENT' when 2 then 'GIFT' when 3 then 'ACTION' when 4 then 'EDUCATION' when 5 then 'EVENT' when 6 then 'PARTICIPANT' 
						when 7 then 'CAMPAIGN' when 8 then 'APPEAL' when 10 then 'FUND' when 11 then 'PACKAGE' when 12 then 'ADDRESS'   when 13 then 'JOB'
						when 14 then 'MEMBERSHIP' when 15 then 'INDIVIDUAL_RELATIONSHIP' when 16 then 'ORGANIZATION_RELATIONSHIP' 
						when 17 then 'ACCOUNT' when 18 then 'PROPOSAL' end as [Type],
					
						case dbo.AttributeTypes.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
														   when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
														   when 8 then 'FuzzyDate' end as [DataType], 						 
					
						dbo.AttributeTypes.[DESCRIPTION] AS [Category], 
						dbo.CODETABLES.NAME as [TableName],
						case dbo.AttributeTypes.[REQUIRED] when 0 then 'FALSE' when -1 then 'TRUE' end as [Required], 
						case dbo.AttributeTypes.MUSTBEUNIQUE when 0 then 'FALSE' when -1 then 'TRUE' end as [Unique], 
						case dbo.AttributeTypes.ACTIVE when 0 then 'FALSE' when -1 then 'TRUE' end as [Active], COUNT(*) [Count]     
						  
			FROM         dbo.AttributeTypes LEFT JOIN 
								  dbo.CODETABLES ON dbo.AttributeTypes.CODETABLESID = dbo.CODETABLES.CODETABLESID LEFT JOIN
								  dbo.ConstituentAttributes ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.ConstituentAttributes.ATTRIBUTETYPESID LEFT JOIN
								  dbo.ConstitAddressAttributes ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.ConstitAddressAttributes.ATTRIBUTETYPESID LEFT JOIN
								  dbo.GiftAttributes ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.GiftAttributes.ATTRIBUTETYPESID LEFT JOIN
								  dbo.ActionAttributes ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.ActionAttributes.ATTRIBUTETYPESID LEFT JOIN
								  dbo.EducationAttributes ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.EducationAttributes.ATTRIBUTETYPESID LEFT JOIN
								  dbo.CampaignAttributes ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.CampaignAttributes.ATTRIBUTETYPESID LEFT JOIN
								  dbo.FundAttributes ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.FundAttributes.ATTRIBUTETYPESID LEFT JOIN
								  dbo.AppealAttributes ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.AppealAttributes.ATTRIBUTETYPESID LEFT JOIN
								  dbo.PackageAttributes ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.PackageAttributes.ATTRIBUTETYPESID LEFT JOIN
								  dbo.ParticipantAttributes ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.ParticipantAttributes.ATTRIBUTETYPESID LEFT JOIN
								  dbo.PROPOSALATTRIBUTES ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.PROPOSALATTRIBUTES.ATTRIBUTETYPESID LEFT JOIN
								  dbo.MemberAttributes ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.MemberAttributes.ATTRIBUTETYPESID LEFT JOIN
								  dbo.JOBATTRIBUTES ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.JOBATTRIBUTES.ATTRIBUTETYPESID LEFT JOIN
								  dbo.EventAttributes ON dbo.AttributeTypes.ATTRIBUTETYPESID = dbo.EventAttributes.ATTRIBUTETYPESID 

			 WHERE dbo.AttributeTypes.SEQUENCE is not null
			/*               
			 WHERE  (dbo.ConstitAddressAttributes.ATTRIBUTETYPESID IS NOT NULL) OR (dbo.ConstituentAttributes.ATTRIBUTETYPESID is not null) OR  
					(dbo.GiftAttributes.ATTRIBUTETYPESID IS NOT NULL) OR (dbo.ActionAttributes.ATTRIBUTETYPESID is not null) OR
					(dbo.EducationAttributes.ATTRIBUTETYPESID IS NOT NULL) OR (dbo.CampaignAttributes.ATTRIBUTETYPESID is not null) OR
					(dbo.FundAttributes.ATTRIBUTETYPESID IS NOT NULL) OR (dbo.AppealAttributes.ATTRIBUTETYPESID is not null) OR
					(dbo.PackageAttributes.ATTRIBUTETYPESID IS NOT NULL) OR (dbo.ParticipantAttributes.ATTRIBUTETYPESID is not null) OR  
					(dbo.PROPOSALATTRIBUTES.ATTRIBUTETYPESID IS NOT NULL) OR (dbo.MemberAttributes.ATTRIBUTETYPESID is not null) OR 
					(dbo.JOBATTRIBUTES.ATTRIBUTETYPESID IS NOT NULL) OR (dbo.EventAttributes.ATTRIBUTETYPESID is not null)  
			*/		                 
			 GROUP BY	dbo.AttributeTypes.RECORDTYPE, dbo.AttributeTypes.DESCRIPTION, dbo.AttributeTypes.TYPEOFDATA, 
						dbo.AttributeTypes.REQUIRED, dbo.AttributeTypes.ACTIVE, dbo.AttributeTypes.MUSTBEUNIQUE, dbo.CODETABLES.NAME
						
			GO
			SET ANSI_NULLS OFF
			GO
			SET QUOTED_IDENTIFIER OFF
			GO	


--CHART_SECURITY USERS 

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
			
		CREATE VIEW [CHART].[Users]
		AS
			SELECT RE_DB_OwnerShort, UserName, UserDescription, UserHasSupervisorRights, UserGroupName, UserGroupDescription, RE_DB_Owner
			FROM dbo.HC_Users_v   
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	
		         


 
--CHART_ASSIGNED_SOLICITOR_TYPE

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
			
		CREATE VIEW [CHART].[ASRType]
		AS
			SELECT RE_DB_OwnerShort, ASRType, COUNT(*) [Count]
			FROM dbo.HC_Cons_Solicitor_v   
			GROUP BY RE_DB_OwnerShort, ASRType
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	
		 
 
 
 
-- [CHART].[GFLtrCode]
		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
			
		CREATE VIEW [CHART].[GFLtrCode]
		AS
			SELECT RE_DB_OwnerShort, GFLtrCode, COUNT(*) [Count]
			FROM dbo.HC_Gift_v   
			GROUP BY RE_DB_OwnerShort, GFLtrCode
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	
		 
 
			


 
  

-- CHART Gift Cons Code
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[GiftConsCode]
		AS
		select g.RE_DB_OwnerShort, c.KeyInd, 
			CASE WHEN g.GFCons IS NULL THEN 'NULL' ELSE g.GFCons END AS GFCons, g.GFConsDesc, count(*) as [Count]
			from dbo.HC_Gift_v g 
			INNER JOIN dbo.HC_Constituents_v c ON g.ImportID=c.ImportID
			group by g.GFCons, c.KeyInd, g.GFConsDesc, g.RE_DB_OwnerShort

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO

				  
--CHART CONSTITUENCY CODES (ALL from CONS AND GIFTS)				  
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO
		CREATE VIEW [CHART].[ConstituencyCodes]  
		AS

		select RE_DB_OwnerShort, KeyInd, ConsCode, ConsCodeDesc, sum([Count]) RecordCount 
		from 
		(select c.RE_DB_OwnerShort, c.KeyInd, CASE WHEN c.ConsCode IS NULL THEN 'NULL' ELSE c.ConsCode END AS ConsCode, c.ConsCodeDesc, c.[count] from [CHART].[ConsCode] c
		 union
		 select g.RE_DB_OwnerShort, g.KeyInd, CASE WHEN g.GFCons IS NULL THEN 'NULL' ELSE g.GFCons END AS ConsCode, g.GFConsDesc as ConsCodeDesc, g.[count] from [CHART].[GiftConsCode] g
		) as T group by RE_DB_OwnerShort, KeyInd, ConsCode, ConsCodeDesc  

 

		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO              



 --CHART_PROPOSAL_ATTRIBUTE

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
			
		CREATE VIEW [CHART].[ProposalAttribute]
		AS
			SELECT RE_DB_OwnerShort, PRAttrCat, 
			CASE WHEN PRAttrDesc IS NULL THEN 'NULL' ELSE PRAttrDesc END AS PRAttrDesc,  COUNT(*) [Count]
			FROM dbo.HC_Proposal_Attr_v  
			GROUP BY RE_DB_OwnerShort, PRAttrCat, PRAttrDesc
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	
		 

--CHART_PROPOSAL_STATUS


		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
			
		CREATE VIEW [CHART].[ProposalStatus]
		AS
			SELECT RE_DB_OwnerShort,
			CASE WHEN PRStatus IS NULL THEN 'NULL' ELSE PRStatus END AS PRStatus,  COUNT(*) [Count]
			FROM dbo.HC_Proposal_v  
			GROUP BY RE_DB_OwnerShort, PRStatus
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	
		 


 --CHART_PROSPECT_RATING

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
			
		CREATE VIEW [CHART].[ProspectRating]
		AS
			SELECT RE_DB_OwnerShort, PRateCat , PRateDesc , PRateSource , COUNT(*) [Count]
			FROM dbo.HC_Prospect_Rating_v  
			GROUP BY RE_DB_OwnerShort, PRateCat , PRateDesc , PRateSource
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	
 
 
 
 
 
--CHART_MEDIA_TYPE

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
			
		CREATE VIEW [CHART].[MediaType]
		AS
		 
		 select RE_DB_OwnerShort, MediaType, COUNT(*) as [Count]
		 from dbo.HC_Media_v 
		 group by MediaType, RE_DB_OwnerShort
		 
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	
		 
		 
 

--CHART_PARTICIPANTS_Attr

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
			
		CREATE VIEW [CHART].[ParticipantAttr]
		AS
		 
		 select RE_DB_OwnerShort, REGAttrCat, 
		 CASE WHEN REGAttrDesc IS NULL THEN 'NULL' ELSE REGAttrDesc END AS REGAttrDesc, DataType, COUNT(*) as [Count]
		 from dbo.HC_Event_Participant_Attr_v 
		 group by RE_DB_OwnerShort, REGAttrCat, REGAttrDesc, DataType
		 
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	
		  
  
 
--CHART_EVENT_Attr

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
			
		CREATE VIEW [CHART].[EventAttr]
		AS
		 
		 select RE_DB_OwnerShort, EVAttrCat, 
		 CASE WHEN EVAttrDesc IS NULL THEN 'NULL' ELSE EVAttrDesc END AS EVAttrDesc, DataType, COUNT(*) as [Count]
		 from dbo.HC_Event_Attr_v 
		 group by RE_DB_OwnerShort,  EVAttrCat, EVAttrDesc, DataType
		 
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	
		 
 
 
 
 
--CHART_CONSALIAS 

		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
			
		CREATE VIEW [CHART].[ConsAlias]
		AS
		 
		 select RE_DB_OwnerShort, AliasType, COUNT(*) as [Count]
		 from dbo.HC_Cons_Alias_v 
		 group by RE_DB_OwnerShort,  AliasType
		 
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	
		 
		 
 

--CHART_ACTIONSOLICITOR


		SET ANSI_NULLS ON 
		GO
		SET QUOTED_IDENTIFIER ON
		GO
			
		CREATE VIEW [CHART].[ActionSolicitor]
		AS
		 
		 select C.RE_DB_OwnerShort, A.ACSolImpID, C.ConsID, C.Name, C.IsSolicitor, COUNT(*) as [Count]
		 from dbo.HC_Cons_Action_Solicitor_v A 
		 inner join dbo.HC_Constituents_v C on A.ACSolImpID = C.ImportID AND a.RE_DB_OwnerShort = c.RE_DB_OwnerShort
		 group by C.RE_DB_OwnerShort, A.ACSolImpID, C.ConsID, C.Name, C.IsSolicitor
		 
		GO
		SET ANSI_NULLS OFF
		GO
		SET QUOTED_IDENTIFIER OFF
		GO	
		 
  