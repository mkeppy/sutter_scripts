

 
--CONSTITUENT SOLICIT CODES @ dbo.[HC_Cons_SolicitCodes_v]
	--Compile multiple databases into 1 in SS2008R2. 
	--RE7_ABSF;RE7_DMHF;RE7_EMCF;RE7_MHLB;RE7_MPHF;RE7_PAMF;RE7_SAFH;RE7_SAHF;RE7_SCAH;RE7_SDHF;RE7_SMCF;RE7_SMFn;RE7_SRMC;RE7_SSCF
	--Note: CPMC not included as it's the target. 


--RE7_SH_SMCF
  SELECT * INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_SolicitCodes_v]

--RE7_SH_SAFH
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_SolicitCodes_v]


--RE7_SH_ABSF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_ABSF.dbo.[HC_Cons_SolicitCodes_v]

--RE7_SH_WBRX
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_WBRX.dbo.[HC_Cons_SolicitCodes_v]
--RE7_SH_HOTV
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_HOTV.dbo.[HC_Cons_SolicitCodes_v]

--RE7_SH_DMHF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_DMHF.dbo.[HC_Cons_SolicitCodes_v]

--RE7_SH_EMCF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_EMCF.dbo.[HC_Cons_SolicitCodes_v]

--RE7_SH_CVRX
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_CVRX.dbo.[HC_Cons_SolicitCodes_v]

--RE7_SH_MPHF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_MPHF.dbo.[HC_Cons_SolicitCodes_v]

--RE7_SH_PAMF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_PAMF.dbo.[HC_Cons_SolicitCodes_v]


--RE7_SH_SAHF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAHF.dbo.[HC_Cons_SolicitCodes_v]

--RE7_SH_SCAH
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SCAH.dbo.[HC_Cons_SolicitCodes_v]

--RE7_SH_SDHF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SDHF.dbo.[HC_Cons_SolicitCodes_v]


--RE7_SH_SMFX
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SMFX.dbo.[HC_Cons_SolicitCodes_v]

--RE7_SH_SRMC
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SRMC.dbo.[HC_Cons_SolicitCodes_v]

--RE7_SH_SSCF
	INSERT INTO SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SSCF.dbo.[HC_Cons_SolicitCodes_v]

 
--SELECT COUNT(*) FROM SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] 
/*
 
SELECT  * FROM SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] 
SELECT RE_DB_OwnerShort, COUNT(*) FROM SUTTER_1P_DATA.dbo.[HC_Cons_SolicitCodes_v] group by RE_DB_OwnerShort

*/

  