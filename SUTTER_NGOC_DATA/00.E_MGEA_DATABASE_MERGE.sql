USE SUTTER_MGEA_DATA
GO
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Appeal_v] FROM        [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Appeal_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Appeal_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Appeal_v]
		
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Appeal_Attr_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Appeal_Attr_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Appeal_Attr_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Appeal_Attr_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.HC_Appeal_Benefits_v FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.HC_Appeal_Benefits_v
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.HC_Appeal_Benefits_v SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.HC_Appeal_Benefits_v

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Appeal_Expenses_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Appeal_Expenses_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Appeal_Expenses_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Appeal_Expenses_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Bank_Financial_Institution_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Bank_Financial_Institution_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Bank_Financial_Institution_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Bank_Financial_Institution_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Campaign_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Campaign_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Campaign_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Campaign_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_CampaignAppeal_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_CampaignAppeal_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_CampaignAppeal_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_CampaignAppeal_v]


	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_CampaignSolicitor_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_CampaignSolicitor_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_CampaignSolicitor_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_CampaignSolicitor_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_CampaignFund_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_CampaignFund_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_CampaignFund_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_CampaignFund_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Campaign_Attr_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Campaign_Attr_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Campaign_Attr_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Campaign_Attr_v]
	--RE7_SH_SMCF
			 SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Action_Solicitor_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Action_Solicitor_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Action_Solicitor_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Action_Solicitor_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Action_Notes_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Action_Notes_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Action_Notes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Action_Notes_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Action_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Action_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Action_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Action_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Action_Attr_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Action_Attr_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Action_Attr_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Action_Attr_v]




	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Tribute_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Tribute_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Tribute_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Tribute_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Solicitor_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Solicitor_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Solicitor_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Solicitor_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_SolicitCodes_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_SolicitCodes_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_SolicitCodes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_SolicitCodes_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Phone_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Phone_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Phone_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Phone_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Notes_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Notes_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Notes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Notes_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_ConsCode_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_ConsCode_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_ConsCode_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_ConsCode_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Bank_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Bank_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Bank_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Bank_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Attributes_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Attributes_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Attributes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Attributes_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Appeal_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Appeal_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Appeal_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Appeal_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Annotation_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Annotation_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Annotation_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Annotation_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Alias_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Alias_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Alias_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Alias_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Address_Attr_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Address_Attr_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Address_Attr_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Address_Attr_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Addl_Addressee_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Addl_Addressee_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Addl_Addressee_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Addl_Addressee_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Constituents_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Constituents_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Constituents_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Constituents_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Address_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Cons_Address_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Cons_Address_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Cons_Address_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Educ_Relat_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Educ_Relat_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Educ_Relat_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Educ_Relat_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Educ_Relat_Major_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Educ_Relat_Major_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Educ_Relat_Major_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Educ_Relat_Major_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Educ_Relat_Minor_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Educ_Relat_Minor_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Educ_Relat_Minor_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Educ_Relat_Minor_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Educ_Relat_Attribute_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Educ_Relat_Attribute_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Educ_Relat_Attribute_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Educ_Relat_Attribute_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Registration_Fees_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Event_Registration_Fees_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Registration_Fees_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Event_Registration_Fees_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Price_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Event_Price_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Price_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Event_Price_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Participant_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Event_Participant_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Participant_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Event_Participant_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Participant_Attr_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Event_Participant_Attr_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Participant_Attr_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Event_Participant_Attr_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Notepad_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Event_Notepad_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Notepad_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Event_Notepad_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Menu_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Event_Menu_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Menu_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Event_Menu_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Expense_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Event_Expense_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Expense_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Event_Expense_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Award_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Event_Award_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Award_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Event_Award_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Event_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Event_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Event_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Event_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Attr_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Event_Attr_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Event_Attr_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Event_Attr_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Fund_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Fund_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Fund_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Fund_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Fund_Relationship_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Fund_Relationship_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Fund_Relationship_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Fund_Relationship_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Fund_Attr_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Fund_Attr_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Fund_Attr_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Fund_Attr_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Fund_GLDistribution_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Fund_GLDistribution_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Fund_GLDistribution_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Fund_GLDistribution_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_WriteOff_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_WriteOff_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_WriteOff_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_WriteOff_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Tribute_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_Tribute_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Tribute_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_Tribute_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_SplitGift_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_SplitGift_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_SplitGift_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_SplitGift_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Solicitor_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_Solicitor_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Solicitor_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_Solicitor_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_SoftCredit_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_SoftCredit_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_SoftCredit_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_SoftCredit_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_ProposalLink_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_ProposalLink_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_ProposalLink_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_ProposalLink_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Notes_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_Notes_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Notes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_Notes_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_MatchingLink_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_MatchingLink_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_MatchingLink_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_MatchingLink_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Link_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_Link_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Link_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_Link_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Installments_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_Installments_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Installments_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_Installments_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Benefits_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_Benefits_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Benefits_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_Benefits_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Attr_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_Attr_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Attr_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_Attr_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Amendments_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_Amendments_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Amendments_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_Amendments_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Adjustments_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Gift_Adjustments_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Gift_Adjustments_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Gift_Adjustments_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Org_Relat_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Org_Relat_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Org_Relat_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Org_Relat_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Org_Relat_Phone_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Org_Relat_Phone_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Org_Relat_Phone_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Org_Relat_Phone_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Org_Relat_Attr_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Org_Relat_Attr_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Org_Relat_Attr_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Org_Relat_Attr_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Media_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Media_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Media_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Media_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Ind_Relat_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Ind_Relat_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Ind_Relat_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Ind_Relat_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Ind_Relat_Phone_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Ind_Relat_Phone_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Ind_Relat_Phone_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Ind_Relat_Phone_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Org_Relat_NonConsIR_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Org_Relat_NonConsIR_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Org_Relat_NonConsIR_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Org_Relat_NonConsIR_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Ind_Relat_Attr_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Ind_Relat_Attr_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Ind_Relat_Attr_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Ind_Relat_Attr_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Planned_Gift_Relationship_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Planned_Gift_Relationship_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Planned_Gift_Relationship_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Planned_Gift_Relationship_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Planned_Gift_Beneficiaries_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Planned_Gift_Beneficiaries_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Planned_Gift_Beneficiaries_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Planned_Gift_Asset_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Planned_Gift_Asset_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Planned_Gift_Asset_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Planned_Gift_Asset_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Package_Attr_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Package_Attr_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Package_Attr_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Package_Attr_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Package_Benefits_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Package_Benefits_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Package_Benefits_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Package_Benefits_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Package_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Package_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Package_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Package_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Proposal_Solicitor_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Proposal_Solicitor_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Proposal_Solicitor_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Proposal_Solicitor_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Proposal_Notes_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Proposal_Notes_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Proposal_Notes_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Proposal_Notes_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Proposal_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Proposal_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Proposal_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Proposal_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Proposal_Attr_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Proposal_Attr_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Proposal_Attr_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Proposal_Attr_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Prospect_Rating_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Prospect_Rating_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Prospect_Rating_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Prospect_Rating_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Prospect_Philanthropic_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Prospect_Philanthropic_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Prospect_Philanthropic_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Prospect_Philanthropic_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Prospect_Other_Gifts_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Prospect_Other_Gifts_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Prospect_Other_Gifts_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Prospect_Other_Gifts_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Prospect_WillNotGive_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Prospect_WillNotGive_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Prospect_WillNotGive_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Prospect_WillNotGive_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Prospect_Financial_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Prospect_Financial_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Prospect_Financial_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Prospect_Financial_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Membership_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Membership_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Membership_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Membership_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Membership_Linked_Gift_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Membership_Linked_Gift_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Membership_Linked_Gift_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Membership_Linked_Gift_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Membership_History_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Membership_History_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Membership_History_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Membership_History_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Membeship_Solicitor_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Membeship_Solicitor_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Membeship_Solicitor_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Membeship_Solicitor_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Membership_Benefits_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Membership_Benefits_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Membership_Benefits_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Membership_Benefits_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Volunteer_Type_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Volunteer_Type_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Volunteer_Type_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Volunteer_Type_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Volunteer_Availability_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Volunteer_Availability_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Volunteer_Availability_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Volunteer_Availability_v]
	
		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Volunteer_Assignment_Interest_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Volunteer_Assignment_Interest_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Volunteer_Assignment_Interest_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Volunteer_Assignment_Interest_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Volunteer_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Volunteer_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Volunteer_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Volunteer_v]

		--RE7_SH_SMCF
		  SELECT * INTO SUTTER_MGEA_DATA.dbo.[HC_Users_v] FROM          [DMSQL\SQL2008R2].RE7_SH_SMCF.dbo.[HC_Users_v]
		--RE7_SH_SAFH
			INSERT INTO SUTTER_MGEA_DATA.dbo.[HC_Users_v] SELECT * FROM [DMSQL\SQL2008R2].RE7_SH_SAFH.dbo.[HC_Users_v]

