
USE RE7_SH_SMFX
GO

/**************** CHANGE variable @OrgInit = "SMPL" with the client's initials (E.g. Heller Consulting Inc a.k.a. HC)  *********/
--FUNCTION returns the speocified client initials. 

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_OrgInit_f]') and objectproperty(id, N'IsScalarFunction') = 1)
	drop function [dbo].[HC_OrgInit_f]
	 
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  FUNCTION [dbo].[HC_OrgInit_f]()  
  
  RETURNS varchar(4)  
  BEGIN 
	declare @OrgInit as varchar(4)     
	set @OrgInit = 'SMFX'   
	return @OrgInit
  END

GO




if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_OrgName_v]') and objectproperty(id, N'IsScalarFunction') = 1)
	drop function [dbo].[HC_OrgName_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_FormatAmount_f]') and objectproperty(id, N'IsScalarFunction') = 1)
	drop function [dbo].[HC_FormatAmount_f]

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Appeal_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Appeal_v]  
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Campaign_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Campaign_v]
	 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Action_Attr_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Action_Attr_v] 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Action_Notes_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Action_Notes_v] 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Action_Solicitor_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Action_Solicitor_v] 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Action_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Action_v] 

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Addl_Addressee_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Addl_Addressee_v] 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Address_Attr_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Address_Attr_v] 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Alias_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Alias_v] 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Address_NonPref_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Address_NonPref_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Appeal_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Appeal_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Attributes_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Attributes_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_ConsCode_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_ConsCode_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Constituents_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Constituents_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Notes_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Notes_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Phone_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Phone_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_SolicitCodes_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_SolicitCodes_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Solicitor_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Solicitor_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Tribute_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Tribute_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Country_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Country_v]

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Educ_Relat_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Educ_Relat_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Educ_Relat_Attribute_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Educ_Relat_Attribute_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Educ_Relat_Major_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Educ_Relat_Major_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Educ_Relat_Minor_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Educ_Relat_Minor_v]
	
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Attr_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Event_Attr_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Event_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Award_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Event_Award_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Expense_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Event_Expense_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Menu_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Event_Menu_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Notepad_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Event_Notepad_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Price_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Event_Price_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Participant_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Event_Participant_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Registration_Fees_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Event_Registration_Fees_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Event_Participant_Attr_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Event_Participant_Attr_v]
		
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Fund_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Fund_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Attr_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_Attr_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Installments_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_Installments_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Main]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_Main]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_PledgePay]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_PledgePay]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Link_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_Link_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Notes_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_Notes_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_SoftCredit_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_SoftCredit_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Solicitor_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_Solicitor_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_SplitGift_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_SplitGift_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Tribute_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_Tribute_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_MatchingLink_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_MatchingLink_v]	
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_WriteOff_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_WriteOff_v]	
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Benefits_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_Benefits_v]	


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Ind_Relat]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Ind_Relat]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Ind_Relat_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Ind_Relat_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Ind_Relat_Phone]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Ind_Relat_Phone]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Ind_Relat_Phone_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Ind_Relat_Phone_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Ind_Relat_Attr_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Ind_Relat_Attr_v]

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat_Phone]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Org_Relat_Phone]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat_Phone_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Org_Relat_Phone_v]	
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Org_Relat]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Org_Relat_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat_Attr_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Org_Relat_Attr_v]
	
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Package_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Package_v]

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Proposal_Attr_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Proposal_Attr_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Proposal_Notes_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Proposal_Notes_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Proposal_Solicitor_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Proposal_Solicitor_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Proposal_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Proposal_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Prospect_Other_Gifts_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Prospect_Other_Gifts_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Prospect_Philanthropic_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Prospect_Philanthropic_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Prospect_Financial_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Prospect_Financial_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Prospect_Rating_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Prospect_Rating_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Prospect_WillNotGive_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Prospect_WillNotGive_v]

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_CampaignFund_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_CampaignFund_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_CampaignAppeal_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_CampaignAppeal_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_CampaignSolicitor_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_CampaignSolicitor_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Bank_Financial_Institution_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Bank_Financial_Institution_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Bank_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Bank_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Address_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Cons_Address_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_PivotWizard_p]') and objectproperty(id, N'IsProcedure') = 1)
	drop procedure [dbo].[HC_PivotWizard_p]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_PivotWizard_IRPhones_p]') and objectproperty(id, N'IsProcedure') = 1)
	drop procedure [dbo].[HC_PivotWizard_IRPhones_p]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_PivotWizard_ORPhones_p]') and objectproperty(id, N'IsProcedure') = 1)
	drop procedure [dbo].[HC_PivotWizard_ORPhones_p]

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Phone_t]') and objectproperty(id, N'IsTable') = 1)
	drop table [dbo].[HC_Cons_Phone_t]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Ind_Relat_Phone_t]') and objectproperty(id, N'IsTable') = 1)
	drop table [dbo].HC_Ind_Relat_Phone_t
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat_Phone_t]') and objectproperty(id, N'IsTable') = 1)
	drop table [dbo].HC_Org_Relat_Phone_t
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Fund_Relationship_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Fund_Relationship_v] 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Users_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Users_v]

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Volunteer_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Volunteer_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Volunteer_Type_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Volunteer_Type_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Volunteer_Availability_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Volunteer_Availability_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Volunteer_Assignment_Interest_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Volunteer_Assignment_Interest_v]

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Adjustments_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_Adjustments_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_Amendments_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_Amendments_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Appeal_Expenses_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Appeal_Expenses_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Appeal_Benefits_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Appeal_Benefits_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Package_Benefits_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Package_Benefits_v]

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Fund_GLDistribution_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Fund_GLDistribution_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Planned_Gift_Asset_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Planned_Gift_Asset_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Gift_ProposalLink_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Gift_ProposalLink_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Org_Relat_NonConsIR_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Org_Relat_NonConsIR_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Planned_Gift_Beneficiaries_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Planned_Gift_Beneficiaries_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Planned_Gift_Relationship_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Planned_Gift_Relationship_v]
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Media_v]') and objectproperty(id, N'IsView') = 1)
	drop view [dbo].[HC_Media_v]
	  
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Fund_Attr_v]') and objectproperty(id, N'isView') = 1)
	drop view [dbo].HC_Fund_Attr_v
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Campaign_Attr_v]') and objectproperty(id, N'isView') = 1)
	drop view [dbo].HC_Campaign_Attr_v
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Appeal_Attr_v]') and objectproperty(id, N'isView') = 1)
	drop view [dbo].HC_Appeal_Attr_v
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Package_Attr_v]') and objectproperty(id, N'isView') = 1)
	drop view [dbo].HC_Package_Attr_v
  
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Membership_v]') and objectproperty(id, N'isView') = 1)
	drop view [dbo].HC_Membership_v
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Membership_History_v]') and objectproperty(id, N'isView') = 1)
	drop view [dbo].HC_Membership_History_v
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Membership_Benefits_v]') and objectproperty(id, N'isView') = 1)
	drop view [dbo].HC_Membership_Benefits_v
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Membership_Linked_Gift_v]') and objectproperty(id, N'isView') = 1)
	drop view [dbo].HC_Membership_Linked_Gift_v
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Membeship_Solicitor_v]') and objectproperty(id, N'isView') = 1)
	drop view [dbo].HC_Membeship_Solicitor_v

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Cons_Annotation_v]') and objectproperty(id, N'isView') = 1)
	drop view [dbo].HC_Cons_Annotation_v


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HC_Tribute_Acknowledgee_v]') and objectproperty(id, N'isView') = 1)
	drop view [dbo].HC_Tribute_Acknowledgee_v



SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

--FUNCTION Returns Organization name of Licensee
CREATE  FUNCTION [dbo].[HC_OrgName_v]()  

	RETURNS varchar(60)  
	BEGIN 
	declare @OrgName as varchar(60)
		select @OrgName = ORGANIZATION_NAME 
		from general_information
	return @Orgname
	END



GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


	/****** Replace Blackbaud formula [dbo].[FormatAmount] to use BIGINT vs INT to accomodate larger currency amounts.  ******/
		SET ANSI_NULLS ON
		GO

		--SQL datatypes https://msdn.microsoft.com/en-us/library/ms187745.aspx

		SET QUOTED_IDENTIFIER ON
		GO
		CREATE FUNCTION [dbo].[HC_FormatAmount_f](@dAmount BIGINT, @iCountry INTEGER)
		RETURNS VARCHAR(2000)
		BEGIN
		  DECLARE @sSymbol VARCHAR(2000);
		  DECLARE @sAmount VARCHAR(2000);
		  DECLARE @i SMALLINT;
		  DECLARE @sReturn VARCHAR(2000);
		  DECLARE @iContinue INTEGER;
		  SET @i=1;
		  IF @iCountry = 0
			BEGIN
				 SET @sSymbol='$'
			END
		  ELSE
			BEGIN
    			SELECT  @sSymbol = Symbol FROM dbo.Country_Codes WHERE ID = @iCountry
			END;
  
		  SET @sAmount=CAST(@dAmount AS DECIMAL(17,2));
		  WHILE @iContinue = 0 
			BEGIN
				SET @i=@i+1;
				SET @sReturn=RTRIM(LTRIM(SUBSTRING(@sAmount,@i,1)));
				IF @sReturn = '0'
				BEGIN
					  SET @iContinue=0
	    			END
			   ELSE
				BEGIN
					 IF @sReturn = ''
					BEGIN
							SET @iContinue=0
		      			END
					 ELSE
					BEGIN
							SET @iContinue=1
		      			END
	    			END
			  END;
		  SET @sAmount=@sSymbol + SUBSTRING(@sAmount,@i,20);
		  IF @sAmount = @sSymbol
			BEGIN
				SET @sAmount=''
  			END;
		  RETURN @sAmount
		END
		GO


/* START CREATING VIEWS *****************************/

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
 
CREATE    VIEW dbo.HC_Cons_Annotation_v
AS

	SELECT	CONSTITUENT_ID as ConsID
			,IMPORT_ID as ImportID
			,ANNOTATIONTEXT as Annotation
			,CASE AnnotationAutoDisplay WHEN '-1' THEN 'TRUE' ELSE 'FALSE' END AS AnnotationAutoDisplay
			,dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort]
			,row_number() over (order by IMPORT_ID) as ID
	FROM DBO.RECORDS
	WHERE ANNOTATIONTEXT IS NOT NULL AND ANNOTATIONTEXT!=''

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


 
CREATE    VIEW dbo.HC_Country_v
AS

SELECT  T_2.LONGDESCRIPTION AS [Country], 
		T_2.SHORTDESCRIPTION AS [CountryCode],
		dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort]

FROM DBO.address AS T_1 
	 left outer join dbo.tableentries as T_2 on T_1.COUNTRY = T_2.TABLEENTRIESID 

WHERE T_1.COUNTRY is not null
GROUP BY T_2.LONGDESCRIPTION, T_2.SHORTDESCRIPTION

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

 
CREATE   VIEW dbo.HC_Campaign_v
AS

SELECT	IMPORT_ID as CampImportID,
		CAMPAIGN_ID as [CampID],
		DESCRIPTION as [CampDesc], 
		CASE CAMPAIGN.INACTIVE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [CampIsInactive], 
		dbo.dateformat(START_DATE, 'MM/DD/YYYY') [CampStartDate], 
		dbo.dateformat(END_DATE, 'MM/DD/YYYY') [CampEndDate],
		GOAL AS CampGoal, CAMPAIGN_CATEGORY.LONGDESCRIPTION CampCategory, NOTES CampNote, 
		
		dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort]
		
FROM    DBO.CAMPAIGN
LEFT OUTER JOIN DBO.TABLEENTRIES AS CAMPAIGN_CATEGORY ON CAMPAIGN.CATEGORY = CAMPAIGN_CATEGORY.TABLEENTRIESID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

 
CREATE  VIEW dbo.HC_Fund_v
AS

SELECT	IMPORT_ID as FundImportID,
		FUND.FUND_ID as [FundID] , 
		FUND.DESCRIPTION as [FundDesc], 
		CASE FUND.INACTIVE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [FundIsInactive], 
		dbo.dateformat(FUND.START_DATE, 'MM/DD/YYYY') [FundStartDate], 
		dbo.dateformat(FUND.END_DATE, 'MM/DD/YYYY')  [FundEndDate],
		T_1.LONGDESCRIPTION AS [FundCategory],
		T_2.LONGDESCRIPTION AS [FundType],
		CASE FUND.RESTRICTED_FLAG WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [FundIsRestricted],
		GOAL FundGoal, NOTES FundNote, 
		dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() as [RE_DB_OwnerShort]
 
FROM	DBO.FUND
		LEFT JOIN TABLEENTRIES T_1 ON FUND.FUND_CATEGORY = T_1.TABLEENTRIESID
		LEFT JOIN TABLEENTRIES T_2 ON FUND.FUNDTYPE= T_2.TABLEENTRIESID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

 
CREATE  VIEW dbo.HC_Appeal_v
AS


SELECT	IMPORT_ID as AppealImportID,
		APPEAL_ID as [AppealID], 
		DESCRIPTION as [AppDesc], 
		CASE APPEAL.INACTIVE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [AppIsInactive], 
		dbo.dateformat(START_DATE, 'MM/DD/YYYY') [AppStartDate], 
		dbo.dateformat(END_DATE, 'MM/DD/YYYY')  [AppEndDate], APPEAL_Category.LONGDESCRIPTION AppCategory,
		GOAL AppGoal, 
		NO_SOLICITED AS AppNumSolicited,
		NOTES AppNote,
		
		dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() as [RE_DB_OwnerShort], APPEAL.ID [APSysRecID]

FROM    DBO.APPEAL
LEFT OUTER JOIN DBO.TABLEENTRIES AS APPEAL_Category ON APPEAL.APPEALCATEGORYID = APPEAL_Category.TABLEENTRIESID  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
CREATE VIEW dbo.HC_Package_v
AS

SELECT	DBO.Package.IMPORT_ID as PackageImportID,
		Package.Package_ID PackageID, 
		dbo.Appeal.Appeal_ID AppealID, 
		dbo.Package.DESCRIPTION PackDesc, 
		T_1.LONGDESCRIPTION AS [PackCategory],
		CASE Package.INACTIVE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [PackIsInactive], 
		Package.START_DATE [PackStartDate], Package.END_DATE [PackEndDate],

		dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() as [RE_DB_OwnerShort]

FROM    DBO.Package left outer join dbo.Appeal on dbo.Package.Appeal_ID = dbo.Appeal.ID
LEFT OUTER JOIN DBO.TABLEENTRIES AS T_1 ON Package.PACKAGECATEGORYID = T_1.TABLEENTRIESID 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE VIEW HC_CampaignFund_v 
AS 
SELECT 
 CAMPAIGN.CAMPAIGN_ID CampID,
 T_1.FUND_ID CampFundID,
 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() as [RE_DB_OwnerShort]
 
FROM 
DBO.CAMPAIGN AS CAMPAIGN 
INNER JOIN DBO.CAMPAIGNFUNDS AS CAMPAIGN_CAMPAIGNFUNDS ON CAMPAIGN.ID = CAMPAIGN_CAMPAIGNFUNDS.CAMPAIGNID 
INNER JOIN DBO.FUND AS T_1 ON CAMPAIGN_CAMPAIGNFUNDS.FUNDID = T_1.ID

WHERE 
((T_1.FUND_ID IS NOT NULL))  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW HC_CampaignAppeal_v
AS 
SELECT 
 CAMPAIGN.CAMPAIGN_ID CampID,
 T_1.APPEAL_ID CampAppealID,
  dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() as [RE_DB_OwnerShort]

FROM 
DBO.CAMPAIGN AS CAMPAIGN 
INNER JOIN DBO.CAMPAIGNAPPEALS AS CAMPAIGN_CAMPAIGNAPPEALS ON CAMPAIGN.ID = CAMPAIGN_CAMPAIGNAPPEALS.CAMPAIGNID 
INNER JOIN DBO.APPEAL AS T_1 ON CAMPAIGN_CAMPAIGNAPPEALS.APPEALID = T_1.ID

WHERE 
((T_1.APPEAL_ID IS NOT NULL))  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

 
 
 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW HC_CampaignSolicitor_v 
AS 
SELECT 
 Campaign.CAMPAIGN_ID CampID,
 DBO.BuildFullName3(T_2.LAST_NAME,T_2.FIRST_NAME,T_2.MIDDLE_NAME,T_2.ORG_NAME,T_2.KEY_INDICATOR) CampSolicitorName,
 T_2.IMPORT_ID CampSolicitorImportID, 
 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() as [RE_DB_OwnerShort]

FROM 
DBO.CAMPAIGN AS Campaign 
INNER JOIN DBO.SOLICITOR_CAMPAIGN AS T_1 ON Campaign.ID = T_1.CAMPAIGN_ID LEFT OUTER JOIN DBO.RECORDS AS T_2 ON T_1.CONSTIT_ID = T_2.ID

WHERE 
((T_1.Constit_ID IS NOT NULL))   
 
 GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
 
 
 
 

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- VIEW 6
CREATE VIEW [dbo].[HC_Cons_Action_Notes_v]  
AS
SELECT     ACTIONS.IMPORT_ID AS [CALink], actions_ACTIONNOTEPAD.Import_Id AS [CANoteImpID], 
                      actions_ACTIONNOTEPAD.NotepadDate AS [CANoteDate], T_1.LONGDESCRIPTION AS [CANoteType], 
                      actions_ACTIONNOTEPAD.Description AS [CANoteDesc], 
                      REPLACE(REPLACE(REPLACE(CAST(actions_ACTIONNOTEPAD.ActualNotes AS varchar(max)), CHAR(13), ' '), CHAR(10), ' '), CHAR(9), ' ') 
                      AS [CANoteNotes], 
					  actions_ACTIONNOTEPAD.Title AS [CANoteTitle], 
					  actions_ACTIONNOTEPAD.Author AS [CANoteAuthor], 
					  dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'AN' [RE_DB_Tbl],
                      row_number() over (order by ACTIONS.ID) as ID
                      
FROM         dbo.ACTIONS AS ACTIONS 
			 LEFT OUTER JOIN dbo.ActionNotepad AS actions_ACTIONNOTEPAD ON ACTIONS.ID = actions_ACTIONNOTEPAD.ParentId 
			 LEFT OUTER JOIN dbo.TABLEENTRIES AS T_1 ON actions_ACTIONNOTEPAD.NoteTypeId = T_1.TABLEENTRIESID

WHERE		actions_ACTIONNOTEPAD.Import_Id IS NOT NULL


GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 7
CREATE VIEW dbo.HC_Cons_Addl_Addressee_v
AS
SELECT     T_1.IMPORT_ID [AddSalImpID], T_1.salutation_id [AddSalID], 
                      T_2.LONGDESCRIPTION [AddSalType], DBO.Query_GetAddrSal(T_1.CONSTIT_ID, T_1.EDITABLE, T_1.SALUTATION_ID, T_1.SALUTATION, 
                      getdate()) [AddSalText], RECORDS.IMPORT_ID [ImportID], 
                      RECORDS.CONSTITUENT_ID [ConsID], 
                      CASE T_1.EDITABLE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [AddSalEditable], 
					  dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'SA' [RE_DB_Tbl],
					  row_number() over (order by T_1.ID) as ID

FROM         DBO.RECORDS AS RECORDS LEFT OUTER JOIN
                      DBO.CONSTITUENT_SALUTATION AS T_1 ON RECORDS.ID = T_1.CONSTIT_ID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS T_2 ON T_1.SAL_TYPE = T_2.TABLEENTRIESID
WHERE     ((RECORDS.IS_CONSTITUENT = - 1)) AND T_1.IMPORT_ID IS NOT NULL

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 8
CREATE VIEW dbo.HC_Cons_Appeal_v
AS
SELECT     dbo.CONSTITUENT_APPEALS.IMPORT_ID AS [CAPImpID], T_2.APPEAL_ID AS [CAPAppealID], 
                      dbo.CONSTITUENT_APPEALS.COMMENTS AS [CAPComment], RECORDS.IMPORT_ID AS [ImportID], 
                      RECORDS.CONSTITUENT_ID AS [ConsID], dbo.CONSTITUENT_APPEALS.DTE AS [CAPDate], 
                      T_3.PACKAGE_ID AS [CAPPackageID], T_4.LONGDESCRIPTION AS [CAPResponse], 
                      dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'CL' [RE_DB_Tbl],
					  row_number() over (order by RECORDS.ID) as ID
					  
FROM         dbo.RECORDS RECORDS LEFT OUTER JOIN
                      dbo.CONSTITUENT_APPEALS ON RECORDS.ID = dbo.CONSTITUENT_APPEALS.CONSTIT_ID LEFT OUTER JOIN
                      dbo.APPEAL T_2 ON dbo.CONSTITUENT_APPEALS.APPEAL_ID = T_2.ID LEFT OUTER JOIN
                      dbo.Package T_3 ON dbo.CONSTITUENT_APPEALS.PackageID = T_3.ID LEFT OUTER JOIN
                      dbo.TABLEENTRIES T_4 ON dbo.CONSTITUENT_APPEALS.Response = T_4.TABLEENTRIESID
WHERE     (RECORDS.IS_CONSTITUENT = - 1) AND (dbo.CONSTITUENT_APPEALS.IMPORT_ID IS NOT NULL)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 9

CREATE VIEW dbo.HC_Cons_Action_v
AS
SELECT				 ACTIONS_RECORDS.IMPORT_ID [ImportID],  ACTIONS.ID [ACSysRecID],
					  ACTIONS.IMPORT_ID [ACImpID], 
					  cast(dbo.dateformat(ACTIONS.DTE, 'MM/DD/YYYY') as varchar(10)) AS [ACDate], 
					  actions_ALERT_TITLE.LONGDESCRIPTION [ACAlertTitle], 
                      ACTIONS_APPEAL_ID.APPEAL_ID [ACAppeal], 
                      ACTIONS_CAMPAIGN_ID.CAMPAIGN_ID [ACCampaign], 
                      ACTIONS_FUND_ID.FUND_ID [ACFund], 
                      CASE ACTIONS.CATEGORY WHEN 1 THEN 'Phone Call' WHEN 2 THEN 'Meeting' WHEN 3 THEN 'Mailing' WHEN 4 THEN 'Email' WHEN 5 THEN 'Task/Other'
                       WHEN 6 THEN 'Advocacy' ELSE '' END [ACCat], 
                      CASE ACTIONS.Completed WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [ACComplete], 
                      cast(dbo.dateformat(ACTIONS.COMPLETED_DATE, 'MM/DD/YYYY')  as varchar(10)) AS [ACCompleteDate], 
                      ACTIONS_RECORDS.CONSTITUENT_ID [ConsID], T_1.Import_ID [ACContact],  
                      actions_DELIVERY_METHOD.LONGDESCRIPTION [ACDeliveryMethod], Actions.PhoneNumber [ACEmail], 
                      ACTIONS.END_TIME [ACEndTime], 
                      actions_ISSUE.LONGDESCRIPTION [ACIssue], ACTIONS_LETTER_CODE.LONGDESCRIPTION [ACLetter], 
					  ACTIONS.WORDDOCNAME 'ACDocument',
                      ACTIONS_Location.LONGDESCRIPTION [ACLocation], ACTIONS.LINK_ID [ACMessageID], 
                      ACTIONS.PhoneNumber [ACPhone], 
                      CASE ACTIONS.PRIORITY WHEN 0 THEN 'High' WHEN 1 THEN 'Normal' WHEN 2 THEN 'Low' ELSE '' END [ACPriority], 
                      ACTIONS_PROPOSAL_ID.Import_ID [ProposalImpID], ACTIONS.RECIPIENT_NAME [ACRecipient], 
                      case WHEN ACTIONS_STATUS.LONGDESCRIPTION IS NULL THEN 'NULL' ELSE ACTIONS_STATUS.LONGDESCRIPTION END AS [ACStatus], 
                      CASE WHEN ACTIONS_TYPE.LONGDESCRIPTION IS NULL THEN 'NULL' ELSE ACTIONS_TYPE.LONGDESCRIPTION  END AS [ACType], 
                      ACTIONS.OUTLOOK_EMAIL_SUBJECT ACEmailSubject, ACTIONS.OUTLOOK_EMAIL_TEXT as ACEmailText, 
  					  CONVERT(CHAR(10),ACTIONS.DATEADDED,101) [DateAdded], 
					  CONVERT(CHAR(10),ACTIONS.DATECHANGED,101) [DateLastChanged],
					  ACTIONS_AddedBy.NAME [AddedBy],
					  ACTIONS_AddedBy.NAME [LastChangedBy],

					  dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'CA' [RE_DB_Tbl],
					  row_number() over (order by ACTIONS.ID) as ID

FROM         DBO.ACTIONS AS ACTIONS LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS actions_ALERT_TITLE ON ACTIONS.ALERT_TITLE = actions_ALERT_TITLE.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.APPEAL AS ACTIONS_APPEAL_ID ON ACTIONS.SELECTED_APPEAL_ID = ACTIONS_APPEAL_ID.ID LEFT OUTER JOIN
                      DBO.CAMPAIGN AS ACTIONS_CAMPAIGN_ID ON ACTIONS.CAMPAIGN_ID = ACTIONS_CAMPAIGN_ID.ID LEFT OUTER JOIN
                      DBO.RECORDS AS ACTIONS_RECORDS ON ACTIONS.RECORDS_ID = ACTIONS_RECORDS.ID LEFT OUTER JOIN
                      DBO.CONSTIT_RELATIONSHIPS AS T_1 ON ACTIONS.CONTACT_ID = T_1.ID LEFT OUTER JOIN
                      DBO.RECORDS AS T_2 ON T_1.RELATION_ID = T_2.ID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS actions_DELIVERY_METHOD ON 
                      ACTIONS.DELIVERY_METHOD = actions_DELIVERY_METHOD.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.FUND AS ACTIONS_FUND_ID ON ACTIONS.FUND_ID = ACTIONS_FUND_ID.ID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS actions_ISSUE ON ACTIONS.ISSUE = actions_ISSUE.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS ACTIONS_LETTER_CODE ON ACTIONS.LETTER_CODE = ACTIONS_LETTER_CODE.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS ACTIONS_Location ON ACTIONS.LOCATION = ACTIONS_Location.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.PROPOSAL AS ACTIONS_PROPOSAL_ID ON ACTIONS.PROPOSAL_ID = ACTIONS_PROPOSAL_ID.ID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS ACTIONS_STATUS ON ACTIONS.STATUS = ACTIONS_STATUS.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS ACTIONS_TYPE ON ACTIONS.TYPE = ACTIONS_TYPE.TABLEENTRIESID
					  LEFT JOIN DBO.USERS AS ACTIONS_AddedBy ON ACTIONS.ADDED_BY = ACTIONS_AddedBy.USER_ID 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 10

CREATE VIEW dbo.HC_Cons_Action_Attr_v
AS
SELECT     dbo.ActionAttributes.IMPORTID [ACAttrImpID], dbo.AttributeTypes.DESCRIPTION [ACAttrCat], 
                      dbo.Actions.import_id [ACImpID], dbo.ActionAttributes.COMMENTS [ACAttrCom], 
                      dbo.ActionAttributes.AttributeDate [ACAttrDate], 
					  ACAttrDesc = CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL 
                      THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
					  WHEN dbo.actionattributes.boolean IS NOT NULL THEN (CASE dbo.actionattributes.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END) --CONVERT(varchar, dbo.actionattributes.boolean) 
					  WHEN dbo.actionattributes.text IS NOT NULL THEN dbo.actionattributes.text 
                      WHEN dbo.actionattributes.num IS NOT NULL  THEN CONVERT(varchar, dbo.actionattributes.num) 
					  WHEN dbo.actionattributes.datetime IS NOT NULL THEN CONVERT(varchar, dbo.actionattributes.datetime) 
					  WHEN dbo.actionattributes.currency IS NOT NULL THEN CONVERT(varchar, dbo.actionattributes.currency) 
                      WHEN dbo.actionattributes.constitid IS NOT NULL THEN CONVERT(varchar, dbo.actionattributes.constitid) ELSE NULL END, 
                      case dbo.AttributeTypes.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
					  when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
					  when 8 then 'FuzzyDate' end as [DataType], 	
					  dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'AR' [RE_DB_Tbl],
                      row_number() over (order by ACTIONS.ID) as ID

FROM         ((dbo.ACTIONS LEFT JOIN
                      dbo.ActionAttributes ON dbo.ACTIONS.ID = dbo.ActionAttributes.PARENTID) LEFT JOIN
                      (dbo.AttributeTypes LEFT JOIN
                      dbo.CODETABLES ON dbo.AttributeTypes.CODETABLESID = dbo.CODETABLES.CODETABLESID) ON 
                      dbo.ActionAttributes.ATTRIBUTETYPESID = dbo.AttributeTypes.ATTRIBUTETYPESID) LEFT JOIN
                      dbo.TABLEENTRIES ON dbo.ActionAttributes.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.attributeTYPEs AS T_1 ON ACTIONATTRIBUTES.ATTRIBUTETYPESID = T_1.ATTRIBUTETYPESID
WHERE     dbo.ActionAttributes.IMPORTID IS NOT NULL

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



-- VIEW 11
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW [dbo].HC_Cons_Phone_v
AS
SELECT      RECORDS.CONSTITUENT_ID AS ConsID, RECORDS.IMPORT_ID AS ImportID,
			RECORDS_CONSTIT_ADDRESS.IMPORT_ID AS [PhoneAddrImpID], T_1.IMPORT_ID AS [PhoneImpID], 
                      T_3.LONGDESCRIPTION AS PhoneType, convert(varchar(255), T_2.NUM) AS PhoneNum, 
					case T_2.DO_NOT_CALL when -1 then 'TRUE' else 'FALSE' end as PhoneDoNotCall,
                     -- T_1.DO_NOT_SHARE, T_3.PHONETYPENOTSHAREABLE, 
                      dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'PH' [RE_DB_Tbl],
                      row_number() over (order by T_1.IMPORT_ID) as ID

FROM         dbo.RECORDS RECORDS INNER JOIN
                      dbo.QConstit_Address RECORDS_CONSTIT_ADDRESS ON RECORDS.ID = RECORDS_CONSTIT_ADDRESS.CONSTIT_ID LEFT OUTER JOIN
                     dbo.CONSTIT_ADDRESS_PHONES T_1 ON RECORDS_CONSTIT_ADDRESS.ID = T_1.CONSTITADDRESSID LEFT OUTER JOIN
                      dbo.PHONES T_2 ON T_1.PHONESID = T_2.PHONESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES T_3 ON T_2.PHONETYPEID = T_3.TABLEENTRIESID
WHERE     (RECORDS.IS_CONSTITUENT = - 1) AND (T_2.NUM IS NOT NULL)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF
GO



-- STORED PROCEDURE FOR PHONE NUMBERS STORED IN CONSTITUENT ADDRESSES.

		CREATE PROCEDURE HC_PivotWizard_p
		   @P_Row_Field    VARCHAR(255),
		   @P_Column_Field VARCHAR(255),
		   @P_Value        VARCHAR(4000),
		   @P_Into         VARCHAR(4000),
		   @P_From         VARCHAR(4000),
		   @P_Where        VARCHAR(4000) = '1=1'

		AS

		  DECLARE @SQL NVARCHAR(4000)

		  -- Build SQL statment that upload @Columns string 
		  -- with @P_Column_Filed values
		  CREATE TABLE #TEMP  (ColumnField varchar(255))
		  SET @sql ='SELECT DISTINCT '+@P_Column_Field+' AS ColumnField'+
					  ' FROM '+@P_From+
					  ' WHERE '+@P_Where+
					  ' ORDER BY '+@P_Column_Field
		  INSERT INTO #TEMP
		  EXEC(@sql)
		  --PRINT @sql

		  -- Check count of columns
		  DECLARE @Count_Columns int
		  SELECT @Count_Columns = COUNT(*) FROM #Temp
		  IF (@Count_Columns<1) OR (@Count_Columns>255)  BEGIN
			  DROP TABLE #Temp
			  RAISERROR('%d is invalid columns amount. Valid is 1-255',
						16,1,@Count_columns)
			  RETURN
		  END
		  -- Upload @Columns from #Temp
		  DECLARE @Columns VARCHAR(8000),
				  @Column_Field VARCHAR(8000)

		  SET @Columns = ''
		  DECLARE Column_cursor CURSOR LOCAL FOR
		  SELECT CAST(ColumnField AS VARCHAR(60))
		  FROM #Temp
		  OPEN Column_cursor
		  FETCH NEXT FROM Column_cursor
		  INTO @Column_Field 
		  WHILE @@FETCH_STATUS = 0 BEGIN
			  SET @Columns = @Columns +
				' MAX('+
					 ' CASE WHEN '+@P_Column_Field+'='''+ @Column_Field+''''+
					 ' THEN '+@P_Value+
					 ' ELSE null END'+
					 ') AS ['+ @Column_Field +'], '
			  FETCH NEXT FROM Column_cursor
			  INTO @Column_Field
		  END
		  CLOSE Column_cursor
		  DEALLOCATE Column_cursor
		  DROP TABLE #Temp

		  IF @Columns='' RETURN 1
		  SET @Columns = Left(@Columns,Len(@Columns)-1)

		  -- Build Pivot SQL statment
		  DECLARE @Pivot_SQL VARCHAR(8000)
		  SET @Pivot_SQL =              'SELECT '  +@P_Row_Field+', '+@Columns
		  SET @Pivot_SQL = @Pivot_SQL +' INTO '    +@P_Into  
		  SET @Pivot_SQL = @Pivot_SQL +' FROM '    +@P_From
		  SET @Pivot_SQL = @Pivot_SQL +' WHERE '   +@P_Where
		  SET @Pivot_SQL = @Pivot_SQL +' GROUP BY '+@P_Row_Field
		  SET @Pivot_SQL = @Pivot_SQL +' ORDER BY '+@P_Row_Field
		  SET @Pivot_SQL = @Pivot_SQL + '#'

		  IF Right(@Pivot_SQL,1)<>'#'
		  BEGIN
			 RAISERROR('SQL statement is too long. It must be less
						than 8000 charachter!',16,1)
			 RETURN 1
		  END
		  SET @Pivot_SQL = Left(@Pivot_SQL,Len(@Pivot_SQL)-1)

		  -- PRINT @Pivot_SQL
		  EXEC(@Pivot_SQL)

		  RETURN 0
		GO  

		--  execuate the sp. 
		exec HC_PivotWizard_P 'PhoneAddrImpID', 
							  'PhoneType',
						      '[PhoneNum]',
    						  'dbo.HC_Cons_Phone_t',
						      'dbo.HC_Cons_Phone_v',
						      'PhoneAddrImpID is not null'  



 


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 12: NON PREFERRED ADDRESS with PHONE NUMBERS 
 
CREATE VIEW [dbo].HC_Cons_Address_NonPref_v
AS
SELECT				  RECORDS_CONSTIT_ADDRESS.ADDRESS_ID as REFERENCE_ADDRESS_ID, RECORDS_CONSTIT_ADDRESS.ID AS REFERENCE_ID,
					  RECORDS.import_id [ImportID], RECORDS.Constituent_id [ConsID], 
					  RECORDS_CONSTIT_ADDRESS.IMPORT_ID [AddrImpID],  RECORDS_CONSTIT_ADDRESS.SEQUENCE [AddrSequence],
				      DBO.ADDRESSLINE(1, T_1.ADDRESS_BLOCK) [AddrLine1], 
                      DBO.ADDRESSLINE(2, T_1.ADDRESS_BLOCK) [AddrLine2], DBO.ADDRESSLINE(3, T_1.ADDRESS_BLOCK) [AddrLine3], 
                      DBO.ADDRESSLINE(4, T_1.ADDRESS_BLOCK) [AddrLine4], DBO.ADDRESSLINE(5, T_1.ADDRESS_BLOCK) [AddrLine5], 
                      T_1.CARRIER_ROUTE [CART], T_1.CITY [City], T_2.LONGDESCRIPTION [AddrCountry], T_3.LONGDESCRIPTION [AddrCounty], 
                      RECORDS_CONSTIT_ADDRESS.DATE_FROM [AddrValidFrom], RECORDS_CONSTIT_ADDRESS.DATE_TO [AddrValidTo], T_1.DPC [AddrDPC], 
                      T_4.LONGDESCRIPTION [AddrInfoSrc], T_1.LOT [AddrLOT], T_5.LONGDESCRIPTION [AddrNZCity], T_6.LONGDESCRIPTION [AddrNZSuburb], 
                      CASE RECORDS_CONSTIT_ADDRESS.PREFERRED WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [PrefAddr], 
                      T_7.LONGDESCRIPTION [AddrRegion], 
                      CASE RECORDS_CONSTIT_ADDRESS.SEASONAL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [AddrSeasonal], 
                      RECORDS_CONSTIT_ADDRESS.SEASONAL_FROM [AddrSeasFrom], 
                      RECORDS_CONSTIT_ADDRESS.SEASONAL_TO [AddrSeasTo], 
                      CASE RECORDS_CONSTIT_ADDRESS.SENDMAIL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [AddrSendMail], 
                      T_8.SHORTDESCRIPTION [AddrState], T_9.LONGDESCRIPTION [AddrType], T_1.POST_CODE [AddrZIP], 
					  --properties
                      CONVERT(CHAR(10),RECORDS_CONSTIT_ADDRESS.DATE_ADDED,101) [DateAdded], 
                      CONVERT(CHAR(10),RECORDS_CONSTIT_ADDRESS.DATE_LAST_CHANGED,101) [DateLastChanged], 
 					  RECORDS_CONSTIT_ADDRESS.ID [SystemRecordID],

					 --PHONES (T_10)
					 -- T_10.*,
                      dbo.hc_OrgName_v() [RE_DB_Owner],  dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'DA' [RE_DB_Tbl],
                      row_number() over (order by T_1.ID) as ID

FROM         DBO.RECORDS AS RECORDS 
					INNER JOIN DBO.QCONSTIT_ADDRESS AS RECORDS_CONSTIT_ADDRESS ON RECORDS.ID = RECORDS_CONSTIT_ADDRESS.CONSTIT_ID 
					INNER JOIN DBO.ADDRESS AS T_1 ON RECORDS_CONSTIT_ADDRESS.ADDRESS_ID = T_1.ID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON T_1.COUNTRY = T_2.TABLEENTRIESID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_3 ON T_1.COUNTY = T_3.TABLEENTRIESID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_4 ON RECORDS_CONSTIT_ADDRESS.INFO_SOURCE = T_4.TABLEENTRIESID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_5 ON T_1.NZCITY = T_5.TABLEENTRIESID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_6 ON T_1.SUBURB = T_6.TABLEENTRIESID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_7 ON T_1.REGION = T_7.TABLEENTRIESID 
					LEFT OUTER JOIN DBO.STATES AS T_8 ON T_1.STATE = T_8.SHORTDESCRIPTION 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_9 ON RECORDS_CONSTIT_ADDRESS.TYPE = T_9.TABLEENTRIESID

					LEFT JOIN dbo.HC_Cons_Phone_t AS T_10 ON RECORDS_CONSTIT_ADDRESS.IMPORT_ID  = T_10.PHONEADDRIMPID

WHERE     ((RECORDS.IS_CONSTITUENT = - 1) and (RECORDS_CONSTIT_ADDRESS.PREFERRED = 0))




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 13
CREATE VIEW dbo.HC_Cons_Address_Attr_v
AS
SELECT     dbo.constitaddressattributes.IMPORTID [CADAttrImpID], dbo.AttributeTypes.DESCRIPTION [CADAttrCat], 
                      dbo.constit_address.import_id [CADAttrAddrImpID], dbo.constitaddressattributes.COMMENTS [CADAttrCom], 
                      dbo.constitaddressattributes.AttributeDate [CADAttrDate], CADAttrDesc = CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL 
                      THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
					  WHEN dbo.constitaddressattributes.boolean IS NOT NULL THEN (CASE dbo.constitaddressattributes.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END)
					  WHEN dbo.constitaddressattributes.text IS NOT NULL 
                      THEN dbo.constitaddressattributes.text WHEN dbo.constitaddressattributes.num IS NOT NULL THEN CONVERT(varchar, 
                      dbo.constitaddressattributes.num) WHEN dbo.constitaddressattributes.datetime IS NOT NULL THEN CONVERT(varchar, 
                      dbo.constitaddressattributes.datetime) WHEN dbo.constitaddressattributes.currency IS NOT NULL THEN CONVERT(varchar, 
                      dbo.constitaddressattributes.currency) WHEN dbo.constitaddressattributes.constitid IS NOT NULL THEN CONVERT(varchar, 
                      dbo.constitaddressattributes.constitid) ELSE NULL END, 
                      case dbo.AttributeTypes.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
											   when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
											   when 8 then 'FuzzyDate' end as [DataType], 	
					  dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'AA' [RE_DB_Tbl],
                      row_number() over (order by ID) as ID

FROM         ((dbo.constit_address LEFT JOIN
                      dbo.constitaddressattributes ON dbo.constit_address.ID = dbo.constitaddressattributes.PARENTID) LEFT JOIN
                      (dbo.AttributeTypes LEFT JOIN
                      dbo.CODETABLES ON dbo.AttributeTypes.CODETABLESID = dbo.CODETABLES.CODETABLESID) ON 
                      dbo.constitaddressattributes.ATTRIBUTETYPESID = dbo.AttributeTypes.ATTRIBUTETYPESID) LEFT JOIN
                      dbo.TABLEENTRIES ON dbo.constitaddressattributes.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.attributeTYPEs AS T_1 ON constitaddressattributes.ATTRIBUTETYPESID = T_1.ATTRIBUTETYPESID
WHERE     dbo.constitaddressattributes.IMPORTID IS NOT NULL

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- VIEW 14
CREATE   view dbo.HC_Cons_Attributes_v
AS
SELECT     dbo.ConstituentAttributes.IMPORTID [CAttrImpID], dbo.AttributeTypes.DESCRIPTION [CAttrCat], 
                      dbo.records.constituent_ID [ConsID], dbo.records.import_id [ImportID], 
                      dbo.ConstituentAttributes.COMMENTS [CAttrCom], CONVERT(CHAR(10),dbo.ConstituentAttributes.AttributeDate ,101) [CAttrDate], 
                      CAttrDesc = CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL 
                      THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
					  WHEN dbo.constituentattributes.boolean IS NOT NULL THEN (CASE dbo.constituentattributes.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END)
						WHEN dbo.ConstituentAttributes.text IS NOT NULL 
                      THEN dbo.ConstituentAttributes.text WHEN dbo.ConstituentAttributes.num IS NOT NULL THEN CONVERT(varchar, dbo.ConstituentAttributes.num) 
                      WHEN dbo.ConstituentAttributes.datetime IS NOT NULL THEN CONVERT(varchar, dbo.ConstituentAttributes.datetime) 
                      WHEN dbo.ConstituentAttributes.currency IS NOT NULL THEN CONVERT(varchar, dbo.ConstituentAttributes.currency) 
                      WHEN dbo.ConstituentAttributes.constitid IS NOT NULL THEN CONVERT(varchar, dbo.ConstituentAttributes.constitid) ELSE NULL END, 
                      case dbo.AttributeTypes.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
											   when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
											   when 8 then 'FuzzyDate' end as [DataType], 	
                      dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'AT' [RE_DB_Tbl],
                      row_number() over (order by ID) as ID

FROM         ((dbo.records LEFT JOIN
                      dbo.ConstituentAttributes ON dbo.records.ID = dbo.ConstituentAttributes.PARENTID) LEFT JOIN
                      (dbo.AttributeTypes LEFT JOIN
                      dbo.CODETABLES ON dbo.AttributeTypes.CODETABLESID = dbo.CODETABLES.CODETABLESID) ON 
                      dbo.ConstituentAttributes.ATTRIBUTETYPESID = dbo.AttributeTypes.ATTRIBUTETYPESID) LEFT JOIN
                      dbo.TABLEENTRIES ON dbo.ConstituentAttributes.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.attributeTYPEs AS T_1 ON ConstituentATTRIBUTES.ATTRIBUTETYPESID = T_1.ATTRIBUTETYPESID
WHERE     dbo.ConstituentAttributes.IMPORTID IS NOT NULL



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
 
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

 

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 15: Constituent Bio info and Preferred Address
CREATE     view dbo.HC_Constituents_v  
AS
SELECT    DISTINCT RECORDS.KEY_INDICATOR [KeyInd], 
					  CASE WHEN  ISDATE(RECORDS.BIRTH_DATE)=1 AND LEN(RECORDS.BIRTH_DATE)=8 THEN CAST(DBO.DATEFORMAT(RECORDS.BIRTH_DATE, 'MM/DD/YYYY') as varchar(10))
					  WHEN ISDATE(RECORDS.BIRTH_DATE)=1 AND LEN(RECORDS.BIRTH_DATE)=6 THEN CAST(RIGHT(RECORDS.BIRTH_DATE,2) +'/'+ LEFT(RECORDS.BIRTH_DATE,4) AS varchar(10))
					  WHEN ISDATE(RECORDS.BIRTH_DATE)=1 AND LEN(RECORDS.BIRTH_DATE)=4 THEN cast(RECORDS.BIRTH_DATE as varchar(10))
	                  ELSE NULL END AS [BDay],  RECORDS.BIRTHPLACE [BPLACE], 
                      RECORDS.CONSTITUENT_ID [ConsID], records.import_ID [ImportID], 
                      CASE RECORDS.DECEASED WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [Deceased], 
                      CASE WHEN ISDATE(RECORDS.DECEASED_DATE)=1 AND LEN(RECORDS.DECEASED_DATE)=8 THEN CAST(DBO.DATEFORMAT(RECORDS.DECEASED_DATE, 'MM/DD/YYYY') AS varchar(10))
					  WHEN ISDATE(RECORDS.DECEASED_DATE)=1 AND LEN(RECORDS.DECEASED_DATE)=6 THEN cast(RIGHT(RECORDS.DECEASED_DATE,2) +'/'+ LEFT(RECORDS.DECEASED_DATE,4) AS varchar(10))
					  WHEN ISDATE(RECORDS.DECEASED_DATE)=1 AND LEN(RECORDS.DECEASED_DATE)=4 THEN cast(RECORDS.DECEASED_DATE as varchar(10))
					  ELSE NULL END AS [DecDate],  RECORDS_ETHNICITY.LONGDESCRIPTION [Ethnicity], RECORDS.FIRST_NAME [FirstName], 
                      RECORDS.FISCAL_YEAR_STARTS [FiscalYrSt], 
                      DBO.Query_ConstitName(1,RECORDS.LAST_NAME,RECORDS.FIRST_NAME,RECORDS.MIDDLE_NAME,RECORDS.ORG_NAME,RECORDS.KEY_INDICATOR,RECORDS.CONSTITUENT_ID) As Name,
                      CASE RECORDS.SEX WHEN 1 THEN 'Male' WHEN 2 THEN 'Female' WHEN 3 THEN 'Unknown' ELSE '' END [Gender], 
                      CASE RECORDS.ANONYMOUS WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [GivesAnon], 
                      CASE RECORDS.NO_VALID_ADDRESSES WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [NoValidAddr], --RECORDS.IMPORT_ID [ImportID], 
                      RECORDS_INCOME.LONGDESCRIPTION [Income], RECORDS_INDUSTRY.LONGDESCRIPTION [Industry], 
                      CASE RECORDS.IS_HONORMEMORIAL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [IsHonMem], 
                      CASE RECORDS.INACTIVE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [IsInactive], 
                      CASE RECORDS.SOLICITOR_FLAG WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [IsSolicitor], 
                      RECORDS.LAST_NAME [LastName], RECORDS.MAIDEN_NAME [MaidName], 
                      RECORDS_MARITALSTATUS.LONGDESCRIPTION [MrtlStat], RECORDS.MATCHING_FACTOR [MtcFactor], 
                      RECORDS.MAX_MATCH_ANNUAL [MaxMtcAnn], RECORDS.MAX_MATCH_PER_GIFT [MaxMtcPer], 
                      RECORDS.MAX_MATCH_TOTAL [MaxMtcTot], RECORDS.MIDDLE_NAME [MidName], 
                      RECORDS.MIN_MATCH_ANNUAL [MinMtcAnn], RECORDS.MIN_MATCH_PER_GIFT [MinMtcPer], 
                      RECORDS.MIN_MATCH_TOTAL [MinMtcTot], RECORDS.NICKNAME [Nickname], 
                      CASE RECORDS.MATCHING_GIFT_FLAG WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [OrgMatch], 
					  RECORDS.MATCH_NOTES as OrgMatchNotes,
                      RECORDS.ORG_NAME [OrgName], PARENT_CORP_RECORDS.IMPORT_ID [ParentCorpLink], DBO.Query_GetParentCorp(RECORDS.ID, 
                      RECORDS.PARENT_CORP, RECORDS.PARENT_CORP_ID) [ParentCorpName], 
                      CASE RECORDS.PRIMARY_ADDRESSEE_EDIT WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [PrimAddEdit], 
                      records.primary_addressee_ID [PrimAddID], DBO.Query_GetAddrSal(RECORDS.ID, 
                      RECORDS.PRIMARY_ADDRESSEE_EDIT, RECORDS.PRIMARY_ADDRESSEE_ID, RECORDS.PRIMARY_ADDRESSEE, '02/08/2007') 
                      [PrimAddText], 
                      CASE RECORDS.PRIMARY_SALUTATION_EDIT WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [PrimSalEdit], 
                      records.primary_salutation_id [PrimSalID], DBO.Query_GetAddrSal(RECORDS.ID, 
                      RECORDS.PRIMARY_SALUTATION_EDIT, RECORDS.PRIMARY_SALUTATION_ID, RECORDS.PRIMARY_SALUTATION, '02/08/2007') 
                      [PrimSalText], T_0.LONGDESCRIPTION [ProspectClass], RECORDS_PROSPECT_STATUS.LONGDESCRIPTION [ProspectStat], 
                      CASE RECORDS.RECEIPT_TYPE WHEN 1 THEN 'Consolidated receipts' WHEN 0 THEN 'One receipt per gift' ELSE '' END [RcptType], 
                      RECORDS_RELIGION.LONGDESCRIPTION [Religion], 
                      CASE RECORDS.NO_EMAIL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [NoEmail], 
                      CASE RECORDS.SOLICITOR_INACTIVE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [SolInactive], 
                      RECORDS.SOCIAL_SECURITY_NO [SSNum], RECORDS_SUFFIX_1.LONGDESCRIPTION [Suff1], RECORDS_SUFFIX_2.LONGDESCRIPTION [Suff2], 
                      RECORDS_TARGET.LONGDESCRIPTION [Trgt], RECORDS_TITLE_1.LONGDESCRIPTION [Titl1], RECORDS_TITLE_2.LONGDESCRIPTION [Titl2], 
                      
                      --properties
                      CONVERT(CHAR(10),RECORDS.DATE_ADDED,101) [DateAdded], 
                      CONVERT(CHAR(10),RECORDS.DATE_LAST_CHANGED,101) [DateLastChanged], 
                      RECORDS_AddedBy.NAME [AddedBy],
					  RECORDS_LastChangedBy.NAME [LastChangedBy],
					  RECORDS.ID [SystemRecordID],
					 
					  -- ADDRESS fields. 
					  RECORDS_CONSTIT_ADDRESS.ADDRESS_ID as REFERENCE_ADDRESS_ID, RECORDS_CONSTIT_ADDRESS.ID AS REFERENCE_ID,
					  RECORDS_CONSTIT_ADDRESS.IMPORT_ID [AddrImpID],  RECORDS_CONSTIT_ADDRESS.SEQUENCE [AddrSequence],
					  DBO.ADDRESSLINE(1, T_1.ADDRESS_BLOCK) [AddrLine1], 
                      DBO.ADDRESSLINE(2, T_1.ADDRESS_BLOCK) [AddrLine2], DBO.ADDRESSLINE(3, T_1.ADDRESS_BLOCK) [AddrLine3], 
                      DBO.ADDRESSLINE(4, T_1.ADDRESS_BLOCK) [AddrLine4], DBO.ADDRESSLINE(5, T_1.ADDRESS_BLOCK) [AddrLine5], 
                      T_1.CARRIER_ROUTE [CART], T_1.CITY [City], T_2.LONGDESCRIPTION [AddrCountry], T_3.LONGDESCRIPTION [AddrCounty], 
                      RECORDS_CONSTIT_ADDRESS.DATE_FROM [AddrValidFrom], RECORDS_CONSTIT_ADDRESS.DATE_TO [AddrValidTo], T_1.DPC [AddrDPC], 
                      T_4.LONGDESCRIPTION [AddrInfoSrc], T_1.LOT [AddrLOT], T_5.LONGDESCRIPTION [AddrNZCity], T_6.LONGDESCRIPTION [AddrNZSuburb], 
                      CASE RECORDS_CONSTIT_ADDRESS.PREFERRED WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [PrefAddr], 
                      T_7.LONGDESCRIPTION [AddrRegion], 
                      CASE RECORDS_CONSTIT_ADDRESS.SEASONAL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [AddrSeasonal], 
                      RECORDS_CONSTIT_ADDRESS.SEASONAL_FROM [AddrSeasFrom], 
                      RECORDS_CONSTIT_ADDRESS.SEASONAL_TO [AddrSeasTo], 
                      CASE RECORDS_CONSTIT_ADDRESS.SENDMAIL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [AddrSendMail], 
                      T_8.SHORTDESCRIPTION [AddrState], T_9.LONGDESCRIPTION [AddrType], T_1.POST_CODE [AddrZIP], 
               
                      
                      dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'CN' [RE_DB_Tbl] 

FROM         DBO.RECORDS AS RECORDS LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS RECORDS_ETHNICITY ON RECORDS.ETHNICITY = RECORDS_ETHNICITY.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS RECORDS_INCOME ON RECORDS.INCOME = RECORDS_INCOME.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS RECORDS_INDUSTRY ON RECORDS.INDUSTRY = RECORDS_INDUSTRY.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS RECORDS_MARITALSTATUS ON RECORDS.MARITAL_STATUS = RECORDS_MARITALSTATUS.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.PROSPECT AS RECORDS_PROSPECT ON RECORDS.ID = RECORDS_PROSPECT.CONSTIT_ID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS T_0 ON RECORDS_PROSPECT.CLASSIFICATION = T_0.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS RECORDS_PROSPECT_STATUS ON RECORDS_PROSPECT.STATUS = RECORDS_PROSPECT_STATUS.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS RECORDS_RELIGION ON RECORDS.RELIGION = RECORDS_RELIGION.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS RECORDS_SUFFIX_1 ON RECORDS.SUFFIX_1 = RECORDS_SUFFIX_1.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS RECORDS_SUFFIX_2 ON RECORDS.SUFFIX_2 = RECORDS_SUFFIX_2.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS RECORDS_TARGET ON RECORDS.TARGET = RECORDS_TARGET.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS RECORDS_TITLE_1 ON RECORDS.TITLE_1 = RECORDS_TITLE_1.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS RECORDS_TITLE_2 ON RECORDS.TITLE_2 = RECORDS_TITLE_2.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.RECORDS AS PARENT_CORP_RECORDS ON records.parent_corp_ID = PARENT_CORP_RECORDS.id
                     
                      LEFT JOIN DBO.USERS AS RECORDS_AddedBy ON RECORDS.ADDED_BY = RECORDS_AddedBy.USER_ID 
                      LEFT JOIN DBO.USERS AS RECORDS_LastChangedBy ON RECORDS.LAST_CHANGED_BY = RECORDS_LastChangedBy.USER_ID

					--Address tables info.
					LEFT JOIN DBO.QCONSTIT_ADDRESS AS RECORDS_CONSTIT_ADDRESS ON RECORDS.ID = RECORDS_CONSTIT_ADDRESS.CONSTIT_ID 
					LEFT JOIN DBO.ADDRESS AS T_1 ON RECORDS_CONSTIT_ADDRESS.ADDRESS_ID = T_1.ID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON T_1.COUNTRY = T_2.TABLEENTRIESID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_3 ON T_1.COUNTY = T_3.TABLEENTRIESID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_4 ON RECORDS_CONSTIT_ADDRESS.INFO_SOURCE = T_4.TABLEENTRIESID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_5 ON T_1.NZCITY = T_5.TABLEENTRIESID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_6 ON T_1.SUBURB = T_6.TABLEENTRIESID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_7 ON T_1.REGION = T_7.TABLEENTRIESID 
					LEFT OUTER JOIN DBO.STATES AS T_8 ON T_1.STATE = T_8.SHORTDESCRIPTION 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_9 ON RECORDS_CONSTIT_ADDRESS.TYPE = T_9.TABLEENTRIESID
			 
					WHERE ((RECORDS.IS_CONSTITUENT = - 1) and (RECORDS_CONSTIT_ADDRESS.PREFERRED = -1))
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
 
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 16.
CREATE VIEW dbo.HC_Cons_ConsCode_v
AS
SELECT     RECORDS.CONSTITUENT_ID AS ConsID, RECORDS_CONSTITUENT_CODES.IMPORT_ID AS [ConsCodeImpID], RECORDS_CONSTITUENT_CODES.SEQUENCE AS [ConsCodeSequence],
                      CASE WHEN T_1.SHORTDESCRIPTION IS NULL THEN 'NULL' ELSE T_1.SHORTDESCRIPTION  END AS [ConsCode], T_1.LONGDESCRIPTION AS [ConsCodeDesc], 
                      RECORDS.IMPORT_ID AS [ImportID],  
                                            
                      CASE WHEN  ISDATE(RECORDS_CONSTITUENT_CODES.DATE_FROM)=1 AND LEN(RECORDS_CONSTITUENT_CODES.DATE_FROM)=8 THEN CAST(DBO.DATEFORMAT(RECORDS_CONSTITUENT_CODES.DATE_FROM, 'MM/DD/YYYY') as varchar(10))
					  WHEN ISDATE(RECORDS_CONSTITUENT_CODES.DATE_FROM)=1 AND LEN(RECORDS_CONSTITUENT_CODES.DATE_FROM)=6 THEN CAST(RIGHT(RECORDS_CONSTITUENT_CODES.DATE_FROM,2) +'/'+ LEFT(RECORDS_CONSTITUENT_CODES.DATE_FROM,4) AS varchar(10))
					  WHEN ISDATE(RECORDS_CONSTITUENT_CODES.DATE_FROM)=1 AND LEN(RECORDS_CONSTITUENT_CODES.DATE_FROM)=4 THEN cast(RECORDS_CONSTITUENT_CODES.DATE_FROM as varchar(10))
	                  ELSE NULL END AS [ConsCodeDateFrom],
                      
                        
                      CASE WHEN  ISDATE(RECORDS_CONSTITUENT_CODES.DATE_TO)=1 AND LEN(RECORDS_CONSTITUENT_CODES.DATE_TO)=8 THEN CAST(DBO.DATEFORMAT(RECORDS_CONSTITUENT_CODES.DATE_TO, 'MM/DD/YYYY') as varchar(10))
					  WHEN ISDATE(RECORDS_CONSTITUENT_CODES.DATE_TO)=1 AND LEN(RECORDS_CONSTITUENT_CODES.DATE_TO)=6 THEN CAST(RIGHT(RECORDS_CONSTITUENT_CODES.DATE_TO,2) +'/'+ LEFT(RECORDS_CONSTITUENT_CODES.DATE_TO,4) AS varchar(10))
					  WHEN ISDATE(RECORDS_CONSTITUENT_CODES.DATE_TO)=1 AND LEN(RECORDS_CONSTITUENT_CODES.DATE_TO)=4 THEN cast(RECORDS_CONSTITUENT_CODES.DATE_TO as varchar(10))
	                  ELSE NULL END AS [ConsCodeDateTo],
                      
					  dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'CC' [RE_DB_Tbl],
                      row_number() over (order by RECORDS_CONSTITUENT_CODES.IMPORT_ID) as ID

FROM         dbo.RECORDS RECORDS LEFT OUTER JOIN
                      dbo.CONSTITUENT_CODES RECORDS_CONSTITUENT_CODES ON RECORDS.ID = RECORDS_CONSTITUENT_CODES.CONSTIT_ID LEFT OUTER JOIN
                      dbo.TABLEENTRIES T_1 ON RECORDS_CONSTITUENT_CODES.CODE = T_1.TABLEENTRIESID
WHERE     (RECORDS.IS_CONSTITUENT = - 1) --AND (RECORDS_CONSTITUENT_CODES.IMPORT_ID IS NOT NULL)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- VIEW 17.
CREATE view dbo.HC_Cons_Notes_v
AS
SELECT				RECORDS.IMPORT_ID AS [ImportID], RECORDS.CONSTITUENT_ID AS [ConsID], 
					dbo.ConstituentNotepad.Import_Id AS [NoteImpID], 
                    T_2.LONGDESCRIPTION AS [NoteType], 	                
	             
                    CASE WHEN  ISDATE(dbo.ConstituentNotepad.NotepadDate)=1 AND LEN(dbo.ConstituentNotepad.NotepadDate)=8 THEN CAST(DBO.DATEFORMAT(dbo.ConstituentNotepad.NotepadDate, 'MM/DD/YYYY') as varchar(10))
					WHEN ISDATE(dbo.ConstituentNotepad.NotepadDate)=1 AND LEN(dbo.ConstituentNotepad.NotepadDate)=6 THEN CAST(RIGHT(dbo.ConstituentNotepad.NotepadDate,2) +'/'+ LEFT(dbo.ConstituentNotepad.NotepadDate,4) AS varchar(10))
					WHEN ISDATE(dbo.ConstituentNotepad.NotepadDate)=1 AND LEN(dbo.ConstituentNotepad.NotepadDate)=4 THEN cast(dbo.ConstituentNotepad.NotepadDate as varchar(10))
	                ELSE NULL END AS [NoteDate],

                    dbo.ConstituentNotepad.Description AS [NoteDesc], 
                    REPLACE(REPLACE(REPLACE(CAST(dbo.ConstituentNotepad.ActualNotes AS varchar(max)), CHAR(13), ' '), CHAR(10), ' '), CHAR(9), ' ') 
                    AS [NoteNotes], dbo.ConstituentNotepad.Title AS [NoteTitle], dbo.ConstituentNotepad.Author AS [NoteAuth], 
					dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'NP' [RE_DB_Tbl],
                    row_number() over (order by ID) as ID

FROM         dbo.RECORDS RECORDS LEFT OUTER JOIN
                      dbo.ConstituentNotepad ON RECORDS.ID = dbo.ConstituentNotepad.ParentId LEFT OUTER JOIN
                      dbo.TABLEENTRIES T_2 ON dbo.ConstituentNotepad.NoteTypeId = T_2.TABLEENTRIESID
WHERE     (RECORDS.IS_CONSTITUENT = - 1) AND (dbo.ConstituentNotepad.Import_Id IS NOT NULL)



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

 
-- VIEW 18.
CREATE VIEW dbo.HC_Event_Attr_v
AS
SELECT     dbo.special_event.eventid [EventID], dbo.eventattributes.IMPORTID [EVAttrImpID], 
                    dbo.AttributeTypes.DESCRIPTION [EVAttrCat], dbo.eventattributes.COMMENTS [EVAttrCom], 
                   dbo.dateformat(EVENTATTRIBUTES.ATTRIBUTEDATE, 'MM/DD/YYYY') as [EVAttrDate], 
					EVAttrDesc = CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL 
                    THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
					WHEN dbo.eventattributes.boolean IS NOT NULL THEN (CASE dbo.eventattributes.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END)
                    WHEN dbo.eventAttributes.text IS NOT NULL THEN dbo.eventAttributes.text WHEN dbo.eventAttributes.num IS NOT NULL THEN CONVERT(varchar, 
                    dbo.eventAttributes.num) WHEN dbo.eventAttributes.datetime IS NOT NULL THEN CONVERT(varchar, dbo.eventAttributes.datetime) 
                    WHEN dbo.eventAttributes.currency IS NOT NULL THEN CONVERT(varchar, dbo.eventAttributes.currency) 
                    WHEN dbo.eventAttributes.constitid IS NOT NULL THEN CONVERT(varchar, dbo.eventAttributes.constitid) ELSE NULL END, 
                    case dbo.AttributeTypes.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
											   when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
											   when 8 then 'FuzzyDate' end as [DataType], 	
					dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'VA' [RE_DB_Tbl],
                    row_number() over (order by ID) as ID	

FROM         ((dbo.special_event LEFT JOIN
                      dbo.eventattributes ON dbo.special_event.ID = dbo.eventattributes.PARENTID) LEFT JOIN
                      (dbo.AttributeTypes LEFT JOIN
                      dbo.CODETABLES ON dbo.AttributeTypes.CODETABLESID = dbo.CODETABLES.CODETABLESID) ON 
                      dbo.eventattributes.ATTRIBUTETYPESID = dbo.AttributeTypes.ATTRIBUTETYPESID) LEFT JOIN
                      dbo.TABLEENTRIES ON dbo.eventattributes.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.attributeTYPEs AS T_1 ON eventattributes.ATTRIBUTETYPESID = T_1.ATTRIBUTETYPESID
WHERE     dbo.eventattributes.IMPORTID IS NOT NULL

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.HC_Event_Award_v AS
	SELECT 
	 SPECIAL_EVENT.EVENTID EventID,
	 T_2.ImportID EVAwardImpID,
	 T_3.LONGDESCRIPTION EVAward,
	 T_2.PresentedByName EVAwardPresented,
	 T_2.COMMENTS EVAwardCom,
	 T_1.Import_ID EVAwardRecipient,
 	 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'VW' [RE_DB_Tbl],
	 row_number() over (order by T_2.ImportID) as ID
	 
	FROM 
	DBO.SPECIAL_EVENT AS SPECIAL_EVENT 
	INNER JOIN DBO.PARTICIPANTS AS T_1 ON SPECIAL_EVENT.ID = T_1.EVENTID 
	INNER JOIN DBO.EVENTAWARDS AS T_2 ON T_1.ID = T_2.RECIPIENTID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_3 ON T_2.AWARD = T_3.TABLEENTRIESID

	WHERE ((T_2.AWARD IS NOT NULL))  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.HC_Event_Expense_v AS
	SELECT 
	 SPECIAL_EVENT.EVENTID EventID,
	 T_1.ImportID EVExpImpID,
	 T_2.LONGDESCRIPTION EVExpType,
	 T_1.EXPENSEAMOUNT EVExpAmount,
	 T_1.AMOUNTPAID EVExpAmountPaid,
	 T_1.BUDGETEDAMOUNT EVExpBudgetedAmount,
	 T_1.Comments EVExpComments,
	 DBO.BuildFullName3(T_4.LAST_NAME,T_4.FIRST_NAME,T_4.MIDDLE_NAME,T_4.ORG_NAME,T_4.KEY_INDICATOR) EVExpVendorName,
	 dbo.dateformat(T_1.EXPENSEDATE, 'MM/DD/YYYY') EVExpDate,
	 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort] , 'VE' [RE_DB_Tbl],
	  row_number() over (order by T_1.ImportID) as ID
	 
	FROM 
	DBO.SPECIAL_EVENT AS SPECIAL_EVENT 
	INNER JOIN DBO.EVENTEXPENSES AS T_1 ON SPECIAL_EVENT.ID = T_1.EVENTID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON T_1.EXPENSETYPE = T_2.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.PARTICIPANTS AS T_3 ON T_1.VENDOR = T_3.ID 
	LEFT OUTER JOIN DBO.RECORDS AS T_4 ON T_3.RECORDSID = T_4.ID  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.HC_Event_Menu_v AS
	SELECT 
	  T_1.ImportID AS EVMenuItemImpID,
	 SPECIAL_EVENT.EVENTID AS EventID,
	
	 T_3.LONGDESCRIPTION EVMenuItem,
 	 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'VM' [RE_DB_Tbl],
	 row_number() over (order by T_1.ImportID) as ID

	FROM dbo.EventMenu T_1
	INNER JOIN DBO.SPECIAL_EVENT AS SPECIAL_EVENT ON T_1.EventID = SPECIAL_EVENT.ID
	INNER JOIN DBO.TABLEENTRIES AS T_3 ON T_1.MENUITEM = T_3.TABLEENTRIESID

	WHERE ((T_1.MenuItem IS NOT NULL))  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

 CREATE VIEW dbo.HC_Event_Notepad_v AS

	SELECT 
	 SPECIAL_EVENT.EVENTID EventID, T_1.Import_ID EVNoteImpID,
	 T_2.LONGDESCRIPTION EVNoteType, T_1.Description EVNoteDesc,
	 dbo.dateformat(T_1.NotepadDate, 'MM/DD/YYYY') AS EVNoteDate, T_1.Author EVNoteAuthor,
	 T_1.Title EVNoteTitle, T_1.ActualNotes EVNoteNotes,  dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort],  'VN' [RE_DB_Tbl],
	  row_number() over (order by T_1.Import_ID) as ID


	FROM 
	DBO.SPECIAL_EVENT AS SPECIAL_EVENT 
	INNER JOIN DBO.EVENTNOTEPAD AS T_1 ON SPECIAL_EVENT.ID = T_1.PARENTID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON T_1.NOTETYPEID = T_2.TABLEENTRIESID

	WHERE T_1.Import_ID IS NOT NULL
	
	GO



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.HC_Event_Price_v AS

	SELECT 
	 SPECIAL_EVENT.EVENTID EventID,
	 SPECIAL_EVENT_EVENTPRICES.ImportID EVPriceImpID,
	 T_1.LONGDESCRIPTION EVPriceUnit,
	 SPECIAL_EVENT_EVENTPRICES.RECEIPTAMOUNT EVPriceRcptAmt,
	 SPECIAL_EVENT_EVENTPRICES.GIFTAMOUNT EVPriceGiftAmt,
	 SPECIAL_EVENT_EVENTPRICES.COMMENTS EVPriceCom,
	 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'VR' [RE_DB_Tbl],
 	row_number() over (order by SPECIAL_EVENT_EVENTPRICES.ImportID ) as ID


	FROM 
	DBO.SPECIAL_EVENT AS SPECIAL_EVENT 
	INNER JOIN DBO.EVENTPRICES AS SPECIAL_EVENT_EVENTPRICES ON SPECIAL_EVENT.ID = SPECIAL_EVENT_EVENTPRICES.EVENTID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_1 ON SPECIAL_EVENT_EVENTPRICES.UNIT = T_1.TABLEENTRIESID

 
 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.HC_Event_v AS


	SELECT 
	 SPECIAL_EVENT.IMPORT_ID EVImpID,
	 SPECIAL_EVENT.EVENTID EVID,
	 SPECIAL_EVENT.NAME EVName,
	 SPECIAL_EVENT.DESCRIPTION EVDesc,
	 SPECIAL_EVENT.CAPACITY EVCapacity,
	 CASE SPECIAL_EVENT.CATEGORY WHEN 1 THEN 'Sporting Event' WHEN 2 THEN 'Dinner' WHEN 3 THEN 'Class' WHEN 4 THEN 'Other' ELSE '' END  EVCategory,
	 SPECIAL_EVENT.GOAL EVGoal,
	 SPECIAL_EVENT_GROUPID.LONGDESCRIPTION EVGroup,
	 CASE SPECIAL_EVENT.INACTIVE WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  EVInactive,
	 SPECIAL_EVENT.ItineraryDesc EVItinDesc,
	 dbo.dateformat(SPECIAL_EVENT.START_DATE, 'MM/DD/YYYY') EVStartDate, dbo.GetTime(SPECIAL_EVENT.START_TIME) EVStartTime,
	 dbo.dateformat(SPECIAL_EVENT.END_DATE, 'MM/DD/YYYY') EVEndDate,  dbo.GetTime(SPECIAL_EVENT.END_TIME) EVEndTime,
	 special_event_CAMPAIGN.CAMPAIGN_ID EVCampID,
	 SPECIAL_EVENT.LocationName EVLoc, SPECIAL_EVENT.LocationContact EVLocContact, SPECIAL_EVENT.LocationAddress EVLocAddress,
	 SPECIAL_EVENT.LocationCity EVLocCity, T_1.LONGDESCRIPTION EVLocState, SPECIAL_EVENT.LocationPostCode EVLocZIP,
	 T_2.LONGDESCRIPTION EVLocCountry, SPECIAL_EVENT.LocationPhone EVLocPhone, SPECIAL_EVENT.LocationNotes EVLocNotes,
	 SPECIAL_EVENT_TYPEID.LONGDESCRIPTION EVType,
	 
	 DBO.Query_EventCapacity(SPECIAL_EVENT.ID,SPECIAL_EVENT.SeatingNumSections,SPECIAL_EVENT.SeatingNumTables,SPECIAL_EVENT.SeatingNumSeats,2) EVTotalSeatingAvailable,
	 DBO.Query_EventCapacity(SPECIAL_EVENT.ID,SPECIAL_EVENT.SeatingNumSections,SPECIAL_EVENT.SeatingNumTables,SPECIAL_EVENT.SeatingNumSeats,1) EVTotalSeatingCapacity,
	 SPECIAL_EVENT.NUM_INVITED EVNumberInvited,
	 DBO.Query_EventTotals(SPECIAL_EVENT.ID, 3) EVNumberOfParticipants,
	 DBO.Query_EventTotals(SPECIAL_EVENT.ID, 1) EVNumberRegistered,
	 SPECIAL_EVENT.AnnotationText EVAnnotation,
	 CASE SPECIAL_EVENT.DISPLAYONCALENDAR WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  EVDisplayOnCalendar,
	 dbo.hc_OrgName_v() [RE_DB_Owner] , dbo.HC_OrgInit_f() [RE_DB_OwnerShort], SPECIAL_EVENT.ID [EVSysRecID]


	FROM 
	DBO.SPECIAL_EVENT AS SPECIAL_EVENT 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS SPECIAL_EVENT_GROUPID ON SPECIAL_EVENT.GROUPID = SPECIAL_EVENT_GROUPID.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.CAMPAIGN AS special_event_CAMPAIGN ON SPECIAL_EVENT.CAMPAIGN_ID = special_event_CAMPAIGN.ID 
	LEFT OUTER JOIN DBO.STATES AS T_1 ON SPECIAL_EVENT.LOCATIONSTATE = T_1.SHORTDESCRIPTION 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON SPECIAL_EVENT.LOCATIONCOUNTRY = T_2.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS SPECIAL_EVENT_TYPEID ON SPECIAL_EVENT.TYPEID = SPECIAL_EVENT_TYPEID.TABLEENTRIESID  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

	CREATE VIEW dbo.HC_Event_Participant_v 
	AS
	SELECT 
	 Participants.Import_ID REGImpID,
	 CASE Participants.Type WHEN 1 THEN 'Registrant' WHEN 2 THEN 'Guest' WHEN 3 THEN 'Sponsor' ELSE '' END  AS REGType ,
	 Participants_Status.LONGDESCRIPTION AS REGStatus , 
	 DBO.ParticipantPaidTotal(Participants.ID,1) AS REGAmtPaid,
	 dbo.dateformat(DBO.ParticipantDatePaid(Participants.ID), 'MM/DD/YYYY') AS REGDatePaid,
	 
	 Participants.Grade AS REGGrade,
	 
	 CASE Participants.Attended WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END AS REGAttended, 
	 PARTICIPANTS_RECORDS.KEY_INDICATOR REGKeyInd ,

	 CASE WHEN (PARTICIPANTS_RECORDS.IS_CONSTITUENT = -1) THEN 'TRUE' ELSE 'FALSE' END AS REGConstituent,
	 CASE WHEN (PARTICIPANTS_RECORDS.IS_CONSTITUENT = -1) THEN PARTICIPANTS_RECORDS.CONSTITUENT_ID ELSE NULL END AS ConsID, 
	 CASE WHEN (PARTICIPANTS_RECORDS.IS_CONSTITUENT = -1) THEN PARTICIPANTS_RECORDS.IMPORT_ID ELSE NULL END AS ImportId,
	 T_9.LONGDESCRIPTION AS REGTitl1 ,
	 T_10.LONGDESCRIPTION REGTitl2 ,
	 Participants_SearchName.FIRST_NAME AS REGFirstName, 
	 PARTICIPANTS_RECORDS.LAST_NAME AS REGLastName,
	 Participants_SearchName.MIDDLE_NAME AS REGMiddleName,
	 T_7.LONGDESCRIPTION AS REGSuff1,
	 T_8.LONGDESCRIPTION AS REGSuff2,
	 PARTICIPANTS_RECORDS.ORG_NAME AS REGOrgName ,

	 
	 Participants.Contact AS REGContact, 
	 CASE Participants.IsCoordinator WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  AS REGCoord,
	 CASE Participants.DoNotSeat WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  AS REGDoNotSeat,
	 T_1.EVENTID EventID, DBO.GetParticipantGroupName(Participants.ID) REGroup, Participants.GroupNote AS REGroupNotes,
	 
	 DBO.BuildFullName3(T_3.LAST_NAME,T_3.FIRST_NAME,T_3.MIDDLE_NAME,T_3.ORG_NAME,T_3.KEY_INDICATOR) AS REGGuestOf,
	 T_2.Import_ID GuestOfREGImpID,
	 CASE Participants.IsInstructor WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  AS REGInstructor ,
	 CASE Participants.Invitation WHEN 1 THEN 'Do not invite' WHEN 2 THEN 'Not invited' WHEN 3 THEN 'Invited' ELSE '' END  AS REGInvite,
	 CASE Participants.InviteDate when isdate(Participants.InviteDate) then dbo.DATEFORMAT(Participants.InviteDate, 'MM/DD/YYYY') else null end AS REGInviteDate ,

	  Participants.Notes AS REGNotes ,
	 
	 T_4.LONGDESCRIPTION AS REGParticipation,
	 CASE Participants.Registration WHEN 1 THEN 'Do not register' WHEN 2 THEN 'Not registered' WHEN 3 THEN 'Registered' ELSE '' END  AS REGRegistration ,
	 case participants.RegistrationDate when ISDATE(participants.RegistrationDate) then dbo.DATEFORMAT(Participants.RegistrationDate, 'MM/DD/YYYY') else null end AS REGRegDate ,
	 participants_RESPONSE.LONGDESCRIPTION AS REGResponse ,
	 case Participants.ResponseDate when isdate(Participants.ResponseDate) then dbo.DATEFORMAT(Participants.ResponseDate, 'MM/DD/YYYY') else null end AS  REGRespDate ,
	 
	 DBO.BuildFullName3(PARTICIPANTS_SOLICITOR.LAST_NAME,PARTICIPANTS_SOLICITOR.FIRST_NAME,PARTICIPANTS_SOLICITOR.MIDDLE_NAME,PARTICIPANTS_SOLICITOR.ORG_NAME,PARTICIPANTS_SOLICITOR.KEY_INDICATOR) AS REGSolicitor ,
	 PARTICIPANTS_SOLICITOR.IMPORT_ID AS REGSolicitorImpID, 
	 
	 CASE Participants.IsSpeaker WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  AS REGSpeaker ,
	 Participants.SpeakComments AS REGSpeakCom ,
	 Participants.SpeakTime AS REGSpeakTime ,
	 Participants.SpeakTopic AS REGSpeakTopic ,
	 
	 DBO.BuildFullName3(T_6.LAST_NAME,T_6.FIRST_NAME,T_6.MIDDLE_NAME,T_6.ORG_NAME,T_6.KEY_INDICATOR) AS REGSponsor ,
	 T_5.IMPORT_ID REGSponsorImpID, 
	 
	 Participants.SportAbility as REGAbility , Participants.SportAge  as REGAge , Participants.SportNumber as REGAssignedNum , 
	 Participants.SportDivision as REGDivision , 
	 case Participants.SportGender when 1 then 'Male' when 2 then 'Female' when 3 then 'Unknown' else null end as REGGender , 
	 Participants.SportHeight as REGHeight , Participants.SportLocation as REGLocation , Participants.SportPlace as REGPlace,
	 Participants.SportTeam as REGTeam, Participants.SportTime as REGTime, Participants.SportWeight as REGWeight,
	   
	 
	 
	 CASE Participants.IsVendor WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  AS REGVendor,
	 Participants.VendBooth AS REGVendorBooth ,
	 Participants.VendComments AS REGVendorCom ,
	 Participants_VendPurpose.LONGDESCRIPTION AS REGVendorPurpose ,
	 Participants_VendRating.LONGDESCRIPTION AS REGVendorRating ,
	 Participants.VendSpace AS REGVendorSpace ,
	 CASE Participants.IsVolunteer WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  AS REGVol ,
	 PArticipants.WaiveBenefits AS REGWaiveBen, 
	 
	 T_11.IMPORT_ID AS REGAddrImpID, T_12.ADDRESS_BLOCK AS REGAddrLines,
	 T_12.CITY AS REGAddrCity,
	 T_13.SHORTDESCRIPTION AS REGAddrState, T_12.POST_CODE REGAddrZIP,
	 T_14.LONGDESCRIPTION AS REGAddrCounty ,
	 T_15.LONGDESCRIPTION AS REGAddrCountry,
	 T_16.LONGDESCRIPTION AS REGAddrType,
	 T_17.LONGDESCRIPTION AS REGAddrInfoSource,
	 case T_11.DATE_FROM when isdate(T_11.DATE_FROM) then dbo.DATEFORMAT(T_11.DATE_FROM,'MM/DD/YYYY') else null end AS REGAddrValidFrom,
	 case T_11.DATE_TO   when isdate(T_11.DATE_TO) then dbo.DATEFORMAT(T_11.DATE_TO,'MM/DD/YYYY') else null end AS REGAddrValidTo,
	 CASE T_11.SENDMAIL WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  REGAddrSendMail,
	 CASE T_19.ISSEASONAL WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  AS REGAddrSeasonal,
	 T_11.SEASONAL_FROM AS REGAddrSeasonalFrom,
	 T_11.SEASONAL_TO AS REGAddrSeasonalTo, dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort],   'VP' [RE_DB_Tbl],
	 row_number() over (order by Participants.Type, Participants.Import_ID) as ID
	  
	FROM 
	DBO.PARTICIPANTS AS Participants 
	INNER JOIN DBO.RECORDS AS PARTICIPANTS_RECORDS ON Participants.RECORDSID = PARTICIPANTS_RECORDS.ID 
	LEFT OUTER JOIN DBO.SPECIAL_EVENT AS T_1 ON Participants.EVENTID = T_1.ID 
	INNER JOIN DBO.SEARCHNAME AS Participants_SearchName ON Participants.RECORDSID = Participants_SearchName.RECORDS_ID 
	LEFT OUTER JOIN DBO.PARTICIPANTS AS T_2 ON Participants.GUESTOF = T_2.ID 
	LEFT OUTER JOIN DBO.RECORDS AS T_3 ON T_2.RECORDSID = T_3.ID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_4 ON Participants.PARTICIPATION = T_4.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS participants_RESPONSE ON Participants.RESPONSE = participants_RESPONSE.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.RECORDS AS PARTICIPANTS_SOLICITOR ON Participants.SOLICITOR = PARTICIPANTS_SOLICITOR.ID 
	LEFT OUTER JOIN DBO.PARTICIPANTS AS T_5 ON Participants.SPONSOR = T_5.ID 
	LEFT OUTER JOIN DBO.RECORDS AS T_6 ON T_5.RECORDSID = T_6.ID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS Participants_Status ON Participants.STATUS = Participants_Status.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_7 ON PARTICIPANTS_RECORDS.SUFFIX_1 = T_7.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_8 ON PARTICIPANTS_RECORDS.SUFFIX_2 = T_8.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_9 ON PARTICIPANTS_RECORDS.TITLE_1 = T_9.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_10 ON PARTICIPANTS_RECORDS.TITLE_2 = T_10.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS Participants_VendPurpose ON Participants.VENDPURPOSE = Participants_VendPurpose.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS Participants_VendRating ON Participants.VENDRATING = Participants_VendRating.TABLEENTRIESID 

	LEFT OUTER JOIN DBO.CAPREFERRED AS T_11 ON PARTICIPANTS_RECORDS.ID = T_11.CONSTIT_ID 
	LEFT OUTER JOIN DBO.ADDRESS AS T_12 ON T_11.ADDRESS_ID = T_12.ID 
 
	LEFT OUTER JOIN DBO.STATES AS T_13 ON T_12.STATE = T_13.SHORTDESCRIPTION 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_14 ON T_12.COUNTY = T_14.TABLEENTRIESID
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_15 ON T_12.COUNTRY = T_15.TABLEENTRIESID  
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_16 ON T_11.TYPE = T_16.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_17 ON T_11.INFO_SOURCE = T_17.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.SEASONAL AS T_19 ON T_11.ID = T_19.ID



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.HC_Event_Registration_Fees_v 
AS
SELECT 
	T_1.Comments AS REGFeeCom,
	REGFeeDate= case when len(T_1.FeeDate) =8 then dbo.dateformat(T_1.FeeDate,'MM/DD/YYYY')  
				when len(T_1.FeeDate) =6 then right(T_1.FeeDate,2) +'/'+ left(T_1.FeeDate,4) 
				when len(T_1.FeeDate) =4 then left(T_1.FeeDate,4)else null end,
	T_1.GiftAmount REGFeeGiftAmt,
	T_1.NumUnits AS REGFeeNumUnits,
	PARTICIPANTS.Import_ID AS REGImpID,
	T_1.ReceiptAmount AS REGFeeRcptAmt,
	T_3.LONGDESCRIPTION AS REGFeeUnit,
	CASE (CASE WHEN T_5.ID is null THEN 0 ELSE -1 END ) WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE null END  REGLinkedToGift,
	T_5.Amount REGFeeAppliedAmt,
	T_5.Import_ID GFImpID,
	T_2.ImportID EVPriceImpID,
	DBO.ParticipantPaidTotal(PARTICIPANTS.ID,1) REGFeeAmtPaid,
	dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'VF' [RE_DB_Tbl],
	row_number() over (order by Participants.Import_ID) as ID

 
	FROM 
	DBO.PARTICIPANTS AS PARTICIPANTS 
	INNER JOIN DBO.PARTICIPANTFEES AS T_1 ON PARTICIPANTS.ID = T_1.ParticipantsID 
	INNER JOIN DBO.EVENTPRICES AS T_2 ON T_1.EventPricesID = T_2.ID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_3 ON T_2.UNIT = T_3.TABLEENTRIESID
	LEFT OUTER JOIN DBO.EVENT_REGISTRATIONFEES AS T_4 ON PARTICIPANTS.ID = T_4.PARTICIPANTSID 
	LEFT OUTER JOIN DBO.GIFT AS T_5 ON T_4.GIFTID = T_5.ID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
 

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
 
CREATE VIEW [dbo].[HC_Event_Participant_Attr_v]
AS 

SELECT 

PARTICIPANTS.Import_ID REGImpID, PARTICIPANTATTRIBUTES.IMPORTID REGAttrImpID ,
ATTRIBUTETYPES.DESCRIPTION REGAttrCat,
PARTICIPANTATTRIBUTES.COMMENTS REGAttrCom,
dbo.dateformat(PARTICIPANTATTRIBUTES.ATTRIBUTEDATE, 'MM/DD/YYYY') AS REGAttrDate,
REGAttrDesc = CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL 
		THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
		WHEN dbo.PARTICIPANTATTRIBUTES.boolean IS NOT NULL THEN (CASE dbo.PARTICIPANTATTRIBUTES.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END) --CONVERT(varchar, dbo.actionattributes.boolean) 
		WHEN dbo.PARTICIPANTATTRIBUTES.text IS NOT NULL THEN dbo.PARTICIPANTATTRIBUTES.text 
		WHEN dbo.PARTICIPANTATTRIBUTES.num IS NOT NULL  THEN CONVERT(varchar, dbo.PARTICIPANTATTRIBUTES.num) 
		WHEN dbo.PARTICIPANTATTRIBUTES.datetime IS NOT NULL THEN CONVERT(varchar, dbo.PARTICIPANTATTRIBUTES.datetime) 
		WHEN dbo.PARTICIPANTATTRIBUTES.currency IS NOT NULL THEN CONVERT(varchar, dbo.PARTICIPANTATTRIBUTES.currency) 
		WHEN dbo.PARTICIPANTATTRIBUTES.constitid IS NOT NULL THEN CONVERT(varchar, dbo.PARTICIPANTATTRIBUTES.constitid) ELSE NULL END, 
		case dbo.AttributeTypes.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
											   when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
											   when 8 then 'FuzzyDate' end as [DataType], 	
		dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'VB' [RE_DB_Tbl],
		row_number() over (order by PARTICIPANTS.Import_ID) as ID
		
FROM	((dbo.PARTICIPANTS LEFT JOIN
		dbo.PARTICIPANTATTRIBUTES ON dbo.PARTICIPANTS.ID = dbo.PARTICIPANTATTRIBUTES.PARENTID) LEFT JOIN
		(dbo.AttributeTypes LEFT JOIN
		dbo.CODETABLES ON dbo.AttributeTypes.CODETABLESID = dbo.CODETABLES.CODETABLESID) ON 
		dbo.PARTICIPANTATTRIBUTES.ATTRIBUTETYPESID = dbo.AttributeTypes.ATTRIBUTETYPESID) LEFT JOIN
		dbo.TABLEENTRIES ON dbo.PARTICIPANTATTRIBUTES.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID LEFT OUTER JOIN
		dbo.attributeTYPEs AS T_1 ON PARTICIPANTATTRIBUTES.ATTRIBUTETYPESID = T_1.ATTRIBUTETYPESID

WHERE     dbo.PARTICIPANTATTRIBUTES.IMPORTID IS NOT NULL

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO





SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.HC_Gift_MatchingLink_v
AS

SELECT 
 GIFT.IMPORT_ID GFLink,
 CASE GIFT.TYPE WHEN 1 THEN 'Cash' WHEN 2 THEN 'Pay-Cash' WHEN 3 THEN 'MG Pay-Cash' WHEN 8 THEN 'Pledge' WHEN 9 THEN 'Stock/Property' WHEN 10 THEN 'Stock/Property (Sold)' WHEN 11 THEN 'Pay-Stock/Property' WHEN 12 THEN 'MG Pay-Stock/Property' WHEN 13 THEN 'Pay-Stock/Property (Sold)' WHEN 14 THEN 'MG Pay-Stock/Property (Sold)' WHEN 15 THEN 'Gift-in-Kind' WHEN 16 THEN 'Pay-Gift-in-Kind' WHEN 17 THEN 'MG Pay-Gift-in-Kind' WHEN 18 THEN 'Other' WHEN 19 THEN 'Pay-Other' WHEN 20 THEN 'MG Pay-Other' WHEN 21 THEN 'Write Off' WHEN 22 THEN 'MG Write Off' WHEN 27 THEN 'MG Pledge' WHEN 30 THEN 'Recurring Gift' WHEN 31 THEN 'Recurring Gift Pay-Cash' WHEN 32 THEN 'GL Reversal' WHEN 33 THEN 'Amendment' WHEN 34 THEN 'Planned Gift' ELSE '' END  GFType,
 T_2.IMPORT_ID MatchedBy_ImportID,
 DBO.BuildFullName3(T_2.LAST_NAME,T_2.FIRST_NAME,T_2.MIDDLE_NAME,T_2.ORG_NAME,T_2.KEY_INDICATOR) MatchedBy_OrgName,
 T_1.IMPORT_ID Matching_GFImpID,
 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort],
 row_number() over (order by GIFT.IMPORT_ID) as ID
FROM 
DBO.GIFT AS GIFT 
INNER JOIN DBO.GIFTMATCHINGGIFT AS GIFT_GIFTMATCHINGGIFT ON GIFT.ID = GIFT_GIFTMATCHINGGIFT.GIFTID 
INNER JOIN DBO.GIFT T_1 ON GIFT_GIFTMATCHINGGIFT.MATCHINGGIFTID = T_1.ID 
LEFT OUTER JOIN DBO.RECORDS AS T_2 ON T_1.CONSTIT_ID = T_2.ID

WHERE 
(((T_1.IMPORT_ID IS NOT NULL)) AND ((GIFT.TYPE NOT IN ('28','32','33') OR (GIFT.TYPE IS NULL))))  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO







-- VIEW 19.
CREATE VIEW dbo.HC_Gift_Main
AS
SELECT     GIFT.IMPORT_ID [GFImpID],  
					  CONVERT(CHAR(255), DBO.GetGiftFunds(GIFT.ID, GIFT.TYPE, 1)) [FundID], 
					  GIFT.BATCH_NUMBER, GIFT.DTE [GFDate], 
					   
                      CASE GIFT.TYPE WHEN 1 THEN 'Cash' WHEN 2 THEN 'Pay-Cash' WHEN 3 THEN 'MG Pay-Cash' WHEN 8 THEN 'Pledge' WHEN 9 THEN 'Stock/Property'
                       WHEN 10 THEN 'Stock/Property (Sold)' WHEN 11 THEN 'Pay-Stock/Property' WHEN 12 THEN 'MG Pay-Stock/Property' WHEN 13 THEN 'Pay-Stock/Property (Sold)'
                       WHEN 14 THEN 'MG Pay-Stock/Property (Sold)' WHEN 15 THEN 'Gift-in-Kind' WHEN 16 THEN 'Pay-Gift-in-Kind' WHEN 17 THEN 'MG Pay-Gift-in-Kind' WHEN
                       18 THEN 'Other' WHEN 19 THEN 'Pay-Other' WHEN 20 THEN 'MG Pay-Other' WHEN 21 THEN 'Write Off' WHEN 22 THEN 'MG Write Off' WHEN 27 THEN
                       'MG Pledge' WHEN 30 THEN 'Recurring Gift' WHEN 31 THEN 'Recurring Gift Pay-Cash' WHEN 32 THEN 'GL Reversal' WHEN 33 THEN 'Amendment' 
                       WHEN 34 THEN 'Planned Gift'  ELSE null END [GFType], GIFT.Amount [GFTAmt], 
                       
                       CASE GIFT.TYPE WHEN 8 THEN dbo.Gift_GetPledgeBalance(GIFT.ID, GIFT.Amount, '','','','') 
									  WHEN 27 THEN dbo.Gift_GetPledgeBalance(GIFT.ID, GIFT.Amount, '','','','') ELSE null END AS GFPledgeBalance, 
                      
                      CASE GIFT.ACKNOWLEDGE_FLAG WHEN 1 THEN 'Acknowledged' WHEN 2 THEN 'Not Acknowledged' WHEN 3 THEN 'Do Not Acknowledge' ELSE '' END
                       [GFAck], 
                      CASE WHEN  ISDATE(GIFT.AcknowledgeDate)=1 AND LEN(GIFT.AcknowledgeDate)=8 THEN CAST(DBO.DATEFORMAT(GIFT.AcknowledgeDate, 'MM/DD/YYYY') as varchar(10))
						WHEN ISDATE(GIFT.AcknowledgeDate)=1 AND LEN(GIFT.AcknowledgeDate)=6 THEN CAST(RIGHT(GIFT.AcknowledgeDate,2) +'/'+ LEFT(GIFT.AcknowledgeDate,4) AS varchar(10))
						WHEN ISDATE(GIFT.AcknowledgeDate)=1 AND LEN(GIFT.AcknowledgeDate)=4 THEN cast(GIFT.AcknowledgeDate as varchar(10))
					  ELSE NULL END AS [GFAckDate], 
                       GIFT.AmountBills [GFAmtBills], 
                      GIFT.AmountCoins [GFAmtCoins], 
                      CASE GIFT.ANONYMOUS WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [GFAnon], 
					  CONVERT(CHAR(255), DBO.GetGiftAppeals(GIFT.ID, GIFT.TYPE, 1)) [GFAppeal], 
					  GIFT.AUTHORIZATION_CODE [GFAuthCode], 
                      GIFT_Constituent_Bank.IMPORT_ID [BAImpID], GIFT.BenefitsNotes [GFBenNote], 
                      GIFT.CardholderName [GFCardholderName], 
                      CONVERT(CHAR(255), DBO.GetGiftCampaigns(GIFT.ID, GIFT.TYPE, 1)) [CampID], 
                      CASE WHEN  ISDATE(GIFT.CHECK_DATE)=1 AND LEN(GIFT.CHECK_DATE)=8 THEN CAST(DBO.DATEFORMAT(GIFT.CHECK_DATE, 'MM/DD/YYYY') as varchar(10))
						WHEN ISDATE(GIFT.CHECK_DATE)=1 AND LEN(GIFT.CHECK_DATE)=6 THEN CAST(RIGHT(GIFT.CHECK_DATE,2) +'/'+ LEFT(GIFT.CHECK_DATE,4) AS varchar(10))
						WHEN ISDATE(GIFT.CHECK_DATE)=1 AND LEN(GIFT.CHECK_DATE)=4 THEN cast(GIFT.CHECK_DATE as varchar(10))
					  ELSE NULL END AS [GFCheckDate], 
                      GIFT_CONSTITUENCY.shortdescription [GFCons], GIFT_CONSTITUENCY.LONGDESCRIPTION [GFConsDesc], 
                      GIFT.CHECK_NUMBER [GFCheckNum], GIFT_RECORDS.CONSTITUENT_ID [ConsID], 
                      GIFT_RECORDS.IMPORT_ID [ImportID], GIFT.EXPIRES_ON [GFCCExpOn], 
                      GIFT.CREDIT_CARD_NUMBER [GFCCNum], GIFT_CreditType.longdescription [GFCCType], 
                      CASE GIFT.ProcessedByEFT WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [GFEFT], 
                      GIFT_GiftCode.longdescription [GFGiftCode], GIFT.CURRENCY_AMOUNT [GFCurAmt], 
                      t_3.shortdescription [GFCurCntry], GIFT.CURRENCY_EXCHANGE_RATE [GFCurXRate], 
                      GIFT.CURRENCY_RECEIPT_AMOUNT [GFCurRcpt], GIFT.UserGiftId [GiftID], GIFT.POST_DATE [GFPostDate], 
                      CASE GIFT.POST_STATUS WHEN 1 THEN 'Posted' WHEN 2 THEN 'Not Posted' WHEN 3 THEN 'Do Not Post' ELSE '' END [GFPostStat],
           		      CASE GIFT.BENEFITSINCLUDENOTES WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [GFIncludeNotes],
                      GIFT_LETTER_CODE.longdescription [GFLtrCode], 
                      CONVERT(CHAR(255), DBO.GetGiftPackages(GIFT.ID, GIFT.TYPE, 1)) [PackageID], 
                      CASE GIFT.PAYMENT_TYPE WHEN 1 THEN 'Cash' WHEN 2 THEN 'Personal Check' WHEN 3 THEN 'Business Check' WHEN 4 THEN 'Credit Card' WHEN
                       6 THEN 'Direct Debit' WHEN 8 THEN 'Other' ELSE '' END [GFPayMeth], 
                      GIFT.Send_Prenote_Units [GFPreNotifyUnits], 
                      CASE GIFT.RECEIPT_FLAG WHEN 1 THEN 'Receipted' WHEN 2 THEN 'Not Receipted' WHEN 3 THEN 'Do Not Receipt' ELSE '' END [Receipt],
                       GIFT.RECEIPT_AMOUNT [GFRcptAmt], 
                      CASE WHEN  ISDATE(GIFT.ReceiptDate)=1 AND LEN(GIFT.ReceiptDate)=8 THEN CAST(DBO.DATEFORMAT(GIFT.ReceiptDate, 'MM/DD/YYYY') as varchar(10))
						WHEN ISDATE(GIFT.ReceiptDate)=1 AND LEN(GIFT.ReceiptDate)=6 THEN CAST(RIGHT(GIFT.ReceiptDate,2) +'/'+ LEFT(GIFT.ReceiptDate,4) AS varchar(10))
						WHEN ISDATE(GIFT.ReceiptDate)=1 AND LEN(GIFT.ReceiptDate)=4 THEN cast(GIFT.ReceiptDate as varchar(10))
					  ELSE NULL END AS [GFRcptDate],                      GIFT.RECEIPT_NUMBER [GFRcptNum], 
					  CASE GIFT.RECEIVED WHEN 0 THEN 'False' WHEN - 1 THEN 'True' WHEN 1 THEN 'True'ELSE '' END [GFReceived],
					  replace(replace(replace(GIFT.REF, char(13), ' '), 
                      char(10), ' '), char(9), ' ') [GFRef], 
                      CASE WHEN  ISDATE(GIFT.REFERENCE_DATE)=1 AND LEN(GIFT.REFERENCE_DATE)=8 THEN CAST(DBO.DATEFORMAT(GIFT.REFERENCE_DATE, 'MM/DD/YYYY') as varchar(10))
						WHEN ISDATE(GIFT.REFERENCE_DATE)=1 AND LEN(GIFT.REFERENCE_DATE)=6 THEN CAST(RIGHT(GIFT.REFERENCE_DATE,2) +'/'+ LEFT(GIFT.REFERENCE_DATE,4) AS varchar(10))
						WHEN ISDATE(GIFT.REFERENCE_DATE)=1 AND LEN(GIFT.REFERENCE_DATE)=4 THEN cast(GIFT.REFERENCE_DATE as varchar(10))
					  ELSE NULL END AS [GFRefDate],
                      GIFT.REFERENCE_NUMBER [GFRefNum], 
                      CASE GIFT.REMIND_FLAG WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [GFSendRem], 
					  CASE GIFT.SENDPRENOTIFICATION WHEN 0 THEN 'False' WHEN - 1 THEN 'True' WHEN 1 THEN 'True' ELSE '' END  [GFPreNotify],                    
                      CASE GIFT.GIFT_STATUS WHEN 1 THEN 'Active' WHEN 2 THEN 'Held' WHEN 3 THEN 'Terminated' WHEN 4 THEN 'Completed' ELSE '' END [GFStatus],
                       GIFT.GiftStatusDate [GFStatusDate], GIFT.Issuer [GFStkIss], 
                      GIFT.IssuerMedianPrice [GFStkMedPrice], GIFT.IssuerNumUnits [GFStkNumUnits], 
                      GIFT.STOCK_SALE_VALUE [GFStkSaleAmt], GIFT.BROKER_FEE [GFStkSaleBrokerFee], 
                      GIFT.STOCK_SALE_DATE [GFStkSaleDate], GIFT.STOCK_SALE_COMMENT [GFStkSaleNotes], 
                      GIFT.SALE_OF_STOCK_POST_DATE [GFStkSalePostDate], 
                      CASE GIFT.SALE_OF_STOCK_POST_STATUS WHEN 1 THEN 'Posted' WHEN 2 THEN 'Not Posted' WHEN 3 THEN 'Do Not Post' ELSE '' END [GFStkSalePostStat],
                       GIFT.IssuerSymbol [GFStkSymbol], GIFT_GiftSubType.longdescription [GFSubType], 
                      GIFT.SCHEDULE_MONTHLYDAYOFWEEK [GFInsDayName], 
                      CASE GIFT.TYPE WHEN 30 THEN NULL ELSE DBO.Query_GiftInstallmentEndingOn(GIFT.ID, GIFT.TYPE, GIFT.Schedule_EndDate) END AS [GFInsEndDate], 
					  DBO.Query_GetFrequency(GIFT.TYPE, GIFT.ID, GIFT.INSTALLMENT_FREQUENCY) [GFInsFreq], 
                      gift.schedule_spacing [GFInsFreqNum], 
                      CASE GIFT.SCHEDULE_MONTHLYTYPE WHEN 1 THEN 'Specific Day' WHEN 2 THEN 'User Defined Day' ELSE '' END [GFInsFreqOpt],
                       CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 6 THEN 'True' ELSE 'False' END [GFInsFri], 
                      CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 2 THEN 'True' ELSE 'False' END [GFInsMon], 
                      GIFT.SCHEDULE_MONTH [GFInsMonthName], GIFT.NUMBER_OF_INSTALLMENTS [GFInsNumPay], 
                      GIFT.SCHEDULE_SPACING [GFInsWeekFreq], GIFT.SCHEDULE_DAYOFMONTH [GFInsNumDay], 
                      GIFT.SCHEDULE_DAYOFMONTH2 [GFInsNumDay2], GIFT.SCHEDULE_MONTHLYORDINAL [GFInsOrdDay], 
                      GIFT.SCHEDULE_SMDAYTYPE1 [GFInsOrdDay1], GIFT.SCHEDULE_SMDAYTYPE2 [GFInsOrdDay2], 
                      CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 7 THEN 'True' ELSE 'False' END [GFInsSat], 
                      GIFT.DATE_1ST_PAY [GFInsStartDate], 
                      CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 1 THEN 'True' ELSE 'False' END [GFInsSun], 
                      CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 5 THEN 'True' ELSE 'False' END [GFInsThu], 
                      CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 3 THEN 'True' ELSE 'False' END [GFInsTue], 
                      CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 4 THEN 'True' ELSE 'False' END [GFInsWed],
		  			  
					  CASE GIFT.INSTALLMENT_FREQUENCY  
								WHEN '1' THEN (CASE WHEN (GIFT.NUMBER_OF_INSTALLMENTS/1)=0 THEN '1' ELSE CEILING(GIFT.NUMBER_OF_INSTALLMENTS/1) END)		--Annually
								WHEN '2' THEN (CASE WHEN (GIFT.NUMBER_OF_INSTALLMENTS/2)=0 THEN '1' ELSE CEILING(GIFT.NUMBER_OF_INSTALLMENTS/2) END)		--Semi-Annually	
								WHEN '3' THEN (CASE WHEN (GIFT.NUMBER_OF_INSTALLMENTS/4)=0 THEN '1' ELSE CEILING(GIFT.NUMBER_OF_INSTALLMENTS/4) END)		--Quarterly
								WHEN '4' THEN (CASE WHEN (GIFT.NUMBER_OF_INSTALLMENTS/6)=0 THEN '1' ELSE CEILING(GIFT.NUMBER_OF_INSTALLMENTS/6) END)		--Bimothly	(every 2 months)
								WHEN '5' THEN (CASE WHEN (GIFT.NUMBER_OF_INSTALLMENTS/12)=0 THEN '1' ELSE CEILING(GIFT.NUMBER_OF_INSTALLMENTS/12) END)	--Monthly	Monthly
								WHEN '6' THEN (CASE WHEN (GIFT.NUMBER_OF_INSTALLMENTS/24)=0 THEN '1' ELSE CEILING(GIFT.NUMBER_OF_INSTALLMENTS/24) END)	--Semi-Monthly	Semi-Monthly
								WHEN '7' THEN (CASE WHEN (GIFT.NUMBER_OF_INSTALLMENTS/26)=0 THEN '1' ELSE CEILING(GIFT.NUMBER_OF_INSTALLMENTS/26) END)	--Biweekly	Bi-Weekly
								WHEN '8' THEN (CASE WHEN (GIFT.NUMBER_OF_INSTALLMENTS/52)=0 THEN '1' ELSE CEILING(GIFT.NUMBER_OF_INSTALLMENTS/52) END)	--Weekly	Weekly
								--WHEN '9' --Irregular	Irregular
								--WHEN '10' --Single Installment
						ELSE '1' END AS GFInsYears,

					
                      --plannedGifts 
							 CAST(GIFT.PlannedGiftID  as varchar(50))AS GFPlannedGiftID,
							 CAST(GIFT_GIFT2FIELDS.InterestRate  as varchar(50))AS GFDiscountRt,
							 CAST(GIFT_GIFT2FIELDS.ExpectedMaturityYear  as varchar(50))AS GFMaturityYr,
							 CASE GIFT_GIFT2FIELDS.FlexibleDeferred WHEN 0 THEN 'No' WHEN -1 THEN 'Yes' ELSE '' END  GFFlexibleDef,
							 CASE GIFT_GIFT2FIELDS.Realized WHEN 0 THEN 'No' WHEN -1 THEN 'Yes' ELSE '' END  AS GFTRealized,
							 CASE GIFT_GIFT2FIELDS.Vehicle WHEN 1 THEN 'Bequest' WHEN 2 THEN 'Gift Annuity' 
							 WHEN 3 THEN 'Lead Annuity Trust' WHEN 4 THEN 'Remainder Annuity Trust' WHEN 5 THEN 'Lead Unitrust' 
							 WHEN 6 THEN 'Remainder Unitrust' WHEN 7 THEN 'Pooled Income Fund' WHEN 8 THEN 'Retained Life Estate' 
							 WHEN 9 THEN 'Other Planned Gift' WHEN 10 THEN 'Life Insurance' ELSE '' END  AS GFVehicle,
							 cast(GIFT_GIFT2FIELDS.PIFNumUnits as varchar(50)) AS GFNumUnits,
							 CAST(GIFT_GIFT2FIELDS.PayoutAmount as varchar(50)) AS GFPayoutAmt,
							 CAST(GIFT_GIFT2FIELDS.PayoutPercent  as varchar(50))AS GFPayoutPct,
							 T_11.LONGDESCRIPTION AS GFPIFName,
							 T_12.LONGDESCRIPTION AS GFPGStatus,
							 cast(GIFT_GIFT2FIELDS.RemainderAsOf as varchar(10)) AS GFRemainderAsOfDate,
							 CAST(GIFT_GIFT2FIELDS.RemainderValue  as varchar(50))AS GFRemainderValue,
							 T_13.LONGDESCRIPTION AS GFPPolicyType,
							 CAST(GIFT_GIFT2FIELDS.PolicyNumber  as varchar(50))AS GFPPolicyNum,
							 CAST(GIFT_GIFT2FIELDS.PolicyFaceAmount  as varchar(50))AS GFPPolicyFaceAmt,
							 CAST(GIFT_GIFT2FIELDS.PayoutAmount  as varchar(50))AS GFPPolicyPremium,
							 cast(GIFT_GIFT2FIELDS.NetPresentValue as varchar(100))  AS GFNetPresentValue,
							 cast(GIFT_GIFT2FIELDS.NPVAsOf as varchar(10)) AS GFNetPresentValueAsOf,
							 CASE GIFT_GIFT2FIELDS.Revocable WHEN 0 THEN 'No' WHEN -1 THEN 'Yes' ELSE '' END  AS GFRevocable,
							 CAST(GIFT_GIFT2FIELDS.TermEndDate  as varchar(25))AS GFTermEndDt,
							 CASE GIFT_GIFT2FIELDS.TermType WHEN 1 THEN 'Fixed Term' WHEN 2 THEN 'Lives Only' WHEN 3 THEN 'Shorter of Fixed Term or Lives' WHEN 4 THEN 'Longer of Fixed Term or Lives' WHEN 5 THEN 'Lives then Shorter of Fixed Term or Lives' ELSE '' END  AS GFTermType,
							 CAST(GIFT_GIFT2FIELDS.PIFTotalUnits  as varchar(50))AS GFPIFTotalUnits,
							 CAST(GIFT_GIFT2FIELDS.TrustTaxID  as varchar(50))AS GFTrustTaxID,		 
													  
					    --properties
					    CONVERT(CHAR(10),GIFT.DATEADDED,101) [DateAdded], 
						CONVERT(CHAR(10),GIFT.DATECHANGED,101) [DateLastChanged],
						GIFT_ADDEDBYID.NAME [AddedBy],
						GIFT_LASTCHANGEDBYID.NAME [LastChangedBy],
						GIFT.ID [SystemRecordID],

					
					  dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'GF' [RE_DB_Tbl],
                      'G' + CAST(row_number() over (order by GIFT.IMPORT_ID) as char) as ID

FROM         DBO.Gift AS GIFT LEFT OUTER JOIN
                      DBO.GIFT2FIELDS AS GIFT_GIFT2FIELDS ON GIFT.ID = GIFT_GIFT2FIELDS.ID LEFT OUTER JOIN
                      DBO.CONSTITUENT_BANK AS GIFT_Constituent_Bank ON GIFT.CONSTITUENTBANKID = GIFT_Constituent_Bank.ID LEFT OUTER JOIN
                      DBO.BANK AS T_1 ON GIFT_Constituent_Bank.BRANCH_ID = T_1.ID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS T_2 ON T_1.BANK = T_2.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS GIFT_CONSTITUENCY ON GIFT.CONSTITUENT_CODE = GIFT_CONSTITUENCY.TABLEENTRIESID INNER JOIN
                      DBO.RECORDS AS GIFT_RECORDS ON GIFT.CONSTIT_ID = GIFT_RECORDS.ID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS GIFT_CreditType ON GIFT.CREDIT_TYPE = GIFT_CreditType.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS GIFT_GiftCode ON GIFT.GIFT_CODE = GIFT_GiftCode.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.COUNTRY_CODES AS GIFT_country_codes ON GIFT.CURRENCY_COUNTRY = GIFT_country_codes.COUNTRYCODESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS T_3 ON GIFT_country_codes.countrycodesid = T_3.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS GIFT_LETTER_CODE ON GIFT.LETTER_CODE = GIFT_LETTER_CODE.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS GIFT_GiftSubType ON GIFT.GiftSubType = GIFT_GiftSubType.TABLEENTRIESID
					  LEFT JOIN DBO.USERS AS GIFT_ADDEDBYID ON GIFT.ADDEDBYID = GIFT_ADDEDBYID.USER_ID 
					  LEFT JOIN DBO.USERS AS GIFT_LASTCHANGEDBYID ON GIFT.LASTCHANGEDBYID = GIFT_LASTCHANGEDBYID.USER_ID
					  LEFT OUTER JOIN DBO.TABLEENTRIES AS T_11 ON GIFT_GIFT2FIELDS.PIFNAME = T_11.TABLEENTRIESID 
					  LEFT OUTER JOIN DBO.TABLEENTRIES AS T_12 ON GIFT_GIFT2FIELDS.PlannedGiftStatus = T_12.TABLEENTRIESID 
					  LEFT OUTER JOIN DBO.TABLEENTRIES AS T_13 ON GIFT_GIFT2FIELDS.PolicyType = T_13.TABLEENTRIESID

                      
WHERE     GIFT.TYPE NOT IN (21, 22, 28, 32, 33) AND NOT (GIFT.TYPE IS NULL) AND (GIFT.TYPE IN (1, 8, 9, 10, 15, 18, 30, 34))

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 20.
CREATE VIEW dbo.HC_Gift_PledgePay
AS
SELECT     GIFT.IMPORT_ID [GFImpID], 
					CONVERT(CHAR(255), DBO.GetGiftFunds(GIFT.ID, GIFT.TYPE, 1)) [FundID], GIFT.BATCH_NUMBER, GIFT.DTE [GFDate], 
                      CASE GIFT.TYPE WHEN 1 THEN 'Cash' WHEN 2 THEN 'Pay-Cash' WHEN 3 THEN 'MG Pay-Cash' WHEN 8 THEN 'Pledge' WHEN 9 THEN 'Stock/Property'
                       WHEN 10 THEN 'Stock/Property (Sold)' WHEN 11 THEN 'Pay-Stock/Property' WHEN 12 THEN 'MG Pay-Stock/Property' WHEN 13 THEN 'Pay-Stock/Property (Sold)'
                       WHEN 14 THEN 'MG Pay-Stock/Property (Sold)' WHEN 15 THEN 'Gift-in-Kind' WHEN 16 THEN 'Pay-Gift-in-Kind' WHEN 17 THEN 'MG Pay-Gift-in-Kind' WHEN
                       18 THEN 'Other' WHEN 19 THEN 'Pay-Other' WHEN 20 THEN 'MG Pay-Other' WHEN 21 THEN 'Write Off' WHEN 22 THEN 'MG Write Off' WHEN 27 THEN
                       'MG Pledge' WHEN 30 THEN 'Recurring Gift' WHEN 31 THEN 'Recurring Gift Pay-Cash' WHEN 32 THEN 'GL Reversal' WHEN 33 THEN 'Amendment' 
                       WHEN 34 THEN 'Planned Gift'  ELSE null END [GFType], Gift.Amount [GFTAmt], 
                       CASE GIFT.TYPE WHEN 8 THEN dbo.Gift_GetPledgeBalance(GIFT.ID, GIFT.Amount, '','','','') 
									  WHEN 27 THEN dbo.Gift_GetPledgeBalance(GIFT.ID, GIFT.Amount, '','','','') ELSE null END AS GFPledgeBalance, 
                       CASE GIFT.TYPE WHEN 8 THEN dbo.Gift_GetPledgePaid (GIFT.ID, GIFT.Amount, '') 
									  WHEN 27 THEN dbo.Gift_GetPledgePaid (GIFT.ID, GIFT.Amount, '') ELSE null END AS GFPledgePaid,

					  -- [GFLink] = case when GIFT_Recurring.IMPORT_ID IS null then GIFT_PLEDGE.IMPORT_ID else GIFT_Recurring.IMPORT_ID  end,
	                  
	                  CASE GIFT.ACKNOWLEDGE_FLAG WHEN 1 THEN 'Acknowledged' WHEN 2 THEN 'Not Acknowledged' WHEN 3 THEN 'Do Not Acknowledge' ELSE '' END
                       [GFAck], 
                      CASE WHEN  ISDATE(GIFT.AcknowledgeDate)=1 AND LEN(GIFT.AcknowledgeDate)=8 THEN CAST(DBO.DATEFORMAT(GIFT.AcknowledgeDate, 'MM/DD/YYYY') as varchar(10))
						WHEN ISDATE(GIFT.AcknowledgeDate)=1 AND LEN(GIFT.AcknowledgeDate)=6 THEN CAST(RIGHT(GIFT.AcknowledgeDate,2) +'/'+ LEFT(GIFT.AcknowledgeDate,4) AS varchar(10))
						WHEN ISDATE(GIFT.AcknowledgeDate)=1 AND LEN(GIFT.AcknowledgeDate)=4 THEN cast(GIFT.AcknowledgeDate as varchar(10))
					  ELSE NULL END AS [GFAckDate], 
                       
                       GIFT.AmountBills [GFAmtBills], 
                      GIFT.AmountCoins [GFAmtCoins], 
                      CASE GIFT.ANONYMOUS WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [GFAnon], 
                      CONVERT(CHAR(255), DBO.GetGiftAppeals(GIFT.ID, GIFT.TYPE, 1)) [GFAppeal], GIFT.AUTHORIZATION_CODE [GFAuthCode], 
                      GIFT_Constituent_Bank.IMPORT_ID [BAImpID], GIFT.BenefitsNotes [GFBenNote], 
                      GIFT.CardholderName [GFCardholderName], 
                      CONVERT(CHAR(255), DBO.GetGiftCampaigns(GIFT.ID, GIFT.TYPE, 1)) [CampID], 
                      CASE WHEN  ISDATE(GIFT.CHECK_DATE)=1 AND LEN(GIFT.CHECK_DATE)=8 THEN CAST(DBO.DATEFORMAT(GIFT.CHECK_DATE, 'MM/DD/YYYY') as varchar(10))
						WHEN ISDATE(GIFT.CHECK_DATE)=1 AND LEN(GIFT.CHECK_DATE)=6 THEN CAST(RIGHT(GIFT.CHECK_DATE,2) +'/'+ LEFT(GIFT.CHECK_DATE,4) AS varchar(10))
						WHEN ISDATE(GIFT.CHECK_DATE)=1 AND LEN(GIFT.CHECK_DATE)=4 THEN cast(GIFT.CHECK_DATE as varchar(10))
					  ELSE NULL END AS [GFCheckDate], 
                      GIFT_CONSTITUENCY.shortdescription [GFCons], GIFT_CONSTITUENCY.LONGDESCRIPTION [GFConsDesc], 
                      GIFT.CHECK_NUMBER [GFCheckNum], GIFT_RECORDS.CONSTITUENT_ID [ConsID], 
                      GIFT_RECORDS.IMPORT_ID [ImportID], GIFT.EXPIRES_ON [GFCCExpOn], 
                      GIFT.CREDIT_CARD_NUMBER [GFCCNum], GIFT_CreditType.longdescription [GFCCType], 
                      CASE GIFT.ProcessedByEFT WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [GFEFT], 
                      GIFT_GiftCode.longdescription [GFGiftCode], GIFT.CURRENCY_AMOUNT [GFCurAmt], 
                      t_3.shortdescription [GFCurCntry], GIFT.CURRENCY_EXCHANGE_RATE [GFCurXRate], 
                      GIFT.CURRENCY_RECEIPT_AMOUNT [GFCurRcpt], GIFT.UserGiftId [GiftID], GIFT.POST_DATE [GFPostDate], 
                      CASE GIFT.POST_STATUS WHEN 1 THEN 'Posted' WHEN 2 THEN 'Not Posted' WHEN 3 THEN 'Do Not Post' ELSE '' END [GFPostStat],
                      CASE GIFT.BENEFITSINCLUDENOTES WHEN 0 THEN 'False' WHEN -1 THEN 'True' ELSE '' END [GFIncludeNotes], 
                      GIFT_LETTER_CODE.longdescription [GFLtrCode], 
                      CONVERT(CHAR(255), DBO.GetGiftPackages(GIFT.ID, GIFT.TYPE, 1)) [PackageID], 
                      CASE GIFT.PAYMENT_TYPE WHEN 1 THEN 'Cash' WHEN 2 THEN 'Personal Check' WHEN 3 THEN 'Business Check' WHEN 4 THEN 'Credit Card' WHEN
                       6 THEN 'Direct Debit' WHEN 8 THEN 'Other' ELSE '' END [GFPayMeth], 
                      GIFT.Send_Prenote_Units [GFPreNotifyUnits], 
                      CASE GIFT.RECEIPT_FLAG WHEN 1 THEN 'Receipted' WHEN 2 THEN 'Not Receipted' WHEN 3 THEN 'Do Not Receipt' ELSE '' END [Receipt],
                       GIFT.RECEIPT_AMOUNT [GFRcptAmt], 
                      CASE WHEN  ISDATE(GIFT.ReceiptDate)=1 AND LEN(GIFT.ReceiptDate)=8 THEN CAST(DBO.DATEFORMAT(GIFT.ReceiptDate, 'MM/DD/YYYY') as varchar(10))
						WHEN ISDATE(GIFT.ReceiptDate)=1 AND LEN(GIFT.ReceiptDate)=6 THEN CAST(RIGHT(GIFT.ReceiptDate,2) +'/'+ LEFT(GIFT.ReceiptDate,4) AS varchar(10))
						WHEN ISDATE(GIFT.ReceiptDate)=1 AND LEN(GIFT.ReceiptDate)=4 THEN cast(GIFT.ReceiptDate as varchar(10))
					  ELSE NULL END AS [GFRcptDate], 
                      GIFT.RECEIPT_NUMBER [GFRcptNum], 
					  CASE GIFT.RECEIVED WHEN 0 THEN 'False' WHEN -1 THEN 'True' ELSE '' END [GFReceived], 
					  replace(replace(replace(GIFT.REF, char(13), ' '), char(10), ' '), char(9), ' ') [GFRef], 
                      CASE WHEN  ISDATE(GIFT.REFERENCE_DATE)=1 AND LEN(GIFT.REFERENCE_DATE)=8 THEN CAST(DBO.DATEFORMAT(GIFT.REFERENCE_DATE, 'MM/DD/YYYY') as varchar(10))
						WHEN ISDATE(GIFT.REFERENCE_DATE)=1 AND LEN(GIFT.REFERENCE_DATE)=6 THEN CAST(RIGHT(GIFT.REFERENCE_DATE,2) +'/'+ LEFT(GIFT.REFERENCE_DATE,4) AS varchar(10))
						WHEN ISDATE(GIFT.REFERENCE_DATE)=1 AND LEN(GIFT.REFERENCE_DATE)=4 THEN cast(GIFT.REFERENCE_DATE as varchar(10))
					  ELSE NULL END AS [GFRefDate],
                      GIFT.REFERENCE_NUMBER [GFRefNum], 
                      CASE GIFT.REMIND_FLAG WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [GFSendRem], 
                      CASE GIFT.SENDPRENOTIFICATION WHEN 0 THEN 'False' WHEN -1 THEN 'True' WHEN 1 THEN 'True' ELSE '' END [GFPreNotify], 
                      CASE GIFT.GIFT_STATUS WHEN 1 THEN 'Active' WHEN 2 THEN 'Held' WHEN 3 THEN 'Terminated' WHEN 4 THEN 'Completed' ELSE '' END [GFStatus],
                       GIFT.GiftStatusDate [GFStatusDate], GIFT.Issuer [GFStkIss], 
                      GIFT.IssuerMedianPrice [GFStkMedPrice], GIFT.IssuerNumUnits [GFStkNumUnits], 
                      GIFT.STOCK_SALE_VALUE [GFStkSaleAmt], GIFT.BROKER_FEE [GFStkSaleBrokerFee], 
                      GIFT.STOCK_SALE_DATE [GFStkSaleDate], GIFT.STOCK_SALE_COMMENT [GFStkSaleNotes], 
                      GIFT.SALE_OF_STOCK_POST_DATE [GFStkSalePostDate], 
                      CASE GIFT.SALE_OF_STOCK_POST_STATUS WHEN 1 THEN 'Posted' WHEN 2 THEN 'Not Posted' WHEN 3 THEN 'Do Not Post' ELSE '' END [GFStkSalePostStat],
                       GIFT.IssuerSymbol [GFStkSymbol], GIFT_GiftSubType.longdescription [GFSubType], 
                      GIFT.SCHEDULE_MONTHLYDAYOFWEEK [GFInsDayName], 
                      CASE GIFT.TYPE WHEN 30 THEN DBO.Query_GiftInstallmentEndingOn(GIFT.ID, GIFT.TYPE, GIFT.Schedule_EndDate) ELSE NULL 
                      END [GFInsEndDate], DBO.Query_GetFrequency(GIFT.TYPE, GIFT.ID, GIFT.INSTALLMENT_FREQUENCY) 
                      [GFInsFreq], gift.schedule_spacing [GFInsFreqNum], 
                      CASE GIFT.SCHEDULE_MONTHLYTYPE WHEN 1 THEN 'Specific Day' WHEN 2 THEN 'User Defined Day' ELSE '' END [GFInsFreqOpt],
                       CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 6 THEN 'True' ELSE 'False' END [GFInsFri], 
                      CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 2 THEN 'True' ELSE 'False' END [GFInsMon], 
                      GIFT.SCHEDULE_MONTH [GFInsMonthName], GIFT.NUMBER_OF_INSTALLMENTS [GFInsNumPay], 
                      GIFT.SCHEDULE_SPACING [GFInsWeekFreq], GIFT.SCHEDULE_DAYOFMONTH [GFInsNumDay], 
                      GIFT.SCHEDULE_DAYOFMONTH2 [GFInsNumDay2], GIFT.SCHEDULE_MONTHLYORDINAL [GFInsOrdDay], 
                      GIFT.SCHEDULE_SMDAYTYPE1 [GFInsOrdDay1], GIFT.SCHEDULE_SMDAYTYPE2 [GFInsOrdDay2], 
                      CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 7 THEN 'True' ELSE 'False' END [GFInsSat], 
                      GIFT.DATE_1ST_PAY [GFInsStartDate], 
                      CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 1 THEN 'True' ELSE 'False' END [GFInsSun], 
                      CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 5 THEN 'True' ELSE 'False' END [GFInsThu], 
                      CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 3 THEN 'True' ELSE 'False' END [GFInsTue], 
                      CASE GIFT.SCHEDULE_WEEKLYDAYOFWEEK WHEN 4 THEN 'True' ELSE 'False' END [GFInsWed], 
					  NULL AS GFInsYears,
					  --plannedGifts 
							 '' GFPlannedGiftID, '' GFDiscountRt, '' GFMaturityYr, '' GFFlexibleDef,
							 '' GFTRealized, '' GFVehicle, '' GFNumUnits, '' GFPayoutAmt, '' GFPayoutPct, 
							 '' GFPIFName, '' GFPGStatus, '' GFRemainderAsOfDate, '' GFRemainderValue,
							 '' GFPPolicyType, '' GFPPolicyNum, '' GFPPolicyFaceAmt, '' GFPPolicyPremium,
							 '' GFNetPresentValue, '' GFNetPresentValueAsOf, '' GFRevocable, '' GFTermEndDt, 
							 '' GFTermType, '' GFPIFTotalUnits, '' GFTrustTaxID,		 
											  
						
						--properties
					    CONVERT(CHAR(10),GIFT.DATEADDED,101) [DateAdded], 
						CONVERT(CHAR(10),GIFT.DATECHANGED,101) [DateLastChanged],
						GIFT_ADDEDBYID.NAME [AddedBy],
						GIFT_LASTCHANGEDBYID.NAME [LastChangedBy],
						GIFT.ID [SystemRecordID],						
						
					  dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'GF' [RE_DB_Tbl],
                      'P' + CAST(row_number() over (order by GIFT.IMPORT_ID) as char) as ID

FROM         DBO.Gift AS GIFT LEFT OUTER JOIN
                      --dbo.GIFT_PLEDGEPAYMENTLINK ON GIFT.ID = dbo.GIFT_PLEDGEPAYMENTLINK.PAYMENTID LEFT OUTER JOIN
                     -- dbo.GIFT AS GIFT_Pledge ON dbo.GIFT_PLEDGEPAYMENTLINK.PLEDGEID = GIFT_Pledge.id  LEFT OUTER JOIN 
					 -- dbo.RecurringGiftActivity ON GIFT.ID = dbo.RecurringGiftActivity.PaymentID LEFT OUTER JOIN 
					  --dbo.GIFT AS GIFT_Recurring ON  dbo.RecurringGiftActivity.RecurringGiftId = GIFT_Recurring.id LEFT OUTER JOIN 

					  DBO.CONSTITUENT_BANK AS GIFT_Constituent_Bank ON GIFT.CONSTITUENTBANKID = GIFT_Constituent_Bank.ID LEFT OUTER JOIN
                      DBO.BANK AS T_1 ON GIFT_Constituent_Bank.BRANCH_ID = T_1.ID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS T_2 ON T_1.BANK = T_2.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS GIFT_CONSTITUENCY ON GIFT.CONSTITUENT_CODE = GIFT_CONSTITUENCY.TABLEENTRIESID INNER JOIN
                      DBO.RECORDS AS GIFT_RECORDS ON GIFT.CONSTIT_ID = GIFT_RECORDS.ID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS GIFT_CreditType ON GIFT.CREDIT_TYPE = GIFT_CreditType.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS GIFT_GiftCode ON GIFT.GIFT_CODE = GIFT_GiftCode.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.COUNTRY_CODES AS GIFT_country_codes ON GIFT.CURRENCY_COUNTRY = GIFT_country_codes.COUNTRYCODESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS T_3 ON GIFT_country_codes.countrycodesid = T_3.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS GIFT_LETTER_CODE ON GIFT.LETTER_CODE = GIFT_LETTER_CODE.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS GIFT_GiftSubType ON GIFT.GiftSubType = GIFT_GiftSubType.TABLEENTRIESID
                      
                      LEFT JOIN DBO.USERS AS GIFT_ADDEDBYID ON GIFT.ADDEDBYID = GIFT_ADDEDBYID.USER_ID 
					  LEFT JOIN DBO.USERS AS GIFT_LASTCHANGEDBYID ON GIFT.LASTCHANGEDBYID = GIFT_LASTCHANGEDBYID.USER_ID
	
WHERE     (GIFT.TYPE NOT IN (21, 22, 28, 32, 33)) AND NOT (GIFT.TYPE IS NULL) AND (GIFT.TYPE NOT IN (1, 8, 9, 10, 15, 18, 30, 34))

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 48: GIFTS 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[HC_Gift_v]
 
				AS 

					select GFImpID, FundID, CampID, GFAppeal, PackageID, BATCH_NUMBER, GFDate, GFType, GFTAmt, 
					GFPledgeBalance, GFAck, GFAckDate, GFAmtBills, GFAmtCoins, GFAnon, 
					GFAuthCode, BAImpID, CONVERT(VARCHAR(MAX), GFBenNote) as GFBenNote, 
					GFCardholderName , GFCheckDate, GFCons, GFConsDesc, GFCheckNum, ConsID,
					ImportID, GFCCExpOn, GFCCNum, GFCCType, GFEFT, GFGiftCode, GFCurAmt, GFCurCntry, GFCurXRate, GFCurRcpt, GiftID, 
					GFPostDate, GFPostStat, GFIncludeNotes, GFLtrCode, GFPayMeth, GFPreNotifyUnits, Receipt, GFRcptAmt, 
					GFRcptDate, GFRcptNum, GFReceived, GFRef, GFRefDate, GFRefNum, GFSendRem, GFPreNotify, GFStatus, GFStatusDate, 
					GFStkIss, GFStkMedPrice, GFStkNumUnits, GFStkSaleAmt, GFStkSaleBrokerFee, GFStkSaleDate,
					CONVERT(VARCHAR(MAX), GFStkSaleNotes) as GFStkSaleNotes, 
					GFStkSalePostDate, GFStkSalePostStat, GFStkSymbol, GFSubType, GFInsDayName, GFInsEndDate, GFInsFreq, 
					GFInsFreqNum, GFInsFreqOpt, GFInsFri, GFInsMon, GFInsMonthName, GFInsNumPay, GFInsWeekFreq, GFInsNumDay, 
					GFInsNumDay2, GFInsOrdDay, GFInsOrdDay1, GFInsOrdDay2, GFInsSat, GFInsStartDate, GFInsSun, GFInsThu, GFInsTue, GFInsWed,
					GFInsYears,

					GFPlannedGiftID, GFDiscountRt, GFMaturityYr, GFFlexibleDef, GFTRealized, GFVehicle, GFNumUnits, GFPayoutAmt, GFPayoutPct,
					GFPIFName, GFPGStatus, GFRemainderAsOfDate, GFRemainderValue, GFPPolicyType, GFPPolicyNum, GFPPolicyFaceAmt, GFPPolicyPremium,
					GFNetPresentValue, GFNetPresentValueAsOf, GFRevocable, GFTermEndDt, GFTermType, GFPIFTotalUnits, GFTrustTaxID,
	 
					DateAdded, DateLastChanged, AddedBy, LastChangedBy, SystemRecordID, 	
					RE_DB_Owner, [RE_DB_OwnerShort], [RE_DB_Tbl], ID
					from dbo.HC_Gift_Main

					UNION ALL

					select GFImpID, FundID, CampID, GFAppeal, PackageID, BATCH_NUMBER, GFDate, GFType, GFTAmt, 
					GFPledgeBalance,
					GFAck, GFAckDate, GFAmtBills, GFAmtCoins, GFAnon,
					GFAuthCode, BAImpID, CONVERT(VARCHAR(MAX), GFBenNote) as GFBenNote, 
					GFCardholderName, GFCheckDate, GFCons, GFConsDesc, GFCheckNum, ConsID, 
					ImportID, GFCCExpOn, GFCCNum, GFCCType, GFEFT, GFGiftCode, GFCurAmt, GFCurCntry, GFCurXRate, GFCurRcpt, GiftID, 
					GFPostDate, GFPostStat, GFIncludeNotes, GFLtrCode, GFPayMeth, GFPreNotifyUnits, Receipt, GFRcptAmt,
					GFRcptDate, GFRcptNum, GFReceived, GFRef, GFRefDate, GFRefNum, GFSendRem, GFPreNotify, GFStatus, GFStatusDate, 
					GFStkIss, GFStkMedPrice, GFStkNumUnits, GFStkSaleAmt, GFStkSaleBrokerFee, GFStkSaleDate, 
					CONVERT(VARCHAR(MAX), GFStkSaleNotes) as GFStkSaleNotes, 
					GFStkSalePostDate, GFStkSalePostStat, GFStkSymbol, GFSubType, GFInsDayName, GFInsEndDate, GFInsFreq,
					GFInsFreqNum, GFInsFreqOpt, GFInsFri, GFInsMon, GFInsMonthName, GFInsNumPay, GFInsWeekFreq, GFInsNumDay, 
					GFInsNumDay2, GFInsOrdDay, GFInsOrdDay1, GFInsOrdDay2, GFInsSat, GFInsStartDate, GFInsSun, GFInsThu, GFInsTue, GFInsWed, 
					GFInsYears,

					GFPlannedGiftID, GFDiscountRt, GFMaturityYr, GFFlexibleDef, GFTRealized, GFVehicle, GFNumUnits, GFPayoutAmt, GFPayoutPct,
					GFPIFName, GFPGStatus, GFRemainderAsOfDate, GFRemainderValue, GFPPolicyType, GFPPolicyNum, GFPPolicyFaceAmt, GFPPolicyPremium,
					GFNetPresentValue, GFNetPresentValueAsOf, GFRevocable, GFTermEndDt, GFTermType, GFPIFTotalUnits, GFTrustTaxID,
	 
					DateAdded, DateLastChanged, AddedBy, LastChangedBy, SystemRecordID, 	
					RE_DB_Owner, [RE_DB_OwnerShort], [RE_DB_Tbl], ID
					from dbo.HC_Gift_PledgePay


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO





SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view dbo.HC_Gift_Link_v
AS


SELECT  dbo.PledgePayments.pledgeid AS GFSysRecId, 
      CASE GIFT.TYPE WHEN 1 THEN 'Cash' WHEN 2 THEN 'Pay-Cash' WHEN 3 THEN 'MG Pay-Cash' WHEN 8 THEN 'Pledge' WHEN 9 THEN 'Stock/Property' WHEN 10 THEN
       'Stock/Property (Sold)' WHEN 11 THEN 'Pay-Stock/Property' WHEN 12 THEN 'MG Pay-Stock/Property' WHEN 13 THEN 'Pay-Stock/Property (Sold)' WHEN 14 THEN 'MG Pay-Stock/Property (Sold)'
       WHEN 15 THEN 'Gift-in-Kind' WHEN 16 THEN 'Pay-Gift-in-Kind' WHEN 17 THEN 'MG Pay-Gift-in-Kind' WHEN 18 THEN 'Other' WHEN 19 THEN 'Pay-Other' WHEN 20 THEN
       'MG Pay-Other' WHEN 21 THEN 'Write Off' WHEN 22 THEN 'MG Write Off' WHEN 27 THEN 'MG Pledge' WHEN 30 THEN 'Recurring Gift' WHEN 31 THEN 'Recurring Gift Pay-Cash'
       WHEN 32 THEN 'GL Reversal' WHEN 33 THEN 'Amendment' WHEN 34 THEN 'Planned Gift' ELSE NULL END AS GFType,
      dbo.GIFT.IMPORT_ID AS GFImpID, 
      dbo.PledgePayments.paymentid AS GFPaymentSysRecId, 
      CASE GIFT_1.TYPE WHEN 1 THEN 'Cash' WHEN 2 THEN 'Pay-Cash' WHEN 3 THEN 'MG Pay-Cash' WHEN 8 THEN 'Pledge' WHEN 9 THEN 'Stock/Property' WHEN 10 THEN
       'Stock/Property (Sold)' WHEN 11 THEN 'Pay-Stock/Property' WHEN 12 THEN 'MG Pay-Stock/Property' WHEN 13 THEN 'Pay-Stock/Property (Sold)' WHEN 14 THEN 'MG Pay-Stock/Property (Sold)'
       WHEN 15 THEN 'Gift-in-Kind' WHEN 16 THEN 'Pay-Gift-in-Kind' WHEN 17 THEN 'MG Pay-Gift-in-Kind' WHEN 18 THEN 'Other' WHEN 19 THEN 'Pay-Other' WHEN 20 THEN
       'MG Pay-Other' WHEN 21 THEN 'Write Off' WHEN 22 THEN 'MG Write Off' WHEN 27 THEN 'MG Pledge' WHEN 30 THEN 'Recurring Gift' WHEN 31 THEN 'Recurring Gift Pay-Cash'
       WHEN 32 THEN 'GL Reversal' WHEN 33 THEN 'Amendment' WHEN 34 THEN 'Planned Gift' ELSE NULL END AS GFPaymentType, 
		GIFT_1.IMPORT_ID AS GFPaymentGFImpID, SUM(dbo.PledgePayments.Amount) AS GFPaymentAmount,
		dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort]
FROM	dbo.PledgePayments 
		INNER JOIN dbo.GIFT ON dbo.PledgePayments.pledgeid = dbo.GIFT.ID 
		INNER JOIN dbo.GIFT AS GIFT_1 ON dbo.PledgePayments.paymentid = GIFT_1.ID
GROUP BY dbo.GIFT.IMPORT_ID, dbo.PledgePayments.pledgeid, dbo.PledgePayments.paymentid, GIFT_1.IMPORT_ID, GIFT.TYPE, GIFT_1.TYPE 
 
 UNION ALL
 
 
 
 SELECT     dbo.RecurringGiftActivity.RecurringGiftId AS GFSysRecId, 
                      CASE GIFT.TYPE WHEN 1 THEN 'Cash' WHEN 2 THEN 'Pay-Cash' WHEN 3 THEN 'MG Pay-Cash' WHEN 8 THEN 'Pledge' WHEN 9 THEN 'Stock/Property' WHEN 10 THEN
                       'Stock/Property (Sold)' WHEN 11 THEN 'Pay-Stock/Property' WHEN 12 THEN 'MG Pay-Stock/Property' WHEN 13 THEN 'Pay-Stock/Property (Sold)' WHEN 14 THEN 'MG Pay-Stock/Property (Sold)'
                       WHEN 15 THEN 'Gift-in-Kind' WHEN 16 THEN 'Pay-Gift-in-Kind' WHEN 17 THEN 'MG Pay-Gift-in-Kind' WHEN 18 THEN 'Other' WHEN 19 THEN 'Pay-Other' WHEN 20 THEN
                       'MG Pay-Other' WHEN 21 THEN 'Write Off' WHEN 22 THEN 'MG Write Off' WHEN 27 THEN 'MG Pledge' WHEN 30 THEN 'Recurring Gift' WHEN 31 THEN 'Recurring Gift Pay-Cash'
                       WHEN 32 THEN 'GL Reversal' WHEN 33 THEN 'Amendment' WHEN 34 THEN 'Planned Gift' ELSE NULL END AS GFType, 
                       dbo.GIFT.IMPORT_ID AS GFImpID, 
                      dbo.RecurringGiftActivity.PaymentId AS GFPaymentSysRecId, 
                      CASE GIFT_1.TYPE WHEN 1 THEN 'Cash' WHEN 2 THEN 'Pay-Cash' WHEN 3 THEN 'MG Pay-Cash' WHEN 8 THEN 'Pledge' WHEN 9 THEN 'Stock/Property' WHEN 10 THEN
                       'Stock/Property (Sold)' WHEN 11 THEN 'Pay-Stock/Property' WHEN 12 THEN 'MG Pay-Stock/Property' WHEN 13 THEN 'Pay-Stock/Property (Sold)' WHEN 14 THEN 'MG Pay-Stock/Property (Sold)'
                       WHEN 15 THEN 'Gift-in-Kind' WHEN 16 THEN 'Pay-Gift-in-Kind' WHEN 17 THEN 'MG Pay-Gift-in-Kind' WHEN 18 THEN 'Other' WHEN 19 THEN 'Pay-Other' WHEN 20 THEN
                       'MG Pay-Other' WHEN 21 THEN 'Write Off' WHEN 22 THEN 'MG Write Off' WHEN 27 THEN 'MG Pledge' WHEN 30 THEN 'Recurring Gift' WHEN 31 THEN 'Recurring Gift Pay-Cash'
                       WHEN 32 THEN 'GL Reversal' WHEN 33 THEN 'Amendment' WHEN 34 THEN 'Planned Gift' ELSE NULL END AS GFPaymentType, 
                      GIFT_1.IMPORT_ID AS GFPaymentGFImpID, dbo.RecurringGiftActivity.VInstallmentAmount AS GFPaymentAmount,
                      	dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort]
FROM         dbo.RecurringGiftActivity INNER JOIN
                      dbo.GIFT ON dbo.RecurringGiftActivity.RecurringGiftId = dbo.GIFT.ID INNER JOIN
                      dbo.GIFT AS GIFT_1 ON dbo.RecurringGiftActivity.PaymentId = GIFT_1.ID
 
 
 
 
 
 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



 
-- WRITE OFF GIFTS. 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.HC_Gift_WriteOff_v
AS
SELECT      GIFT_RECORDS.CONSTITUENT_ID [ConsID], GIFT_RECORDS.IMPORT_ID [ImportID], GIFT.IMPORT_ID [GFImpID], GIFT.ID [GiftID], GIFT.DTE [GFDate], 
            CASE GIFT.TYPE WHEN 21 THEN 'Write Off' WHEN 22 THEN 'MG Write Off' ELSE null END [GFType], Gift.Amount [GFTAmt], 
			[GFLink] = case when GIFT_Recurring.IMPORT_ID IS null then GIFT_PLEDGE.IMPORT_ID else GIFT_Recurring.IMPORT_ID  end,
			GIFT.POST_DATE [GFPostDate], CASE GIFT.POST_STATUS WHEN 1 THEN 'Posted' WHEN 2 THEN 'Not Posted' WHEN 3 THEN 'Do Not Post' ELSE '' END [GFPostStat],
	        	
			--properties
			CONVERT(CHAR(10),GIFT.DATEADDED,101) [DateAdded], CONVERT(CHAR(10),GIFT.DATECHANGED,101) [DateLastChanged], GIFT_ADDEDBYID.NAME [AddedBy],
			GIFT_LASTCHANGEDBYID.NAME [LastChangedBy], GIFT.ID [SystemRecordID], dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'GF' [RE_DB_Tbl],
			'WO' + CAST(row_number() over (order by GIFT.IMPORT_ID) as char) as ID

FROM        DBO.Gift AS GIFT LEFT OUTER JOIN
			dbo.GIFT_PLEDGEPAYMENTLINK ON GIFT.ID = dbo.GIFT_PLEDGEPAYMENTLINK.PAYMENTID LEFT OUTER JOIN
            dbo.GIFT AS GIFT_Pledge ON dbo.GIFT_PLEDGEPAYMENTLINK.PLEDGEID = GIFT_Pledge.id  LEFT OUTER JOIN 
			dbo.RecurringGiftActivity ON GIFT.ID = dbo.RecurringGiftActivity.PaymentID LEFT OUTER JOIN 
			dbo.GIFT AS GIFT_Recurring ON  dbo.RecurringGiftActivity.RecurringGiftId = GIFT_Recurring.id LEFT OUTER JOIN 
			DBO.RECORDS AS GIFT_RECORDS ON GIFT.CONSTIT_ID = GIFT_RECORDS.ID LEFT OUTER JOIN
            DBO.USERS AS GIFT_ADDEDBYID ON GIFT.ADDEDBYID = GIFT_ADDEDBYID.USER_ID 
			LEFT JOIN DBO.USERS AS GIFT_LASTCHANGEDBYID ON GIFT.LASTCHANGEDBYID = GIFT_LASTCHANGEDBYID.USER_ID
	
WHERE		GIFT.TYPE IN (21, 22)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



-- VIEW 21.
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view dbo.HC_Gift_Attr_v
AS
SELECT     dbo.gift.import_id [GFImpID], dbo.giftattributes.IMPORTID [GFAttrImpID], 
                      dbo.AttributeTypes.DESCRIPTION [GFAttrCat], dbo.giftattributes.COMMENTS [GFAttrCom], 
                      CONVERT(CHAR(10),dbo.GiftAttributes.AttributeDate,101) [GFAttrDate], 
					  GFAttrDesc = CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL 
                      THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
					  WHEN dbo.giftattributes.boolean IS NOT NULL THEN (CASE dbo.giftattributes.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END) 
                      WHEN dbo.giftattributes.text IS NOT NULL THEN dbo.giftattributes.text WHEN dbo.giftattributes.num IS NOT NULL THEN CONVERT(varchar, 
                      dbo.giftattributes.num) WHEN dbo.giftattributes.datetime IS NOT NULL THEN CONVERT(CHAR(10),dbo.GiftAttributes.datetime,101) 
                      WHEN dbo.giftattributes.currency IS NOT NULL THEN CONVERT(varchar, dbo.giftattributes.currency) WHEN dbo.giftattributes.constitid IS NOT NULL 
                      THEN CONVERT(varchar, dbo.giftattributes.constitid) ELSE NULL END, 
                      case dbo.AttributeTypes.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
											   when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
											   when 8 then 'FuzzyDate' end as [DataType], 	
					  dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'GA' [RE_DB_Tbl],
                      row_number() over (order by gift.import_id) as ID

FROM         ((dbo.gift LEFT JOIN
                      dbo.giftattributes ON dbo.gift.ID = dbo.giftattributes.PARENTID) LEFT JOIN
                      (dbo.AttributeTypes LEFT JOIN
                      dbo.CODETABLES ON dbo.AttributeTypes.CODETABLESID = dbo.CODETABLES.CODETABLESID) ON 
                      dbo.giftattributes.ATTRIBUTETYPESID = dbo.AttributeTypes.ATTRIBUTETYPESID) LEFT JOIN
                      dbo.TABLEENTRIES ON dbo.giftattributes.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.attributeTYPEs AS T_1 ON giftattributes.ATTRIBUTETYPESID = T_1.ATTRIBUTETYPESID
WHERE     dbo.giftattributes.importid IS NOT NULL



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 22.
CREATE view dbo.HC_Gift_Notes_v
AS
SELECT     GIFT.IMPORT_ID AS [GFLink], GIFT_GIFTNOTEPAD.Import_Id AS [GFNoteImpID], GIFT_GIFTNOTEPAD.NotepadDate AS [GFNoteDate], 
                      T_1.LONGDESCRIPTION AS [GFNoteType], GIFT_GIFTNOTEPAD.Author AS [GFNoteAuth], GIFT_GIFTNOTEPAD.Description AS [GFNoteDesc], 
                      REPLACE(REPLACE(REPLACE(CAST(GIFT_GIFTNOTEPAD.ActualNotes AS varchar(max)), CHAR(13), ' '), CHAR(10), ' '), CHAR(9), ' ') AS [GFNoteNotes], 
                      GIFT_GIFTNOTEPAD.Title AS [GFNoteTitle], 
					  dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'GN' [RE_DB_Tbl],
                      row_number() over (order by gift.import_id) as ID

FROM         dbo.GIFT GIFT LEFT OUTER JOIN
                      dbo.GiftNotepad GIFT_GIFTNOTEPAD ON GIFT.ID = GIFT_GIFTNOTEPAD.ParentId LEFT OUTER JOIN
                      dbo.TABLEENTRIES T_1 ON GIFT_GIFTNOTEPAD.NoteTypeId = T_1.TABLEENTRIESID
WHERE     (GIFT.TYPE NOT IN (28, 32, 33) OR
                      GIFT.TYPE IS NULL) AND (GIFT_GIFTNOTEPAD.Import_Id IS NOT NULL)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 23.
CREATE VIEW dbo.HC_Gift_Solicitor_v

AS
SELECT     dbo.GIFT.IMPORT_ID AS [GFLink], dbo.RECORDS.IMPORT_ID AS [SolImpID], 
                      dbo.GiftSolicitor.Amount AS [GFSolAmtGift Amount], dbo.GiftSolicitor.Import_Id AS [GFSolImpID], 
					  dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'GS' [RE_DB_Tbl],
                      row_number() over (order by GIFT.IMPORT_ID) as ID

FROM         dbo.GIFT LEFT OUTER JOIN
                      dbo.GiftSolicitor ON dbo.GIFT.ID = dbo.GiftSolicitor.ParentId LEFT OUTER JOIN
                      dbo.RECORDS ON dbo.GiftSolicitor.SolicitorId = dbo.RECORDS.ID
WHERE     (dbo.GiftSolicitor.SolicitorId IS NOT NULL)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- VIEW 24.
CREATE VIEW dbo.HC_Ind_Relat
AS
SELECT     T_2.IMPORT_ID AS [IRImpID], T_1.IMPORT_ID AS [ImportID], T_1.CONSTITUENT_ID AS [ConsID], 
		   CASE T_13.is_constituent WHEN - 1 THEN T_13.IMPORT_ID ELSE '' END AS [IRLink], T_23.IMPORT_ID AS [RRImpID],

			-- ADDRESS IDs for reference and linking purposes. 
			T_3.ADDRESS_ID as REFERENCE_ADDRESS_ID, T_3.ID as REFERENCE_ID, T_3.IMPORT_ID REFERENCE_IMPORT_ID,
			
			dbo.ADDRESSLINE(1, T_4.ADDRESS_BLOCK) AS [IRAddrLine1], dbo.ADDRESSLINE(2, 
                      T_4.ADDRESS_BLOCK) AS [IRAddrLine2], dbo.ADDRESSLINE(3, T_4.ADDRESS_BLOCK) AS [IRAddrLine3], dbo.ADDRESSLINE(4, 
                      T_4.ADDRESS_BLOCK) AS [IRAddrLine4], dbo.ADDRESSLINE(5, T_4.ADDRESS_BLOCK) AS [IRAddrLine5], T_4.CARRIER_ROUTE AS IRAddrCART, 
                      T_4.CITY AS IRAddrCity, T_5.LONGDESCRIPTION AS IRAddrCountry, T_6.LONGDESCRIPTION AS IRAddrCounty, 
                      T_3.DATE_FROM AS [IRAddrValidFrom], T_3.DATE_TO AS [IRAddrValidTo], T_4.DPC AS IRAddrDPC, 
                      T_7.LONGDESCRIPTION AS [IRAddrInfoSrc], T_4.LOT AS IRAddrLOT, T_8.LONGDESCRIPTION AS IRAddrNZSuburb, 
                      T_9.LONGDESCRIPTION AS IRAddrNZCity, T_10.LONGDESCRIPTION AS IRAddrRegion, 
                      CASE T_3.SEASONAL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRAddrSeasonal], 
                      T_3.SEASONAL_FROM AS [IRAddrSeaFrom], T_3.SEASONAL_TO AS [IRAddrSeaTo], 
                      CASE T_3.SENDMAIL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRSendMail], 
                      T_11.LONGDESCRIPTION AS [IRAddrType], T_12.SHORTDESCRIPTION AS [IRAddrState], T_4.POST_CODE AS [IRAddrZip], 
                      CASE T_2.HON_MEM_ACKNOWLEDGE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRAck],
                      CASE T_2.SOFTCREDIT_GIFTS WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRAutoSoftCredit],
                      case	when  ISDATE(T_13.BIRTH_DATE)=1 AND LEN(T_13.BIRTH_DATE)=8 then cast(dbo.dateformat(T_13.BIRTH_DATE, 'MM/DD/YYYY') AS varchar(10))
							when  ISDATE(T_13.BIRTH_DATE)=1 AND LEN(T_13.BIRTH_DATE)=6 then cast(RIGHT(T_13.BIRTH_DATE,2) +'/'+ LEFT(T_13.BIRTH_DATE,4) as varchar(10)) 
							when  ISDATE(T_13.BIRTH_DATE)=1 AND LEN(T_13.BIRTH_DATE)=4 then cast(T_13.BIRTH_DATE as varchar(10))
					        else null end as  [IRBDay], 
					   T_14.LONGDESCRIPTION AS [IRContactType], 
                      
		  			   case when  ISDATE(T_2.DATE_FROM)=1 AND LEN(T_2.DATE_FROM)=8 then cast(dbo.dateformat(T_2.DATE_FROM, 'MM/DD/YYYY') AS varchar(10))
					   when  ISDATE(T_2.DATE_FROM)=1 AND LEN(T_2.DATE_FROM)=6 then cast(RIGHT(T_2.DATE_FROM,2) +'/'+ LEFT(T_2.DATE_FROM,4) AS varchar(10))
					   when  ISDATE(T_2.DATE_FROM)=1 AND LEN(T_2.DATE_FROM)=4 then cast(T_2.DATE_FROM AS varchar(10))
					   else null end AS  [IRFromDate],
					   
					   case when  ISDATE(T_2.DATE_TO)=1 AND LEN(T_2.DATE_TO)=8 then cast(dbo.dateformat(T_2.DATE_TO, 'MM/DD/YYYY') AS varchar(10))
					   when  ISDATE(T_2.DATE_TO)=1 AND LEN(T_2.DATE_TO)=6 then cast(RIGHT(T_2.DATE_TO,2) +'/'+ LEFT(T_2.DATE_TO,4) AS varchar(10))
					   when  ISDATE(T_2.DATE_TO)=1 AND LEN(T_2.DATE_TO)=4 then cast(T_2.DATE_TO AS varchar(10))
					   else null end AS  [IRToDate],

                      CASE T_13.DECEASED WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRDeceased], 
                      case when  ISDATE(T_13.DECEASED_DATE)=1 AND LEN(T_13.DECEASED_DATE)=8 then cast(dbo.dateformat(T_13.DECEASED_DATE, 'MM/DD/YYYY') AS varchar(10))
						   when  ISDATE(T_13.DECEASED_DATE)=1 AND LEN(T_13.DECEASED_DATE)=6 then cast(RIGHT(T_13.DECEASED_DATE,2) +'/'+ LEFT(T_13.DECEASED_DATE,4) AS varchar(10))
						   when  ISDATE(T_13.DECEASED_DATE)=1 AND LEN(T_13.DECEASED_DATE)=4 then cast(T_13.DECEASED_DATE AS varchar(10))
						   else null end AS  [IRDecDate],
                      CASE T_2.DO_NOT_MAIL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRNoMail], 
                      T_15.FIRST_NAME AS [IRFirstName], 
                      CASE T_13.SEX WHEN 1 THEN 'Male' WHEN 2 THEN 'Female' WHEN 3 THEN 'Unknown' ELSE '' END AS [IRGender], 
                      CASE T_2.IS_CONTACT WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRIsContact], 
                      CASE T_2.IS_EMPLOYEE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRIsEmp], 
                      CASE T_2.IS_HEADOFHOUSEHOLD WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRIsHH], 
                      CASE T_2.IS_PRIMARY WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRIsPrimary], 
                      CASE T_2.IS_SPOUSE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRIsSpouse], 
                      T_13.LAST_NAME AS [IRLastName], T_13.MAIDEN_NAME AS [IRMaidName], 
                      T_15.MIDDLE_NAME AS [IRMidName], T_13.NICKNAME AS [IRNickname], 
                      T_2.NOTES AS [IRNotes], T_2.POSITION AS [IRPos], 
                      CASE T_13.PRIMARY_ADDRESSEE_EDIT WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRPrimAddEdit], 
                      T_13.PRIMARY_ADDRESSEE_ID AS [IRPrimAddID], dbo.Query_GetAddrSal(T_13.ID, 
                      T_13.PRIMARY_ADDRESSEE_EDIT, T_13.PRIMARY_ADDRESSEE_ID, T_13.PRIMARY_ADDRESSEE, '01/25/2007') 
                      AS [IRPrimAddText], 
                      CASE T_13.PRIMARY_SALUTATION_EDIT WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRPrimSalEdit], 
                      T_13.PRIMARY_SALUTATION_ID AS [IRPrimSalID], dbo.Query_GetAddrSal(T_13.ID, T_13.PRIMARY_SALUTATION_EDIT, 
                      T_13.PRIMARY_SALUTATION_ID, T_13.PRIMARY_SALUTATION, '01/25/2007') AS [IRPrimSalText], 
                      CASE T_2.PRINT_ORG WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRPrintOrg],
                       CASE T_2.PRINT_POSITION WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRPrintPos], 
                      T_16.LONGDESCRIPTION AS [IRProf], 
                      CASE WHEN T_17.LONGDESCRIPTION IS NULL THEN 'NULL' ELSE T_17.LONGDESCRIPTION END AS [IRRecip], 
                      CASE WHEN T_18.LONGDESCRIPTION IS NULL THEN 'NULL' ELSE T_18.LONGDESCRIPTION END AS [IRRelat], 
                      
                      T_19.LONGDESCRIPTION AS [IRSuff1], T_20.LONGDESCRIPTION AS [IRSuff2], 
                      T_21.LONGDESCRIPTION AS [IRTitl1], T_22.LONGDESCRIPTION AS [IRTitl2], 
                      dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'IR' [RE_DB_Tbl],
                      'HH' + cast(row_number() over (order by T_2.IMPORT_ID) as char) as ID      
                                      
FROM         dbo.CONSTIT_RELATIONSHIPS AS CONSTIT_RELATIONSHIPS INNER JOIN
                      dbo.RECORDS AS T_1 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_1.ID LEFT OUTER JOIN
                      dbo.QUERY_IND_RELATIONSHIPS AS T_2 ON CONSTIT_RELATIONSHIPS.ID = T_2.ID LEFT OUTER JOIN
                      dbo.CONSTIT_ADDRESS AS T_3 ON T_2.CONSTIT_ADDRESS_ID = T_3.ID LEFT OUTER JOIN
                      dbo.ADDRESS AS T_4 ON T_3.ADDRESS_ID = T_4.ID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_5 ON T_4.COUNTRY = T_5.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_6 ON T_4.COUNTY = T_6.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_7 ON T_3.INFO_SOURCE = T_7.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_8 ON T_4.SUBURB = T_8.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_9 ON T_4.NZCITY = T_9.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_10 ON T_4.REGION = T_10.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_11 ON T_3.TYPE = T_11.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.STATES AS T_12 ON T_4.STATE = T_12.SHORTDESCRIPTION LEFT OUTER JOIN
                      
                      dbo.RECORDS AS T_13 ON T_2.RELATION_ID = T_13.ID LEFT OUTER JOIN
                      dbo.CONSTIT_RELATIONSHIPS AS T_23 ON T_2.RECIPROCAL_ID = T_23.ID LEFT OUTER JOIN
                      
                      dbo.TABLEENTRIES AS T_14 ON T_2.CONTACT_TYPE = T_14.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.SEARCHNAME AS T_15 ON T_13.ID = T_15.RECORDS_ID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_16 ON T_2.PROFESSION = T_16.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_17 ON T_2.RECIP_RELATION_CODE = T_17.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_18 ON T_2.RELATION_CODE = T_18.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_19 ON T_13.SUFFIX_1 = T_19.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_20 ON T_13.SUFFIX_2 = T_20.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_21 ON T_13.TITLE_1 = T_21.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_22 ON T_13.TITLE_2 = T_22.TABLEENTRIESID
WHERE     (T_2.IMPORT_ID IS NOT NULL) AND (T_2.IS_HEADOFHOUSEHOLD = - 1) OR
                      (T_2.IMPORT_ID IS NOT NULL) AND (T_2.IS_PRIMARY = - 1)

UNION ALL

SELECT     T_2.IMPORT_ID AS [IRImpID], T_1.IMPORT_ID AS [ImportID], T_1.CONSTITUENT_ID AS [ConsID], 
		   CASE T_13.is_constituent WHEN - 1 THEN T_13.IMPORT_ID ELSE '' END AS [IRLink], T_23.IMPORT_ID AS [RRImpID],

			-- ADDRESS IDs for reference and linking purposes. 
			T_3.ADDRESS_ID as REFERENCE_ADDRESS_ID, T_3.ID as REFERENCE_ID, T_3.IMPORT_ID REFERENCE_IMPORT_ID,
			dbo.ADDRESSLINE(1, T_4.ADDRESS_BLOCK) AS [IRAddrLine1], dbo.ADDRESSLINE(2, 
                      T_4.ADDRESS_BLOCK) AS [IRAddrLine2], dbo.ADDRESSLINE(3, T_4.ADDRESS_BLOCK) AS [IRAddrLine3], dbo.ADDRESSLINE(4, 
                      T_4.ADDRESS_BLOCK) AS [IRAddrLine4], dbo.ADDRESSLINE(5, T_4.ADDRESS_BLOCK) AS [IRAddrLine5], T_4.CARRIER_ROUTE AS IRAddrCART, 
                      T_4.CITY AS IRAddrCity, T_5.LONGDESCRIPTION AS IRAddrCountry, T_6.LONGDESCRIPTION AS IRAddrCounty, 
                      T_3.DATE_FROM AS [IRAddrValidFrom], T_3.DATE_TO AS [IRAddrValidTo], T_4.DPC AS IRAddrDPC, 
                      T_7.LONGDESCRIPTION AS [IRAddrInfoSrc], T_4.LOT AS IRAddrLOT, T_8.LONGDESCRIPTION AS IRAddrNZSuburb, 
                      T_9.LONGDESCRIPTION AS IRAddrNZCity, T_10.LONGDESCRIPTION AS IRAddrRegion, 
                      CASE T_3.SEASONAL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRAddrSeasonal], 
                      T_3.SEASONAL_FROM AS [IRAddrSeaFrom], T_3.SEASONAL_TO AS [IRAddrSeaTo], 
                      CASE T_3.SENDMAIL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRSendMail], 
                      T_11.LONGDESCRIPTION AS [IRAddrType], T_12.SHORTDESCRIPTION AS [IRAddrState], T_4.POST_CODE AS [IRAddrZip], 
                      CASE T_2.HON_MEM_ACKNOWLEDGE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRAck],
                      CASE T_2.SOFTCREDIT_GIFTS WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRAutoSoftCredit],
                      case	when  ISDATE(T_13.BIRTH_DATE)=1 AND LEN(T_13.BIRTH_DATE)=8 then cast(dbo.dateformat(T_13.BIRTH_DATE, 'MM/DD/YYYY') AS varchar(10))
							when  ISDATE(T_13.BIRTH_DATE)=1 AND LEN(T_13.BIRTH_DATE)=6 then cast(RIGHT(T_13.BIRTH_DATE,2) +'/'+ LEFT(T_13.BIRTH_DATE,4) as varchar(10)) 
							when  ISDATE(T_13.BIRTH_DATE)=1 AND LEN(T_13.BIRTH_DATE)=4 then cast(T_13.BIRTH_DATE as varchar(10))
					        else null end as  [IRBDay], 
					   T_14.LONGDESCRIPTION AS [IRContactType], 

		  			   case when  ISDATE(T_2.DATE_FROM)=1 AND LEN(T_2.DATE_FROM)=8 then cast(dbo.dateformat(T_2.DATE_FROM, 'MM/DD/YYYY') AS varchar(10))
					   when  ISDATE(T_2.DATE_FROM)=1 AND LEN(T_2.DATE_FROM)=6 then cast(RIGHT(T_2.DATE_FROM,2) +'/'+ LEFT(T_2.DATE_FROM,4) AS varchar(10))
					   when  ISDATE(T_2.DATE_FROM)=1 AND LEN(T_2.DATE_FROM)=4 then cast(T_2.DATE_FROM AS varchar(10))
					   else null end AS  [IRFromDate],
					   
					   case when  ISDATE(T_2.DATE_TO)=1 AND LEN(T_2.DATE_TO)=8 then cast(dbo.dateformat(T_2.DATE_TO, 'MM/DD/YYYY') AS varchar(10))
					   when  ISDATE(T_2.DATE_TO)=1 AND LEN(T_2.DATE_TO)=6 then cast(RIGHT(T_2.DATE_TO,2) +'/'+ LEFT(T_2.DATE_TO,4) AS varchar(10))
					   when  ISDATE(T_2.DATE_TO)=1 AND LEN(T_2.DATE_TO)=4 then cast(T_2.DATE_TO AS varchar(10))
					   else null end AS  [IRToDate],

                      CASE T_13.DECEASED WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRDeceased], 
                       case when  ISDATE(T_13.DECEASED_DATE)=1 AND LEN(T_13.DECEASED_DATE)=8 then cast(dbo.dateformat(T_13.DECEASED_DATE, 'MM/DD/YYYY') AS varchar(10))
						   when  ISDATE(T_13.DECEASED_DATE)=1 AND LEN(T_13.DECEASED_DATE)=6 then cast(RIGHT(T_13.DECEASED_DATE,2) +'/'+ LEFT(T_13.DECEASED_DATE,4) AS varchar(10))
						   when  ISDATE(T_13.DECEASED_DATE)=1 AND LEN(T_13.DECEASED_DATE)=4 then cast(T_13.DECEASED_DATE AS varchar(10))
						   else null end AS  [IRDecDate],
                      CASE T_2.DO_NOT_MAIL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRNoMail], 
                      T_15.FIRST_NAME AS [IRFirstName], 
                      CASE T_13.SEX WHEN 1 THEN 'Male' WHEN 2 THEN 'Female' WHEN 3 THEN 'Unknown' ELSE '' END AS [IRGender], 
                      CASE T_2.IS_CONTACT WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRIsContact], 
                      CASE T_2.IS_EMPLOYEE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRIsEmp], 
                      CASE T_2.IS_HEADOFHOUSEHOLD WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRIsHH], 
                      CASE T_2.IS_PRIMARY WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRIsPrimary], 
                      CASE T_2.IS_SPOUSE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRIsSpouse], 
                      T_13.LAST_NAME AS [IRLastName], T_13.MAIDEN_NAME AS [IRMaidName], 
                      T_15.MIDDLE_NAME AS [IRMidName], T_13.NICKNAME AS [IRNickname], 
                      T_2.NOTES AS [IRNotes], T_2.POSITION AS [IRPos], 
                      CASE T_13.PRIMARY_ADDRESSEE_EDIT WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRPrimAddEdit], 
                      T_13.PRIMARY_ADDRESSEE_ID AS [IRPrimAddID], dbo.Query_GetAddrSal(T_13.ID, 
                      T_13.PRIMARY_ADDRESSEE_EDIT, T_13.PRIMARY_ADDRESSEE_ID, T_13.PRIMARY_ADDRESSEE, '01/25/2007') 
                      AS [IRPrimAddText], 
                      CASE T_13.PRIMARY_SALUTATION_EDIT WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRPrimSalEdit], 
                      T_13.PRIMARY_SALUTATION_ID AS [IRPrimSalID], dbo.Query_GetAddrSal(T_13.ID, T_13.PRIMARY_SALUTATION_EDIT, 
                      T_13.PRIMARY_SALUTATION_ID, T_13.PRIMARY_SALUTATION, '01/25/2007') AS [IRPrimSalText], 
                      CASE T_2.PRINT_ORG WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRPrintOrg],
                       CASE T_2.PRINT_POSITION WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS [IRPrintPos], 
                      T_16.LONGDESCRIPTION AS [IRProf], 
                      CASE WHEN T_17.LONGDESCRIPTION IS NULL THEN 'NULL' ELSE T_17.LONGDESCRIPTION END AS [IRRecip], 
                      CASE WHEN T_18.LONGDESCRIPTION IS NULL THEN 'NULL' ELSE T_18.LONGDESCRIPTION END AS [IRRelat], 
                      
                      T_19.LONGDESCRIPTION AS [IRSuff1], T_20.LONGDESCRIPTION AS [IRSuff2], 
                      T_21.LONGDESCRIPTION AS [IRTitl1], T_22.LONGDESCRIPTION AS [IRTitl2], 
                      dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'IR' [RE_DB_Tbl],
                      'NH' + cast(row_number() over (order by T_2.IMPORT_ID) as char) as ID    
FROM         dbo.CONSTIT_RELATIONSHIPS AS CONSTIT_RELATIONSHIPS 
			INNER JOIN dbo.RECORDS AS T_1 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_1.ID LEFT OUTER JOIN
                      dbo.QUERY_IND_RELATIONSHIPS AS T_2 ON CONSTIT_RELATIONSHIPS.ID = T_2.ID LEFT OUTER JOIN
                      dbo.CONSTIT_ADDRESS AS T_3 ON T_2.CONSTIT_ADDRESS_ID = T_3.ID LEFT OUTER JOIN
                      dbo.ADDRESS AS T_4 ON T_3.ADDRESS_ID = T_4.ID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_5 ON T_4.COUNTRY = T_5.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_6 ON T_4.COUNTY = T_6.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_7 ON T_3.INFO_SOURCE = T_7.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_8 ON T_4.SUBURB = T_8.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_9 ON T_4.NZCITY = T_9.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_10 ON T_4.REGION = T_10.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_11 ON T_3.TYPE = T_11.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.STATES AS T_12 ON T_4.STATE = T_12.SHORTDESCRIPTION LEFT OUTER JOIN
                      
                      dbo.RECORDS AS T_13 ON T_2.RELATION_ID = T_13.ID LEFT OUTER JOIN
					  dbo.CONSTIT_RELATIONSHIPS AS T_23 ON T_2.RECIPROCAL_ID = T_23.ID LEFT OUTER JOIN

                      dbo.TABLEENTRIES AS T_14 ON T_2.CONTACT_TYPE = T_14.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.SEARCHNAME AS T_15 ON T_13.ID = T_15.RECORDS_ID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_16 ON T_2.PROFESSION = T_16.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_17 ON T_2.RECIP_RELATION_CODE = T_17.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_18 ON T_2.RELATION_CODE = T_18.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_19 ON T_13.SUFFIX_1 = T_19.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_20 ON T_13.SUFFIX_2 = T_20.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_21 ON T_13.TITLE_1 = T_21.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_22 ON T_13.TITLE_2 = T_22.TABLEENTRIESID
WHERE     ((T_2.IMPORT_ID IS NOT NULL) AND (T_2.IS_HEADOFHOUSEHOLD = 0) AND (T_2.IS_PRIMARY = 0) AND (T_2.IS_SPOUSE = - 1)) 
                     /* (T_13.IS_CONSTITUENT = 0)*/ OR
          ((T_2.IMPORT_ID IS NOT NULL) AND (T_2.IS_HEADOFHOUSEHOLD = 0) AND (T_2.IS_PRIMARY = 0) AND (T_2.IS_SPOUSE = 0))

GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 26.
CREATE VIEW dbo.HC_Ind_Relat_Phone


AS
SELECT     T_5.IMPORT_ID AS [IRPhoneImpID], T_7.LONGDESCRIPTION AS IRPhoneType, 
                      T_1.IMPORT_ID AS [IRPhoneIRImpID], convert(varchar(255), T_6.NUM) AS IRPhoneNum, 
					case T_6.DO_NOT_CALL when -1 then 'TRUE' else 'FALSE' end as IRPhoneDoNotCall,
                      dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'RP' [RE_DB_Tbl],
                      row_number() over (order by T_5.IMPORT_ID) as ID 

FROM         dbo.CONSTIT_RELATIONSHIPS CONSTIT_RELATIONSHIPS INNER JOIN
                      dbo.QUERY_IND_RELATIONSHIPS T_1 ON CONSTIT_RELATIONSHIPS.ID = T_1.ID INNER JOIN
                      dbo.RECORDS T_2 ON T_1.RELATION_ID = T_2.ID INNER JOIN
                      dbo.RECORDS T_3 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_3.ID LEFT OUTER JOIN
                      dbo.CONSTIT_ADDRESS T_4 ON T_1.CONSTIT_ADDRESS_ID = T_4.ID LEFT OUTER JOIN
                      dbo.CONSTIT_ADDRESS_PHONES T_5 ON T_4.ID = T_5.CONSTITADDRESSID LEFT OUTER JOIN
                      dbo.PHONES T_6 ON T_5.PHONESID = T_6.PHONESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES T_7 ON T_6.PHONETYPEID = T_7.TABLEENTRIESID
WHERE     (T_2.IMPORT_ID IS NOT NULL) AND (T_5.IMPORT_ID IS NOT NULL)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



-- STORED PROCEDURE FOR PHONE NUMBERS STORED IN INDIVIDUAL RELATIONSHIPS.

		CREATE PROCEDURE HC_PivotWizard_IRPhones_p
		   @P_Row_Field    VARCHAR(255),
		   @P_Column_Field VARCHAR(255),
		   @P_Value        VARCHAR(4000),
		   @P_Into         VARCHAR(4000),
		   @P_From         VARCHAR(4000),
		   @P_Where        VARCHAR(4000) = '1=1'

		AS

		  DECLARE @SQL NVARCHAR(4000)

		  -- Build SQL statment that upload @Columns string 
		  -- with @P_Column_Filed values
		  CREATE TABLE #TEMP  (ColumnField varchar(255))
		  SET @sql ='SELECT DISTINCT '+@P_Column_Field+' AS ColumnField'+
					  ' FROM '+@P_From+
					  ' WHERE '+@P_Where+
					  ' ORDER BY '+@P_Column_Field
		  INSERT INTO #TEMP
		  EXEC(@sql)
		  --PRINT @sql

		  -- Check count of columns
		  DECLARE @Count_Columns int
		  SELECT @Count_Columns = COUNT(*) FROM #Temp
		  IF (@Count_Columns<1) OR (@Count_Columns>255)  BEGIN
			  DROP TABLE #Temp
			  RAISERROR('%d is invalid columns amount. Valid is 1-255',
						16,1,@Count_columns)
			  RETURN
		  END
		  -- Upload @Columns from #Temp
		  DECLARE @Columns VARCHAR(8000),
				  @Column_Field VARCHAR(8000)

		  SET @Columns = ''
		  DECLARE Column_cursor CURSOR LOCAL FOR
		  SELECT CAST(ColumnField AS VARCHAR(60))
		  FROM #Temp
		  OPEN Column_cursor
		  FETCH NEXT FROM Column_cursor
		  INTO @Column_Field 
		  WHILE @@FETCH_STATUS = 0 BEGIN
			  SET @Columns = @Columns +
				' MAX('+
					 ' CASE WHEN '+@P_Column_Field+'='''+ @Column_Field+''''+
					 ' THEN '+@P_Value+
					 ' ELSE null END'+
					 ') AS ['+ @Column_Field +'], '
			  FETCH NEXT FROM Column_cursor
			  INTO @Column_Field
		  END
		  CLOSE Column_cursor
		  DEALLOCATE Column_cursor
		  DROP TABLE #Temp

		  IF @Columns='' RETURN 1
		  SET @Columns = Left(@Columns,Len(@Columns)-1)

		  -- Build Pivot SQL statment
		  DECLARE @Pivot_SQL VARCHAR(8000)
		  SET @Pivot_SQL =              'SELECT '  +@P_Row_Field+', '+@Columns
		  SET @Pivot_SQL = @Pivot_SQL +' INTO '    +@P_Into  
		  SET @Pivot_SQL = @Pivot_SQL +' FROM '    +@P_From
		  SET @Pivot_SQL = @Pivot_SQL +' WHERE '   +@P_Where
		  SET @Pivot_SQL = @Pivot_SQL +' GROUP BY '+@P_Row_Field
		  SET @Pivot_SQL = @Pivot_SQL +' ORDER BY '+@P_Row_Field
		  SET @Pivot_SQL = @Pivot_SQL + '#'

		  IF Right(@Pivot_SQL,1)<>'#'
		  BEGIN
			 RAISERROR('SQL statement is too long. It must be less
						than 8000 charachter!',16,1)
			 RETURN 1
		  END
		  SET @Pivot_SQL = Left(@Pivot_SQL,Len(@Pivot_SQL)-1)

		  -- PRINT @Pivot_SQL
		  EXEC(@Pivot_SQL)

		  RETURN 0
		GO  

		--  execuate the sp. 
		exec HC_PivotWizard_IRPhones_p 'IRPhoneIRImpID', 
									   'IRPhoneType',
									   'IRPhoneNum',
    								   'dbo.HC_Ind_Relat_Phone_t',
									   'dbo.HC_Ind_Relat_Phone',
									   'IRPhoneImpID is not null'  

 
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- VIEW 27.
CREATE VIEW dbo.HC_Gift_Installments_v
AS
SELECT		GIFT_Installment.Amount AS GFInsAmt, GIFT_Installment.Dte AS GFInsDate, Gift.IMPORT_ID AS GFLink, 
			GIFT_Installment.Import_Id AS GFInsImpID, 
			GIFT_Installment.InstallmentId,
            dbo.Gift_GetInstallmentBalance2(GIFT_Installment.PledgeID,GIFT_Installment.amount,GIFT_Installment.InstallmentID) GFInsBalance,

			dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'GI' [RE_DB_Tbl],
			row_number() over (order by Gift.IMPORT_ID) as ID
                      
FROM         dbo.GIFT Gift LEFT OUTER JOIN
                      dbo.Installment GIFT_Installment ON Gift.ID = GIFT_Installment.PledgeId
WHERE     (Gift.TYPE NOT IN (28, 32, 33) OR
                      Gift.TYPE IS NULL) AND (GIFT_Installment.Import_Id IS NOT NULL)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- VIEW 28.
CREATE VIEW dbo.HC_Org_Relat

AS
SELECT				  T_1.IMPORT_ID AS ORImpID, T_2.IMPORT_ID AS ImportID, T_2.CONSTITUENT_ID AS ConsID, 
					  CASE T_16.IS_CONSTITUENT WHEN - 1 THEN T_16.IMPORT_ID ELSE '' END AS ORLink, T_23.IMPORT_ID AS [RRImpID], 
					  T_16.ORG_NAME AS ORFullName, 

		  			-- ADDRESS IDs for reference and linking purposes. 
					T_3.ADDRESS_ID as REFERENCE_ADDRESS_ID, T_3.ID as REFERENCE_ID, T_3.IMPORT_ID REFERENCE_IMPORT_ID,

					  T_4.CARRIER_ROUTE AS ORAddrCART, dbo.ADDRESSLINE(1, T_4.ADDRESS_BLOCK) AS [ORAddrLine1], 
                      dbo.ADDRESSLINE(2, T_4.ADDRESS_BLOCK) AS [ORAddrLine2], dbo.ADDRESSLINE(3, T_4.ADDRESS_BLOCK) AS [ORAddrLine3], 
                      dbo.ADDRESSLINE(4, T_4.ADDRESS_BLOCK) AS [ORAddrLine4], dbo.ADDRESSLINE(5, T_4.ADDRESS_BLOCK) AS [ORAddrLine5], 
                      T_4.CITY AS ORAddrCity, T_5.LONGDESCRIPTION AS ORAddrCountry, T_6.LONGDESCRIPTION AS ORAddrCounty, 
                      T_3.DATE_FROM AS ORAddrValidFrom, T_3.DATE_TO AS ORAddrValidTo, T_4.DPC AS ORAddrDPC, T_7.LONGDESCRIPTION AS ORAddrInfoSrc, 
                      T_4.LOT AS ORAddrLot, T_8.LONGDESCRIPTION AS ORAddrNZCity, T_9.LONGDESCRIPTION AS ORAddrNZSuburb, 
                      T_10.LONGDESCRIPTION AS ORAddrRegion, CASE T_3.SEASONAL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORAddrSeasonal, 
                      T_3.SEASONAL_FROM AS ORAddrSeaFrom, T_3.SEASONAL_TO AS ORAddrSeaTo, 
                      CASE T_3.SENDMAIL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORSendMail, T_11.SHORTDESCRIPTION AS ORAddrState, 
                      T_12.LONGDESCRIPTION AS ORAddrType, T_4.POST_CODE AS ORAddrZIP, 
                      CASE T_1.HON_MEM_ACKNOWLEDGE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORHMAck, 
                      CASE T_1.SOFTCREDIT_GIFTS WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORAutoSoftCredit, 
                      T_13.LONGDESCRIPTION AS ORContactType, 
                      
					   case when  ISDATE(T_1.DATE_FROM)=1 AND LEN(T_1.DATE_FROM)=8 then cast(dbo.dateformat(T_1.DATE_FROM, 'MM/DD/YYYY') AS varchar(10))
					   when  ISDATE(T_1.DATE_FROM)=1 AND LEN(T_1.DATE_FROM)=6 then cast(RIGHT(T_1.DATE_FROM,2) +'/'+ LEFT(T_1.DATE_FROM,4) AS varchar(10))
					   when  ISDATE(T_1.DATE_FROM)=1 AND LEN(T_1.DATE_FROM)=4 then cast(T_1.DATE_FROM AS varchar(10))
					   else null end AS  [ORFromDate],
					   
					   case when  ISDATE(T_1.DATE_TO)=1 AND LEN(T_1.DATE_TO)=8 then cast(dbo.dateformat(T_1.DATE_TO, 'MM/DD/YYYY') AS varchar(10))
					   when  ISDATE(T_1.DATE_TO)=1 AND LEN(T_1.DATE_TO)=6 then cast(RIGHT(T_1.DATE_TO,2) +'/'+ LEFT(T_1.DATE_TO,4) AS varchar(10))
					   when  ISDATE(T_1.DATE_TO)=1 AND LEN(T_1.DATE_TO)=4 then cast(T_1.DATE_TO AS varchar(10))
					   else null end AS  [ORToDate],
                    
                      CASE T_1.DO_NOT_MAIL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORNoMail, T_1.FISCAL_YEAR_STARTS AS ORFiscalYear, 
                      T_14.LONGDESCRIPTION AS ORIncome, T_15.LONGDESCRIPTION AS ORIndustry, 
                      CASE T_1.IS_CONTACT WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORIsContact, 
                      CASE T_1.IS_EMPLOYEE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORIsEmp, 
                      CASE T_1.IS_PRIMARY WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORIsPrimary, 
                      REPLACE(REPLACE(REPLACE(CAST(T_1.MATCH_NOTES AS varchar), CHAR(13), ' '), CHAR(10), ' '), CHAR(9), ' ') AS ORMtcNotes, 
                      T_1.MATCHING_FACTOR AS ORMtcFactor, T_1.MAX_MATCH_ANNUAL AS ORMaxMtcAnn, T_1.MAX_MATCH_PER_GIFT AS ORMaxMtcGift, 
                      T_1.MAX_MATCH_TOTAL AS ORMaxMtcTotal, T_1.MIN_MATCH_ANNUAL AS ORMinMtcAnn, T_1.MIN_MATCH_PER_GIFT AS ORMinMtcGift, 
                      T_1.MIN_MATCH_TOTAL AS ORMinMtcTot, 
                     REPLACE(REPLACE(REPLACE(CAST(T_1.NOTES AS varchar(8000)), 
                      CHAR(13), ' '), CHAR(10), ' '), CHAR(9), ' ') AS ORNotes, 
                      CASE T_1.MATCHING_GIFT_FLAG WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS OROrgMatchesGift, T_1.POSITION AS ORPos, 
                      CASE T_1.PRINT_ORG WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORPrintOrg, 
                      CASE T_1.PRINT_POSITION WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORPrintPos, T_17.LONGDESCRIPTION AS ORProf, 
                      CASE WHEN T_18.LONGDESCRIPTION IS NULL THEN 'NULL' ELSE T_18.LONGDESCRIPTION  END AS ORRecip, 
                      CASE WHEN T_19.LONGDESCRIPTION IS NULL THEN 'NULL' ELSE T_19.LONGDESCRIPTION  END AS ORRelat, 
                      
                      
                      dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'OR' [RE_DB_Tbl],
                      row_number() over (order by T_1.IMPORT_ID) as ID 
FROM         dbo.CONSTIT_RELATIONSHIPS AS CONSTIT_RELATIONSHIPS INNER JOIN
                      dbo.QUERY_ORG_RELATIONSHIPS AS T_1 ON CONSTIT_RELATIONSHIPS.ID = T_1.ID INNER JOIN
                      dbo.RECORDS AS T_2 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_2.ID LEFT OUTER JOIN
                      dbo.CONSTIT_ADDRESS AS T_3 ON T_1.CONSTIT_ADDRESS_ID = T_3.ID LEFT OUTER JOIN
                      dbo.ADDRESS AS T_4 ON T_3.ADDRESS_ID = T_4.ID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_5 ON T_4.COUNTRY = T_5.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_6 ON T_4.COUNTY = T_6.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_7 ON T_3.INFO_SOURCE = T_7.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_8 ON T_4.NZCITY = T_8.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_9 ON T_4.SUBURB = T_9.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_10 ON T_4.REGION = T_10.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.STATES AS T_11 ON T_4.STATE = T_11.SHORTDESCRIPTION LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_12 ON T_3.TYPE = T_12.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_13 ON T_1.CONTACT_TYPE = T_13.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_14 ON T_1.INCOME = T_14.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_15 ON T_1.INDUSTRY = T_15.TABLEENTRIESID LEFT OUTER JOIN
                      
                      dbo.RECORDS AS T_16 ON T_1.RELATION_ID = T_16.ID LEFT OUTER JOIN
					  dbo.CONSTIT_RELATIONSHIPS AS T_23 ON T_1.RECIPROCAL_ID = T_23.ID LEFT OUTER JOIN

                      dbo.TABLEENTRIES AS T_17 ON T_1.PROFESSION = T_17.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_18 ON T_1.RECIP_RELATION_CODE = T_18.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_19 ON T_1.RELATION_CODE = T_19.TABLEENTRIESID
WHERE				 (((T_1.IMPORT_ID IS NOT NULL)) AND (T_2.IS_CONSTITUENT  =  -1))  --new line corrected.
					 --(T_1.IMPORT_ID IS NOT NULL) AND (T_1.IS_PRIMARY = 0) OR
                     --(T_1.IMPORT_ID IS NOT NULL) AND (T_1.IS_PRIMARY = - 1) AND (T_16.IS_CONSTITUENT = 0)
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO






-- VIEW 29.
CREATE VIEW dbo.HC_Org_Relat_Phone
AS
SELECT     T_4.IMPORT_ID AS [ORPhoneImpID], 
T_6.LONGDESCRIPTION AS [ORPhoneType], 
                      T_1.IMPORT_ID AS [ORPhoneORImpID], 
                      convert(varchar(255), T_5.NUM) AS [ORPhoneNum], 
					  case T_5.DO_NOT_CALL when -1 then 'TRUE' else 'FALSE' end as IRPhoneDoNotCall,
                      dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'OP' [RE_DB_Tbl],
                      row_number() over (order by T_4.IMPORT_ID) as ID
FROM         dbo.CONSTIT_RELATIONSHIPS CONSTIT_RELATIONSHIPS INNER JOIN
                      dbo.QUERY_ORG_RELATIONSHIPS T_1 ON CONSTIT_RELATIONSHIPS.ID = T_1.ID INNER JOIN
                      dbo.RECORDS T_2 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_2.ID LEFT OUTER JOIN
                      dbo.CONSTIT_ADDRESS T_3 ON T_1.CONSTIT_ADDRESS_ID = T_3.ID LEFT OUTER JOIN
                      dbo.CONSTIT_ADDRESS_PHONES T_4 ON T_3.ID = T_4.CONSTITADDRESSID LEFT OUTER JOIN
                      dbo.PHONES T_5 ON T_4.PHONESID = T_5.PHONESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES T_6 ON T_5.PHONETYPEID = T_6.TABLEENTRIESID
WHERE     (T_4.IMPORT_ID IS NOT NULL) AND (T_1.IMPORT_ID IS NOT NULL)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- STORED PROCEDURE FOR PHONE NUMBERS STORED IN ORGANIZATIONS RELATIONSHIPS.

		CREATE PROCEDURE HC_PivotWizard_ORPhones_p
		   @P_Row_Field    VARCHAR(255),
		   @P_Column_Field VARCHAR(255),
		   @P_Value        VARCHAR(4000),
		   @P_Into         VARCHAR(4000),
		   @P_From         VARCHAR(4000),
		   @P_Where        VARCHAR(4000) = '1=1'

		AS

		  DECLARE @SQL NVARCHAR(4000)

		  -- Build SQL statment that upload @Columns string 
		  -- with @P_Column_Filed values
		  CREATE TABLE #TEMP  (ColumnField varchar(255))
		  SET @sql ='SELECT DISTINCT '+@P_Column_Field+' AS ColumnField'+
					  ' FROM '+@P_From+
					  ' WHERE '+@P_Where+
					  ' ORDER BY '+@P_Column_Field
		  INSERT INTO #TEMP
		  EXEC(@sql)
		  --PRINT @sql

		  -- Check count of columns
		  DECLARE @Count_Columns int
		  SELECT @Count_Columns = COUNT(*) FROM #Temp
		  IF (@Count_Columns<1) OR (@Count_Columns>255)  BEGIN
			  DROP TABLE #Temp
			  RAISERROR('%d is invalid columns amount. Valid is 1-255',
						16,1,@Count_columns)
			  RETURN
		  END
		  -- Upload @Columns from #Temp
		  DECLARE @Columns VARCHAR(8000),
				  @Column_Field VARCHAR(8000)

		  SET @Columns = ''
		  DECLARE Column_cursor CURSOR LOCAL FOR
		  SELECT CAST(ColumnField AS VARCHAR(60))
		  FROM #Temp
		  OPEN Column_cursor
		  FETCH NEXT FROM Column_cursor
		  INTO @Column_Field 
		  WHILE @@FETCH_STATUS = 0 BEGIN
			  SET @Columns = @Columns +
				' MAX('+
					 ' CASE WHEN '+@P_Column_Field+'='''+ @Column_Field+''''+
					 ' THEN '+@P_Value+
					 ' ELSE null END'+
					 ') AS ['+ @Column_Field +'], '
			  FETCH NEXT FROM Column_cursor
			  INTO @Column_Field
		  END
		  CLOSE Column_cursor
		  DEALLOCATE Column_cursor
		  DROP TABLE #Temp

		  IF @Columns='' RETURN 1
		  SET @Columns = Left(@Columns,Len(@Columns)-1)

		  -- Build Pivot SQL statment
		  DECLARE @Pivot_SQL VARCHAR(8000)
		  SET @Pivot_SQL =              'SELECT '  +@P_Row_Field+', '+@Columns
		  SET @Pivot_SQL = @Pivot_SQL +' INTO '    +@P_Into  
		  SET @Pivot_SQL = @Pivot_SQL +' FROM '    +@P_From
		  SET @Pivot_SQL = @Pivot_SQL +' WHERE '   +@P_Where
		  SET @Pivot_SQL = @Pivot_SQL +' GROUP BY '+@P_Row_Field
		  SET @Pivot_SQL = @Pivot_SQL +' ORDER BY '+@P_Row_Field
		  SET @Pivot_SQL = @Pivot_SQL + '#'

		  IF Right(@Pivot_SQL,1)<>'#'
		  BEGIN
			 RAISERROR('SQL statement is too long. It must be less
						than 8000 charachter!',16,1)
			 RETURN 1
		  END
		  SET @Pivot_SQL = Left(@Pivot_SQL,Len(@Pivot_SQL)-1)

		  -- PRINT @Pivot_SQL
		  EXEC(@Pivot_SQL)

		  RETURN 0
		GO  

		--  execuate the sp. 
		exec HC_PivotWizard_ORPhones_p 'ORPhoneORImpID', 
									   'ORPhoneType',
									   'ORPhoneNum',
    								   'dbo.HC_Org_Relat_Phone_t',
									   'dbo.HC_Org_Relat_Phone',
									   'ORPhoneImpID is not null'  


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- VIEW 30.
CREATE VIEW dbo.HC_Gift_SoftCredit_v

AS
SELECT     GIFT_GIFTSOFTCREDIT.Import_Id AS SoftCredImpID, GIFT.IMPORT_ID AS [GFLink], 
                      T_1.IMPORT_ID AS [SoftCredRecip], T_1.CONSTITUENT_ID AS ConsID, GIFT_GIFTSOFTCREDIT.Amount AS SoftCredAmt, 
                      dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'SC' [RE_DB_Tbl],
                      row_number() over (order by GIFT.IMPORT_ID) as ID
FROM         dbo.GIFT GIFT INNER JOIN
                      dbo.GiftSoftCredit GIFT_GIFTSOFTCREDIT ON GIFT.ID = GIFT_GIFTSOFTCREDIT.GiftId LEFT OUTER JOIN
                      dbo.RECORDS T_1 ON GIFT_GIFTSOFTCREDIT.ConstitId = T_1.ID
WHERE     (GIFT.TYPE NOT IN (28, 32, 33)) OR
                      (GIFT.TYPE IS NULL)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- VIEW 31.
CREATE VIEW dbo.HC_Cons_SolicitCodes_v
AS
SELECT     T_2.LONGDESCRIPTION AS [SolicitCode], RECORDS.IMPORT_ID AS [ImportID], 
		   RECORDS.CONSTITUENT_ID AS ConsID, 
           dbo.hc_OrgName_v() AS [RE_DB_Owner],  dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'OC' [RE_DB_Tbl],
                      row_number() over (order by T_1.RECORDSID) as ID

FROM         dbo.RECORDS RECORDS LEFT OUTER JOIN
                      dbo.CONSTITUENT_SOLICITCODES T_1 ON RECORDS.ID = T_1.RECORDSID LEFT OUTER JOIN
                      dbo.TABLEENTRIES T_2 ON T_1.SOLICIT_CODE = T_2.TABLEENTRIESID
WHERE     (RECORDS.IS_CONSTITUENT = - 1) AND (T_2.LONGDESCRIPTION IS NOT NULL)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 32.
CREATE VIEW dbo.HC_Gift_SplitGift_v
AS
SELECT     dbo.GiftSplit.Import_Id AS [GSplitImpID], dbo.GIFT.IMPORT_ID AS [GFImpID], 
					  dbo.GiftSplit.Sequence AS [GFSplitSequence], 
                      dbo.GiftSplit.Amount AS [GSplitAmt], dbo.FUND.FUND_ID AS [GSplitFund], 
                      dbo.APPEAL.APPEAL_ID AS [GSplitAppeal], dbo.CAMPAIGN.CAMPAIGN_ID AS [GSplitCamp], 
                      dbo.Package.Package_ID AS [GSplitPkg], 
				      dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'GT' [RE_DB_Tbl],
                      row_number() over (order by GIFT.IMPORT_ID, dbo.GiftSplit.Sequence) as ID		
FROM         dbo.GIFT INNER JOIN
                      dbo.GiftSplit ON dbo.GIFT.ID = dbo.GiftSplit.GiftId LEFT OUTER JOIN
                      dbo.Package ON dbo.GiftSplit.PackageId = dbo.Package.ID LEFT OUTER JOIN
                      dbo.FUND ON dbo.FUND.ID = dbo.GiftSplit.FundId LEFT OUTER JOIN
                      dbo.APPEAL ON dbo.APPEAL.ID = dbo.GiftSplit.AppealId LEFT OUTER JOIN
                      dbo.CAMPAIGN ON dbo.CAMPAIGN.ID = dbo.GiftSplit.CampaignId
WHERE     (dbo.GIFT.TYPE NOT IN (28, 32, 33)) OR (dbo.GIFT.TYPE IS NULL)
 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- VIEW 33.
CREATE VIEW dbo.HC_Cons_Tribute_v 
AS
SELECT     records_tribute.Import_ID [TRImpID], T_1.LONGDESCRIPTION [TRType], 
                      RECORDS.CONSTITUENT_ID [ConsID], RECORDS.IMPORT_ID [ImportID], 
                      RECORDS_TRIBUTE.FROM_DATE [TRDateFrom], RECORDS_TRIBUTE.TO_DATE [TRDateTo], 
                      records_tribute_FUND.FUND_ID [TRDefaultFund], RECORDS_TRIBUTE.DESCRIPTION [TRDesc], 
                      CASE RECORDS_TRIBUTE.ACTIVE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [TRGiftGiven], 
                      RECORDS_TRIBUTE.NOTES [TRNotes], 
                      dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'TR' [RE_DB_Tbl],
                      row_number() over (order by RECORDS.IMPORT_ID) as ID
FROM         DBO.RECORDS AS RECORDS LEFT OUTER JOIN
                      DBO.TRIBUTE AS RECORDS_TRIBUTE ON RECORDS.ID = RECORDS_TRIBUTE.RECORDS_ID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS T_1 ON RECORDS_TRIBUTE.TRIBUTE_TYPE = T_1.TABLEENTRIESID LEFT OUTER JOIN
                      DBO.FUND AS records_tribute_FUND ON RECORDS_TRIBUTE.DEFAULT_FUND = records_tribute_FUND.ID
WHERE     ((RECORDS.IS_CONSTITUENT = - 1)) AND records_tribute.Import_ID IS NOT NULL

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
  -- VIEW 34.
CREATE VIEW dbo.HC_Gift_Tribute_v
AS
SELECT     gift_gift_tribute.import_ID [TribGiftImpID], GIFT.IMPORT_ID [GFTLink], 
                      tribute.import_ID [TribImpID], 
                      CASE GIFT_GIFT_TRIBUTE.ACKNOWLEDGEMENT WHEN 1 THEN 'Acknowledged' WHEN 2 THEN 'Not Acknowledged' WHEN 3 THEN 'Do Not Acknowledge'
                       ELSE '' END [GFTribAck], T_1.LONGDESCRIPTION [GFTribType],  Tribute.Description as GFTribDesc,
                       dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'TG' [RE_DB_Tbl],
                       row_number() over (order by GIFT.IMPORT_ID) as ID 
FROM         DBO.GIFT AS GIFT LEFT OUTER JOIN
                      DBO.GIFT_TRIBUTE AS GIFT_GIFT_TRIBUTE ON GIFT.ID = GIFT_GIFT_TRIBUTE.GIFT_ID LEFT OUTER JOIN
                      DBO.tribute AS TRibute ON GIFT_GIFT_TRIBUTE.TRIBUTE_ID = TRIBUTE.ID LEFT OUTER JOIN
                      DBO.TABLEENTRIES AS T_1 ON GIFT_GIFT_TRIBUTE.TRIBUTE_TYPE = T_1.TABLEENTRIESID
WHERE     (((GIFT.TYPE NOT IN (28, 32, 33) OR
                      (GIFT.TYPE IS NULL)))) AND gift_gift_tribute.import_ID IS NOT NULL

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- VIEW 35.
CREATE VIEW dbo.HC_Cons_Action_Solicitor_v
AS
SELECT 
 T_2.IMPORT_ID [ACSolImpID],
 RECORDS_ACTIONS.IMPORT_ID [ACImpID],
 dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'AL' [RE_DB_Tbl],
 row_number() over (order by T_2.IMPORT_ID) as ID

FROM 
DBO.RECORDS AS RECORDS 
LEFT OUTER JOIN DBO.ACTIONS AS RECORDS_ACTIONS ON RECORDS.ID = RECORDS_ACTIONS.RECORDS_ID 
LEFT OUTER JOIN DBO.ACTION_SOLICITOR AS T_1 ON RECORDS_ACTIONS.ID = T_1.ACTION_ID 
LEFT OUTER JOIN DBO.RECORDS AS T_2 ON T_1.RECORDS_ID = T_2.ID

WHERE 
((RECORDS.IS_CONSTITUENT  =  -1)) AND T_2.ID IS NOT NULL

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- VIEW 36.
CREATE VIEW dbo.HC_Cons_Solicitor_v

AS
SELECT 
T_1.IMPORT_ID [ASRImpID],
 T_2.IMPORT_ID [ASRLink],
 T_1.AMOUNT [ASRAmt],
 T_3.CAMPAIGN_ID [ASRCampID],
 RECORDS.CONSTITUENT_ID [ConsID],
 RECORDS.IMPORT_ID [ImportID],
 T_1.DATE_FROM [ASRDateFrom],
 T_1.DATE_TO [ASRDateTo],
 T_4.FUND_ID [ASRFundID],
 T_1.NOTES [ASRNotes],
 T_5.LONGDESCRIPTION [ASRType],
 dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'AS' [RE_DB_Tbl],
 row_number() over (order by T_1.IMPORT_ID) as ID

FROM 
DBO.RECORDS AS RECORDS INNER JOIN DBO.CONSTIT_SOLICITORS AS T_1 ON RECORDS.ID = T_1.CONSTIT_ID INNER JOIN DBO.RECORDS AS T_2 ON T_1.SOLICITOR_ID = T_2.ID LEFT OUTER JOIN DBO.CAMPAIGN AS T_3 ON T_1.CAMPAIGN_ID = T_3.ID LEFT OUTER JOIN DBO.FUND AS T_4 ON T_1.FUND_ID = T_4.ID LEFT OUTER JOIN DBO.TABLEENTRIES AS T_5 ON T_1.SOLICITOR_TYPE = T_5.TABLEENTRIESID

WHERE 
((T_2.ID IS NOT NULL) AND (RECORDS.IS_CONSTITUENT  =  -1))  


GO
SET QUOTED_IDENTIFIER OFF 
GO

SET ANSI_NULLS ON 
GO


-- VIEW 37.
CREATE VIEW dbo.HC_Educ_Relat_Major_v

AS
SELECT 
 T_4.IMPORT_ID [ESRMajESRImpID],
 T_11.IMPORTID [ESRMajImpID],
 T_22.LONGDESCRIPTION [ESRMajMajor],
 T_11.NOTES [ESRMajCom],
 dbo.hc_OrgName_v() AS [RE_DB_Owner], 
 dbo.HC_OrgInit_f() [RE_DB_OwnerShort],  'EA' [RE_DB_Tbl],
 row_number() over (order by T_4.IMPORT_ID) as ID

FROM 
DBO.EDUCATION AS T_4 
LEFT OUTER JOIN DBO.EDUCATIONMAJOR AS T_11 ON T_4.ID = T_11.PARENTID
LEFT OUTER JOIN DBO.TABLEENTRIES AS T_22 ON T_22.TABLEENTRIESID = T_11.TABLEENTRIESID 

where T_11.IMPORTID IS NOT NULL 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 38.
CREATE VIEW dbo.HC_Proposal_v
AS

SELECT				RECORDS_PROSPECT_PROPOSAL.IMPORT_ID AS PRImpID, RECORDS_PROSPECT_PROPOSAL.Proposal_Name AS PRName, 
                      RECORDS_PROSPECT_PROPOSAL.AMOUNT_ASKED AS PRAmtAsk, RECORDS_PROSPECT_PROPOSAL.AMOUNT_EXPECTED AS PRAmtExp, 
                      RECORDS_PROSPECT_PROPOSAL.AMOUNT_FUNDED AS PRAmtFund, T_1.CAMPAIGN_ID AS PRCampID, RECORDS.CONSTITUENT_ID AS ConsID, 
                      RECORDS.IMPORT_ID AS ImportID, RECORDS.SOCIAL_SECURITY_NO AS SSNum, dbo.Query_ConstitName(1, T_2.LAST_NAME, T_2.FIRST_NAME, 
                      T_2.MIDDLE_NAME, T_2.ORG_NAME, T_2.KEY_INDICATOR, T_2.CONSTITUENT_ID) AS PRContact, 
                      dbo.dateformat(RECORDS_PROSPECT_PROPOSAL.DATE_ASKED,'MM/DD/YYYY') AS PRDateAsk, 
                      dbo.dateformat(RECORDS_PROSPECT_PROPOSAL.DATE_EXPECTED,'MM/DD/YYYY')  AS PRDateExp, 
                      dbo.dateformat(RECORDS_PROSPECT_PROPOSAL.DATE_FUNDED,'MM/DD/YYYY')  AS PRDateFund, 
                      dbo.dateformat(RECORDS_PROSPECT_PROPOSAL.DATE_RATED,'MM/DD/YYYY')  AS PRDateRate, 
                      dbo.dateformat(RECORDS_PROSPECT_PROPOSAL.DEADLINE,'MM/DD/YYYY')  AS PRDeadline, 
                      T_3.FUND_ID AS PRFundID, T_4.LONGDESCRIPTION AS PRInstr, 
                      CASE RECORDS_PROSPECT_PROPOSAL.INACTIVE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS PrIsInactive, 
                      T_6.LONGDESCRIPTION AS PRPurpose, T_7.LONGDESCRIPTION AS PRRating, T_8.LONGDESCRIPTION AS PRReason, 
                      T_9.LONGDESCRIPTION AS PRStatus, T_10.LONGDESCRIPTION AS PRTypeGift,
                      dbo.dateformat(RECORDS_PROSPECT_PROPOSAL.DATE_ADDED,'MM/DD/YYYY')  AS DateAdded, 
					  dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort],  'PP' [RE_DB_Tbl],
					  row_number() over (order by RECORDS_PROSPECT_PROPOSAL.IMPORT_ID) as ID

FROM         dbo.RECORDS AS RECORDS LEFT OUTER JOIN
                      dbo.PROSPECT AS RECORDS_PROSPECT ON RECORDS.ID = RECORDS_PROSPECT.CONSTIT_ID LEFT OUTER JOIN
                      dbo.PROPOSAL AS RECORDS_PROSPECT_PROPOSAL ON 
                      RECORDS_PROSPECT.ID = RECORDS_PROSPECT_PROPOSAL.PROSPECT_ID LEFT OUTER JOIN
                      dbo.CAMPAIGN AS T_1 ON RECORDS_PROSPECT_PROPOSAL.CAMPAIGN = T_1.ID LEFT OUTER JOIN
                      dbo.RECORDS AS T_2 ON RECORDS_PROSPECT_PROPOSAL.CONTACTID = T_2.ID LEFT OUTER JOIN
                      dbo.FUND AS T_3 ON RECORDS_PROSPECT_PROPOSAL.FUND = T_3.ID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_4 ON RECORDS_PROSPECT_PROPOSAL.INSTRUMENT = T_4.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_6 ON RECORDS_PROSPECT_PROPOSAL.PURPOSE = T_6.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_7 ON RECORDS_PROSPECT_PROPOSAL.RATING = T_7.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_8 ON RECORDS_PROSPECT_PROPOSAL.REASON = T_8.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_9 ON RECORDS_PROSPECT_PROPOSAL.STATUS = T_9.TABLEENTRIESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES AS T_10 ON RECORDS_PROSPECT_PROPOSAL.GIFT_TYPE = T_10.TABLEENTRIESID
 
WHERE     (RECORDS.IS_CONSTITUENT = - 1) AND (RECORDS_PROSPECT_PROPOSAL.IMPORT_ID IS NOT NULL)
 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 39.
CREATE VIEW dbo.HC_Proposal_Notes_v

AS
SELECT 
		 T_1.Import_Id "PRNoteImpID",
		 T_1.NotepadDate "PRNoteDate",
		 T_2.LONGDESCRIPTION "PRNoteType",
		 T_1.Author "PRNoteAuth",
		 T_1.Description "PRNoteDesc",
		 T_1.ActualNotes "PRNoteNotes",
		 records_prospect_proposal.Import_ID "PRLink",
		 T_1.Title "PRNoteTitle",
		 dbo.hc_OrgName_v() AS [RE_DB_Owner], 
		 dbo.HC_OrgInit_f() [RE_DB_OwnerShort],  'PN' [RE_DB_Tbl],
		 row_number() over (order by T_1.IMPORT_ID) as ID

FROM 
		DBO.RECORDS AS records 
		INNER JOIN DBO.PROSPECT AS records_prospect ON records.ID = records_prospect.CONSTIT_ID 
		INNER JOIN DBO.PROPOSAL AS records_prospect_proposal ON records_prospect.ID = records_prospect_proposal.PROSPECT_ID 
		INNER JOIN DBO.PROPOSALNOTEPAD AS T_1 ON records_prospect_proposal.ID = T_1.PARENTID 
		LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON T_1.NOTETYPEID = T_2.TABLEENTRIESID

WHERE 
		(((T_1.Import_Id IS NOT NULL)) AND (records.IS_CONSTITUENT  =  -1))  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 40.
CREATE VIEW dbo.HC_Prospect_Financial_v

AS

	SELECT 
	RECORDS.CONSTITUENT_ID as ConsID,
	RECORDS.IMPORT_ID as ImportID,
	T_1.IMPORT_ID as PFIImportID,
	T_2.LONGDESCRIPTION as PFIType,
	T_3.LONGDESCRIPTION as PFISource,
	 
	CASE WHEN  ISDATE(T_1.DATE_ACQUIRED)=1 AND LEN(T_1.DATE_ACQUIRED)=8 THEN CAST(DBO.DATEFORMAT(T_1.DATE_ACQUIRED, 'MM/DD/YYYY') as varchar(10))
	WHEN LEN(T_1.DATE_ACQUIRED)=6 THEN CAST(RIGHT(T_1.DATE_ACQUIRED,2) +'/'+ LEFT(T_1.DATE_ACQUIRED,4) AS varchar(10))
	WHEN ISDATE(T_1.DATE_ACQUIRED)=1 AND LEN(T_1.DATE_ACQUIRED)=4 THEN cast(T_1.DATE_ACQUIRED as varchar(10))
	ELSE NULL END AS PFIDateAcq,
	
	CASE WHEN  ISDATE(T_1.DATE_ASSESSED)=1 AND LEN(T_1.DATE_ASSESSED)=8 THEN CAST(DBO.DATEFORMAT(T_1.DATE_ASSESSED, 'MM/DD/YYYY') as varchar(10))
	WHEN LEN(T_1.DATE_ASSESSED)=6 THEN CAST(RIGHT(T_1.DATE_ASSESSED,2) +'/'+ LEFT(T_1.DATE_ASSESSED,4) AS varchar(10))
	WHEN ISDATE(T_1.DATE_ASSESSED)=1 AND LEN(T_1.DATE_ASSESSED)=4 THEN cast(T_1.DATE_ASSESSED as varchar(10))
	ELSE NULL END AS PFIDateAssed,
	 
	T_1.NOTES as PFINotes,
	T_1.AMOUNT as PFIAmt,

	dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'PF' [RE_DB_Tbl],
    row_number() over (order by T_1.IMPORT_ID) as ID

	 
	FROM 
	DBO.RECORDS AS RECORDS 
	INNER JOIN DBO.PROSPECT AS RECORDS_PROSPECT ON RECORDS.ID = RECORDS_PROSPECT.CONSTIT_ID 
	INNER JOIN DBO.FINANCIAL_DATA AS T_1 ON RECORDS_PROSPECT.ID = T_1.PROSPECT_ID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON T_1.INFO_TYPE = T_2.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_3 ON T_1.INFORMATION_SOURCE = T_3.TABLEENTRIESID

	WHERE 
	((T_1.INFO_TYPE IS NOT NULL AND (RECORDS.IMPORT_ID IS NOT NULL)) AND (RECORDS.IS_CONSTITUENT  =  -1))  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- VIEW 41.
CREATE VIEW dbo.HC_Prospect_Rating_v 
AS

SELECT 
		RECORDS_PROSPECT_RATINGS.IMPORT_ID "PRateImpID",
		 DBO.GetRatingCategory(RECORDS_PROSPECT_RATINGS.CATEGORY_ID,RECORDS_PROSPECT_RATINGS.CATEGORY_CODE) "PRateCat",
		 RECORDS.CONSTITUENT_ID "ConsID",
		 RECORDS.IMPORT_ID "ImportID", 
		 CONVERT(CHAR(10), RECORDS_PROSPECT_RATINGS.DTE, 101) AS  "PRateDate",
		 DBO.GetRatingDescription(RECORDS_PROSPECT_RATINGS.CATEGORY_ID,CASE RECORDS_PROSPECT_RATINGS.BOOLEAN WHEN 0 THEN 'No' WHEN -1 THEN 'Yes' ELSE '' END ,DBO.HC_FormatAmount_f(RECORDS_PROSPECT_RATINGS.CURR,0),RECORDS_PROSPECT_RATINGS.TEXT,CASE WHEN COALESCE(CAST(RECORDS_PROSPECT_RATINGS.DATE_TIME AS VARCHAR),'1/1/1000') = '1/1/1000' THEN '' ELSE (CAST(COALESCE((CAST(COALESCE({fn MONTH(COALESCE(CAST(RECORDS_PROSPECT_RATINGS.DATE_TIME AS VARCHAR),'1/1/1000'))},'') AS VARCHAR)+CAST(COALESCE('/','') AS VARCHAR)+CAST(COALESCE({fn DAYOFMONTH(COALESCE(CAST(RECORDS_PROSPECT_RATINGS.DATE_TIME AS VARCHAR),'1/1/1000'))},'') AS VARCHAR)+CAST(COALESCE('/','') AS VARCHAR)+CAST(COALESCE({fn YEAR(COALESCE(CAST(RECORDS_PROSPECT_RATINGS.DATE_TIME AS VARCHAR),'1/1/1000'))},'') AS VARCHAR)),'') AS VARCHAR)+CAST(COALESCE(' ','') AS VARCHAR)+CAST(COALESCE(CASE WHEN Convert(varchar(255), 
			Convert(datetime,COALESCE(RECORDS_PROSPECT_RATINGS.DATE_TIME,'00:00:00')), 
			108) = '00:00:00' THEN '' ELSE Convert(varchar(255), 
			Convert(datetime,COALESCE(RECORDS_PROSPECT_RATINGS.DATE_TIME,'00:00:00')), 
			101) END ,'') AS VARCHAR)) END ,RECORDS_PROSPECT_RATINGS.NUM,RECORDS_PROSPECT_RATINGS.STATICENTRY,RECORDS_PROSPECT_RATINGS.TABLEENTRY) "PRateDesc",
		 RECORDS_PROSPECT_RATINGS.COMMENTS "PRateNotes",
		 T_1.LONGDESCRIPTION "PRateSource",
		 dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort],  'PR' [RE_DB_Tbl],
		 row_number() over (order by RECORDS.IMPORT_ID) as ID

FROM 
		DBO.RECORDS AS RECORDS 
		LEFT OUTER JOIN DBO.PROSPECT AS RECORDS_PROSPECT ON RECORDS.ID = RECORDS_PROSPECT.CONSTIT_ID 
		LEFT OUTER JOIN DBO.RATINGS AS RECORDS_PROSPECT_RATINGS ON RECORDS_PROSPECT.ID = RECORDS_PROSPECT_RATINGS.PROSPECT_ID 
		LEFT OUTER JOIN DBO.TABLEENTRIES AS T_1 ON RECORDS_PROSPECT_RATINGS.SOURCE = T_1.TABLEENTRIESID

WHERE 
		((RECORDS.IS_CONSTITUENT  =  -1)) and (RECORDS_PROSPECT_RATINGS.IMPORT_ID) IS NOT NULL

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[HC_Prospect_WillNotGive_v]
AS 

	SELECT RECORDS.CONSTITUENT_ID as ConsID,
	RECORDS.IMPORT_ID as ImportID, 
	T_2.LONGDESCRIPTION as PWNGCode, 
	T_1.Comments as PWNGCom,
	dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'PW' [RE_DB_Tbl],
    row_number() over (order by T_1.INTEREST_CODE) as ID

	FROM 
	DBO.RECORDS AS RECORDS 
	INNER JOIN DBO.PROSPECT AS RECORDS_PROSPECT ON RECORDS.ID = RECORDS_PROSPECT.CONSTIT_ID 
	INNER JOIN DBO.QUERY_CON_PHILANTHROPY AS T_1 ON RECORDS_PROSPECT.ID = T_1.PROSPECT_ID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON T_1.INTEREST_CODE = T_2.TABLEENTRIESID

	WHERE 
	((T_1.INTEREST_CODE IS NOT NULL AND (RECORDS.IMPORT_ID IS NOT NULL)) AND (RECORDS.IS_CONSTITUENT  =  -1))  
GO
	
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO








-- VIEW 42.
CREATE VIEW dbo.HC_Cons_Alias_v
AS

SELECT 
		 T_2.KEY_NAME [AliasName],
		 T_3.LONGDESCRIPTION [AliasType],
		 RECORDS.CONSTITUENT_ID [ConsID],
		 RECORDS.IMPORT_ID [ImportID],
		 dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort],
		 'AI' [RE_DB_Tbl],
		row_number() over (order by T_2.KEY_NAME) as ID
FROM 
		DBO.RECORDS AS RECORDS INNER JOIN DBO.SEARCHNAME AS Records_SearchName ON RECORDS.ID = Records_SearchName.RECORDS_ID LEFT OUTER JOIN DBO.RECORDS AS T_1 ON Records_SearchName.RECORDS_ID = T_1.ID LEFT OUTER JOIN DBO.ALIASNAME AS T_2 ON T_1.ID = T_2.RECORDS_ID LEFT OUTER JOIN DBO.TABLEENTRIES AS T_3 ON T_2.ALIAS_TYPE = T_3.TABLEENTRIESID

WHERE 
		((RECORDS.IS_CONSTITUENT  =  -1)) and T_2.KEY_NAME is not null

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 

CREATE VIEW [dbo].[HC_Educ_Relat_v] 
AS 


		SELECT
			R.CONSTITUENT_ID AS [ConsID], 
			case when R.IS_CONSTITUENT = -1 then R.IMPORT_ID else '' end AS [ImportID], 
			T.IMPORT_ID [ESRImpID], 
			'' [ESRIRImpID],
			T_6.LONGDESCRIPTION AS [ESRSchoolName], CASE T.ALUMNI_INFO when -1 then 'TRUE' else 'FALSE' end AS [ESRPrimAlum],
			T_5.LONGDESCRIPTION AS [ESRType], T.CAMPUS AS [ESRCampus], 
			T.KNOWN_NAME [ESRKnownName], T.FRATERNITY [ESRFrat], 
			T_4.LONGDESCRIPTION AS [ESRDegree],
			T.GPA AS [ESRGPA],
			T.CLASS_OF AS [ESRClassOf],
			T.DATE_ENTERED [ESRDateEnt],
			T.DATE_GRADUATED [ESRDateGrad],
			T.DATE_LEFT [ESRDateLeft],
			T_7.LONGDESCRIPTION [ESRStatus],
			T.NOTES [ESRNotes],
			dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort],  'ED' [RE_DB_Tbl],
			'ER' + CAST(row_number() over (order by R.CONSTITUENT_ID) as char) ID
			
		FROM
			dbo.EDUCATION AS T
			LEFT OUTER JOIN dbo.RECORDS AS R ON T.RECORD_ID = R.ID
			--LEFT OUTER JOIN dbo.QUERY_IND_RELATIONSHIPS T_8 ON R.ID=T_8.RELATION_ID 
			LEFT OUTER JOIN dbo.TABLEENTRIES AS T_6 ON T.SCHOOL_ID = T_6.TABLEENTRIESID
			LEFT OUTER JOIN DBO.TABLEENTRIES AS T_1 ON T.SCHOOL_ID = T_1.TABLEENTRIESID
			LEFT OUTER JOIN DBO.TABLEENTRIES AS T_4 ON T.DEGREE = T_4.TABLEENTRIESID
			LEFT OUTER JOIN DBO.TABLEENTRIES AS T_5 ON T.SCHOOL_TYPE = T_5.TABLEENTRIESID
			LEFT OUTER JOIN dbo.TABLEENTRIES AS T_7 ON T.[STATUS] = T_7.TABLEENTRIESID

		WHERE (R.IS_CONSTITUENT = -1)

		UNION ALL 
		-- Education Relationships on non-cons Ind Relat. 
		SELECT
			'' [ConsID], 
 			'' [ImportID], 
 			T.IMPORT_ID [ESRImpID], 
			case when R.IS_CONSTITUENT = 0 then T_8.IMPORT_ID else '' end  AS [ESRIRImpID],
			T_6.LONGDESCRIPTION AS [ESRSchoolName], CASE T.ALUMNI_INFO when -1 then 'TRUE' else 'FALSE' end AS [ESRPrimAlum],
			T_5.LONGDESCRIPTION AS [ESRType], T.CAMPUS AS [ESRCampus], 
			T.KNOWN_NAME [ESRKnownName], T.FRATERNITY [ESRFrat], 
			T_4.LONGDESCRIPTION AS [ESRDegree],
			T.GPA AS [ESRGPA],
			T.CLASS_OF AS [ESRClassOf],
			T.DATE_ENTERED [ESRDateEnt],
			T.DATE_GRADUATED [ESRDateGrad],
			T.DATE_LEFT [ESRDateLeft],
			T_7.LONGDESCRIPTION [ESRStatus],
			T.NOTES [ESRNotes],
			dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort],  'ED' [RE_DB_Tbl],
			'IR' + CAST(row_number() over (order by R.CONSTITUENT_ID) as char) as ID
			
		FROM
			dbo.EDUCATION AS T
			LEFT OUTER JOIN dbo.RECORDS AS R ON T.RECORD_ID = R.ID
			INNER JOIN dbo.QUERY_IND_RELATIONSHIPS T_8 ON R.ID=T_8.RELATION_ID 
			LEFT OUTER JOIN dbo.TABLEENTRIES AS T_6 ON T.SCHOOL_ID = T_6.TABLEENTRIESID
			LEFT OUTER JOIN DBO.TABLEENTRIES AS T_1 ON T.SCHOOL_ID = T_1.TABLEENTRIESID
			LEFT OUTER JOIN DBO.TABLEENTRIES AS T_4 ON T.DEGREE = T_4.TABLEENTRIESID
			LEFT OUTER JOIN DBO.TABLEENTRIES AS T_5 ON T.SCHOOL_TYPE = T_5.TABLEENTRIESID
			LEFT OUTER JOIN dbo.TABLEENTRIES AS T_7 ON T.[STATUS] = T_7.TABLEENTRIESID

		WHERE (R.IS_CONSTITUENT = 0) 
		  
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO





SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.HC_Educ_Relat_Attribute_v AS
SELECT 
	case when R.IS_CONSTITUENT = -1 then R.IMPORT_ID else '' end AS [ImportID], 
	--case when R.IS_CONSTITUENT = 0 then T_8.IMPORT_ID else '' end  AS [IRImpID],
	EDUCATION.IMPORT_ID ESRAttrESRImpID, 
	EducationAttributes.IMPORTID AS ESRAttrImpID,
	dbo.AttributeTypes.DESCRIPTION ESRAttrCat,
	ESRAttrDesc = CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL 
				THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
				WHEN dbo.EducationAttributes.boolean IS NOT NULL THEN (CASE dbo.EducationAttributes.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END)
				WHEN dbo.EducationAttributes.text IS NOT NULL 
                THEN dbo.EducationAttributes.text WHEN dbo.EducationAttributes.num IS NOT NULL THEN CONVERT(varchar, dbo.EducationAttributes.num) 
                WHEN dbo.EducationAttributes.datetime IS NOT NULL THEN CONVERT(varchar, dbo.EducationAttributes.datetime) 
                WHEN dbo.EducationAttributes.currency IS NOT NULL THEN CONVERT(varchar, dbo.EducationAttributes.currency) 
                WHEN dbo.EducationAttributes.constitid IS NOT NULL THEN CONVERT(varchar, dbo.EducationAttributes.constitid) ELSE NULL END,
                case dbo.AttributeTypes.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
											   when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
											   when 8 then 'FuzzyDate' end as [DataType], 	

	dbo.dateformat(EducationAttributes.ATTRIBUTEDATE, 'MM/DD/YYYY') ESRAttrDate,
	EducationAttributes.COMMENTS ESRAttrCom,
	dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort],  'ET' [RE_DB_Tbl],
	row_number() over (order by EDUCATION.IMPORT_ID) as ID

 
FROM ((DBO.EDUCATION 
INNER JOIN DBO.EducationAttributes ON EDUCATION.ID = EducationAttributes.PARENTID)
LEFT JOIN  (dbo.AttributeTypes LEFT JOIN
                      dbo.CODETABLES ON dbo.AttributeTypes.CODETABLESID = dbo.CODETABLES.CODETABLESID) ON 
                      dbo.EducationAttributes.ATTRIBUTETYPESID = dbo.AttributeTypes.ATTRIBUTETYPESID) LEFT JOIN
                      dbo.TABLEENTRIES ON dbo.EducationAttributes.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID 
                      LEFT OUTER JOIN
                      dbo.AttributeTypes AS T_1 ON EducationAttributes.ATTRIBUTETYPESID = T_1.ATTRIBUTETYPESID
	LEFT JOIN dbo.RECORDS AS R ON EDUCATION.RECORD_ID = R.ID
--	LEFT JOIN dbo.QUERY_IND_RELATIONSHIPS T_8 ON R.ID=T_8.RELATION_ID 

 WHERE     dbo.EducationAttributes.IMPORTID IS NOT NULL and  R.IS_CONSTITUENT = -1

 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO





-- VIEW 43.
CREATE VIEW dbo.HC_Educ_Relat_Minor_v

AS
SELECT 
		 T_4.IMPORT_ID [ESRMinESRImpID],
		 T_11.IMPORTID [ESRMinImpID],
		 T_22.LONGDESCRIPTION [ESRMinMinor],
		 T_11.NOTES [ESRMinCom],
	     dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort],  'EO' [RE_DB_Tbl],
		 row_number() over (order by T_4.IMPORT_ID) as ID
 FROM 
		DBO.EDUCATION AS T_4 
		LEFT OUTER JOIN DBO.EDUCATIONMINOR AS T_11 ON T_4.ID = T_11.PARENTID
		LEFT OUTER JOIN DBO.TABLEENTRIES AS T_22 ON T_22.TABLEENTRIESID = T_11.TABLEENTRIESID 
where T_11.IMPORTID IS NOT NULL 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 44.
CREATE VIEW dbo.HC_Proposal_Attr_v

AS

SELECT 
RECORDS_PROSPECT_PROPOSAL.IMPORT_ID AS [PRAttrPRImpID],
DBO.PROPOSALATTRIBUTES.IMPORTID AS [PRAttrImpID],
T_2.DESCRIPTION AS [PRAttrCat],
DBO.PROPOSALATTRIBUTES.COMMENTS AS [PRAttrCom],
DBO.PROPOSALATTRIBUTES.ATTRIBUTEDATE AS [PRAttrDate],
PRAttrDesc = CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL 
                      THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
					  WHEN dbo.proposalattributes.boolean IS NOT NULL THEN (CASE dbo.proposalattributes.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END)
					  WHEN dbo.proposalattributes.text IS NOT NULL 
                      THEN dbo.proposalattributes.text WHEN dbo.proposalattributes.num IS NOT NULL THEN CONVERT(varchar, dbo.proposalattributes.num) 
                      WHEN dbo.proposalattributes.datetime IS NOT NULL THEN CONVERT(varchar, dbo.proposalattributes.datetime) 
                      WHEN dbo.proposalattributes.currency IS NOT NULL THEN CONVERT(varchar, dbo.proposalattributes.currency) 
                      WHEN dbo.proposalattributes.constitid IS NOT NULL THEN CONVERT(varchar, dbo.proposalattributes.constitid) ELSE NULL END,
                      case T_2.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
											   when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'ConstituentName' 
											   when 8 then 'FuzzyDate' end as [DataType], 	
					  dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'PT' [RE_DB_Tbl], 
					  row_number() over (order by RECORDS_PROSPECT_PROPOSAL.IMPORT_ID) as ID

FROM 
		DBO.RECORDS AS RECORDS INNER JOIN DBO.PROSPECT AS RECORDS_PROSPECT ON RECORDS.ID = RECORDS_PROSPECT.CONSTIT_ID 
							   INNER JOIN DBO.PROPOSAL AS RECORDS_PROSPECT_PROPOSAL ON RECORDS_PROSPECT.ID = RECORDS_PROSPECT_PROPOSAL.PROSPECT_ID 
							   INNER JOIN DBO.PROPOSALATTRIBUTES ON RECORDS_PROSPECT_PROPOSAL.ID = DBO.PROPOSALATTRIBUTES.PARENTID 
							   LEFT JOIN DBO.ATTRIBUTETYPES AS T_2 ON DBO.PROPOSALATTRIBUTES.ATTRIBUTETYPESID = T_2.ATTRIBUTETYPESID 
							   LEFT JOIN dbo.TABLEENTRIES ON dbo.ProposalAttributes.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID  
							    
WHERE 
		(((DBO.PROPOSALATTRIBUTES.IMPORTID IS NOT NULL)) AND (RECORDS.IS_CONSTITUENT  =  -1))   
		
 
 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 45.  -- select * from dbo.proposal_solicitor
CREATE VIEW dbo.HC_Proposal_Solicitor_v

AS

SELECT 
		records_prospect_proposal.IMPORT_ID "PRSolPRImpID",
		T_2.IMPORT_ID "PRSolImpID",
		T_1.Amount as PRSolAmount,
		dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'PS' [RE_DB_Tbl],
		row_number() over (order by T_2.IMPORT_ID) as ID
FROM 
		DBO.RECORDS AS records 
		INNER JOIN DBO.PROSPECT AS records_prospect ON records.ID = records_prospect.CONSTIT_ID 
		INNER JOIN DBO.PROPOSAL AS records_prospect_proposal ON records_prospect.ID = records_prospect_proposal.PROSPECT_ID 
		INNER JOIN DBO.PROPOSAL_SOLICITOR AS T_1 ON records_prospect_proposal.ID = T_1.PARENTID 
		INNER JOIN DBO.RECORDS AS T_2 ON T_1.SOLICITORID = T_2.ID
WHERE 
		(T_1.IMPORT_ID) is not null 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW 46.
CREATE VIEW dbo.HC_Prospect_Philanthropic_v

AS

SELECT 
	 T_2.LONGDESCRIPTION "PPICode",
	 T_1.COMMENTS "PPICom",
	 RECORDS.CONSTITUENT_ID "ConsID",
	 RECORDS.IMPORT_ID "ImportID",
	 RECORDS.SOCIAL_SECURITY_NO "SSNum",
	 dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'PC' [RE_DB_Tbl],
	 row_number() over (order by RECORDS.IMPORT_ID) as ID
FROM 
	DBO.RECORDS AS RECORDS LEFT OUTER JOIN DBO.PROSPECT AS RECORDS_PROSPECT ON RECORDS.ID = RECORDS_PROSPECT.CONSTIT_ID LEFT OUTER JOIN DBO.QUERY_PRO_PHILANTHROPY AS T_1 ON RECORDS_PROSPECT.ID = T_1.PROSPECT_ID LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON T_1.INTEREST_CODE = T_2.TABLEENTRIESID

WHERE 
	((RECORDS.IS_CONSTITUENT  =  -1)) and T_2.LONGDESCRIPTION is not null
 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- VIEW 47.
CREATE VIEW dbo.HC_Prospect_Other_Gifts_v

AS

SELECT 
		T_1.IMPORT_ID "POGImpID",
		T_1.Amount "POGAmt",
		T_1.Comments "POGCom",
		RECORDS.CONSTITUENT_ID "ConsID",
		RECORDS.IMPORT_ID "ImportID",
		RECORDS.SOCIAL_SECURITY_NO "SSNum",
		CASE WHEN  ISDATE(T_1.DATE_GIVEN)=1 AND LEN(T_1.DATE_GIVEN)=8 THEN CAST(DBO.DATEFORMAT(T_1.DATE_GIVEN, 'MM/DD/YYYY') as varchar(10))
		WHEN ISDATE(T_1.DATE_GIVEN)=1 AND LEN(T_1.DATE_GIVEN)=6 THEN CAST(RIGHT(T_1.DATE_GIVEN,2) +'/'+ LEFT(T_1.DATE_GIVEN,4) AS varchar(10))
		WHEN ISDATE(T_1.DATE_GIVEN)=1 AND LEN(T_1.DATE_GIVEN)=4 THEN cast(T_1.DATE_GIVEN AS varchar(10))
	    ELSE NULL END AS [POGDateGiven],
	
		Org_Records.Import_ID "POGOrgImpID",
		DBO.Query_OtherGift_OrgName(T_1.Organization_ID,T_1.Organization_Name) "POGOrgName",
		T_2.LONGDESCRIPTION "POGOrgType",
		T_1.Reason "POGReason",
		dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'PO' [RE_DB_Tbl],
		row_number() over (order by T_1.IMPORT_ID) as ID

FROM 
		DBO.RECORDS AS RECORDS LEFT OUTER JOIN DBO.PROSPECT AS RECORDS_PROSPECT ON RECORDS.ID = RECORDS_PROSPECT.CONSTIT_ID LEFT OUTER JOIN DBO.OTHER_GIFTS AS T_1 ON RECORDS_PROSPECT.ID = T_1.PROSPECT_ID LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON T_1.ORGANIZATION_TYPE = T_2.TABLEENTRIESID 
		left join dbo.records as Org_Records ON T_1.ORGANIZATION_ID = Org_Records.ID

WHERE 
		((RECORDS.IS_CONSTITUENT  =  -1)) and (T_1.IMPORT_ID) IS NOT NULL 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO




--. CONS ADDRESS ALL
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

		 
		CREATE VIEW dbo.HC_Cons_Address_v
		AS
		SELECT		DISTINCT		  RECORDS_CONSTIT_ADDRESS.ADDRESS_ID as REFERENCE_ADDRESS_ID, RECORDS_CONSTIT_ADDRESS.ID AS REFERENCE_ID,
							  RECORDS.import_id [ImportID], RECORDS.Constituent_id [ConsID],
							  RECORDS_CONSTIT_ADDRESS.IMPORT_ID [AddrImpID], RECORDS_CONSTIT_ADDRESS.SEQUENCE [AddrSequence],
							  T_9.LONGDESCRIPTION [AddrType], 
							  DBO.ADDRESSLINE(1, T_1.ADDRESS_BLOCK) [AddrLine1], 
							  DBO.ADDRESSLINE(2, T_1.ADDRESS_BLOCK) [AddrLine2], DBO.ADDRESSLINE(3, T_1.ADDRESS_BLOCK) [AddrLine3], 
							  DBO.ADDRESSLINE(4, T_1.ADDRESS_BLOCK) [AddrLine4], DBO.ADDRESSLINE(5, T_1.ADDRESS_BLOCK) [AddrLine5], 
							  T_1.CITY [AddrCity], T_8.SHORTDESCRIPTION [AddrState], T_1.POST_CODE [AddrZIP], T_3.LONGDESCRIPTION [AddrCounty], 
							   T_2.LONGDESCRIPTION [AddrCountry], 
							  RECORDS_CONSTIT_ADDRESS.DATE_FROM [AddrValidFrom], RECORDS_CONSTIT_ADDRESS.DATE_TO [AddrValidTo], T_1.DPC [AddrDPC], T_1.CARRIER_ROUTE [AddrCART], 
							  T_4.LONGDESCRIPTION [AddrInfoSrc], T_1.LOT [AddrLOT], T_5.LONGDESCRIPTION [AddrNZCity], T_6.LONGDESCRIPTION [AddrNZSuburb], 
							  CASE RECORDS_CONSTIT_ADDRESS.PREFERRED WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [AddrPref], 
							  T_7.LONGDESCRIPTION [AddrRegion], 
							  CASE RECORDS_CONSTIT_ADDRESS.SEASONAL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [AddrSeasonal], 
							  RECORDS_CONSTIT_ADDRESS.SEASONAL_FROM [AddrSeasFrom], 
							  RECORDS_CONSTIT_ADDRESS.SEASONAL_TO [AddrSeasTo], 
							  CASE RECORDS_CONSTIT_ADDRESS.SENDMAIL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END [AddrSendMail],
		                       
							  --properties
							  CONVERT(CHAR(10),RECORDS_CONSTIT_ADDRESS.DATE_ADDED,101) [DateAdded], 
							  CONVERT(CHAR(10),RECORDS_CONSTIT_ADDRESS.DATE_LAST_CHANGED,101) [DateLastChanged], 
 							  RECORDS_CONSTIT_ADDRESS.ID [SystemRecordID],

							  dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'AD' [RE_DB_Tbl] 

		FROM         DBO.RECORDS AS RECORDS INNER JOIN
							  DBO.QCONSTIT_ADDRESS AS RECORDS_CONSTIT_ADDRESS ON RECORDS.ID = RECORDS_CONSTIT_ADDRESS.CONSTIT_ID INNER JOIN
							  DBO.ADDRESS AS T_1 ON RECORDS_CONSTIT_ADDRESS.ADDRESS_ID = T_1.ID LEFT OUTER JOIN
							  DBO.TABLEENTRIES AS T_2 ON T_1.COUNTRY = T_2.TABLEENTRIESID LEFT OUTER JOIN
							  DBO.TABLEENTRIES AS T_3 ON T_1.COUNTY = T_3.TABLEENTRIESID LEFT OUTER JOIN
							  DBO.TABLEENTRIES AS T_4 ON RECORDS_CONSTIT_ADDRESS.INFO_SOURCE = T_4.TABLEENTRIESID LEFT OUTER JOIN
							  DBO.TABLEENTRIES AS T_5 ON T_1.NZCITY = T_5.TABLEENTRIESID LEFT OUTER JOIN
							  DBO.TABLEENTRIES AS T_6 ON T_1.SUBURB = T_6.TABLEENTRIESID LEFT OUTER JOIN
							  DBO.TABLEENTRIES AS T_7 ON T_1.REGION = T_7.TABLEENTRIESID LEFT OUTER JOIN
							  DBO.STATES AS T_8 ON T_1.STATE = T_8.SHORTDESCRIPTION LEFT OUTER JOIN
							  DBO.TABLEENTRIES AS T_9 ON RECORDS_CONSTIT_ADDRESS.TYPE = T_9.TABLEENTRIESID
		WHERE     ((RECORDS.IS_CONSTITUENT = - 1))

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
 



--VIEW 49:
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.HC_Ind_Relat_v
AS
               

SELECT 	T_1.[ConsID], T_1.[ImportID], T_1.[IRImpID], T_1.[IRLink], T_1.[RRImpID], T_1.[REFERENCE_ADDRESS_ID], T_1.[REFERENCE_ID], T_1.[REFERENCE_IMPORT_ID]
		,T_1.[IRTitl1],T_1.[IRTitl2],T_1.[IRFirstName],T_1.[IRMidName],T_1.[IRLastName],T_1.[IRSuff1],T_1.[IRSuff2],T_1.[IRMaidName],T_1.[IRNickname],T_1.[IRGender]
		,T_1.[IRPrimAddEdit],T_1.[IRPrimAddID],T_1.[IRPrimAddText],T_1.[IRPrimSalEdit],T_1.[IRPrimSalID],T_1.[IRPrimSalText]
		,T_1.[IRBDay],T_1.[IRDeceased],T_1.[IRDecDate]
    
		,T_1.[IRAddrType],T_1.[IRAddrLine1],T_1.[IRAddrLine2],T_1.[IRAddrLine3],T_1.[IRAddrLine4],T_1.[IRAddrLine5],T_1.[IRAddrCity],T_1.[IRAddrState],T_1.[IRAddrZip],T_1.[IRAddrCountry],T_1.[IRAddrCounty]
		,T_1.[IRAddrValidFrom],T_1.[IRAddrValidTo],T_1.[IRAddrDPC],T_1.[IRAddrCART],T_1.[IRAddrInfoSrc],T_1.[IRAddrLOT],T_1.[IRAddrNZSuburb],T_1.[IRAddrNZCity],T_1.[IRAddrRegion]
		,T_1.[IRAddrSeasonal],T_1.[IRAddrSeaFrom],T_1.[IRAddrSeaTo],T_1.[IRSendMail]
		
		,T_1.[IRRelat],T_1.[IRRecip] ,T_1.[IRFromDate],T_1.[IRToDate],T_1.[IRIsContact],T_1.[IRContactType],T_1.[IRIsEmp],T_1.[IRIsHH],T_1.[IRIsPrimary],T_1.[IRIsSpouse]
		,T_1.[IRAck],T_1.[IRAutoSoftCredit],T_1.[IRNoMail],T_1.[IRProf],T_1.[IRPos],T_1.[IRPrintOrg],T_1.[IRPrintPos],T_1.[IRNotes]
		,T_1.[RE_DB_Owner],T_1.[RE_DB_OwnerShort], T_1.[RE_DB_Tbl],T_1.ID
		
		--, T_10.*   --PHONES (T_10)      
FROM  HC_Ind_Relat T_1
LEFT JOIN HC_Ind_Relat_Phone_t T_10 ON T_1.IRImpID = T_10.IRPhoneIRImpID

 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



--VIEW 50:
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.HC_Org_Relat_v
AS
SELECT T_1.[ConsID],T_1.[ImportID], T_1.[ORImpID],T_1.[ORLink],T_1.[RRImpID],T_1.[REFERENCE_ADDRESS_ID],T_1.[REFERENCE_ID],T_1.[REFERENCE_IMPORT_ID]
      ,T_1.[ORFullName],T_1.[ORFiscalYear],T_1.[ORIncome],T_1.[ORIndustry],T_1.[ORAddrType],T_1.[ORAddrLine1],T_1.[ORAddrLine2],T_1.[ORAddrLine3],T_1.[ORAddrLine4],T_1.[ORAddrLine5]
      ,T_1.[ORAddrCity],T_1.[ORAddrState],T_1.[ORAddrZIP],T_1.[ORAddrCountry],T_1.[ORAddrCounty],T_1.[ORAddrValidFrom],T_1.[ORAddrValidTo]
      ,T_1.[ORAddrDPC],T_1.[ORAddrCART],T_1.[ORAddrInfoSrc],T_1.[ORAddrLot],T_1.[ORAddrNZCity],T_1.[ORAddrNZSuburb],T_1.[ORAddrRegion]
      ,T_1.[ORAddrSeasonal],T_1.[ORAddrSeaFrom],T_1.[ORAddrSeaTo],T_1.[ORSendMail]
      ,T_1.[ORRecip],T_1.[ORRelat],T_1.[ORFromDate],T_1.[ORToDate],T_1.[ORIsContact],T_1.[ORContactType],T_1.[ORIsEmp],T_1.[ORIsPrimary]
	  ,T_1.[OROrgMatchesGift],T_1.[ORMtcNotes],T_1.[ORMtcFactor],T_1.[ORMaxMtcAnn],T_1.[ORMaxMtcGift],T_1.[ORMaxMtcTotal],T_1.[ORMinMtcAnn],T_1.[ORMinMtcGift],T_1.[ORMinMtcTot]
	  ,T_1.[ORHMAck],T_1.[ORAutoSoftCredit],T_1.[ORNoMail],T_1.[ORProf],T_1.[ORPos],T_1.[ORPrintOrg],T_1.[ORPrintPos],T_1.[ORNotes]      
      ,T_1.[RE_DB_Owner],T_1.[RE_DB_OwnerShort],T_1.[RE_DB_Tbl],[ID]
--,T_10.*   --PHONES (T_10)                   

FROM  HC_Org_Relat T_1
LEFT JOIN HC_Org_Relat_Phone_t T_10 ON T_1.ORImpID = T_10.ORPhoneORImpID   

 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


--

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
CREATE VIEW dbo.HC_Ind_Relat_Phone_v

AS
SELECT     T_5.IMPORT_ID AS [IRPhoneImpID], T_7.LONGDESCRIPTION AS IRPhoneType, 
                      T_1.IMPORT_ID AS [IRPhoneIRImpID], convert(varchar(255), T_6.NUM) AS IRPhoneNum, 
			case T_6.DO_NOT_CALL when -1 then 'TRUE' else 'FALSE' end as IRPhoneDoNotCall,
                      dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'RP' [RE_DB_Tbl],
                      row_number() over (order by T_5.IMPORT_ID) as ID 

FROM         dbo.CONSTIT_RELATIONSHIPS CONSTIT_RELATIONSHIPS INNER JOIN
                      dbo.QUERY_IND_RELATIONSHIPS T_1 ON CONSTIT_RELATIONSHIPS.ID = T_1.ID INNER JOIN
                      dbo.RECORDS T_2 ON T_1.RELATION_ID = T_2.ID INNER JOIN
                      dbo.RECORDS T_3 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_3.ID LEFT OUTER JOIN
                      dbo.CONSTIT_ADDRESS T_4 ON T_1.CONSTIT_ADDRESS_ID = T_4.ID LEFT OUTER JOIN
                      dbo.CONSTIT_ADDRESS_PHONES T_5 ON T_4.ID = T_5.CONSTITADDRESSID LEFT OUTER JOIN
                      dbo.PHONES T_6 ON T_5.PHONESID = T_6.PHONESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES T_7 ON T_6.PHONETYPEID = T_7.TABLEENTRIESID
WHERE     (T_2.IMPORT_ID IS NOT NULL) AND (T_5.IMPORT_ID IS NOT NULL)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.HC_Org_Relat_Phone_v
AS
SELECT     T_4.IMPORT_ID AS [ORPhoneImpID], 
T_6.LONGDESCRIPTION AS [ORPhoneType], 
                      T_1.IMPORT_ID AS [ORPhoneORImpID], 
                      convert(varchar(255), T_5.NUM) AS [ORPhoneNum], 
                      dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'OP' [RE_DB_Tbl],
                      row_number() over (order by T_4.IMPORT_ID) as ID
FROM         dbo.CONSTIT_RELATIONSHIPS CONSTIT_RELATIONSHIPS INNER JOIN
                      dbo.QUERY_ORG_RELATIONSHIPS T_1 ON CONSTIT_RELATIONSHIPS.ID = T_1.ID INNER JOIN
                      dbo.RECORDS T_2 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_2.ID LEFT OUTER JOIN
                      dbo.CONSTIT_ADDRESS T_3 ON T_1.CONSTIT_ADDRESS_ID = T_3.ID LEFT OUTER JOIN
                      dbo.CONSTIT_ADDRESS_PHONES T_4 ON T_3.ID = T_4.CONSTITADDRESSID LEFT OUTER JOIN
                      dbo.PHONES T_5 ON T_4.PHONESID = T_5.PHONESID LEFT OUTER JOIN
                      dbo.TABLEENTRIES T_6 ON T_5.PHONETYPEID = T_6.TABLEENTRIESID
WHERE     (T_4.IMPORT_ID IS NOT NULL) AND (T_1.IMPORT_ID IS NOT NULL)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO




SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

		CREATE VIEW [HC_Bank_Financial_Institution_v] 
		AS 
		SELECT		T_1.IMPORT_ID BAImpID, 	T_2.LONGDESCRIPTION AS BAFinInstit,  T_1.BRANCH_NAME AS BABranch, 
		
		
			CASE T_1.I_ORIGIN_TYPE WHEN 0 THEN 'Transit/Routing no.' WHEN 1 THEN 'Other' ELSE NULL END AS BAOrigType, 
			CASE T_1.I_ORIGIN_TYPE WHEN 0 THEN T_1.SORT_CODE END AS BATransRoutNum,
			CASE T_1.I_ORIGIN_TYPE WHEN 1 THEN T_1.I_ORIGIN_USERDEFINED ELSE NULL END AS BAOther, 
			
			T_1.NOTES AS BANotes, 
			CASE T_1.SPONSORING_BANK WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE NULL END AS BASponBank,
			T_1.ACCOUNT_NAME AS BAAcctName, T_1.ACCOUNT_NO AS BAAcctNum, T_1.USER_NUMBER AS BAUserNum,  
			
			NULL AS [BAAbbr_AUS], T_1.ADDRESS_BLOCK AS BAAddrlines, NULL AS [BANumber_NZ],T_1.CITY AS BACity, T_1.STATE AS BAState, 
			T_3.LONGDESCRIPTION AS BACountry, T_1.COUNTY AS BACounty, NULL AS [BADestID_CAN], NULL AS BANZCity,  NULL AS BASuburb,
			T_4.LONGDESCRIPTION AS BARegion,
			T_1.POST_CODE BAZip,
			dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'BA' [RE_DB_Tbl],
			row_number() over (order by T_1.IMPORT_ID) as ID

		FROM 
		DBO.BANK AS T_1   
		LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON T_1.BANK = T_2.TABLEENTRIESID 
		LEFT OUTER JOIN DBO.TABLEENTRIES AS T_3 ON T_1.COUNTRY = T_3.TABLEENTRIESID
		LEFT OUTER JOIN dbo.TABLEENTRIES AS T_4 ON T_1.REGION = T_4.TABLEENTRIESID	
		
	WHERE T_1.IMPORT_ID  IS NOT NULL  
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO






SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

	CREATE VIEW [HC_Cons_Bank_v] 
	AS 
	SELECT RECORDS.CONSTITUENT_ID AS ConsID, 	RECORDS.IMPORT_ID as ImportID, T_1.IMPORT_ID as BAImpID, RECORDS_CONSTITUENT_BANK.IMPORT_ID as FIRRelImpID, 
	 	 
	 CASE RECORDS_CONSTITUENT_BANK.INACTIVE WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  FIRAcctInactive,
	 RECORDS_CONSTITUENT_BANK.NOTES FIRAcctNotes, RECORDS_CONSTITUENT_BANK.ACCOUNT_NO FIRAcctNum,
	 CASE RECORDS_CONSTITUENT_BANK.ACCOUNTTYPE WHEN 0 THEN 'Checking' WHEN 1 THEN 'Savings' WHEN 2 THEN 'Other' ELSE '' END  FIRAcctType ,
	 T_2.LONGDESCRIPTION FIRFinInstit,  T_3.BRANCH_NAME FIRBranch , 
	 CASE RECORDS_CONSTITUENT_BANK.IS_PRIMARY WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  FIRPrimBank ,
	 CASE (CASE WHEN T_1.I_ORIGIN_TYPE = 0 THEN -1 ELSE 0 END ) WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  FIRUseTransRoutNum,
     CASE (CASE WHEN T_1.I_ORIGIN_TYPE = 0 THEN -1 ELSE 0 END ) WHEN 0 THEN null WHEN -1 THEN  T_1.SORT_CODE ELSE '' END FIRTransRoutNum,
     CASE (CASE WHEN T_1.I_ORIGIN_TYPE = 0 THEN -1 ELSE 0 END ) WHEN 0 THEN T_1.I_ORIGIN_USERDEFINED WHEN -1 THEN  null ELSE '' END FIROther,
     dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort],  'CK' [RE_DB_Tbl],
	 row_number() over (order by RECORDS_CONSTITUENT_BANK.IMPORT_ID) as ID
     
	FROM 
	DBO.RECORDS AS RECORDS 
	INNER JOIN DBO.CONSTITUENT_BANK AS RECORDS_CONSTITUENT_BANK ON RECORDS.ID = RECORDS_CONSTITUENT_BANK.CONSTIT_ID 
	LEFT OUTER JOIN DBO.BANK AS T_1 ON RECORDS_CONSTITUENT_BANK.BRANCH_ID = T_1.ID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON T_1.BANK = T_2.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.BANK AS T_3 ON RECORDS_CONSTITUENT_BANK.BRANCH_ID = T_3.ID

	WHERE 
	(((RECORDS_CONSTITUENT_BANK.IMPORT_ID IS NOT NULL)) AND (RECORDS.IS_CONSTITUENT  =  -1)) 

	 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO






SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

		CREATE VIEW [HC_Fund_Relationship_v] 
		AS 


		SELECT 
		 records.CONSTITUENT_ID AS ConsID,
		 records.IMPORT_ID AS ImportID,
		 records_CONSTIT_FUNDS.IMPORT_ID AS FRImpID,
		 T_1.FUND_ID AS FRFundID,
		 T_1.DESCRIPTION AS FRFundDesc,
		 case	when LEN(records_CONSTIT_FUNDS.DATE_FROM)=8 then cast(dbo.dateformat(records_CONSTIT_FUNDS.DATE_FROM, 'MM/DD/YYYY') AS varchar(10))
									when LEN(records_CONSTIT_FUNDS.DATE_FROM)=6 then cast(RIGHT(records_CONSTIT_FUNDS.DATE_FROM,2) +'/'+ LEFT(records_CONSTIT_FUNDS.DATE_FROM,4) as varchar(10)) 
									when LEN(records_CONSTIT_FUNDS.DATE_FROM)=4 then cast(records_CONSTIT_FUNDS.DATE_FROM as varchar(10))
									else null end as FRDateFrom, 
		 case	when LEN(records_CONSTIT_FUNDS.DATE_TO)=8 then cast(dbo.dateformat(records_CONSTIT_FUNDS.DATE_TO, 'MM/DD/YYYY') AS varchar(10))
									when LEN(records_CONSTIT_FUNDS.DATE_TO)=6 then cast(RIGHT(records_CONSTIT_FUNDS.DATE_TO,2) +'/'+ LEFT(records_CONSTIT_FUNDS.DATE_TO,4) as varchar(10)) 
									when LEN(records_CONSTIT_FUNDS.DATE_TO)=4 then cast(records_CONSTIT_FUNDS.DATE_TO as varchar(10))
									else null end as FRDateTo, 
		 T_2.LONGDESCRIPTION AS FRRecip,
		 T_3.LONGDESCRIPTION AS FRRelat,
		 records_CONSTIT_FUNDS.NOTES AS FRNotes,
		 DBO.Query_FundRelationCount(records.ID) FRTotalFundRelat,
		 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'FR' [RE_DB_Tbl],
		row_number() over (order by records_CONSTIT_FUNDS.IMPORT_ID ) as ID

		FROM  
		DBO.RECORDS AS records 
		INNER JOIN DBO.CONSTIT_FUNDS AS records_CONSTIT_FUNDS 
			ON records.ID = records_CONSTIT_FUNDS.CONSTIT_ID 
		LEFT OUTER JOIN DBO.FUND AS T_1 
			ON records_CONSTIT_FUNDS.FUND_ID = T_1.ID 
		LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 
			ON records_CONSTIT_FUNDS.RECIP_RELATION_CODE = T_2.TABLEENTRIESID 
		LEFT OUTER JOIN DBO.TABLEENTRIES AS T_3 
			ON records_CONSTIT_FUNDS.RELATION_CODE = T_3.TABLEENTRIESID

		WHERE 
		(((records_CONSTIT_FUNDS.IMPORT_ID IS NOT NULL)) AND (records.IS_CONSTITUENT  =  -1))  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW [dbo].[HC_Ind_Relat_Attr_v]
AS 
SELECT 
 T_1.IMPORT_ID as IRAttrIRImpID, T_2.IMPORTID as IRAttrImpID, 

 AttributeTypes.DESCRIPTION as IRAttrCat,
 IRAttrDesc  =	CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
				WHEN T_2.boolean IS NOT NULL THEN (CASE T_2.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END)
				WHEN T_2.text IS NOT NULL THEN T_2.text 
				WHEN T_2.num IS NOT NULL THEN CONVERT(varchar, T_2.num) 
                WHEN T_2.datetime IS NOT NULL THEN CAST(DBO.DATEFORMAT(T_2.datetime, 'MM/DD/YYYY') as varchar(10))--CONVERT(varchar, T_2.datetime) 
                WHEN T_2.currency IS NOT NULL THEN CONVERT(varchar, T_2.currency) 
				WHEN T_2.constitid IS NOT NULL THEN CONVERT(varchar, T_0.IMPORT_ID) ELSE NULL END,
				case dbo.AttributeTypes.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
											   when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
											   when 8 then 'FuzzyDate' end as [DataType], 	
				CAST(DBO.DATEFORMAT(T_2.ATTRIBUTEDATE, 'MM/DD/YYYY') as varchar(10))  as IRAttrDate, T_2.COMMENTS as IRAttrCom,  
				dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'IA' [RE_DB_Tbl], 
				row_number() over (order by T_1.IMPORT_ID) as ID

FROM 
((DBO.CONSTIT_RELATIONSHIPS AS CONSTIT_RELATIONSHIPS 
LEFT JOIN DBO.QUERY_IND_RELATIONSHIPS AS T_1 ON CONSTIT_RELATIONSHIPS.ID = T_1.ID 
LEFT JOIN DBO.RECORDS AS T_3 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_3.ID 
LEFT JOIN DBO.RELATIONSHIPATTRIBUTES AS T_2 ON T_1.ID = T_2.PARENTID)
LEFT Join dbo.RECORDS AS T_0 ON T_2.ConstitID = T_0.ID
LEFT JOIN (dbo.AttributeTypes 
				LEFT JOIN dbo.CODETABLES ON dbo.AttributeTypes.CODETABLESID = dbo.CODETABLES.CODETABLESID) ON 
                      T_2.ATTRIBUTETYPESID = dbo.AttributeTypes.ATTRIBUTETYPESID) 
                LEFT JOIN dbo.TABLEENTRIES ON T_2.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID 
                LEFT JOIN dbo.AttributeTypes T_9 ON T_2.ATTRIBUTETYPESID = T_9 .ATTRIBUTETYPESID
 
WHERE 
T_2.IMPORTID IS NOT NULL and T_1.IMPORT_ID is not null 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



--HC_ORG_RELAT_ATTR_V

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW [dbo].[HC_Org_Relat_Attr_v]
AS 
SELECT 
 T_1.IMPORT_ID as ORAttrORImpID, T_2.IMPORTID as ORAttrImpID, 

 AttributeTypes.DESCRIPTION as ORAttrCat,
 ORAttrDesc  =	CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
				WHEN T_2.boolean IS NOT NULL THEN (CASE T_2.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END)
				WHEN T_2.text IS NOT NULL THEN T_2.text 
				WHEN T_2.num IS NOT NULL THEN CONVERT(varchar, T_2.num) 
                WHEN T_2.datetime IS NOT NULL THEN CAST(DBO.DATEFORMAT(T_2.datetime, 'MM/DD/YYYY') as varchar(10))--CONVERT(varchar, T_2.datetime) 
                WHEN T_2.currency IS NOT NULL THEN CONVERT(varchar, T_2.currency) 
				WHEN T_2.constitid IS NOT NULL THEN CONVERT(varchar, T_0.IMPORT_ID) ELSE NULL END,
				case dbo.AttributeTypes.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
											   when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
											   when 8 then 'FuzzyDate' end as [DataType], 	
				CAST(DBO.DATEFORMAT(T_2.ATTRIBUTEDATE, 'MM/DD/YYYY') as varchar(10))  as ORAttrDate, T_2.COMMENTS as ORAttrCom,  
				dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'OA' [RE_DB_Tbl], row_number() over (order by T_1.IMPORT_ID) as ID

FROM 
((DBO.CONSTIT_RELATIONSHIPS AS CONSTIT_RELATIONSHIPS 
LEFT JOIN DBO.QUERY_ORG_RELATIONSHIPS AS T_1 ON CONSTIT_RELATIONSHIPS.ID = T_1.ID 
LEFT JOIN DBO.RECORDS AS T_3 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_3.ID 
LEFT JOIN DBO.RELATIONSHIPATTRIBUTES AS T_2 ON T_1.ID = T_2.PARENTID)
LEFT Join dbo.RECORDS AS T_0 ON T_2.ConstitID = T_0.ID
LEFT JOIN (dbo.AttributeTypes 
				LEFT JOIN dbo.CODETABLES ON dbo.AttributeTypes.CODETABLESID = dbo.CODETABLES.CODETABLESID) ON 
                      T_2.ATTRIBUTETYPESID = dbo.AttributeTypes.ATTRIBUTETYPESID) 
                LEFT JOIN dbo.TABLEENTRIES ON T_2.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID 
                LEFT JOIN dbo.AttributeTypes T_9 ON T_2.ATTRIBUTETYPESID = T_9 .ATTRIBUTETYPESID
 
WHERE 
T_2.IMPORTID IS NOT NULL and T_1.IMPORT_ID is not null 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- VIEW: SECURITY USERS 
 
CREATE VIEW [dbo].HC_Users_v
AS

SELECT     dbo.USERS.NAME AS UserName, dbo.USERS.DESCRIPTION as UserDescription, 
		   CASE dbo.USERS.HASSUPERVISORRIGHTS WHEN -1 then 'Yes' else null end as UserHasSupervisorRights, 
			dbo.SECURITYGROUP.NAME AS UserGroupName, dbo.SECURITYGROUP.DESCRIPTION AS UserGroupDescription,
			dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() as [RE_DB_OwnerShort]

FROM        dbo.USERS  
			LEFT JOIN dbo.SECURITYUSER ON dbo.SECURITYUSER.USERS_ID = dbo.USERS.USER_ID
			LEFT JOIN dbo.SECURITYGROUP On dbo.SECURITYGROUP.SECURITYGROUP_ID = dbo.SECURITYUSER.SECURITYGROUP_ID
WHERE		(dbo.USERS.DELETED = 0)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[HC_Volunteer_v]
AS
	SELECT 
 		RECORDS.CONSTITUENT_ID as ConsID, RECORDS.IMPORT_ID as ImportID,
		RECORDS_VOLUNTEER.EMERGENCY_NAME as VEmerName, RECORDS_VOLUNTEER.EMERGENCY_PHONE as VEmerPhone, T_1.LONGDESCRIPTION as VEmerRel,
		T_2.LONGDESCRIPTION as VFutureAward, RECORDS_VOLUNTEER.FUTURE_AWARD_COMMENTS as VFutureAwardCom,
		T_3.AGENCY as VMandateName, T_3.ADDRESS as VMandateAddr, T_3.CONTACT as VMandateContact, T_3.PHONE as VMandatePhone,
		T_3.COMMENTS as VMandateCom,
		
		CASE WHEN  ISDATE(T_3.START_DATE)=1 AND LEN(T_3.START_DATE)=8 THEN CAST(DBO.DATEFORMAT(T_3.START_DATE, 'MM/DD/YYYY') as varchar(10))
		WHEN LEN(T_3.START_DATE)=6 THEN CAST(RIGHT(T_3.START_DATE,2) +'/'+ LEFT(T_3.START_DATE,4) AS varchar(10))
		WHEN LEN(T_3.START_DATE)=4 THEN cast(T_3.START_DATE as varchar(10))
		ELSE NULL END AS VMandateStart,
		
		CASE WHEN  ISDATE(T_3.COMPLETED_DATE)=1 AND LEN(T_3.COMPLETED_DATE)=8 THEN CAST(DBO.DATEFORMAT(T_3.COMPLETED_DATE, 'MM/DD/YYYY') as varchar(10))
		WHEN LEN(T_3.COMPLETED_DATE)=6 THEN CAST(RIGHT(T_3.COMPLETED_DATE,2) +'/'+ LEFT(T_3.COMPLETED_DATE,4) AS varchar(10))
		WHEN LEN(T_3.COMPLETED_DATE)=4 THEN cast(T_3.COMPLETED_DATE as varchar(10))
		ELSE NULL END AS VMandateCmplDate,
		
		T_3.HOURS_REQUIRED as VMandateHours,  
		T_4.LONGDESCRIPTION as VVehicleType, CASE RECORDS_VOLUNTEER.VEHICLE_AVAILABLE WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  as VVehicleAvail,
		RECORDS_VOLUNTEER.VEHICLE_COMMENTS as VVehicleCom,
		
		dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'VO' [RE_DB_Tbl],
		row_number() over (order by RECORDS.IMPORT_ID) as ID


	FROM 
		DBO.RECORDS AS RECORDS 
		LEFT OUTER JOIN DBO.VOLUNTEER AS RECORDS_VOLUNTEER ON RECORDS.ID = RECORDS_VOLUNTEER.RECORD_ID 
		LEFT OUTER JOIN DBO.TABLEENTRIES AS T_1 ON RECORDS_VOLUNTEER.EMERGENCY_RELATION = T_1.TABLEENTRIESID 
		LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON RECORDS_VOLUNTEER.FUTURE_AWARD = T_2.TABLEENTRIESID 
		LEFT OUTER JOIN DBO.VOL_MANDATE AS T_3 ON RECORDS_VOLUNTEER.ID = T_3.VOLUNTEER_ID 
		LEFT OUTER JOIN DBO.TABLEENTRIES AS T_4 ON RECORDS_VOLUNTEER.VEHICLE_TYPE = T_4.TABLEENTRIESID

	WHERE 
		(((RECORDS.IMPORT_ID IS NOT NULL)) AND (RECORDS.IS_CONSTITUENT  =  -1)) AND (RECORDS_VOLUNTEER.ID IS NOT NULL)
GO		


SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO




SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[HC_Volunteer_Type_v]
AS


	SELECT 
	 RECORDS.CONSTITUENT_ID as ConsID,
	 RECORDS.IMPORT_ID as ImportID,
	 T_2.LONGDESCRIPTION as VTType,
	 T_3.LONGDESCRIPTION as VTStatus,
	 
	 CASE WHEN  ISDATE(T_1.DATE_STARTED)=1 AND LEN(T_1.DATE_STARTED)=8 THEN CAST(DBO.DATEFORMAT(T_1.DATE_STARTED, 'MM/DD/YYYY') as varchar(10))
	 WHEN LEN(T_1.DATE_STARTED)=6 THEN CAST(RIGHT(T_1.DATE_STARTED,2) +'/'+ LEFT(T_1.DATE_STARTED,4) AS varchar(10))
	 WHEN LEN(T_1.DATE_STARTED)=4 THEN cast(T_1.DATE_STARTED as varchar(10))
	 ELSE NULL END AS VTStartDate,

	 CASE WHEN  ISDATE(T_1.DATE_FINISHED)=1 AND LEN(T_1.DATE_FINISHED)=8 THEN CAST(DBO.DATEFORMAT(T_1.DATE_FINISHED, 'MM/DD/YYYY') as varchar(10))
	 WHEN LEN(T_1.DATE_FINISHED)=6 THEN CAST(RIGHT(T_1.DATE_FINISHED,2) +'/'+ LEFT(T_1.DATE_FINISHED,4) AS varchar(10))
	 WHEN LEN(T_1.DATE_FINISHED)=4 THEN cast(T_1.DATE_FINISHED as varchar(10))
	 ELSE NULL END AS VTEndDate,
	 		
	 T_1.REASON_FINISHED VTReasonFin,
	
	dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'VT' [RE_DB_Tbl],
	row_number() over (order by T_1.VOLUNTEER_ID) as ID


	

	FROM 
	DBO.RECORDS AS RECORDS INNER JOIN DBO.VOLUNTEER AS RECORDS_VOLUNTEER ON RECORDS.ID = RECORDS_VOLUNTEER.RECORD_ID 
	INNER JOIN DBO.VOL_TYPE AS T_1 ON RECORDS_VOLUNTEER.ID = T_1.VOLUNTEER_ID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON T_1.VOLUNTEER_TYPE = T_2.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_3 ON T_1.STATUS = T_3.TABLEENTRIESID

	WHERE 
	(((RECORDS.IMPORT_ID IS NOT NULL) AND T_1.VOLUNTEER_TYPE IS NOT NULL) AND (RECORDS.IS_CONSTITUENT  =  -1))  

	GO
 
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO




--VOLUNTEER AVAILABILITY

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.HC_Volunteer_Availability_v
AS
	SELECT 
	 RECORDS.CONSTITUENT_ID as ConsID,
	 RECORDS.IMPORT_ID as ImportID,
	 T_1.IMPORT_ID as VAvailImpID,
	 CASE T_1.DAY WHEN 11 THEN '' WHEN 1 THEN 'Sunday' WHEN 2 THEN 'Monday' WHEN 3 THEN 'Tuesday' WHEN 4 THEN 'Wednesday' WHEN 5 THEN 'Thursday' WHEN 6 THEN 'Friday' WHEN 7 THEN 'Saturday' WHEN 8 THEN 'All Week' WHEN 9 THEN 'Weekdays' WHEN 10 THEN 'Weekends' ELSE '' END  as VAvailDay,
	 T_1.FROM_DATE as VAvailFromDate,
	 T_1.TO_DATE as VAvailToDate,
	 T_1.FROM_TIME as VAvailStartTime,
	 T_1.TO_TIME as VAvailEndTime,

	 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'VY' [RE_DB_Tbl],
	 row_number() over (order by T_1.VOLUNTEER_ID) as ID
	 
	FROM 
	DBO.RECORDS AS RECORDS 
	INNER JOIN DBO.VOLUNTEER AS RECORDS_VOLUNTEER ON RECORDS.ID = RECORDS_VOLUNTEER.RECORD_ID 
	INNER JOIN DBO.VOL_AVAILABILITY AS T_1 ON RECORDS_VOLUNTEER.ID = T_1.VOLUNTEER_ID

	WHERE 
	(((T_1.IMPORT_ID IS NOT NULL)) AND (RECORDS.IS_CONSTITUENT  =  -1))  

GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF
GO
	
-- VOLUNTEER ASSIGNMENT INTEREST

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

	CREATE VIEW dbo.HC_Volunteer_Assignment_Interest_v
	AS	
		

	SELECT 
	 RECORDS.CONSTITUENT_ID as ConsID,
	 RECORDS.IMPORT_ID as ImportID,
	 T_1.IMPORT_ID as VAIImpID,
	 T_2.LONGDESCRIPTION as VAIInterest,

	 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'VS' [RE_DB_Tbl],
	 row_number() over (order by T_1.VOLUNTEER_ID) as ID


	FROM 
	DBO.RECORDS AS RECORDS 
	INNER JOIN DBO.VOLUNTEER AS RECORDS_VOLUNTEER ON RECORDS.ID = RECORDS_VOLUNTEER.RECORD_ID 
	INNER JOIN DBO.VOL_INTEREST AS T_1 ON RECORDS_VOLUNTEER.ID = T_1.VOLUNTEER_ID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON T_1.INTEREST_CODE = T_2.TABLEENTRIESID

	WHERE 
	((T_1.INTEREST_CODE IS NOT NULL AND (RECORDS.IMPORT_ID IS NOT NULL)) AND (RECORDS.IS_CONSTITUENT  =  -1))  

GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF
GO

-- GIFT BENEFITS

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.HC_Gift_Benefits_v
AS	
	
	SELECT 
	 GIFT.IMPORT_ID as GFLink,
	 GIFT_GIFTBENEFIT.IMPORT_ID as GFBenImpID, 
	 T_1.LONGDESCRIPTION as GFBenBen,
	 GIFT_GIFTBENEFIT.Count as GFBenCount,
	 
	 CASE WHEN  ISDATE(GIFT_GIFTBENEFIT.Sent)=1 AND LEN(GIFT_GIFTBENEFIT.Sent)=8 THEN CAST(DBO.DATEFORMAT(GIFT_GIFTBENEFIT.Sent, 'MM/DD/YYYY') as varchar(10))
	 WHEN LEN(GIFT_GIFTBENEFIT.Sent)=6 THEN CAST(RIGHT(GIFT_GIFTBENEFIT.Sent,2) +'/'+ LEFT(GIFT_GIFTBENEFIT.Sent,4) AS varchar(10))
	 WHEN LEN(GIFT_GIFTBENEFIT.Sent)=4 THEN cast(GIFT_GIFTBENEFIT.Sent as varchar(10))
	 ELSE NULL END AS GFBenSent,

	 GIFT_GIFTBENEFIT.TotalBenefitValue as GFBenTotVal,
	 GIFT_GIFTBENEFIT.UnitCost as GFBenUnitCost,
	 GIFT_GIFTBENEFIT.Comments as GFBenCom,
	 GIFT.BenefitsNotes as GFBenNotes,
	 
	 GIFT_GIFTBENEFIT.Sequence as GFBenSequence,
	 
	 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'GB' [RE_DB_Tbl],
	 row_number() over (order by GIFT.IMPORT_ID) as ID


	FROM 
	DBO.GIFT AS GIFT 
	INNER JOIN DBO.GIFTBENEFIT AS GIFT_GIFTBENEFIT ON GIFT.ID = GIFT_GIFTBENEFIT.PARENTID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS T_1 ON GIFT_GIFTBENEFIT.BENEFITID = T_1.TABLEENTRIESID

	WHERE 
	((GIFT_GIFTBENEFIT.BenefitId IS NOT NULL) AND ((GIFT.TYPE NOT IN ('28','32','33') OR (GIFT.TYPE IS NULL))))  
	
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF
GO



--FUND GL DISTRIBUTIONS

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
 
CREATE  VIEW dbo.HC_Fund_GLDistribution_v
AS
		SELECT 
		 FUND.FUND_ID [FundID], FUND.DESCRIPTION [FundDesc],
		 T_1.LONGDESCRIPTION [FundDistrGiftType],
		 T_2.LONGDESCRIPTION [FundDistrGiftSubType],
		 fund_Qfunddistribution.GLCREDIT_NUMBER [FundDistrCreditAccountNumber],
		 fund_Qfunddistribution.GLDEBIT_NUMBER [FundDistrDebittAccountNumber],
		 fund_Qfunddistribution.PERCENTAGE [FundDistrPercentage],
		 fund_Qfunddistribution.NOTES [FundDistrComments],
		 T_3.PERCENTAGE [FundProjectPercentage],
		 T_3.GLPROJECT_NUMBER [FundProjectNumber],
		 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() as [RE_DB_OwnerShort],
		 row_number() over (order by FUND.FUND_ID) as ID


		FROM 
		DBO.FUND AS FUND 
		LEFT OUTER JOIN DBO.QFUNDDISTRIBUTION AS fund_Qfunddistribution ON FUND.ID = fund_Qfunddistribution.FUNDID 
		LEFT OUTER JOIN DBO.TABLEENTRIES AS T_1 ON fund_Qfunddistribution.GIFTTYPEID = T_1.TABLEENTRIESID 
		LEFT OUTER JOIN DBO.TABLEENTRIES AS T_2 ON fund_Qfunddistribution.GIFTSUBTYPEID = T_2.TABLEENTRIESID 
		LEFT OUTER JOIN DBO.GLDISTRIBUTIONDETAIL AS T_3 ON fund_Qfunddistribution.DISTRIBUTION_ID = T_3.DISTRIBUTION_ID  

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
 
  

  
  
-- GIFT ADJUSTMENTS

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
 
		CREATE VIEW dbo.HC_Gift_Adjustments_v
		AS
		SELECT 
		 OGIFT.UserGiftId as GiftID, 
		 OGIFT.IMPORT_ID AS GFImpID, 
		 OT_1.Amount AS ADJAmount, 
		 dbo.dateformat(OT_1.DTE, 'MM/DD/YYYY') AS ADJDate,
		 OT_1.AdjustmentNotes AS ADJNotes, 
		 OT_1.REF ADJReason,
		 OT_4.FUND_ID AS ADJPreviousFundID,
		 OT_4.DESCRIPTION AS ADJPreviousFundDesc, 
		 OT_3.Amount AS ADJPreviousFundSplitAmount, 
		 dbo.dateformat(OT_1.[POST_DATE], 'MM/DD/YYYY') AS ADJGLPostDate,
		 CASE OT_1.[POST_STATUS] WHEN 1 THEN 'Posted' WHEN 2 THEN 'Not Posted' WHEN 3 THEN 'Do Not Post' ELSE '' END ADJGLPostStatus
 
		FROM 
		DBO.GIFT AS OGIFT 
		INNER JOIN DBO.GIFTADJUSTMENT AS OGIFT_GIFTADJUSTMENT ON OGIFT.ID = OGIFT_GIFTADJUSTMENT.GIFTID 
		INNER JOIN DBO.GIFT AS OT_1 ON OGIFT_GIFTADJUSTMENT.ADJUSTMENTID = OT_1.ID 
		LEFT OUTER JOIN DBO.GIFT OT_2 ON OGIFT_GIFTADJUSTMENT.ADJUSTMENTID = OT_2.ID 
		LEFT OUTER JOIN DBO.GIFTPREVIOUSSPLIT AS OT_3 ON OT_2.ID = OT_3.GIFTID 
		LEFT OUTER JOIN DBO.FUND AS OT_4 ON OT_3.FUNDID = OT_4.ID

		WHERE 
		((OT_1.DTE IS NOT NULL) AND ((OGIFT.TYPE NOT IN ('28','32','33') OR (OGIFT.TYPE IS NULL))))    AND EXISTS (SELECT 
		 GIFT.ID FROM DBO.GIFT GIFT   WHERE OGIFT.ID  =  GIFT.ID)
 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
 


 

--GIFT AMENDMENTS (Recurring Gift Amendment)

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
 
		CREATE  VIEW dbo.HC_Gift_Amendments_v
		AS


		SELECT 
		 GIFT.IMPORT_ID as RGALink, GIFT.UserGiftId as RGAGiftID, T_1.IMPORT_ID as RGAImpID ,  dbo.dateformat(T_1.DTE, 'MM/DD/YYYY')  as RGADate,
		 DBO.GetPreviousGiftAmount(GIFT_giftamendment.AmendmentId) as AmendmentPreviousAmount,
		 CASE T_2.Installment_Frequency WHEN 1 THEN 'Annually' WHEN 2 THEN 'Semi-Annually' WHEN 3 THEN 'Quarterly' 
		 WHEN 4 THEN 'Bimonthly' WHEN 5 THEN 'Monthly' WHEN 6 THEN 'Semi-Monthly' WHEN 7 THEN 'Biweekly' WHEN 8 THEN 'Weekly' 
		 WHEN 9 THEN 'Irregular' WHEN 10 THEN 'Single Installment' ELSE '' END  as RGAPrevFreq,
		 T_4.FUND_ID RGAPrevFundID, T_5.CAMPAIGN_ID RGAPrevCampID, T_6.APPEAL_ID RGAPrevAppID, T_7.Package_ID RGAPrevPackID,
		 T_1.AdjustmentNotes RGANotes,
		 
		 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'GM' [RE_DB_Tbl],
		 row_number() over (order by GIFT.IMPORT_ID, T_1.DTE DESC) as ID

		FROM 
		DBO.GIFT AS GIFT 
		INNER JOIN DBO.GIFTAMENDMENT AS GIFT_giftamendment ON GIFT.ID = GIFT_giftamendment.GIFTID 
		INNER JOIN DBO.GIFT T_1 ON GIFT_giftamendment.AmendmentID = T_1.ID 
		LEFT OUTER JOIN DBO.GIFTHISTORYFIELDS AS T_2 ON GIFT_giftamendment.AMENDMENTID = T_2.ID 
		LEFT OUTER JOIN DBO.GIFTPREVIOUSSPLIT AS T_3 ON GIFT_giftamendment.AmendmentID = T_3.GIFTID 
		LEFT OUTER JOIN DBO.FUND AS T_4 ON T_3.FUNDID = T_4.ID 
		LEFT OUTER JOIN DBO.CAMPAIGN AS T_5 ON T_3.CAMPAIGNID = T_5.ID 
		LEFT OUTER JOIN DBO.APPEAL AS T_6 ON T_3.APPEALID = T_6.ID 
		LEFT OUTER JOIN DBO.PACKAGE AS T_7 ON T_3.PACKAGEID = T_7.ID

		WHERE 
		(((T_1.IMPORT_ID IS NOT NULL)) AND ((GIFT.TYPE NOT IN ('28','32','33') OR (GIFT.TYPE IS NULL))))  
 
		GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
  


-- APPEAL EXPENSES

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
 
		CREATE  VIEW dbo.HC_Appeal_Expenses_v
		AS

		SELECT 
			APPEAL.APPEAL_ID AppealID, APPEAL_APPEAL_EXPENSE.Import_ID APExpImpID, APPEAL_APPEAL_EXPENSE.AMOUNT APExpAmt ,
			APPEAL_APPEAL_EXPENSE.Budgeted APExpBud , APPEAL_APPEAL_EXPENSE.COMMENTS APExpCom ,
			dbo.dateformat(APPEAL_APPEAL_EXPENSE.DTE, 'MM/DD/YYYY') APExpDate ,
			T_1.LONGDESCRIPTION APExpExp ,
		 		 
			dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'GM' [RE_DB_Tbl],
			row_number() over (order by APPEAL.APPEAL_ID) as ID


		FROM 
			DBO.APPEAL AS APPEAL 
			INNER JOIN DBO.APPEAL_EXPENSE AS APPEAL_APPEAL_EXPENSE ON APPEAL.ID = APPEAL_APPEAL_EXPENSE.APPEAL_ID 
			LEFT OUTER JOIN DBO.TABLEENTRIES AS T_1 ON APPEAL_APPEAL_EXPENSE.EXPENSE_CODE = T_1.TABLEENTRIESID 
			LEFT OUTER JOIN DBO.PACKAGE AS T_2 ON APPEAL_APPEAL_EXPENSE.PACKAGE_ID = T_2.ID

		WHERE 
			((APPEAL_APPEAL_EXPENSE.AMOUNT IS NOT NULL))  

		GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
  

-- APPEAL BENEFIT. 

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

		CREATE  VIEW dbo.HC_Appeal_Benefits_v
		AS

			SELECT  Appeal.APPEAL_ID AS AppealID, Appeal_AppealBenefit.Import_Id AS APBenImpID, T_1.LONGDESCRIPTION AS APBenBen, 
					Appeal_AppealBenefit.Comments AS APBenCom, Appeal_AppealBenefit.Count AS APBenCount, Appeal_AppealBenefit.Sent AS APBenSent, 
					Appeal_AppealBenefit.TotalBenefitValue AS APBenTotVal, Appeal_AppealBenefit.UnitCost AS APBenUnitCost, 
					Appeal.BenefitsNotes APBenNotes, 
					case Appeal.BenefitsIncludeNotes when -1 then 'TRUE' else 'FALSE' end as APBenIncludeNotes,
					dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'AB' [RE_DB_Tbl],
					row_number() over (order by APPEAL.APPEAL_ID) as ID

			FROM    dbo.APPEAL AS Appeal 
			INNER JOIN dbo.AppealBenefit AS Appeal_AppealBenefit ON Appeal.ID = Appeal_AppealBenefit.ParentId 
			LEFT OUTER JOIN dbo.TABLEENTRIES AS T_1 ON Appeal_AppealBenefit.BenefitId = T_1.TABLEENTRIESID

	GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


-- APPEAL PACKAGE BENEFITS

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

	CREATE  VIEW dbo.HC_Package_Benefits_v
	AS

	SELECT     dbo.APPEAL.APPEAL_ID AS AppealID, dbo.Package.Package_ID AS PackageID, dbo.PackageBenefit.Import_Id AS PKBenImpID, 
				dbo.PackageBenefit.Comments AS PKBenCom, dbo.PackageBenefit.Count AS PKBenCount, dbo.PackageBenefit.Sent AS PKBenSent, 
				dbo.PackageBenefit.TotalBenefitValue AS PKBenTotVal, dbo.PackageBenefit.UnitCost AS PKBenUnitCost, dbo.Package.BenefitsNotes PKBenNotes, 
				case dbo.Package.BenefitsIncludeNotes when -1 then 'TRUE' else 'FALSE' end as PKBenIncludeNotes,
				dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'PB' [RE_DB_Tbl],
				row_number() over (order by APPEAL.APPEAL_ID) as ID



	FROM         dbo.APPEAL INNER JOIN
						  dbo.Package ON dbo.APPEAL.ID = dbo.Package.APPEAL_ID INNER JOIN
						  dbo.PackageBenefit ON dbo.Package.ID = dbo.PackageBenefit.ParentId
	GO
	
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

              
              
              

 
 --PLANNED GIFT ASSETS

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
 
		CREATE  VIEW dbo.HC_Planned_Gift_Asset_v    
		AS 
		
		SELECT 
		 GIFT_GIFTASSET.Amount GFPGAssetAmt, GIFT_GIFTASSET.CostBasis GFPGCostBasis, GIFT_GIFTASSET.Description GFPGAssetDesc,
		 GIFT_GIFTASSET.Import_ID GFPGAssetImpID, T_1.LONGDESCRIPTION GFPGAssetType, GIFT.IMPORT_ID GFPGAssetGiftImpID,  
		 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'PG' [RE_DB_Tbl],
		 row_number() over (order by GIFT.IMPORT_ID) as ID

		FROM 
		DBO.GIFT AS GIFT 
		INNER JOIN DBO.GIFTASSET AS GIFT_GIFTASSET ON GIFT.ID = GIFT_GIFTASSET.GIFTID 
		LEFT OUTER JOIN DBO.TABLEENTRIES AS T_1 ON GIFT_GIFTASSET.ASSETTYPE = T_1.TABLEENTRIESID

		WHERE (((GIFT_GIFTASSET.Import_ID IS NOT NULL)) AND ((GIFT.TYPE NOT IN ('28','32','33') OR (GIFT.TYPE IS NULL))))  

		GO
		
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
  

 --GIFT PROPOSAL LINK

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
 
		CREATE  VIEW dbo.HC_Gift_ProposalLink_v    
		AS 
		
			SELECT 
			 GIFT.IMPORT_ID AS GFLink,
			 T_1.IMPORT_ID PRImpID,
			 GIFT.Amount Amount,
			 T_1.AMOUNT_FUNDED UpdatePRAmtFunded,
			 T_1.Proposal_Name PRName,
			  dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'GP' [RE_DB_Tbl],
				row_number() over (order by T_1.IMPORT_ID, GIFT.IMPORT_ID) as ID

			 
			FROM 
			DBO.GIFT AS GIFT 
			INNER JOIN DBO.GIFTPROPOSAL AS GIFT_GIFTPROPOSAL ON GIFT.ID = GIFT_GIFTPROPOSAL.GIFTID 
			INNER JOIN DBO.PROPOSAL AS T_1 ON GIFT_GIFTPROPOSAL.PROPOSALID = T_1.ID

			WHERE 
			((DBO.Query_ProposalIsLinked(T_1.ID)  =  -1) AND ((GIFT.TYPE NOT IN ('28','32','33') OR (GIFT.TYPE IS NULL))))  
					
		GO
		
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO       
              
  




SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- ORGANIZATION RELATIONSHIPS FOR NON-CONSTITUENT INDIVIDUAL RELATIONSHIPS. 
CREATE VIEW dbo.HC_Org_Relat_NonConsIR_v
AS 
	SELECT					TBL_3.IMPORT_ID AS ORImpID,	
							'' ImportID, -- this ImportID is used only when the Ind Relat IS a Constituent -- TBL_2.IMPORT_ID AS ImportID, 
							''ConsID,    -- this ConsID   is used only when the Ind Relat IS a Constituent -- TBL_2.CONSTITUENT_ID AS ConsID, 	 
							CASE TBL_5.IS_CONSTITUENT WHEN - 1 THEN TBL_5.IMPORT_ID ELSE '' END AS ORLink, 
							TBL_1.IMPORT_ID [IRImpID],  -- this is the "Individual Relation Import ID" of the Ind Rel having and Org Rel linked to it,
							--T_23.IMPORT_ID AS [RRImpID], 
							TBL_5.ORG_NAME AS ORFullName,
				
		  				-- ADDRESS IDs for reference and linking purposes. 
							T_3.ADDRESS_ID as REFERENCE_ADDRESS_ID, T_3.ID as REFERENCE_ID, T_3.IMPORT_ID REFERENCE_IMPORT_ID,

							T_4.CARRIER_ROUTE AS ORAddrCART, dbo.ADDRESSLINE(1, T_4.ADDRESS_BLOCK) AS [AddrLine1], 
							dbo.ADDRESSLINE(2, T_4.ADDRESS_BLOCK) AS [AddrLine2], dbo.ADDRESSLINE(3, T_4.ADDRESS_BLOCK) AS [AddrLine3], 
							dbo.ADDRESSLINE(4, T_4.ADDRESS_BLOCK) AS [AddrLine4], dbo.ADDRESSLINE(5, T_4.ADDRESS_BLOCK) AS [AddrLine5], 
							T_4.CITY AS ORAddrCity, T_5.LONGDESCRIPTION AS ORAddrCountry, T_6.LONGDESCRIPTION AS ORAddrCounty, 
							T_3.DATE_FROM AS ORAddrValidFrom, T_3.DATE_TO AS ORAddrValidTo, T_4.DPC AS ORAddrDPC, T_7.LONGDESCRIPTION AS ORAddrInfoSrc, 
							T_4.LOT AS ORAddrLot, T_8.LONGDESCRIPTION AS ORAddrNZCity, T_9.LONGDESCRIPTION AS ORAddrNZSuburb, 
							T_10.LONGDESCRIPTION AS ORAddrRegion, CASE T_3.SEASONAL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORAddrSeasonal, 
							T_3.SEASONAL_FROM AS ORAddrSeaFrom, T_3.SEASONAL_TO AS ORAddrSeaTo, 
							CASE T_3.SENDMAIL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORSendMail, T_11.SHORTDESCRIPTION AS ORAddrState, 
							T_12.LONGDESCRIPTION AS ORAddrType, T_4.POST_CODE AS ORAddrZIP, 
							CASE TBL_3.HON_MEM_ACKNOWLEDGE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORHMAck, 
							CASE TBL_3.SOFTCREDIT_GIFTS WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORAutoSoftCredit, 
							T_13.LONGDESCRIPTION AS ORContactType, 
		                      
							case when  ISDATE(TBL_3.DATE_FROM)=1 AND LEN(TBL_3.DATE_FROM)=8 then cast(dbo.dateformat(TBL_3.DATE_FROM, 'MM/DD/YYYY') AS varchar(10))
							when  ISDATE(TBL_3.DATE_FROM)=1 AND LEN(TBL_3.DATE_FROM)=6 then cast(RIGHT(TBL_3.DATE_FROM,2) +'/'+ LEFT(TBL_3.DATE_FROM,4) AS varchar(10))
							when  ISDATE(TBL_3.DATE_FROM)=1 AND LEN(TBL_3.DATE_FROM)=4 then cast(TBL_3.DATE_FROM AS varchar(10))
							else null end AS  [ORFromDate],
							   
							case when  ISDATE(TBL_3.DATE_TO)=1 AND LEN(TBL_3.DATE_TO)=8 then cast(dbo.dateformat(TBL_3.DATE_TO, 'MM/DD/YYYY') AS varchar(10))
							when  ISDATE(TBL_3.DATE_TO)=1 AND LEN(TBL_3.DATE_TO)=6 then cast(RIGHT(TBL_3.DATE_TO,2) +'/'+ LEFT(TBL_3.DATE_TO,4) AS varchar(10))
							when  ISDATE(TBL_3.DATE_TO)=1 AND LEN(TBL_3.DATE_TO)=4 then cast(TBL_3.DATE_TO AS varchar(10))
							else null end AS  [ORToDate],
		                    
							CASE TBL_3.DO_NOT_MAIL WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORNoMail, TBL_3.FISCAL_YEAR_STARTS AS ORFiscalYear, 
							T_14.LONGDESCRIPTION AS ORIncome, T_15.LONGDESCRIPTION AS ORIndustry, 
							CASE TBL_3.IS_CONTACT WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORIsContact, 
							CASE TBL_3.IS_EMPLOYEE WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORIsEmp, 
							CASE TBL_3.IS_PRIMARY WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORIsPrimary, 
							REPLACE(REPLACE(REPLACE(CAST(TBL_3.MATCH_NOTES AS varchar), CHAR(13), ' '), CHAR(10), ' '), CHAR(9), ' ') AS ORMtcNotes, 
							TBL_3.MATCHING_FACTOR AS ORMtcFactor, TBL_3.MAX_MATCH_ANNUAL AS ORMaxMtcAnn, TBL_3.MAX_MATCH_PER_GIFT AS ORMaxMtcGift, 
							TBL_3.MAX_MATCH_TOTAL AS ORMaxMtcTotal, TBL_3.MIN_MATCH_ANNUAL AS ORMinMtcAnn, TBL_3.MIN_MATCH_PER_GIFT AS ORMinMtcGift, 
							TBL_3.MIN_MATCH_TOTAL AS ORMinMtcTot, 
							REPLACE(REPLACE(REPLACE(CAST(TBL_3.NOTES AS varchar(8000)), 
							CHAR(13), ' '), CHAR(10), ' '), CHAR(9), ' ') AS ORNotes, 
							CASE TBL_3.MATCHING_GIFT_FLAG WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS OROrgMatchesGift, TBL_3.POSITION AS ORPos, 
							CASE TBL_3.PRINT_ORG WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORPrintOrg, 
							CASE TBL_3.PRINT_POSITION WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END AS ORPrintPos, T_17.LONGDESCRIPTION AS ORProf, 
							T_18.LONGDESCRIPTION AS ORRecip, T_19.LONGDESCRIPTION AS ORRelat, 
	                                          
							dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'OR' [RE_DB_Tbl],
							row_number() over (order by TBL_3.IMPORT_ID) as ID 
	FROM       
				DBO.CONSTIT_RELATIONSHIPS AS CONS_RELAT 
				INNER JOIN DBO.QUERY_IND_RELATIONSHIPS AS TBL_1 ON CONS_RELAT.ID = TBL_1.ID 
				INNER JOIN DBO.RECORDS AS TBL_2 ON TBL_1.RELATION_ID = TBL_2.ID 

				INNER JOIN DBO.QUERY_ORG_RELATIONSHIPS AS TBL_3 ON TBL_2.ID = TBL_3.CONSTIT_ID 
				INNER JOIN DBO.RECORDS AS TBL_4 ON CONS_RELAT.CONSTIT_ID = TBL_4.ID 
				LEFT OUTER JOIN DBO.RECORDS AS TBL_5 ON TBL_3.RELATION_ID = TBL_5.ID
				LEFT JOIN  dbo.CONSTIT_RELATIONSHIPS AS T_23 ON TBL_3.RECIPROCAL_ID = T_23.ID
	 			LEFT OUTER JOIN dbo.CONSTIT_ADDRESS AS T_3 ON TBL_3.CONSTIT_ADDRESS_ID = T_3.ID 
				
				LEFT OUTER JOIN dbo.ADDRESS AS T_4 ON T_3.ADDRESS_ID = T_4.ID LEFT OUTER JOIN
						  dbo.TABLEENTRIES AS T_5 ON T_4.COUNTRY = T_5.TABLEENTRIESID LEFT OUTER JOIN
						  dbo.TABLEENTRIES AS T_6 ON T_4.COUNTY = T_6.TABLEENTRIESID LEFT OUTER JOIN
						  dbo.TABLEENTRIES AS T_7 ON T_3.INFO_SOURCE = T_7.TABLEENTRIESID LEFT OUTER JOIN
						  dbo.TABLEENTRIES AS T_8 ON T_4.NZCITY = T_8.TABLEENTRIESID LEFT OUTER JOIN
						  dbo.TABLEENTRIES AS T_9 ON T_4.SUBURB = T_9.TABLEENTRIESID LEFT OUTER JOIN
						  dbo.TABLEENTRIES AS T_10 ON T_4.REGION = T_10.TABLEENTRIESID LEFT OUTER JOIN
						  dbo.STATES	   AS T_11 ON T_4.STATE = T_11.SHORTDESCRIPTION LEFT OUTER JOIN
						  dbo.TABLEENTRIES AS T_12 ON T_3.TYPE = T_12.TABLEENTRIESID LEFT OUTER JOIN
						  dbo.TABLEENTRIES AS T_13 ON TBL_3.CONTACT_TYPE = T_13.TABLEENTRIESID LEFT OUTER JOIN
						  dbo.TABLEENTRIES AS T_14 ON TBL_3.INCOME = T_14.TABLEENTRIESID LEFT OUTER JOIN
						  dbo.TABLEENTRIES AS T_15 ON TBL_3.INDUSTRY = T_15.TABLEENTRIESID LEFT OUTER JOIN
						  dbo.TABLEENTRIES AS T_17 ON TBL_3.PROFESSION = T_17.TABLEENTRIESID LEFT OUTER JOIN
						  dbo.TABLEENTRIES AS T_18 ON TBL_3.RECIP_RELATION_CODE = T_18.TABLEENTRIESID LEFT OUTER JOIN
						  dbo.TABLEENTRIES AS T_19 ON TBL_3.RELATION_CODE = T_19.TABLEENTRIESID 

	 
	WHERE			(((TBL_1.IMPORT_ID IS NOT NULL) AND TBL_2.IS_CONSTITUENT  =  0 AND (TBL_3.IMPORT_ID IS NOT NULL)) AND (TBL_4.IS_CONSTITUENT  =  -1))  
	GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
  




 

-- GIFT PLANNED BENEFICIARIES
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.HC_Planned_Gift_Beneficiaries_v
AS 
	-- Individual and Organization Relationships
	SELECT     dbo.GIFT.UserGiftId AS GiftID, dbo.GIFT.IMPORT_ID AS GFPGBenGiftImpID, dbo.CONSTIT_RELATIONSHIPS.IMPORT_ID AS GFPGBenRelImpID, 
			   CASE T_1.BeneficiaryType WHEN 1 THEN 'Donor' WHEN 2 THEN 'Active' WHEN 3 THEN 'Inactive' WHEN 4 THEN 'Remainderman' WHEN 5 THEN 'Non-Income' WHEN
			   6 THEN 'Donor + Active' WHEN 7 THEN 'Donor + Inactive' WHEN 8 THEN 'Donor + Non-Income' WHEN 9 THEN 'Primary' WHEN 10 THEN 'Secondary' ELSE '' END AS GFPGBenType,
			   CASE T_1.TypeOfRelationship WHEN 0 THEN 'Self' WHEN 1 THEN 'Individual' WHEN 2 THEN 'Organization' WHEN 3 THEN 'Financial' WHEN 4 THEN 'Education' ELSE
			   '' END AS GFPGBenTypeofRelat,
			   dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'BE' [RE_DB_Tbl],
			   T_1.ID 
	FROM       dbo.GiftBeneficiary AS T_1 
			   INNER JOIN dbo.GIFT ON T_1.GiftID = dbo.GIFT.ID 
			   INNER JOIN dbo.CONSTIT_RELATIONSHIPS ON T_1.ConstitRelationshipID = dbo.CONSTIT_RELATIONSHIPS.ID

	UNION ALL 
    -- Bank/Financial Relationships
  	SELECT     dbo.GIFT.UserGiftId AS GiftID, dbo.GIFT.IMPORT_ID AS GFPGBenGiftImpID, dbo.CONSTITUENT_BANK.IMPORT_ID  AS GFPGBenRelImpID, 
			   CASE T_1.BeneficiaryType WHEN 1 THEN 'Donor' WHEN 2 THEN 'Active' WHEN 3 THEN 'Inactive' WHEN 4 THEN 'Remainderman' WHEN 5 THEN 'Non-Income' WHEN
               6 THEN 'Donor + Active' WHEN 7 THEN 'Donor + Inactive' WHEN 8 THEN 'Donor + Non-Income' WHEN 9 THEN 'Primary' WHEN 10 THEN 'Secondary' ELSE '' END AS GFPGBenType,
               CASE T_1.TypeOfRelationship WHEN 0 THEN 'Self' WHEN 1 THEN 'Individual' WHEN 2 THEN 'Organization' WHEN 3 THEN 'Financial' WHEN 4 THEN 'Education' ELSE
               '' END AS GFPGBenTypeofRelat,
			   dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'BE' [RE_DB_Tbl],
			   T_1.ID 
	FROM       dbo.GiftBeneficiary AS T_1 
		       INNER JOIN dbo.GIFT ON T_1.GiftID = dbo.GIFT.ID 
			   INNER JOIN dbo.CONSTITUENT_BANK ON T_1.ConstitBankID = dbo.CONSTITUENT_BANK.ID
	UNION ALL 
    -- Education

	SELECT     dbo.GIFT.UserGiftId AS GiftID, dbo.GIFT.IMPORT_ID AS GFPGBenGiftImpID, dbo.EDUCATION.IMPORT_ID AS GFPGBenRelImpID, 
               CASE T_1.BeneficiaryType WHEN 1 THEN 'Donor' WHEN 2 THEN 'Active' WHEN 3 THEN 'Inactive' WHEN 4 THEN 'Remainderman' WHEN 5 THEN 'Non-Income' WHEN
               6 THEN 'Donor + Active' WHEN 7 THEN 'Donor + Inactive' WHEN 8 THEN 'Donor + Non-Income' WHEN 9 THEN 'Primary' WHEN 10 THEN 'Secondary' ELSE '' END AS GFPGBenType,
               CASE T_1.TypeOfRelationship WHEN 0 THEN 'Self' WHEN 1 THEN 'Individual' WHEN 2 THEN 'Organization' WHEN 3 THEN 'Financial' WHEN 4 THEN 'Education' ELSE
               '' END AS GFPGBenTypeofRelat,
			   dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'BE' [RE_DB_Tbl],
			   T_1.ID 
	FROM		dbo.GiftBeneficiary AS T_1 INNER JOIN
                dbo.GIFT ON T_1.GiftID = dbo.GIFT.ID INNER JOIN
                dbo.EDUCATION ON T_1.ConstitEducationID = dbo.EDUCATION.ID
    
	UNION ALL
	
	--Self 
	SELECT     dbo.GIFT.UserGiftId AS GiftID, dbo.GIFT.IMPORT_ID AS GFPGBenGiftImpID, 'NULL' as GFPGBenRelImpID,
               CASE T_1.BeneficiaryType WHEN 1 THEN 'Donor' WHEN 2 THEN 'Active' WHEN 3 THEN 'Inactive' WHEN 4 THEN 'Remainderman' WHEN 5 THEN 'Non-Income' WHEN
               6 THEN 'Donor + Active' WHEN 7 THEN 'Donor + Inactive' WHEN 8 THEN 'Donor + Non-Income' WHEN 9 THEN 'Primary' WHEN 10 THEN 'Secondary' ELSE '' END AS GFPGBenType,
               CASE T_1.TypeOfRelationship WHEN 0 THEN 'Self' WHEN 1 THEN 'Individual' WHEN 2 THEN 'Organization' WHEN 3 THEN 'Financial' WHEN 4 THEN 'Education' ELSE
               '' END AS GFPGBenTypeofRelat,
			   dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'BE' [RE_DB_Tbl],
			   T_1.ID 
	FROM       dbo.GiftBeneficiary AS T_1 
			   INNER JOIN dbo.GIFT ON T_1.GiftID = dbo.GIFT.ID
	WHERE	   T_1.TypeOfRelationship ='0'

GO


SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


-- GIFT PLANNED RELATIONSHIPS
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.HC_Planned_Gift_Relationship_v 
AS 
	-- Individual and Organization Relationships
	SELECT     dbo.GIFT.UserGiftId AS GiftID, dbo.GIFT.IMPORT_ID AS GFPGRelGiftImpID, dbo.CONSTIT_RELATIONSHIPS.IMPORT_ID AS GFPGRelImpID, 
			   CASE T_1.TypeOfRelationship WHEN 0 THEN 'Self' WHEN 1 THEN 'Individual' WHEN 2 THEN 'Organization' WHEN 3 THEN 'Financial' WHEN 4 THEN 'Education' ELSE
			   '' END AS GFPGRelTypeofRel,
			   dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'RE' [RE_DB_Tbl],
			   T_1.ID 
	FROM       dbo.GiftRelationship AS T_1 
			   INNER JOIN dbo.GIFT ON T_1.GiftID = dbo.GIFT.ID 
			   INNER JOIN dbo.CONSTIT_RELATIONSHIPS ON T_1.ConstitRelationshipID = dbo.CONSTIT_RELATIONSHIPS.ID

	UNION ALL 
    -- Bank/Financial Relationships
  	SELECT     dbo.GIFT.UserGiftId AS GiftID, dbo.GIFT.IMPORT_ID AS GFPGRelGiftImpID, dbo.CONSTITUENT_BANK.IMPORT_ID  AS GFPGRelImpID, 
               CASE T_1.TypeOfRelationship WHEN 0 THEN 'Self' WHEN 1 THEN 'Individual' WHEN 2 THEN 'Organization' WHEN 3 THEN 'Financial' WHEN 4 THEN 'Education' ELSE
               '' END AS GFPGRelTypeofRel,
			   dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'RE' [RE_DB_Tbl],
			   T_1.ID  
	FROM       dbo.GiftRelationship AS T_1 
		       INNER JOIN dbo.GIFT ON T_1.GiftID = dbo.GIFT.ID 
			   INNER JOIN dbo.CONSTITUENT_BANK ON T_1.ConstitBankID = dbo.CONSTITUENT_BANK.ID
	UNION ALL 
    -- Education

	SELECT     dbo.GIFT.UserGiftId AS GiftID, dbo.GIFT.IMPORT_ID AS GFPGRelGiftImpID, dbo.EDUCATION.IMPORT_ID AS GFPGRelImpID, 
               CASE T_1.TypeOfRelationship WHEN 0 THEN 'Self' WHEN 1 THEN 'Individual' WHEN 2 THEN 'Organization' WHEN 3 THEN 'Financial' WHEN 4 THEN 'Education' ELSE
               '' END AS GFPGRelTypeofRel,
			   dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'RE' [RE_DB_Tbl],
			   T_1.ID  
	FROM		dbo.GiftRelationship AS T_1 INNER JOIN
                dbo.GIFT ON T_1.GiftID = dbo.GIFT.ID INNER JOIN
                dbo.EDUCATION ON T_1.ConstitEducationID = dbo.EDUCATION.ID
    
	UNION ALL
	
	--Self 
	SELECT     dbo.GIFT.UserGiftId AS GiftID, dbo.GIFT.IMPORT_ID AS GFPGRelGiftImpID, 'NULL' as GFPGRelImpID,
               CASE T_1.TypeOfRelationship WHEN 0 THEN 'Self' WHEN 1 THEN 'Individual' WHEN 2 THEN 'Organization' WHEN 3 THEN 'Financial' WHEN 4 THEN 'Education' ELSE
               '' END AS GFPGRelTypeofRel,
			   dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'RE' [RE_DB_Tbl],
			   T_1.ID  
	FROM       dbo.GiftRelationship AS T_1 
			   INNER JOIN dbo.GIFT ON T_1.GiftID = dbo.GIFT.ID
	WHERE	   T_1.TypeOfRelationship ='0'

GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[HC_Media_v]
AS
	SELECT 
	 RECORDS.IMPORT_ID as ImportID, 
	 RECORDS_MEDIA.ID as MediaID,
	 RECORDS_MEDIA_TYPE.LONGDESCRIPTION as MediaType,
	 
     CASE WHEN  ISDATE(RECORDS_MEDIA.MEDIADATE)=1 AND LEN(RECORDS_MEDIA.MEDIADATE)=8 THEN CAST(DBO.DATEFORMAT(RECORDS_MEDIA.MEDIADATE, 'MM/DD/YYYY') as varchar(10))
	 WHEN ISDATE(RECORDS_MEDIA.MEDIADATE)=1 AND LEN(RECORDS_MEDIA.MEDIADATE)=6 THEN CAST(RIGHT(RECORDS_MEDIA.MEDIADATE,2) +'/'+ LEFT(RECORDS_MEDIA.MEDIADATE,4) AS varchar(10))
	 WHEN ISDATE(RECORDS_MEDIA.MEDIADATE)=1 AND LEN(RECORDS_MEDIA.MEDIADATE)=4 THEN cast(RECORDS_MEDIA.MEDIADATE as varchar(10))
     ELSE NULL END AS [MediaDate],
	 
	 
	 RECORDS_MEDIA.DESCRIPTION as MediaDescription,
	 RECORDS_MEDIA.TITLE as MediaTitle, RECORDS_MEDIA.AUTHOR as MediaAuthor,
	 records_media_PROPOSAL.Proposal_Name as MediaProposal, records_media_proposal.import_id MediaProposalImportID,
	 dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort],  'ME' [RE_DB_Tbl],
	 row_number() over (order by RECORDS_MEDIA.ID) as ID

	FROM 
	DBO.RECORDS AS RECORDS 
	INNER JOIN DBO.MEDIA AS RECORDS_MEDIA ON RECORDS.ID = RECORDS_MEDIA.PARENT_ID 
	LEFT OUTER JOIN DBO.TABLEENTRIES AS RECORDS_MEDIA_TYPE ON RECORDS_MEDIA.TYPE = RECORDS_MEDIA_TYPE.TABLEENTRIESID 
	LEFT OUTER JOIN DBO.PROPOSAL AS records_media_PROPOSAL ON RECORDS_MEDIA.PROPOSAL_ID = records_media_PROPOSAL.ID

	WHERE 
	((RECORDS_MEDIA.TYPE IS NOT NULL) AND (RECORDS.IS_CONSTITUENT  =  -1))  
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



	  
-- FUND ATTRIBUTES
	SET QUOTED_IDENTIFIER ON 
	GO
	SET ANSI_NULLS ON 
	GO

		CREATE VIEW dbo.HC_Fund_Attr_v
		AS	

		SELECT 
		FUND.FUND_ID AS 'FundID', FUND_FUNDATTRIBUTES.IMPORTID as 'FundAttrImpID', T_1.DESCRIPTION as 'FundAttrCat',
		 dbo.dateformat(FUND_FUNDATTRIBUTES.ATTRIBUTEDATE, 'MM/DD/YYYY')  as 'FundAttrDate', FUND_FUNDATTRIBUTES.COMMENTS 'FundAttrCom',
		FundAttrDesc = CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
							  WHEN FUND_FUNDATTRIBUTES.boolean IS NOT NULL THEN (CASE FUND_FUNDATTRIBUTES.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END)
							  WHEN FUND_FUNDATTRIBUTES.text IS NOT NULL 
							  THEN FUND_FUNDATTRIBUTES.text WHEN FUND_FUNDATTRIBUTES.num IS NOT NULL THEN CONVERT(varchar, FUND_FUNDATTRIBUTES.num) 
							  WHEN FUND_FUNDATTRIBUTES.datetime IS NOT NULL THEN CONVERT(varchar, FUND_FUNDATTRIBUTES.datetime) 
							  WHEN FUND_FUNDATTRIBUTES.currency IS NOT NULL THEN CONVERT(varchar, FUND_FUNDATTRIBUTES.currency) 
							  WHEN FUND_FUNDATTRIBUTES.constitid IS NOT NULL THEN CONVERT(varchar, FUND_FUNDATTRIBUTES.constitid) ELSE NULL END,
							  case T_1.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
													   when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
													   when 8 then 'FuzzyDate' end as [DataType],
						dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'FA' [RE_DB_Tbl],
						row_number() over (order by ID) as ID													    
		FROM	DBO.FUND AS FUND 
				INNER JOIN DBO.FUNDATTRIBUTES AS FUND_FUNDATTRIBUTES ON FUND.ID = FUND_FUNDATTRIBUTES.PARENTID 
				LEFT OUTER JOIN DBO.ATTRIBUTETYPES AS T_1 ON FUND_FUNDATTRIBUTES.ATTRIBUTETYPESID = T_1.ATTRIBUTETYPESID
				LEFT JOIN dbo.TABLEENTRIES ON FUND_FUNDATTRIBUTES.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID
		WHERE (((FUND_FUNDATTRIBUTES.IMPORTID IS NOT NULL)))  	

	GO
	SET QUOTED_IDENTIFIER OFF 
	GO
	SET ANSI_NULLS ON 
	GO


--CAMPAING ATTRIBUTES
	SET QUOTED_IDENTIFIER ON 
	GO
	SET ANSI_NULLS ON 
	GO

		CREATE VIEW dbo.HC_Campaign_Attr_v  
		AS	

			 SELECT 
				 CAMPAIGN.CAMPAIGN_ID as 'CampID', CAMPAIGN_ATTRIBUTES.IMPORTID as 'CampAttrImpID',
				 T_1.DESCRIPTION as 'CampAttrCat', dbo.dateformat(CAMPAIGN_ATTRIBUTES.ATTRIBUTEDATE, 'MM/DD/YYYY')  as 'CampAttrDate', 
				 CAMPAIGN_ATTRIBUTES.COMMENTS as 'CampAttrCom',
				 CampAttrDesc =	CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
										  WHEN CAMPAIGN_ATTRIBUTES.boolean IS NOT NULL THEN (CASE CAMPAIGN_ATTRIBUTES.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END)
										  WHEN CAMPAIGN_ATTRIBUTES.text IS NOT NULL 
										  THEN CAMPAIGN_ATTRIBUTES.text WHEN CAMPAIGN_ATTRIBUTES.num IS NOT NULL THEN CONVERT(varchar, CAMPAIGN_ATTRIBUTES.num) 
										  WHEN CAMPAIGN_ATTRIBUTES.datetime IS NOT NULL THEN CONVERT(varchar, CAMPAIGN_ATTRIBUTES.datetime) 
										  WHEN CAMPAIGN_ATTRIBUTES.currency IS NOT NULL THEN CONVERT(varchar, CAMPAIGN_ATTRIBUTES.currency) 
										  WHEN CAMPAIGN_ATTRIBUTES.constitid IS NOT NULL THEN CONVERT(varchar, CAMPAIGN_ATTRIBUTES.constitid) ELSE NULL END,
										  case T_1.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
											when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
											when 8 then 'FuzzyDate' end as [DataType],
									dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'CE' [RE_DB_Tbl],
									row_number() over (order by ID) as ID													    
				FROM DBO.CAMPAIGN AS CAMPAIGN 
				INNER JOIN DBO.CAMPAIGNATTRIBUTES AS CAMPAIGN_ATTRIBUTES ON CAMPAIGN.ID = CAMPAIGN_ATTRIBUTES.PARENTID 
				LEFT OUTER JOIN DBO.ATTRIBUTETYPES AS T_1 ON CAMPAIGN_ATTRIBUTES.ATTRIBUTETYPESID = T_1.ATTRIBUTETYPESID
				LEFT JOIN dbo.TABLEENTRIES ON CAMPAIGN_ATTRIBUTES.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID
				WHERE (((CAMPAIGN_ATTRIBUTES.IMPORTID IS NOT NULL)))  
	GO
	SET QUOTED_IDENTIFIER OFF 
	GO
	SET ANSI_NULLS ON 
	GO


--APPEAL ATTRIBUTES
	SET QUOTED_IDENTIFIER ON 
	GO
	SET ANSI_NULLS ON 
	GO
		CREATE VIEW dbo.HC_Appeal_Attr_v  
		AS	
		SELECT 
		APPEAL.APPEAL_ID as 'AppealID',
		APPEAL_APPEALATTRIBUTES.IMPORTID as 'APAttrImpID', T_1.DESCRIPTION as 'APAttrCat',
		dbo.dateformat(APPEAL_APPEALATTRIBUTES.ATTRIBUTEDATE, 'MM/DD/YYYY')  as 'APAttrDate', APPEAL_APPEALATTRIBUTES.COMMENTS as 'APAttrCom', 
		APAttrDesc = CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
					WHEN APPEAL_APPEALATTRIBUTES.boolean IS NOT NULL THEN (CASE APPEAL_APPEALATTRIBUTES.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END)
					WHEN APPEAL_APPEALATTRIBUTES.text IS NOT NULL 
					THEN APPEAL_APPEALATTRIBUTES.text WHEN APPEAL_APPEALATTRIBUTES.num IS NOT NULL THEN CONVERT(varchar, APPEAL_APPEALATTRIBUTES.num) 
					WHEN APPEAL_APPEALATTRIBUTES.datetime IS NOT NULL THEN CONVERT(varchar, dbo.dateformat(APPEAL_APPEALATTRIBUTES.datetime, 'MM/DD/YYYY')) 
					WHEN APPEAL_APPEALATTRIBUTES.currency IS NOT NULL THEN CONVERT(varchar, APPEAL_APPEALATTRIBUTES.currency) 
					WHEN APPEAL_APPEALATTRIBUTES.constitid IS NOT NULL THEN CONVERT(varchar, APPEAL_APPEALATTRIBUTES.constitid) ELSE NULL END,
					case T_1.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
					when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
					when 8 then 'FuzzyDate' end as [DataType],
					dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'AU' [RE_DB_Tbl],
					row_number() over (order by ID) as ID													    
		FROM DBO.APPEAL AS APPEAL 
		INNER JOIN DBO.APPEALATTRIBUTES AS APPEAL_APPEALATTRIBUTES ON APPEAL.ID = APPEAL_APPEALATTRIBUTES.PARENTID 
		LEFT OUTER JOIN DBO.ATTRIBUTETYPES AS T_1 ON APPEAL_APPEALATTRIBUTES.ATTRIBUTETYPESID = T_1.ATTRIBUTETYPESID
		LEFT JOIN dbo.TABLEENTRIES ON APPEAL_APPEALATTRIBUTES.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID
		WHERE (((APPEAL_APPEALATTRIBUTES.IMPORTID IS NOT NULL)))  

	GO
	SET QUOTED_IDENTIFIER OFF 
	GO
	SET ANSI_NULLS ON 
	GO


-- APPEAL PACKAGE ATTRIBUTES
	SET QUOTED_IDENTIFIER ON 
	GO
	SET ANSI_NULLS ON 
	GO
		CREATE VIEW dbo.HC_Package_Attr_v  
		AS	

		SELECT 
			APPEAL.APPEAL_ID as 'AppealID', APPEAL_PACKAGE.Package_ID as 'PackageID', T_1.IMPORTID as 'PKAttrImpID',
			T_1.DESCRIPTION as 'PKAttrCat', dbo.dateformat(PACKAGE_ATTRIBUTES.ATTRIBUTEDATE, 'MM/DD/YYYY')  as 'PKAttrDate', 
			PACKAGE_ATTRIBUTES.COMMENTS as 'PKAttrCom',
			
			PKAttrDesc  = CASE WHEN dbo.TABLEENTRIES.LONGDESCRIPTION IS NOT NULL THEN dbo.TABLEENTRIES.LONGDESCRIPTION 
								  WHEN PACKAGE_ATTRIBUTES.boolean IS NOT NULL THEN (CASE PACKAGE_ATTRIBUTES.boolean WHEN 0 THEN 'False' WHEN - 1 THEN 'True' ELSE '' END)
								  WHEN PACKAGE_ATTRIBUTES.text IS NOT NULL 
								  THEN PACKAGE_ATTRIBUTES.text WHEN PACKAGE_ATTRIBUTES.num IS NOT NULL THEN CONVERT(varchar, PACKAGE_ATTRIBUTES.num) 
								  WHEN PACKAGE_ATTRIBUTES.datetime IS NOT NULL THEN CONVERT(varchar, dbo.dateformat(PACKAGE_ATTRIBUTES.datetime, 'MM/DD/YYYY')) 
								  WHEN PACKAGE_ATTRIBUTES.currency IS NOT NULL THEN CONVERT(varchar, PACKAGE_ATTRIBUTES.currency) 
								  WHEN PACKAGE_ATTRIBUTES.constitid IS NOT NULL THEN CONVERT(varchar, PACKAGE_ATTRIBUTES.constitid) ELSE NULL END,
								  case T_1.TYPEOFDATA when 1 then 'Text' when 2 then 'Number' when 3 then 'Date' when 4 then 'Currency' 
														   when 5 then 'Boolean' when 6 then 'TableEntry' when 7 then 'Constituent' 
														   when 8 then 'FuzzyDate' end as [DataType],
							dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 
							'PU' [RE_DB_Tbl], 
							row_number() over (order by PACKAGE_ATTRIBUTES.ATTRIBUTESID) as ID													    


			FROM 
			DBO.APPEAL AS APPEAL 
			INNER JOIN DBO.PACKAGE AS APPEAL_PACKAGE ON APPEAL.ID = APPEAL_PACKAGE.APPEAL_ID 
			INNER JOIN DBO.PACKAGEATTRIBUTES AS PACKAGE_ATTRIBUTES ON APPEAL_PACKAGE.ID = PACKAGE_ATTRIBUTES.PARENTID 
			LEFT JOIN DBO.ATTRIBUTETYPES AS T_1 ON PACKAGE_ATTRIBUTES.ATTRIBUTETYPESID = T_1.ATTRIBUTETYPESID
			LEFT JOIN dbo.TABLEENTRIES ON PACKAGE_ATTRIBUTES.TABLEENTRIESID = dbo.TABLEENTRIES.TABLEENTRIESID
			WHERE 
			(((T_1.IMPORTID IS NOT NULL)))   

	GO
	SET QUOTED_IDENTIFIER OFF 
	GO
	SET ANSI_NULLS ON 
	GO

	
		--HC_Membership_v


		SET QUOTED_IDENTIFIER ON 
		GO
		SET ANSI_NULLS ON 
		GO

 
		CREATE VIEW dbo.HC_Membership_v
		AS

				SELECT 
				 Memberships_RECORDS.CONSTITUENT_ID as ConsID, Memberships_RECORDS.IMPORT_ID as ImportID, 
				 Memberships.Import_ID as MEImpID, Memberships.MemID as MembershipID, 
				 cast(dbo.dateformat(Memberships.Date_Joined, 'MM/DD/YYYY') as nvarchar(20)) as MECycleStart, 
				 cast(dbo.dateformat(T_1.ExpiresOn, 'MM/DD/YYYY') as nvarchar(20)) as MEExpiresDate,
				 CASE T_1.LifeTimeMembership WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END as MELifetime,
				 T_1.Dues as MEDues, 
				 CASE DBO.Query_MembershipIsLinked(T_1.ID) WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  as MELinkedToGift, 
				 CASE T_1.Type WHEN 1 THEN 'Joined' WHEN 2 THEN 'Renewal' WHEN 3 THEN 'Rejoined' WHEN 4 THEN 'Upgrade' WHEN 5 THEN 'Downgrade' WHEN 6 THEN 'Dropped' ELSE '' END  as METype,
				  T_3.LONGDESCRIPTION as MECat, T_8.LONGDESCRIPTION as MESubCat,
				 T_4.LONGDESCRIPTION as MEProgram, T_5.LONGDESCRIPTION MEReason,
		 
				 CASE T_1.PrintRenewals WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  as MEPrintNotices,
				 CASE T_1.SendNoticeTo WHEN 0 THEN 'Donor' WHEN 1 THEN 'Primary Member' WHEN 2 THEN 'Both' ELSE '' END  as MESendNoticesTo,
				 CASE T_6.SentTo WHEN 0 THEN 'Donor' WHEN 1 THEN 'Primary Member' WHEN 2 THEN 'Both' ELSE '' END  as MERenewalSentTo,
				 T_9.import_id as MEGivenBy, 
				 T_1.OverrideRenewalDefaults MEOverrideRenewalDef, T_1.WaiveBenefits as MEWaiveBenefit,
				 Memberships.TotalChildren as MENumChild, Memberships.TotalMembers as MENumMembers,
				 CASE Memberships.Standing WHEN 0 THEN 'New' WHEN 1 THEN 'Active' WHEN 3 THEN 'Dropped' WHEN 2 THEN 'Lapsed' ELSE '' END  as MEStanding,
				 cast(T_1.MemComment as nvarchar(max)) as MECom, 
				 cast(Memberships.Notes as nvarchar(max)) as MENotes, 
				 cast(T_1.BenefitsNotes as nvarchar(max)) as MEBenNotes, T_1.SpecialMessage as MEGiftSpecialMsg,
				 T_1.Sequence, case Memberships.PrimaryMember when '0' then 'FALSE' when '-1' then 'TRUE' end as MEPrimaryMember,
				 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() as [RE_DB_OwnerShort]

		 
				FROM 
				DBO.MEMBERSHIPS AS Memberships 
				INNER JOIN DBO.MEMBERSHIPTRANSACTION AS T_1 ON Memberships.CURRENTTRANSACTIONID = T_1.ID 
				INNER JOIN DBO.MEMBERSHIPCATEGORY AS T_2 ON T_1.CATEGORY = T_2.MEMBERSHIPCATEGORYID 
				INNER JOIN DBO.TABLEENTRIES AS T_3 ON T_2.CATEGORYID = T_3.TABLEENTRIESID 
				INNER JOIN DBO.RECORDS AS Memberships_RECORDS ON Memberships.RECID = Memberships_RECORDS.ID 
				LEFT OUTER JOIN DBO.TABLEENTRIES AS T_4 ON T_1.PROGRAM = T_4.TABLEENTRIESID 
				LEFT OUTER JOIN DBO.TABLEENTRIES AS T_5 ON T_1.REASON = T_5.TABLEENTRIESID 
				LEFT OUTER JOIN DBO.MEMBERSHIPRENEWALS AS T_6 ON T_1.ID = T_6.MEMBERSHIPID 
				LEFT OUTER JOIN DBO.MEMBERSHIPSUBCATEGORIES AS T_7 ON T_1.SUBCATEGORY = T_7.MEMBERSHIPSUBCATEGORIESID 
				LEFT OUTER JOIN DBO.TABLEENTRIES AS T_8 ON T_7.SUBCATEGORYID = T_8.TABLEENTRIESID
				LEFT OUTER JOIN DBO.RECORDS AS T_9 ON T_1.GIVENBY = T_9.ID
	
		GO
		SET QUOTED_IDENTIFIER OFF 
		GO
		SET ANSI_NULLS ON 
		GO	 

		-- HC_Membership_History_v

		SET QUOTED_IDENTIFIER ON 
		GO
		SET ANSI_NULLS ON 
		GO

 
		CREATE VIEW dbo.HC_Membership_History_v
		AS

				SELECT DISTINCT 
				 Memberships_RECORDS.CONSTITUENT_ID as CosnID, Memberships_RECORDS.IMPORT_ID as ImportID, 
				 Memberships.Import_ID as MEImpID, T_1.Import_ID as MECurImportID ,
				 T_2.Import_ID as MEHisImportID , Memberships.MemID as MembershipID, Memberships.MemID as HisMembershipID,   
				  CASE	WHEN T_2.Type= 1 THEN 'Joined'
					WHEN T_2.Type= 2 AND T_2.RenewalType= 1 THEN 'Renewal - Downgrade'
					WHEN T_2.Type= 2 AND T_2.RenewalType= 2 THEN 'Renewal - Same'
					WHEN T_2.Type= 2 AND T_2.RenewalType= 0 THEN 'Renewal - Upgrade'
					WHEN T_2.Type= 3 AND T_2.RenewalType= 1 THEN 'Rejoined - Downgrade'
					WHEN T_2.Type= 3 AND T_2.RenewalType= 2 THEN 'Rejoined - Same'
					WHEN T_2.Type= 3 AND T_2.RenewalType= 0 THEN 'Rejoined - Upgrade'
					WHEN T_2.Type= 4 THEN 'Upgrade'
					WHEN T_2.Type= 5 THEN 'Downgrade'
					WHEN T_2.Type= 6 THEN 'Dropped'
					ELSE '' END AS 'Transaction_Type',
				  cast(dbo.dateformat(
					 CASE	WHEN T_2.Type= 1 THEN DBO.Query_MemActivityDate(1,T_2.Type,T_2.ActivityDate,-1,-1) 
							WHEN T_2.Type= 2 AND T_2.RenewalType= 1 THEN DBO.Query_MemActivityDate(2,T_2.Type,T_2.ActivityDate,T_2.RenewalType,1)  
							WHEN T_2.Type= 2 AND T_2.RenewalType= 2 THEN DBO.Query_MemActivityDate(2,T_2.Type,T_2.ActivityDate,T_2.RenewalType,2)  
							WHEN T_2.Type= 2 AND T_2.RenewalType= 0 THEN DBO.Query_MemActivityDate(2,T_2.Type,T_2.ActivityDate,T_2.RenewalType,0)  
							WHEN T_2.Type= 3 AND T_2.RenewalType= 1 THEN DBO.Query_MemActivityDate(3,T_2.Type,T_2.ActivityDate,T_2.RenewalType,1)  
							WHEN T_2.Type= 3 AND T_2.RenewalType= 2 THEN DBO.Query_MemActivityDate(3,T_2.Type,T_2.ActivityDate,T_2.RenewalType,2)  
							WHEN T_2.Type= 3 AND T_2.RenewalType= 0 THEN DBO.Query_MemActivityDate(3,T_2.Type,T_2.ActivityDate,T_2.RenewalType,0)  
							WHEN T_2.Type= 4 THEN DBO.Query_MemActivityDate(4,T_2.Type,T_2.ActivityDate,-1,-1) 
							WHEN T_2.Type= 5 THEN DBO.Query_MemActivityDate(5,T_2.Type,T_2.ActivityDate,-1,-1) 
							WHEN T_2.Type= 6 THEN DBO.Query_MemActivityDate(6,T_2.Type,T_2.ActivityDate,-1,-1) 
							ELSE ''
				  END, 'MM/DD/YYYY') as nvarchar(20)) AS 'Transaction_Date',
	 
				 cast(dbo.dateformat(T_2.ExpiresOn, 'MM/DD/YYYY')  as nvarchar(20)) as MEExpiresDate,
 				 CASE T_2.LifeTimeMembership WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END as MELifetime,
				 T_2.Dues as MEDues, 
      			 CASE DBO.Query_MembershipIsLinked(T_2.ID) WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  as MELinkedToGift, 
				 CASE T_2.Type WHEN 1 THEN 'Joined' WHEN 2 THEN 'Renewal' WHEN 3 THEN 'Rejoined' WHEN 4 THEN 'Upgrade' WHEN 5 THEN 'Downgrade' WHEN 6 THEN 'Dropped' ELSE '' END  as METype,
  				 T_9.LONGDESCRIPTION as MECat, T_7.LONGDESCRIPTION as MESubCat,
  				 T_3.LONGDESCRIPTION as MEProgram,  T_10.LONGDESCRIPTION MEReason,
				 CASE T_2.PrintRenewals WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  as MEPrintNotices,
		 
				 T_12.import_id as MEGivenBy, 
				 T_2.OverrideRenewalDefaults MEOverrideRenewalDef, T_2.WaiveBenefits as MEWaiveBenefit,
				 Memberships.TotalChildren as MENumChild, Memberships.TotalMembers as MENumMembers,
				 CASE Memberships.Standing WHEN 0 THEN 'New' WHEN 1 THEN 'Active' WHEN 3 THEN 'Dropped' WHEN 2 THEN 'Lapsed' ELSE '' END  as MEStanding,
				 T_2.SpecialMessage as MEGiftSpecialMsg,
				 T_2.Sequence, case Memberships.PrimaryMember when '0' then 'FALSE' when '-1' then 'TRUE' end as MEPrimaryMember,
				 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() as [RE_DB_OwnerShort]
		 
				 --CASE T_2.SendNoticeTo WHEN 0 THEN 'Donor' WHEN 1 THEN 'Primary Member' WHEN 2 THEN 'Both' ELSE '' END  as MESendNoticesTo,
				 --CASE T_11.SentTo WHEN 0 THEN 'Donor' WHEN 1 THEN 'Primary Member' WHEN 2 THEN 'Both' ELSE '' END  as MERenewalSentTo,
				 --T_2.BenefitsNotes as MEBenNotes,  T_2.MemComment as MECom, Memberships.Notes as MENotes
				FROM     
					DBO.MEMBERSHIPS AS Memberships 
					INNER JOIN DBO.RECORDS AS Memberships_RECORDS ON Memberships.RECID = Memberships_RECORDS.ID 
					INNER JOIN DBO.MEMBERSHIPTRANSACTION AS T_1 ON Memberships.CURRENTTRANSACTIONID = T_1.ID 
					INNER JOIN DBO.MEMBERSHIPTRANSACTION AS T_2 ON Memberships.ID = T_2.MEMBERSHIPID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_3 ON T_2.PROGRAM = T_3.TABLEENTRIESID 
					LEFT OUTER JOIN DBO.USERS AS T_4 ON T_2.LAST_CHANGED_BY = T_4.USER_ID 
					LEFT OUTER JOIN DBO.TRANSACTIONGIFT AS T_5 ON T_2.ID = T_5.TRANSACTIONID 
					LEFT OUTER JOIN DBO.MEMBERSHIPSUBCATEGORIES AS T_6 ON T_2.SUBCATEGORY = T_6.MEMBERSHIPSUBCATEGORIESID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_7 ON T_6.SUBCATEGORYID = T_7.TABLEENTRIESID
					LEFT OUTER JOIN DBO.MEMBERSHIPCATEGORY AS T_8 ON T_2.CATEGORY = T_8.MEMBERSHIPCATEGORYID 
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_9 ON T_8.CATEGORYID = T_9.TABLEENTRIESID
					LEFT OUTER JOIN DBO.TABLEENTRIES AS T_10 ON T_2.REASON = T_10.TABLEENTRIESID
					LEFT OUTER JOIN DBO.MEMBERSHIPRENEWALS AS T_11 ON T_2.ID = T_11.MEMBERSHIPID 
					LEFT OUTER JOIN DBO.RECORDS AS T_12 ON T_2.GIVENBY = T_12.ID
         
				WHERE ((Memberships.PrimaryMember <> 0))  
		 
				GO  
		
		SET QUOTED_IDENTIFIER OFF 
		GO
		SET ANSI_NULLS ON 
		GO	 

		--HC_Membership_Benefits_v

		SET QUOTED_IDENTIFIER ON 
		GO
		SET ANSI_NULLS ON 
		GO

 
		CREATE VIEW dbo.HC_Membership_Benefits_v
		AS	
				 SELECT 
				 Memberships.MemID MembershipID,
				 T_1.Import_ID MEImpID,
				 T_2.Import_ID MEBenImpID,
				 T_3.LONGDESCRIPTION AS MEBenBen,
				 T_2.Count MEBenCount,
				 CASE T_1.SendBenefitsTo WHEN 0 THEN 'Primary Member' WHEN 1 THEN 'Donor' ELSE '' END  MEBenSendBenTo,
				 T_2.Sent As MEBenSentFullFilled,
				 T_2.TotalBenefitValue MEBenTotVal ,
				 T_2.UnitCost MEBenUnitCost ,
				 CASE T_1.WaiveBenefits WHEN 0 THEN 'FALSE' WHEN -1 THEN 'TRUE' ELSE '' END  MEBenWaivBen,
				 T_2.Comments MEBenCom,
				 T_1.BenefitsNotes MEBenNote,
				 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() as [RE_DB_OwnerShort]

				FROM 
				DBO.MEMBERSHIPS AS Memberships 
				INNER JOIN DBO.MEMBERSHIPTRANSACTION AS T_1 ON Memberships.CURRENTTRANSACTIONID = T_1.ID 
				LEFT OUTER JOIN DBO.MEMBERSHIPBENEFIT AS T_2 ON T_1.ID = T_2.PARENTID 
				LEFT OUTER JOIN DBO.TABLEENTRIES AS T_3 ON T_2.BENEFITID = T_3.TABLEENTRIESID

				WHERE 
				((Memberships.PrimaryMember <> 0))  
		
		GO
		SET QUOTED_IDENTIFIER OFF 
		GO
		SET ANSI_NULLS ON 
		GO	 
	
		--HC_Membership_Linked_Gift_v
		SET QUOTED_IDENTIFIER ON 
		GO
		SET ANSI_NULLS ON 
		GO

 
		CREATE VIEW dbo.HC_Membership_Linked_Gift_v
		AS	
				SELECT 
				 Memberships.MemID MembershipID, T_1.Import_ID MEHisImportID, T_3.IMPORT_ID GFImpID, T_3.Amount MELinkGiftAmt,
				 dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() as [RE_DB_OwnerShort] 
		 
				FROM 
				DBO.MEMBERSHIPS AS Memberships 
				INNER JOIN DBO.MEMBERSHIPTRANSACTION AS T_1 ON Memberships.ID = T_1.MEMBERSHIPID 
				LEFT OUTER JOIN DBO.TRANSACTIONGIFT AS T_2 ON T_1.ID = T_2.TRANSACTIONID 
				LEFT OUTER JOIN DBO.GIFT AS T_3 ON T_2.GIFTID = T_3.ID

				WHERE 
				Memberships.PrimaryMember <> 0 and T_3.IMPORT_ID is not null

		GO
		SET QUOTED_IDENTIFIER OFF 
		GO
		SET ANSI_NULLS ON 
		GO		
		
		--HC_Membeship_Solicitor_v

		SET QUOTED_IDENTIFIER ON 
		GO
		SET ANSI_NULLS ON 
		GO

 
		CREATE VIEW dbo.HC_Membeship_Solicitor_v
		AS	
				SELECT 
				Memberships.MemID AS MembershipID,
				T_1.Import_ID MEImpID,  
				T_2.IMPORT_ID MESolImpID,
				T_3.IMPORT_ID ImportID,
				DBO.BuildFullName3(T_3.LAST_NAME,T_3.FIRST_NAME,T_3.MIDDLE_NAME,T_3.ORG_NAME,T_3.KEY_INDICATOR) as Name,
				dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() as [RE_DB_OwnerShort]

				FROM 
				DBO.MEMBERSHIPS AS Memberships 
				INNER JOIN DBO.MEMBERSHIPTRANSACTION AS T_1 ON Memberships.ID = T_1.MEMBERSHIPID 
				LEFT OUTER JOIN DBO.MEMBERSHIPSOLICITOR AS T_2 ON T_1.ID = T_2.PARENTID 
				LEFT OUTER JOIN DBO.RECORDS AS T_3 ON T_2.SOLICITORID = T_3.ID

				WHERE 
				Memberships.PrimaryMember <> 0 and  T_3.IMPORT_ID  is not null

		GO
		SET QUOTED_IDENTIFIER OFF 
		GO
		SET ANSI_NULLS ON 
		GO		
	 
	

--TRIBUTE_ACKNOWLEDGEE
		SET QUOTED_IDENTIFIER ON 
		GO
		SET ANSI_NULLS ON 
		GO

  
		CREATE  VIEW dbo.HC_Tribute_Acknowledgee_v
		AS
		 --select * from [dbo].[QUERY_TRIBUTE_ACKNOWLEDGEE]--> query used as basis for HC view. Does not include acknowledgees with relat/recip = "self"/

			select
					T_1.CONSTITUENT_ID ConsId
				  , T_1.IMPORT_ID as ImportID

				  , Tribute.Import_ID TATRImpID
				  , Tribute_Acknowledgee.Import_ID as TAImpID
 				  , case when T_2.ID is not null then 'I' 
					   when T_20.ID is not null then 'O' end AS [TARKeyInd]
		
				  , case when T_2.ID is not null then T_2.IMPORT_ID 
					   when T_20.ID is not null then T_20.IMPORT_ID end AS [TARImpID]
				  , case when T_2.ID is not null then DBO.Query_ConstitName(1,T_13.LAST_NAME,T_13.FIRST_NAME,T_13.MIDDLE_NAME,T_13.ORG_NAME,T_13.KEY_INDICATOR,T_13.CONSTITUENT_ID) 
					   when T_20.ID is not null then  T_22.ORG_NAME  end As TAName
				  , case when T_2.ID is not null then  T_19.[LONGDESCRIPTION]
					   when T_20.ID is not null then  T_24.LONGDESCRIPTION end AS TARRelat
				  , case when T_2.ID is not null then  T_18.[LONGDESCRIPTION]
					   when T_20.ID is not null then  T_23.LONGDESCRIPTION end AS TARRecip
				  , T_L.LONGDESCRIPTION AS TALetter
				  , dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'TA' [RE_DB_Tbl] 
			from
				dbo .Tribute
				inner join dbo.Tribute_Acknowledgee on Tribute. ID = Tribute_Acknowledgee .Tribute_ID
				left join dbo.tableentries as T_L on Tribute_Acknowledgee.letter = T_L.tableentriesid
				--IND_RELAT
					inner join dbo.Constit_Relationships on Tribute_Acknowledgee. Relationships_ID = Constit_Relationships.ID
					left join dbo.RECORDS AS T_1 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_1.ID 
					left join dbo.QUERY_IND_RELATIONSHIPS AS T_2 ON CONSTIT_RELATIONSHIPS.ID = T_2.ID  
					left join dbo.RECORDS AS T_13 ON T_2.RELATION_ID = T_13.ID
					left join dbo.TABLEENTRIES AS T_18 ON Constit_Relationships.RECIP_RELATION_CODE = T_18.TABLEENTRIESID  
					left join dbo.TABLEENTRIES AS T_19 ON Constit_Relationships.RELATION_CODE = T_19.TABLEENTRIESID		 
				--ORG_RELAT
					LEFT JOIN dbo.QUERY_ORG_RELATIONSHIPS AS T_20 ON CONSTIT_RELATIONSHIPS.ID = T_20.ID 
					left JOIN dbo.RECORDS AS T_21 ON CONSTIT_RELATIONSHIPS.CONSTIT_ID = T_21.ID 
					LEFT JOIN dbo.RECORDS AS T_22 ON T_20.RELATION_ID = T_22.ID 
					LEFT JOIN dbo.TABLEENTRIES AS T_23 ON T_20.RECIP_RELATION_CODE = T_23.TABLEENTRIESID  
					LEFT JOIN dbo.TABLEENTRIES AS T_24 ON T_20.RELATION_CODE = T_24.TABLEENTRIESID

		 GO
		SET QUOTED_IDENTIFIER OFF 
		GO
		SET ANSI_NULLS OFF 
		GO

/******************************************************************************************************************
NOTE:	Phone Numbers appened to the following views have been removed to 
		avoid issues with special characters on Phone Types when running 
		Fashmac (temp phone table with alias T_10 has been disabled):
				HC_Constituents_v 
				HC_Cons_Address_NonPref_v 
				HC_Ind_Relat_v
				HC_Org_Relat_v 
******************************************************************************************************************/
 
/* Heller Consulting, Inc. 
Purpose:  To extract data from RE using SQL Server Views.  These Views emulate RE Import Files

Revision History: 
12/04/2015: added Appeal Number Solicited to Appeal table
12/04/2015: added Cons Actions fields: AddedBy, LastChangedBy, DateAdded, DateLastChanged.
07/20/2012: created HC_Ind_Relat_Attr_v and HC_Org_Relat_Attr_v 
05/29/2012: Constituents and Gifts: included AddedBy, LastChangedBy, AddedDate, LastChangedDate, SystemRecordID.
08/05/2011: Constituent Action ACContact field updated to pull Relationship Import ID instead of Contact Name.
08/05/2011: Updated GFLink to pull Recurring Gift Payments Import ID. 
01/29/2011: Added PReferred Address Phone Numbers to HC_Cons_Main 
01/29/2011: Renamed HC_Gift_Main_v to HC_Gift_Main
01/29/2011: Renamed HC_Gift_PledgePay_v to HC_Gift_PledgePay
01/29/2011: Created HC_Gift_v
01/29/2011: Renamed HC_Ind_Relat_HH_PrimBiz_v to HC_Ind_Relat_HH_PrimBiz
01/29/2011: Renamed HC_Ind_Relat_NoHH_NoPrimBiz_NoConsSp_v to HC_Ind_Relat_NoHH_NoPrimBiz_NoConsSp_v
01/29/2011: Created HC_Ind_Relat_v
01/29/2011: Created Sotred Procedure for HC_Ind_Relat_Phone_t to collect al Ind Rel Phone numbers. 
01/29/2011: Renamed HC_Org_Relat_v to HC_Org_Relat
01/29/2011: Renamed HC_Org_Relat_Phone_v to HC_Org_Relat_Phone
01/29/2011: Created Sotred Procedure for HC_Org_Relat_Phone_t to collect al Org Rel Phone numbers. 
01/29/2011: Created HC_Org_Relat_v 
08/24/2010: Added 'StartDate', 'EndDate', and 'Inactive' fields on views corresponding to Campaing, Fund, Appeal and Package. 
02/08/2010: Continued with adding statement to check and remove views if they already exist in the database.  
02/08/2010: Renamed views. 
02/05/2010: Added new view to extract all Constituent Bio info with corresponding Preferred Address. 
02/05/2010: Added new view to extract Alternate Address only. 
02/05/2010: Added statement to check and remove views if they already exist in the database.
02/19/2009: Completed row_number ID. 
02/18/2009: pending -> use "CAST(row_number() over (order by GIFT_IMPORT.ID) as char) as ID" to convert number to char for gifts and ind relationships views. 
02/17/2009: Completed adding and checking 'auto' ID up to "dbo.HC_Export_Gift_Solicitor"
02/16/2009: Completed adding and checking 'auto' ID up to "dbo.HC_Export_Action"  
03/04/2008: Revised Org Relationship View to conditionally populate ORLink field.  Added REPLACE logic to Org Notes to eliminate embedded carriage returns.
03/04/2008: Added new Views to install script.
02/27/2008: Apdated with shorted column heading names. Updated View names to coincide with expected text file names when View is run.  
*/