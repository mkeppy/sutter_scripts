			 DROP TABLE SUTTER_1P_MIGRATION.tbl.zTEST_Contact_HofH
			
			--B_tbl_Contact_Cons_HofH:  Constituents that are Head of Household Constituents only;
 				SELECT T.RE_DB_OwnerShort, T.KeyInd, T.ImportID,  T.ConsID, T.Deceased,  -- hh
				T3.ImportID AS NonHHImpID, T3.Deceased NonHHDeceased,		--non-hh
				T2.IRLink, T2.IRIsHH, T2.IRIsSpouse, T2.IRImpID,  
				CAST('' AS nvarchar(50)) AS HH_ImportID, 
				CAST('' AS nvarchar(50)) AS HH_ConsID, 
				CAST('' AS nvarchar(50)) AS NoHH_ImportID, 
				CAST('' AS nvarchar(50)) AS NoHH_ConsID,
				'tbl_B' AS scr
				,ROW_NUMBER() OVER ( PARTITION BY T.ImportID, T3.ImportID ORDER BY T.ImportID, T3.ImportId) AS Seq 
				INTO SUTTER_1P_MIGRATION.tbl.zTEST_Contact_HofH
				FROM SUTTER_1P_DATA.TBL.SF_CONSTITUENTS T
				INNER JOIN SUTTER_1P_DATA.TBL.SF_IND_RELAT T2 ON T.ImportID = T2.IRLink  
				INNER JOIN SUTTER_1P_DATA.TBL.SF_CONSTITUENTS T3 ON T2.ImportID=T3.ImportID  
				WHERE (((T.KeyInd)='I') AND ((T2.IRIsHH)='TRUE') AND ((T2.IRIsSpouse)='TRUE')) 
				ORDER BY T.ImportID
				GO
				--17,627

	
				--HH update to set the correct HH based on Deceased constituents. 	
					UPDATE SUTTER_1P_MIGRATION.tbl.zTEST_Contact_HofH
					SET HH_ImportID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN NonHHImpID ELSE ImportID END WHERE importid!=NonHHImpID 
					GO
					--Non-HH update
					UPDATE SUTTER_1P_MIGRATION.tbl.zTEST_Contact_HofH
					SET NoHH_ImportID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN ImportID ELSE NonHHImpID END WHERE importid!=NonHHImpID 
					GO
			 

	 			 
			--C_tbl_Contact_Cons_NonConsSpouse; Constituents with non-cons Ind Relat Spouse
				INSERT INTO SUTTER_1P_MIGRATION.tbl.zTEST_Contact_HofH (RE_DB_OwnerShort, KeyInd, ImportID, ConsID, Deceased, 
																  NonHHImpID, NonHHDeceased, 
																  IRLink, IRIsHH, IRIsSpouse, IRImpID,   
														          HH_ImportID, HH_ConsID, NoHH_ImportID, NoHH_ConsID, scr, Seq)
				SELECT T.RE_DB_OwnerShort, T.KeyInd,  T.ImportID, T.ConsID, T.Deceased, 
				T2.ImportID AS IRNonHHImpID,  T2.IRDeceased AS NonHHDeceased, 
				'' AS IRLink, T2.IRIsHH, T2.IRIsSpouse, T2.IRImpID,  
				NULL AS HH_ImportID, null AS HH_ConsID, null AS NoHH_ImportID, null AS NoHH_ConsID,
				'tbl_C' AS scr,
				ROW_NUMBER() OVER ( PARTITION BY T.ImportID, T2.ImportID ORDER BY T.ImportID, T2.ImportId) AS Seq 
				FROM SUTTER_1P_DATA.TBL.SF_CONSTITUENTS T
				LEFT JOIN SUTTER_1P_DATA.TBL.SF_IND_RELAT T2 ON T.ImportID = T2.ImportID 
				LEFT JOIN SUTTER_1P_DATA.TBL.SF_CONSTITUENTS T3 ON T2.ImportID=T3.ImportID 
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.zTEST_Contact_HofH T4 ON T.IMPORTID =T4.IMPORTID  --- THIS IS TO REMOVE DUPLICATE CONSTITUENTS ORIGINATED FROM MDM MERGE. WHERE SOME MAY HAVE A SPOUSE RELATIONSHIP WITH AN IRLINK (SPOUSE =TRUE) AND 
																												 --AND SOME MAY HAVE A RELATINOSHIPS SPOUSE == TRUE WITH A NON-CONSTITUENT... LOOK EXAMPLE WITH IMPORTIID = ABSF-000223
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.zTEST_Contact_HofH T5 ON T.IMPORTID =T5.NoHH_ImportID
				GROUP BY T.KeyInd, T2.IRIsHH, T.ImportID,  T.RE_DB_OwnerShort, T.ConsID,  T.Deceased, T2.ImportID, T2.IRDeceased, T2.IRLink, T2.IRIsSpouse, T2.IRImpID, T2.RE_DB_OwnerShort, T4.IMPORTID, T5.NoHH_ImportID
				HAVING (((T.KeyInd)='I') AND ((T2.IRLink) Is Null Or (T2.IRLink)='') AND ((T2.IRIsSpouse)='TRUE'))  AND T4.IMPORTID IS NULL AND T5.NoHH_ImportID IS NULL 
				ORDER BY T.ImportID
				GO
			    --193,384


			--D_tbl_Contact_Cons_WNoSpouse: all other non-cons Ind Relat; Ind Constituents with no HofH constituents/relationships;  
			    -- Ind Constituents with no non-HofH constituents/relationships; 
				INSERT INTO SUTTER_1P_MIGRATION.tbl.zTEST_Contact_HofH (RE_DB_OwnerShort, KeyInd, ImportID, ConsID, Deceased, 
	
																  NonHHImpID, NonHHDeceased, 
																  IRLink, IRIsHH, IRIsSpouse, IRImpID, 
																  HH_ImportID, HH_ConsID, NoHH_ImportID, NoHH_ConsID, scr, Seq)
				SELECT T.RE_DB_OwnerShort, T.KeyInd, T.ImportID, T.ConsID, T.Deceased,
				T3.NonHHImpID, '' AS NonHHDeceased, '' AS IRLink, 'TRUE' AS IRIsHH, 
				'' AS IRIsSpouse, '' AS IRImpID,  
				NULL AS HH_ImportID, null AS HH_ConsID, null AS NoHH_ImportID, null AS NoHH_ConsID,
				'tbl_D' AS scr,
				ROW_NUMBER() OVER ( PARTITION BY T.ImportID, T3.NonHHImpID ORDER BY T.ImportID, T3.NonHHImpID) AS Seq 
				FROM SUTTER_1P_DATA.TBL.SF_CONSTITUENTS T
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.zTEST_Contact_HofH AS T2 ON T.ImportID = T2.ImportID  
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.zTEST_Contact_HofH AS T3 ON T.ImportID = T3.NonHHImpID  
				GROUP BY T.KeyInd, T.ImportID, T.RE_DB_OwnerShort, T.ConsID, T.Deceased, T3.NonHHImpID, T2.ImportID, T3.RE_DB_OwnerShort
				HAVING (((T.KeyInd)='I') AND ((T3.NonHHImpID) Is Null) AND ((T2.ImportID) Is Null)) 
				GO
				--652,672
 

 				--HH update to set the correct HH based on Deceased constituents. 	
					UPDATE SUTTER_1P_MIGRATION.tbl.zTEST_Contact_HofH
					SET HH_ImportID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN NonHHImpID ELSE ImportID END WHERE importid!=NonHHImpID 
					GO
					--Non-HH update
					UPDATE SUTTER_1P_MIGRATION.tbl.zTEST_Contact_HofH
					SET NoHH_ImportID= CASE WHEN Deceased='True' AND NonHHDeceased='False' THEN ImportID ELSE NonHHImpID END WHERE importid!=NonHHImpID 
					GO
					 --UPDATE missing blank HH 
					UPDATE SUTTER_1P_MIGRATION.tbl.Contact_HofH 
					SET HH_ImportID=ImportID WHERE HH_ImportID IS NULL OR HH_ImportID=''
					GO
	  



 SELECT * FROM SUTTER_1P_MIGRATION.tbl.Contact_HofH
 WHERE HH_ImportID='SCAH-087094' OR [NoHH_ImportID]='SCAH-087094'


 




