			SELECT DISTINCT
			 dbo.fnc_OwnerID() AS OwnerID
			
			,T3.[IRImpID]
			,dbo.fnc_RecordType('Contact_Organizational') AS RecordTypeID
 		 	,NULL AS RE_ConsID__c
			,T2.NEW_TGT_IMPORT_ID AS RE_ImportID__c
			,T2.NEW_TGT_IMPORT_ID AS External_ID__c
			,T2.NEW_TGT_IMPORT_ID AS [ACCOUNT:External_ID__c]
			,NULL AS RE_IRImpID__c			
		
			,NULL MDM_PTY_ID__c
 			,NULL AS BirthDate
			,NULL AS rC_Bios__Birth_Month__c
			,NULL AS rC_Bios__Birth_Day__c
			,NULL AS rC_Bios__Birth_Year__c
 			,NULL AS rC_Bios__Deceased__c
			,NULL AS rC_Bios__Deceased_Date__c
			,NULL AS rC_Bios__Deceased_Month__c
			,NULL AS rC_Bios__Deceased_Day__c
			,NULL AS rC_Bios__Deceased_Year__c
			 ,NULL AS rC_Bios__Ethnicity__c
			,'MAILING'	AS	FirstName
			,NULL AS rC_Bios__Gender__c
			,NULL AS Gives_Anonymously__c
			,NULL AS No_Valid_Address__c
			,NULL AS rC_Bios__Active__c
			,NULL AS Is_Solicitor__c
			,'CONTACT' AS	LastName
			,NULL AS rC_Bios__Maiden_Name__c
			,NULL AS rC_Bios__Marital_Status__c
			,NULL AS rC_Bios__Middle_Name__c
			,NULL AS Nick_Name__c
			,NULL AS HasOptedOutOfEmail
			,NULL AS Suffix_1__c
			,NULL AS Suffix_2__c
			,NULL AS Salutation
			,NULL AS Title_2__c
			,T.DateAdded	AS	CreatedDate
			,'TRUE' AS rC_Bios__Preferred_Contact__c
			,'FALSE' AS rC_Bios__Secondary_Contact__c
			
			,GETDATE() AS	rC_Giving__Rollup_Hard_Credits__c
			,GETDATE() AS	rC_Giving__Rollup_Soft_Credits__c
		
			,NULL AS Do_Not_Phone__c
			,NULL AS Do_Not_Solicit__c
			,NULL AS Do_Not_Mail__c
			,NULL AS Do_Not_Contact__c
					
			,NULL AS rC_Bios__Preferred_Email__c
			,NULL AS Fax
			,NULL AS HomePhone
			,NULL AS MobilePhone
			,NULL AS Pager__c  
			,NULL AS  rC_Bios__Home_Email__c
			,NULL AS rC_Bios__Other_Email__c
			,NULL AS rC_Bios__Other_Phone__c
			,NULL AS rC_Bios__Work_Email__c
			,NULL AS rC_Bios__Work_Phone__c
			,NULL AS Website__c

			,NULL AS rC_Bios__Home_Do_Not_Call__c
			,NULL AS rC_Bios__Mobile_Do_Not_Call__c
			,NULL AS rC_Bios__Work_Do_Not_Call__c
			,NULL AS rC_Bios__Other_Do_Not_Call__c 

			,NULL AS Additional_Phone_Numbers__c
			,NULL AS Agefinder_Birthdate__c
			,NULL AS CMS_Age__c
			,NULL AS PhoneFinder__c
			,CASE WHEN T9.AddrLine1 IS NULL THEN NULL WHEN T9.AddrLine2!='' THEN T9.AddrLine1 +' '+T9.AddrLine2 ELSE T9.AddrLine1 END AS MailingStreet
			,T9.AddrCity AS MailingCity
			,T9.AddrState AS MailingState
			,T9.AddrZIP AS MailingPostalCode
			,T9.AddrCountry MailingCountry

			,NULL AS Merged_from__c
			,NULL AS Merged_from_ConsID__c

		 	,'DonorProxy' AS zrefSource
 	  		INTO [SUTTER_1P_MIGRATION].IMP.CONTACT_DONOR_PROXY
			FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T	
			INNER JOIN SUTTER_1P_MIGRATION.tbl.Account_Org T2 ON T.RE_DB_ID=T2.NEW_TGT_IMPORT_ID 
			LEFT JOIN [SUTTER_1P_DATA].DBO.[HC_Ind_Relat_v] T3 ON T2.NEW_TGT_IMPORT_ID=T3.[NEW_TGT_IMPORT_ID_CN] 
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Address_Pref	T9 ON T.RE_DB_ID=T9.RE_DB_ID 

			WHERE T3.[NEW_TGT_IMPORT_ID_CN] IS NULL
			--20717
			 
			--CONTACT DONOR PROXY (org cons w/no ind relat)
			SELECT DISTINCT
			X1.ID AS rC_Bios__Contact__c
  			,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T4.rC_Bios__Type__c
			,T4.rC_Bios__Active__c
			,CASE WHEN T4.rC_Bios__Active__c='False' THEN 'False' 
		          WHEN (T1.AddrPref='True' AND T0.RE_DB_ID!=T0.NEW_TGT_IMPORT_ID) THEN 'False' ELSE T1.AddrPref END AS rC_Bios__Preferred_Mailing__c
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c

			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			,NULL AS rC_Bios__Do_Not_Mail__c

			--refernce
			,'Contact_Proxy' AS zrefSource
			,T0.ImportID AS zrefOldImportID
			,T0.NEW_TGT_IMPORT_ID AS zrefNewImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq
 
 		--	INTO SUTTER_1P_MIGRATION.IMP.CONTACT_DONOR_PROXY_ADDRESS
			FROM SUTTER_1P_DATA.dbo.HC_Constituents_v T0  
			INNER JOIN SUTTER_1P_MIGRATION.tbl.Account_Org T2 ON T0.RE_DB_ID=T2.NEW_TGT_IMPORT_ID 
			LEFT JOIN [SUTTER_1P_DATA].DBO.[HC_Ind_Relat_v] T3 ON T2.NEW_TGT_IMPORT_ID=T3.[NEW_TGT_IMPORT_ID_CN] 
			INNER JOIN SUTTER_1P_DATA.DBO.HC_Cons_Address_v T1 ON T0.RE_DB_ID=T1.RE_DB_ID

			LEFT JOIN SUTTER_1P_MIGRATION.XTR.CONTACT X1 ON T2.[NEW_TGT_IMPORT_ID]=X1.External_Id__c
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_AddressType T4 ON T1.AddrType=T4.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T4.[Convert]='Yes' AND (T1.[AddrLine1] IS NOT NULL AND T1.[AddrLine1]!='' AND T1.[AddrLine1] NOT LIKE '%deceased%' AND T1.[AddrLine1]!='dec')
			AND T3.[NEW_TGT_IMPORT_ID_CN] IS NULL
	 
 	  	 
		  --CONSTITUENT DONOR PROXY from    ORG ACCOUNTS. 
			SELECT DISTINCT  
			X1.ID AS rC_Bios__Contact__c
			--(T.ImportID) AS [rC_Bios__Account__r:External_ID__c]
			,(T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.AddrImpID) AS rC_Bios__External_ID__c
			,T2.rC_Bios__Type__c 
			,T2.rC_Bios__Active__c
			,CASE WHEN T2.rC_Bios__Active__c='False' THEN 'False' 
			      WHEN (T1.AddrPref='True' AND T0.RE_DB_ID!=T0.NEW_TGT_IMPORT_ID) THEN 'False' ELSE T1.AddrPref END AS rC_Bios__Preferred_Mailing__c
			,T1.[AddrLine1] AS rC_Bios__Original_Street_Line_1__c
		    ,T1.[AddrLine2] AS rC_Bios__Original_Street_Line_2__c
			,T1.[AddrCity] AS rC_Bios__Original_City__c
			,T1.[AddrState] AS rC_Bios__Original_State__c
			,T1.[AddrZIP] AS rC_Bios__Original_Postal_Code__c
			,T1.[AddrCountry] AS rC_Bios__Original_Country__c

			,CONVERT(NVARCHAR(10),T1.[AddrValidFrom], 101) AS rC_Bios__Start_Date__c
			,CONVERT(NVARCHAR(10),T1.[AddrValidTo], 101) AS rC_Bios__End_Date__c
			,T2.RE_DB_OwnerShort AS Affiliation__c
			, NULL AS rC_Bios__Do_Not_Mail__c
			
			--refernce
			,'Account_Org' AS zrefSource
			,T0.RE_DB_ID AS zrefOldImportID
			,T.ImportID AS zrefNewImportID
			,T1.AddrImpID AS zrefAddrImpID
			,T1.RE_DB_OwnerShort AS zrefDBOwner
			,T1.AddrSequence AS zrefAddrSeq
			
		 	FROM SUTTER_1P_MIGRATION.TBL.Account_Org T
			INNER JOIN SUTTER_1P_DATA.dbo.HC_Constituents_v T0 ON T.ImportID=T0.NEW_TGT_IMPORT_ID
			LEFT JOIN [SUTTER_1P_DATA].dbo.[HC_Ind_Relat_v] TT ON T0.[NEW_TGT_IMPORT_ID]=TT.[NEW_TGT_IMPORT_ID_CN]
			INNER JOIN SUTTER_1P_DATA.DBO.HC_Cons_Address_v T1 ON T0.RE_DB_ID=T1.RE_DB_ID 
			LEFT JOIN SUTTER_1P_MIGRATION.XTR.CONTACT X1 ON T.ImportID=X1.External_Id__c
			LEFT JOIN SUTTER_1P_DATA.DBO.CHART_AddressType T2 ON T1.AddrType=T2.AddrType AND T1.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			WHERE T2.[Convert]='Yes' 
			AND (T1.[AddrLine1] IS NOT NULL AND T1.[AddrLine1]!='' AND T1.[AddrLine1] NOT LIKE '%deceased%' AND T1.[AddrLine1]!='dec')
		    AND TT.[NEW_TGT_IMPORT_ID_CN] IS NULL  