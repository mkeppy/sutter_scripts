

	SELECT T.ImportID, T.IRImpID, T.IRLink, T1.KeyInd, T.IRRelat, T.IRRecip, T3.*
	FROM SUTTER_DATA.dbo.HC_Ind_Relat_v T
			 
			INNER JOIN SUTTER_DATA.dbo.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
			LEFT JOIN SUTTER_DATA.dbo.HC_Constituents_v T2 ON T.IRLink=T2.ImportID AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
			LEFT JOIN SUTTER_MGEA.dbo.CHART_RelatRecipType T3 ON T.IRRelat=T3.RELATIONSHIP AND T.IRRecip=T3.RECIPROCAL 
																	AND T.RE_DB_OwnerShort=T3.RE_DB_OwnerShort 
																	AND T1.KeyInd=T3.KeyInd
			
	WHERE T.RE_DB_OwnerShort='safh' 
	AND [RelationshipIndicator]='Individual'
	--AND t3.relationship IS null
	
	--35,569

	--SELECT * FROM SUTTER_DATA.dbo.HC_Ind_Relat_v WHERE RE_DB_OwnerShort='safh' and IRRelat='null'
	--16,333

SELECT role_1, role_2, [dbo].[fnc_ProperCase](role_1) r1, [dbo].[fnc_ProperCase](role_2) r2 
FROM SUTTER_MGEA.[dbo].[CHART_RelatRecipType] WHERE relationship ='null'

