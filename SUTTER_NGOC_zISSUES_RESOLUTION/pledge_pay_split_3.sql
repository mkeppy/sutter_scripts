USE [RE7_SH_SMCF]
GO

/****** Object:  UserDefinedFunction [dbo].[VIEW_PaymentSplitAllocation_FN]    Script Date: 12/19/2015 2:36:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  FUNCTION 
[dbo].[VIEW_PaymentSplitAllocation_FN]()
RETURNS TABLE
AS
RETURN
(

SELECT
ip.PledgeID,
ip.PaymentID,
paymentsplit.CampaignID,
paymentsplit.FundID,
paymentsplit.AppealID,
paymentsplit.PackageID,
paymentsplit.Amount paymentsplitamount,
paymentsplit.giftsplitid,
ip.amount,
dbo.PaymentSplitAllocation(
	ip.PledgeID,
	ip.amount, 
	paymentsplit.giftsplitid, 
	ip.PaymentID,
	ISNULL(paymentsplit.CampaignID, 0), 
	ISNULL(paymentsplit.FundID, 0),
	ISNULL(paymentsplit.AppealID, 0), 
	ISNULL(paymentsplit.PackageID, 0), 
	Paymentsplit.Amount) PaymentAllocation
FROM
	dbo.GiftSplit paymentsplit
INNER JOIN
(
	SELECT pledgeid, paymentid, SUM(Amount) Amount
	FROM dbo.installmentpayment
	GROUP BY pledgeid, paymentid
) ip ON paymentsplit.giftid = ip.paymentid

)
GO


