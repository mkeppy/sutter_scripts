

SELECT [ConsID], [ImportID], [GFImpID], [GFDate], [GFTAmt], [GFInsStartDate], [GFInsEndDate] 
FROM [RE7_SH_ABSF].dbo.[HC_Gift_v]
WHERE [GFType] ='pledge' AND [GFImpID]='02869-545-0000420124'
ORDER BY GFDate desc
SELECT * FROM [RE7_SH_ABSF].dbo.[HC_Gift_Installments_v]
WHERE [GFLink]='02869-545-0000420124' ORDER BY GFInsDate


 SELECT * FROM [SUTTER_1P_MIGRATION].imp.[OPPORTUNITY_PARENT]
 WHERE [RE_GFImpID__c]='ABSF-GT-02869-545-0000420124' 


SELECT [RE_GFImpID__c], [CloseDate], [rC_Giving__First_Payment_Date__c], [rC_Giving__Payment_End_Date__c] 
FROM [SUTTER_MGEA_MIGRATION].[IMP].[OPPORTUNITY_PARENT]
WHERE [rC_Giving__First_Payment_Date__c] IS NOT NULL  AND [CloseDate]!=[rC_Giving__First_Payment_Date__c]
ORDER BY closedate DESC

--HC_Gift_SplitGift_Link_v

		SELECT	--gift
				CASE GIFT.TYPE WHEN 1 THEN 'Cash' WHEN 2 THEN 'Pay-Cash' WHEN 3 THEN 'MG Pay-Cash' WHEN 8 THEN 'Pledge' WHEN 9 THEN 'Stock/Property' WHEN 10 THEN
				'Stock/Property (Sold)' WHEN 11 THEN 'Pay-Stock/Property' WHEN 12 THEN 'MG Pay-Stock/Property' WHEN 13 THEN 'Pay-Stock/Property (Sold)' WHEN 14 THEN 'MG Pay-Stock/Property (Sold)'
				WHEN 15 THEN 'Gift-in-Kind' WHEN 16 THEN 'Pay-Gift-in-Kind' WHEN 17 THEN 'MG Pay-Gift-in-Kind' WHEN 18 THEN 'Other' WHEN 19 THEN 'Pay-Other' WHEN 20 THEN
				'MG Pay-Other' WHEN 21 THEN 'Write Off' WHEN 22 THEN 'MG Write Off' WHEN 27 THEN 'MG Pledge' WHEN 30 THEN 'Recurring Gift' WHEN 31 THEN 'Recurring Gift Pay-Cash'
				WHEN 32 THEN 'GL Reversal' WHEN 33 THEN 'Amendment' WHEN 34 THEN 'Planned Gift' ELSE NULL END AS GFType,
				dbo.GIFT.IMPORT_ID AS GFImpID, 
				dbo.GiftSplit.Import_Id AS [GSplitImpID],
				dbo.GiftSplit.Amount [GSplitAmt],
				dbo.GIFT.Amount AS [GFTAmt],
				--payment
				CASE GIFT_1.TYPE WHEN 1 THEN 'Cash' WHEN 2 THEN 'Pay-Cash' WHEN 3 THEN 'MG Pay-Cash' WHEN 8 THEN 'Pledge' WHEN 9 THEN 'Stock/Property' WHEN 10 THEN
				'Stock/Property (Sold)' WHEN 11 THEN 'Pay-Stock/Property' WHEN 12 THEN 'MG Pay-Stock/Property' WHEN 13 THEN 'Pay-Stock/Property (Sold)' WHEN 14 THEN 'MG Pay-Stock/Property (Sold)'
				WHEN 15 THEN 'Gift-in-Kind' WHEN 16 THEN 'Pay-Gift-in-Kind' WHEN 17 THEN 'MG Pay-Gift-in-Kind' WHEN 18 THEN 'Other' WHEN 19 THEN 'Pay-Other' WHEN 20 THEN
				'MG Pay-Other' WHEN 21 THEN 'Write Off' WHEN 22 THEN 'MG Write Off' WHEN 27 THEN 'MG Pledge' WHEN 30 THEN 'Recurring Gift' WHEN 31 THEN 'Recurring Gift Pay-Cash'
				WHEN 32 THEN 'GL Reversal' WHEN 33 THEN 'Amendment' WHEN 34 THEN 'Planned Gift' ELSE NULL END AS GFPaymentType, 
				GIFT_1.IMPORT_ID AS GFPaymentGFImpID, 
				GiftSplit_1.Import_Id AS [GFPaymentGSplitImpID],
				GiftSplit_1.Amount AS [GFPaymentGSplitAmt],
				GIFT_1.Amount AS [PaytmentGFTAmt],
				--ref
				dbo.hc_OrgName_v() [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort]
		FROM	dbo.PledgePayments 
				--gift
				INNER JOIN dbo.GIFT ON dbo.PledgePayments.pledgeid = dbo.GIFT.ID 
				INNER JOIN dbo.GiftSplit ON dbo.GIFT.ID = dbo.GiftSplit.GiftId
				--payment
				INNER JOIN dbo.GIFT AS GIFT_1 ON dbo.PledgePayments.paymentid = GIFT_1.ID
				INNER JOIN dbo.GiftSplit AS GiftSplit_1 ON GIFT_1.ID = GiftSplit_1.GiftId
		WHERE dbo.GIFT.IMPORT_ID ='03947-057-0223336'
		 
 
  
 
 SELECT     dbo.GiftSplit.Import_Id AS [GSplitImpID], dbo.GIFT.IMPORT_ID AS [GFImpID], 
					  dbo.GiftSplit.Sequence AS [GFSplitSequence], 
                      dbo.GiftSplit.Amount AS [GSplitAmt], dbo.FUND.FUND_ID AS [GSplitFund], 
                      dbo.APPEAL.APPEAL_ID AS [GSplitAppeal], dbo.CAMPAIGN.CAMPAIGN_ID AS [GSplitCamp], 
                      dbo.Package.Package_ID AS [GSplitPkg], 
				      dbo.hc_OrgName_v() AS [RE_DB_Owner], dbo.HC_OrgInit_f() [RE_DB_OwnerShort], 'GT' [RE_DB_Tbl],
                      row_number() over (order by GIFT.IMPORT_ID, dbo.GiftSplit.Sequence) as ID		
FROM         dbo.GIFT INNER JOIN
                      dbo.GiftSplit ON dbo.GIFT.ID = dbo.GiftSplit.GiftId LEFT OUTER JOIN
                      dbo.Package ON dbo.GiftSplit.PackageId = dbo.Package.ID LEFT OUTER JOIN
                      dbo.FUND ON dbo.FUND.ID = dbo.GiftSplit.FundId LEFT OUTER JOIN
                      dbo.APPEAL ON dbo.APPEAL.ID = dbo.GiftSplit.AppealId LEFT OUTER JOIN
                      dbo.CAMPAIGN ON dbo.CAMPAIGN.ID = dbo.GiftSplit.CampaignId
--WHERE     (dbo.GIFT.TYPE NOT IN (28, 32, 33)) OR (dbo.GIFT.TYPE IS NULL)
WHERE dbo.GIFT.IMPORT_ID='03947-057-0223336' 



--
--ABSF
SELECT T2.[ConsID], T2.[GFType], T1.*
FROM dbo.[HC_Gift_SplitGift_v] T1
LEFT JOIN dbo.hc_gift_v T2 ON T1.[GFImpID]=T2.[GFImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
WHERE T1.[GFImpID] IN (SELECT [GFImpID] FROM dbo.[HC_Gift_SplitGift_v] GROUP BY [GFImpID] HAVING COUNT(*)>1)
ORDER BY [GFImpID], [GFSplitSequence]
--PLEDGE TEST gfimpid='02869-545-0000331277' OR GFIMPID='02869-545-0000374234'
--PAYCASH TEST = 

SELECT * FROM dbo.[HC_Gift_Link_v]
ORDER BY [GFImpID]
	

--PLEDGE/PAY W/SPLITS. 
	SELECT * FROM dbo.[HC_Gift_Link_v]
	WHERE [GFImpID]='02869-545-0000331277' OR GFIMPID='02869-545-0000374234'
	OR [GFImpID]='00001-545-0000051698'
	ORDER BY [GFImpID]



	--SAMPLE 1 PLEDGE
	SELECT T2.[ConsID], T2.[GFType], T1.*
	FROM dbo.[HC_Gift_SplitGift_v] T1
	LEFT JOIN dbo.hc_gift_v T2 ON T1.[GFImpID]=T2.[GFImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
	WHERE T1.[GFImpID]='02869-545-0000331277' 
	ORDER BY [GFImpID], [GFSplitSequence]

	--SAMPLE 1 PAYMENT 
	SELECT T2.[ConsID], T2.[GFType], T1.*
	FROM dbo.[HC_Gift_SplitGift_v] T1
	LEFT JOIN dbo.hc_gift_v T2 ON T1.[GFImpID]=T2.[GFImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
	WHERE 
	T1.[GFImpID]='02869-545-0000378794' OR 
	T1.[GFImpID]='02869-545-0000331625' OR 
	T1.[GFImpID]='02869-545-0000344705' OR 
	T1.[GFImpID]='02869-545-0000356883' OR 
	T1.[GFImpID]='02869-545-0000370532'  
	ORDER BY [GFImpID], [GFSplitSequence]

	
	--SAMPLE 2 PLEDGE
	SELECT T2.[ConsID], T2.[GFType], T1.*
	FROM dbo.[HC_Gift_SplitGift_v] T1
	LEFT JOIN dbo.hc_gift_v T2 ON T1.[GFImpID]=T2.[GFImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
	WHERE T1.[GFImpID]='02869-545-0000374234' 
	ORDER BY [GFImpID], [GFSplitSequence]

	
	
	--SAMPLE 2 PAYMENT --THE PLEDGE HAS 25 OR 26 PAYMENTS. ONLY 5 USED IN EXAMPLE
	SELECT T2.[ConsID], T2.[GFType], T1.*
	FROM dbo.[HC_Gift_SplitGift_v] T1
	LEFT JOIN dbo.hc_gift_v T2 ON T1.[GFImpID]=T2.[GFImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
	WHERE 
	T1.[GFImpID]='02869-545-0000383926' OR 
	T1.[GFImpID]='02869-545-0000384592' OR 
	T1.[GFImpID]='02869-545-0000381427' OR 
	T1.[GFImpID]='02869-545-0000381786' OR 
	T1.[GFImpID]='02869-545-0000382217'  
	ORDER BY [GFImpID], [GFSplitSequence]

	
	--SAMPLE 3 PLEDGE  [RE7_SH_SDHF]
		SELECT T2.[ConsID], T2.[GFType], T1.*
		FROM [RE7_SH_SDHF].dbo.[HC_Gift_SplitGift_v] T1
		LEFT JOIN [RE7_SH_SDHF].dbo.hc_gift_v T2 ON T1.[GFImpID]=T2.[GFImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
		WHERE T1.[GFImpID]='00001-545-0000051698' 
		ORDER BY [GFImpID], [GFSplitSequence]

	--SAMPLE 3 PAYMENT [RE7_SH_SDHF]
		SELECT T2.[ConsID], T2.[GFType], T1.*
		FROM [RE7_SH_SDHF].dbo.[HC_Gift_SplitGift_v] T1
		LEFT JOIN [RE7_SH_SDHF].dbo.hc_gift_v T2 ON T1.[GFImpID]=T2.[GFImpID] AND T1.[RE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
		WHERE 
		T1.[GFImpID]='00001-545-0000052127' OR 
		T1.[GFImpID]='00001-545-0000052545' OR 
		T1.[GFImpID]='00001-545-0000053354' OR 
		T1.[GFImpID]='00001-545-0000054321' OR 
		T1.[GFImpID]='00001-545-0000057348'  
		ORDER BY [GFImpID], [GFSplitSequence]    
	 