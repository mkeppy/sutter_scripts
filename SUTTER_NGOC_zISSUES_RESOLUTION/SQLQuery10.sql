USE [RE7_SH_MPHF]
GO

/****** Object:  UserDefinedFunction [dbo].[FormatAmount]    Script Date: 11/19/2015 15:15:42 ******/
SET ANSI_NULLS ON
GO

--SQL datatypes https://msdn.microsoft.com/en-us/library/ms187745.aspx



SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[HC_FormatAmount_f](@dAmount BIGINT, @iCountry INTEGER)
RETURNS VARCHAR(2000)
BEGIN
  DECLARE @sSymbol VARCHAR(2000);
  DECLARE @sAmount VARCHAR(2000);
  DECLARE @i SMALLINT;
  DECLARE @sReturn VARCHAR(2000);
  DECLARE @iContinue INTEGER;
  SET @i=1;
  IF @iCountry = 0
    BEGIN
         SET @sSymbol='$'
    END
  ELSE
    BEGIN
    	SELECT  @sSymbol = Symbol FROM dbo.Country_Codes WHERE ID = @iCountry
    END;
  
  SET @sAmount=CAST(@dAmount AS DECIMAL(17,2));
  WHILE @iContinue = 0 
	BEGIN
	    SET @i=@i+1;
	    SET @sReturn=RTRIM(LTRIM(SUBSTRING(@sAmount,@i,1)));
	    IF @sReturn = '0'
		BEGIN
		      SET @iContinue=0
	    	END
	   ELSE
		BEGIN
		     IF @sReturn = ''
			BEGIN
			        SET @iContinue=0
		      	END
		     ELSE
			BEGIN
			        SET @iContinue=1
		      	END
	    	END
	  END;
  SET @sAmount=@sSymbol + SUBSTRING(@sAmount,@i,20);
  IF @sAmount = @sSymbol
	BEGIN
	    SET @sAmount=''
  	END;
  RETURN @sAmount
END
GO
