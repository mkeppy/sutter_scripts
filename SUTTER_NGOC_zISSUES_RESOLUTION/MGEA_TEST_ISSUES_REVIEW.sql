--ISSUES resolution

--0002

	SELECT * FROM SUTTER_DATA.dbo.HC_Cons_Phone_v WHERE ImportID='000003137' AND RE_DB_OwnerShort='SMCF'
	SELECT * FROM SUTTER_DATA.dbo.HC_Cons_SolicitCodes_v WHERE ImportID='000003137' AND RE_DB_OwnerShort='SMCF'

--0010
	
	SELECT * FROM SUTTER_DATA.dbo.HC_Ind_Relat_v WHERE (ImportID='000030924' OR ImportID='000003137') AND(IRLink='000003137' OR IRLink='000030924')
	 
--0014
	SELECT ImportID, PrimAddText, PrimSalText FROM SUTTER_DATA.dbo.HC_Constituents_v WHERE ImportID='000003137' 
	SELECT * FROM SUTTER_DATA.dbo.HC_Cons_Addl_Addressee_v WHERE ImportID='000003137' 

--0015
	SELECT DISTINCT ACSolImpID FROM SUTTER_DATA.dbo.HC_Cons_Action_Solicitor_v   ---996 unique solicitors

	SELECT * FROM SUTTER_DATA.dbo.HC_Cons_Action_Solicitor_v   
	WHERE ACImpID IN (SELECT ACImpID FROM SUTTER_DATA.dbo.HC_Cons_Action_Solicitor_v GROUP BY ACImpID HAVING COUNT(*)>1)
	AND RE_DB_OwnerShort='smcf' 
	ORDER BY ACImpID, ACSolImpID

--0016
	
	SELECT * FROM SUTTER_DATA.dbo.HC_Gift_Tribute_v

	SELECT * FROM SUTTER_DATA.dbo.HC_Gift_Tribute_v
	INNER JOIN SUTTER_DATA.dbo.HC_Cons_Tribute_v ON SUTTER_DATA.dbo.HC_Gift_Tribute_v.TribImpID = SUTTER_DATA.dbo.HC_Cons_Tribute_v.TRImpID
	WHERE GFTLink IN (SELECT GFTLink FROM SUTTER_DATA.dbo.HC_Gift_Tribute_v GROUP BY GFTLink HAVING COUNT (*)>1) AND SUTTER_DATA.dbo.HC_Gift_Tribute_v.RE_DB_OwnerShort='SMCF'
	ORDER BY GFTLink, TribImpID

--0022

	
	SELECT * FROM SUTTER_MGEA.imp.CONTACT WHERE rC_Bios__Birth_Year__c IS NOT NULL AND rC_Bios__Birth_Month__c IS NULL   SAFH-04121-593-0000033919

	SELECT * FROM SUTTER_DATA.dbo.HC_Constituents_v where ImportID='03947-593-0000120819'

--0030

	SELECT * FROM SUTTER_DATA.dbo.HC_Cons_Address_v WHERE ImportID='000036703'
	SELECT * FROM SUTTER_DATA.dbo.HC_Constituents_v WHERE ImportID='000036703'

--0044
	
	--pledge
	SELECT * FROM SUTTER_MGEA.IMP.OPPORTUNITY_PARENT WHERE RE_GFImpID__c LIKE '%04121-545-0000119162'
	--payment
	SELECT * FROM SUTTER_MGEA.IMP.OPPORTUNITY_TRANSACTION WHERE [rC_Giving__Parent__r:External_ID__c]='SAFH-GT-04121-553-0000122934'

	SELECT * FROM SUTTER_DATA.dbo.HC_Gift_Link_v WHERE GFImpID='04121-545-0000119162'


	SELECT     TOP 10 dbo.HC_Gift_Link_v.GFSysRecId, dbo.HC_Gift_Link_v.GFType, dbo.HC_Gift_Link_v.GFImpID, dbo.HC_Gift_Link_v.GFPaymentSysRecId, 
                      dbo.HC_Gift_Link_v.GFPaymentType, dbo.HC_Gift_Link_v.GFPaymentGFImpID, dbo.HC_Gift_Link_v.GFPaymentAmount, dbo.HC_Gift_Link_v.RE_DB_Owner, 
                      dbo.HC_Gift_Link_v.RE_DB_OwnerShort, SplitGift_Payment.GSplitImpID AS Pledge_GSplitImpID, SplitGift_Payment.GFSplitSequence AS Pledge_GFSplitSequence, 
                      SplitGift_Payment.GSplitAmt AS Pledge_GSplitAmt, SplitGift_Pledge.GSplitImpID AS Payment_GSplitImpID, 
                      SplitGift_Pledge.GFSplitSequence AS Payment_GFSplitSequence, SplitGift_Pledge.GSplitAmt AS Payment_GSplitAmt
	FROM dbo.HC_Gift_Link_v 
	INNER JOIN  dbo.HC_Gift_SplitGift_v AS SplitGift_Pledge ON dbo.HC_Gift_Link_v.GFImpID=SplitGift_Pledge.GFImpID  AND  dbo.HC_Gift_Link_v.RE_DB_OwnerShort=SplitGift_Pledge.RE_DB_OwnerShort
	INNER JOIN  dbo.HC_Gift_SplitGift_v AS SplitGift_Payment ON dbo.HC_Gift_Link_v.GFPaymentGFImpID=SplitGift_Payment.GFImpID AND dbo.HC_Gift_Link_v.RE_DB_OwnerShort=SplitGift_Payment.RE_DB_OwnerShort 
	AND SplitGift_Pledge.GFSplitSequence=SplitGift_Payment.GFSplitSequence
	WHERE dbo.HC_Gift_Link_v.GFImpID LIKE '%04121-545-0000119162%'


