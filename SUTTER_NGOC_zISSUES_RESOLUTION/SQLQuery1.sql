



		SELECT * FROM SUTTER_MGEA.tbl.ztestCredit
		
		SELECT [ID]
			  ,[rC_Giving__Opportunity__c:External_ID__c]
			  ,[rC_Giving__Account__r:External_ID__c]
			  ,[rC_Giving__Contact__r:External_ID__c]
			  ,SUM([rC_Giving__Amount__c]) GIVAMT
			  ,SUM([Solicitor_Amount__c]) SOLAMT
			  ,[Affiliation__c]
			  ,[rC_Giving__Type__c]
			  ,[rC_Giving__Contact_Role__c]
			  ,[rC_Giving__Is_Fixed__c]
			FROM [SUTTER_MGEA].[TBL].[ztestCredit]
			GROUP BY 
			[ID]
			  ,[rC_Giving__Opportunity__c:External_ID__c]
			  ,[rC_Giving__Account__r:External_ID__c]
			  ,[rC_Giving__Contact__r:External_ID__c]
			  ,[Affiliation__c]
			  ,[rC_Giving__Type__c]
			  ,[rC_Giving__Contact_Role__c]
			  ,[rC_Giving__Is_Fixed__c]

