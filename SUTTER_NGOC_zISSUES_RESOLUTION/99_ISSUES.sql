

--add sal


	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Constituents_v] WHERE [LastName]='ho' AND [FirstName] ='margaret'
		--Margaret Ho in the MPHF dB 

		WBRX-36307
		MPHF-174
		ABSF-02869-079-0000241966

		SELECT * 
		FROM [SUTTER_1P_DATA].dbo.[HC_Cons_Addl_Addressee_v]  AS T1 
		LEFT JOIN [SUTTER_1P_DATA].[dbo].[CHART_AddSalType]	  AS T2 ON T1.[RE_DB_OwnerShort] = t2.[RE_DB_OwnerShort] AND [T2].[AddSalType] = [T1].[AddSalType]
	 	WHERE t1.[RE_DB_OwnerShort] ='MPHF' AND t1.[ImportID]='174'

								 					--ADDITIONAL ADD/SAL
				SELECT DISTINCT
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Salutation_Salutation') AS RecordTypeID
				,T.UniqueID AS External_ID__c
				,CASE WHEN T2.NoHH_ImportID IS NOT NULL THEN T2.HH_ImportID ELSE T.NEW_TGT_IMPORT_ID  END AS [ACCOUNT:External_Id__c]
				,T.NEW_TGT_IMPORT_ID AS [rC_Bios__Contact__r:External_ID__c]
				,CAST(T.rc_Bios_Salutation_Type__c AS NVARCHAR(255)) AS Name
				,CAST(T.rc_Bios_Salutation_Type__c AS NVARCHAR(255)) AS rC_Bios__Salutation_Type__c
				,CAST(T.rC_Bios__Salutation_Line_1__c AS NVARCHAR(255)) AS rC_Bios__Salutation_Line_1__c
				,CAST(T.rC_Bios__Inside_Salutation__c AS NVARCHAR(255)) AS rC_Bios__Inside_Salutation__c
			    ,CAST (T.Additional_Recognition_Subtype__c AS VARCHAR(255)) AS Additional_Recognition_Subtype__c
				,'FALSE' AS rC_Bios__Preferred_Salutation__c
				,T.[RE_DB_OwnerShort] AS Affiliation__c
	 			--reference
					,'AddAddSal' AS zrefSource	
					,T.RE_DB_ID AS zrefOldImportId
					,T.NEW_TGT_IMPORT_ID AS zrefNewImportID
					,NULL AS zrefHHImportID
					,NULL AS zrefNoHHImportID
					,NULL AS zrefTable
					,NULL AS zrefIRImpID		
				FROM SUTTER_1P_MIGRATION.tbl.AddtlAddSal_final  T
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH AS T2 ON T.[NEW_TGT_IMPORT_ID]=T2.NoHH_ImportID    
				WHERE T.[NEW_TGT_IMPORT_ID]='MPHF-174'

					 SELECT * FROM SUTTER_1P_MIGRATION.tbl.AddtlAddSal_final


	
--973

SELECT * FROM SUTTER_DATA_T2.dbo.[HC_Constituents_v] WHERE [NEW_TGT_IMPORT_ID]='MPHF-11150'

 

 SELECT * FROM SUTTER_DATA_T2.dbo.[HC_Ind_Relat_v] WHERE [NEW_TGT_IMPORT_ID_CN]='MPHF-11150'

 SELECT * FROM [SUTTER_1P_MIGRATION].imp.contact WHERE [External_ID__c] LIKE '%00001-518-0000034412%'

--salutation  recognition
	 SELECT * FROM [SUTTER_1P_MIGRATION].imp.[SALUTATION_RECOGNITION]	

 SELECT [rC_Bios__Contact__r:External_ID__c], COUNT(*) C 
 FROM [SUTTER_1P_MIGRATION].imp.[SALUTATION_RECOGNITION]
 GROUP BY [rC_Bios__Contact__r:External_ID__c] 
 ORDER BY c DESC
 
 
BEGIN
  			USE [SUTTER_1P_MIGRATION] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'SALUTATION_RECOGNITION'
 		 
		--SELECT NAME FROM SUTTER_1P_MIGRATION.IMP.SALUTATION WHERE NAME LIKE '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.SALUTATION_RECOGNITION SET NAME=REPLACE(NAME,'"','''') where NAME like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.SALUTATION_RECOGNITION SET rC_Bios__Salutation_Line_1__c=REPLACE(rC_Bios__Salutation_Line_1__c,'"','''') where rC_Bios__Salutation_Line_1__c like '%"%'
		 
END		
	
	 

-- check gifts -- emp giving for donor SRMC-02594-593-0000094013

SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Constituents_v] WHERE [NEW_TGT_IMPORT_ID]='SRMC-02594-593-0000094013'
SELECT * FROM [SUTTER_1P_DATA].dbo.hc_gift_v WHERE [ImportID]='02594-593-0000094013'

SELECT * FROM [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PARENT] WHERE [ACCOUNT:External_Id__c]='SRMC-02594-593-0000094013'
SELECT * FROM [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_TRANSACTION] WHERE [ACCOUNT:External_Id__c]='SRMC-02594-593-0000094013'



SELECT * FROM [SUTTER_1P_DELTA].[IMP].[OPPORTUNITY_PARENT] WHERE [ACCOUNT:External_Id__c]='SRMC-02594-593-0000094013'
SELECT * FROM [SUTTER_1P_DELTA].[IMP].[OPPORTUNITY_TRANSACTION] WHERE [ACCOUNT:External_Id__c]='SRMC-02594-593-0000094013'
   
--proposal date

/*If RE Date Funded Not Blank then RE Date Funded else
  if RE Date Asked Not Blank then RE Date Funded else
  If RE Date Expected Not Blank then RE Date Expected else
"Whatever you deem appropriate"
*/									 
			SELECT   DISTINCT  X1.ID
 					 
					,CASE WHEN T1.[PRDateFund]	IS NOT NULL THEN T1.[PRDateFund]	 
							   WHEN T1.[PRDateAsk] IS NOT NULL THEN T1.[PRDateAsk] 
							   WHEN T1.[PRDateExp] IS NOT NULL THEN T1.[PRDateExp]
							   ELSE '01/01/1900' END AS CloseDate
					,T1.[PRDateFund] zrefPRDateFund
					,T1.[PRDateAsk] AS zrefPRDateAsk
					,T1.[PRDateExp] AS zrefPRDateExp
					,t2.[External_Id__c] AS zrefExternal_Id__c
							
					INTO SUTTER_1P_MIGRATION.IMP.UPDATE_OPPORTUNITY_PROPOSAL_closedate
					FROM SUTTER_1P_DATA.dbo.HC_Proposal_v T1
				 	INNER JOIN [SUTTER_1P_MIGRATION].imp.[OPPORTUNITY_PROPOSAL] AS T2 ON (T1.RE_DB_OwnerShort+'-'+T1.RE_DB_Tbl+'-'+T1.PRImpID) = T2.[External_Id__c] 	-- WHERE T1.PRName NOT LIKE '%planned%'  [SUTTER_1P_MIGRATION].IMP.GIVING_SOLICITOR	
 					INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY] AS X1 ON T2.[RE_PRImpID__c]=X1.[RE_PRImpID__c]
				 	WHERE x1.[RE_PRIMPID__C] IS NOT NULL AND x1.[RE_PRIMPID__C]!=''
																							 
		 			SELECT * FROM SUTTER_1P_MIGRATION.IMP.UPDATE_OPPORTUNITY_PROPOSAL_closedate	
					ORDER BY zrefPRDateFund, zrefPRDateAsk, zrefPRDateExp 
			 






--preferences legacy data

		SELECT DISTINCT 
				 rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.CAttrImpID)
 	    		,T.[CAttrCat] AS RE_CAttrCat__c
				,T.[CAttrDesc] AS RE_CAttrDesc__c
				,NULL AS RE_ConsCode__c
				,NULL AS RE_Solcode__c
				--reference
				,T1.KeyInd AS zrefKeyInd
				,T.CAttrImpID AS zrefRecImportID
				,'ConsAttribute_1' AS zrefRecSource
				INTO SUTTER_1P_MIGRATION.IMP.UPDATE_PREFERENCE_legacy_values
				FROM SUTTER_1P_DATA.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T3 ON T1.NEW_TGT_IMPORT_ID=T3.NoHH_ImportID  
				INNER JOIN SUTTER_1P_DATA.dbo.CHART_Attributes T2 ON T.CAttrCat=T2.category AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.SF_Object_1='RC_BIOS_PREFERENCE__C' AND T.CAttrDesc IS NOT NULL AND T2.[Convert]='Yes'
			UNION ALL 
		--CONSTITUENT ATTRIBUTE from CHART_CONS_ATTRIBUTES 
				SELECT DISTINCT 
				rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.CAttrImpID)
  	    		,T.[CAttrCat] AS RE_CAttrCat__c
				,T.[CAttrDesc] AS RE_CAttrDesc__c
				,NULL AS RE_ConsCode__c
				,NULL AS RE_Solcode__c
    			--reference
				,T1.KeyInd AS zrefKeyInd
				,T.CAttrImpID AS zrefRecImportID
				,'ConsAttribute_2' AS zrefRecSource
				FROM SUTTER_1P_DATA.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T3 ON T1.NEW_TGT_IMPORT_ID=T3.NoHH_ImportID  
				INNER JOIN SUTTER_1P_DATA.dbo.CHART_ConsAttributes T2 ON T.CAttrCat=T2.CAttrCat AND T.CAttrDesc=T2.CAttrDesc AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.PREFERENCE_RecordType IS NOT NULL AND T2.PREFERENCE_RecordType !='' AND T2.[Convert]='Yes'
			UNION ALL 
		--CONSTITUENT CODE 
				SELECT DISTINCT 
				 rC_Bios__External_ID__c= CASE WHEN T.ConsCodeImpID IS NULL THEN T.RE_DB_Tbl+'-'+T1.NEW_TGT_IMPORT_ID ELSE (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.ConsCodeImpID) END 
 	    		,NULL AS RE_CAttrCat__c
				,NULL AS RE_CAttrDesc__c
				,T.[ConsCodeDesc] AS RE_ConsCode__c
				,NULL AS RE_Solcode__c
 				--reference
				,T1.KeyInd AS zrefKeyInd
				,T.ConsCodeImpID AS zrefRecImportID
				,'ConsCode' AS zrefRecSource
				FROM SUTTER_1P_DATA.dbo.HC_Cons_ConsCode_v T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_1P_DATA.dbo.CHART_ConstituencyCodes T2 ON T.ConsCode=T2.ConsCode AND T1.KeyInd=T2.KeyInD AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.[Convert]='Yes'
 			UNION ALL 	 
 		--CONSTITUENT SOLICIT CODE 
				SELECT DISTINCT 
				 rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+CAST(T.ID AS NVARCHAR(20)))
 	    		,NULL AS RE_CAttrCat__c
				,NULL  AS RE_CAttrDesc__c
				,NULL AS RE_ConsCode__c
				,T.[SolicitCode] AS RE_Solcode__c
				--reference
				,T1.KeyInd AS zrefKeyInd
				,CAST(T.ID AS NVARCHAR(20))AS zrefRecImportID
				,'ConsSolCode' AS zrefRecSource
				FROM SUTTER_1P_DATA.dbo.HC_Cons_SolicitCodes_v T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				INNER JOIN SUTTER_1P_DATA.dbo.CHART_ConsSolicitCodes T2 ON T.SolicitCode=T2.SolicitCode AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.[Convert]='Yes' AND T2.SF_Object_1 ='RC_BIOS_PREFERENCE__C'
			
	
		BEGIN 
			USE [SUTTER_1P_MIGRATION] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'UPDATE_PREFERENCE_legacy_values'
 		END 
		
		UPDATE SUTTER_1P_MIGRATION.IMP.UPDATE_PREFERENCE_legacy_values SET RE_CAttrCat__c=REPLACE(RE_CAttrCat__c,'"','''') where RE_CAttrCat__c like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.UPDATE_PREFERENCE_legacy_values SET RE_CAttrDesc__c=REPLACE(RE_CAttrDesc__c,'"','''') where RE_CAttrDesc__c like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.UPDATE_PREFERENCE_legacy_values SET RE_ConsCode__c=REPLACE(RE_ConsCode__c,'"','''') where RE_ConsCode__c like '%"%'
		UPDATE SUTTER_1P_MIGRATION.IMP.UPDATE_PREFERENCE_legacy_values SET RE_Solcode__c=REPLACE(RE_Solcode__c,'"','''') where RE_Solcode__c like '%"%'
	 




	--CONSTITUENT ATTRIBUTE from CHART_CONS_ATTRIBUTES 

		DATA 	
		--Amb. Treatment Request	CPMC - Friend
		--Amb. Treatment Request	CPMC - Staff
		--Amb. Treatment Request	CPMC - Sutter Health Staff
		--Amb. Treatment Request	CPMCF - Board Trustee
		Amb. Treatment Request	CPMCF - Staff
	
		CHART	
		--Amb. Treatment Request	CPMC Staff
		--Amb. Treatment Request	CPMCF Board Trustee
		Amb. Treatment Request	CPMCF Staff
		--Amb. Treatment Request	Friend
		--Amb. Treatment Request	Sutter Health Staff

	-- 
	SELECT * FROM SUTTER_1P_DATA.dbo.CHART_ConsAttributes  WHERE [CAttrCat] ='Amb. Treatment Request' AND [CAttrDesc]!='NULL'
		UPDATE SUTTER_1P_DATA.dbo.CHART_ConsAttributes
		SET [CAttrDesc]='CPMCF - Staff'	, [PREFERENCE_Type]='CPMCF - Staff'
		WHERE [CAttrCat] ='Amb. Treatment Request' AND [CAttrDesc]='CPMCF Staff'

				SELECT DISTINCT 
				dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType(T2.PREFERENCE_RecordType) AS RecordTypeID
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.CAttrImpID)
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN (T1.KeyInd='O') THEN T1.NEW_TGT_IMPORT_ID 
														   WHEN (T1.KeyInd='I' AND T2.PREFERENCE_Category='Ratings' AND T3.NoHH_ImportID IS NOT NULL) THEN T3.HH_ImportID 
														   ELSE NULL END	

				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T1.NEW_TGT_IMPORT_ID END 
				,rC_Bios__Category__c=T2.PREFERENCE_Category
				,rC_Bios__Subcategory__c=T2.PREFERENCE_Subcategory
				,rC_Bios__Type__c=T2.PREFERENCE_Type
				,rC_Bios__Subtype__c=T2.PREFERENCE_Subtype
				,rC_Bios__Value__c= T2.PREFERENCE_Value  
				,rC_Bios__Comments__c=T.CAttrCom
				,rC_Bios__Start_Date__c=T.CAttrDate
				,rC_Bios__End_Date__c=NULL
				,rC_Bios__Priority__c=NULL
				,rC_Bios__Affiliation__c=T.[RE_DB_OwnerShort]
				,T2.PREFERENCE_Active AS rC_Bios__Active__c  
				,NULL AS rC_Bios__Status__c
				,T2.PREFERENCE_Capacity_Rating__c AS Capacity_Rating__c
				,T2.PREFERENCE_Net_Worth__c AS Net_Worth__c
			 
				--reference
				,T1.KeyInd AS zrefKeyInd
				,T.CAttrImpID AS zrefRecImportID
				,'ConsAttribute_2' AS zrefRecSource
				INTO [SUTTER_1P_MIGRATION].[IMP].PREFERENCES_ambassador
	
				FROM SUTTER_1P_DATA.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T3 ON T1.NEW_TGT_IMPORT_ID=T3.NoHH_ImportID  
				INNER JOIN SUTTER_1P_DATA.dbo.CHART_ConsAttributes T2 ON T.CAttrCat=T2.CAttrCat AND T.CAttrDesc=T2.CAttrDesc AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.PREFERENCE_RecordType IS NOT NULL AND T2.PREFERENCE_RecordType !='' AND T2.[Convert]='Yes'
				AND t2.[CAttrCat] ='Amb. Treatment Request' AND t2.[CAttrDesc]!='NULL'
	
	   		
			
				SELECT DISTINCT 
				 rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.CAttrImpID)
				,T2.[CAttrCat] AS RE_CAttrCat__c
				,T2.[CAttrDesc] AS RE_CAttrDesc__c
			 
				--reference
				,T1.KeyInd AS zrefKeyInd
				,T.CAttrImpID AS zrefRecImportID
			
			--	INTO [SUTTER_1P_MIGRATION].[IMP].PREFERENCES_ambassador
	
				FROM SUTTER_1P_DATA.dbo.HC_Cons_Attributes_v T
				INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				LEFT JOIN SUTTER_1P_MIGRATION.tbl.Contact_HofH T3 ON T1.NEW_TGT_IMPORT_ID=T3.NoHH_ImportID  
				INNER JOIN SUTTER_1P_DATA.dbo.CHART_ConsAttributes T2 ON T.CAttrCat=T2.CAttrCat AND T.CAttrDesc=T2.CAttrDesc AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.PREFERENCE_RecordType IS NOT NULL AND T2.PREFERENCE_RecordType !='' AND T2.[Convert]='Yes'
				
	
	--account solicitor missing contact
		SELECT * FROM [SUTTER_1P_MIGRATION].[IMP].[ACCOUNT] WHERE [External_ID__c]='CVRX-8006'
		SELECT * FROM [SUTTER_1P_MIGRATION].[IMP].[CONTACT] WHERE [External_ID__c] LIKE '%CVRX-8006' OR [ACCOUNT:External_ID__c]='CVRX-8006'

	--pledge amt wrong

		SELECT * FROM [SUTTER_1P_MIGRATION].imp.[OPPORTUNITY_PARENT] WHERE [External_Id__c]='SAHF-GT-04164-553-0000024665'
		SELECT * FROM [SUTTER_1P_MIGRATION].imp.[OPPORTUNITY_TRANSACTION] WHERE [rC_Giving__Parent__r:External_ID__c]='SAHF-GT-04164-553-0000024665'


		SAHF-GI-04164-556-0000016823
		SAHF-GI-04164-556-0000016824
	
		SELECT * FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_new] WHERE [EXTERNAL_ID__C] ='SAHF-GI-04164-556-0000016823'
		OR [EXTERNAL_ID__C]='SAHF-GI-04164-556-0000016824' OR RC_GIVING__PARENT__C='0066100000Bysj3AAB'

		SELECT * FROM [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_TRANSACTION] WHERE [EXTERNAL_ID__C] ='SAHF-GI-04164-556-0000016823'
		OR [EXTERNAL_ID__C]='SAHF-GI-04164-556-0000016824' OR RC_GIVING__PARENT__C='0066100000Bysj3AAB'



	--Opportunity name for CVRX and WBRX
		DROP TABLE [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_bak]  
     	
		SELECT * 
		INTO [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY_bak]
		FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY]  
		
		ALTER TABLE  [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY] 
		ADD oldNAME VARCHAR(255)
				
		UPDATE [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY]
		SET oldNAME=NAME
	
		--check affiliations on all gifts except proposals. 
		SELECT * FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY] 
		WHERE (RC_GIVING__AFFILIATION__C ='WBRX' OR RC_GIVING__AFFILIATION__C='CVRX') AND [RE_PRIMPID__C] =''

		--test new name
		SELECT [ID], [NAME], RC_GIVING__AFFILIATION__C, oldNAME, REPLACE(NAME,LEFT(NAME,4),RC_GIVING__AFFILIATION__C) AS newNAME
		FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY] 
		WHERE (NAME LIKE '%WBRX%' OR NAME LIKE '%CVRX%') AND [RE_PRIMPID__C] =''

		--update new name
		UPDATE [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY]  SET NAME=REPLACE(NAME,LEFT(NAME,4),RC_GIVING__AFFILIATION__C) 
		WHERE (NAME LIKE '%WBRX%' OR NAME LIKE '%CVRX%') AND [RE_PRIMPID__C] =''

		--create imp file
		SELECT [ID], [NAME], RC_GIVING__AFFILIATION__C AS zref
		INTO [SUTTER_1P_MIGRATION].[IMP].UPDATE_OPPORTUNITY_NAME_affiliation
		FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY] 
		WHERE (oldNAME LIKE '%WBRX%' OR oldNAME LIKE '%CVRX%') AND [RE_PRIMPID__C] =''



	--I-247 Account name

		DROP TABLE [SUTTER_1P_MIGRATION].IMP.[CONTACT_UPDATE_lname]

		SELECT ID, [LASTNAME] +' ' AS NEW_LASTNAME, [LASTNAME] AS OLD_LASTNAME  
		INTO [SUTTER_1P_MIGRATION].IMP.[CONTACT_UPDATE_lname]
		FROM [SUTTER_1P_MIGRATION].XTR.[CONTACT]
		WHERE [LASTNAME] IS NOT NULL  AND [LASTNAME] NOT LIKE '%,%'

		USE [SUTTER_1P_MIGRATION] 
 		EXEC sp_FindStringInTable '%,%', 'IMP', 'CONTACT_UPDATE_lname'

		SELECT * FROM [SUTTER_1P_MIGRATION].IMP.[CONTACT_UPDATE_lname]
	     
	--I-248 Solicitor team member 
		  --missing solicitor 
	  		SELECT * FROM [SUTTER_1P_MIGRATION].[IMP].[SOLICITOR_TEAM_MEMBER] 
					WHERE [Solicitor_External_Contact__r:External_ID__c] IS NULL 	---external
					AND   [Solicitor__c] IS null									--internal uuser. 

			SELECT * FROM [SUTTER_1P_DATA].dbo.[CHART_ConsSolicitor]  WHERE name LIKE '%rena%'
		
			--look for missing users and users with invalid email address in the chart. 
			 SELECT * FROM [SUTTER_1P_MIGRATION].XTR.[USERS] WHERE [EMAIL] LIKE 'p%'
					  00561000001hr8LAAQ	 / pdst@sutterhealth.org

			SELECT T1.* 
			FROM [SUTTER_1P_DATA].dbo.[CHART_ConsSolicitor]  AS T1
			LEFT JOIN [SUTTER_1P_MIGRATION].XTR.[USERS] AS X1 ON T1.[SH_Email]=X1.[EMAIL]
			WHERE X1.[EMAIL] IS NULL AND T1.[SOLICITOR_TEAM_RECORD_TYPE]='Solicitor_Philanthropy'	AND T1.[SH_Email] IS NOT NULL
			ORDER BY T1.[SH_Email]


	  --I-249	TASKS missing correct Owner 

			SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Cons_Action_v] WHERE [ACImpID]='00001-504-0000007297' AND [RE_DB_OwnerShort]='SSCF'
			SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Cons_Action_Solicitor_v] WHERE [ACImpID]='00001-504-0000007297' AND [RE_DB_OwnerShort]='SSCF'

			SELECT T1.* 
			FROM [SUTTER_1P_DATA].dbo.[CHART_ActionSolicitor] AS T1
			LEFT JOIN [SUTTER_1P_MIGRATION].XTR.USERS AS X1 ON T1.[SF_User_Email] = X1.[EMAIL]
			WHERE X1.[EMAIL] IS NULL 

			   SELECT * FROM [SUTTER_1P_MIGRATION].XTR.USERS ORDER BY [EMAIL]



	 --I-251 GAU

		SELECT * FROM [SUTTER_1P_MIGRATION].tbl.[GL_Distribution_final]
		WHERE  [FundId] LIKE'%pri%'

		SELECT * FROM 	[dbo].[GL_Distribution_cpmc]
		WHERE fundid LIKE '%pride%'

		SELECT * FROM 	[dbo].GL_Distribution_no_cpmc
		WHERE fundid LIKE '%past%'

		SELECT * FROM [SUTTER_1P_MIGRATION].imp.[GAU]
		WHERE [rC_Giving__External	_ID__c] LIKE '%past%'

	--I-257 missing gau on opportunity. 


		SELECT X.ID , X2.ID AS rC_Giving__GAU__c  
		INTO [SUTTER_1P_MIGRATION].IMP.OPPORTUNITY_PARENT_UPDATE_GAU
		FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT	T
		INNER JOIN [SUTTER_1P_MIGRATION].XTR.[OPPORTUNITY] AS X ON T.[External_Id__c]=X.[EXTERNAL_ID__C]
		INNER JOIN [SUTTER_1P_MIGRATION].XTR.GAU AS X2 ON T.[rC_Giving__GAU__r:rC_Giving__External_ID__c]=X2.RC_GIVING__EXTERNAL_ID__C

		 --2,512,701
		
		SELECT COUNT(*) FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT	 --2,512,731
		
		SELECT [External_Id__c], [RE_Legacy_Financial_Code__c] FROM SUTTER_1P_MIGRATION.IMP.OPPORTUNITY_PARENT	
		WHERE [rC_Giving__GAU__r:rC_Giving__External_ID__c] IS null	
		--30    

--I-0269
	/*
	(1 AND 2 AND 3) OR (1 AND 2 AND 4) 
	1. Giving Record Type = Donation 
	2. RE Gift Type = Recurring Gift 
	3. Primary Campaign Source Contains emp, sts, mphf, ec.physician, brick, bchp, nchx pay ann, thfxannual, pamf cper ann 
	4. RE Gift Constituency Code contains Employee, physician 
	*/

	DROP TABLE [SUTTER_1P_MIGRATION].TBL.zRecurring_missing_attributes

			SELECT T1.[ACCOUNT:External_Id__c], T1.[GSplitRE_DB_OwnerShort], [T1].[GSplitImpID], [T1].[GSplitGFImpID], [T1].[GFType], [T1].[GFConsDesc], T1.[CAMPAIGN_External_Id__c] , 
					T2.[GFAttrCat], T2.[GFAttrDesc], 
			CASE WHEN T1.[GFConsDesc] LIKE '%employee%' OR T1.[GFConsDesc] LIKE '%physician%' THEN 'TRUE' END AS EMP_GIVING_CONS_CODE,
			CASE WHEN T1.[CAMPAIGN_External_Id__c] LIKE '%emp%' 
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%sts%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%mphf%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%ec.physician%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%brick%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%bchp%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%nchx pay ann%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%thfxannual%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%pamf cper ann%' 
				   THEN 'TRUE' END AS EMP_GIVING_CAMPAIGN
			INTO [SUTTER_1P_MIGRATION].TBL.zRecurring_missing_attributes
			FROM [SUTTER_1P_MIGRATION].TBL.[GIFT] AS T1
			LEFT JOIN [SUTTER_1P_MIGRATION].TBL.[GiftAttr_employee_recurring] AS T2 ON T1.[GSplitGFImpID]=T2.[GFImpID] AND T1.[GSplitRE_DB_OwnerShort]=T2.[RE_DB_OwnerShort]
			WHERE (T1.[GFType] ='Recurring Gift'	 AND T2.[GFImpID] IS NULL)
			AND ((T1.[GFConsDesc] LIKE '%employee%' OR T1.[GFConsDesc] LIKE '%physician%')  
			OR  (T1.[CAMPAIGN_External_Id__c] LIKE '%emp%' 
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%sts%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%mphf%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%ec.physician%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%brick%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%bchp%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%nchx pay ann%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%thfxannual%'
				   OR T1.[CAMPAIGN_External_Id__c] LIKE '%pamf cper ann%'))	  --1,880
	
	--Recuring gift pay-cash. 
		SELECT * FROM  [SUTTER_1P_MIGRATION].TBL.zRecurring_missing_attributes
		ORDER BY  [GSplitRE_DB_OwnerShort],  [GSplitImpID]

		   SELECT T1.* FROM [SUTTER_1P_DATA].dbo.[HC_Gift_Link_v] AS T1
		   INNER JOIN  [SUTTER_1P_MIGRATION].TBL.zRecurring_missing_attributes AS T2 ON T1.[GFImpID]=T2.[GSplitGFImpID] AND T1.[RE_DB_OwnerShort] = T2.[GSplitRE_DB_OwnerShort]
		   WHERE T1.[GFType] LIKE '%recur%'


	--ISSUES with Payment Count
	SELECT * FROM [SUTTER_1P_MIGRATION].xtr.[OPPORTUNITY] WHERE id ='0066100000ByfGvAAJ'
	SELECT * FROM [SUTTER_1P_MIGRATION].IMP.[OPPORTUNITY_PARENT] WHERE [External_Id__c]='MPHF-GT-00001-553-0000364849'
	
	SELECT X.ID, T.rC_Giving__Payment_Count__c, T.[External_Id__c] AS zrefExtId , T.[rC_Giving__Giving_Frequency__c] AS zrefNGOCFreq, T.[zrefGFInsFreq] 
	FROM [SUTTER_1P_MIGRATION].IMP.[OPPORTUNITY_PARENT] AS T
	INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY] AS X ON T.[External_Id__c]=X.[EXTERNAL_ID__C]
	WHERE rC_Giving__Payment_Count__c IS NOT null
	ORDER BY [zrefGFInsFreq]


		
	SELECT X.ID, T.rC_Giving__Payment_Count__c, T.[External_Id__c] AS zrefExtId , T.[rC_Giving__Giving_Frequency__c] AS zrefNGOCFreq, T.[zrefGFInsFreq] 
	FROM [SUTTER_1P_MIGRATION].IMP.[OPPORTUNITY_PARENT] AS T
	INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY] AS X ON T.[External_Id__c]=X.[EXTERNAL_ID__C]
	WHERE [rC_Giving__Giving_Frequency__c]  IS NOT null
	ORDER BY [zrefGFInsFreq]

		SELECT id, [EXTERNAL_ID__C] FROM [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY] WHERE Id='0066100000BpUuQAAV'
		SELECT [External_Id__c], [CloseDate], [Gift_Date__c]
		,rC_Giving__First_Payment_Date__c
		,[rC_Giving__Giving_End_Date__c] 
		,[rC_Giving__Payment_End_Date__c]
		FROM [SUTTER_1P_MIGRATION].[IMP].[OPPORTUNITY_PARENT] WHERE [External_Id__c]='WBRX-GT-05502-553-0000322195'

		SELECT * FROM [SUTTER_1P_DATA].dbo.[CHART_Pledge_Frequency]

		SELECT X.ID, T.CloseDate, T.[rC_Giving__Payment_End_Date__c]
		,T.Gift_Date__c
		,T.rC_Giving__First_Payment_Date__c
		,T.rC_Giving__Giving_End_Date__c
	 	,T.[External_Id__c] AS zrefExtId 
		FROM [SUTTER_1P_MIGRATION].IMP.[OPPORTUNITY_PARENT] AS T
		INNER JOIN [SUTTER_1P_MIGRATION].[XTR].[OPPORTUNITY] AS X ON T.[External_Id__c]=X.[EXTERNAL_ID__C]
		WHERE CAST(T.CloseDate AS DATE) > CAST(T.rC_Giving__Payment_End_Date__c AS DATE)  
		ORDER BY CAST(T.CloseDate AS DATE) asc

/*Test 2 issues --I-0187

	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Cons_Phone_v]
	WHERE [RE_DB_OwnerShort]='ABSF' AND [ImportID]='1288373'

	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Constituents_v] WHERE [RE_DB_Id]='ABSF-1288373'

	SELECT * FROM [SUTTER_1P_DATA].dbo.[CHART_PhoneType]


	SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].[Phones] 
	WHERE [RE_DB_OwnerShort]='ABSF' AND [ImportID]='1288373'
	SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].Phones_seq_1 
	WHERE [RE_DB_OwnerShort]='ABSF' AND [ImportID]='1288373'
	SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].[Phones_seq_1_final]
	WHERE [NEW_TGT_IMPORT_ID]='ABSF-1288373'
	SELECT * FROM [SUTTER_1P_MIGRATION].imp.contact WHERE [External_ID__c]='ABSF-1288373'


				SELECT		X1.[ID], T1.[Fax__c]
				--INTO		SUTTER_1P_MIGRATION.IMP.CONTACT_XCP
				FROM        SUTTER_1P_MIGRATION.IMP.CONTACT AS T1 
				INNER JOIN	SUTTER_1P_MIGRATION.XTR.CONTACT AS X1 ON T1.External_ID__c = X1.EXTERNAL_ID__C
				WHERE       T1.[Fax__c] IS NOT NULL 
				-- AND [T1].[External_ID__c]='ABSF-1288373'
				ORDER BY T1.External_ID__c
			--102

--I-0198
--deceased date not present. 

	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Ind_Relat_v]
	WHERE [RE_DB_Id]='ABSF-02869-700-0027253'

	select convert(datetime,'3/13/41',1)

			SELECT T.NEW_TGT_IMPORT_ID_IR AS RE_IRImpID__c
			
			,T.IRBDay 
			,LEN(T.IRBDay) lenIRBDay
			,ISDATE(T.IRBDay) BDayisdatevalue
			--,CASE WHEN ISDATE(T.[IRBDay])='1' THEN CONVERT(DATETIME, T.[IRBDay],1) ELSE NULL END AS bdayconvert
			--,MONTH(CAST(T.IRBDay AS DATE)) AS rC_Bios__Birth_Month__c
			--,DAY(CAST(T.IRBDay AS DATE)) AS rC_Bios__Birth_Day__c
			--,YEAR(CAST(T.IRBDay AS DATE))  AS rC_Bios__Birth_Year__c
			
			,T.IRDecDate, LEN(T.IRDecDate) lenIRDecDate, ISDATE(T.IRDecDate) isdatevalue
			,T.IRDeceased	AS	rC_Bios__Deceased__c
			,CASE WHEN LEN(T.IRDecDate)=10 THEN T.IRDecDate END AS rC_Bios__Deceased_Date__c
			,CASE WHEN LEN(T.IRDecDate)=10 THEN MONTH(T.IRDecDate ) WHEN LEN(T.IRDecDate)=7 THEN LEFT(T.IRDecDate,2) WHEN LEN(T.IRDecDate)=4 THEN NULL END AS rC_Bios__Deceased_Month__c
			,CASE WHEN LEN(T.IRDecDate)=10 THEN DAY(T.IRDecDate ) END AS rC_Bios__Deceased_Day__c
			,CASE WHEN LEN(T.IRDecDate)=10 THEN YEAR(T.IRDecDate )  WHEN LEN(T.IRDecDate)=7 THEN RIGHT(T.IRDecDate,4) WHEN LEN(T.IRDecDate)=4 THEN T.IRDecDate END AS rC_Bios__Deceased_Year__c

			FROM SUTTER_1P_DATA.dbo.HC_Ind_Relat_v T
			INNER JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T8 ON T.NEW_TGT_IMPORT_ID_CN=T8.RE_DB_ID 
			INNER JOIN SUTTER_1P_DATA.TBL.SF_IND_RELAT T9 ON T.RE_DB_ID=T9.IRImpID    --(IRImpID is the NEW_TGT_IMPORT_ID_OR)
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.Contact_HofH T2 ON T.NEW_TGT_IMPORT_ID_CN=T2.NoHH_ImportID  
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.IRPhones_seq_1_final T6 ON T.NEW_TGT_IMPORT_ID_IR=T6.NEW_TGT_IMPORT_ID_IR 
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.IRPhones_seq_2_final T7 ON T.NEW_TGT_IMPORT_ID_IR=T7.NEW_TGT_IMPORT_ID_IR 
			LEFT JOIN SUTTER_1P_MIGRATION.TBL.RELATIONSHIPS_MERGED_FROM_IR T10 ON T.NEW_TGT_IMPORT_ID_IR=T10.NEW_TGT_IMPORT_ID_IR
			WHERE (T.NEW_TGT_IMPORT_ID_IRLINK='' OR T.NEW_TGT_IMPORT_ID_IRLINK IS NULL)  
		--	AND (T.IRBDay IS NOT NULL OR T.[IRDecDate] IS NOT NULL)
			AND ISDATE(T.[IRBDay])=0 AND T.[IRBDay] IS NOT NULL 

--I-0194

SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Cons_Phone_v] WHERE [RE_DB_OwnerShort]='ABSF' AND [ImportID]='02869-593-0000383440'

--I0193

	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Constituents_v] WHERE [RE_DB_Id]='WBRX-05502-593-0000194983'
 
	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Cons_Phone_v]
	WHERE [RE_DB_OwnerShort]='WBRX' AND [ImportID]='05502-593-0000194983'

 
	SELECT * FROM [SUTTER_1P_DATA].dbo.[CHART_PhoneType]


	SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].[Phones] 
	WHERE [RE_DB_OwnerShort]='WBRX' AND [ImportID]='05502-593-0000194983'
	SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].Phones_seq_1 
	WHERE [RE_DB_OwnerShort]='WBRX' AND [ImportID]='05502-593-0000194983'
	SELECT * FROM [SUTTER_1P_MIGRATION].[TBL].[Phones_seq_1_final]
	WHERE [NEW_TGT_IMPORT_ID]='WBRX-05502-593-0000194983'
	SELECT * FROM [SUTTER_1P_MIGRATION].imp.contact WHERE [External_ID__c]='WBRX-05502-593-0000194983'

--I-0192

	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Ind_Relat_v] WHERE [ConsID]='1900004'

--I-0199
	
	SELECT [ConsID], [ImportID], [RE_DB_OwnerShort], [IRImpID], [IRLink]
	[IRFirstName], [IRLastName], [IRIsSpouse], [IRRelat], [IRRecip]
	 FROM [SUTTER_1P_DATA].dbo.[HC_Ind_Relat_v] WHERE [ConsID]='8453' AND [RE_DB_OwnerShort]='MPHF'

	 SELECT [ImportID], [ConsID], [Name], [RE_DB_OwnerShort], re_Db_id FROM [SUTTER_1P_DATA].dbo.[HC_Constituents_v]
	 WHERE [RE_DB_Id]='MPHF-8453'

--I-0196

	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Cons_Address_v] 
	WHERE [AddrLine1] IS null

	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Cons_Address_v] 
	WHERE ([AddrLine1] IS NOT NULL AND [AddrLine1]!='') AND [AddrLine1] NOT LIKE '%deceased%' AND [AddrLine1]!='dec'
	ORDER BY [AddrLine1]

	SELECT * FROM [SUTTER_1P_DATA].dbo.[HC_Cons_Address_v] 
	WHERE [AddrLine1] LIKE '%deceased%'
*/
