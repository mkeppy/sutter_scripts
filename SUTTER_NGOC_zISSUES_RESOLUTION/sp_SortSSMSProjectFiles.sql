	
CREATE PROCEDURE SortSSMSProjectFiles
(
    @ProjectFileName VARCHAR(512)
)
AS

DECLARE @x XML, @qry NVARCHAR(500), @param NVARCHAR(100)
SELECT @qry = N'
    SELECT 
        @x = CAST(bulkcolumn AS XML) 
    FROM OPENROWSET(BULK ''' + @ProjectFileName + ''', 
        SINGLE_BLOB) AS x'
PRINT @qry
SELECT @param = '@x XML OUTPUT'
EXECUTE sp_executesql @qry, @param, @x OUTPUT 

SELECT @x.query('
<SqlWorkbenchSqlProject 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
    Name="{SqlWorkbenchSqlProject/@Name}">
  <Items>
    {
        for $lf in SqlWorkbenchSqlProject/Items/LogicalFolder
        return 
            if ($lf/@Name = "Queries") 
            then 
                <LogicalFolder Name="{$lf/@Name}" 
                    Type="{$lf/@Type}" Sorted="{$lf/@Sorted}">
                    <Items>
                        {
                            for $i in $lf/Items/*
                            order by $i/@Name
                            return $i
                        }
                    </Items>
                </LogicalFolder>
            else $lf
    }
  </Items>
</SqlWorkbenchSqlProject>    
')


/* EXECUTE SortSSMSProjectFiles
    @ProjectFileName = 'D:\01_Data_Migrations\SUTTER_NGOC\SQL Queries&Views\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_MIGRATION\SUTTER_NGOC_MIGRATION.ssmssqlproj'
    
*/