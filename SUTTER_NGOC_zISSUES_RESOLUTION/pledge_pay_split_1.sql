
			SELECT
			ip.PledgeID,
			ip.PaymentID,
			paymentsplit.CampaignID,
			paymentsplit.FundID,
			paymentsplit.AppealID,
			paymentsplit.PackageID,
			paymentsplit.Amount paymentsplitamount,
			paymentsplit.giftsplitid,
			ip.amount,
			dbo.PaymentSplitAllocation(
				ip.PledgeID,
				ip.amount, 
				paymentsplit.giftsplitid, 
				ip.PaymentID,
				ISNULL(paymentsplit.CampaignID, 0), 
				ISNULL(paymentsplit.FundID, 0),
				ISNULL(paymentsplit.AppealID, 0), 
				ISNULL(paymentsplit.PackageID, 0), 
				Paymentsplit.Amount) AS PaymentAllocation
			FROM
				dbo.GiftSplit paymentsplit
			INNER JOIN
			(
				SELECT pledgeid, paymentid, SUM(Amount) Amount
				FROM dbo.installmentpayment
				GROUP BY pledgeid, paymentid
			) ip ON paymentsplit.giftid = ip.paymentid
			ORDER BY ip.PledgeID,
			ip.PaymentID
		--	WHERE ip.PledgeID='119162'  OR ip.PledgeId='124761' OR ip.PledgeId='130009' OR ip.PledgeId='159419'
 
  SELECT * FROM dbo.HC_Gift_Link_v WHERE GFType ='pledge'