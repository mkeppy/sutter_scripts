
--Transaction_Source='NON_Payment'

	--TRANSACTIONS OPPORTUNITIES FROM NON_PAYMENT 
					SELECT  
					T.[ACCOUNT:External_Id__c]
					,'T-'+T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS External_Id__c
					,'T-'+T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS RE_GSplitImpID__c
					,'T-'+T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitGFImpID AS RE_GFImpID__c
					,T.[GSplitRE_DB_OwnerShort]+'-'+T.[GSplitRE_DB_Tbl]+'-'+T.GSplitImpID AS [rC_Giving__Parent__r:External_ID__c]
					--reference
					,T3.ID zrefAccountId
					,T.GFType AS zrefGFType
					,NULL AS zrefParentGFImpID
					,NULL AS zrefPaymentGFImpID

					FROM SUTTER_MGEA.TBL.GIFT T
					LEFT JOIN SUTTER_MGEA.XTR.ACCOUNT T3 ON T.[ACCOUNT:External_Id__c]=T3.EXTERNAL_ID__C 
					WHERE T.GSplitGFImpID='03947-545-0000458570'
/*transaction*/		WHERE T.Transaction_Source='NON_PAYMENT'
					 -- ORDER BY T.ImportId  273,838
					 
					 
					 
--Transaction_Source='Payment'

	--TRANSACTIONS OPPORTUNITIES FROM NON_PAYMENT 
					SELECT  
					 T3.Name
					,T2.[ACCOUNT:External_Id__c]
					,T3.[GSplitRE_DB_OwnerShort]+'-'+T3.[GSplitRE_DB_Tbl]+'-'+T3.GSplitImpID AS External_Id__c
					,T3.[GSplitRE_DB_OwnerShort]+'-'+T3.[GSplitRE_DB_Tbl]+'-'+T3.GSplitImpID AS RE_GSplitImpID__c
					,T3.[GSplitRE_DB_OwnerShort]+'-'+T3.[GSplitRE_DB_Tbl]+'-'+T3.GSplitGFImpID AS RE_GFImpID__c
					,T2.[GSplitRE_DB_OwnerShort]+'-'+T2.[GSplitRE_DB_Tbl]+'-'+T2.GSplitImpID AS [rC_Giving__Parent__r:External_ID__c]
					--reference
				--	,T3.ID zrefAccountId
				--	,T.GFType AS zrefGFType
					,T1.GFImpID AS zrefParent_GFImpID
					,T1.GFPaymentGFImpID zrefPayment_GFImpID

					FROM SUTTER_DATA.dbo.HC_Gift_Link_v AS T1 
/*parent*/			INNER JOIN SUTTER_MGEA.TBL.GIFT AS T2 ON T1.GFImpID = T2.GSplitGFImpID AND T1.RE_DB_OwnerShort = T2.GSplitRE_DB_OwnerShort
/*transaction*/		INNER JOIN SUTTER_MGEA.TBL.GIFT AS T3 ON T1.GFPaymentGFImpID = T3.GSplitGFImpID AND T1.RE_DB_OwnerShort = T3.GSplitRE_DB_OwnerShort
					AND T2.GFSplitSequence = T3.GFSplitSequence
					LEFT JOIN SUTTER_MGEA.XTR.ACCOUNT T4 ON T2.[ACCOUNT:External_Id__c]=T4.EXTERNAL_ID__C 
					
/*transaction*/		WHERE T3.Transaction_Source='PAYMENT' AND T3.Name='Bank of America Foundation'
					 -- ORDER BY T.ImportId  273,838
					 
			
			
				 
					 
SELECT		T4.Id, T4.Name, T1.GFType, T2.GSplitImpID, T2.GSplitGFImpID, T2.GFSplitSequence, T2.GSplitAmt, 
			T3.GSplitGFImpID AS payGFImpID, T3.GSplitImpID AS payGSplitImpID, 
            T3.GFSplitSequence AS paySplitSequency, T3.GSplitAmt AS payGSplitAmt
            
FROM        SUTTER_DATA.dbo.HC_Gift_Link_v AS T1 
			INNER JOIN SUTTER_MGEA.TBL.GIFT AS T2 ON T1.GFImpID = T2.GSplitGFImpID AND T1.RE_DB_OwnerShort = T2.GSplitRE_DB_OwnerShort
			INNER JOIN SUTTER_MGEA.TBL.GIFT AS T3 ON T1.GFPaymentGFImpID = T3.GSplitGFImpID AND T1.RE_DB_OwnerShort = T3.GSplitRE_DB_OwnerShort
			AND T2.GFSplitSequence = T3.GFSplitSequence
			LEFT JOIN SUTTER_MGEA.XTR.ACCOUNT T4 ON T2.[ACCOUNT:External_Id__c]=T4.EXTERNAL_ID__C 
			WHERE		T1.GFImpID='04121-057-0000069674'



				SELECT T1.*
				FROM SUTTER_MGEA.TBL.GIFT_WRITEOFF T1
/*Parent ID*/	INNER JOIN SUTTER_DATA.DBO.HC_Gift_SplitGift_v T3 ON T1.GFLink=T3.GFImpID AND T1.RE_DB_OwnerShort=T3.RE_DB_OwnerShort  
				LEFT JOIN SUTTER_MGEA.XTR.ACCOUNT T2 ON T1.[ACCOUNT:External_Id__c]=T2.EXTERNAL_ID__C 
				
				
SELECT * FROM SUTTER_MGEA.IMP.OPPORTUNITY_PARENT WHERE External_Id__c='SMCF-GT-03947-700-0000189622'		

SELECT * FROM SUTTER_MGEA.IMP.zUPDATE_OPPORTUNITY_ExpAmt WHERE External_Id__c='SMCF-GT-03947-700-0000189622'		
	
				
				SELECT GFCheckDate, LEN(GFCheckDate) l, ISDATE(GFCheckDate) isdatef,
				CASE WHEN LEN(GFCheckDate)=8 THEN SUBSTRING(GFCheckDate, 5,2) +'/'+ SUBSTRING(GFCheckDate, 7,2) +'/'+LEFT(GFCheckDate , 4) ELSE NULL END AS rC_Giving__Tribute_Effective_Date__c
				,CASE WHEN len(GFCheckDate)=10 THEN GFCheckDate END AS CD
				FROM SUTTER_MGEA.tbl.gift 
				WHERE GFCheckDate IS NOT null
				ORDER BY l ASC 