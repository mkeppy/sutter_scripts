		SELECT DISTINCT 
				 dbo.fnc_OwnerID() AS OwnerID
				,dbo.fnc_RecordType('Preference_Standard') AS RecordTypeID
				,rC_Bios__External_ID__c= (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.ImportId+'-'+T.SolicitCode)
				,[rC_Bios__Account__r:External_ID__c]=CASE WHEN T1.KeyInd='O' THEN T1.NEW_TGT_IMPORT_ID END 
				,[rC_Bios__Contact__r:External_ID__c]=CASE WHEN T1.KeyInd='I' THEN T1.NEW_TGT_IMPORT_ID END 
				,rC_Bios__Category__c=T2.PREFERENCE_Category
				,rC_Bios__Subcategory__c=T2.PREFERENCE_SubCategory
				,rC_Bios__Type__c=T2.[PREFERENCE_Type]
				,rC_Bios__Subtype__c=T2.[PREFERENCE_Subtype]
				,rC_Bios__Value__c=T2.PREFERENCE_Value 
				,RE_Category__c = T.SolicitCode
				,rC_Bios__Affiliation__c=T.[RE_DB_OwnerShort]
				,LEN( (T.RE_DB_OwnerShort+'-'+T.RE_DB_Tbl+'-'+T.ImportId+'-'+T.SolicitCode)) AS zref_len_extid
		 
				--reference
				,T1.KeyInd AS zrefKeyInd
				,'ConsSolCode' AS zrefRecSource
				INTO [SUTTER_1P_MIGRATION].IMP.PREFERENCE_SOLCODE_v2
				FROM SUTTER_1P_DATA.dbo.HC_Cons_SolicitCodes_v T
				left JOIN SUTTER_1P_DATA.DBO.HC_Constituents_v T1 ON T.ImportID=T1.ImportID AND T.RE_DB_OwnerShort=T1.RE_DB_OwnerShort
				left JOIN SUTTER_1P_DATA.dbo.CHART_ConsSolicitCodes T2 ON T.SolicitCode=T2.SolicitCode AND T.RE_DB_OwnerShort=T2.RE_DB_OwnerShort
				WHERE T2.[Convert]='Yes' AND T2.SF_Object_1 ='RC_BIOS_PREFERENCE__C' -- AND T.ImportId LIKE '%00001-079-0000043740%'
			
	    	
		
BEGIN--CHECK DUPLICATES
		SELECT DISTINCT * 
		FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE_SOLCODE_v2
		WHERE rC_Bios__External_ID__c IN (SELECT DISTINCT rC_Bios__External_ID__c FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE_SOLCODE_v2 GROUP BY rC_Bios__External_ID__c HAVING COUNT(*)>1)
		ORDER BY rC_Bios__External_ID__c

    	SELECT DISTINCT * 
		FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE_SOLCODE_v2
		WHERE rC_Bios__External_ID__c IS NULL 

		SELECT DISTINCT * 
		FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE_SOLCODE_v2
		WHERE [rC_Bios__Account__r:External_ID__c] IS NULL AND [rC_Bios__Contact__r:External_ID__c] is NULL 
		
		SELECT DISTINCT * 
		FROM SUTTER_1P_MIGRATION.IMP.PREFERENCE_SOLCODE_v2
		WHERE LEN(rC_Bios__External_ID__c) >255

		BEGIN 
			USE [SUTTER_1P_MIGRATION] 
 			EXEC sp_FindStringInTable '%"%', 'IMP', 'PREFERENCE_SOLCODE_v2'
 		END 
END	
			
			
				 SELECT * FROM [SUTTER_1P_MIGRATION].IMP.PREFERENCE_SOLCODE_v2
			SELECT * FROM [SUTTER_1P_MIGRATION].imp.preference WHERE rC_Bios__External_ID__c LIKE '%-OC-%'		 


			SELECT * FROM [SUTTER_1P_DELTA_migration].imp.[PREFERENCE] WHERE [rC_Bios__External_ID__c] LIKE '%-oc-%'